/*
 * This file is generated and updated by Sencha Cmd. You can edit this file as
 * needed for your application, but these edits will have to be merged by
 * Sencha Cmd when upgrading.
 */
 Ext.define('AppGlobals', {
    singleton: true,
    agente: 0,
    nombreAgente: '',
    usuario: '[vacio]',
    legajoUser: 0,
    primeraContr: '',
    feriados: new Array(),
    licenciaSeleccionadaAlta: 0, //Guarda el id de la licencia seleccionada para darle de alta
    licenciaSeleccionadaAltaCorridos: 0, //Guarda los dias corridos o habiles
    confirmarLicenciaAltaCorridos: 0, //Guarda los dias corridos o habiles al confirmar una licencia
    licenciaSeleccionadaMismoAnio: 0, //Guarda el valor si la licencia seleccionada debe ser en un mismo anio o no
    diasDisponibles: 0,//Compensatoria
    diasDisponiblesDobleTurno: 0,//Doble turno
    diasLicPorAnio: 0,
    itemsPorPagina: 50,
    permiso: 0,
    evaluador:"",
    rrhh: "no",
    dpto_personal: "no",
    id_licencia: 0,//Guardo el id de la licencia seleccionada en historial de licencias (Historial.js)
    licencia: '',//Guardo la licencia seleccionada en historial de licencias (Historial.js)
    legEvaluador: 0,
    ver_evaluados: "",
    exist_evaluacion:"",
    //A ser usados en las estadisticas historicas por año de licencias
    arrFieldsHist:new Array(),
    arrLicHist: new Array(),
    domicilioAgente: "",
    //A ser usados en la pestaña de configuracion de evaluadores
    legAgenteAConfigurar: 0,

    //Controlo que no se esté editando nada, caso contrario largo cartel de advertencia cuando se 
    //está por abandonar una edicion sin guardar cambios
    isEditing: 'no',

    //Variable para guardar los items a cargar en el formulario de evaluacion
    itemsCargarFormEvaluacion: new Array(),

    //Variable para guardar el legajo en contabilidad->cuentas
    legCuentaBancaria: '',

    //Variable para guardar datos de liquidacion en  contabilidad->archivo usuras
    fecha_usu: '',
    liquidacion_usu: '',
    periodo_usu: '',

    //Variables para guardar agente seleccionado para cargar compensatorias masivamente
    legAgenteMasiva: 0,
    nombApAgenteMasiva: '',

    //Guardar variable que muestra solo agentes activos o todos
    showAgentes: '',

    //Guardar record editar capacitacion
    recordEditCapacitacion: '',

    //Guardar record editar comision
    recordEditComision: '',

    //Guardar record editar historia clinica
    recordEditHistoriaClinica: '',

    //Guardar record editar solicitud de licencia
    recordEditSolicitudLicencia: '',

    //Guardar record editar firma digital
    recordEditFirmaDigital: '',
    
    //Arreglo con permisos del usuario
    permisos: '{}',

    //Arreglo con todo el registro del agente al hacer doble click
    registroAgente: '{}'

});
 
Ext.application({
    name: 'personal',

    extend: 'personal.Application',

    requires: [
        'personal.view.main.Main'
    ],

    // The name of the initial view to create. With the classic toolkit this class
    // will gain a "viewport" plugin if it does not extend Ext.Viewport. With the
    // modern toolkit, the main view will be added to the Viewport.
    //
    //mainView: 'personal.view.main.Main'

    //-------------------------------------------------------------------------
    // Most customizations should be made to personal.Application. If you need to
    // customize this file, doing so below this section reduces the likelihood
    // of merge conflicts when upgrading to new versions of Sencha Cmd.
    //-------------------------------------------------------------------------
});
