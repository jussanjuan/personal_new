/**
 * The main application class. An instance of this class is created by app.js when it
 * calls Ext.application(). This is the ideal place to handle application launch and
 * initialization details.
 */

 


Ext.define('personal.Application', {
    extend: 'Ext.app.Application',

    name: 'personal',

    quickTips: false,
    platformConfig: {
        desktop: {
            quickTips: true
        }
    },
    requires: [
        'personal.view.login.Login'
        
    ],
    stores: [
        // TODO: add global / shared stores here
    ],

    launch: function () {
        // TODO - Launch the application
        console.log("App lanzada");
        // It's important to note that this type of application could use
        // any type of storage, i.e., Cookies, LocalStorage, etc.
        var loggedIn;
        var me = this;
        // Check to see the current value of the localStorage key
        loggedIn = localStorage.getItem("PersonalLoggedIn");
        if(loggedIn)
        {
            Ext.Ajax.request({
                url: 'app/usr.session.php',
                async: false,
                success: function(response, opts) 
                {
                    //console.log("user session "+response.responseText);
                    var obj = Ext.decode(response.responseText);
                    //console.table(obj);
    
                    AppGlobals.sucursal= obj.lugar;
                    AppGlobals.usuario=obj.nombre_real;
                    AppGlobals.permiso= obj.permiso;
                    AppGlobals.evaluador= obj.evaluador;
                    AppGlobals.legajoUser= obj.legajo;
                    AppGlobals.exist_evaluacion = obj.exist_evaluacion;
                    AppGlobals.permisos = obj.permisos;
    
                    //console.log(AppGlobals.permisos[0]);
                    if(typeof AppGlobals.permisos == 'undefined' || typeof AppGlobals.permisos[0] == 'undefined')
                    {
                        Ext.Msg.alert('Error!', 'Error al cargar los permisos, o el usuario no tiene permisos asignados para el sistema.');
    
                        // Remove the localStorage key/value
                        localStorage.removeItem('PersonalLoggedIn');
                        /*console.log(me.getView());
                        // Remove Main View
                        me.getView().destroy();
    
                        // Add the Login Window
                        Ext.create({
                            xtype: 'login'
                        });    */
                    }
                    console.log("Existe evaluacion?: "+AppGlobals.exist_evaluacion);
                    if(AppGlobals.evaluador == "si")
                    {
                        AppGlobals.legEvaluadorEntry = obj.legajo;
                    }
                    AppGlobals.primeraContr= obj.primeraContr;
                    //console.log("Primera contrasena: "+AppGlobals.primeraContr);
                    if(AppGlobals.primeraContr == 'si')
                    {
                        Ext.Msg.alert('Informaci&oacute;n', 'Si este es su primer acceso al sistema o su contrase&ntilde;a nunca ha sido cambiada, se recomienda hacerlo haciendo click en el icono de llaves ubicado en la esquina superior derecha.')
                    }
                    var tipo_user="";
                    if( AppGlobals.permiso == "1")
                    {
                        tipo_user = "ADMINISTRADOR";
                    }
                    else
                    {
                        tipo_user = "INVITADO";
                    }
                    AppGlobals.rrhh=obj.rrhh;
                    AppGlobals.dpto_personal = obj.dpto_personal;    
                },
                
                failure: function(response, opts) 
                {
                    console.log(response.responseText);
                    var obj = Ext.decode(response.responseText);
                    Ext.msg.alert("Error", obj.error);
                    
                }
            });
                            
            // Carga los feriados
            Ext.Ajax.request({
                url: 'app/proxy.feriados.php',
                params: {
                    tarea: 'diasLaborales',
                    method: 'GET',
                    anio: new Date().getFullYear()
                },
                success: function(response){
                    var obj = Ext.decode(response.responseText);
                    console.log(obj);
                    if(obj.results.length==0)
                    {
                        //Muestro el cartel solo si el usuarios pertenece al departamento de personal o RRHH, dado que ellos tienen incumbencia en la carga de feriados
                        if(AppGlobals.rrhh== "si" || AppGlobals.dpto_personal== "si")
                        {
                            Ext.Msg.alert('Advertencia', 'Hay pocos o ning&uacute;n feriado cargado, por favor revise y cargue todos los feriados del per&iacute;odo actual.');
                        }
                    }
                    else
                    {
                        //onCargaFeriados(response);    
                    }
                    
                },
                failure: function(response, opts) {
                    Ext.Msg.alert('Error', 'No se pudo conectar a la base de datos para cargar los feriados del a&ntilde;o actual. Error de servidor. <br>Estado: ' + response.status);
                    console.log('server-side failure with status code ' + response.status);
                }
            });
        }
        // This ternary operator determines the value of the TutorialLoggedIn key.
        // If TutorialLoggedIn isn't true, we display the login window,
        // otherwise, we display the main view
        Ext.create({
            xtype: loggedIn ? 'app-main' : 'login'
        });
    },

    onAppUpdate: function () {
        Ext.Msg.confirm('Actualizar aplicaci&oacute;n', 'Esta aplicaci&oacute;n tiene una actualiaci&oacute;n, recargar?',
            function (choice) {
                if (choice === 'yes') {
                    window.location.reload();
                }
            }
        );
    }
});
