<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require '../res/PHPMailer-master/src/Exception.php';
require '../res/PHPMailer-master/src/PHPMailer.php';
require '../res/PHPMailer-master/src/SMTP.php';

function escribir_log($sistema = "personal", $legajo, $legajo_objetivo = 0, $tabla, $operacion, $log, $fecha_hora)
{
    global $conn;
    $data = array();
    $str_limpia = addslashes($log);
    $str_ins = "INSERT INTO usuarios_log (sistema, legajo, legajo_objetivo, tabla, operacion, log, fecha) VALUES ('".$sistema."', ".$legajo.", ".$legajo_objetivo.", '".$tabla."', ".$operacion.", '".$str_limpia."', '".$fecha_hora."')";
    //echo $str_ins;
    $cons_ins = mysqli_query($conn, $str_ins);
    if(!$cons_ins)
    {
        $data["success"] = false;
        $data["error"] = mysqli_error($conn);
    }
    else
    {
        $data["success"] = true;
        $data["msg"] = "Log escrito correctamente";
    }
    return json_encode($data);
}

function sendMailCargaLicencia($legajo, $id_licencia)
{
    global $conn;
    $data = array();
    $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
    try 
    {
        $str_emp = "SELECT * FROM general WHERE legajo=".$legajo;
        $cons_emp = $conn->query($str_emp);
        if(!$cons_emp)
        {
            throw new Exception("Error al consultar la base de datos de agentes. Error BD: ".$conn->error);
        }
        $obj_emp = $cons_emp->fetch_object();
        $str_fuero = "SELECT l.nro_fuero FROM general g LEFT JOIN lugar l ON g.lugaractu=l.nrolugar WHERE g.legajo=".$legajo;
        $cons_fuero = $conn->query($str_fuero);
        if(!$cons_fuero)
        {
            throw new Exception("Error al consultar la base de datos de fueros. Error BD: ".$conn->error);
        }  
        $obj_fuero = $cons_fuero->fetch_object();  
        $str_lic = "SELECT l.*, tl.nombre FROM licencias l LEFT JOIN tipo_licencias tl ON l.tipo_licencia=tl.id WHERE l.id=".$id_licencia;
        $cons_lic = $conn->query($str_lic);
        if(!$cons_lic)
        {
            throw new Exception("Error al consultar la base de datos de licencias. Error BD: ".$conn->error);
        }
        $obj_lic = $cons_lic->fetch_object();
        $desde = new DateTime($obj_lic->desde);
        $hasta = new DateTime($obj_lic->hasta);
        $subject = "Licencia cargada";
        $est = $obj_emp->sexo=='M'?"Estimado":"Estimada";
        
        $enviar_a_corp = $obj_emp->mail_corporativo;
        
        //Server settings
        //$mail->SMTPDebug = 2;                                 // Enable verbose debug output
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.sanjuan.gov.ar';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'milegajo';                 // SMTP username
        $mail->Password = 'M1l2g3j4';                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to

        //Recipients
        //$mail->setFrom('soporte-milegajo@jussanjuan.gov.ar', 'Soporte legajo electronico');
        $mail->setFrom('milegajo@jussanjuan.gov.ar', 'Notificaciones Mi Legajo');
        if($enviar_a_corp!="")
        {
            $mail->addAddress($enviar_a_corp);
        }
        else
        {
            throw new Exception("El agente no tiene mail corporativo asignado");
        }
        
        //Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = $subject;
        $body = file_get_contents('template_mail_licencia.html');
        $body = $obj_fuero->nro_fuero==2?str_replace("_organismo_", "La Oficina Administrativa", $body):str_replace("_organismo_", "El Departamento de Personal", $body);
        $body = $obj_fuero->nro_fuero==2?str_replace("_acuerdo_", "Acuerdo de Superintendencia Nro. 36/2017 y Resoluci&oacute;n 3768", $body):str_replace("_acuerdo_", "Acuerdo de Superintendencia Nro. 36/2017", $body);
        
        $body = str_replace("_usuario_", $est." ".ucwords(strtolower($obj_emp->nombre)), $body);
        $body = str_replace("_nombre_licencia_", $obj_lic->nombre, $body);
        $body = str_replace("_desde_", $desde->format("d-m-Y"), $body);
        $body = str_replace("_hasta_", $hasta->format("d-m-Y"), $body);
        $body = str_replace("_expediente_", $obj_lic->expediente, $body);
        
        if($obj_lic->tipo_licencia==10 || $obj_lic->tipo_licencia==13 || $obj_lic->tipo_licencia==18 || $obj_lic->tipo_licencia==20 || $obj_lic->tipo_licencia==21)
        {
            $body = str_replace("_texto_acreditar_", ".", $body);
        }
        else
        {
            $body = str_replace("_texto_acreditar_", ", la que deber&aacute; acreditar a su reintegro conforme lo establecido por Acuerdo General Nro. 24/1998", $body);
        }
        $alt_body = $est.' '.$obj_emp->nombre.': Se ha cargado una licencia '.$obj_lic->nombre.' del '.$desde->format("d-m-Y").' al '.$hasta->format("d-m-Y").'. Total dias: '.$obj_lic->dias_licencia.'. Saludos cordiales, Mi Legajo';
        $mail->Body = $body;
        
        $obj_fuero->nro_fuero==2?$mail->AddEmbeddedImage('../res/img/mail/logo_ministerio_publico_100.jpg','logo'):$mail->AddEmbeddedImage('../res/img/mail/logo_100.png','logo');
        $mail->AddEmbeddedImage('../res/img/mail/rounder-up.png','header');
        $mail->AddEmbeddedImage('../res/img/mail/rounder-dwn.png','footer');
        
        $mail->AddEmbeddedImage('../res/img/mail/facebook@2x.png','facebook');
        $mail->AddEmbeddedImage('../res/img/mail/instagram@2x.png','instagram');
        $mail->AddEmbeddedImage('../res/img/mail/twitter@2x.png','twitter');
        
        $mail->AltBody = $alt_body;
        $mail->send();
                
        //Cargo envio en tabla de notificaciones
        $str_ins_not = "INSERT INTO notificaciones_licencias (tipo_notificacion, id_licencia, tipo_movimiento, log) VALUES (1, ".$id_licencia.", 1, '".date("Y-m-d H:i:s")."')";
        $cons_ins_not = $conn->query($str_ins_not);
        if(!$cons_ins_not)
        {
            throw new Exception("Error al escribir notificacion. Error BD: ".$conn->error);
        }
        $conn->commit();
        $data["success"] = true;
        $data["msg"] = "Mail enviado correctamente";
        
    } 
    catch (Exception $e) 
    {
        $conn->rollBack();
        $data["success"] = false;
        $data["error"] = $e->getMessage();
    }
    return $data;
}

function sendMailDesisteLicencia($legajo, $id_licencia)
{
    global $conn;
    $data = array();
    $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
    try 
    {
        $str_emp = "SELECT * FROM general WHERE legajo=".$legajo;
        $cons_emp = $conn->query($str_emp);
        if(!$cons_emp)
        {
            throw new Exception("Error al consultar la base de datos de agentes. Error BD: ".$conn->error);
        }
        $obj_emp = $cons_emp->fetch_object();
        $str_fuero = "SELECT l.nro_fuero FROM general g LEFT JOIN lugar l ON g.lugaractu=l.nrolugar WHERE g.legajo=".$legajo;
        $cons_fuero = $conn->query($str_fuero);
        if(!$cons_fuero)
        {
            throw new Exception("Error al consultar la base de datos de fueros. Error BD: ".$conn->error);
        }  
        $obj_fuero = $cons_fuero->fetch_object();
        $str_lic = "SELECT l.*, tl.nombre FROM licencias l LEFT JOIN tipo_licencias tl ON l.tipo_licencia=tl.id WHERE l.id=".$id_licencia;
        $cons_lic = $conn->query($str_lic);
        if(!$cons_lic)
        {
            throw new Exception("Error al consultar la base de datos de licencias. Error BD: ".$conn->error);
        }
        $obj_lic = $cons_lic->fetch_object();
        $desde = new DateTime($obj_lic->desde);
        $hasta = new DateTime($obj_lic->hasta);
        $subject = "Licencia desistida";
        $est = $obj_emp->sexo=='M'?"Estimado":"Estimada";
        
        $enviar_a_corp = $obj_emp->mail_corporativo;
        
        //Server settings
        //$mail->SMTPDebug = 2;                                 // Enable verbose debug output
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.sanjuan.gov.ar';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'milegajo';                 // SMTP username
        $mail->Password = 'M1l2g3j4';                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to

        //Recipients
        //$mail->setFrom('soporte-milegajo@jussanjuan.gov.ar', 'Soporte legajo electronico');
        $mail->setFrom('milegajo@jussanjuan.gov.ar', 'Notificaciones Mi Legajo');
        if($enviar_a_corp!="")
        {
            $mail->addAddress($enviar_a_corp);
        }
        else
        {
            throw new Exception("El agente no tiene mail corporativo asignado");
        }
        
        //Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = $subject;
        $body = file_get_contents('template_mail_desiste.html');
        $body = $obj_fuero->nro_fuero==2?str_replace("_organismo_", "La Oficina Administrativa", $body):str_replace("_organismo_", "El Departamento de Personal", $body);
        $body = $obj_fuero->nro_fuero==2?str_replace("_acuerdo_", "Acuerdo de Superintendencia Nro. 36/2017 y Resoluci&oacute;n 3768", $body):str_replace("_acuerdo_", "Acuerdo de Superintendencia Nro. 36/2017", $body);
        $body = str_replace("_usuario_", $est." ".ucwords(strtolower($obj_emp->nombre)), $body);
        $body = str_replace("_nombre_licencia_", $obj_lic->nombre, $body);
        $body = str_replace("_desde_", $desde->format("d-m-Y"), $body);
        $body = str_replace("_hasta_", $hasta->format("d-m-Y"), $body);
        $body = str_replace("_expediente_", $obj_lic->expediente, $body);
        
        $alt_body = $est.' '.$obj_emp->nombre.': Se ha desistido una licencia '.$obj_lic->nombre.' del '.$desde->format("d-m-Y").' al '.$hasta->format("d-m-Y").'. Total dias: '.$obj_lic->dias_licencia.'. Saludos cordiales, Mi Legajo';
        $mail->Body = $body;
        //exit;
        $obj_fuero->nro_fuero==2?$mail->AddEmbeddedImage('../res/img/mail/logo_ministerio_publico_100.jpg','logo'):$mail->AddEmbeddedImage('../res/img/mail/logo_100.png','logo');
        $mail->AddEmbeddedImage('../res/img/mail/rounder-up.png','header');
        $mail->AddEmbeddedImage('../res/img/mail/rounder-dwn.png','footer');
        
        $mail->AddEmbeddedImage('../res/img/mail/facebook@2x.png','facebook');
        $mail->AddEmbeddedImage('../res/img/mail/instagram@2x.png','instagram');
        $mail->AddEmbeddedImage('../res/img/mail/twitter@2x.png','twitter');
        
        $mail->AltBody = $alt_body;
        $mail->send();
                
        //Cargo envio en tabla de notificaciones
        $str_ins_not = "INSERT INTO notificaciones_licencias (tipo_notificacion, id_licencia, tipo_movimiento, log) VALUES (1, ".$id_licencia.", 2, '".date("Y-m-d H:i:s")."')";
        $cons_ins_not = $conn->query($str_ins_not);
        if(!$cons_ins_not)
        {
            throw new Exception("Error al escribir notificacion. Error BD: ".$conn->error);
        }
        $conn->commit();
        $data["success"] = true;
        $data["msg"] = "Mail enviado correctamente";
        
    } 
    catch (Exception $e) 
    {
        $conn->rollBack();
        $data["success"] = false;
        $data["error"] = $e->getMessage();
    }
    return $data;
}


?>