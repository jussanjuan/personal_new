Ext.define('personal.model.Agentes', {
	extend: 'Ext.data.Model',
	fields: [
		'id', 'apNomLeg', 'apellido', 'nombre', 'legajo'
	],
        
	
	proxy: {
        type: 'rest',
        url: 'app/proxy.agentes.php',
        params: {
            act: 'all'
        },
        reader: {
            type: 'json',
            rootProperty: 'results',
            successProperty: 'success'
        }
    }

});