Ext.define('personal.model.AniosCertificados', {
	extend: 'Ext.data.Model',
	fields: ['id', 'anio'],
	
	proxy: {
        type: 'ajax',
        method: 'POST',
        url: 'app/proxy.familia.php',
        extraParams:{
            act: 'getYears'
        },
        reader: {
            type: 'json',
            rootProperty: 'results',
            successProperty: 'success'
        }
    }

});