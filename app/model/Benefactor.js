Ext.define('RH.model.Benefactor', {
	extend: 'Ext.data.Model',
	fields: [
		'id', 'apNomLeg'
	],
        
	
	proxy: {
        type: 'rest',
        url: 'app/proxy.agentes.php',
        params: {
            act: 'all'
        },
        reader: {
            type: 'json',
            rootProperty: 'results',
            successProperty: 'success'
        }
    }

});