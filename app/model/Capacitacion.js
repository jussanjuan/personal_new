Ext.define('personal.model.Capacitacion', {
	extend: 'Ext.data.Model',
	fields: ['id', 'legajo', 'nombre', 'tipo', 'origen', 'inherencia', {name: 'fecha', type: 'date', dateFormat: 'Y-m-d'}, 'cant_horas', 'modalidad', 'evaluacion', 'nota_evaluacion', 'observaciones']
});