Ext.define('personal.model.CapacitacionModalidad', {
	extend: 'Ext.data.Model',
	fields: ['id', 'nombre']
});