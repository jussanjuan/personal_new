Ext.define('personal.model.CapacitacionTipo', {
	extend: 'Ext.data.Model',
	fields: ['id', 'nombre']
});