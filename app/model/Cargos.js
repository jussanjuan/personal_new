Ext.define('personal.model.Cargos', {
	extend: 'Ext.data.Model',
	fields: ['nrocargo', 'cargo', 'acuerdo', 
        {name: 'desde', type: 'date', dateFormat: 'Y-m-d'}, 
        'sitlabo', 'cupo', 'cantidad']
	
	

});