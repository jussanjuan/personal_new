Ext.define('personal.model.Familia', {
	extend: 'Ext.data.Model',
	fields: ['id', 'relacion', 'relacion_nombre', 'nombre', 'apellido', 'dni', 
        {name: 'fecha_nac', type: 'date', dateFormat: 'Y-m-d'}, {
            name: 'certificado_inicio',
            type: 'string',
            convert: function(val, row) {
                return (val == 0 || !val) ? 'No' : 'Si';
            }
        },{
            name: 'certificado_fin',
            type: 'string',
            convert: function(val, row) {
                return (val == 0 || !val) ? 'No' : 'Si';
            }
        },{
            name: 'guardapolvo',
            type: 'string',
            convert: function(val, row) {
                return (val == 0 || !val) ? 'No' : 'Si';
            }
        }, 'anio', 'nivel_estudios', 'discapacitado', 'id_certificado', 'observaciones_tesoreria']
	
    

});