Ext.define('personal.model.Feriado', {
	extend: 'Ext.data.Model',
    
	fields: [
		'id', 'anio', 'mes', 'dia', 'ocasion', {name: 'fecha', type: 'date', dateFormat: 'Y-m-d' }
    ]
        
	

});