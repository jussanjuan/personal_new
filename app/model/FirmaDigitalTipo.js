Ext.define('personal.model.FirmaDigitalTipo', {
	extend: 'Ext.data.Model',
	fields: ['id', 'nombre']
});