Ext.define('personal.model.LicenciaTipo', {
	extend: 'Ext.data.Model',
	fields: ['id', 'nombre', 'max_dias_anio', 'dias_corridos']
});