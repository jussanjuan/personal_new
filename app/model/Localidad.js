Ext.define('personal.model.Localidad', {
	extend: 'Ext.data.Model',
	fields: [
		'id', 'nombre']
});