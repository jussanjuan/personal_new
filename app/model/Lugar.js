Ext.define('personal.model.Lugar', {
	extend: 'Ext.data.Model',
	fields: ['nrolugar', 'nombrelugar', {name: 'desde', type: 'date', dateFormat: 'Y-m-d'}]
});