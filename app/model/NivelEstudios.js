Ext.define('personal.model.NivelEstudios', {
	extend: 'Ext.data.Model',
	fields: ['id', 'nombre', 'descripcion']
});