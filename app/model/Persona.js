Ext.define('personal.model.Persona', {
	extend: 'Ext.data.Model',
	fields: [
		'id', 'tipodoc', 'nrodoc', 'cuil',
        {name: 'fecnac', type: 'date', dateFormat: 'Y-m-d'}, 
        'legajo', 'sexo', 'padron', 'apellido', 'nombre', 'domic', 'estaciv', 
        {name: 'fecnac', type: 'date', dateFormat: 'Y-m-d'},
        {
            name: 'fecha_carga', 
            type: 'date', 
            dateFormat: 'Y-m-d',
            convert: function (val,row){
                return val.substring(0,10);//Convierto manualmente el formato dateTime de mysql a date
            }
        },
         'cargoactu', 'lugaractu', 'activa',
        
        {   name: 'titulo', 
            type: 'string'
             
        },
        'telef', 'obs', 'sangre', 'factor', 'nacionalidad',
        {
            name: 'firma_autorizada',
            type: 'string',
            convert: function(val, row) {
                return (val == 'N') ? false : true;
            }
        }, 'antiguedad', 'retencion', 'cautelar', 'mail_personal', 'mail_corporativo', 'usuario_dominio', 
        {
            name: 'fecha_baja',
            type: 'date', 
            dateFormat: 'Y-m-d',
            convert: function (val,row)
            {
                if(val)
                {
                    return val.substring(0,10);//Convierto manualmente el formato dateTime de mysql a date
                }
                else
                {
                    return "";
                }
            }
        }, 'acordada_baja', 'cargo', 'nombrelugar'
	],
        
	
	proxy: {
        type: 'rest',
        url: 'app/proxy.agentes.php',
        params: {
            start: 0,
            limit: AppGlobals.itemsPorPagina
        },
        reader: {
            type: 'json',
            rootProperty: 'results',
            successProperty: 'success'
        }
    }

});