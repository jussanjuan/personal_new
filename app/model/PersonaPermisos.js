Ext.define('RH.model.PersonaPermisos', {
	extend: 'Ext.data.Model',
	fields: ['id', 'nrodoc', 'legajo', 'padron', 'apellido', 'nombre'],
        
	proxy: {
        type: 'rest',
        url: 'app/proxy.agentes.php',
        method: 'GET',
        extraParams: {
            act: 'getAgentesPermisos'
        },
        reader: {
            type: 'json',
            root: 'results',
            successProperty: 'success'
        }
    }

});