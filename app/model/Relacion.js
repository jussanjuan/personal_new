Ext.define('personal.model.Relacion', {
	extend: 'Ext.data.Model',
	fields: ['id', 'nombre', 'observaciones']
});