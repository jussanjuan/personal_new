Ext.define('RH.model.TipoCuenta', {
	extend: 'Ext.data.Model',
	fields: ['id', 'nombre']
});