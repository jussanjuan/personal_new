Ext.define('personal.model.auditoria.Auditoria', {
	extend: 'Ext.data.Model',
	fields: [
		'id', 'sistema', 'legajo', 'legajo_objetivo', 'tabla', 'operacion', 'log',
		{name: 'fecha', type: 'date', dateFormat: 'Y-m-d H:i:s'}, 'nombre', 'nombreEmpleado', 'apellido', 'nombreObjetivo', 'apellidoObjetivo'
    ],

    proxy: {
        type: 'rest',
        url: 'app/proxy.auditoria.php',
        params: {
            start: 0,
            limit: AppGlobals.itemsPorPagina
        },
        reader: {
            type: 'json',
            rootProperty: 'results',
            successProperty: 'success'
        }
    }
});