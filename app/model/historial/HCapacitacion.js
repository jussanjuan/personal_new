Ext.define('personal.model.historial.HCapacitacion', {
	extend: 'Ext.data.Model',
	fields: ['id', 'nombre', 'tipo_capacitacion', {name: 'fecha', type: 'date', dateFormat: 'Y-m-d'}, 'cant_horas', 'modalidad', 'origen', 'inherencia', 'evaluacion', 'nota_evaluacion', 'observaciones'],
	
    proxy: {
        type: 'ajax',
        method: 'GET',
        url: 'app/proxy.capacitaciones.php',
        reader: {
            type: 'json',
            rootProperty: 'results',
            successProperty: 'success'
        }
    }

});