Ext.define('personal.model.historial.HCargos', {
	extend: 'Ext.data.Model',
	fields: ['id', 'nrocargo', 'cargo', 'acuerdo', 
        {name: 'desde', type: 'date', dateFormat: 'Y-m-d'},
        {name: 'hasta', type: 'date', dateFormat: 'Y-m-d', convert: function(val, row) {
                return (val == null)?'Actualidad':val;
            }}, 
        'sitlabo', 'cupo', 'cantidad'],
	
    proxy: {
        type: 'ajax',
        method: 'POST',
        url: 'app/proxy.cargos.php',
        reader: {
            type: 'json',
            rootProperty: 'results',
            successProperty: 'success'
        }
    }

});