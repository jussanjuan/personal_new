Ext.define('RH.model.historial.HCompensatoria', {
	extend: 'Ext.data.Model',
	fields: [
		'id', 'legajo', 
		{name: 'desde', type: 'date', dateFormat: 'Y-m-d' }, 
		{name: 'hasta', type: 'date', dateFormat: 'Y-m-d' },
		'anio', 'total_dias', 'disponibles', 'causa', {name: 'goce', convert: function(val) {
                return (val == 'N') ? 'SI' : 'NO';
            }}
	]
	
});