Ext.define('personal.model.historial.HCompensatoriaDiasFeria', {
	extend: 'Ext.data.Model',
	fields: [
		'id', 'legajo', 
		//'desde', 'hasta',
		{name: 'desde', type: 'date', dateFormat: 'Y-m-d' }, 
		{name: 'hasta', type: 'date', dateFormat: 'Y-m-d' },
		'totaldias', 'disponibles', {name: 'goce', convert: function(val) {
                return (val == 'N') ? 'NO' : 'SI';
            }}
	],

	proxy: {
        type: 'ajax',
        method: 'POST',
        url: 'app/proxy.historial.php',
        reader: {
            type: 'json',
            rootProperty: 'results',
            successProperty: 'success'
        }
    }
	
});