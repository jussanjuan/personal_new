Ext.define('personal.model.historial.HDiasTrabajados', {
	extend: 'Ext.data.Model',
	fields: [
		'id','id_tipo_licencia', 'nombre', 'legajo', {name: 'desde', type: 'date', dateFormat: 'Y-m-d'}, 
        {name: 'hasta', type: 'date', dateFormat: 'Y-m-d'}, {name: 'totaldias', type: 'int'}, {name: 'disponibles', type: 'int'},
		{name: 'goce', convert: function(val) {
                return (val == 'N') ? 'SI' : 'NO';
            }}
    ]
	
});