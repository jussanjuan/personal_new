Ext.define('RH.model.historial.HElectivas', {
	extend: 'Ext.data.Model',
	fields: [
		'legajo', 
        {name: 'desde', type: 'date', dateFormat: 'Y-m-d'},
        {name: 'hasta', type: 'date', dateFormat: 'Y-m-d'},
         'cargo', 'diasotorgad', 'total_dias', {name: 'goce', convert: function(val) {
                return (val == 'N') ? 'SI' : 'NO';
            }}
	],

	//autoLoad: true,
    autoSync: true,

    proxy: {
        type: 'ajax',
        method: 'POST',
        url: 'app/proxy.historial.php',
        /*params: {
            tabla: 'lic_salud',
            legajo: AppGlobals.legAgente
        },*/
        reader: {
            type: 'json',
            root: 'results',
            successProperty: 'success'
        }
    }
	
});