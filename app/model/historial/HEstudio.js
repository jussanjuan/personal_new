Ext.define('personal.model.historial.HEstudio', {
	extend: 'Ext.data.Model',
	fields: [
		'legajo', 'asignatura', 'carrera', 
        {name: 'fecha_examen', type: 'date', dateFormat: 'Y-m-d'},
         'acredito', 
        {name: 'desde', type: 'date', dateFormat: 'Y-m-d'}, 
        {name: 'hasta', type: 'date', dateFormat: 'Y-m-d'},
         'diasotorgad', 'total_dias', {name: 'goce', convert: function(val) {
                return (val == 'N') ? 'SI' : 'NO';
            }}
	],

	//autoLoad: true,
    autoSync: true,

    proxy: {
        type: 'ajax',
        method: 'POST',
        url: 'app/proxy.historial.php',
        /*params: {
            tabla: 'lic_salud',
            legajo: AppGlobals.legAgente
        },*/
        reader: {
            type: 'json',
            rootProperty: 'results',
            successProperty: 'success'
        }
    }
	
});