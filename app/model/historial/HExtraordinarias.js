Ext.define('RH.model.historial.HExtraordinarias', {
	extend: 'Ext.data.Model',
	fields: [
		'legajo', 
        {name: 'desde', type: 'date', dateFormat: 'Y-m-d'}, 
        {name: 'hasta', type: 'date', dateFormat: 'Y-m-d'},
        {name: 'goce', convert: function(val) {
                return (val == 'N') ? 'SI' : 'NO';
            }}, 'observaciones', 'est_ext', 'diasotorgad', 'total_dias'
	],

	//autoLoad: true,
    autoSync: true,

    proxy: {
        type: 'ajax',
        method: 'POST',
        url: 'app/proxy.historial.php',
        /*params: {
            tabla: 'lic_salud',
            legajo: AppGlobals.legAgente
        },*/
        reader: {
            type: 'json',
            root: 'results',
            successProperty: 'success'
        }
    }
	
});