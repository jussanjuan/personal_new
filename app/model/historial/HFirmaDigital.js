Ext.define('personal.model.historial.HFirmaDigital', {
	extend: 'Ext.data.Model',
	fields: ['id', 'legajo', 'tipoFirma', 'descripcion_firma', {name: 'fecha_alta', type: 'date', dateFormat: 'Y-m-d H:i:s'}, {name: 'fecha_revocacion', type: 'date', dateFormat: 'Y-m-d H:i:s'}, 'tipo']
});