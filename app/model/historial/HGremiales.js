Ext.define('RH.model.historial.HGremiales', {
	extend: 'Ext.data.Model',
	fields: [
		'legajo', 
        {name: 'desde', type: 'date', dateFormat: 'Y-m-d'},
        {name: 'hasta', type: 'date', dateFormat: 'Y-m-d'},
        'causa', 'car_grem', 'acredito', 'diasotorgad', 'total_dias', {name: 'goce', convert: function(val) {
                return (val == 'N') ? 'SI' : 'NO';
            }}
	],

	//autoLoad: true,
    autoSync: true,

    proxy: {
        type: 'ajax',
        method: 'POST',
        url: 'app/proxy.historial.php',
        reader: {
            type: 'json',
            root: 'results',
            successProperty: 'success'
        }
    }
	
});