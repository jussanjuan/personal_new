Ext.define('personal.model.historial.HLicencia', {
	extend: 'Ext.data.Model',
	fields: [
		'id_tipo_licencia', 'legajo', {name: 'desde', type: 'date', dateFormat: 'Y-m-d'}, {name: 'hasta', type: 'date', dateFormat: 'Y-m-d'}, 'dias_licencia', 'suma_anio', 'causa', 
		{name: 'goce', convert: function(val) {
                return (val == 'N') ? 'SI' : 'NO';
            }}, {name: 'observaciones', convert: function(val) {
                return (val) ? val : '';
            }},{name: 'pierde_presentismo', convert: function(val) {
                return (val == 0) ? 'NO' : 'SI';
            }}, 'expediente' , 'total_dias', {name: 'fpp', type: 'date', dateFormat: 'Y-m-d'}, 'certnac', {name: 'fp', type: 'date', dateFormat: 'Y-m-d'}, 
            {name: 'lactahasta', type: 'date', dateFormat: 'Y-m-d'}, 'lactahorario', 'asignatura', 'carrera', 
        {name: 'fecha_examen', type: 'date', dateFormat: 'Y-m-d'},
         'acredito','nombre', {name: 'notificado', convert: function(val) {
                return (val) ? val : 'NO';
            }}
    ]
	
});