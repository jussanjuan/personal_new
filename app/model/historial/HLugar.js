Ext.define('personal.model.historial.HLugar', {
	extend: 'Ext.data.Model',
	fields: ['id', 'nrolugar', 'nombrelugar', {name: 'desde', type: 'date', dateFormat: 'Y-m-d'}, 
    {name: 'hasta', type: 'date', dateFormat: 'Y-m-d', convert: function(val, row) {
                return (val == null)?'Actualidad':val;
            }}],
	
    proxy: {
        type: 'ajax',
        method: 'POST',
        url: 'app/proxy.lugar.php',
        reader: {
            type: 'json',
            rootProperty: 'results',
            successProperty: 'success'
        }
    }

});