Ext.define('RH.model.historial.HNacimiento', {
	extend: 'Ext.data.Model',
	fields: [
		'legajo', 
        {name: 'desde', type: 'date', dateFormat: 'Y-m-d'}, 
        {name: 'hasta', type: 'date', dateFormat: 'Y-m-d'}, 
        'observaciones', 'acredito', 'diasotorgad', {name: 'goce', convert: function(val) {
                return (val == 'N') ? 'SI' : 'NO';
            }}, 'total_dias'
	],

	//autoLoad: true,
    autoSync: true,

    proxy: {
        type: 'ajax',
        method: 'POST',
        url: 'app/proxy.historial.php',
        
        reader: {
            type: 'json',
            root: 'results',
            successProperty: 'success'
        }
    }
	
});