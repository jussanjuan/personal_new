Ext.define('RH.model.historial.HSaludPersonal', {
	extend: 'Ext.data.Model',
	fields: [
		'legajo', 'diaspedidos', 'diasotorgad', 
		{name: 'desde', type: 'date', dateFormat: 'Y-m-d'}, 
		{name: 'hasta', type: 'date', dateFormat: 'Y-m-d'}, 
		'causa', {name: 'goce', convert: function(val) {
                return (val == 'N') ? 'SI' : 'NO';
            }}, 'total_dias'
	]
	
});