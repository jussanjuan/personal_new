Ext.define('personal.model.historial.HSancion', {
	extend: 'Ext.data.Model',
	fields: ['id', 'nombre',  
        {name: 'desde', type: 'date', dateFormat: 'Y-m-d'},
        {name: 'hasta', type: 'date', dateFormat: 'Y-m-d'}, 
        'dispuesta_por', 'expediente', 'observaciones']
	
    

});