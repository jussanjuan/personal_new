Ext.define('personal.model.historial.HSolicitudLicencia', {
	extend: 'Ext.data.Model',
	fields: [
		'id_tipo_licencia', 'legajo', {name: 'desde', type: 'date', dateFormat: 'Y-m-d'}, {name: 'hasta', type: 'date', dateFormat: 'Y-m-d'}, 'dias_licencia', 'suma_anio', 'causa', 
		{name: 'observaciones', convert: function(val) {
                return (val) ? val : '';
            }},{name: 'pierde_presentismo', convert: function(val) {
                return (val == 0) ? 'NO' : 'SI';
            }}, 'expediente' , 'total_dias', {name: 'fpp', type: 'date', dateFormat: 'Y-m-d'}, 'certnac', {name: 'fp', type: 'date', dateFormat: 'Y-m-d'}, 
            {name: 'lactahasta', type: 'date', dateFormat: 'Y-m-d'}, 'lactahorario', 'asignatura', 'carrera', 
        {name: 'fecha_examen', type: 'date', dateFormat: 'Y-m-d'},
         'acredito','nombre', {name: 'hora_solicitud', type: 'date', dateFormat: 'Y-m-d H:i:s'}, 'nombre_solicitante', 'legajo_receptor', 'nombre_receptor', 'direccion', 'estado', 'nombre_estado', 'dias_corridos'
    ]
	
});