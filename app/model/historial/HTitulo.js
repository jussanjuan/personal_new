Ext.define('personal.model.historial.HTitulo', {
	extend: 'Ext.data.Model',
	fields: ['id', 'titulo', 'establec', {name: 'fecha_titulo', type: 'date', dateFormat: 'Y-m-d'}],
	
    proxy: {
        type: 'ajax',
        method: 'POST',
        url: 'app/proxy.titulo.php',
        reader: {
            type: 'json',
            rootProperty: 'results',
            successProperty: 'success'
        }
    }

});