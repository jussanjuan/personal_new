Ext.define('personal.model.historial.HistoriaClinica', {
	extend: 'Ext.data.Model',
	fields: [
		'id', 'legajo', {name: 'fecha_hora', type: 'date', dateFormat: 'Y-m-d H:i:s'}, 'observaciones', 'certificado'
    ]
	
});