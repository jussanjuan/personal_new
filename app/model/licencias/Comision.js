Ext.define('personal.model.licencias.Comision', {
	extend: 'Ext.data.Model',
	fields: [
		'total_dias', 
		{name: 'desde', type: 'date', dateFormat: 'Y-m-d'}, 
		{name: 'hasta', type: 'date', dateFormat: 'Y-m-d'}, 
		'causa', {name: 'goce', convert: function(val) {
                return (val == 'N') ? 'SI' : 'NO';
            }}, 'observaciones', 'expediente'
    ]
});