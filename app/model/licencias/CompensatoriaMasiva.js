Ext.define('RH.model.licencias.CompensatoriaMasiva', {
	extend: 'Ext.data.Model',
	fields: [
             'id', 'legajo', 'apellido_nombre', {name: 'desde', type: 'date', dateFormat: 'Y-m-d'}, {name: 'hasta', type: 'date', dateFormat: 'Y-m-d'}, 'totaldias'
	]/*,
        
	proxy: {
        type: 'rest',
        method: 'POST',
        url: 'app/proxy.licencias.php',
        extraParams: {
            tarea: 'getLicenciasMasivasCompensatorias'
        },
        reader: {
            type: 'json',
            root: 'results',
            successProperty: 'success'
            }
        }*/

});