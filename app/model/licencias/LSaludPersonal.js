Ext.define('RH.model.licencias.LSaludPersonal', {
	extend: 'Ext.data.Model',
	fields: [
		'legajo', 'diaspedidos', 'diasotorgad', 'desde', 'hasta', 'causa', 'gocce', 'total_dias'
	]
	
});