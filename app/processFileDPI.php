<?php 
session_set_cookie_params(0);
session_start();
require('system.config.php');
require('proxy.feriados.php');

$tarea = $_SERVER['REQUEST_METHOD'];
switch ($tarea) 
{
    case 'GET':
        break;
    case 'POST':
        $act = isset($_POST["act"])?$_POST["act"]:"";
        switch($act)
        {
            case "uploadFile": 
                uploadFile();
                break;
            case "uploadFileUSU": 
                uploadFileUSU();
                break;    
            case "processFile": 
                processFile();
                break;
            case "processFileUSU": 
                processFileUSU();
                break;    
            case "generateFileUSU":
                generateFileUSU();
                break;   
        }
        break;
    case 'PUT':
        break;
    case 'DELETE':
        break;
    default:
        echo "({ failure: 'Error por default'})";
        break;
}

function uploadFile()
{
    $data = array();
    $file = $_FILES['archivoDPI'];
    $fecha = $_POST["fecha_acreditacion"];
    $liquidacion = $_POST["liquidacion"];
    $periodo = $_POST["periodo"];
    
    if($file['name'] != "")
    {
        //Carpeta de subida de los archivos
        $dir_subida = '../res/archivos/';
        if(checkFile($file))
        {
            $archivo_subido = $dir_subida.basename($file["name"]);
            if (move_uploaded_file($file["tmp_name"], $archivo_subido))
            {
                $data["success"] = true;
                $data["archivo"] = $archivo_subido;
                $data["fecha"] = $fecha;
                $data["liquidacion"] = $liquidacion;
                $data["periodo"] = $periodo;
                if(chmod($archivo_subido, 0755))
                {
                    $data["msg"] = "Archivo subido correctamente, permisos asignados correctamente";
                }
                else
                {
                    $data["msg"] = "Archivo subido correctamente, pero no se pudieron asignar permisos";
                }
            } 
            else 
            {
                $data["success"] = false;
                $data["error"] = "Error al subir el archivo";
            }
        }//end if(file (validaciones))
        else
        {
            $data["success"] = false;
            $data["error"] = "Archivo no v&aacute;lido. Subir solo archivos enviados por la DPI";
        }
    }//end if($_FILES['archivo']['name'][0] != "")
    else
    {
        $data["success"] = false;
        $data["error"] = "No se subi&oacute; ning&uacute;n archivo";
    }
    echo json_encode($data);
}

function uploadFileUSU()
{
    $data = array();
    $file = $_FILES['archivoDPIUsu'];
    $fecha = $_POST["fecha_acreditacion_usu"];
    $liquidacion = $_POST["liquidacion_usu"];
    $periodo = $_POST["periodo_usu"];
    
    if($file['name'] != "")
    {
        //Carpeta de subida de los archivos
        $dir_subida = '../res/archivos/';
        if(checkFile($file))
        {
            $archivo_subido = $dir_subida.basename($file["name"]);
            if (move_uploaded_file($file["tmp_name"], $archivo_subido))
            {
                $data["success"] = true;
                $data["archivo"] = $archivo_subido;
                $data["fecha"] = $fecha;
                $data["liquidacion"] = $liquidacion;
                $data["periodo"] = $periodo;
                if(chmod($archivo_subido, 0755))
                {
                    $data["msg"] = "Archivo subido correctamente, permisos asignados correctamente";
                }
                else
                {
                    $data["msg"] = "Archivo subido correctamente, pero no se pudieron asignar permisos";
                }
            } 
            else 
            {
                $data["success"] = false;
                $data["error"] = "Error al subir el archivo";
            }
        }//end if(file (validaciones))
        else
        {
            $data["success"] = false;
            $data["error"] = "Archivo no v&aacute;lido. Subir solo archivos enviados por la DPI";
        }
    }//end if($_FILES['archivo']['name'][0] != "")
    else
    {
        $data["success"] = false;
        $data["error"] = "No se subi&oacute; ning&uacute;n archivo";
    }
    echo json_encode($data);
}

function processFile()
{
    global $conn;
    $data = array();

    $archivo = $_POST["archivo"];
    $fecha = new DateTime($_POST["fecha"]);
    $liquidacion = $_POST["liquidacion"];
    $periodo = $_POST["periodo"];
    $fecha_ac = $fecha->format("dmy");
    $dir_guardar = '../res/archivos/';
    
    $nombre_archivo_export = "PJSUELDO_".$fecha_ac.".txt";
    $fp = fopen($archivo, "r");
    $file_exp = $dir_guardar.$nombre_archivo_export;
    $fp_out = fopen($file_exp,"wb");
    $errores_nombres = "";
    $cant_errores_nombre = 0;

    //$res_trunc = $conn->query($trunc);
    $neto_total = 0;
    while(!feof($fp)) 
    {
        $linea = fgets($fp);

        if($linea)//mientras la linea no sea vacia, hago todo
        {
            $caracter_inicial = substr($linea, 0, 1);
            $cod_concepto = substr($linea, 1, 2); //haberes, cta alimentaria, depositos judiciales, etc
            $cod_concepto_2 = substr($linea, 3, 3); //sueldos, embargos y cuota alim., viaticos
            
            $sueldo_neto = substr($linea, 23, 15);
            $nombre = substr($linea, 38, 30);
            $dni = substr($linea, 69, 8);
            $sucursal_pj = "009";
            $tipo_cuenta_pj = "1";
            $nro_cuenta_pj = "00030003";
            
            //Sueldo en valor float
            $sueldo_float = floatval($sueldo_neto)/100;
            $neto_total+=$sueldo_float;
            $str_leg = "SELECT legajo FROM general WHERE nrodoc='".$dni."'";

            $res_leg = $conn->query($str_leg);
            if($res_leg->num_rows)
            {
                //echo $str_leg;
                $obj_leg = $res_leg->fetch_object();
                $leg_cuenta = $obj_leg->legajo;
                $str_cue = "SELECT tipo_cuenta, numero, cuenta FROM cont_cuenta, cont_sucursal WHERE cont_cuenta.id_sucursal=cont_sucursal.id AND cont_cuenta.legajo=".$leg_cuenta;
                $res_cue = $conn->query($str_cue);
                if($res_cue->num_rows)
                {
                    $obj_cue = $res_cue->fetch_object();
                    $tipo_cuenta = $obj_cue->tipo_cuenta;
                    $sucursal = $obj_cue->numero;
                    $nro_cuenta = $obj_cue->cuenta;

                    //Relleno con ceros si el dni tiene menos de 9 digitos
                    $dni = str_pad($dni,9,"0",STR_PAD_LEFT);
    
                    //Voy generando cada linea del archivo nuevo
                    $new_line= $caracter_inicial.$cod_concepto.$cod_concepto_2.$fecha_ac.$sucursal.$tipo_cuenta.$nro_cuenta.$sueldo_neto.$nombre.$dni.$sucursal_pj.$tipo_cuenta_pj.$nro_cuenta_pj;
                    
                    //Controlo que no haya mas de una liquidacion igual en el mismo mes y mismo año. Si es asi, actualizo el registro en lugar de insertar uno nuevo
                    $cons_liq = "SELECT * FROM cont_archivo_bsj WHERE tipo_acreditacion='".$cod_concepto."' AND codigo_empresa='".$cod_concepto_2."' AND fecha_acreditacion='".$fecha_ac."' AND legajo=".$leg_cuenta." AND liquidacion=".$liquidacion." AND periodo='".$periodo."'";
                    $res_liq = $conn->query($cons_liq);
                    if($res_liq->num_rows>0)
                    {
                        $upd_reg_bd = "UPDATE cont_archivo_bsj SET sucursal_acreditacion='".$sucursal."', tipo_cuenta_acred='".$tipo_cuenta."', cuenta_acreditacion='".$nro_cuenta."', neto='".$sueldo_neto."', nombre='".$nombre."', documento='".$dni."', sucursal_debito='".$sucursal_pj."', tipo_cuenta_deb='".$tipo_cuenta_pj."', cuenta_deb='".$nro_cuenta_pj."', registro='".$new_line."' WHERE tipo_acreditacion='".$cod_concepto."' AND codigo_empresa='".$cod_concepto_2."' AND fecha_acreditacion='".$fecha_ac."' AND legajo=".$leg_cuenta." AND liquidacion=".$liquidacion." AND periodo='".$periodo."'";
                        $res_upd_bd = $conn->query($upd_reg_bd);
                    }
                    else
                    {
                        //Inserto en una tabla para luego recuperar los registros en orden alfabetico
                        $ins_reg_bd = "INSERT INTO cont_archivo_bsj (codigo, tipo_acreditacion, codigo_empresa, fecha_acreditacion, sucursal_acreditacion, tipo_cuenta_acred, cuenta_acreditacion, neto, nombre, documento, sucursal_debito, tipo_cuenta_deb,cuenta_deb, registro, legajo, liquidacion, periodo, fecha) VALUES ('".$caracter_inicial."', '".$cod_concepto."', '".$cod_concepto_2."', '".$fecha_ac."', '".$sucursal."', '".$tipo_cuenta."', '".$nro_cuenta."', '".$sueldo_neto."', '".$nombre."', '".$dni."', '".$sucursal_pj."', '".$tipo_cuenta_pj."', '".$nro_cuenta_pj."', '".$new_line."', ".$leg_cuenta.",".$liquidacion.",'".$periodo."', '".$fecha->format("Y-m-d")."')";
                        $res_reg_bd = $conn->query($ins_reg_bd);
                    }
                    
                }
                else
                {
                    $errores_nombres.= $nombre." | ";
                    $cant_errores_nombre++; 
                }
            }
            else
            {
                //echo $str_leg;
                $errores_nombres.= $nombre." | ";
                $cant_errores_nombre++;
            }
        }
    }
    $cons_sel_reg_bd = "SELECT * FROM cont_archivo_bsj WHERE tipo_acreditacion='".$cod_concepto."' AND codigo_empresa='".$cod_concepto_2."' AND fecha_acreditacion='".$fecha_ac."' AND liquidacion=".$liquidacion." AND periodo='".$periodo."' ORDER BY nombre ASC";
    $res_sel_reg_bd = $conn->query($cons_sel_reg_bd);
    $cant_reg = 0;
    while($obj_sel_reg = $res_sel_reg_bd->fetch_object())
    {
        $cant_reg++;
        $new_line = $obj_sel_reg->registro;

        //Relleno la linea con espacios en blanco para llegar a los 128 caracteres que pide la especificacion del BSJ
        $new_line = str_pad($new_line, 128);

        //Inserto salto de linea
        $new_line.=PHP_EOL;

        //Escribo linea
        fwrite($fp_out,$new_line);
    }

    fclose($fp_out);
    $data["success"] = true;
    $data["msg"] = "Archivo creado correctamente";
    $data["archivo"] = $file_exp;
    $data["nombre"] = $nombre_archivo_export;
    $data["errores"] = $errores_nombres;
    $data["cant_errores"] = $cant_errores_nombre;
    $data["neto_total"] = $neto_total;
    $data["cant_reg"] = $cant_reg;
    
    echo json_encode($data);

    //Cabeceras para descargar el archivo desde servidor. No usado por ahora
    /*
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="'.basename($file_exp).'"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file_exp));
    readfile($file_exp);
    exit;*/
}

function processFileUSU()
{

    global $conn;
    $data = array();

    $archivo = $_POST["archivo"];
    $fecha = new DateTime($_POST["fecha"]);
    $liquidacion = $_POST["liquidacion"];
    $periodo = $_POST["periodo"];
    $fecha_ac = $fecha->format("dmy");
    $dir_guardar = '../res/archivos/';

    $nombre_archivo_export = "PJUSURA_".$fecha_ac.".txt";
    $fp = fopen($archivo, "r");
    $file_exp = $dir_guardar.$nombre_archivo_export;
    $fp_out = fopen($file_exp,"wb");
    $errores_nombres = "";
    $cant_errores_nombre = 0;

    $neto_total = 0;
    $i=0;

    $array_leg = array();
    $array_mont = array();
    while(!feof($fp)) 
    {
        $i++;
        $linea = fgets($fp);

        if($linea)//mientras la linea no sea vacia, hago todo
        {
            $cod_liq = trim(substr($linea, 44, 4));
            $padron = trim(substr($linea, 21, 11));
            //Relleno con ceros si el padron tiene menos de 7 digitos
            $padron = str_pad(trim($padron),7,"0",STR_PAD_LEFT);
            $padron_1 = substr($padron,0,6)."-".substr($padron,-1);
            $padron_2 = substr($padron,0,6)."/".substr($padron,-1);
            $monto_leg = 0;
            if($cod_liq=="J98" || $cod_liq=="J99")
            {
                $monto = trim(substr($linea, 49, 13));
                $monto = toFloat($monto);
                //echo $padron_1." ".$monto;
                //echo "<br/>";
                $str_leg = "SELECT legajo FROM general WHERE padron='".$padron_1."' or padron='".$padron_2."'";
                $res_leg = $conn->query($str_leg);
                if($res_leg->num_rows>0)
                {
                    $obj_leg = $res_leg->fetch_object();
                    $leg = $obj_leg->legajo;
                    if(!in_array($leg, $array_leg))
                    {
                        array_push($array_leg, $leg);
                        $pos = array_search($leg, $array_leg);
                        $array_mont[$pos] = $monto;
                    }
                    else
                    {
                        $pos = array_search($leg, $array_leg);
                        $array_mont[$pos]+= $monto;
                    }
                }
                else
                {
                    $errores_nombres.=  "Padron: ".$padron_1." - Error: no se encontr&oacute; un empleado con dicho padr&oacute;n en el sistema";
                    $cant_errores_nombre++;
                }
            }
        }
    }
    for($i=0;$i<count($array_leg);$i++)
    {
        $str_cons = "SELECT count(*) as cant_benef, importe FROM cont_beneficiarios_usuras WHERE legajo_benefactor=".$array_leg[$i];
        $res_cons = $conn->query($str_cons);
        $obj_cons = $res_cons->fetch_object();
        if($obj_cons->cant_benef==0)
        {
            $errores_nombres.=  "Legajo: ".$array_leg[$i]." - Error: No hay cargados beneficiarios para el legajo";
        }
        else
        {
            $monto_div = $array_mont[$i]/$obj_cons->cant_benef;
            if($obj_cons->importe==0 || $obj_cons->cant_benef==1)
            {
                $str_upd = "UPDATE cont_beneficiarios_usuras SET importe=".$monto_div." WHERE legajo_benefactor=".$array_leg[$i];
                $res_upd = $conn->query($str_upd);
                if(!$res_upd)
                {
                    $errores_nombres.=  "Legajo: ".$array_leg[$i]." - Error: ".$conn->error;
                    $cant_errores_nombre++;
                    $data["success"] = false;
                }
                else
                {
                    $data["success"] = true;
                    $data["msg"] = "Archivo procesado correctamente. Revisar los montos en la lista de la derecha antes de descargar el archivo.";
                }
            }
        }
    }
    $data["errores"] = $errores_nombres;
    $data["cant_errores"] = $cant_errores_nombre;
    echo json_encode($data);       
}

function generateFileUSU()
{
    global $conn;
    $data = array();
    
    $fecha = new DateTime($_POST["fecha"]);
    $liquidacion = $_POST["liquidacion"];
    $periodo = $_POST["periodo"];
    $fecha_ac = $fecha->format("dmy");
    $dir_guardar = '../res/archivos/';

    $nombre_archivo_export = "PJUSURA_".$fecha_ac.".txt";
    
    $file_exp = $dir_guardar.$nombre_archivo_export;
    $fp_out = fopen($file_exp,"wb");
    //Genero todos los caracteres para formar la linea
    $caracter_inicial = "A";
    $cod_concepto = "11"; //cta alimentaria, depositos judiciales, etc
    $cod_concepto_2 = "273"; //sueldos, embargos y cuota alim., viaticos
    $sucursal_pj = "009";
    $tipo_cuenta_pj = "1";
    $nro_cuenta_pj = "00030003";
    
    $str_ben = "SELECT cont_beneficiarios_usuras.*, cont_sucursal.numero FROM cont_beneficiarios_usuras, cont_sucursal WHERE cont_beneficiarios_usuras.sucursal_acred=cont_sucursal.id";
    $res_ben = $conn->query($str_ben);
    while($obj = $res_ben->fetch_object())
    {
        //Relleno con ceros si el dni tiene menos de 9 digitos
        $dni = str_pad($obj->dni,9,"0",STR_PAD_LEFT);
        
        $neto = $obj->importe*100;
        //Relleno con 0 el importe
        $sueldo_neto = str_pad($neto,15,"0",STR_PAD_LEFT);

        //Relleno con espacios el nombre
        $nombre = str_pad($obj->apellido.",".$obj->nombre,30);

        $sucursal = $obj->numero;
        $tipo_cuenta = $obj->tipo_cuenta;
        $nro_cuenta = $obj->cuenta_acred;
        //Voy generando cada linea del archivo nuevo
        $new_line= $caracter_inicial.$cod_concepto.$cod_concepto_2.$fecha_ac.$sucursal.$tipo_cuenta.$nro_cuenta.$sueldo_neto.$nombre.$dni.$sucursal_pj.$tipo_cuenta_pj.$nro_cuenta_pj;

        //Controlo que no haya mas de una liquidacion igual en el mismo mes y mismo año. Si es asi, actualizo el registro en lugar de insertar uno nuevo
        $cons_liq = "SELECT * FROM cont_archivo_bsj WHERE tipo_acreditacion='".$cod_concepto."' AND codigo_empresa='".$cod_concepto_2."' AND fecha_acreditacion='".$fecha_ac."' AND documento='".$dni."' AND liquidacion=".$liquidacion." AND periodo='".$periodo."'";
        //echo $cons_liq;
        $res_liq = $conn->query($cons_liq);
        if($res_liq->num_rows>0)
        {
            $upd_reg_bd = "UPDATE cont_archivo_bsj SET sucursal_acreditacion='".$sucursal."', tipo_cuenta_acred='".$tipo_cuenta."', cuenta_acreditacion='".$nro_cuenta."', neto='".$sueldo_neto."', nombre='".$nombre."', documento='".$dni."', sucursal_debito='".$sucursal_pj."', tipo_cuenta_deb='".$tipo_cuenta_pj."', cuenta_deb='".$nro_cuenta_pj."', registro='".$new_line."' WHERE tipo_acreditacion='".$cod_concepto."' AND codigo_empresa='".$cod_concepto_2."' AND fecha_acreditacion='".$fecha_ac."' AND documento='".$dni."' AND liquidacion=".$liquidacion." AND periodo='".$periodo."'";
            $res_upd_bd = $conn->query($upd_reg_bd);
        }
        else
        {
            //Inserto en una tabla para luego recuperar los registros en orden alfabetico
            $ins_reg_bd = "INSERT INTO cont_archivo_bsj (codigo, tipo_acreditacion, codigo_empresa, fecha_acreditacion, sucursal_acreditacion, tipo_cuenta_acred, cuenta_acreditacion, neto, nombre, documento, sucursal_debito, tipo_cuenta_deb,cuenta_deb, registro, legajo, liquidacion, periodo, fecha) VALUES ('".$caracter_inicial."', '".$cod_concepto."', '".$cod_concepto_2."', '".$fecha_ac."', '".$sucursal."', '".$tipo_cuenta."', '".$nro_cuenta."', '".$sueldo_neto."', '".$nombre."', '".$dni."', '".$sucursal_pj."', '".$tipo_cuenta_pj."', '".$nro_cuenta_pj."', '".$new_line."', 0,".$liquidacion.",'".$periodo."', '".$fecha->format("Y-m-d")."')";
            $res_reg_bd = $conn->query($ins_reg_bd);
        }
    }

    $cons_sel_reg_bd = "SELECT * FROM cont_archivo_bsj WHERE tipo_acreditacion='".$cod_concepto."' AND codigo_empresa='".$cod_concepto_2."' AND fecha_acreditacion='".$fecha_ac."' AND liquidacion=".$liquidacion." AND periodo='".$periodo."' ORDER BY nombre ASC";
    $res_sel_reg_bd = $conn->query($cons_sel_reg_bd);
    $cant_reg = 0;
    while($obj_sel_reg = $res_sel_reg_bd->fetch_object())
    {
        $cant_reg++;
        $new_line = $obj_sel_reg->registro;

        //Relleno la linea con espacios en blanco para llegar a los 128 caracteres que pide la especificacion del BSJ
        $new_line = str_pad($new_line, 128);

        //Inserto salto de linea
        $new_line.=PHP_EOL;

        //Escribo linea
        fwrite($fp_out,$new_line);
    }

    fclose($fp_out);
    $data["success"] = true;
    $data["msg"] = "Archivo creado correctamente";
    $data["archivo"] = $file_exp;
    $data["nombre"] = $nombre_archivo_export;
        
    echo json_encode($data);
}

function checkFile($file)
{
    $ext_permitidas = array("txt");
    $mimes_permitidos = array("text/plain");
    $finfo = finfo_open(FILEINFO_MIME_TYPE);
    $mtype = finfo_file( $finfo, $file["tmp_name"]);
    //echo $mtype;
    if(/*in_array(substr($file["name"], -3), $ext_permitidas) && */in_array($mtype, $mimes_permitidos))
    {
        return true;
    }
    else
    {
        return false;
    }
    finfo_close($finfo);
}

function toFloat($num) {
    $dotPos = strrpos($num, '.');
    $commaPos = strrpos($num, ',');
    $sep = (($dotPos > $commaPos) && $dotPos) ? $dotPos : 
        ((($commaPos > $dotPos) && $commaPos) ? $commaPos : false);
   
    if (!$sep) {
        return floatval(preg_replace("/[^0-9]/", "", $num));
    } 

    return floatval(
        preg_replace("/[^0-9]/", "", substr($num, 0, $sep)) . '.' .
        preg_replace("/[^0-9]/", "", substr($num, $sep+1, strlen($num)))
    );
}
?>