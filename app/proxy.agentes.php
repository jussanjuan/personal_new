<?php
session_set_cookie_params(0);
set_time_limit(0);
session_start();
require('system.config.php');
require('proxy.feriados.php');


$tarea = $_SERVER['REQUEST_METHOD'];
$act = isset($_GET["act"])?$_GET["act"]:"";
if(!isset($_SESSION['loggedin']))
{
    echo "({ failure: 'Se ha vencido la sesi&oacute;n del usuario. Por favor vuelva a iniciar sesi&oacute;n'})";
    exit;
}
switch ($tarea) {
	case 'GET':
        switch($act)
        {
            case "":
            case "activosYBajas":
                getAgentes();
                break;
            case "all":
                getAgentesAll();
                break;
            case "getAgentesPermisos":
                getAgentesPermisos();
                break;
            case "updateSeniority":
                updateSeniority();
                break;
            case "getAgenteByLegajo":
                getAgente();
                break;        
        }
        break;        
	case 'POST'://Modificado mauro
        $act = isset($_POST["act"])?$_POST["act"]:"";
        switch($act)
        {
            case "darBaja":
                bajaAgente();
                break;
            case "setAgente":
            case "editAgente":
                setAgente();
                break;
            case "updateSeniority":
                updateSeniority();
                break;        
            default:
                echo "({ failure: 'Error por default'})";
                break;
        }
        break;
	case 'PUT':
		break;
	case 'DELETE':
		break;
	default:
		echo "({ failure: 'Error por default'})";
		break;
}


function getAgentes() 
{
    global $conn;
    $str_filt_user = "";
    //Permiso = 0 es para ver todos los agentes. 
    //Permiso = 1 ver solo el propio legajo
    //Permiso = 2: ver propio legajo y agentes especificos
    //Permiso = 3: ver propio legajo y area completa
    //Permiso = 4: ver propio legajo fuero completo

    $legajos = ""; //Legajos que el agente esta autorizado a ver
    $str_filt_user = "";
    //var_dump($_SESSION);
    switch ($_SESSION["permiso_vista"]) 
    {
        case '0': //permiso total (Ver todos los agentes)
            $str_filt_user = "";
            break;
        case '1': //permiso individual (permiso para ver solo el propio usuario)
            $legajos.=$_SESSION["legajo"]; //Agrego el legajo del propio usuario
            $str_filt_user = " AND g.legajo IN (".$legajos.")";
            break;
        case '2': //Permisos especificos (ver el propio usuario y ciertos agentes)
            $str_permisos = "SELECT usuarios.legajo, permisos_vista.legajo_vista FROM usuarios, permisos_vista WHERE usuarios.legajo =permisos_vista.permiso_vista AND usuarios.legajo=".$_SESSION["legajo"];
            $res_permisos = $conn->query($str_permisos);

            while($obj = $res_permisos->fetch_object())
            {
                $legajos.=$obj->legajo_vista.",";
            }
            $legajos.=$_SESSION["legajo"]; //Agrego el legajo del propio usuario
            $str_filt_user = " AND g.legajo IN (".$legajos.")";
            break;
        case '3': //Permiso de area (Ver propio usuario y todos los agentes del area a la que pertenece)
            //Obtengo el area del agente
            $str_area = "SELECT lugaractu FROM general WHERE legajo=".$_SESSION["legajo"];
            $res_area = $conn->query($str_area);
            $obj_area = $res_area->fetch_object();
            $area = $obj_area->lugaractu;

            $str_permisos = "SELECT legajo FROM general WHERE lugaractu=".$area;
            $res_permisos = $conn->query($str_permisos);
            while($obj = $res_permisos->fetch_object())
            {
                $legajos.=$obj->legajo.",";
            }
            $legajos.=$_SESSION["legajo"]; //Agrego el legajo del propio usuario
            $str_filt_user = " AND g.legajo IN (".$legajos.")";
            break;
        case '4': //Permiso de fuero (Ver propio usuario y todos los agentes del fuero al que pertenece)
            //Obtengo el fuero del agente
            $str_fuero = "SELECT nro_fuero FROM lugar, general WHERE legajo=".$_SESSION["legajo"]." AND lugaractu=nrolugar";
            $res_fuero = $conn->query($str_fuero);
            $obj_fuero = $res_fuero->fetch_object();
            $fuero = $obj_fuero->nro_fuero;

            $str_permisos = "SELECT legajo FROM general, lugar WHERE lugaractu=nrolugar AND nro_fuero=".$fuero;
            $res_permisos = $conn->query($str_permisos);
            while($obj = $res_permisos->fetch_object())
            {
                $legajos.=$obj->legajo.",";
            }
            $legajos.=$_SESSION["legajo"]; //Agrego el legajo del propio usuario
            $str_filt_user = " AND g.legajo IN (".$legajos.")";
            break;    
    }
    //$activos = (isset($_GET["act"]) && $_GET["act"]=="activosYBajas")?"g.activa=0 or g.activa=1":"g.activa=1";
    $activos = "1=1";
    if (isset($_GET["query"])) //Se ingresaron caracteres para buscar
    {
        $query = trim($_GET["query"]);
        $query = str_replace ("'","",$query);
        $query = str_replace("\"","",$query);
        $query = str_replace("*","",$query);
        $query = str_replace("%","",$query);
        $query = htmlspecialchars($query);

        //Parseo la variable $_GET["fields"], que trae los filtros de busqueda. Mauro
        if($_GET["fields"]!=NULL) //Si se selecciono al menos un filtro de busqueda
        {
            $cad = str_replace("[", "", $_GET["fields"]);
            $cad = str_replace("]", "", $cad);
            $cad = str_replace("\"", "", $cad);
            $cad = explode(",", $cad);
            $str_filt = "";
            for($i=0;$i<count($cad);$i++)
            {
                $str_filt.= "g.".$cad[$i]." LIKE '%".$query."%'";
                if($i!=(count($cad)-1))
                    $str_filt.=" OR ";
            }
            $str = "SELECT g.*, u.usuario_dominio, b.fecha as fecha_baja, b.acordada as acordada_baja, c.cargo, l.nombrelugar FROM general g LEFT JOIN usuarios u ON g.legajo=u.legajo LEFT JOIN bajas b ON g.legajo=b.legajo LEFT JOIN cargos c ON g.cargoactu=c.nrocargo LEFT JOIN lugar l ON g.lugaractu=l.nrolugar WHERE (".$activos.") AND (".$str_filt.")".$str_filt_user." ORDER BY apellido ASC";
            
        }
        else //Si se destiladaron todos los filtros de busqueda, busco todo, para evitar errores
        {
            $str = "SELECT g.*, u.usuario_dominio, b.fecha as fecha_baja, b.acordada as acordada_baja, c.cargo, l.nombrelugar FROM general g LEFT JOIN usuarios u ON g.legajo=u.legajo LEFT JOIN bajas b ON g.legajo=b.legajo LEFT JOIN cargos c ON g.cargoactu=c.nrocargo LEFT JOIN lugar l ON g.lugaractu=l.nrolugar WHERE (".$activos.")".$str_filt_user." ORDER BY apellido ASC";
        }
        
        $result = $conn->query($str);
        $nbrows = $result->num_rows;
    } 
    else //Busca todo
    {
        $str = "SELECT g.*, u.usuario_dominio, b.fecha as fecha_baja, b.acordada as acordada_baja, c.cargo, l.nombrelugar FROM general g LEFT JOIN usuarios u ON g.legajo=u.legajo LEFT JOIN bajas b ON g.legajo=b.legajo LEFT JOIN cargos c ON g.cargoactu=c.nrocargo LEFT JOIN lugar l ON g.lugaractu=l.nrolugar WHERE (".$activos.")".$str_filt_user." ORDER BY apellido ASC ";
        //echo $str;
        $result = $conn->query($str);
        $nbrows = $result->num_rows;
    } 
    //echo $str;
    $st = (integer) (isset($_GET['start']) ? $_GET['start'] :"");
    $end = (integer) (isset($_GET['limit']) ? $_GET['limit'] : "");
    if($st=="" || $end=="")
    {
        $limite=$str."";
    }
    else
    {
        $limite = $str." LIMIT ".$st.",".$end;  
    }
    //echo $limite;
    $result = $conn->query($limite);
    //echo $limite;
    $arr = array();
    while ($obj = $result->fetch_object()){
            $obj->domic=htmlspecialchars_decode($obj->domic, ENT_QUOTES);//Mauro
            $arr[] = $obj;
    }
    //var_dump($arr);
    echo "({ total: ".$nbrows.",  results: " . json_encode($arr) . "})";
        
}

function getAgentesAll() 
{
    global $conn;
    $act = isset($_GET["activos"])?"activa=1":"1=1";
    if (isset($_GET["query"])) //Se ingresaron caracteres para buscar
    {
        $query = trim($_GET["query"]);
        $query = str_replace ("'","",$query);
        $query = str_replace("\"","",$query);
        $query = str_replace("*","",$query);
        $query = str_replace("%","",$query);
        $query = htmlspecialchars($query);
        $str = "SELECT * FROM general WHERE ".$act." AND apellido LIKE '%".$query."%' OR nombre LIKE '%".$query."%' OR legajo LIKE '%".$query."%' OR nrodoc LIKE '%".$query."%' ORDER BY apellido ASC";
    }
    else
    {
        $str = "SELECT * FROM general WHERE ".$act." ORDER BY apellido ASC ";
    }
    //echo $str;
    $result = $conn->query($str);
    $nbrows = $result->num_rows;
    
    $arr = array();
    while ($obj = $result->fetch_object()){
            $obj->apNomLeg= $obj->apellido.", ".$obj->nombre." - Leg. ".$obj->legajo;
            $arr[] = $obj;
    }

    echo "({ total: ".$nbrows.",  results: " . json_encode($arr) . "})";
        
}

function getAgentesPermisos() 
{
    global $conn;
    $str_filt_user = "";
    //Permiso = 0 es para ver todos los agentes. 
    //Permiso = 1 ver solo el propio legajo
    //Permiso = 2: ver propio legajo y agentes especificos
    //Permiso = 3: ver propio legajo y area completa
    //Permiso = 4: ver propio legajo fuero completo

    $legajos = ""; //Legajos que el agente esta autorizado a ver
    switch ($_SESSION["permiso_vista"]) 
    {
        case '0': //permiso total (Ver todos los agentes)
            $str_filt_user = "";
            break;
        case '1': //permiso individual (permiso para ver solo el propio usuario)
            $legajos.=$_SESSION["legajo"]; //Agrego el legajo del propio usuario
            $str_filt_user = " AND legajo IN (".$legajos.")";
            break;
        case '2': //Permisos especificos (ver el propio usuario y ciertos agentes)
            $str_permisos = "SELECT usuarios.legajo, permisos_vista.legajo_vista FROM usuarios, permisos_vista WHERE usuarios.legajo =permisos_vista.permiso_vista AND usuarios.legajo=".$_SESSION["legajo"];
            $res_permisos = $conn->query($str_permisos);

            while($obj = $res_permisos->fetch_object())
            {
                $legajos.=$obj->legajo_vista.",";
            }
            $legajos.=$_SESSION["legajo"]; //Agrego el legajo del propio usuario
            $str_filt_user = " AND legajo IN (".$legajos.")";
            break;
        case '3': //Permiso de area (Ver propio usuario y todos los agentes del area a la que pertenece)
            //Obtengo el area del agente
            $str_area = "SELECT lugaractu FROM general WHERE legajo=".$_SESSION["legajo"];
            $res_area = $conn->query($str_area);
            $obj_area = $res_area->fetch_object();
            $area = $obj_area->lugaractu;

            $str_permisos = "SELECT legajo FROM general WHERE lugaractu=".$area;
            $res_permisos = $conn->query($str_permisos);
            while($obj = $res_permisos->fetch_object())
            {
                $legajos.=$obj->legajo.",";
            }
            $legajos.=$_SESSION["legajo"]; //Agrego el legajo del propio usuario
            $str_filt_user = " AND legajo IN (".$legajos.")";
            break;
        case '4': //Permiso de fuero (Ver propio usuario y todos los agentes del fuero al que pertenece)
            //Obtengo el fuero del agente
            $str_fuero = "SELECT nro_fuero FROM lugar, general WHERE legajo=".$_SESSION["legajo"]." AND lugaractu=nrolugar";
            $res_fuero = $conn->query($str_fuero);
            $obj_fuero = $res_fuero->fetch_object();
            $fuero = $obj_fuero->nro_fuero;

            $str_permisos = "SELECT legajo FROM general, lugar WHERE lugaractu=nrolugar AND nro_fuero=".$fuero;
            $res_permisos = $conn->query($str_permisos);
            while($obj = $res_permisos->fetch_object())
            {
                $legajos.=$obj->legajo.",";
            }
            $legajos.=$_SESSION["legajo"]; //Agrego el legajo del propio usuario
            $str_filt_user = " AND legajo IN (".$legajos.")";
            break;    
    }
    
    if (isset($_GET["query"])) //Se ingresaron caracteres para buscar
    {
        //Parseo la variable $_GET["fields"], que trae los filtros de busqueda. Mauro
        if($_GET["fields"]!=NULL) //Si se selecciono al menos un filtro de busqueda
        {
            $cad = str_replace("[", "", $_GET["fields"]);
            $cad = str_replace("]", "", $cad);
            $cad = str_replace("\"", "", $cad);
            $cad = explode(",", $cad);
            $str_filt = "";
            for($i=0;$i<count($cad);$i++)
            {
                $str_filt.= $cad[$i]." LIKE '%".$_GET["query"]."%'";
                if($i!=(count($cad)-1))
                    $str_filt.=" OR ";
            }
            $str = "SELECT * FROM general WHERE activa=1 AND (".$str_filt.")".$str_filt_user." ORDER BY apellido ASC";    
        }
        else //Si se destiladaron todos los filtros de busqueda, busco todo, para evitar errores
        {
            $str = "SELECT * FROM general WHERE activa=1".$str_filt_user." ORDER BY apellido ASC";
        }
        
        $result = $conn->query($str);
        $nbrows = $result->num_rows;
    } 
    else //Busca todo
    {
        $str = "SELECT * FROM general WHERE activa=1".$str_filt_user." ORDER BY apellido ASC ";
        $result = $conn->query($str);
        $nbrows = $result->num_rows;
    } 
    
    $result = $conn->query($str);
    
    $arr = array();
    while ($obj = $result->fetch_object()){
            $obj->domic=htmlspecialchars_decode($obj->domic, ENT_QUOTES);//Mauro
            $arr[] = $obj;
    }

    echo "({ total: ".$nbrows.",  results: " . json_encode($arr) . "})";
        
}

function getAgente() 
{
    global $conn;
    $str = "SELECT * FROM general WHERE activa=1 AND legajo=".$_GET["leg"];
    $result = $conn->query($str);
    $nbrows = $result->num_rows;
    
    $arr = array();
    while ($obj = $result->fetch_object())
    {
        $obj->domic=htmlspecialchars_decode($obj->domic, ENT_QUOTES);//Mauro
        $arr[] = $obj;
    }

    echo "({ total: ".$nbrows.",  results: " . json_encode($arr) . "})";
        
}

// Editar o Agregar
function setAgente() 
{
    global $conn;
    $conn->autocommit(false);
    $firma_aut = ($_POST["firma_autorizada"] == "on")? "S" : "N";
    //$sanciones = ($_POST["sanciones"] == "on")? "S" : "N";
    //$estudia = ($_POST["estudia"] == "on")? "S" : "N";
    //$embargos = ($_POST["embargos"] == "on")? "S" : "N";
    //$asigfamil = ($_POST["asigfamil"] == "on")? "S" : "N";
    $sangre = isset($_POST["sangre"])?$_POST["sangre"]:"";
    $factor = isset($_POST["factor"])?$_POST["factor"]:"";
    $retencion = ($_POST["retencion"] == "on")?1:0;
    $antiguedad = isset($_POST["antiguedad"]) && $_POST["antiguedad"]!=""?$_POST["antiguedad"]:0;
    $domic = addslashes($_POST["domic"]);
    $data = array();
    $fecha_baja = isset($_POST["fecha_baja"])?setFecha($_POST["fecha_baja"]):"";
    $acordada_baja = isset($_POST["acordada_baja"])?$_POST["acordada_baja"]:"";
    $leg = isset($_SESSION["legajo"])? $_SESSION["legajo"]:'';
    $padron_1 = substr($_POST["padron"],0,6).substr($_POST["padron"],-1);
    $padron_relleno = str_pad($padron_1,7,"0");
    try
    {
        if ($_POST["accion"] == 'editar') 
        {
            // Editar
    
            //Si el empleado está dado de baja, hago update de la acordada y de la fecha de baja.
            $str_cons = "SELECT * FROM general WHERE legajo=".$_POST["legajo"];
            $res_cons = $conn->query($str_cons);
            if(!$res_cons)
            {
                throw new Exception("Error al consultar la base de datos. Error: ".$conn->error);
            }
            $obj_cons = $res_cons->fetch_object();
            if($obj_cons->activa==0) //Si está dado de baja, puedo actualizar los datos de la baja. Caso contrario, los omito y actualizo solo los datos del legajo
            {
                $str_upd_baja = "UPDATE bajas SET fecha='".$fecha_baja."', acordada='".$acordada_baja."' WHERE legajo=".$_POST["legajo"];
                $cons_upd_baja = $conn->query($str_upd_baja);
                if(!$cons_upd_baja)
                {
                    throw new Exception("Error al actualizar la base de datos. Error: ".$conn->error);
                }
                escribir_log("personal", $leg, $_POST["legajo"], "bajas", 2, $str_upd_baja, date("Y-m-d H:i:s"));
            }
            $str = "UPDATE general SET padron='".$_POST["padron"]."', apellido=UPPER('".trim($_POST["apellido"])."'), nombre=UPPER('".trim($_POST["nombre"])."'), domic=UPPER('".trim($domic)."'), localidad=".$_POST["localidad"].", tipodoc='".$_POST["tipodoc"]."', nrodoc='".trim($_POST["nrodoc"])."', cuil='".trim($_POST["cuil"])."',sexo='".$_POST["sexo"]."', telef='".trim($_POST["telef"])."', estaciv='".$_POST["estaciv"]."', fecnac='".setFecha($_POST["fecnac"])."', fecha_carga='".setFecha($_POST["fecha_carga"])."', obs='".$_POST["obs"]."', sangre='".$sangre."', factor='".$factor."', nacionalidad='".$_POST["nacionalidad"]."', firma_autorizada='".$firma_aut."', antiguedad=".$antiguedad.", retencion=".$retencion.", padron_ci='".$padron_relleno."', mail_personal='".trim($_POST["mail_personal"])."', mail_corporativo='".trim($_POST["mail_corporativo"])."' WHERE legajo=".$_POST["legajo"];
    
            $result = $conn->query($str);
            if(!$result)
            {
                throw new Exception("Error al actualizar la base de datos. Error: ".$conn->error);
            }
            //Actualizo el nombre de usuario de dominio
            $str_dom = "UPDATE usuarios SET usuario_dominio='".trim($_POST["usuario_dominio"])."' WHERE legajo=".$_POST["legajo"];
            $cons_dom = $conn->query($str_dom);
            if(!$cons_dom)
            {
                throw new Exception("Error al actualizar la base de datos. Error: ".$conn->error);
            }    
            // Escribir log
            escribir_log("personal", $leg, $_POST["legajo"], "general", 2, $str, date("Y-m-d H:i:s"));
            escribir_log("personal", $leg, $_POST["legajo"], "usuarios", 2, $str_dom, date("Y-m-d H:i:s"));
            $data["success"] = true;
            $data["legajo"] = $_POST["legajo"];
            $ultimo = $_POST["legajo"];
            $conn->commit();
        } 
        else 
        {
            // Agregar
            //Compruebo que el legajo no exista
            $str_exist = "SELECT * FROM general WHERE legajo=".$_POST["legajo"];
            $res_exist = $conn->query($str_exist);
            if(!$res_exist)
            {
                throw new Exception("Error al consultar la base de datos. Error: ".$conn->error);
            }
            if($res_exist->num_rows>0)
            {
                throw new Exception("Numero de legajo existente. Ingresar uno diferente.");
            }
            $str = "INSERT INTO general (legajo, padron, apellido, nombre, domic, localidad, tipodoc, nrodoc, cuil, sexo, telef, estaciv, fecnac, fecha_carga, activa, obs, sangre, factor, nacionalidad, firma_autorizada, retencion, antiguedad, padron_ci, mail_personal, mail_corporativo) VALUES (".trim($_POST["legajo"]).", '".trim($_POST["padron"])."', UPPER('".trim($_POST["apellido"])."'), UPPER('".trim($_POST["nombre"])."'), UPPER('".trim($domic)."'), ".$_POST["localidad"].", '".$_POST["tipodoc"]."', ".trim($_POST["nrodoc"]).", '".trim($_POST["cuil"])."', '".$_POST["sexo"]."', '".trim($_POST["telef"])."', '".$_POST["estaciv"]."', '".setFecha($_POST["fecnac"])."', '".setFecha($_POST["fecha_carga"])."', 1, '".$_POST["obs"]."', '".$sangre."', '".$factor."', '".$_POST["nacionalidad"]."', '".$firma_aut."', ".$retencion.", ".$antiguedad.", '".$padron_relleno."', '".trim($_POST["mail_personal"])."', '".trim($_POST["mail_corporativo"])."')";

            $result = $conn->query($str);
            if(!$result)
            {
                throw new Exception("Error al actualizar la base de datos. Error: ".$conn->error);
            }
            //Creo el nombre de usuario para que el agente acceda al sistema
            $user = createUser($_POST["nombre"], 1, $_POST["apellido"]);
            
            //Si el usuario ya existe, agrego la siguiente letra del nombre e itero hasta que encuentre un nombre de usuario disponible
            $pos=2;
            while(!existUser($user))
            {
                $user = createUser($_POST["nombre"], $pos, $_POST["apellido"]);
                $pos++;
            }
            //Password aleatorio
            $pass = getRandomPass();

            //Inserto el nuevo usuario
            $str_user = "INSERT INTO usuarios (usuario, password, legajo, permiso, permiso_vista, firstPass, evaluador, usuario_dominio, dominio) VALUES ('".strtolower($user)."','".md5($pass)."',".$_POST["legajo"].",3,1,'si','no','".trim($_POST["usuario_dominio"])."', 'jussanjuan.gov.ar')";
            
            $cons_user = $conn->query($str_user);
            if(!$cons_user)
            {
                throw new Exception("Error al actualizar la base de datos. Error: ".$conn->error);
            }
            //Inserto permisos basicos (estan predeterminados en la tabla, por lo que solamente inserto una fila con el legajo)
            $str_perm = "INSERT INTO permisos_sist_pers (legajo) VALUES (".$_POST["legajo"].")";
            $cons_perm = $conn->query($str_perm);
            if(!$cons_perm)
            {
                throw new Exception("Error al actualizar la base de datos. Error: ".$conn->error);
            }
            // Escribir log
            escribir_log("personal", $leg, trim($_POST["legajo"]), "general", 1, $str, date("Y-m-d H:i:s"));
            escribir_log("personal", $leg, trim($_POST["legajo"]), "usuarios", 1, $str_user, date("Y-m-d H:i:s"));
            escribir_log("personal", $leg, trim($_POST["legajo"]), "permisos_sist_per", 1, $str_perm, date("Y-m-d H:i:s"));
            $data["success"] = true;
            $data["legajo"] = $_POST["legajo"];
            $data["nrodoc"] = $_POST["nrodoc"];
            $data["apellido"] = $_POST["apellido"];
            $data["nombre"] = $_POST["nombre"];
            $data["user"] = strtolower($user);
            $data["pass"] = $pass;
            $conn->commit();
        }
    }
    catch(Exception $e)
    {
        $conn->rollBack();
        $data["success"] = false;
        $data["error"] = $e->getMessage();
    }
    echo json_encode($data);
}

//Echar al empleado. Mauro
function bajaAgente() 
{
    global $conn;
    $data = array();
    $conn->autocommit(false);
    
    $leg = isset($_SESSION["legajo"])? $_SESSION["legajo"]:'';
    $tipo_baja = isset($_POST["tramite_baja"]) && $_POST["tramite_baja"]=='on'?2:3;
    try
    {
        $str_exist = "SELECT * FROM general WHERE legajo=".$_POST["legajo"];
        $res_exist = $conn->query($str_exist);
        $fila_exist = $res_exist->fetch_object();
        if($fila_exist->activa == 1 || $fila_exist->activa == 2) //Si el empleado está activo o en tramite, doy de baja si aplica
        {
            if($tipo_baja==2)//Tramite en curso, solo tildo en la tabla general
            {
                $str_temp = "UPDATE general SET activa=2 WHERE legajo=".$_POST["legajo"];
                $cons_temp = $conn->query($str_temp);
                if(!$cons_temp)
                {
                    throw new Exception("Error al actualizar base de datos. Error BD: ".$conn->error);
                }
                escribir_log("personal", $leg, $_POST["legajo"], "general", 2, $str_temp, date("Y-m-d H:i:s"));
                $conn->commit();
                $data["success"] = true;
                $data["msg"] = "Agente en tr&aacute;mite de jubilaci&oacute;n";
            }
            else //Baja definitiva. Borro el registro de todas las tablas
            {
                $str_baja = "UPDATE general SET activa=3 WHERE legajo=".$_POST["legajo"];
                $cons_baja = $conn->query($str_baja);
                if(!$cons_baja)
                {
                    throw new Exception("Error al actualizar base de datos. Error BD: ".$conn->error);
                }
                
                //Elimino el usuario de la tabla usuarios y evaluadores
                $str_del_user = "DELETE FROM usuarios WHERE legajo=".$_POST["legajo"];
                $cons_del_user = $conn->query($str_del_user);
                if(!$cons_del_user)
                {
                    throw new Exception("Error al actualizar base de datos. Error BD: ".$conn->error);
                }

                $str_del_eval = "DELETE FROM eval_evaluadores WHERE legajo_evaluador=".$_POST["legajo"]." OR legajo_evaluado=".$_POST["legajo"];
                $cons_del_eval = $conn->query($str_del_eval);
                if(!$cons_del_eval)
                {
                    throw new Exception("Error al actualizar base de datos. Error BD: ".$conn->error);
                }

                $str_ins = "INSERT INTO bajas (legajo, fecha, acordada, estado) VALUES (".$_POST["legajo"].", '".setFecha($_POST["fecha_baja"])."', '".$_POST["acordada"]."', ".$tipo_baja.")";
                $result_ins = $conn->query($str_ins);
                if(!$result_ins)
                {
                    throw new Exception("Error al cargar en la tabla bajas. Error BD: ".$conn->error);
                }

                //Doy de baja al usuario del sistema
                $str_baja_user = "DELETE FROM usuarios WHERE legajo=".$_POST["legajo"];
                $cons_baja_user = $conn->query($str_baja_user);
                if(!$cons_baja_user)
                {
                    throw new Exception("Error al dar de baja al usuario. Error BD: ".$conn->error);
                }
                //Obtengo el ultimo id del historial de lugar, para poner en "hasta" la fecha de baja del empleado
                $str_cons_hist = "SELECT * FROM histolugar where legajo=".$_POST["legajo"]." ORDER BY desde DESC LIMIT 0,1";
                $res_cons_hist = $conn->query($str_cons_hist);
                $fila_cons = $res_cons_hist->fetch_object();
                $id_hist = $fila_cons->id;
        
                $str_upd_hist_lugar = "UPDATE histolugar SET hasta='".setFecha($_POST["fecha_baja"])."' WHERE id=".$id_hist;
                $res_upd_hist_lugar = $conn->query($str_upd_hist_lugar);
                
                // Escribir log
                
                //$str_limpia = addslashes($str); //Bug corregido Mauro
                //$str_log = "INSERT INTO usuarios_log (legajo, tabla, log, fecha) VALUES (".$leg.", 'general', '".$str_limpia."', '".date("Y-m-d H:i:s")."')";
                //mysqli_query($conn, $str_log);
                escribir_log("personal", $leg, $_POST["legajo"], "general", 2, $str_baja, date("Y-m-d H:i:s"));
                escribir_log("personal", $leg, $_POST["legajo"], "bajas", 1, $str_ins, date("Y-m-d H:i:s"));
                escribir_log("personal", $leg, $_POST["legajo"], "histolugar", 2, $str_upd_hist_lugar, date("Y-m-d H:i:s"));
                escribir_log("personal", $leg, $_POST["legajo"], "usuarios", 3, $str_baja_user, date("Y-m-d H:i:s"));
            }
        }
        else //El empleado ya fue echado
        {
            throw new Exception("El empleado ya fue dado de baja anteriormente");
        }
    }
    catch(Exception $e)
    {
        $conn->rollBack();
        $data["success"] = false;
        $data["error"] = $e->getMessage();
    }
    echo json_encode($data);
}

function existUser($user)
{
    global $conn;
    
    $str_ex = "SELECT * FROM usuarios WHERE usuario='".$user."'";
    $res_ex = $conn->query($str_ex);
    if($res_ex->num_rows>0)
    {
        return false;
    }
    else
    {
        return true;
    }
}

function existUserByLegajo($legajo)
{
    global $conn;
    
    $str_ex = "SELECT * FROM usuarios WHERE legajo=".$legajo;
    $res_ex = $conn->query($str_ex);
    if($res_ex->num_rows>0)
    {
        return false;
    }
    else
    {
        return true;
    }
}

function createUser($nombre, $cantCharName, $apellido)
{
    //Elimino espacios en los nombres
    $nombre = str_replace(" ", "", $nombre);

    $nom = substr($nombre,0,$cantCharName);//primera letra del nombre
    $primer_apellido = explode(" ",$apellido);//primer apellido
    $ap="";
    $cant_palabras = count($primer_apellido);
    if(strlen($primer_apellido[0])<4)//Si el apellido es compuesto o es menor de 3 caracteres, junto todas las palabras
    {
        $i=0;
        $flag=0;
        while($flag==0 && $i<$cant_palabras)
        {
            if(strlen($primer_apellido[$i])>3)
            {
                $flag=1;
            }    
            $ap.=$primer_apellido[$i];
            $i++;
        }
    }
    else
    {
        $ap = $primer_apellido[0];
    }
    
    $user = $nom.$ap; //Construyo el nombre del usuario con la primera letra del nombre y el primer apellido
    $user = str_replace("ñ","n",$user);
    $user = str_replace("Ã‘","n",$user);
    $user = str_replace("Ñ","n",$user);
    $user = str_replace("ã±","n",$user);
    return $user;
}

function getRandomPass()
{
    //Cadena de caractares. Saco la i mayuscula y minuscula y la l mayuscula y minuscula para evitar confusiones
    $cadena = "ABCDEFGHJKMNOPQRSTUVWXYZabcdefghjkmnopqrstuvwxyz1234567890";
    
    //Longitud de la cadena de caracteres
    $longitudCadena=strlen($cadena);

    //Variable que va a contener la contraseña
    $pass = "";
    
    //Longitud de la contraseña
    $longitudPass=5;

    for($i=1 ; $i<=$longitudPass ; $i++){
        //Definimos numero aleatorio entre 0 y la longitud de la cadena de caracteres-1
        $pos=rand(0,$longitudCadena-1);

        //Vamos formando la contraseña en cada iteraccion del bucle, añadiendo a la cadena $pass la letra correspondiente a la posicion $pos en la cadena de caracteres definida.
        $pass.= substr($cadena,$pos,1);
    }
    return $pass;
//return(md5("123456"));
}

function updateSeniority()
{
    global $conn;
    $data = array();
    $mes_act = date("m");
    //$mes_act = "01";
    $conn->autocommit(false);
    $hoy = date("Y-m-d");
    //$hoy = "2020-01-05";
    $mes_limite = "06-30";
    $anio_ant = date("Y")-1;
    //$anio_ant = "2019";
    $band = 0;
    $fecha_limite = $anio_ant."-".$mes_limite;
    
    $data["cant_actualizados"] = 0;
    $data["cant_procesados"] = 0;
    $data["errores"] = "";
    try
    {
        if($mes_act=='01')//Esto es para que el proceso solo se puda correr durante el mes de enero, que es cuando se actualizan las antiguedades de los empleados
        {
            $str_fecha = "SELECT legajo, fecha_carga FROM general WHERE activa=1";
            $cons_fecha = $conn->query($str_fecha);
            if(!$cons_fecha)
            {
                throw new Exception("Error al consultar la base de datos. Error BD: ".$conn->error);
            }
            if($cons_fecha->num_rows==0)
            {
                throw new Exception("Error al consultar la base de datos. No se encontraron agentes cargados");
            }
            while($obj_fecha = $cons_fecha->fetch_object())
            {
                $data["cant_procesados"]++;
                $fecha_ing = new DateTime($obj_fecha->fecha_carga);
                if($fecha_ing->format("Y-m-d")<=$fecha_limite)
                {
                    $upd_ant = "UPDATE general SET antiguedad=antiguedad+1 WHERE legajo=".$obj_fecha->legajo;
                    $cons_ant = $conn->query($upd_ant);
                    if(!$cons_ant)
                    {
                        throw new Exception("Error al escribir la base de datos. Error BD: ".$conn->error);
                    }
                    $data["cant_actualizados"]++;
                    $conn->commit();
                    $leg = isset($_SESSION["legajo"])? $_SESSION["legajo"]:'';
                    escribir_log("personal", $leg, $obj_fecha->legajo, "general", 2, $upd_ant, date("Y-m-d H:i:s"));
                }
            }
            
            $data["success"] = true;
            $data["msg"] = "Antiguedades actualizadas";
        }
        else
        {
            throw new Exception("Este proceso solo puede ser ejecutado en el mes de enero");
        }
    }
    catch(Exception $e)
    {
        $conn->rollBack();
        $data["success"] = false;
        $data["error"] = $e->getMessage();
    }
    echo json_encode($data);
}