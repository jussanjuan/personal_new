<?php
session_set_cookie_params(0);
session_start();

require('system.config.php');

$sist = isset($_GET['sist'])?$_GET['sist']:'';

switch ($sist) {
	case 'personal': 
		getAuditoriaSistPersonal();
		break;
	default:
		echo "({ total: 0, results: })";
		break;
}

function getAuditoriaSistPersonal() 
{
	global $conn;
    $arr = array();
    $st = isset($_GET['start'])?(integer)$_GET['start']:null;
    $end = isset($_GET['limit'])?(integer)$_GET['limit']:null;
    if($st || $end)
    {
        $limite = " LIMIT ".$st.",".$end; 
    }
    else
    {
        $limite = ""; 
    }
    

    if (isset($_GET["query"])) //Se ingresaron caracteres para buscar
    {
        $query = trim($_GET["query"]);
        $query = str_replace ("'","",$query);
        $query = str_replace("\"","",$query);
        $query = str_replace("*","",$query);
        $query = str_replace("%","",$query);
        $query = htmlspecialchars($query);

        //Parseo la variable $_GET["fields"], que trae los filtros de busqueda. Mauro
        if($_GET["fields"]!=NULL) //Si se selecciono al menos un filtro de busqueda
        {
            $cad = str_replace("[", "", $_GET["fields"]);
            $cad = str_replace("]", "", $cad);
            $cad = str_replace("\"", "", $cad);
            $cad = explode(",", $cad);
            $str_filt = "";
            for($i=0;$i<count($cad);$i++)
            {
            	if($cad[$i]=="legajo")
            	{
            		$str_filt.= "general.".$cad[$i]." LIKE '%".$query."%'";
            	}
            	else
                {
                	if($cad[$i]=="nombreEmpleado" || $cad[$i]=="nombreObjetivo")
	            	{
	            		$str_filt.= "general.nombre LIKE '%".$query."%'";
	            	}
	            	else
                	{
                		if($cad[$i]=="nombre")
		            	{
		            		$str_filt.= "tipo_operacion_log.nombre LIKE '%".$query."%'";
		            	}
		            	else
                		{
                            if($cad[$i]=="apellidoObjetivo")
                			{
                                $str_filt.= "general.apellido LIKE '%".$query."%'";
                            }
                            else
                            {
                                $str_filt.= $cad[$i]." LIKE '%".$query."%'";
                            }
                		}
                	}
                }
                if($i!=(count($cad)-1))
                    $str_filt.=" OR ";
            }
            $str_log = "SELECT usuarios_log.*, tipo_operacion_log.nombre, general.apellido, general.nombre as nombreEmpleado FROM usuarios_log, tipo_operacion_log, general WHERE usuarios_log.operacion=tipo_operacion_log.id AND usuarios_log.legajo=general.legajo AND sistema='personal' AND tabla!='historiaClinica' AND (".$str_filt.") ORDER BY usuarios_log.id DESC";
            
        }
        else //Si se destiladaron todos los filtros de busqueda, busco todo, para evitar errores
        {
            $str_log = "SELECT usuarios_log.*, tipo_operacion_log.nombre, general.apellido, general.nombre as nombreEmpleado FROM usuarios_log, tipo_operacion_log, general WHERE usuarios_log.operacion=tipo_operacion_log.id AND usuarios_log.legajo=general.legajo AND sistema='personal' AND tabla!='historiaClinica' ORDER BY usuarios_log.id DESC";
        }
        //echo $str_log;
    } 
    else
    {
    	$str_log = "SELECT usuarios_log.*, tipo_operacion_log.nombre, general.apellido, general.nombre as nombreEmpleado FROM usuarios_log, tipo_operacion_log, general WHERE usuarios_log.operacion=tipo_operacion_log.id AND usuarios_log.legajo=general.legajo AND sistema='personal' AND tabla!='historiaClinica' ORDER BY usuarios_log.id DESC";
    }
    $conn->query("SET NAMES UTF-8");
    $cons_log = $conn->query($str_log);
    $nbrows = $cons_log->num_rows;
    
    if($nbrows>0)
    {
    	$str_log.=$limite;
    	$cons_log = $conn->query($str_log);
    	while ($obj_log = $cons_log->fetch_object())
        {
            $str_obj = "SELECT apellido, nombre FROM general WHERE legajo=".$obj_log->legajo_objetivo;
        	$cons_obj = $conn->query($str_obj);
        	if($cons_obj->num_rows>0)
        	{
        		$obj_obj = $cons_obj->fetch_object();
        		$obj_log->nombreObjetivo = $obj_obj->nombre;
        		$obj_log->apellidoObjetivo = $obj_obj->apellido;

        	}
        	else
        	{
        		$obj_log->nombreObjetivo = "";
        		$obj_log->apellidoObjetivo = "";
        	}
            $arr[]= $obj_log;
        }
    }
	echo "({ total: ".$nbrows.", results: " . json_encode($arr) . "})";
}
?>