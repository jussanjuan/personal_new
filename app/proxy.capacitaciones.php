<?php
session_set_cookie_params(0);
session_start();
require('system.config.php');
require('proxy.feriados.php');

$tarea = $_SERVER['REQUEST_METHOD'];
$tarea = isset($_POST["idCapacitacionElimin"])?'DELETE':$_SERVER['REQUEST_METHOD'];
$act = isset($_POST["act"])?$_POST["act"]:"";
//echo $tarea;
switch ($tarea) {
	case 'GET':
        $act = isset($_GET["act"])?$_GET["act"]:"";
        switch ($act)
        {
            case "":
                getCapacitaciones();
                break;
            case "getTipoCapacitaciones":
                getTipoCapacitaciones();
                break;    
            case "getModalidadCapacitaciones":
                getModalidadCapacitaciones();
                break; 
            case "getOrigenCapacitaciones":
                getOrigenCapacitaciones();
                break;       
        }
        break;
	case 'POST':
        switch ($act)
        {
            case "": //Si es vacio, asigna una capacitacion al agente. Mauro
                setCapacitacion();
                break;
            case "editCapacitacion": //Modifica una capacitacion asignada al agente. Mauro
                editCapacitacion();
                break;    
            case "altaCapacitacion": //Alta de una nueva capacitacion en el sistema. Mauro
                altaCapacitacion();
                break;
            case "bajaCapacitacion": //Eliminar una capacitacion del sistema. Mauro
                bajaCapacitacion();
                break;
            case "modificaCapacitacion": //Modifica una capacitacion del sistema. Mauro
                modificaCapacitacion();
                break;
        }
		break;
	case 'PUT':
		break;
	case 'DELETE':
        deleteCapacitacion(); //Des asigna una capacitacion a un agente
		break;
	default:
		echo "({ failure: 'Error por default'})";
		break;
}


function getCapacitaciones() 
{
	global $conn;
    $arr = array();
    $nbrows = 0;
	if (isset($_GET["legajo"])) 
    {
		$str = "SELECT capacitaciones.id, capacitaciones.legajo, capacitaciones.nombre, capacitaciones.tipo as idtipo, capacitaciones.origen as idorigen, capacitaciones.inherencia, capacitaciones.fecha, capacitaciones.cant_horas, capacitaciones.modalidad as idmodalidad, capacitaciones.evaluacion, capacitaciones.nota_evaluacion, capacitaciones.observaciones, capacitacionestipo.nombre as tipo_capacitacion, capacitacionesmodalidad.nombre as modalidad, capacitacionesorigen.nombre as origen FROM capacitaciones, capacitacionestipo, capacitacionesmodalidad, capacitacionesorigen WHERE capacitaciones.tipo=capacitacionestipo.id AND capacitaciones.modalidad=capacitacionesmodalidad.id AND capacitaciones.origen=capacitacionesorigen.id AND capacitaciones.legajo=".$_GET["legajo"]." ORDER BY capacitaciones.tipo ASC";
        $result = $conn->query($str);
        $nbrows = $result->num_rows;
        while ($obj = $result->fetch_object())
        {
            $arr[] = $obj;
        }
        $str_2 = "SELECT c.id, ca.legajo, c.nombre, c.tipo as idtipo, c.origen as idorigen, ca.inherencia, cs.nombre as nombreSesion, cs.fecha, c.cant_horas, c.modalidad as idmodalidad, c.evaluacion, ca.nota_capacitacion, c.observaciones, ct.nombre as tipo_capacitacion, cm.nombre as modalidad, co.nombre as origen FROM cap_capacitacion c LEFT JOIN cap_capacitacion_sesion cs ON c.id=cs.id_capacitacion LEFT JOIN capacitacionestipo ct ON c.tipo=ct.id LEFT JOIN capacitacionesmodalidad cm ON c.modalidad=cm.id LEFT JOIN capacitacionesorigen co ON c.origen=co.id LEFT JOIN cap_capacitacion_asistente ca ON cs.id=ca.id_sesion WHERE ca.legajo=".$_GET["legajo"]." AND (c.asistencia=0 OR (c.asistencia=1 AND ca.presente=1)) ORDER BY c.tipo ASC";
        $cons_str_2 = $conn->query($str_2);
        $nbrows+=$cons_str_2->num_rows;
        while($obj_str_2 = $cons_str_2->fetch_object())
        {
            $obj_str_2->nombre = $obj_str_2->nombre." ".$obj_str_2->nombreSesion;
            $obj_str_2->inherencia = $obj_str_2->inherencia==1?'Si':'No';
            $obj_str_2->evaluacion = $obj_str_2->evaluacion==1?'Si':'No';
            $obj_str_2->nota_evaluacion = $obj_str_2->nota_capacitacion;
            $arr[] = $obj_str_2;
        }
	}
	echo "({ total: ".$nbrows.",  results: " . json_encode($arr) . "})";
}

function getTipoCapacitaciones() 
{
    global $conn;

    $str = "SELECT * FROM capacitacionestipo ORDER BY id ASC";
        
    $result = $conn->query($str);
    $nbrows = $result->num_rows;
    $arr = array();
    while ($obj = $result->fetch_object()){
        $arr[] = $obj;
    }

    echo "({ total: ".$nbrows.",  results: " . json_encode($arr) . "})";
}

function getModalidadCapacitaciones() 
{
    global $conn;

    $str = "SELECT * FROM capacitacionesmodalidad ORDER BY id ASC";
        
    $result = $conn->query($str);
    $nbrows = $result->num_rows;
    $arr = array();
    while ($obj = $result->fetch_object()){
        $arr[] = $obj;
    }

    echo "({ total: ".$nbrows.",  results: " . json_encode($arr) . "})";
}

function getOrigenCapacitaciones()
{
    global $conn;

    $str = "SELECT * FROM capacitacionesorigen ORDER BY id ASC";
        
    $result = $conn->query($str);
    $nbrows = $result->num_rows;
    $arr = array();
    while ($obj = $result->fetch_object()){
        $arr[] = $obj;
    }

    echo "({ total: ".$nbrows.",  results: " . json_encode($arr) . "})";
}


function setCapacitacion() 
{
	global $conn;
    $data = array();
    $nota = isset($_POST["notaCapacitacion"])?$_POST["notaCapacitacion"]:'';
    $inherencia = !isset($_POST["inherenciaCapacitacion"]) || $_POST["inherenciaCapacitacion"]==''?'No':$_POST["inherenciaCapacitacion"];
    $str_ins = "INSERT INTO capacitaciones (legajo, nombre, tipo, origen, inherencia, fecha, cant_horas, modalidad, evaluacion, nota_evaluacion, observaciones) VALUES (".$_POST["legajo"].", '".$_POST["nombre_capacitacion"]."', ".$_POST["tipoCapacitacion"].", ".$_POST["origenCapacitacion"].", '".$inherencia."', '".setFecha($_POST["fecha"])."', ".$_POST["cant_horas"].", ".$_POST["modalidadCapacitacion"].", '".$_POST["evaluacionCapacitacion"]."', '".$_POST["notaCapacitacion"]."', '".$_POST["observacionesCapacitacion"]."')";
    //echo $str_ins;
    $cons_ins = $conn->query($str_ins);
    if($cons_ins)
    {
        $data["success"] = true;
        $data["msg"] = "Capacitaci&oacute;n cargada correctamente.";

        //Escribir log
        $legajo=isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
        //$str_log_ins = "INSERT INTO usuarios_log (legajo, tabla, log, fecha) VALUES (".$legajo.", 'capacitaciones', '".addslashes($str_ins)."', '".date("Y-m-d H:i:s")."')";
        //$res_log_gen = $conn->query($str_log_ins);
        escribir_log("personal", $legajo, $_POST["legajo"], "capacitaciones", 1, $str_ins, date("Y-m-d H:i:s"));

    }
    else
    {
        $data["success"] = false;
        $data["error"] = $conn->error;

    }
	echo json_encode($data);
}

function editCapacitacion() 
{
    global $conn;
    $data = array();
    $nota = isset($_POST["nota_evaluacion"])?$_POST["nota_evaluacion"]:'';
    $id = $_POST["idCap"];
    $inherencia = !isset($_POST["inherencia"]) || $_POST["inherencia"]==''?'No':$_POST["inherencia"];
    $str_upd = "UPDATE capacitaciones SET nombre='".$_POST["nombre"]."', tipo=".$_POST["tipo"].", origen=".$_POST["origen"].", inherencia='".$inherencia."', fecha='".setFecha($_POST["fecha"])."', cant_horas=".$_POST["cant_horas"].", modalidad=".$_POST["modalidad"].", evaluacion='".$_POST["evaluacion"]."', nota_evaluacion='".$_POST["nota_evaluacion"]."', observaciones='".$_POST["observaciones"]."' WHERE id=".$id;
    //echo $str_upd;
    $cons_upd = $conn->query($str_upd);
    if($cons_upd)
    {
        $data["success"] = true;
        $data["msg"] = "Capacitaci&oacute;n modificada correctamente.";

        //Escribir log
        $legajo=isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
        //$str_log_ins = "INSERT INTO usuarios_log (legajo, tabla, log, fecha) VALUES (".$legajo.", 'capacitaciones', '".addslashes($str_upd)."', '".date("Y-m-d H:i:s")."')";
        //$res_log_gen = $conn->query($str_log_ins);
        escribir_log("personal", $legajo, 0, "capacitaciones", 2, $str_upd, date("Y-m-d H:i:s"));

    }
    else
    {
        $data["success"] = false;
        $data["error"] = $conn->error;

    }
    echo json_encode($data);
}

//Des asigna una capacitacion a un agente
function deleteCapacitacion()
{
    global $conn;
    $data = array();    
    $str_del = "DELETE FROM capacitaciones WHERE id=".$_POST['idCapacitacionElimin'];
    $res_del = $conn->query($str_del);
    //echo $str_del;
    if($res_del)
    {
        $data["success"] = true;
        $data["msg"] = "Capacitaci&oacute;n eliminada correctamente.";

        //Escribir log
        $legajo=isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
        //$str_log_ins = "INSERT INTO usuarios_log (legajo, tabla, log, fecha) VALUES (".$legajo.", 'capacitaciones', '".addslashes($str_del)."', '".date("Y-m-d H:i:s")."')";
        //$res_log_gen = $conn->query($str_log_ins);
        escribir_log("personal", $legajo, 0, "capacitaciones", 3, $str_del, date("Y-m-d H:i:s"));
    }
    else
    {
        $data["success"] = false;
        $data["error"] = $conn->error;

    }
    echo json_encode($data);
}

//Ingresa una sancion en la tabla de la bd. Mauro
function altaSancion()
{
    global $conn;
    $data = array();    
    $str_cons = "SELECT * FROM cargos WHERE cargo='".$_POST['nombreCargo']."'";
    //echo $str_cons;
    $res_cons = $conn->query($str_cons);
    if($res_cons->num_rows)
    {
         $data["success"] = false;
         $data["error"] = "Ya existe un cargo con el mismo nombre";
    }
    else
    {
        $str_cons = "SELECT * FROM cargos ORDER BY id DESC";
        $res_cons = $conn->query($str_cons);
        $fila_cons = $res_cons->fetch_object();
        
        $str_ins = "INSERT INTO cargos (nrocargo, cargo, escalafon) VALUES (".($fila_cons->nrocargo+1).", UPPER('".$_POST["nombreCargo"]."'), ".$_POST["escalafon"].")";
        $res_ins = $conn->query($str_ins);
        if(!$res_ins)
        {
            $data["success"] = false;
            $data["error"] = $conn->error;
        }
        else
        {
            $data["success"] = true;
            $data["msg"] = "Cargo dado de alta correctamente";
        }
        
    }
    echo json_encode($data);
    
}

//Elimina una sancion de la tabla de la bd. Mauro
function bajaSancion()
{
    global $conn;
    $data = array();    
    
    $str_nrocarg = "SELECT * FROM cargos WHERE id=".$_POST['cargoElimin'];
    $res_nrocarg = $conn->query($str_nrocarg);
    $fila_nrocarg = $res_nrocarg->fetch_object();
    //echo $str_nrocarg;
    $str_cons = "SELECT * FROM histocargo WHERE nrocargo=".$fila_nrocarg->nrocargo;
    //echo "<br/>".$str_cons;
    $res_ins = $conn->query($str_cons);
    if($res_ins->num_rows)
    {
        $data["success"] = false;
        $data["error"] = "Error al eliminar el cargo, hay agentes con dicho cargo asignado.";
    }
    else
    {
        $str_del = "DELETE FROM cargos WHERE id=".$_POST['cargoElimin'];
        //echo "<br/>".$str_del;
        $res_del = $conn->query($str_del);
        if(!$res_del)
        {
            $data["success"] = false;
            $data["error"] = "Error al eliminar el cargo. Intente nuevamente.";
        }
        else
        {    
            $data["success"] = true;
            $data["msg"] = "Cargo eliminado";
        }
    }
    echo json_encode($data);
    
}

//Modifica una sancion de la tabla de la bd. Mauro
function modificaSancion()
{
    global $conn;
    $data = array();    
    
    $str_carg = "SELECT * FROM cargos WHERE id=".$_POST['cargoModif'];
    $res_carg = $conn->query($str_carg);
    $fila_carg = $res_carg->fetch_object();
    //echo $str_nrocarg;
    
    $str_exist = "SELECT * FROM cargos WHERE cargo='".$_POST['nombreCargoModif']."' AND id<>".$_POST['cargoModif'];
    $res_exist = $conn->query($str_exist);
    
    //Si el nuevo nombre que quieren modificar es igual a algun otro lugar ya cargado, largo error.
    if($res_exist->num_rows > 0)
    {
        $data["success"] = false;
        $data["error"] = "El nombre ingresado ya existe.";
    }
    else
    {
        $str_del = "UPDATE cargos SET cargo=UPPER('".$_POST["nombreCargoModif"]."'), escalafon=".$_POST["escalafon"]." WHERE id=".$_POST['cargoModif'];
        //echo "<br/>".$str_del;
        $res_del = $conn->query($str_del);
        if(!$res_del)
        {
            $data["success"] = false;
            $data["error"] = "Error al modificar el cargo. Intente nuevamente.";
        }
        else
        {    
            $data["success"] = true;
            $data["msg"] = "Cargo modificado";
        }
    }
    echo json_encode($data);
    
}
?>