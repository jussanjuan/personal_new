<?php
session_set_cookie_params(0);
session_start();
require('system.config.php');
require('proxy.feriados.php');

$tarea = $_SERVER['REQUEST_METHOD'];
$tarea = isset($_POST["idCargoElimin"])?'DELETE':$_SERVER['REQUEST_METHOD'];
$act = isset($_POST["act"])?$_POST["act"]:"";
//echo $tarea;
switch ($tarea) {
	case 'GET':
        $act = isset($_GET["act"])?$_GET["act"]:"";
        switch ($act)
        {
            case "":
                getCargos();
                break;
            case "getCargoById": //Obtener un cargo segun su ID
                getCargoById();
                break;
        }
        break;
	case 'POST':
                switch ($act)
                {
                    case "": //Si es vacio, asigna un cargo al agente. Mauro
                        setCargo();
                        break;
                    case "altaCargo": //Alta de un nuevo cargo en el sistema. Mauro
                        altaCargo();
                        break;
                    case "bajaCargo": //Eliminar un cargo del sistema. Mauro
                        bajaCargo();
                        break;
                    case "modificaCargo": //Modifica un cargo del sistema. Mauro
                        modificaCargo();
                        break;
                }
		break;
	case 'PUT':
		break;
	case 'DELETE':
                deleteCargo(); //Des asigna un cargo a un agente
		break;
	default:
		echo "({ failure: 'Error por default'})";
		break;
}


function getCargos() {
	global $conn;

	if (isset($_GET["legajo"])) {
		$str = "SELECT * FROM cargos c, histocargo h WHERE (h.legajo=".$_GET["legajo"].") AND (h.nrocargo=c.nrocargo) ORDER BY h.desde DESC";
	} else {
		$str = "SELECT * FROM cargos ORDER BY cargo ASC ";
	}
	//echo $str;
	$result = $conn->query($str);
	$nbrows = $result->num_rows;
	$arr = array();
	while ($obj = $result->fetch_object()){
		$arr[] = $obj;
	}
    
	echo "({ total: ".$nbrows.",  results: " . json_encode($arr) . "})";
}

function getCargoById()
{
    global $conn;
    $data = array();
    $str = "SELECT * FROM cargos WHERE id=".$_GET["idCargo"];
        
    $result = $conn->query($str);
    $nbrows = $result->num_rows;
    $arr = array();
    while ($obj = $result->fetch_object()){
            $arr[] = $obj;

    }
    $data["total"] = $nbrows;
    $data["results"] = $arr;
    echo json_encode($data);
}


function setCargo() 
{
	global $conn;
    $data = array();

   //Obtengo la fecha "desde" del ultimo cargo que tuvo, para ser usada en distintas partes del script
    $str_cons_ex_car_1 = "SELECT MAX(desde) as desde FROM histocargo WHERE legajo=".$_POST["legajo"];
    $res_cons_ex_car_1 = $conn->query($str_cons_ex_car_1);
    $obj_cons_ex_car_1 = $res_cons_ex_car_1->fetch_object();

    //Cuando no esta seteada la variable de sesion, da error al insertar en la base de datos. Por lo tanto inicializo en 0 la variable. Mauro
    if(isset($_SESSION['legajo']))
    {
        $legajo = $_SESSION['legajo'];
    }
    else
    {
        $legajo = 0;
    }

    //Si se cargó un cargo actual, controlo que la fecha "desde" sea posterior a la fecha "desde" del ultimo cargo que tuvo
    if(!$_POST["hasta"])
    {
        if($obj_cons_ex_car_1->desde >= setFecha($_POST["desde"]))
        {
            $data["success"] = false;
            $data["error"] = "Si desea cargar un cargo actual, la fecha de inicio debe ser posterior a la fecha del cargo anterior.";
        }
        else
        {
            $str_upd_gen = "UPDATE general SET cargoactu=".$_POST["cargoactu"]." WHERE legajo=".$_POST["legajo"];
            $result_upd_gen = $conn->query($str_upd_gen);
            if(!$result_upd_gen)
            {
                $data["success"] = false;
                $data["error"] = $conn->error;
            }
            else
            {
                // Escribir log
                //$str_log_gen = "INSERT INTO usuarios_log (legajo, tabla, log, fecha) VALUES (".$legajo.", 'general', '".addslashes($str_upd_gen)."', '".date("Y-m-d H:i:s")."')";
                //$res_log_gen = $conn->query($str_log_gen);
                escribir_log("personal", $legajo, $_POST["legajo"], "general", 2, $str_upd_gen, date("Y-m-d H:i:s"));
                //Busco el ultimo cargo que tuvo para actualizar la fecha de fin
                $str_c_hist_car = "SELECT id, desde FROM histocargo WHERE desde=(SELECT MAX(desde) FROM histocargo WHERE legajo=".$_POST["legajo"].") AND legajo=".$_POST["legajo"];
                $res_c_hist_car = $conn->query($str_c_hist_car);
                if($res_c_hist_car->num_rows!=0)
                {
                    $obj_c_hist_car = $res_c_hist_car->fetch_object();
                    $id_c_hist_car = $obj_c_hist_car->id;
                    $fecha_ult_cargo_ant = strtotime('-1 day', strtotime(setFecha($_POST["desde"])));
                    $fecha_ult_cargo_ant = date('Y-m-d', $fecha_ult_cargo_ant);
    
                    $str_upd_hist_car = "UPDATE histocargo SET hasta='".$fecha_ult_cargo_ant."' WHERE id=".$id_c_hist_car;
                    $res_upd_hist_car = $conn->query($str_upd_hist_car);
                }
                $str_hist_car = "INSERT INTO histocargo(legajo, nrocargo, sitlabo, acuerdo, desde, hasta) VALUES (".$_POST["legajo"].", ".$_POST["cargoactu"].", '".$_POST["sitlabo"]."', '".$_POST["acuerdo"]."', '".setFecha($_POST["desde"])."', NULL)";
                $res_hist_car = $conn->query($str_hist_car);
                if(!$res_hist_car)
                {
                    $data["success"] = false;
                    $data["error"] = $conn->error;
                }
                else
                {
                    
                    $data["success"] = true;
                    $data["msg"] = "Cargo asignado al empleado correctamente";
                    
                    //Escribir log
                    //$str_log_hist ="INSERT INTO usuarios_log (legajo, tabla, log, fecha) VALUES (".$legajo.", 'histocargo', '".addslashes($str_hist_car)."', '".date("Y-m-d H:i:s")."')";
                    //$res_log_hist = $conn->query($str_log_hist);
                    escribir_log("personal", $legajo, $_POST["legajo"], "histocargo", 1, $str_hist_car, date("Y-m-d H:i:s"));
                    
                }
            }
        }
    }//End if(!$_POST["hasta"])
    else //Se supone que se esta cargando un cargo anterior, dado que se ingresaron ambas fechas (desde y hasta)
    {
        //Controlo que la fecha en que se está cargando el cargo no coincida con fechas ya cargadas
        $str_cons_ex_car_2 = "SELECT * FROM histocargo WHERE legajo=".$_POST["legajo"]." AND ('".setFecha($_POST["desde"])."' BETWEEN desde AND hasta OR '".setFecha($_POST["hasta"])."' BETWEEN desde AND hasta OR desde BETWEEN '".setFecha($_POST["desde"])."' AND '".setFecha($_POST["hasta"])."' OR hasta BETWEEN '".setFecha($_POST["desde"])."' AND '".setFecha($_POST["hasta"])."')";
        $res_cons_ex_car_2 = $conn->query($str_cons_ex_car_2);
        if($res_cons_ex_car_2->num_rows>0)
        {
            $data["success"] = false;
            $data["error"] = "No se puede cargar el cargo, el periodo coincide con uno ya asignado al agente.";
        }
        else
        {
            //Validacion adicional, compruebo que la fecha desde no sea anterior a la de ingreso del empleado o posterior (dado que si es posterior, el usuario deberia cargarla como actual y dejar sin fecha "hasta")
            $str_ing = "SELECT fecha_carga FROM general WHERE legajo=".$_POST["legajo"]." AND '".setFecha($_POST["desde"])."' < DATE(fecha_carga)";
            $res_ing = $conn->query($str_ing);
            if($res_ing->num_rows>0 || setFecha($_POST["desde"]) >= $obj_cons_ex_car_1->desde)
            {
                $data["success"] = false;
                $data["error"] = "No se puede cargar el cargo, la fecha de inicio del cargo es anterior a la fecha de ingreso del empleado al poder judicial o posterior a la ultima fecha de inicio del ultimo cargo.";
            }
            else
            {
                $str_hist_car_2 = "INSERT INTO histocargo(legajo, nrocargo, sitlabo, acuerdo, desde, hasta) VALUES (".$_POST["legajo"].", ".$_POST["cargoactu"].", '".$_POST["sitlabo"]."', '".$_POST["acuerdo"]."', '".setFecha($_POST["desde"])."', '".setFecha($_POST["hasta"])."')";
                $res_hist_car_2 = $conn->query($str_hist_car_2);
                if(!$res_hist_car_2)
                {
                    $data["success"] = false;
                    $data["error"] = $conn->error;
                }
                else
                {
                    $data["success"] = true;
                    $data["msg"] = "Lugar cargado correctamente";
                    
                    // Escribir log
                    //$str_log_2= "INSERT INTO usuarios_log (legajo, tabla, log, fecha) VALUES (".$legajo.", 'histolugar', '".addslashes($str_hist_car_2)."', '".date("Y-m-d H:i:s")."')";
                    //$res_log_2 = $conn->query($str_log_2);
                    escribir_log("personal", $legajo, $_POST["legajo"], "histocargo", 1, $str_hist_car_2, date("Y-m-d H:i:s"));
                }
            }
        }
    }
	echo json_encode($data);
}

//Des asigna un cargo a un agente
function deleteCargo()
{
    global $conn;
    $data = array();   
    $conn->autocommit(false); 
    
    try
    {
        $str_cons = "SELECT * FROM histocargo WHERE id=".$_POST['idCargoElimin'];
        $res_cons = $conn->query($str_cons);
        if(!$res_cons)
        {
            throw new Exception("Error al leer la base de datos. Error: ".$conn->error);
        }
        $fila_cons = $res_cons->fetch_object();
        $legajo = $fila_cons->legajo;
        
        $str_cant = "SELECT * FROM histocargo WHERE legajo=".$legajo;
        $res_cant = $conn->query($str_cant);
        if(!$res_cant)
        {
            throw new Exception("Error al leer la base de datos. Error: ".$conn->error);
        }
        $str_del = "DELETE FROM histocargo WHERE id=".$_POST['idCargoElimin'];
        $res_del = $conn->query($str_del);
        if(!$res_del)
        {
            throw new Exception("Error al escribir la base de datos. Error: ".$conn->error);
        }
        $str_search = "SELECT * FROM histocargo WHERE legajo=".$legajo." ORDER BY id DESC";
        $res_search = $conn->query($str_search);
        if(!$res_search)
        {
            throw new Exception("Error al leer la base de datos. Error: ".$conn->error);
        }
        if($res_search->num_rows==0)
        {
            $str_upd = "UPDATE general SET cargoactu=NULL WHERE legajo=".$legajo;
            $cons_upd = $conn->query($str_upd);
            if(!$cons_upd)
            {
                throw new Exception("Error al escribir la base de datos. Error de BD: ".$conn->error);
            }
        }
        else
        {
            $fila_search = $res_search->fetch_object();
            $ult_cargo = $fila_search->nrocargo;
            $str_upd = "UPDATE general SET cargoactu=".$ult_cargo." WHERE legajo=".$legajo;
            $cons_upd = $conn->query($str_upd);
            if(!$cons_upd)
            {
                throw new Exception("Error al escribir la base de datos. Error de BD: ".$conn->error);
            }

            $str_upd_hist_car_2 = "UPDATE histocargo SET hasta=NULL WHERE id=".$fila_search->id;
            $res_upd_hist_car_2 = $conn->query($str_upd_hist_car_2);
            if(!$res_upd_hist_car_2)
            {
                throw new Exception("Error al escribir la base de datos. Error de BD: ".$conn->error);
            }
            $conn->commit();
            $data["success"] = true;
            $data["msg"] = "Cargo eliminado correctamente.";
            $leg=isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
            escribir_log("personal", $leg, $legajo, "histocargo", 3, $str_del, date("Y-m-d H:i:s"));
            escribir_log("personal", $leg, $legajo, "general", 2, $str_upd, date("Y-m-d H:i:s"));
        }
    }
    catch(Exception $e)
    {
        $conn->rollBack();
        $data["success"] = false;
        $data["error"] = $e->getMessage();
    }
    echo json_encode($data);
}

//Ingresa un cargo en la tabla de la bd. Mauro
function altaCargo()
{
    global $conn;
    $data = array();    
    $str_cons = "SELECT * FROM cargos WHERE cargo='".$_POST['nombreCargo']."'";
    //echo $str_cons;
    $res_cons = $conn->query($str_cons);
    if($res_cons->num_rows)
    {
         $data["success"] = false;
         $data["error"] = "Ya existe un cargo con el mismo nombre";
    }
    else
    {
        $str_cons = "SELECT * FROM cargos ORDER BY id DESC";
        $res_cons = $conn->query($str_cons);
        $fila_cons = $res_cons->fetch_object();
        
        $str_ins = "INSERT INTO cargos (nrocargo, cargo, escalafon) VALUES (".($fila_cons->nrocargo+1).", UPPER('".$_POST["nombreCargo"]."'), ".$_POST["escalafon"].")";
        $res_ins = $conn->query($str_ins);
        if(!$res_ins)
        {
            $data["success"] = false;
            $data["error"] = $conn->error;
        }
        else
        {
            $data["success"] = true;
            $data["msg"] = "Cargo dado de alta correctamente";
            $leg=isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
            escribir_log("personal", $leg, 0, "cargos", 1, $str_ins, date("Y-m-d H:i:s"));
        }
        
    }
    echo json_encode($data);
    
}

//Elimina un cargo de la tabla de la bd. Mauro
function bajaCargo()
{
    global $conn;
    $data = array();    
    
    $str_nrocarg = "SELECT * FROM cargos WHERE id=".$_POST['cargoElimin'];
    $res_nrocarg = $conn->query($str_nrocarg);
    $fila_nrocarg = $res_nrocarg->fetch_object();
    //echo $str_nrocarg;
    $str_cons = "SELECT * FROM histocargo WHERE nrocargo=".$fila_nrocarg->nrocargo;
    //echo "<br/>".$str_cons;
    $res_ins = $conn->query($str_cons);
    if($res_ins->num_rows)
    {
        $data["success"] = false;
        $data["error"] = "Error al eliminar el cargo, hay agentes con dicho cargo asignado.";
    }
    else
    {
        $str_del = "DELETE FROM cargos WHERE id=".$_POST['cargoElimin'];
        //echo "<br/>".$str_del;
        $res_del = $conn->query($str_del);
        if(!$res_del)
        {
            $data["success"] = false;
            $data["error"] = "Error al eliminar el cargo. Intente nuevamente.";
        }
        else
        {    
            $data["success"] = true;
            $data["msg"] = "Cargo eliminado";
            $leg=isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
            escribir_log("personal", $leg, 0, "cargos", 3, $str_del, date("Y-m-d H:i:s"));
        }
    }
    echo json_encode($data);
    
}

//Modifica un cargo de la tabla de la bd. Mauro
function modificaCargo()
{
    global $conn;
    $data = array();    
    
    $str_carg = "SELECT * FROM cargos WHERE id=".$_POST['cargoModif'];
    $res_carg = $conn->query($str_carg);
    $fila_carg = $res_carg->fetch_object();
    //echo $str_nrocarg;
    
    $str_exist = "SELECT * FROM cargos WHERE cargo='".$_POST['nombreCargoModif']."' AND id<>".$_POST['cargoModif'];
    $res_exist = $conn->query($str_exist);
    
    //Si el nuevo nombre que quieren modificar es igual a algun otro lugar ya cargado, largo error.
    if($res_exist->num_rows > 0)
    {
        $data["success"] = false;
        $data["error"] = "El nombre ingresado ya existe.";
    }
    else
    {
        $str_del = "UPDATE cargos SET cargo=UPPER('".$_POST["nombreCargoModif"]."'), escalafon=".$_POST["escalafon"]." WHERE id=".$_POST['cargoModif'];
        //echo "<br/>".$str_del;
        $res_del = $conn->query($str_del);
        if(!$res_del)
        {
            $data["success"] = false;
            $data["error"] = "Error al modificar el cargo. Intente nuevamente.";
        }
        else
        {    
            $data["success"] = true;
            $data["msg"] = "Cargo modificado";
            $leg=isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
            escribir_log("personal", $leg, 0, "cargos", 2, $str_del, date("Y-m-d H:i:s"));
        }
    }
    echo json_encode($data);
    
}
?>