<?php
session_set_cookie_params(0);
session_start();
require('system.config.php');

$tarea = $_GET["chart"];
$dest = isset($_GET["dest"])?$_GET["dest"]:'';

switch ($tarea) 
{
    case "titulos": 
        if($dest=='')
        {
            getCantTitulos();
        }
        else
        {
            getCantTitulosGrid();
        }
        break;
    case "interacciones": 
        if($dest=='')
        {
            getCantInteracciones();
        }
        else
        {
            getCantInteraccionesGrid();
        }
        break;
    case "ingresos":
        if($dest=='')
        {
            getCantIngresos();
        }
        else
        {
            getCantIngresosGrid();
        }
        break;
    case "edades": 
        getCantEdades();
        break;
    case "edadesRetiro": 
        getCantEdadesRetiro();
        break;
    case "cargos": 
        if($dest=='')
        {
            getCantCargos();
        }
        else
        {
            getCantCargosGrid();
        }
        break; 
    case "escalafones": 
        if($dest=='')
        {
            getCantEscalafones();
        }
        else
        {
            getCantEscalafonesGrid();
        }
        break;
    case "ausentismo":
        if(isset($_GET["hist"]) && $_GET["hist"]=='si') 
        {
            if($dest=='')
            {
                getCantFaltas();
            }
            else
            {
                getCantFaltasGrid();
            }
        }
        break;  
    default:
        if(isset($_GET["hist"]) && $_GET["hist"]=='si')
        {
            getCantLicenciasHist($tarea);
        }
        else
        {
            getCantLicencias($tarea);
        }    
        break;

}

function getCantLicencias($tabla)
{
    global $conn;
    
    $str = "SELECT count(*) as cantidad, MONTH(desde) as mes FROM licencias WHERE tipo_licencia=".$tabla." AND YEAR(desde)=YEAR(CURDATE()) AND goce='S' GROUP BY MONTH(desde)";
    $result = $conn->query($str);
    $meses = array(
        1 => 'ENERO',
        2 => 'FEBRERO',
        3 => 'MARZO',
        4 => 'ABRIL',
        5 => 'MAYO',
        6 => 'JUNIO',
        7 => 'JULIO',
        8 => 'AGOSTO',
        9 => 'SEPTIEMBRE',
        10 => 'OCTUBRE',
        11 => 'NOVIEMBRE',
        12 => 'DICIEMBRE',
    );
    $planta_mes = array();
    $anio_hoy = date("Y");
    for($i=1;$i<=count($meses);$i++)
    {
        if($i<10)
        {
            $mes = "0".$i;
        }
        else
        {
            $mes = $i;
        }
        $str_tot = "SELECT COUNT(*) as cantidad FROM general g LEFT JOIN bajas b ON g.legajo=b.legajo WHERE ((b.id IS NOT NULL AND b.fecha IS NOT NULL AND '".$anio_hoy."-".$mes."-30'>=g.fecha_carga AND '".$anio_hoy."-".$mes."-30'<b.fecha) OR b.id IS NULL AND '".$anio_hoy."-".$mes."-30'>=g.fecha_carga)";
        $cons_tot = $conn->query($str_tot);
        $obj_tot = $cons_tot->fetch_object();
        $planta_mes[$i]= $obj_tot->cantidad;
    }
    //var_dump($planta_mes);
    if($result)
    {
        $nbrows = $result->num_rows;
        $arr = array();
        
        while($obj = $result->fetch_object())
        {
            $obj->porc = (float) round($obj->cantidad*100/$planta_mes[$obj->mes],2);
            $obj->planta_mes = (int) $planta_mes[$obj->mes];
            $obj->cantidad = (int)($obj->cantidad);
            $obj->mes = $meses[$obj->mes];
                        
            $arr[]= $obj;
        }
        
        echo "({ total: ".$nbrows.",  results: ".json_encode($arr)."})";
    }
    else
    {
        echo "({ total: 0,  error: ".$conn->error."})";
    }
	
}

function getCantLicenciasHist($tabla)
{
    global $conn;

    $str = "SELECT count(*) as cantidad, MONTH(desde) as mes, YEAR(desde) as anio FROM licencias WHERE tipo_licencia=".$tabla." AND goce='S' GROUP BY YEAR(desde), MONTH(desde)";
    $result = $conn->query($str);
    $meses = array(
        1 => 'ENERO',
        2 => 'FEBRERO',
        3 => 'MARZO',
        4 => 'ABRIL',
        5 => 'MAYO',
        6 => 'JUNIO',
        7 => 'JULIO',
        8 => 'AGOSTO',
        9 => 'SEPTIEMBRE',
        10 => 'OCTUBRE',
        11 => 'NOVIEMBRE',
        12 => 'DICIEMBRE',
    );
    $anios = array();
    if($result)
    {
        
        $nbrows = $result->num_rows;
        $arr = array();
        $arr[0]["mes"] = 'ENERO';
        $arr[1]["mes"] = 'FEBRERO';
        $arr[2]["mes"] = 'MARZO';
        $arr[3]["mes"] = 'ABRIL';
        $arr[4]["mes"] = 'MAYO';
        $arr[5]["mes"] = 'JUNIO';
        $arr[6]["mes"] = 'JULIO';
        $arr[7]["mes"] = 'AGOSTO';
        $arr[8]["mes"] = 'SEPTIEMBRE';
        $arr[9]["mes"] = 'OCTUBRE';
        $arr[10]["mes"] = 'NOVIEMBRE';
        $arr[11]["mes"] = 'DICIEMBRE';
        $cont=0;
        $cont_anio = "";
        $cont_mes = 1;

        //Inicializo el arreglo en 0 para mostrar bien los datos en el grafico
        
        while($obj = $result->fetch_object())
        {
            for($i=0;$i<12;$i++)
            {
                if($obj->mes != 0 && $obj->anio!=0)
                {
                    $arr[$i][$obj->anio] = 0;
                }
            }
        }
        $result = $conn->query($str);
        while($obj = $result->fetch_object())
        {
            $str_tot = "SELECT COUNT(*) as cantidad FROM general g LEFT JOIN bajas b ON g.legajo=b.legajo WHERE ((b.id IS NOT NULL AND b.fecha IS NOT NULL AND '".$obj->anio."-".$obj->mes."-30'>=g.fecha_carga AND '".$obj->anio."-".$obj->mes."-30'<b.fecha) OR b.id IS NULL AND '".$obj->anio."-".$obj->mes."-30'>=g.fecha_carga)";
            $cons_tot = $conn->query($str_tot);
            $obj_tot = $cons_tot->fetch_object();
            if($obj->mes != 0 && $obj->anio!=0)
            {
                if($cont>0 && $cont_anio!=$obj->anio)
                {
                    $cont=0;
                    $cont_anio = $obj->anio;
                }
                else
                {
                    $cont_anio = $obj->anio;
                }
                $obj->cantidad = (int)($obj->cantidad);
                //$obj->mes = $meses[$obj->mes];
    
                $arr[$obj->mes-1][$obj->anio]= round($obj->cantidad*100/$obj_tot->cantidad,2);
                //arr[$cont][$cont_anio]= $obj->cantidad;
                $cont++;
            }
        }
        //var_dump($arr);
        echo "({ total: ".$nbrows.",  results: ".json_encode($arr)."})";
    }
    else
    {
        echo "({ total: 0,  error: ".$conn->error."})";
    }
	
}

function getCantTitulos()
{
    global $conn;

    $str = "SELECT count(*) as cantidad, te.titulo FROM general g LEFT JOIN titulo t ON g.titulo = t.id LEFT JOIN tipo_estudio te ON t.codestud = te.codestudio WHERE g.activa=1 AND g.titulo!='NULL' GROUP BY te.codestudio ORDER BY cantidad desc";
    $result = $conn->query($str);
    
    if($result)
    {
        $nbrows = $result->num_rows;
        $arr = array();
        $cont = 0;
        $sum_cant = 0;
        while($obj = $result->fetch_object())
        {
            //Muestro hasta 8 titulos diferentes. Los demas los agrupo a todos en categoria "Otros", sino el grafico es ilegible
            if($cont<8)
            {
                $obj->cantidad = (int)($obj->cantidad);
                $arr[$cont]["cantidad"] = $obj->cantidad;
                $arr[$cont]["titulo"]= $obj->titulo;
                $cont++;
            }
            else
            {
                $sum_cant+= (int)($obj->cantidad);
                
            }
           
        }
        if($sum_cant>0)
        {
            $arr[$cont]["cantidad"] = $sum_cant;
            $arr[$cont]["titulo"] = "OTROS";
        }
        echo "({ total: ".$nbrows.",  results: ".json_encode($arr)."})";
    }
    else
    {
        echo "({ total: 0,  error: ".$conn->error."})";
    }
	
}

function getCantTitulosGrid()
{
    global $conn;

    $str = "SELECT count(*) as cantidad, te.titulo FROM general g LEFT JOIN titulo t ON g.titulo = t.id LEFT JOIN tipo_estudio te ON t.codestud = te.codestudio WHERE g.activa=1 AND g.titulo IS NOT NULL GROUP BY te.codestudio ORDER BY cantidad desc";
    $result = $conn->query($str);
    
    if($result)
    {
        $nbrows = $result->num_rows;
        $arr = array();
        $cont = 0;
        $sum_cant = 0;
        while($obj = $result->fetch_object())
        {
            $obj->cantidad = (int)($obj->cantidad);
            $arr[$cont]["cantidad"] = $obj->cantidad;
            $arr[$cont]["titulo"]= $obj->titulo;
            $cont++;
            $sum_cant+=$obj->cantidad;
        }
        
        for($i=0;$i<count($arr);$i++)
        {
            $arr[$i]["porc"]=round($arr[$i]["cantidad"]/$sum_cant*100,2);
        }
        echo "({ total: ".$nbrows.",  results: ".json_encode($arr)."})";
    }
    else
    {
        echo "({ total: 0,  error: ".$conn->error."})";
    }
}

function getCantInteracciones()
{
    global $conn;

    $str = "SELECT count(*) as cantidad, g.nombre, g.apellido "
            . "FROM usuarios_log u LEFT JOIN general g ON u.legajo = g.legajo "
            . "WHERE g.activa=1 AND u.legajo!='0' AND u.tabla!='usuarios_log' GROUP BY u.legajo ORDER BY cantidad desc";
    $result = $conn->query($str);
    
    if($result)
    {
        $nbrows = $result->num_rows;
        $arr = array();
        $cont = 0;
        $sum_cant = 0;
        while($obj = $result->fetch_object())
        {
            //Muestro hasta 10 usuarios diferentes. Los demas los agrupo a todos en categoria "Otros", sino el grafico es ilegible
            if($cont<10)
            {
                $obj->cantidad = (int)($obj->cantidad);
                $arr[$cont]["cantidad"] = $obj->cantidad;
                $arr[$cont]["nombre"]= substr($obj->nombre,0,1).". ".$obj->apellido;
                $cont++;
            }
            else
            {
                $sum_cant+= (int)($obj->cantidad);
                
            }
           
        }
        if($sum_cant>0)
        {
            $arr[$cont]["cantidad"] = $sum_cant;
            $arr[$cont]["nombre"] = "OTROS";
        }
        echo "({ total: ".$nbrows.",  results: ".json_encode($arr)."})";
    }
    else
    {
        echo "({ total: 0,  error: ".$conn->error."})";
    }
	
}

function getCantInteraccionesGrid()
{
    global $conn;

    $str = "SELECT count(*) as cantidad, g.nombre, g.apellido FROM usuarios_log u LEFT JOIN general g ON u.legajo = g.legajo WHERE g.activa=1 AND u.legajo!='0' AND u.tabla!='usuarios_log' GROUP BY u.legajo ORDER BY cantidad desc";
    $result = $conn->query($str);
    
    if($result)
    {
        $nbrows = $result->num_rows;
        $arr = array();
        $cont = 0;
        $sum_cant = 0;
        while($obj = $result->fetch_object())
        {
            $obj->cantidad = (int)($obj->cantidad);
            $arr[$cont]["cantidad"] = $obj->cantidad;
            $arr[$cont]["nombre"]= $obj->nombre;
            $arr[$cont]["apellido"]= $obj->apellido;
            $cont++;
            $sum_cant+= (int)($obj->cantidad);
        }
        for($i=0;$i<count($arr);$i++)
        {
            $arr[$i]["porc"]=round($arr[$i]["cantidad"]/$sum_cant*100,2);
        }
        echo "({ total: ".$nbrows.",  results: ".json_encode($arr)."})";
    }
    else
    {
        echo "({ total: 0,  error: ".$conn->error."})";
    }
    
}

function getCantIngresos()
{
    global $conn;

    $str = "SELECT YEAR(fecha_carga) as anio, count(*) as cantidad FROM general GROUP BY YEAR(fecha_carga) ORDER BY fecha_carga ASC";
    $result = $conn->query($str);
    
    if($result)
    {
        $nbrows = $result->num_rows;
        $arr = array();
        $arr_2 = array();
        $arr_3 = array();
        $cont = 0;
        $cont_2 = 0;
        $cont_interv = 0;
        $sum_cant = 0;
        $cont_gral = 0;
        //Muestro hasta 15 intervalos de a�os y su cantidad de ingresos en cada intervalo
        $year_interval = ceil($nbrows/15);
        $fin_arr = ceil($nbrows/$year_interval);
        while($obj = $result->fetch_object())
        {   
            $arr_2[$cont_2]["anio"] = $obj->anio;
            $arr_2[$cont_2]["cantidad"] = (int)$obj->cantidad;
            //echo "Cont interv ".$cont_interv." - cont ".$cont." - Cont gral ".$cont_gral." - Anio ".$obj->anio;
            /*if($cont_interv<($year_interval-1))
            { 
                if($cont_interv == 0)
                {
                    $start_year = $obj->anio;
                }
                
                
                $sum_cant+= (int)$obj->cantidad;
                $cont_interv++;
            }
            else
            {
                if($cont_interv==($year_interval-1) || ($cont == ($fin_arr-1)))
                {
                    //if(($cont == ($fin_arr-1))&&($cont_gral<$nbrows))
                    //echo $obj->anio." ".$cont_interv." ".$cont_gral."<br/>";
                    $end_year = $obj->anio;
                    $arr[$cont]["anio"]= "(".$start_year." - ".$end_year.")";
                    $sum_cant+= (int)$obj->cantidad;
                    $arr[$cont]["cantidad"] = $sum_cant;
                    $cont_interv = 0;
                    $sum_cant = 0;
                    $cont++;
                }
            }

            if($cont_gral==($nbrows-1) && $cont_interv < ($year_interval-1)) //Si llega al final y queda un intervalo abierto, lo a�ado al ultimo
            {
                $end_year=" en adelante";
                $arr[$cont]["anio"]= "(".$start_year." - ".$end_year.")";
                $arr[$cont]["cantidad"] = $sum_cant;
            }
            //echo "comienza en anio ".$start_year." - Cantidad: ".$sum_cant."<br>";
            $cont_gral++; */   
            $cont_2++;
            
        
        }
        echo "({ total: ".$nbrows.",  results: ".json_encode($arr_2)."})";
    }
    else
    {
        echo "({ total: 0,  error: ".$conn->error."})";
    }
	
}

function getCantIngresosGrid()
{
    global $conn;

    $str = "SELECT YEAR(fecha_carga) as anio, count(*) as cantidad FROM general GROUP BY YEAR(fecha_carga) ORDER BY fecha_carga DESC";
    $result = $conn->query($str);
    
    if($result)
    {
        $nbrows = $result->num_rows;
        $arr = array();
        
        while($obj = $result->fetch_object())
        {   
            $obj->cantidad = (int)$obj->cantidad;
            $arr[] = $obj;  
        }
        echo "({ total: ".$nbrows.",  results: ".json_encode($arr)."})";
    }
    else
    {
        echo "({ total: 0,  error: ".$conn->error."})";
    }
    
}

//Retorna la cantidad de empleados con edades menor que 20, entre 20 y 30, entre 30 y 40, entre 40 y 50, entre 50 y 60, entre 60 y 70 y mayores que 70
function getCantEdades()
{
    global $conn;

    $str = "SELECT
            CASE WHEN (YEAR(CURDATE())-YEAR(fecnac) + IF(DATE_FORMAT(CURDATE(),'%m-%d') > DATE_FORMAT(fecnac,'%m-%d'), 0, -1) BETWEEN 0 AND 19) THEN '( - 20)' ELSE
                CASE WHEN (YEAR(CURDATE())-YEAR(fecnac) + IF(DATE_FORMAT(CURDATE(),'%m-%d') > DATE_FORMAT(fecnac,'%m-%d'), 0, -1) BETWEEN 20 AND 29) THEN '(20 - 30)' ELSE
                    CASE WHEN (YEAR(CURDATE())-YEAR(fecnac) + IF(DATE_FORMAT(CURDATE(),'%m-%d') > DATE_FORMAT(fecnac,'%m-%d'), 0, -1) BETWEEN 30 AND 39) THEN '(30 - 40)' ELSE
                        CASE WHEN (YEAR(CURDATE())-YEAR(fecnac) + IF(DATE_FORMAT(CURDATE(),'%m-%d') > DATE_FORMAT(fecnac,'%m-%d'), 0, -1) BETWEEN 40 AND 49) THEN '(40 - 50)' ELSE
                            CASE WHEN (YEAR(CURDATE())-YEAR(fecnac) + IF(DATE_FORMAT(CURDATE(),'%m-%d') > DATE_FORMAT(fecnac,'%m-%d'), 0, -1) BETWEEN 50 AND 59) THEN '(50 - 60)' ELSE
                                CASE WHEN (YEAR(CURDATE())-YEAR(fecnac) + IF(DATE_FORMAT(CURDATE(),'%m-%d') > DATE_FORMAT(fecnac,'%m-%d'), 0, -1) BETWEEN 60 AND 69) THEN '(60 - 70)' ELSE
                                    CASE WHEN (YEAR(CURDATE())-YEAR(fecnac) + IF(DATE_FORMAT(CURDATE(),'%m-%d') > DATE_FORMAT(fecnac,'%m-%d'), 0, -1) >= 70) THEN '(70 - )' END
                                END
                            END
                        END
                    END
                END
            END edad,
            COUNT(*) cantidad
            FROM general
            WHERE activa=1
            GROUP BY edad";
    $result = $conn->query($str);
    
    if($result)
    {
        $nbrows = $result->num_rows;
        $arr = array();
        while($obj = $result->fetch_object())
        {   
            $obj->cantidad = (int)($obj->cantidad);
            $arr[] = $obj;
       
        }
        echo "({ total: ".$nbrows.",  results: ".json_encode($arr)."})";
    }
    else
    {
        echo "({ total: 0,  error: ".$conn->error."})";
    }
	
}

//Retorna la cantidad de empleados dados de baja con edades menor que 20, entre 20 y 30, entre 30 y 40, entre 40 y 50, entre 50 y 60, entre 60 y 70 y mayores que 70
function getCantEdadesRetiro()
{
    global $conn;

    $str = "SELECT
            CASE WHEN (YEAR(CURDATE())-YEAR(fecnac) + IF(DATE_FORMAT(CURDATE(),'%m-%d') > DATE_FORMAT(fecnac,'%m-%d'), 0, -1) BETWEEN 0 AND 19) THEN '( - 20)' ELSE
                CASE WHEN (YEAR(CURDATE())-YEAR(fecnac) + IF(DATE_FORMAT(CURDATE(),'%m-%d') > DATE_FORMAT(fecnac,'%m-%d'), 0, -1) BETWEEN 20 AND 29) THEN '(20 - 30)' ELSE
                    CASE WHEN (YEAR(CURDATE())-YEAR(fecnac) + IF(DATE_FORMAT(CURDATE(),'%m-%d') > DATE_FORMAT(fecnac,'%m-%d'), 0, -1) BETWEEN 30 AND 39) THEN '(30 - 40)' ELSE
                        CASE WHEN (YEAR(CURDATE())-YEAR(fecnac) + IF(DATE_FORMAT(CURDATE(),'%m-%d') > DATE_FORMAT(fecnac,'%m-%d'), 0, -1) BETWEEN 40 AND 49) THEN '(40 - 50)' ELSE
                            CASE WHEN (YEAR(CURDATE())-YEAR(fecnac) + IF(DATE_FORMAT(CURDATE(),'%m-%d') > DATE_FORMAT(fecnac,'%m-%d'), 0, -1) BETWEEN 50 AND 59) THEN '(50 - 60)' ELSE
                                CASE WHEN (YEAR(CURDATE())-YEAR(fecnac) + IF(DATE_FORMAT(CURDATE(),'%m-%d') > DATE_FORMAT(fecnac,'%m-%d'), 0, -1) BETWEEN 60 AND 69) THEN '(60 - 70)' ELSE
                                    CASE WHEN (YEAR(CURDATE())-YEAR(fecnac) + IF(DATE_FORMAT(CURDATE(),'%m-%d') > DATE_FORMAT(fecnac,'%m-%d'), 0, -1) >= 70) THEN '(70 - )' END
                                END
                            END
                        END
                    END
                END
            END edad,
            COUNT(*) cantidad
            FROM general
            WHERE activa=0
            GROUP BY edad";
    $result = $conn->query($str);
    
    if($result)
    {
        $nbrows = $result->num_rows;
        $arr = array();
        while($obj = $result->fetch_object())
        {   
            $obj->cantidad = (int)($obj->cantidad);
            $arr[] = $obj;
       
        }
        echo "({ total: ".$nbrows.",  results: ".json_encode($arr)."})";
    }
    else
    {
        echo "({ total: 0,  error: ".$conn->error."})";
    }
	
}

function getCantCargos()
{
    global $conn;

    $str = "SELECT count(*) as cantidad, c.cargo FROM general g LEFT JOIN cargos c ON g.cargoactu = c.nrocargo WHERE g.activa=1 GROUP BY c.nrocargo ORDER BY cantidad DESC";
    $result = $conn->query($str);
    
    if($result)
    {
        $nbrows = $result->num_rows;
        $arr = array();
        $cont = 0;
        $sum_cant = 0;
        while($obj = $result->fetch_object())
        {
            //Muestro hasta 10 cargos diferentes. Los demas los agrupo a todos en categoria "Otros", sino el grafico es ilegible
            if($cont<11)
            {
                $obj->cantidad = (int)($obj->cantidad);
                $arr[$cont]["cantidad"] = $obj->cantidad;
                $arr[$cont]["cargo"]= $obj->cargo;
                $cont++;
            }
            else
            {
                $sum_cant+= (int)($obj->cantidad);
                
            }
           
        }
        if($sum_cant>0)
        {
            $arr[$cont]["cantidad"] = $sum_cant;
            $arr[$cont]["cargo"] = "OTROS";
        }
        echo "({ total: ".$nbrows.",  results: ".json_encode($arr)."})";
    }
    else
    {
        echo "({ total: 0,  error: ".$conn->error."})";
    }
    
}

function getCantCargosGrid()
{
    global $conn;

    $str = "SELECT count(*) as cantidad, c.cargo FROM general g LEFT JOIN cargos c ON g.cargoactu = c.nrocargo WHERE g.activa=1 GROUP BY c.nrocargo ORDER BY cantidad DESC";
    $result = $conn->query($str);
    
    if($result)
    {
        $nbrows = $result->num_rows;
        $arr = array();
        $cont = 0;
        $sum_cant = 0;
        while($obj = $result->fetch_object())
        {
            $obj->cantidad = (int)($obj->cantidad);
            $arr[$cont]["cantidad"] = $obj->cantidad;
            $arr[$cont]["cargo"]= $obj->cargo;
            $cont++;
            $sum_cant+=$obj->cantidad;
        }
        
        for($i=0;$i<count($arr);$i++)
        {
            $arr[$i]["porc"]=round($arr[$i]["cantidad"]/$sum_cant*100,2);
        }
        echo "({ total: ".$nbrows.",  results: ".json_encode($arr)."})";
    }
    else
    {
        echo "({ total: 0,  error: ".$conn->error."})";
    }
}

function getCantEscalafones()
{
    global $conn;

    $str = "SELECT g.cargoactu, c.escalafon, e.nombre FROM general g LEFT JOIN cargos c ON g.cargoactu=c.nrocargo LEFT JOIN escalafon e ON c.escalafon=e.id WHERE g.activa=1";
    $result = $conn->query($str);
    
    if($result)
    {
        $nbrows = $result->num_rows;
        $arr = array();
        $cont = 0;
        $sum_cant = 0;
        $adm = 0;
        $maestranza = 0;
        $funcionario = 0;
        $magistrado = 0;
        $minist_pub = 0;
        $minist_func = 0;
        $maestranza_nomb = "";
        $adm_nomb = "";
        $func_nomb = "";
        $minis_nomb = "";
        $minis_func_nomb = "";
        $magist_nomb = "";
        
        while($obj = $result->fetch_object())
        {
            switch($obj->escalafon)
            {
                case 1:
                    $maestranza++;
                    $maestranza_nomb = $obj->nombre;
                    break;
                case 2:
                    $adm++;
                    $adm_nomb = $obj->nombre;
                    break;
                case 3:
                    if($obj->cargoactu==21) //Funcionarios minist publico
                    {
                        $minist_func++;
                        $minis_func_nomb = "FUNCIONARIO MINISTERIO PUBLICO";
                    }
                    else
                    {
                        $funcionario++;
                        $func_nomb = $obj->nombre;
                    }
                    break;
                case 4:
                    if($obj->cargoactu==222 || $obj->cargoactu==72 || $obj->cargoactu==2 || $obj->cargoactu==3 || $obj->cargoactu==69 || $obj->cargoactu==70)
                    {
                        $minist_pub++;
                        $minis_nomb = "MAGISTRADO MINISTERIO PUBLICO";
                    }
                    else
                    {
                       $magistrado++;
                       $magist_nomb = $obj->nombre;
                    }   
                    break;         
            }
            $arr[0]["cantidad"] = $maestranza;
            $arr[0]["escalafon"] = $maestranza_nomb;
            $arr[1]["cantidad"] = $adm;
            $arr[1]["escalafon"] = $adm_nomb;
            $arr[2]["cantidad"] = $funcionario;
            $arr[2]["escalafon"] = $func_nomb;
            $arr[3]["cantidad"] = $minist_pub;
            $arr[3]["escalafon"] = $minis_nomb;
            $arr[4]["cantidad"] = $minist_func;
            $arr[4]["escalafon"] = $minis_func_nomb;
            $arr[5]["cantidad"] = $magistrado;
            $arr[5]["escalafon"] = $magist_nomb;
            
            
        }
        echo "({ total: ".$nbrows.",  results: ".json_encode($arr)."})";
    }
    else
    {
        echo "({ total: 0,  error: ".$conn->error."})";
    }
    
}

function getCantEscalafonesGrid()
{
    global $conn;

    $str = "SELECT g.cargoactu, c.escalafon, e.nombre FROM general g LEFT JOIN cargos c ON g.cargoactu=c.nrocargo LEFT JOIN escalafon e ON c.escalafon=e.id WHERE g.activa=1";
    $result = $conn->query($str);
    
    if($result)
    {
        $nbrows = $result->num_rows;
        $arr = array();
        $cont = 0;
        $adm = 0;
        $maestranza = 0;
        $funcionario = 0;
        $magistrado = 0;
        $minist_pub = 0;
        $minist_func = 0;
        $maestranza_nomb = "";
        $adm_nomb = "";
        $func_nomb = "";
        $minis_nomb = "";
        $minis_func_nomb = "";
        $magist_nomb = "";
        
        while($obj = $result->fetch_object())
        {
            switch($obj->escalafon)
            {
                case 1:
                    $maestranza++;
                    $maestranza_nomb = $obj->nombre;
                    break;
                case 2:
                    $adm++;
                    $adm_nomb = $obj->nombre;
                    break;
                case 3:
                    if($obj->cargoactu==21) //Funcionarios minist publico
                    {
                        $minist_func++;
                        $minis_func_nomb = "FUNCIONARIO MINISTERIO PUBLICO";
                    }
                    else
                    {
                        $funcionario++;
                        $func_nomb = $obj->nombre;
                    }
                    break;
                case 4:
                    if($obj->cargoactu==222 || $obj->cargoactu==72 || $obj->cargoactu==2 || $obj->cargoactu==3 || $obj->cargoactu==69 || $obj->cargoactu==70)
                    {
                        $minist_pub++;
                        $minis_nomb = "MAGISTRADO MINISTERIO PUBLICO";
                    }
                    else
                    {
                        
                        $magistrado++;
                        $magist_nomb = $obj->nombre;
                    }   
                    break;         
            }
            $arr[0]["cantidad"] = $maestranza;
            $arr[0]["escalafon"] = $maestranza_nomb;
            $arr[1]["cantidad"] = $adm;
            $arr[1]["escalafon"] = $adm_nomb;
            $arr[2]["cantidad"] = $funcionario;
            $arr[2]["escalafon"] = $func_nomb;
            $arr[3]["cantidad"] = $minist_pub;
            $arr[3]["escalafon"] = $minis_nomb;
            $arr[4]["cantidad"] = $minist_func;
            $arr[4]["escalafon"] = $minis_func_nomb;
            $arr[5]["cantidad"] = $magistrado;
            $arr[5]["escalafon"] = $magist_nomb;
            
            
        }
        for($i=0;$i<count($arr);$i++)
        {
            $arr[$i]["porc"]=round($arr[$i]["cantidad"]/$nbrows*100,2);
        }

        echo "({ total: ".$nbrows.",  results: ".json_encode($arr)."})";
    }
    else
    {
        echo "({ total: 0,  error: ".$conn->error."})";
    }
}

function getCantFaltas()
{
    global $conn;

    $tablas_licencias = ["lic_salud", "lic_salud_lt", "lic_salud_fam", "lic_salud_at", "lic_maternidad", "lic_fallecimiento", "lic_comp", "lic_culturales", "lic_gremiales", "lic_deportivas", "lic_electivas", "lic_estudio", "lic_extraordinaria", "lic_matrimonio", "lic_nacimiento", "lic_particular", "lic_singoce", "lic_doble_turno"];
    $nombres_licencias = ["Salud personal", "Salud largo tratamiento", "Salud familiar", "Salud accidente de trabajo", "Maternidad", "Fallecimiento", "Compensatoria", "Cultural", "Gremial", "Deportiva", "Electiva", "Estudio", "Extrordinaria", "Matrimonio", "Nacimiento", "Particular", "Sin goce", "Doble turno"];
    $arr = array();
    $cont=0;
    for($i=0;$i<count($tablas_licencias);$i++)
    {
        $str = "SELECT count(*) as cantidad, YEAR(desde) as anio FROM ".$tablas_licencias[$i]." WHERE goce='S' GROUP BY YEAR(desde)";
        $result = $conn->query($str);
        
        $anios = array();
        if($result)
        {
            
            while($obj = $result->fetch_object())
            {
                $arr[$cont]["licencia"] = $nombres_licencias[$i];
                $arr[$cont]["cantidad"] = $obj->cantidad;
                $arr[$cont]["anio"] = $obj->anio;
                $cont++;
                
            }
            
        }
    }
    echo "({results: ".json_encode($arr)."})";
    
}

function getCantFaltasGrid()
{
    global $conn;

    $str = "SELECT g.cargoactu, c.escalafon, e.nombre FROM general g LEFT JOIN cargos c ON g.cargoactu=c.nrocargo LEFT JOIN escalafon e ON c.escalafon=e.id WHERE g.activa=1";
    $result = $conn->query($str);
    
    if($result)
    {
        $nbrows = $result->num_rows;
        $arr = array();
        $cont = 0;
        $adm = 0;
        $maestranza = 0;
        $funcionario = 0;
        $magistrado = 0;
        $minist_pub = 0;
        $minist_func = 0;
        $maestranza_nomb = "";
        $adm_nomb = "";
        $func_nomb = "";
        $minis_nomb = "";
        $minis_func_nomb = "";
        $magist_nomb = "";
        
        while($obj = $result->fetch_object())
        {
            switch($obj->escalafon)
            {
                case 1:
                    $maestranza++;
                    $maestranza_nomb = $obj->nombre;
                    break;
                case 2:
                    $adm++;
                    $adm_nomb = $obj->nombre;
                    break;
                case 3:
                    if($obj->cargoactu==21) //Funcionarios minist publico
                    {
                        $minist_func++;
                        $minis_func_nomb = "FUNCIONARIO MINISTERIO PUBLICO";
                    }
                    else
                    {
                        $funcionario++;
                        $func_nomb = $obj->nombre;
                    }
                    break;
                case 4:
                    if($obj->cargoactu==222 || $obj->cargoactu==72 || $obj->cargoactu==2 || $obj->cargoactu==3 || $obj->cargoactu==69 || $obj->cargoactu==70)
                    {
                        $minist_pub++;
                        $minis_nomb = "MAGISTRADO MINISTERIO PUBLICO";
                    }
                    else
                    {
                        
                        $magistrado++;
                        $magist_nomb = $obj->nombre;
                    }   
                    break;         
            }
            $arr[0]["cantidad"] = $maestranza;
            $arr[0]["escalafon"] = $maestranza_nomb;
            $arr[1]["cantidad"] = $adm;
            $arr[1]["escalafon"] = $adm_nomb;
            $arr[2]["cantidad"] = $funcionario;
            $arr[2]["escalafon"] = $func_nomb;
            $arr[3]["cantidad"] = $minist_pub;
            $arr[3]["escalafon"] = $minis_nomb;
            $arr[4]["cantidad"] = $minist_func;
            $arr[4]["escalafon"] = $minis_func_nomb;
            $arr[5]["cantidad"] = $magistrado;
            $arr[5]["escalafon"] = $magist_nomb;
            
            
        }
        for($i=0;$i<count($arr);$i++)
        {
            $arr[$i]["porc"]=round($arr[$i]["cantidad"]/$nbrows*100,2);
        }

        echo "({ total: ".$nbrows.",  results: ".json_encode($arr)."})";
    }
    else
    {
        echo "({ total: 0,  error: ".$conn->error."})";
    }
}
?>