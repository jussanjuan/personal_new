<?php
session_set_cookie_params(0);
session_start();
require('system.config.php');
require('proxy.feriados.php');
$tarea = $_SERVER['REQUEST_METHOD'];


switch ($tarea) 
{
	case 'GET':
        $act = isset($_GET["act"])?$_GET["act"]:"";
        switch($act)
        {
            case "":
                getAllPersonal(); //Obtener la totalidad de las personas 
                break;
            case "getBancos":
                getBancos();
                break;
            case "getSucursales":
                getSucursales();
                break;    
            case "getCuenta":
                getCuenta();
                break;          
            case "getTipoCuentas":
                getTipoCuentas();
                break;
            case "getBeneficiarios":
                getBeneficiarios();
                break;        
        }
        break;
	case 'POST':
        $act = isset($_POST["act"])?$_POST["act"]:"";
        switch($act)
        {
            case "": 
            case "saveCuenta": //Guardar cuenta
                saveCuenta();
                break;
            case "deleteBeneficiario": //Elimina beneficiario
                deleteBeneficiario();
                break;
            case "altaBeneficiario":
                altaBeneficiario();
                break;
            case "updateBeneficiario":
                updateBeneficiario();
                break;    

        }
        break;
	case 'PUT':
		break;
	case 'DELETE':
		break;
	default:
		echo "({ failure: 'Error por default'})";
		break;
}


function getAllPersonal() 
{
    global $conn;
    $data = array();
    if (isset($_GET["query"])) //Se ingresaron caracteres para buscar
    {
        //Parseo la variable $_GET["fields"], que trae los filtros de busqueda. Mauro
        if($_GET["fields"]!=NULL) //Si se selecciono al menos un filtro de busqueda
        {
            $cad = str_replace("[", "", $_GET["fields"]);
            $cad = str_replace("]", "", $cad);
            $cad = str_replace("\"", "", $cad);
            $cad = explode(",", $cad);
            $str_filt = "";
            for($i=0;$i<count($cad);$i++)
            {
                if($cad[$i]!="evaluado")
                {
                    $str_filt.= "general.".$cad[$i]." LIKE '%".$_GET["query"]."%'";
                    if($i!=(count($cad)-1))
                        $str_filt.=" OR ";
                }
            }
            $str_cue = "SELECT general.apellido, general.nombre, general.legajo, general.padron, general.nrodoc FROM general WHERE (".$str_filt.") ORDER BY general.apellido ASC";
            //echo $str_cue;    
        }
        else //Si se destiladaron todos los filtros de busqueda, busco todo, para evitar errores
        {
            $str_cue = "SELECT general.apellido, general.nombre, general.legajo, general.padron, general.nrodoc FROM general ORDER BY general.apellido ASC";
        }
    }
    else
    {
        $str_cue = "SELECT general.apellido, general.nombre, general.legajo, general.padron, general.nrodoc FROM general ORDER BY general.apellido ASC";
    }
    $result = $conn->query($str_cue);
    $nbrows = $result->num_rows;
    $arr = array();
    //echo $str_cue;
    while($obj = $result->fetch_object())
    {
        /*//Aca controlo si el empleado ha sido evaluado o no 
        $hoy = date("Y-m-d"); 
        $str_ev = "SELECT legajo_evaluado FROM eval_evaluacion_persona WHERE legajo_evaluado=".$obj->legajo." AND id_evaluacion IN (SELECT id FROM eval_evaluacion WHERE '".$hoy."' BETWEEN fecha_inicio AND fecha_fin)";
        $res_ev = $conn->query($str_ev);
        
        if($res_ev->num_rows>0)
        {
            $obj->evaluado = "si";
        }
        else 
        {
            $obj->evaluado = "no";
        }*/
        $arr[] = $obj;
    }
    echo "({ total: ".$nbrows.",  results: " . json_encode($arr) . "})";
        
}

function getBancos() 
{
    global $conn;
    $data = array();
    $str_cue = "SELECT * FROM cont_banco ORDER BY nombre ASC";
    
    $result = $conn->query($str_cue);
    $nbrows = $result->num_rows;
    $arr = array();
    //echo $str_cue;
    while($obj = $result->fetch_object())
    {
        $arr[] = $obj;
    }
    echo "({ total: ".$nbrows.",  results: " . json_encode($arr) . "})";
        
}

function getSucursales() 
{
    global $conn;
    $data = array();
    $str_cue = "SELECT * FROM cont_sucursal WHERE id_banco=".$_GET["idBanco"]." ORDER BY nombre ASC";
    //echo $str_cue;
    $result = $conn->query($str_cue);
    $nbrows = $result->num_rows;
    $arr = array();
    //echo $str_cue;
    if($nbrows>0)
    {
        while($obj = $result->fetch_object())
        {
            $obj->nombre = $obj->nombre." - ".$obj->numero;
            $arr[] = $obj;
        }
    }
    echo "({ total: ".$nbrows.",  results: " . json_encode($arr) . "})";
        
}

function getCuenta()
{
    global $conn;
    $data = array();
    $str_cue = "SELECT * FROM cont_cuenta, cont_sucursal WHERE legajo=".$_GET["legajo"]." AND cont_cuenta.id_sucursal=cont_sucursal.id";
    
    $result = $conn->query($str_cue);
    $nbrows = $result->num_rows;
    $arr = array();
    //echo $str_cue;
    while($obj = $result->fetch_object())
    {
        $arr[] = $obj;
    }
    echo "({ total: ".$nbrows.",  results: " . json_encode($arr) . "})";
}

function getTipoCuentas() 
{
    global $conn;
    $data = array();
    $str_cue = "SELECT * FROM cont_tipocuenta";
    //echo $str_cue;
    $result = $conn->query($str_cue);
    $nbrows = $result->num_rows;
    $arr = array();
    //echo $str_cue;
    if($nbrows>0)
    {
        while($obj = $result->fetch_object())
        {
            $arr[] = $obj;
        }
    }
    echo "({ total: ".$nbrows.",  results: " . json_encode($arr) . "})";
        
}

function getBeneficiarios()
{
    global $conn;
    $data = array();
    $str_cue = "SELECT cont_beneficiarios_usuras.id, cont_beneficiarios_usuras.apellido as apellido_beneficiario, cont_beneficiarios_usuras.nombre as nombre_beneficiario, cont_beneficiarios_usuras.dni, cont_beneficiarios_usuras.sucursal_acred, cont_beneficiarios_usuras.cuenta_acred, general.nombre, general.apellido, cont_beneficiarios_usuras.importe FROM cont_beneficiarios_usuras, general WHERE cont_beneficiarios_usuras.legajo_benefactor=general.legajo ORDER BY cont_beneficiarios_usuras.apellido";
    
    $result = $conn->query($str_cue);
    $nbrows = $result->num_rows;
    $arr = array();
    //echo $str_cue;
    while($obj = $result->fetch_object())
    {
        $obj->benefactor = $obj->apellido.", ".$obj->nombre;
        $arr[] = $obj;
    }
    echo "({ total: ".$nbrows.",  results: " . json_encode($arr) . "})";
}

function saveCuenta()
{
    global $conn;
    $data = array();
    $str_cons_cue = "SELECT * FROM cont_cuenta WHERE legajo=".$_POST["legajo"];
    //echo $str_cons_cue;
    $res_cons_cue = $conn->query($str_cons_cue);
    if($res_cons_cue->num_rows>0)
    {
        $str_upd = "UPDATE cont_cuenta SET cuenta='".$_POST["numero_cuenta"]."', cbu='".$_POST["cbu"]."', id_sucursal=".$_POST["sucursal"].", tipo_cuenta=".$_POST["tipo_cuenta"]." WHERE legajo=".$_POST["legajo"];
        $cons_upd = $conn->query($str_upd);
        if($cons_upd)
        {
            $data["success"] = true;
            $data["msg"] = "Cuenta grabada al empleado correctamente.";
            
        }
        else
        {
            $data["success"] = false;
            $data["error"] = $conn->error;    
        }
    }
    else
    {
        $str_ins = "INSERT INTO cont_cuenta (cuenta, cbu, id_sucursal, tipo_cuenta, legajo) VALUES ('".$_POST["numero_cuenta"]."', '".$_POST["cbu"]."', ".$_POST["sucursal"].", ".$_POST["tipo_cuenta"].", ".$_POST["legajo"].")";
        $cons_ins = $conn->query($str_ins);
        if($cons_ins)
        {
            $data["success"] = true;
            $data["msg"] = "Cuenta creada y grabada al empleado correctamente.";
        }
        else
        {
            $data["success"] = false;
            $data["error"] = $conn->error;    
        }
    }
    echo json_encode($data);
}

function deleteBeneficiario() 
{
    global $conn;
    //echo $tarea;
    
    $data = array();
    
    $str_del = "DELETE FROM cont_beneficiarios_usuras WHERE id=".$_POST["idBenef"];
    if($conn->query($str_del))
    {
        $data["success"] = true;
        $data["msg"] = "El beneficiario fue eliminado";

        // Escribir log
        $leg = isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
        $str_limpia = addslashes($str_del);
        $str_log = "INSERT INTO usuarios_log (legajo, tabla, log, fecha) VALUES (".$leg.", 'cont_beneficiarios_usuras', '".$str_limpia."', '".date("Y-m-d H:i:s")."')";
        $res_log = $conn->query($str_log);
    }
    else
    {
        $data["success"] = false;
        $data["error"]= "Error al eliminar. Erro de base de datos: ".$conn->error;
    }
    
    echo json_encode($data);
}

function altaBeneficiario()
{
    global $conn;
    $data = array();
    $str_cons = "SELECT * FROM cont_beneficiarios_usuras WHERE dni=".$_POST["dni_beneficiario"];
    $res_cons = $conn->query($str_cons);
    if($res_cons->num_rows>0)
    {
        $data["success"] = false;
        $data["error"] = "Error al cargar. Ya existe un beneficiario con el DNI ingresado.";
    }
    else
    {
        $str_leg = "SELECT legajo FROM general WHERE id=".$_POST["benefactor"];
        $res_leg = $conn->query($str_leg);
        $obj_leg = $res_leg->fetch_object();
        $legajo = $obj_leg->legajo;
        $cuenta = str_pad($_POST["cuenta_beneficiario"],8,"0",STR_PAD_LEFT);
        $importe = ($_POST["importe_beneficiario"]=="")?0:$_POST["importe_beneficiario"];
        $str_ins = "INSERT INTO cont_beneficiarios_usuras (apellido, nombre, dni, sucursal_acred, cuenta_acred, tipo_cuenta, importe, legajo_benefactor) VALUES (UPPER('".$_POST["apellido_beneficiario"]."'), UPPER('".$_POST["nombre_beneficiario"]."'), ".$_POST["dni_beneficiario"].", ".$_POST["sucursal_beneficiario"].", '".$cuenta."', ".$_POST["tipo_cuenta_beneficiario"].", ".$importe.", ".$legajo.")";
        $res_ins = $conn->query($str_ins);
        if($res_ins)
        {
            $data["success"] = true;
            $data["msg"] = "Beneficiario cargado correctamente!";
        }
        else
        {
            $data["success"] = false;
            $data["error"] = "Error al cargar. Error en la base de datos: ".$str_ins;    
        }
    }
    echo json_encode($data);
}

function updateBeneficiario()
{
    global $conn;
    $registros = json_decode(stripslashes($_POST["records_add"])); 
    $data = array();
    $data["cambios"] = 0;
    
    foreach($registros as $registro)
    {
        
        $conn->query('SET NAMES UTF8');
        
        $str_upd = "UPDATE cont_beneficiarios_usuras SET apellido=UPPER('".$registro->apellido_beneficiario."'), nombre=UPPER('".$registro->nombre_beneficiario."'), dni=".$registro->dni.", cuenta_acred='".$registro->cuenta_acred."', importe=".$registro->importe." WHERE id=".$registro->id;
        
        if($conn->query($str_upd))
        {
            $data["success"] = true;
            $data["cambios"]+= $conn->affected_rows;
        }
        else
        {
            $data["success"] = false;
            $data["error"] = $conn->error;
        }
    }    
    echo json_encode($data);
}

