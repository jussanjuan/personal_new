<?php
session_set_cookie_params(0);
session_start();
require('system.config.php');
require('proxy.feriados.php');

$tarea = $_SERVER['REQUEST_METHOD'];


$show_evaluados = isset($_GET["show"])?$_GET["show"]:"";//Mostrar solo evaluados, no evaluados o todos 
$str_show = "";
switch($show_evaluados)
{
    case "":
        break;
    case "mostrar_todos":
        $str_show = "";
        break;
    case "mostrar_no_evaluados":
        $str_show = " AND general.legajo not in (select legajo_evaluado from eval_evaluacion_persona)";
        break;
    case "mostrar_evaluados":
        $str_show = " AND general.legajo in (select legajo_evaluado from eval_evaluacion_persona)";
        break;
}
//Si el usuario logueado corresponde a recursos humanos, Mauro Avendaño, Marcela Durcak (RRHH) o Laura Garcia (RRHH), tienen permisos para ver todos los evaluados
$perm_RRHH = "no";
$cons_lugar = "SELECT lugaractu FROM general WHERE legajo=".$_SESSION["legajo"];
$res_lugar = $conn->query($cons_lugar);
if($res_lugar->num_rows>0)
{
    $obj_lugar = $res_lugar->fetch_object();
    if($obj_lugar->lugaractu==167 || $_SESSION["legajo"]==1982 || $_SESSION["legajo"]==1877 || $_SESSION["legajo"]==2067 || $_SESSION["legajo"]==2066)
    {
        $perm_RRHH = "si";
    }
}
      
switch ($tarea) 
{
	case 'GET':
        $act = isset($_GET["act"])?$_GET["act"]:"";
        switch($act)
        {
            case "":
                if($perm_RRHH == "no")
                {
                    getPersonalAEvaluar($str_show); //Obtener todas las personas a evaluar por el usuario logueado
                }
                else
                {
                    getAllPersonalAEvaluar($str_show); //Obtener la totalidad de las personas a evaluar (permisos para RRHH)
                }
                break;
            case "getEval":
                getEvaluacion();
                break;
            case "getEvalItemsForm": //Obtengo todos los items para generar dinamicamente el formulario dinamico de evaluacion
                getEvalItemsForm();
                break;    
            case "confEvaluadores":
                getEvaluadorEvaluado($str_show);
                break;
            case "getEvaluador":
                getEvaluador();
                break;
            case "getEvaluados":
                getEvaluados();
                break;
            case "isOffspring":
                isOffspring();
                break;    
            case "getMisEvaluaciones"://Obtener las evaluaciones efectuadas a un empleado
                getMisEvaluaciones(); 
                break;
            case "getAllEvaluaciones"://Obtener todas las evaluaciones
                getAllEvaluaciones(); 
                break;
            case "getItems"://Obtener todos los items de una evaluacion
                getItems(); 
                break; 
            case "getAllItems"://Obtener todos los items existentes
                getAllItems(); 
                break;   
            case "autoAsignarEvaluadores"://Obtener todos los items existentes
                autoAsignarEvaluadores(); 
                break;   
            case "cargarEvaluacionEvaluado":
                cargarEvaluacionEvaluado(); 
                break;                 
        }
        break;
	case 'POST':
        $act = isset($_POST["act"])?$_POST["act"]:"";
        switch($act)
        {
            case "": //Guardar evaluacion de desempeño
            case "evaluar": //Guardar evaluacion de desempeño
                setEvaluacion();
                break;
            case "setEvaluados": //Guardar los cambios en los evaluados de cierto agente
                setEvaluados();
                break;
            case "setEvaluador"://Guardar los cambios en el evaluador de cierto agente
                setEvaluador();
                break;
            case "setItems"://Guardar los cambios en los items generales (sin importar a que evaluacion pertenecen)
                setItems();
                break;   
            case "setItemsEvaluacion"://Guardar los cambios en los items asignados a una evaluacion especifica
                setItemsEvaluacion();
                break;  
            case "newEvaluacion":   
                newEvaluacion();
                break;    
            case "updateFactoresAscenso"://Actualiza todos los factores de ascenso de todos los empleados para un periodo dado
                updateFactoresAscenso();
                break;    
        }
        break;
	case 'PUT':
		break;
	case 'DELETE':
		break;
	default:
		echo "({ failure: 'Error por default'})";
		break;
}


function getPersonalAEvaluar($str_show) 
{
    global $conn;
    $data = array();
    
    if (isset($_GET["query"])) //Se ingresaron caracteres para buscar
    {
        //Parseo la variable $_GET["fields"], que trae los filtros de busqueda. Mauro
        if($_GET["fields"]!=NULL) //Si se selecciono al menos un filtro de busqueda
        {
            $cad = str_replace("[", "", $_GET["fields"]);
            $cad = str_replace("]", "", $cad);
            $cad = str_replace("\"", "", $cad);
            $cad = explode(",", $cad);
            $str_filt = "";
            for($i=0;$i<count($cad);$i++)
            {
                if($cad[$i]!="evaluado")
                {
                    $str_filt.= "general.".$cad[$i]." LIKE '%".$_GET["query"]."%'";
                    if($i!=(count($cad)-2))
                        $str_filt.=" OR ";
                }
            }
            $str_eva = "SELECT general.apellido, general.nombre, general.legajo FROM general, eval_evaluadores WHERE activa=1 AND general.legajo=eval_evaluadores.legajo_evaluado AND eval_evaluadores.legajo_evaluador=".$_SESSION["legajo"]." AND (".$str_filt.")".$str_show." ORDER BY general.apellido ASC";    
        }
        else //Si se destiladaron todos los filtros de busqueda, busco todo, para evitar errores
        {
            $str_eva = "SELECT general.apellido, general.nombre, general.legajo FROM general, eval_evaluadores WHERE activa=1 AND general.legajo=eval_evaluadores.legajo_evaluado AND eval_evaluadores.legajo_evaluador=".$_SESSION["legajo"].$str_show." ORDER BY general.apellido ASC";
        }
    }
    else
    {
        $str_eva = "SELECT general.apellido, general.nombre, general.legajo FROM general, eval_evaluadores WHERE activa=1 AND general.legajo=eval_evaluadores.legajo_evaluado AND eval_evaluadores.legajo_evaluador=".$_SESSION["legajo"].$str_show." ORDER BY general.apellido ASC";
    }
    $result = $conn->query($str_eva);
    $nbrows = $result->num_rows;
    $arr = array();
    
    while($obj = $result->fetch_object())
    {
        //Aca controlo si el empleado ha sido evaluado o no
        $hoy = date("Y-m-d");  
        $str_ev = "SELECT legajo_evaluado FROM eval_evaluacion_persona WHERE legajo_evaluado=".$obj->legajo." AND id_evaluacion IN (SELECT id FROM eval_evaluacion WHERE '".$hoy."' BETWEEN fecha_inicio AND fecha_fin)";
        $res_ev = $conn->query($str_ev);

        if($res_ev->num_rows>0)
        {
            $obj->evaluado = "si";
        }
        else 
        {
            $obj->evaluado = "no";
        }
        $arr[] = $obj;
    }
    echo "({ total: ".$nbrows.",  results: " . json_encode($arr) . "})";
        
}

function getAllPersonalAEvaluar($str_show) 
{
    global $conn;
    $data = array();
    if (isset($_GET["query"])) //Se ingresaron caracteres para buscar
    {
        //Parseo la variable $_GET["fields"], que trae los filtros de busqueda. Mauro
        if($_GET["fields"]!=NULL) //Si se selecciono al menos un filtro de busqueda
        {
            $cad = str_replace("[", "", $_GET["fields"]);
            $cad = str_replace("]", "", $cad);
            $cad = str_replace("\"", "", $cad);
            $cad = explode(",", $cad);
            $str_filt = "";
            for($i=0;$i<count($cad);$i++)
            {
                if($cad[$i]!="evaluado")
                {
                    $str_filt.= "general.".$cad[$i]." LIKE '%".$_GET["query"]."%'";
                    if($i!=(count($cad)-2))
                        $str_filt.=" OR ";
                }
            }
            $str_eva = "SELECT general.apellido, general.nombre, general.legajo FROM general, eval_evaluadores WHERE activa=1 AND general.legajo=eval_evaluadores.legajo_evaluado AND (".$str_filt.")".$str_show." ORDER BY general.apellido ASC";    
        }
        else //Si se destiladaron todos los filtros de busqueda, busco todo, para evitar errores
        {
            $str_eva = "SELECT general.apellido, general.nombre, general.legajo FROM general, eval_evaluadores WHERE activa=1 AND general.legajo=eval_evaluadores.legajo_evaluado ".$str_show." ORDER BY general.apellido ASC";
        }
    }
    else
    {
        $str_eva = "SELECT general.apellido, general.nombre, general.legajo FROM general, eval_evaluadores WHERE activa=1 AND general.legajo=eval_evaluadores.legajo_evaluado ".$str_show." ORDER BY general.apellido ASC";
    }
    $result = $conn->query($str_eva);
    $nbrows = $result->num_rows;
    $arr = array();
    //echo $str_eva;
    while($obj = $result->fetch_object())
    {
        //Aca controlo si el empleado ha sido evaluado o no 
        $hoy = date("Y-m-d"); 
        $str_ev = "SELECT legajo_evaluado FROM eval_evaluacion_persona WHERE legajo_evaluado=".$obj->legajo." AND id_evaluacion IN (SELECT id FROM eval_evaluacion WHERE '".$hoy."' BETWEEN fecha_inicio AND fecha_fin)";
        $res_ev = $conn->query($str_ev);
        
        if($res_ev->num_rows>0)
        {
            $obj->evaluado = "si";
        }
        else 
        {
            $obj->evaluado = "no";
        }
        $arr[] = $obj;
    }
    echo "({ total: ".$nbrows.",  results: " . json_encode($arr) . "})";
        
}

function getEvaluacion() 
{
    global $conn;
    $data = array();
    $hoy = date("Y-m-d");
    $str_eva = "SELECT * FROM eval_item_evaluacion_persona, eval_evaluacion_persona WHERE eval_item_evaluacion_persona.id_evaluacion_pers=eval_evaluacion_persona.id AND eval_evaluacion_persona.legajo_evaluado=".$_GET["leg"]." AND eval_evaluacion_persona.id_evaluacion IN (SELECT id FROM eval_evaluacion WHERE '".$hoy."' BETWEEN fecha_inicio AND fecha_fin)";
    $result = $conn->query($str_eva);
    $nbrows = $result->num_rows;
    $arr = array();
    while($obj = $result->fetch_object())
    {
        $arr[] = $obj;
    }
    echo "({ total: ".$nbrows.",  results: " . json_encode($arr) . "})";
        
}

function getEvalItemsForm()
{
    global $conn;
    $data = array();
    $hoy = date("Y-m-d");
    $arr = array();
    $strEvaItems = "SELECT eval_item.id, eval_item.nombre, eval_item.descripcion FROM eval_item, eval_evaluacion_item, eval_evaluacion WHERE eval_item.id=eval_evaluacion_item.id_item AND eval_evaluacion_item.id_evaluacion=eval_evaluacion.id AND '".$hoy."' BETWEEN eval_evaluacion.fecha_inicio AND eval_evaluacion.fecha_fin ORDER BY eval_item.id";
    $resEvaItems = $conn->query($strEvaItems);
    if($resEvaItems)
    {
        while($obj = $resEvaItems->fetch_object())
        {
            $arr[]=$obj;
        }
        
        $data["success"] = true;
        $data["results"] = $arr;
    }
    else
    {
        $data["success"] = false;
        $data["results"] = 'Error al obtener los items de la evaluaci&oacute;n';
    }
    echo json_encode($data);
}

function setEvaluacion()
{
    global $conn;
    $data = array();
    $campos = array();
    $hoy = date("Y-m-d");
    $cant_campos = 0;
    $sum_notas = 0;
    $cant_aplazos = 0;
    //var_dump($_POST);
    while ($campo = current($_POST)) 
    {
        if(key($_POST)!='evaluador_entry' && key($_POST)!='evaluado' && key($_POST)!='observaciones')
        {
            $campos[]=$campo;
            $cant_campos++;
            $sum_notas+=$campo;
            if($campo<4)
            {
                $cant_aplazos++;
            }
        }
        next($_POST);
    }
    reset($_POST);
    
    //Obtengo el id de la evaluacion vigente
    $str_eva_vigente = "SELECT * FROM eval_evaluacion WHERE '".$hoy."' BETWEEN fecha_inicio AND fecha_fin";
    $res_eva_vigente = $conn->query($str_eva_vigente);
    $row_eva_vigente = $res_eva_vigente->fetch_object();
    $id_ev = $row_eva_vigente->id;


    //Obtengo el evaluador, dado que puede no estar cargando el evaluador la evaluacion, sino el data entry
    $str_evaluador = "SELECT * FROM eval_evaluadores WHERE legajo_evaluado=".$_POST["evaluado"];
    $cons_evaluador = $conn->query($str_evaluador);
    if(!$cons_evaluador || $cons_evaluador->num_rows==0)
    {
        $data["success"] = false;
        $data["error"] = "No se encontro evaluador asignado para la persona. Cargue evaluador e intente nuevamente";
    }
    else
    {
        $row_evaluador = $cons_evaluador->fetch_object();
        $leg_evaluador = $row_evaluador->legajo_evaluador;
        //Hago un query para consultar si existe una evaluacion hecha, que quiere decir que se esta editando la evaluacion y no cargando una nueva
        $str_cons_edit = "SELECT eval_evaluacion_persona.*, eval_evaluacion_persona.id as eval_pers_id FROM eval_evaluacion_persona, eval_evaluacion WHERE legajo_evaluador=".$_POST["evaluador_entry"]." AND legajo_evaluado=".$_POST["evaluado"]." AND id_evaluacion=eval_evaluacion.id AND '".$hoy."' BETWEEN eval_evaluacion.fecha_inicio AND eval_evaluacion.fecha_fin";
        //echo $str_cons_edit;
        $res_cons_edit = $conn->query($str_cons_edit);
        if($res_cons_edit->num_rows==0) //****************NUEVA****************
        {
            $campoForm = "item_";
            $str_set_eva_item = "INSERT INTO eval_item_evaluacion_persona (id_item, id_evaluacion_pers, nota_item) VALUES ";
            
            $prom = round($sum_notas/$cant_campos,2);
            
            $str_set_eva= "INSERT INTO eval_evaluacion_persona (id_evaluacion, legajo_evaluador, legajo_evaluado, legajo_dataentry, fecha_carga, nota_promedio, cant_aplazos, observaciones) VALUES "
                    . "(".$id_ev.", ".$leg_evaluador.", ".$_POST["evaluado"].", ".$_POST["evaluador_entry"].", '".date("Y-m-d H:i:s")."', ".$prom.", ".$cant_aplazos.", '".$_POST["observaciones"]."')";
            
            $res_set_eva = $conn->query($str_set_eva);
            
            if($res_set_eva)
            {
                $idEvaPers = $conn->insert_id;
                $i=0;
                while ($campo = current($_POST)) 
                {
                    if(key($_POST)!='evaluador_entry' && key($_POST)!='evaluado' && key($_POST)!='observaciones')
                    {
                        $arr_exp = explode('_', key($_POST));
                        $idItem = $arr_exp[1];
                        
                        $str_set_eva_item.= "(".$idItem.", ".$idEvaPers.", ".$campo.")";
                        if($i!=($cant_campos-1))
                        {
                            $str_set_eva_item.=", ";
                        }
                        $i++;
                    }
                    next($_POST);
                }
                
                //echo $str_set_eva_item;
                $res_set_eva_item = $conn->query($str_set_eva_item);
                if($res_set_eva_item)
                {
                    $data["success"] = true;
                    $data["msg"] = "Se guard&oacute; exitosamente la evaluacion de desempe&ntilde;o";
                    // Escribir log
                    $leg = isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
                    
                    escribir_log("personal", $leg, $_POST["evaluado"], "eval_item_evaluacion_persona", 1, $str_set_eva_item, date("Y-m-d H:i:s"));
                    escribir_log("personal", $leg, $_POST["evaluado"], "eval_evaluacion_persona", 1, $str_set_eva, date("Y-m-d H:i:s"));
                    /*//Inserto el registro en la tabla eval_factores
                    $str_ins_fact = "INSERT INTO eval_factores (legajo, id_evaluacion) VALUES (".$_POST["evaluado"].", ".$id_ev.")";
                    $cons_ins_fact = $conn->query($str_ins_fact);

                    //Calculo los demas factores (tardanzas, inasistencias, licencias, etc)
                    actualizar_tardanzas($_POST["evaluado"], $id_ev);
                    calcular_licencias($_POST["evaluado"], $id_ev);
                    calcular_asist_puntualidad($_POST["evaluado"], $id_ev);
                    calcular_antiguedad_cargo($_POST["evaluado"], $id_ev);
                    calcular_antiguedad_escalafon($_POST["evaluado"], $id_ev);
                    calcular_capacitaciones($_POST["evaluado"], $id_ev);
                    calcular_sanciones($_POST["evaluado"], $id_ev);
                    calcular_puntaje_total($_POST["evaluado"], $id_ev);*/
                }
                else
                {
                    $data["success"] = false;
                    $data["error"] = $conn->error;
                }
            }
            else 
            {
                $data["success"] = false;
                $data["error"] = $conn->error;
            }
        }
        else //****************EDITAR****************
        {
            $objEdit = $res_cons_edit->fetch_object();
            $idEvaEdit = $objEdit->eval_pers_id;
                    
            $sum_notas = 0;
            $cant_aplazos = 0;
            while ($campo = current($_POST)) 
            {
                if(key($_POST)!='evaluador_entry' && key($_POST)!='evaluado' && key($_POST)!='observaciones')
                {
                    $arr_exp = explode('_', key($_POST));
                    $idItem = $arr_exp[1];

                    $str_set_eva_item_edit = "UPDATE eval_item_evaluacion_persona SET nota_item=".$campo." WHERE id_item=".$idItem." AND id_evaluacion_pers=".$idEvaEdit;
                    
                    $res_edit_item = $conn->query($str_set_eva_item_edit);
                    if($res_edit_item)
                    {
                        $data["success"] = true;
                        $data["msg"] = "Se guardaron exitosamente los cambios efectuados en la evaluacion de desempe&ntilde;o";
                        
                    }
                    else
                    {
                        $data["success"] = false;
                        $data["error"] = $conn->error;
                    }
                    $sum_notas+=$campo;
                    if($campo<4)
                    {
                        $cant_aplazos++;
                    }
                }
                next($_POST);
            }
            
            $prom = round($sum_notas/$cant_campos,2);
            $str_edit_eva= "UPDATE eval_evaluacion_persona SET nota_promedio=".$prom.", cant_aplazos=".$cant_aplazos.", observaciones='".$_POST["observaciones"]."' WHERE id=".$idEvaEdit;
            $res_edit_eva = $conn->query($str_edit_eva);

            //calcular_puntaje_total($_POST["evaluado"], $id_ev);
            
            //Escribo log
            $leg = isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
            //$str_limpia = addslashes($str_edit_eva);
            //$str_log = "INSERT INTO usuarios_log (legajo, tabla, log, fecha) VALUES (".$leg.", 'evaluacion', '".$str_limpia."', '".date("Y-m-d H:i:s")."')";
            //$res_log = $conn->query($str_log);
            escribir_log("personal", $leg, 0, "eval_evaluacion_persona", 2, $str_edit_eva, date("Y-m-d H:i:s"));
        }
    }
    echo json_encode($data);
    
}

function getEvaluadorEvaluado($str_show) 
{
    global $conn;
    $data = array();
    if (isset($_GET["query"])) //Se ingresaron caracteres para buscar
    {
        //Parseo la variable $_GET["fields"], que trae los filtros de busqueda. Mauro
        if($_GET["fields"]!=NULL) //Si se selecciono al menos un filtro de busqueda
        {
            $cad = str_replace("[", "", $_GET["fields"]);
            $cad = str_replace("]", "", $cad);
            $cad = str_replace("\"", "", $cad);
            $cad = explode(",", $cad);
            $str_filt = "";
            for($i=0;$i<count($cad);$i++)
            {
                if($cad[$i]!="evaluador")
                {
                    $str_filt.= "general.".$cad[$i]." LIKE '%".$_GET["query"]."%'";
                    if($i!=(count($cad)-2))
                        $str_filt.=" OR ";
                }
            }
            $str_eva = "SELECT general.apellido, general.nombre, general.legajo FROM general WHERE activa=1 AND (".$str_filt.")".$str_show." ORDER BY general.apellido ASC";
            
        }
        else //Si se destiladaron todos los filtros de busqueda, busco todo, para evitar errores
        {
            $str_eva = "SELECT general.apellido, general.nombre, general.legajo FROM general WHERE activa=1 ".$str_show." ORDER BY general.apellido ASC";
        }
    }
    else
    {
        $str_eva = "SELECT general.apellido, general.nombre, general.legajo FROM general WHERE activa=1 ".$str_show." ORDER BY general.apellido ASC";
    }

    $result = $conn->query($str_eva);
    $nbrows = $result->num_rows;
    $arr = array();
    
    while($obj = $result->fetch_object())
    {
        //Aca controlo si el empleado es evaluador
        $str_ev = "SELECT legajo_evaluador FROM eval_evaluadores WHERE legajo_evaluador=".$obj->legajo;
        $res_ev = $conn->query($str_ev);
        if($res_ev->num_rows>0)
        {
            $obj->evaluador = "si";
        }
        else 
        {
            $obj->evaluador = "no";
        }
        $arr[] = $obj;
    }
    echo "({ total: ".$nbrows.",  results: " . json_encode($arr) . "})";
        
}

function getEvaluador()
{
    global $conn;
    $data = array();
    $str_evaluador = "SELECT eval_evaluadores.id, general.apellido, general.nombre, general.legajo FROM general, eval_evaluadores WHERE activa=1 AND general.legajo=eval_evaluadores.legajo_evaluador AND eval_evaluadores.legajo_evaluado=".$_GET["leg"]." ORDER BY general.apellido ASC";
    $result = $conn->query($str_evaluador);
    $nbrows = $result->num_rows;
    $arr = array();
    //echo $str_eva;
    while($obj = $result->fetch_object())
    {
        $arr[] = $obj;
    }
    echo "({ total: ".$nbrows.",  results: " . json_encode($arr) . "})";
}

function getEvaluados()
{
    global $conn;
    $data = array();
    $str_evaluado = "SELECT eval_evaluadores.id, general.apellido, general.nombre, general.legajo FROM general, eval_evaluadores WHERE activa=1 AND general.legajo=eval_evaluadores.legajo_evaluado AND eval_evaluadores.legajo_evaluador=".$_GET["leg"]." ORDER BY general.apellido ASC";
    $result = $conn->query($str_evaluado);
    $nbrows = $result->num_rows;
    $arr = array();
    //echo $str_eva;
    while($obj = $result->fetch_object())
    {
        $arr[] = $obj;
    }
    echo "({ total: ".$nbrows.",  results: " . json_encode($arr) . "})";
}

function setEvaluador() 
{
    global $conn;
    //echo $tarea;
    $registros = json_decode(stripslashes($_POST["records"])); 
    $registrosAEliminar = json_decode(stripslashes($_POST["del"]));
    //var_dump($registrosAEliminar);
    $data = array();
    $data["nuevos"] = 0;
    $data["eliminados"] = 0;
    foreach($registros as $registro)
    {
        $str_cons_ex = "SELECT * FROM eval_evaluadores WHERE id=".$registro->id;
        
        if(!($res = $conn->query($str_cons_ex)))
        {
            $data["success"] = false;
            $data["error"] = $conn->error;
        }
        else
        {
            if($res->num_rows==0)//Es porque el agente es nuevo (tiene id=0, nunca va a existir en la tabla de la bd)
            {
                $str = "INSERT INTO eval_evaluadores (legajo_evaluador, legajo_evaluado) VALUES (".$registro->legajo.", ".$_POST["leg"].")";
                if($conn->query($str))
                {
                    $data["success"] = true;
                    $data["nuevos"]+= $conn->affected_rows;
                    //Actualizo su estatus de evaluador en la tabla usuarios
                    $str_upd = "UPDATE usuarios SET evaluador='si' WHERE legajo=".$registro->legajo;
                    $conn->query($str_upd);

                    // Escribir log
                    $leg = isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
                    //$str_limpia = addslashes($str);
                    //$str_limpia_2 = addslashes($str_upd);
                    //$str_log = "INSERT INTO usuarios_log (legajo, tabla, log, fecha) VALUES (".$leg.", 'eval_evaluadores', '".$str_limpia."', '".date("Y-m-d H:i:s")."'), (".$leg.", 'usuarios', '".$str_limpia_2."', '".date("Y-m-d H:i:s")."')";
                    //$res_log = $conn->query($str_log);
                    escribir_log("personal", $leg, 0, "eval_evaluadores", 1, $str, date("Y-m-d H:i:s"));
                    
                }
                else
                {
                    $data["success"] = false;
                    $data["error"] = $conn->error;
                    //echo json_encode($data);
                }
            }
        }
    }
    foreach($registrosAEliminar as $registroAEliminar)
    {
        $str_del = "DELETE FROM eval_evaluadores WHERE id=".$registroAEliminar->id;
        if($conn->query($str_del))
        {
            $data["success"] = true;
            $data["eliminados"]+= $conn->affected_rows;
            
            //Actualizo su estatus de evaluador en la tabla usuarios si el evaluador ya no tiene evaluados
            $str_cons = "SELECT * FROM eval_evaluadores WHERE legajo_evaluador=".$registroAEliminar->legajo;
            $res = $conn->query($str_cons);
            $nbrows_eva = $res->num_rows;
            if($nbrows_eva==0)
            {
                $str_upd = "UPDATE usuarios SET evaluador='no' WHERE legajo=".$registroAEliminar->legajo;
                $conn->query($str_upd);
            }

            // Escribir log
            $leg = isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
            $str_limpia = addslashes($str_del);
            if($nbrows_eva==0)
            {
                //$str_limpia_2 = addslashes($str_upd);
                //$str_log = "INSERT INTO usuarios_log (legajo, tabla, log, fecha) VALUES (".$leg.", 'eval_evaluadores', '".$str_limpia."', '".date("Y-m-d H:i:s")."'), (".$leg.", 'usuarios', '".$str_limpia_2."', '".date("Y-m-d H:i:s")."')";
                escribir_log("personal", $leg, 0, "usuarios", 2, $str_upd, date("Y-m-d H:i:s"));
                
            }
            else
            {
                escribir_log("personal", $leg, 0, "eval_evaluadores", 3, $str_del, date("Y-m-d H:i:s"));
                
            }
            //$res_log = $conn->query($str_log);
        }
        else
        {
            $data["success"] = false;
            $data["error"]= $conn->error;
        }
    }
    echo json_encode($data);
}

function setEvaluados() 
{
    global $conn;
    //echo $tarea;
    $registros = json_decode(stripslashes($_POST["records"])); 
    $registrosAEliminar = json_decode(stripslashes($_POST["del"]));
    //var_dump($registrosAEliminar);
    $data = array();
    $data["nuevos"] = 0;
    $data["eliminados"] = 0;
    foreach($registros as $registro)
    {

        //echo "ID registro: ".$registro->id;
        $str_cons_ex = "SELECT * FROM eval_evaluadores WHERE id=".$registro->id;
        //echo $str_cons_ex;
        if(!($res = $conn->query($str_cons_ex)))
        {
            $data["success"] = false;
            $data["error"] = $conn->error;
        }
        else
        {
            if($res->num_rows==0)//Es porque el agente es nuevo (tiene id=0, nunca va a existir en la tabla de la bd)
            {
                $str = "INSERT INTO eval_evaluadores (legajo_evaluador, legajo_evaluado) VALUES (".$_POST["leg"].", ".$registro->legajo.")";
                if($conn->query($str))
                {
                    $data["success"] = true;
                    $data["nuevos"]+= $conn->affected_rows;
                    //echo json_encode($data);

                    // Escribir log
                    $leg = isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
                    //$str_limpia = addslashes($str);
                    //$str_log = "INSERT INTO usuarios_log (legajo, tabla, log, fecha) VALUES (".$leg.", 'eval_evaluadores', '".$str_limpia."', '".date("Y-m-d H:i:s")."')";
                    //$res_log = $conn->query($str_log);
                    escribir_log("personal", $leg, $registro->legajo, "eval_evaluadores", 1, $str, date("Y-m-d H:i:s"));
                }
                else
                {
                    $data["success"] = false;
                    $data["error"] = $conn->error;
                    //echo json_encode($data);
                }
            }
        }
    }
    foreach($registrosAEliminar as $registroAEliminar)
    {
        
        $str_del = "DELETE FROM eval_evaluadores WHERE id=".$registroAEliminar->id;
        if($conn->query($str_del))
        {
            $data["success"] = true;
            $data["eliminados"]+= $conn->affected_rows;

            // Escribir log
            $leg = isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
            //$str_limpia = addslashes($str_del);
            //$str_log = "INSERT INTO usuarios_log (legajo, tabla, log, fecha) VALUES (".$leg.", 'eval_evaluadores', '".$str_limpia."', '".date("Y-m-d H:i:s")."')";
            //$res_log = $conn->query($str_log);
            escribir_log("personal", $leg, 0, "eval_evaluadores", 3, $str_del, date("Y-m-d H:i:s"));
        }
        else
        {
            $data["success"] = false;
            $data["error"]= $conn->error;
        }
        
        
    }
    //Actualizo su estatus de evaluador en la tabla usuarios si el evaluador ya no tiene evaluados
    $str_cons = "SELECT * FROM eval_evaluadores WHERE legajo_evaluador=".$_POST["leg"];
    $res = $conn->query($str_cons);
    $nbrows_eva = $res->num_rows;
    if($nbrows_eva==0)
    {
        $str_upd = "UPDATE usuarios SET evaluador='no' WHERE legajo=".$_POST["leg"];
        $conn->query($str_upd);

        // Escribir log
        $leg = isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
        escribir_log("personal", $leg, $_POST["leg"], "usuarios", 2, $str_upd, date("Y-m-d H:i:s"));
    }
    echo json_encode($data);
}

function isOffspring()
{
    global $conn;
    $data = array();
    
    //Simulo una estructura de arbol para analizar cada uno de los descendientes y el tipo de búsqueda es en profundidad
    $offspring = 0; //0 = no descendiente, 1 = descendiente, 2 = error en la BD
    
    $str_evaluado = "SELECT eval_evaluadores.id, general.apellido, general.nombre, general.legajo FROM general, eval_evaluadores WHERE activa=1 AND general.legajo=eval_evaluadores.legajo_evaluado AND eval_evaluadores.legajo_evaluador=".$_GET["leg"]." ORDER BY general.apellido ASC";
    $result = $conn->query($str_evaluado);
    if(!$result)
    {
        $offspring=2;
    }
    else
    {
        $nbrows = $result->num_rows;
        $queue[0] = $result; //Nodo raiz
        
        if($nbrows!=0)
        {
            while(count($queue)>0)
            {
                $obj = end($queue)->fetch_object();
                if($obj)
                {
                    $legActual = $obj->legajo;
                    if($legActual == $_GET["legSel"])
                    {
                        $offspring=1;
                        unset($queue);
                    }
                    else
                    {
                        $str_evaluado = "SELECT eval_evaluadores.id, general.apellido, general.nombre, general.legajo FROM general, eval_evaluadores WHERE activa=1 AND general.legajo=eval_evaluadores.legajo_evaluado AND eval_evaluadores.legajo_evaluador=".$legActual." ORDER BY general.apellido ASC";
                        $result = $conn->query($str_evaluado);
                        if(!$result)
                        {
                            $offspring=2;
                            unset($queue);
                        }
                        else
                        {
                            $nbrows = $result->num_rows;
                            if($nbrows>0)
                            {
                                array_push($queue, $result);
                            }
                        }
                    }
                }
                else
                {
                    array_pop($queue);
                }
            }
        }
    }
    switch($offspring)
    {
        case 0://OK
            $data["success"] = true;
            $data["msg"] = "El empleado puede ser evaluador.";
            break;
        case 1: //El evaluador esta en la descendencia del empleado
            $data["success"] = false;
            $data["error"] = "El empleado que desea cargar como evaluador est&aacute; por debajo de la cadena de mando.";
            break;
        case 2: //Error en la consulta a la base de datos
            $data["success"] = false;
            $data["error"] = $conn->error;
            break;   
    }
    echo json_encode($data);
}

function getMisEvaluaciones()
{
    global $conn;
    $data = array();
    $hoy = date("Y-m-d");
    //Obtengo los items y la nota de cada uno

    $str_evaluaciones_notas = "SELECT eval_item.nombre as nombre_item, eval_item.descripcion as descripcion_item, eval_item_evaluacion_persona.nota_item as nota_item FROM eval_item_evaluacion_persona, eval_item, eval_evaluacion_persona WHERE eval_item_evaluacion_persona.id_item = eval_item.id AND eval_item_evaluacion_persona.id_evaluacion_pers= eval_evaluacion_persona.id AND eval_evaluacion_persona.legajo_evaluado=".$_SESSION["legajo"]." ORDER BY eval_item_evaluacion_persona.id ASC";
    $result_notas = $conn->query($str_evaluaciones_notas);
    $arr_notas = array();
    //echo $str_eva;
    while($obj_notas = $result_notas->fetch_object())
    {
        $arr_notas[] = $obj_notas;
    }
    //$arr[] = $arr_notas;

    //Obtengo los datos de la evaluacion y le incluyo las notas
    $str_evaluaciones = "SELECT eval_evaluacion.*, eval_evaluacion_persona.nota_promedio, eval_evaluacion_persona.cant_aplazos FROM eval_evaluacion, eval_evaluacion_persona WHERE '".$hoy."' > eval_evaluacion.fecha_fin AND eval_evaluacion.id=eval_evaluacion_persona.id_evaluacion AND eval_evaluacion_persona.legajo_evaluado=".$_SESSION["legajo"]." ORDER BY eval_evaluacion.fecha_inicio ASC";
    $result = $conn->query($str_evaluaciones);
    $nbrows = $result->num_rows;
    $arr = array();
    //echo $str_eva;
    while($obj = $result->fetch_object())
    {
        $obj->notas = $arr_notas;
        $arr[] = $obj;
    }


    echo "({ total: ".$nbrows.",  results: " . json_encode($arr) . "})";
}

function getAllEvaluaciones()
{
    global $conn;
    $data = array();

    $str_evaluaciones = "SELECT eval_evaluacion.* FROM eval_evaluacion ORDER BY eval_evaluacion.fecha_inicio ASC";
    $result = $conn->query($str_evaluaciones);
    $nbrows = $result->num_rows;
    $arr = array();
    //echo $str_eva;
    $hoy = new DateTime();

    while($obj = $result->fetch_object())
    {
        if($hoy->format("Y-m-d")<$obj->fecha_inicio)
        {
            $obj->estado= 'Pendiente';
        }
        else
        {
            if($hoy->format("Y-m-d")>=$obj->fecha_inicio && $hoy->format("Y-m-d")<=$obj->fecha_fin)
            {
                $obj->estado= 'En curso';
            }
            else
            {
                $obj->estado= 'Finalizada';
            }
        }
        $arr[] = $obj;
    }
    echo "({ total: ".$nbrows.",  results: " . json_encode($arr) . "})";
}

function getItems()
{
    global $conn;
    $data = array();

    //Obtengo los items de cada evaluacion
    $str_evaluaciones_items = "SELECT eval_item.id, eval_item.nombre as nombre_item, eval_item.descripcion as descripcion_item FROM eval_item, eval_evaluacion_item WHERE eval_item.id = eval_evaluacion_item.id_item AND eval_evaluacion_item.id_evaluacion=".$_GET["id_ev"]." ORDER BY eval_item.id ASC";
    $result_items = $conn->query($str_evaluaciones_items);
    $arr = array();
    
    while($obj = $result_items->fetch_object())
    {
        $arr[]= $obj;
    }
    $data["results"] = $arr;
    echo json_encode($data);
}

function getAllItems()
{
    global $conn;
    $data = array();

    //Obtengo todos los items
    $str_items = "SELECT eval_item.id, eval_item.nombre as nombre_item, eval_item.descripcion as descripcion_item FROM eval_item WHERE eval_item.activo='si' ORDER BY eval_item.id ASC";
    $result_items = $conn->query($str_items);
    $arr = array();
    
    while($obj = $result_items->fetch_object())
    {
        $arr[]= $obj;
    }
    $data["results"] = $arr;
    echo json_encode($data);
}

function setItems() 
{
    global $conn;
    $registros = json_decode(stripslashes($_POST["records_add"])); 
    $registrosAEliminar = json_decode(stripslashes($_POST["records_del"]));
    
    $data = array();
    $data["cambios"] = 0;
    $data["nuevos"] = 0;
    $data["eliminados"] = 0;
    foreach($registros as $registro)
    {
        $str_cons_ex = "SELECT * FROM eval_item WHERE id=".$registro->id;
        
        if(!($res = $conn->query($str_cons_ex)))
        {
            $data["success"] = false;
            $data["error"] = $conn->error;
        }
        else
        {
            if($res->num_rows>0)//Es porque existe el item (se ha modificado), por lo tanto actualizo
            {   

                $str = "UPDATE eval_item SET nombre='".$registro->nombre_item."', descripcion='".$registro->descripcion_item."' WHERE id=".$registro->id;
                //echo $str; 
                if($conn->query($str))
                {
                    $data["success"] = true;
                    $data["cambios"]+= $conn->affected_rows;
                    $leg = isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
                    escribir_log("personal", $leg, 0, "eval_item", 2, $str, date("Y-m-d H:i:s"));
                }
                else
                {
                    $data["success"] = false;
                    $data["error"] = $conn->error;
                }
                //echo $registro->fecha;
            }
            else //se cargó un item nuevo
            {
                $str = "INSERT INTO eval_item (nombre, descripcion) VALUES ('".$registro->nombre_item."', '".$registro->descripcion_item."')";
                if($conn->query($str))
                {
                    $data["success"] = true;
                    $data["nuevos"]+= $conn->affected_rows;
                    $leg = isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
                    escribir_log("personal", $leg, 0, "eval_item", 1, $str, date("Y-m-d H:i:s"));
                }
                else
                {
                    $data["success"] = false;
                    $data["error"] = $conn->error;
                }
            }
        }
    }
    foreach($registrosAEliminar as $registroAEliminar)
    {
        
        $str_del = "UPDATE eval_item SET activo='no' WHERE id=".$registroAEliminar->id;
        if($conn->query($str_del))
        {
            $data["success"] = true;
            $data["eliminados"]+= $conn->affected_rows;
            $leg = isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
            escribir_log("personal", $leg, 0, "eval_item", 2, $str_del, date("Y-m-d H:i:s"));
        }
        else
        {
            $data["success"] = false;
            $data["error"]= $conn->error;
        }
        
        
    }
    echo json_encode($data);
}

function setItemsEvaluacion() 
{
    global $conn;
    $registros = json_decode(stripslashes($_POST["records_add"])); 
    $registrosAEliminar = json_decode(stripslashes($_POST["records_del"]));
    $data = array();
    $data["nuevos"] = 0;
    $data["eliminados"] = 0;
    
    foreach($registros as $registro)
    {
        $str_exist = "SELECT * FROM eval_evaluacion_item WHERE id_evaluacion=".$_POST["id_evaluacion"]." AND id_item=".$registro->id;
        $res_exist = $conn->query($str_exist);
        if($res_exist->num_rows == 0)
        {
            $str = "INSERT INTO eval_evaluacion_item (id_evaluacion, id_item) VALUES (".$_POST["id_evaluacion"].", ".$registro->id.")";
            if($conn->query($str))
            {
                $data["success"] = true;
                $data["nuevos"]+= $conn->affected_rows;
                $leg = isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
                escribir_log("personal", $leg, 0, "eval_evaluacion_item", 1, $str, date("Y-m-d H:i:s"));
            }
            else
            {
                $data["success"] = false;
                $data["error"] = $conn->error;
            }
        }
    }
    foreach($registrosAEliminar as $registroAEliminar)
    {
        
        $str_del = "DELETE FROM eval_evaluacion_item WHERE id_evaluacion=".$_POST["id_evaluacion"]." AND id_item=".$registroAEliminar->id;
        if($conn->query($str_del))
        {
            $data["success"] = true;
            $data["eliminados"]+= $conn->affected_rows;
            $leg = isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
            escribir_log("personal", $leg, 0, "eval_evaluacion_item", 3, $str_del, date("Y-m-d H:i:s"));
        }
        else
        {
            $data["success"] = false;
            $data["error"]= $conn->error;
        }
    }
    echo json_encode($data);
}

function newEvaluacion()
{
    global $conn;
    $registros = json_decode(stripslashes($_POST["records_add"])); 
    $registrosAEliminar = json_decode(stripslashes($_POST["records_del"]));
    $data = array();
    $data["nuevos"] = 0;
    $data["eliminados"] = 0;
    $data["cambios"] = 0;
    
    foreach($registros as $registro)
    {
        $dateIni = new DateTime($registro->fecha_inicio);
        $dateFin = new DateTime($registro->fecha_fin);
        $hoy = date("Y-m-d");
        $conn->query('SET NAMES utf8');
        
        
        if($registro->id==0) //se agregó una nueva evaluacion
        {
            //Controlo que las fechas de la evaluacion a cargar no coincida con evalauciones existentes (en curso)
            $str_busq = "SELECT * FROM eval_evaluacion WHERE fecha_fin >= '".$hoy."' AND (('".$dateIni->format("Y-m-d")."' BETWEEN fecha_inicio AND fecha_fin) OR ('".$dateFin->format('Y-m-d')."' BETWEEN fecha_inicio AND fecha_fin) OR (fecha_inicio BETWEEN '".$dateIni->format("Y-m-d")."' AND '".$dateFin->format('Y-m-d')."') OR (fecha_fin BETWEEN '".$dateIni->format("Y-m-d")."' AND '".$dateFin->format('Y-m-d')."'))";
            $res_busq = $conn->query($str_busq);
            if($res_busq->num_rows==0)
            {
                $str = "INSERT INTO eval_evaluacion (nombre, fecha_inicio, fecha_fin, anio, motivo, observaciones) VALUES ('".$registro->nombre."', '".$dateIni->format('Y-m-d')."', '".$dateFin->format('Y-m-d')."', '".$registro->anio."', '".$registro->motivo."', '".$registro->observaciones."')";
                if($conn->query($str))
                {
                    $id_ev = $conn->insert_id;
                    $data["success"] = true;
                    $data["nuevos"]+= $conn->affected_rows;
                    $leg = isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
                    escribir_log("personal", $leg, 0, "eval_evaluacion", 1, $str, date("Y-m-d H:i:s"));
                    //Inicializo la tabla eval_factores con los datos de la nueva evaluacion

                }
                else
                {
                    $data["success"] = false;
                    $data["error"] = $conn->error;
                }
            }
            else
            {
                $data["success"] = false;
                $data["error"] = "No se puede cargar evaluaci&oacute;n. Las fechas de inicio y/o de fin coinciden con las fechas de alguna evaluaci&oacute;n cargada.";
            }

        }
        else //Se editó una evaluacion
        {
            //Controlo que las fechas de la evaluacion a cargar no coincida con evalauciones existentes
            $str_busq_ed = "SELECT * FROM eval_evaluacion WHERE fecha_fin >= '".$hoy."' AND (('".$dateIni->format("Y-m-d")."' BETWEEN fecha_inicio AND fecha_fin) OR ('".$dateFin->format('Y-m-d')."' BETWEEN fecha_inicio AND fecha_fin) OR (fecha_inicio BETWEEN '".$dateIni->format("Y-m-d")."' AND '".$dateFin->format('Y-m-d')."') OR (fecha_fin BETWEEN '".$dateIni->format("Y-m-d")."' AND '".$dateFin->format('Y-m-d')."')) AND id!=".$registro->id;
            //echo $str_busq_ed;
            $res_busq_ed = $conn->query($str_busq_ed);
            if($res_busq_ed->num_rows==0)
            {
                $str_upd = "UPDATE eval_evaluacion SET nombre='".$registro->nombre."', fecha_inicio='".$dateIni->format('Y-m-d')."', fecha_fin='".$dateFin->format('Y-m-d')."', anio='".$registro->anio."', motivo='".$registro->motivo."', observaciones='".$registro->observaciones."' WHERE id=".$registro->id;
                if($conn->query($str_upd))
                {
                    $data["success"] = true;
                    $data["cambios"]+= $conn->affected_rows;
                    $leg = isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
                    escribir_log("personal", $leg, 0, "eval_evaluacion", 2, $str_upd, date("Y-m-d H:i:s"));
                }
                else
                {
                    $data["success"] = false;
                    $data["error"] = $conn->error;
                }
            }
            else
            {
                $data["success"] = false;
                $data["error"] = "No se puede editar evaluaci&oacute;n. Las fechas de inicio y/o de fin coinciden con las fechas de alguna evaluaci&oacute;n cargada.";
            }
        }
    }    
    
    $hoy = new DateTime();
    foreach($registrosAEliminar as $registroAEliminar)
    {
        //Controlo que la evaluacion a eliminar no tenga una evaluacion efectuada a algun empleado

        $str_cons = "SELECT * FROM eval_evaluacion, eval_evaluacion_persona WHERE eval_evaluacion.id=eval_evaluacion_persona.id_evaluacion AND eval_evaluacion.id=".$registroAEliminar->id;
        $res_cons = $conn->query($str_cons);
        if($res_cons->num_rows==0)
        {
            $str_del = "DELETE FROM eval_evaluacion WHERE id=".$registroAEliminar->id;
            if($conn->query($str_del))
            {
                $str_del_item = "DELETE FROM eval_evaluacion_item WHERE id_evaluacion=".$registroAEliminar->id;
                $conn->query($str_del_item);
                $data["success"] = true;
                $data["eliminados"]+= $conn->affected_rows;
                $leg = isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
                escribir_log("personal", $leg, 0, "eval_evaluacion", 3, $str_del, date("Y-m-d H:i:s"));
                escribir_log("personal", $leg, 0, "eval_evaluacion_item", 3, $str_del_item, date("Y-m-d H:i:s"));
            }
            else
            {
                $data["success"] = false;
                $data["error"]= $conn->error;
            }
        }
        else
        {
            $data["success"] = false;
            $data["error"] = "No se puede eliminar. Ya existen evaluados para esta evaluaci&oacute;n.";
        }
    }
    echo json_encode($data);
}

function actualizar_tardanzas($legajo, $fecha_inicio, $fecha_fin)
{
    global $conn;

    if($legajo!=2328 && $legajo!=2144 && $legajo!=2051 && $legajo!=2295 && $legajo!=2314 && $legajo!=1312 && $legajo!=964 && $legajo!=1360 && $legajo!=800 && $legajo!=1801 && $legajo!=2161 && $legajo!=2244 && $legajo!=2178 && $legajo!=2207 && $legajo!=2133 && $legajo!=2132 && $legajo!=2131 && $legajo!=1332 && $legajo!=930) //Excepciones, dejar tardanzas en 0
    {
        if($legajo==1788 || $legajo==1123) //Caso Albano menegazzo y Flores Nicolas Alejandro, dejar tardanzas en 29
        {
            $cantidad = 29;
        }
        else
        {
            $str_cons_tard = "SELECT count(*) AS cantidad FROM reloj_fichadas WHERE legajo=".$legajo." AND tardanza=1 AND baja=0 AND (fecha BETWEEN '".$fecha_inicio."' AND '".$fecha_fin."')";
            $res_cons_tard = $conn->query($str_cons_tard);
            $obj = $res_cons_tard->fetch_object();
            $cantidad = $obj->cantidad;
        }
        $str_upd = "UPDATE eval_factores SET cant_tardanzas=".$cantidad." WHERE legajo=".$legajo." AND fecha_inicio='".$fecha_inicio."' AND fecha_fin='".$fecha_fin."'";
        $res_upd = $conn->query($str_upd);
    }

}

function calcular_licencias($legajo, $fecha_inicio, $fecha_fin)
{
    global $conn;
    
    $sum_lic = 0;
    $sum_lic_injust = 0;

    $cons_lic = "SELECT SUM(dias_licencia) as cantidad from licencias WHERE tipo_licencia NOT IN (4,5,7,10,13,18,19) AND legajo=".$legajo." AND goce='S' AND (desde BETWEEN '".$fecha_inicio."' AND '".$fecha_fin."')";
    $res_lic = $conn->query($cons_lic);
    $obj_lic = $res_lic->fetch_object();
    $sum_lic=$obj_lic->cantidad?$obj_lic->cantidad:0;
    $cons_lic_injust = "SELECT SUM(dias_licencia) as cantidad_injust from licencias WHERE tipo_licencia=19 AND legajo=".$legajo." AND goce='S' AND (desde BETWEEN '".$fecha_inicio."' AND '".$fecha_fin."')";
    $res_lic_injust = $conn->query($cons_lic_injust);
    $obj_lic_injust = $res_lic_injust->fetch_object();
    $sum_lic_injust=$obj_lic_injust->cantidad_injust?$obj_lic_injust->cantidad_injust:0;
    $cons_upd = "UPDATE eval_factores SET cant_licencias=".$sum_lic.", cant_licencias_injust=".$sum_lic_injust." WHERE legajo=".$legajo." AND fecha_inicio='".$fecha_inicio."' AND fecha_fin='".$fecha_fin."'";
    $res_upd = $conn->query($cons_upd);
}

function calcular_asist_puntualidad($legajo, $fecha_inicio, $fecha_fin)
{
    global $conn;
    
    $str_leg = "SELECT * FROM eval_factores WHERE legajo=".$legajo." AND fecha_inicio='".$fecha_inicio."' AND fecha_fin='".$fecha_fin."'";
    $cons_leg = $conn->query($str_leg);
    
    while($obj = $cons_leg->fetch_object())
    {
        $licencias = $obj->cant_licencias;
        $licencias_injust = $obj->cant_licencias_injust;
        $tardanzas = $obj->cant_tardanzas;
        $asist_puntualidad = 100;
        if($licencias==0)
        {
            $asist_puntualidad = 100;    
        }
        else
        {
            if($licencias>0 && $licencias<=5)
            {
                $asist_puntualidad = 70;
            }
            else
            {
                if($licencias>5 && $licencias<=10)
                {
                    $asist_puntualidad = 40;
                }
                else
                {
                    if($licencias>10 && $licencias<=15)
                    {
                        $asist_puntualidad = 10;
                    }
                    else
                    {
                        $asist_puntualidad = 0;
                    }
                }
            }
        }

        //Ahora controlo si tuvo licencias injustificadas
        if($licencias_injust>0 && $licencias_injust<2)
        {
            if($asist_puntualidad>70)
            {
                $asist_puntualidad = 70;
            }
        }
        else
        {
            if($licencias_injust>1 && $licencias_injust<3)
            {
                if($asist_puntualidad>40)
                {
                    $asist_puntualidad = 40;
                }
            }
            else
            {
                if($licencias_injust>2 && $licencias_injust<4)
                {
                    if($asist_puntualidad>10)
                    {
                        $asist_puntualidad = 10;
                    }
                }
                else
                {
                    if($licencias_injust>3)
                    {
                        if($asist_puntualidad>0)
                        {
                            $asist_puntualidad = 0;
                        }
                    }
                }
            }
        }
        
        $asist_puntualidad = $asist_puntualidad - ($tardanzas*2);
        $asist_puntualidad = $asist_puntualidad/100;
        $str_upd = "UPDATE eval_factores SET asist_puntualidad=".$asist_puntualidad." WHERE legajo=".$legajo." AND fecha_inicio='".$fecha_inicio."' AND fecha_fin='".$fecha_fin."'";
        $cons_upd = $conn->query($str_upd);
    }
}

function calcular_antiguedad_cargo($legajo, $fecha_inicio, $fecha_fin)
{
    global $conn;
    
    $str_leg = "SELECT general.legajo, desde FROM general, histocargo WHERE general.legajo=".$legajo." AND general.cargoactu=histocargo.nrocargo AND general.legajo=histocargo.legajo";
    $cons_leg = $conn->query($str_leg);
    $cant_anios = 0;
    if($cons_leg->num_rows>0)
    {
        $obj_leg = $cons_leg->fetch_object();
        $datetime_desde = new DateTime($obj_leg->desde);
        $hasta = $fecha_fin;
        $datetime_hasta = new DateTime($hasta);
        $interval = $datetime_desde->diff($datetime_hasta);
        $cant_anios = $interval->format('%y');
    }
    else
    {
        $cant_anios = 0;
    }
    
    $str_upd = "UPDATE eval_factores SET antiguedad_cargo=".$cant_anios." WHERE legajo=".$legajo." AND fecha_inicio='".$fecha_inicio."' AND fecha_fin='".$fecha_fin."'";
    $cons_upd = $conn->query($str_upd);
}

function calcular_antiguedad_escalafon($legajo, $fecha_inicio, $fecha_fin)
{
    global $conn;
    $cont=0;
    $str_car = "SELECT general.legajo, general.cargoactu, cargos.escalafon FROM general, cargos WHERE general.legajo=".$legajo." AND general.cargoactu=cargos.nrocargo";
    $res_car = $conn->query($str_car);
    if($res_car->num_rows>0)
    {
        $obj = $res_car->fetch_object();
        $cargo_actu = $obj->cargoactu;
        $escalafon_actu = $obj->escalafon;
    
        $str_esc = "SELECT MIN(desde) as desde FROM histocargo, cargos WHERE histocargo.legajo=".$legajo." AND histocargo.nrocargo=cargos.nrocargo AND cargos.escalafon=".$escalafon_actu;
        $res_esc = $conn->query($str_esc);
        $obj_esc = $res_esc->fetch_object();
        $min_desde = $obj_esc->desde;
        $datetime_desde = new DateTime($min_desde);
        $datetime_hasta = new DateTime($fecha_fin);
        $interval = $datetime_desde->diff($datetime_hasta);
        $cant_anios = $interval->format('%y');
    }
    else
    {
        $cant_anios = 0;
    }
    $str_upd = "UPDATE eval_factores SET antiguedad_escalafon=".$cant_anios." WHERE legajo=".$legajo." AND fecha_inicio='".$fecha_inicio."' AND fecha_fin='".$fecha_fin."'";
    $res_upd = $conn->query($str_upd);
}

function calcular_capacitaciones($legajo, $fecha_inicio, $fecha_fin)
{
    global $conn;
    
    $fecha_ini = new DateTime($fecha_inicio);
    $años = new DateInterval("P5Y");
    $fecha_limite = $fecha_ini->sub($años);

    $str_cap = "SELECT * FROM capacitaciones WHERE legajo=".$legajo." AND ((fecha>='".$fecha_limite->format('Y-m-d')."' AND tipo!=9 AND tipo!=10 AND tipo!=11) OR tipo=9 OR tipo=10 OR tipo=11) AND inherencia='si' AND cant_horas>=4";
    //echo $str_cap."<br>";
    $res_cap = $conn->query($str_cap);
    $puntaje_cap = 0;
    while($obj = $res_cap->fetch_object())
    {
        switch($obj->tipo)
        {
            case 9: //especializacion
                $puntaje_cap+=2;
                break;
            case 10://Maestria
                $puntaje_cap+=3;
                break;
            case 11://Doctorado
                $puntaje_cap+=5;
                break;
            default: //todas las demas
                if($obj->origen==1) //Interna (escuela de capacitacion)
                {
                    if($obj->modalidad==1) //Presencial
                    {
                        if($obj->cant_horas==4)
                        {
                            $puntaje_cap+=$obj->evaluacion=='No'?0.025:0.025;
                        }
                        if($obj->cant_horas>=5 && $obj->cant_horas<=8)
                        {
                            $puntaje_cap+=$obj->evaluacion=='No'?0.05:0.15;
                        }
                        if($obj->cant_horas>=9 && $obj->cant_horas<=12)
                        {
                            $puntaje_cap+=$obj->evaluacion=='No'?0.1:0.2;
                        }
                        if($obj->cant_horas>=13 && $obj->cant_horas<=16)
                        {
                            $puntaje_cap+=$obj->evaluacion=='No'?0.15:0.25;
                        }
                        if($obj->cant_horas>=17 && $obj->cant_horas<=20)
                        {
                            $puntaje_cap+=$obj->evaluacion=='No'?0.2:0.3;
                        }
                        if($obj->cant_horas>=21 && $obj->cant_horas<=24)
                        {
                            $puntaje_cap+=$obj->evaluacion=='No'?0.25:0.35;
                        }
                        if($obj->cant_horas>=25 && $obj->cant_horas<=28)
                        {
                            $puntaje_cap+=$obj->evaluacion=='No'?0.3:0.4;
                        }
                        if($obj->cant_horas>=29 && $obj->cant_horas<=32)
                        {
                            $puntaje_cap+=$obj->evaluacion=='No'?0.35:0.45;
                        }
                        if($obj->cant_horas>=33 && $obj->cant_horas<=50)
                        {
                            $puntaje_cap+=$obj->evaluacion=='No'?0.4:0.7;
                        }
                        if($obj->cant_horas>=51)
                        {
                            $puntaje_cap+=$obj->evaluacion=='No'?0.5:1;
                        }
                    }
                    else //Virtual
                    {
                        if($obj->cant_horas==4)
                        {
                            $puntaje_cap+=0.05;
                        }
                        if($obj->cant_horas>=5 && $obj->cant_horas<=8)
                        {
                            $puntaje_cap+=0.1;
                        }
                        if($obj->cant_horas>=9 && $obj->cant_horas<=12)
                        {
                            $puntaje_cap+=0.15;
                        }
                        if($obj->cant_horas>=13 && $obj->cant_horas<=16)
                        {
                            $puntaje_cap+=0.2;
                        }
                        if($obj->cant_horas>=17 && $obj->cant_horas<=20)
                        {
                            $puntaje_cap+=0.25;
                        }
                        if($obj->cant_horas>=21 && $obj->cant_horas<=24)
                        {
                            $puntaje_cap+=0.3;
                        }
                        if($obj->cant_horas>=25 && $obj->cant_horas<=28)
                        {
                            $puntaje_cap+=0.35;
                        }
                        if($obj->cant_horas>=29 && $obj->cant_horas<=32)
                        {
                            $puntaje_cap+=0.4;
                        }
                        if($obj->cant_horas>=33 && $obj->cant_horas<=50)
                        {
                            $puntaje_cap+=0.45;
                        }
                        if($obj->cant_horas>=51)
                        {
                            $puntaje_cap+=0.6;
                        }
                    }
                }
                else //Externa
                {
                    if($obj->modalidad==1) //Presencial
                    {
                        if($obj->cant_horas==4)
                        {
                            $puntaje_cap+=$obj->evaluacion=='No'?0.025:0.025;
                        }
                        if($obj->cant_horas>=5 && $obj->cant_horas<=8)
                        {
                            $puntaje_cap+=$obj->evaluacion=='No'?0.05:0.1;
                        }
                        if($obj->cant_horas>=9 && $obj->cant_horas<=12)
                        {
                            $puntaje_cap+=$obj->evaluacion=='No'?0.1:0.15;
                        }
                        if($obj->cant_horas>=13 && $obj->cant_horas<=16)
                        {
                            $puntaje_cap+=$obj->evaluacion=='No'?0.15:0.2;
                        }
                        if($obj->cant_horas>=17 && $obj->cant_horas<=20)
                        {
                            $puntaje_cap+=$obj->evaluacion=='No'?0.2:0.25;
                        }
                        if($obj->cant_horas>=21 && $obj->cant_horas<=24)
                        {
                            $puntaje_cap+=$obj->evaluacion=='No'?0.25:0.3;
                        }
                        if($obj->cant_horas>=25 && $obj->cant_horas<=28)
                        {
                            $puntaje_cap+=$obj->evaluacion=='No'?0.3:0.35;
                        }
                        if($obj->cant_horas>=29 && $obj->cant_horas<=32)
                        {
                            $puntaje_cap+=$obj->evaluacion=='No'?0.35:0.4;
                        }
                        if($obj->cant_horas>=33 && $obj->cant_horas<=50)
                        {
                            $puntaje_cap+=$obj->evaluacion=='No'?0.4:0.6;
                        }
                        if($obj->cant_horas>=51)
                        {
                            $puntaje_cap+=$obj->evaluacion=='No'?0.5:0.8;
                        }
                    }
                    else //Virtual
                    {
                        if($obj->cant_horas==4)
                        {
                            $puntaje_cap+=0.025;
                        }
                        if($obj->cant_horas>=5 && $obj->cant_horas<=8)
                        {
                            $puntaje_cap+=0.05;
                        }
                        if($obj->cant_horas>=9 && $obj->cant_horas<=12)
                        {
                            $puntaje_cap+=0.1;
                        }
                        if($obj->cant_horas>=13 && $obj->cant_horas<=16)
                        {
                            $puntaje_cap+=0.15;
                        }
                        if($obj->cant_horas>=17 && $obj->cant_horas<=20)
                        {
                            $puntaje_cap+=0.2;
                        }
                        if($obj->cant_horas>=21 && $obj->cant_horas<=24)
                        {
                            $puntaje_cap+=0.25;
                        }
                        if($obj->cant_horas>=25 && $obj->cant_horas<=28)
                        {
                            $puntaje_cap+=0.3;
                        }
                        if($obj->cant_horas>=29 && $obj->cant_horas<=32)
                        {
                            $puntaje_cap+=0.25;
                        }
                        if($obj->cant_horas>=33 && $obj->cant_horas<=50)
                        {
                            $puntaje_cap+=0.4;
                        }
                        if($obj->cant_horas>=51)
                        {
                            $puntaje_cap+=0.5;
                        }
                    }
                }            
        }//End switch
    }//End while
    $str_upd_cap = "UPDATE eval_factores SET capacitacion=".$puntaje_cap." WHERE legajo=".$legajo." AND fecha_inicio='".$fecha_inicio."' AND fecha_fin='".$fecha_fin."'";
    //echo $str_upd_cap;
    $res_upd_cap = $conn->query($str_upd_cap);

}//End function

function calcular_sanciones($legajo, $fecha_inicio, $fecha_fin)
{
    global $conn;
    
    $str_leg = "SELECT *, count(*) as cantidad FROM sanciones, eval_factores WHERE sanciones.desde BETWEEN '".$fecha_inicio."' AND '".$fecha_fin."' AND sanciones.legajo=eval_factores.legajo AND eval_factores.fecha_inicio='".$fecha_inicio."' AND eval_factores.fecha_fin='".$fecha_fin."' AND sanciones.legajo=".$legajo;
    //echo $str_leg;
    $cons_leg = $conn->query($str_leg);
    $obj = $cons_leg->fetch_object();
    $puntaje_total = $obj->puntaje_total;
    $desc = 0;
    //Calculo el 20% o 30%  del puntaje total, dependiendo de la cantidad de sanciones
    if($obj->cantidad==2)
    {
        $desc = (float) 20*$puntaje_total/100;
    }
    if($obj->cantidad>=3)
    {
        $desc = (float) 30*$puntaje_total/100;
    }
    $desc = round($desc,2);
    $str_upd = "UPDATE eval_factores SET conducta=".$desc." WHERE legajo=".$legajo." AND fecha_inicio='".$fecha_inicio."' AND fecha_fin='".$fecha_fin."'";
    $cons_upd = $conn->query($str_upd);
}

function calcular_puntaje_total($legajo, $fecha_inicio, $fecha_fin)
{
    global $conn;
    
    //Obtengo la nota de la ultima evaluacion efectuada al empleado
    $str_leg = "SELECT *, eval_evaluacion.id as id_evaluacion FROM eval_evaluacion_persona, eval_evaluacion WHERE (eval_evaluacion.id=4 OR eval_evaluacion.id=5) AND eval_evaluacion_persona.id_evaluacion=eval_evaluacion.id AND eval_evaluacion_persona.legajo_evaluado=".$legajo." ORDER BY eval_evaluacion.fecha_inicio DESC";
    $cons_leg = $conn->query($str_leg);
    if($cons_leg->num_rows>0)
    {
        $obj = $cons_leg->fetch_object();
        $promedio_evaluacion = $obj->nota_promedio;
        $id_evaluacion = $obj->id_evaluacion;
    }
    else //El empleado no fue evaluado. La nota de evaluacion es 0
    {
        $promedio_evaluacion = 0;
        $id_evaluacion = 0;
    }

    //Ahora obtengo los factores de ascenso del empleado para el periodo seleccionado
    $str_fact = "SELECT * FROM eval_factores WHERE legajo=".$legajo." AND fecha_inicio='".$fecha_inicio."' AND fecha_fin='".$fecha_fin."'";
    $cons_fact = $conn->query($str_fact);
    $obj_fact = $cons_fact->fetch_object();
    $antiguedad_categoria = 5*$obj_fact->antiguedad_cargo/100;
    $antiguedad_escalafon = 2*round($obj_fact->antiguedad_escalafon/5)/100;
    $puntaje_total = (float) $antiguedad_categoria+$antiguedad_escalafon+$obj_fact->asist_puntualidad+$promedio_evaluacion+$obj_fact->capacitacion;
    $puntaje_total = round($puntaje_total,2);
    $str_upd = "UPDATE eval_factores SET puntaje_total=".$puntaje_total.", puntaje_evaluacion=".$promedio_evaluacion.", id_evaluacion=".$id_evaluacion." WHERE legajo=".$legajo." AND fecha_inicio='".$fecha_inicio."' AND fecha_fin='".$fecha_fin."'";
    $cons_upd = $conn->query($str_upd);
}

function updateFactoresAscenso()
{
    global $conn;
    $data = array();
    $fecha_inicio  = setFecha($_POST["fecha_inicio_fact"]);
    $fecha_fin =  setFecha($_POST["fecha_fin_fact"]);

    $str_exist = "SELECT * FROM eval_factores WHERE (fecha_inicio BETWEEN '".$fecha_inicio."' AND '".$fecha_fin."') OR (fecha_fin BETWEEN '".$fecha_inicio."' AND '".$fecha_fin."') OR ('".$fecha_inicio."' BETWEEN fecha_inicio AND fecha_fin) OR ('".$fecha_fin."' BETWEEN fecha_inicio AND fecha_fin)";
    $cons_exist = $conn->query($str_exist);
    if($cons_exist->num_rows>0)
    {
        $data["success"] = false;
        $data["error"] = "Ya hay factores calculados para el per&iacute;odo seleccionado. Elija otro per&iacute;odo.";
    }
    else
    {
        $str_leg = "SELECT legajo FROM general WHERE activa=1";
        $cons_leg = $conn->query($str_leg);
        while($obj_leg = $cons_leg->fetch_object())
        {
            $str_ins = "INSERT INTO eval_factores(legajo, fecha_inicio, fecha_fin) VALUES (".$obj_leg->legajo.",'".$fecha_inicio."','".$fecha_fin."')";
            //echo $str_ins;
            $cons_ins = $conn->query($str_ins);

            actualizar_tardanzas($obj_leg->legajo, $fecha_inicio, $fecha_fin);
            calcular_licencias($obj_leg->legajo, $fecha_inicio, $fecha_fin);
            calcular_asist_puntualidad($obj_leg->legajo, $fecha_inicio, $fecha_fin);
            calcular_antiguedad_cargo($obj_leg->legajo, $fecha_inicio, $fecha_fin);
            calcular_antiguedad_escalafon($obj_leg->legajo, $fecha_inicio, $fecha_fin);
            calcular_capacitaciones($obj_leg->legajo, $fecha_inicio, $fecha_fin);
            calcular_puntaje_total($obj_leg->legajo, $fecha_inicio, $fecha_fin);
            //Las sanciones se calculan al último, dado que es un 20 o 30% menos del puntaje total, el cual debe calcularse primero
            calcular_sanciones($obj_leg->legajo, $fecha_inicio, $fecha_fin);
        }
        $data["success"] = true;
        $data["msg"] = "Factores calculados correctamente";
    }
    echo json_encode($data);
}

/*Asigna al juez de un juzgado como evaluador del secretario, y al secretario como evaluador del resto del area*/
function autoAsignarEvaluadores()
{
    global $conn;
    $data = array();
    $i = 0;
    $str_juez = "SELECT * FROM general WHERE activa=1 AND (cargoactu=8 OR cargoactu=13)";
    $cons_juez = $conn->query($str_juez);
    while($obj_juez = $cons_juez->fetch_object())
    {
        $str_sec = "SELECT * FROM general WHERE activa=1 AND (cargoactu=20 OR cargoactu=26 OR cargoactu=32) AND lugaractu=".$obj_juez->lugaractu;
        $cons_sec = $conn->query($str_sec);

        $str_emp = "SELECT * FROM general WHERE fecha_carga<'2019-01-01' AND activa=1 AND cargoactu!=8 AND cargoactu!=13 AND cargoactu=20 AND cargoactu=26 AND cargoactu=32 AND lugaractu=".$obj_juez->lugaractu;
        $cons_emp = $conn->query($str_emp);
        if($cons_sec->num_rows==0)//No hay ni secretario, ni prosecretario ni secretario de paz. El evaluador es el juez
        {
            if($cons_emp->num_rows>0)
            {
                while($obj_emp = $cons_emp->fetch_object())
                {
                    $str_eval = "SELECT * FROM eval_evaluadores WHERE legajo_evaluado=".$obj_emp->legajo;
                    $cons_eval = $conn->query($str_eval);
                    if($cons_eval->num_rows>0) //El evaluado está cargado en la tabla
                    {
                        $obj_eval = $cons_eval->fetch_object();
                        if($obj_juez->legajo!=$obj_eval->legajo_evaluador) //Tiene un evaluador viejo, actualizo con el juez actual
                        {
                            $str_upd_eval = "UPDATE eval_evaluadores SET legajo_evaluador=".$obj_juez->legajo." WHERE legajo_evaluado=".$obj_emp->legajo;
                            $cons_upd_eval = $conn->query($str_upd_eval);
                            echo "Evaluador ".$obj_juez->legajo." Evaluado ".$obj_emp->legajo."<br>";
                            $i++;
                        }
                    }
                    else //Caso contrario, inserto el evaluado en la tabla
                    {
                        $str_ins_eval = "INSERT INTO eval_evaluadores (legajo_evaluador, legajo_evaluado) VALUES (".$obj_juez->legajo.", ".$obj_emp->legajo.")";
                        $cons_ins_eval = $conn->query($str_ins_eval);
                        echo "Evaluador ".$obj_juez->legajo." Evaluado ".$obj_emp->legajo."<br>";
                        $i++;
                    }
                    
                }
            }
        }
        else
        {
            $sec = 0;
            $prosec = 0;
            $secpaz = 0;
            while($obj_sec = $cons_sec->fetch_object())
            {
                switch ($obj_sec->cargoactu) 
                {
                    case 20:
                        $sec = 1;
                        $legsec = $obj_sec->legajo;
                        break;
                    case 26:
                        $prosec = 1;
                        $legprosec = $obj_sec->legajo;
                        break;
                    case 32:
                        $secpaz = 1;
                        $legsecpaz = $obj_sec->legajo;
                        break;    
                }
            }
            if($sec==1) //Si hay secretario, juez evalua a secretario y secretario evalua al resto
            {
                $str_eval_sec = "SELECT * FROM eval_evaluadores WHERE legajo_evaluado=".$legsec;
                $cons_eval_sec = $conn->query($str_eval_sec);
                if($cons_eval_sec->num_rows>0) //El evaluado está cargado en la tabla
                {
                    $obj_eval_sec = $cons_eval_sec->fetch_object();
                    if($obj_eval_sec->legajo_evaluador!=$obj_juez->legajo) //Tiene un evaluador viejo, actualizo con el juez actual
                    {
                        $str_upd_eval_sec = "UPDATE eval_evaluadores SET legajo_evaluador=".$obj_juez->legajo." WHERE legajo_evaluado=".$legsec;
                        $cons_upd_eval_sec = $conn->query($str_upd_eval_sec);
                        echo "Evaluador ".$obj_juez->legajo." Evaluado ".$legsec."<br>";
                        $i++;
                    }
                }
                else //Caso contrario, inserto el evaluado en la tabla
                {
                    $str_ins_eval = "INSERT INTO eval_evaluadores (legajo_evaluador, legajo_evaluado) VALUES (".$obj_juez->legajo.", ".$legsec.")";
                    $cons_ins_eval = $conn->query($str_ins_eval);
                    echo "Evaluador ".$obj_juez->legajo." Evaluado ".$legsec."<br>";
                    $i++;   
                }

                $str_emp = "SELECT * FROM general WHERE fecha_carga<'2019-01-01' AND activa=1 AND cargoactu!=8 AND cargoactu!=13 AND cargoactu!=20 AND lugaractu=".$obj_juez->lugaractu;
                $cons_emp = $conn->query($str_emp);
                if($cons_emp->num_rows>0)
                {
                    while($obj_emp = $cons_emp->fetch_object())
                    {
                        $str_eval = "SELECT * FROM eval_evaluadores WHERE legajo_evaluado=".$obj_emp->legajo;
                        $cons_eval = $conn->query($str_eval);
                        if($cons_eval->num_rows>0) //El evaluado está cargado en la tabla
                        {
                            $obj_eval = $cons_eval->fetch_object();
                            if($legsec!=$obj_eval->legajo_evaluador) //Tiene un evaluador viejo, actualizo con el secretario actual
                            {
                                $str_upd_eval = "UPDATE eval_evaluadores SET legajo_evaluador=".$legsec." WHERE legajo_evaluado=".$obj_emp->legajo;
                                $cons_upd_eval = $conn->query($str_upd_eval);
                                echo "Evaluador ".$legsec." Evaluado ".$obj_emp->legajo."<br>";
                                $i++;
                            }
                        }
                        else //Caso contrario, inserto el evaluado en la tabla
                        {
                            $str_ins_eval = "INSERT INTO eval_evaluadores (legajo_evaluador, legajo_evaluado) VALUES (".$legsec.", ".$obj_emp->legajo.")";
                            $cons_ins_eval = $conn->query($str_ins_eval);
                            echo "Evaluador ".$legsec." Evaluado ".$obj_emp->legajo."<br>";
                            $i++;
                        }
                    }
                }
            }
            else //Si no hay secretario, busco si hay prosecretario. Juez evalua a prosecretario y prosecretario evalua al resto
            {
                if($prosec == 1)
                {
                    $str_eval_sec = "SELECT * FROM eval_evaluadores WHERE legajo_evaluado=".$legprosec;
                    $cons_eval_sec = $conn->query($str_eval_sec);
                    if($cons_eval_sec->num_rows>0) //El evaluado está cargado en la tabla
                    {
                        $obj_eval_sec = $cons_eval_sec->fetch_object();
                        if($obj_eval_sec->legajo_evaluador!=$obj_juez->legajo) //Tiene un evaluador viejo, actualizo con el juez actual
                        {
                            $str_upd_eval_sec = "UPDATE eval_evaluadores SET legajo_evaluador=".$obj_juez->legajo." WHERE legajo_evaluado=".$legprosec;
                            $cons_upd_eval_sec = $conn->query($str_upd_eval_sec);
                            echo "Evaluador ".$obj_juez->legajo." Evaluado ".$legprosec."<br>";
                            $i++;
                        }
                    }
                    else //Caso contrario, inserto el evaluado en la tabla
                    {
                        $str_ins_eval = "INSERT INTO eval_evaluadores (legajo_evaluador, legajo_evaluado) VALUES (".$obj_juez->legajo.", ".$legprosec.")";
                        $cons_ins_eval = $conn->query($str_ins_eval);
                        echo "Evaluador ".$obj_juez->legajo." Evaluado ".$legprosec."<br>";
                        $i++;
                    }

                    $str_emp = "SELECT * FROM general WHERE fecha_carga<'2019-01-01' AND activa=1 AND cargoactu!=8 AND cargoactu!=13 AND cargoactu!=26 AND lugaractu=".$obj_juez->lugaractu;
                    $cons_emp = $conn->query($str_emp);
                    if($cons_emp->num_rows>0)
                    {
                        while($obj_emp = $cons_emp->fetch_object())
                        {
                            $str_eval = "SELECT * FROM eval_evaluadores WHERE legajo_evaluado=".$obj_emp->legajo;
                            $cons_eval = $conn->query($str_eval);
                            if($cons_eval->num_rows>0) //El evaluado está cargado en la tabla
                            {
                                $obj_eval = $cons_eval->fetch_object();
                                if($legprosec!=$obj_eval->legajo_evaluador) //Tiene un evaluador viejo, actualizo con el pro secretario actual
                                {
                                    $str_upd_eval = "UPDATE eval_evaluadores SET legajo_evaluador=".$legprosec." WHERE legajo_evaluado=".$obj_emp->legajo;
                                    $cons_upd_eval = $conn->query($str_upd_eval);
                                    echo "Evaluador ".$legprosec." Evaluado ".$obj_emp->legajo."<br>";
                                    $i++;
                                }
                            }
                            else //Caso contrario, inserto el evaluado en la tabla
                            {
                                $str_ins_eval = "INSERT INTO eval_evaluadores (legajo_evaluador, legajo_evaluado) VALUES (".$legprosec.", ".$obj_emp->legajo.")";
                                $cons_ins_eval = $conn->query($str_ins_eval);
                                echo "Evaluador ".$legprosec." Evaluado ".$obj_emp->legajo."<br>";
                                $i++;
                            }
                        }
                    }
                }
                else //Si no hay prosecretario, busco el secretario de paz. Juez evalua a sec de paz y sec de paz el evalua al resto
                {
                    $str_eval_sec = "SELECT * FROM eval_evaluadores WHERE legajo_evaluado=".$legsecpaz;
                    $cons_eval_sec = $conn->query($str_eval_sec);
                    if($cons_eval_sec->num_rows>0) //El evaluado está cargado en la tabla
                    {
                        $obj_eval_sec = $cons_eval_sec->fetch_object();
                        if($obj_juez->legajo!=$obj_eval_sec->legajo_evaluador) //Tiene un evaluador viejo, actualizo con el juez actual
                        {
                            $str_upd_eval_sec = "UPDATE eval_evaluadores SET legajo_evaluador=".$obj_juez->legajo." WHERE legajo_evaluado=".$legsecpaz;
                            $cons_upd_eval_sec = $conn->query($str_upd_eval_sec);
                            echo "Evaluador ".$obj_juez->legajo." Evaluado ".$legsecpaz."<br>";
                            $i++;
                        }
                    }
                    else //Caso contrario, inserto el evaluado en la tabla
                    {
                        $str_ins_eval = "INSERT INTO eval_evaluadores (legajo_evaluador, legajo_evaluado) VALUES (".$obj_juez->legajo.", ".$legsecpaz.")";
                        $cons_ins_eval = $conn->query($str_ins_eval);
                        echo "Evaluador ".$obj_juez->legajo." Evaluado ".$legsecpaz."<br>";
                        $i++;
                    }

                    $str_emp = "SELECT * FROM general WHERE fecha_carga<'2019-01-01' AND activa=1 AND cargoactu!=8 AND cargoactu!=13 AND cargoactu!=32 AND lugaractu=".$obj_juez->lugaractu;
                    $cons_emp = $conn->query($str_emp);
                    if($cons_emp->num_rows>0)
                    {
                        while($obj_emp = $cons_emp->fetch_object())
                        {
                            $str_eval = "SELECT * FROM eval_evaluadores WHERE legajo_evaluado=".$obj_emp->legajo;
                            $cons_eval = $conn->query($str_eval);
                            if($cons_eval->num_rows>0) //El evaluado está cargado en la tabla
                            {
                                $obj_eval = $cons_eval->fetch_object();
                                if($legsecpaz!=$obj_eval->legajo_evaluador) //Tiene un evaluador viejo, actualizo con el secretario de paz actual
                                {
                                    $str_upd_eval = "UPDATE eval_evaluadores SET legajo_evaluador=".$legsecpaz." WHERE legajo_evaluado=".$obj_emp->legajo;
                                    $cons_upd_eval = $conn->query($str_upd_eval);
                                    echo "Evaluador ".$legsecpaz." Evaluado ".$obj_emp->legajo."<br>";
                                    $i++;
                                }
                            }
                            else //Caso contrario, inserto el evaluado en la tabla
                            {
                                $str_ins_eval = "INSERT INTO eval_evaluadores (legajo_evaluador, legajo_evaluado) VALUES (".$legsecpaz.", ".$obj_emp->legajo.")";
                                $cons_ins_eval = $conn->query($str_ins_eval);
                                echo "Evaluador ".$legsecpaz." Evaluado ".$obj_emp->legajo."<br>";
                                $i++;
                            }
                        }
                    }
                }    
            }
        }
    }
    echo "<br>Registros actualizados/cargados: ".$i;
}

/*Asigna los evaluados a cada evaluacion. O sea, a quien esta dirigida la evaluacion*/
function cargarEvaluacionEvaluado()
{
    global $conn;
    $data = array();
    $i=0;
    $errores = 0;

    $str_general = "SELECT * FROM general g LEFT JOIN cargos c ON g.cargoactu=c.nrocargo WHERE activa=1";
    $cons_general = $conn->query($str_general);
    $str_trunc = "TRUNCATE TABLE eval_evaluacion_evaluado";
    $cons_trunc = $conn->query($str_trunc);
    if(!$cons_trunc)
    {
        echo $conn->error."<br>";
    }
    while($obj_general = $cons_general->fetch_object())
    {
        if($obj_general->escalafon==1 || $obj_general->escalafon==2) //Personal obrero y administrativo
        {
            $str_ins = "INSERT INTO eval_evaluacion_evaluado (legajo_evaluado, id_evaluacion) VALUES (".$obj_general->legajo.", 8)";
            $cons_ins = $conn->query($str_ins);
            if($cons_ins)
            {
                $i++;
            }
            else
            {
                $errores++;
            }
        }
        else
        {
            if($obj_general->escalafon==3) //Funcionario (sin secretario de camara)
            {
                if($obj_general->cargoactu!=15)
                {
                    $str_ins = "INSERT INTO eval_evaluacion_evaluado (legajo_evaluado, id_evaluacion) VALUES (".$obj_general->legajo.", 9)";
                }
                else
                {
                    $str_ins = "INSERT INTO eval_evaluacion_evaluado (legajo_evaluado, id_evaluacion) VALUES (".$obj_general->legajo.", 10)";
                }
                $cons_ins = $conn->query($str_ins);
                if($cons_ins)
                {
                    $i++;
                }
                else
                {
                    $errores++;
                }
            }
        }
    }
    echo "Proceso finalizado. ".$i." registros insertados. ".$errores." errores";
}