<?php
session_set_cookie_params(0);
session_start();
require('system.config.php');
require('proxy.feriados.php');

$tarea = $_SERVER['REQUEST_METHOD'];
$tarea = isset($_POST["idCargoElimin"])?'DELETE':$_SERVER['REQUEST_METHOD'];

//echo $tarea;
switch ($tarea) {
	case 'GET':
        $act = isset($_GET["act"])?$_GET["act"]:"";
        switch ($act)
        {
            case "getFamiliares":
                getFamiliares();
                break;
            case "getRelaciones": 
                getRelaciones();
                break;
            case "getYears":
                getYears();
                break;    
            case "getCertificadosByYearEmployee": //Obtiene los certificados de un año especifico y de un agente específico
                getCertificadosByYearEmployee();
                break;    
            case "getCurrentYear":
                getCurrentYear("no");
                break;
            case "getObservacionesTesoreria":
                getObservacionesTesoreria();
                break;    
        }
        break;
	case 'POST':
        $act = isset($_POST["act"])?$_POST["act"]:"";
        switch ($act)
        {
            case "setFamiliar": //Asigna un familiar al agente
                setFamiliar();
                break;
            case "editFamiliar": //Editar un familiar existente
                editFamiliar();
                break;    
            case "deleteFamiliar":
                deleteFamiliar();
                break;   
            case "setCertificadosByYearEmployee": //Asigna certificados de un año especifico y de un agente específico
                setCertificadosByYearEmployee();
                break;  
            case "closeAllCertificados": //Pone en finalizado todos los certificados, para empezar a cargar los del año siguiente
                closeAllCertificados();
                break;       
        }
		break;
	case 'PUT':
		break;
	default:
		echo "({ failure: 'Error por default'})";
		break;
}


function getFamiliares() 
{
	global $conn;
    $data = array();
    $arr = array();
    $nbrows = 0;
    if(isset($_GET["legajo"]))
	{
        $str_fam = "SELECT fam_familiares.*, fam_relacion.nombre as relacion_nombre, nivel_estudios.id as nivel_estudios, general.observaciones_tesoreria FROM fam_familiares LEFT JOIN fam_relacion ON fam_familiares.relacion=fam_relacion.id LEFT JOIN nivel_estudios ON fam_familiares.nivel_estudios=nivel_estudios.id LEFT JOIN general ON fam_familiares.legajo=general.legajo WHERE fam_familiares.legajo=".$_GET["legajo"];
        
    
        $res_fam = $conn->query($str_fam);
        $nbrows = $res_fam->num_rows;
        if($res_fam)
        {
            while ($obj = $res_fam->fetch_object())
            {
                $str_cert = "SELECT * FROM fam_certificados WHERE status='actual' AND id_familiar=".$obj->id;
                $cons_cert = $conn->query($str_cert);
                if($cons_cert->num_rows>0)
                {
                    $obj_cert = $cons_cert->fetch_object();
                    $obj->id_certificado = $obj_cert->id;
                    $obj->certificado_inicio=$obj_cert->certificado_inicio;
                    $obj->certificado_fin=$obj_cert->certificado_fin;
                    $obj->guardapolvo=$obj_cert->guardapolvo;
                    $obj->anio=$obj_cert->anio;
                }
                else
                {
                    $obj->id_certificado = 0;
                    $obj->certificado_inicio = 0;
                    $obj->certificado_fin = 0;
                    $obj->guardapolvo = 0;
                    $obj->anio = 0;
                }
                $arr[] = $obj;
            }
            $data["success"] = true;
            $data["total"] = $nbrows;
            $data["results"] = json_encode($arr);
        }
        else
        {
            $data["success"] = false;
            $data["error"] = $conn->error;
        }
    }
    echo "({ total: ".$nbrows.",  results: ".json_encode($arr)."})";
    //echo json_encode($data);
}

function getRelaciones() {
    global $conn;
    $data = array();
    $arr = array();
    $str_rel = "SELECT * FROM fam_relacion ORDER BY id ASC";
    $res_rel = $conn->query($str_rel);
    $nbrows = $res_rel->num_rows;
    if($res_rel)
    {
        while ($obj = $res_rel->fetch_object())
        {
            $arr[] = $obj;
        }
        $data["success"] = true;
        $data["total"] = $nbrows;
        $data["results"] = json_encode($arr);
    }
    else
    {
        $data["success"] = false;
        $data["error"] = $conn->error;
    }
    echo "({ total: ".$nbrows.",  results: ".json_encode($arr)."})";
    //echo json_encode($data);
}

function getYears()
{
    global $conn;
    

    $arr = array();
    $i=1;
    $anio_hoy = date("Y");
    //Empiezo a guardar certificados desde el año 2017 en adelante.
    $obj = new stdClass();
    $obj->id="0";
    $obj->anio = "Elija un periodo";
    $arr[] = $obj;
    $j=1;
    for($i=2017;$i<=$anio_hoy;$i++)
    {
        $obj = new stdClass();
        $obj->id=$j;
        $obj->anio = $i;
        $arr[] = $obj;
        $j++;
    }
    
    echo "({results: ".json_encode($arr)."})";
}

function getCertificadosByYearEmployee()
{
    global $conn;
    $str_cons_cert = "SELECT * FROM emp_certificados WHERE anio='".$_GET["anio"]."' AND legajo=".$_GET["legajo"];
    $res_cons_cert = $conn->query($str_cons_cert);
    $arr = array();
    $i=0;
    while($obj = $res_cons_cert->fetch_object())
    {
        $arr[] = $obj;
    }
    echo "({results: ".json_encode($arr)."})";
}

function getCurrentYear($return_value="no")
{
    global $conn;
    $data = array();
    $str_exist = "SELECT * FROM fam_certificados";
    $cons_exist = $conn->query($str_exist);
    if($cons_exist->num_rows==0)
    {
        $data["success"] = true;
        $data["msg"] = "No hay ningun certificado cargado.";
        $data["actual"] = "vacio";
        $data["siguiente"] = date("Y");
    }
    else
    {
        $str_cons_cert = "SELECT MAX(anio) as ultimo_anio FROM fam_certificados WHERE status='actual'";
        $res_cons_cert = $conn->query($str_cons_cert);
        
        if($res_cons_cert->num_rows>0) //si la consulta no fue vacía
        {
            $obj = $res_cons_cert->fetch_object();
            if($obj->ultimo_anio) //Si hay un ultimo año, o sea, no es null
            {
                $data["success"] = true;
                $data["msg"] = "Existe un periodo en curso";
                $data["actual"] = $obj->ultimo_anio;
                $data["siguiente"] = $obj->ultimo_anio;
            }
            else //Si no, es porque estan todos finalizados. Obtengo el último finalizado
            {
                $str_cons_cert = "SELECT MAX(anio) as ultimo_anio FROM fam_certificados WHERE status='finalizado'";
                $res_cons_cert = $conn->query($str_cons_cert);
                $obj = $res_cons_cert->fetch_object();
                $data["success"] = true;
                $data["msg"] = "Todos los periodos estan finalizados. El ultimo periodo es";
                $data["actual"] = $obj->ultimo_anio;
                $data["siguiente"] = $obj->ultimo_anio+1;
            }
        }
        else
        {
            $data["success"] = false;
            $data["actual"] = "vacio";
        }
    }
    if($return_value=="no")
    {
        echo json_encode($data);
    }
    else
    {
        return $data["siguiente"];
    }
}

function getObservacionesTesoreria()
{
    global $conn;
    $data = array();
    $str_obs = "SELECT observaciones_tesoreria, susp_asignacion FROM general WHERE legajo=".$_GET["legajo"];
    //echo $str_obs;
    $cons_obs = $conn->query($str_obs);
    if($cons_obs->num_rows==0)
    {
        $data["success"] = true;
        $data["msg"] = "No hay observaciones para el legajo seleccionado.";
        $data["observaciones_tesoreria"] = "";
    }
    else
    {
        $obj = $cons_obs->fetch_object();
        $data["success"] = true;
        $data["msg"] = "Existen observaciones cargadas para el legajo seleccionado.";
        $data["observaciones_tesoreria"] = stripslashes($obj->observaciones_tesoreria);
        $data["susp_asignacion"] = $obj->susp_asignacion;
    }
    echo json_encode($data);
}

function setFamiliar() 
{
	global $conn;
    $data = array();
    $conn->autocommit(false);
    $discap = $_POST["discapacitado"]=="on"?1:0;
    try
    {
        if($_POST["relacion"]==3 || $_POST["relacion"]==4) //Si es padre o madre, controlo que no hayan cargado 2
        {
            $str_cons = "SELECT count(*) as cantidad_padres FROM fam_familiares WHERE legajo=".$_POST["legajo"]." AND (relacion=1 OR relacion=2)";
            $res_cons = $conn->query($str_cons);
            $obj_cons = $res_cons->fetch_object();
            if($obj_cons->cantidad_padres>1)
            {
                throw new Exception("Ya se cargaron los dos padres para el agente.");
            }
            $nivel_estudios= isset($_POST["nivel_estudios"]) && $_POST["nivel_estudios"]!=""?$_POST["nivel_estudios"]:0;
            $fecha_nacimiento= isset($_POST["fecha_nac"])?setFecha($_POST["fecha_nac"]):"";
            if($fecha_nacimiento!="")
            {
                $str_ins = "INSERT INTO fam_familiares (legajo, relacion, nombre, apellido, dni, fecha_nac, nivel_estudios, discapacitado) VALUES (".$_POST["legajo"].", ".$_POST["relacion"].", UPPER('".$_POST["nombre"]."'), UPPER('".$_POST["apellido"]."'), ".$_POST["dni"].", '".$fecha_nacimiento."', ".$nivel_estudios.", ".$discap.")";
            }
            else
            {
                $str_ins = "INSERT INTO fam_familiares (legajo, relacion, nombre, apellido, dni, nivel_estudios, discapacitado) VALUES (".$_POST["legajo"].", ".$_POST["relacion"].", UPPER('".$_POST["nombre"]."'), UPPER('".$_POST["apellido"]."'), ".$_POST["dni"].", ".$nivel_estudios.", ".$discap.")";
            }
            $res_ins = $conn->query($str_ins);
            if($res_ins)
            {
                $data["success"] = true;
                $data["msg"] = "Familiar cargado correctamente";
                $conn->commit();
                // Escribir log
                $leg = isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
                escribir_log("personal", $leg, $_POST["legajo"], "fam_familiares", 1, $str_ins, date("Y-m-d H:i:s"));
            }
            else
            {
                throw new Exception("Error en la base de datos: ".$conn->error." Consulta: ".$str_ins);
            }
        }
        else
        {
            $nivel_estudios= isset($_POST["nivel_estudios"]) && $_POST["nivel_estudios"]!=""?$_POST["nivel_estudios"]:0;
            $fecha_nacimiento= isset($_POST["fecha_nac"])?setFecha($_POST["fecha_nac"]):"";
            if($fecha_nacimiento!="")
            {
                $str_ins = "INSERT INTO fam_familiares (legajo, relacion, nombre, apellido, dni, fecha_nac, nivel_estudios, discapacitado) VALUES (".$_POST["legajo"].", ".$_POST["relacion"].", UPPER('".$_POST["nombre"]."'), UPPER('".$_POST["apellido"]."'), ".$_POST["dni"].", '".$fecha_nacimiento."', ".$nivel_estudios.", ".$discap.")";
            }
            else
            {
                $str_ins = "INSERT INTO fam_familiares (legajo, relacion, nombre, apellido, dni, nivel_estudios, discapacitado) VALUES (".$_POST["legajo"].", ".$_POST["relacion"].", UPPER('".$_POST["nombre"]."'), UPPER('".$_POST["apellido"]."'), ".$_POST["dni"].", ".$nivel_estudios.", ".$discap.")";
            }
            
            $res_ins = $conn->query($str_ins);
            if($res_ins)
            {
                $id_fam = $conn->insert_id;
                $data["success"] = true;
                $data["msg"] = "Familiar cargado correctamente";
                $conn->commit();
                // Escribir log
                $leg = isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
                escribir_log("personal", $leg, $_POST["legajo"], "fam_familiares", 1, $str_ins, date("Y-m-d H:i:s"));
            }
            else
            {
                throw new Exception("Error en la base de datos: ".$conn->error." Consulta: ".$str_ins);
            }
            if($_POST["relacion"]==2) //Si se cargaron hijos, cargo en la tabla certificados si se presentaron o no y el guardapolvo
            {
                $cert_inicio = $_POST["certificado_inicio"]=="on"?1:0;
                $cert_fin = $_POST["certificado_fin"]=="on"?1:0;
                $guardap = $_POST["guardapolvo"]=="on"?1:0;
    
                //Obtengo el año actual en el cual se están cargando los certificados
                $currentYear = getCurrentYear("si");
                            
                $str_cert = "INSERT INTO fam_certificados (id_familiar, certificado_inicio, certificado_fin, guardapolvo, anio) VALUES (".$id_fam.", ".$cert_inicio.", ".$cert_fin.", ".$guardap.", '".$currentYear."')";
                $res_cert = $conn->query($str_cert);    
                if(!$res_cert)
                {
                    throw new Exception("Error en la base de datos: ".$conn->error." Consulta: ".$str_cert);
                }
                $data["success"] = true;
                $data["msg"] = "Familiar cargado correctamente";
                $conn->commit();
                //Escribir log
                $leg = isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
                escribir_log("personal", $leg, $_POST["legajo"], "fam_certificados", 1, $str_cert, date("Y-m-d H:i:s"));
            }
        }
    }
    catch(Exception $e)
    {
        $conn->rollBack();
        $data["success"] = false;
        $data["error"] = $e->getMessage();
    }
    echo json_encode($data);
}

function editFamiliar() 
{
    global $conn;
    $data = array();
    $discap = $_POST["discapacitado"]=="on"?1:0;
    if($_POST["relacion"]==3 || $_POST["relacion"]==4) //Si es padre o madre, controlo que no hayan cargado 2
    {
        $str_cons = "SELECT count(*) as cantidad_padres FROM fam_familiares WHERE legajo=".$_POST["legajo"]." AND (relacion=1 OR relacion=2)";
        $res_cons = $conn->query($str_cons);
        $obj_cons = $res_cons->fetch_object();
        if($obj_cons->cantidad_padres>1)
        {
            $data["success"] = false;
            $data["error"] = "Ya se cargaron los dos padres para el agente.";
        }
        else
        {
            $str_add = "";
            $str_add.= isset($_POST["nivel_estudios"])?", nivel_estudios=".$_POST["nivel_estudios"]:"";
            $str_add.= isset($_POST["fecha_nac"])?", fecha_nac='".setFecha($_POST["fecha_nac"])."'":"";
            $str_upd = "UPDATE fam_familiares SET relacion=".$_POST["relacion"].", nombre=UPPER('".$_POST["nombre"]."'), apellido=UPPER('".$_POST["apellido"]."'), dni=".$_POST["dni"]." ".$str_add.", discapacitado=".$discap." WHERE id=".$_POST["id_fam"];
            
            $res_upd = $conn->query($str_upd);
            if($res_upd)
            {
                $data["success"] = true;
                $data["msg"] = "Familiar editado correctamente";

                //Escribir log
                $leg = isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
                //$str_limpia = addslashes($str_upd);
                //$str_log = "INSERT INTO usuarios_log (legajo, tabla, log, fecha) VALUES (".$leg.", 'fam_familiares', '".$str_limpia."', '".date("Y-m-d H:i:s")."')";
                //$res_log = $conn->query($str_log);
                escribir_log("personal", $leg, 0, "fam_familiares", 2, $str_upd, date("Y-m-d H:i:s"));
            }
            else
            {
                $data["success"] = false;
                $data["error"] = "Error en la base de datos: ".$conn->error;
            }
        }
    }
    else
    {
        $str_add = "";
        $str_add.= isset($_POST["nivel_estudios"])?", nivel_estudios=".$_POST["nivel_estudios"]:"";
        $str_add.= isset($_POST["fecha_nac"])?", fecha_nac='".setFecha($_POST["fecha_nac"])."'":"";
        $str_upd = "UPDATE fam_familiares SET relacion=".$_POST["relacion"].", nombre=UPPER('".$_POST["nombre"]."'), apellido=UPPER('".$_POST["apellido"]."'), dni=".$_POST["dni"]." ".$str_add.", discapacitado=".$discap." WHERE id=".$_POST["id_fam"];
        //echo $str_upd;
        $res_upd = $conn->query($str_upd);
        if($res_upd)
        {
            $data["success"] = true;
            $data["msg"] = "Familiar editado correctamente";

            //Escribir log
            $leg = isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
            //$str_limpia = addslashes($str_upd);
            //$str_log = "INSERT INTO usuarios_log (legajo, tabla, log, fecha) VALUES (".$leg.", 'fam_familiares', '".$str_limpia."', '".date("Y-m-d H:i:s")."')";
            //$res_log = $conn->query($str_log);
            escribir_log("personal", $leg, 0, "fam_familiares", 2, $str_upd, date("Y-m-d H:i:s"));
        }
        else
        {
            $data["success"] = false;
            $data["error"] = "Error en la base de datos: ".$conn->error;
        }
        if($_POST["relacion"]==2) //Si se cargaron hijos, actualizo en la tabla certificados si se presentaron o no
        {
            $cert_inicio = $_POST["certificado_inicio"]=="on"?1:0;
            $cert_fin = $_POST["certificado_fin"]=="on"?1:0;
            $guardap = $_POST["guardapolvo"]=="on"?1:0;

            //Obtengo el año actual en el cual se están cargando los certificados
            $currentYear = getCurrentYear("si");

            $str_ex = "SELECT * FROM fam_certificados WHERE id=".$_POST["id_certificado"]." AND anio='".$currentYear."'";
            $cons_ex = $conn->query($str_ex);
            if($cons_ex->num_rows>0)
            {
                $str_cert = "UPDATE fam_certificados SET certificado_inicio=".$cert_inicio.", certificado_fin=".$cert_fin.", guardapolvo=".$guardap." WHERE id=".$_POST["id_certificado"];
                $res_cert = $conn->query($str_cert);
                $leg = isset($_SESSION["legajo"])?$_SESSION["legajo"]:''; 
                escribir_log("personal", $leg, 0, "fam_certificados", 2, $str_cert, date("Y-m-d H:i:s"));
            }
            else
            {
                $str_cert = "INSERT INTO fam_certificados (id_familiar, certificado_inicio, certificado_fin, guardapolvo, anio, status) VALUES (".$_POST["id_fam"].", ".$cert_inicio.", ".$cert_fin.", ".$guardap.", '".$currentYear."','actual')";
                $res_cert = $conn->query($str_cert);     
                $leg = isset($_SESSION["legajo"])?$_SESSION["legajo"]:''; 
                escribir_log("personal", $leg, 0, "fam_certificados", 1, $str_cert, date("Y-m-d H:i:s"));
            }
        }
    }
    echo json_encode($data);
}

//Des asigna un cargo a un agente
function deleteFamiliar()
{
    global $conn;
    $data = array();    
    
    $str_del = "DELETE FROM fam_familiares WHERE id=".$_POST["idFam"];
    $cons_del = $conn->query($str_del);
    if($cons_del)
    {
        $str_del_cert = "DELETE FROM fam_certificados WHERE id_familiar=".$_POST["idFam"];
        $cons_del_cert = $conn->query($str_del_cert);
        $data["success"] = true;
        $data["msg"] = "El familiar fue eliminado correctamente.";

        //Escribir log
        $leg = isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
        //$str_limpia = addslashes($str_del_cert);
        //$str_log = "INSERT INTO usuarios_log (legajo, tabla, log, fecha) VALUES (".$leg.", 'fam_familiares', '".$str_limpia."', '".date("Y-m-d H:i:s")."')";
        //$res_log = $conn->query($str_log);
        escribir_log("personal", $leg, 0, "fam_certificados", 3, $str_del_cert, date("Y-m-d H:i:s"));
    }
    else
    {
        $data["success"] = false;
        $data["error"] = $conn->error;
    }
    echo json_encode($data);
}

//Asigna certificados a un agente para un año seleccionado
function setCertificadosByYearEmployee()
{
    global $conn;
    $data = array();
    $declaracion_jur_asig = isset($_POST["declaracion_jurada_asig"]) && $_POST["declaracion_jurada_asig"]=='on'?1:0;
    //$declaracion_jur_cony = isset($_POST["declaracion_jurada_conyuge"]) && $_POST["declaracion_jurada_conyuge"]=='on'?1:0;
    $cert_anses = isset($_POST["certificacion_anses"]) && $_POST["certificacion_anses"]=='on'?1:0;
    $suspendido = isset($_POST["suspendido_asignacion"]) && $_POST["suspendido_asignacion"]=='on'?1:0;
    //echo $suspendido;
    $data["success"] = true;
    if($_POST["anio"]!=0)//Si el año es distinto de 0, actualizo los certificados para el año seleccionado
    {
        //Controlo que exista registro para el año seleccionado, si no lo cargo
        $str_cons_anio = "SELECT * FROM emp_certificados WHERE anio='".$_POST["anio"]."' AND legajo=".$_POST["legajo"];
        $res_cons_anio = $conn->query($str_cons_anio);
        if($res_cons_anio->num_rows>0)
        {
            $str_upd_cert = "UPDATE emp_certificados SET asignacion_familiar=".$declaracion_jur_asig.", cert_neg_anses=".$cert_anses." WHERE anio='".$_POST["anio"]."' AND legajo=".$_POST["legajo"];
            $res_cons_cert = $conn->query($str_upd_cert);
            $leg = isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
            escribir_log("personal", $leg, $_POST["legajo"], "emp_certificados", 2, $str_upd_cert, date("Y-m-d H:i:s"));
        }
        else
        {
            $str_upd_cert = "INSERT INTO emp_certificados (legajo, asignacion_familiar, cert_neg_anses, anio) VALUES (".$_POST["legajo"].", ".$declaracion_jur_asig.", ".$cert_anses.", '".$_POST["anio"]."')"; 
            $res_cons_cert = $conn->query($str_upd_cert);
            $leg = isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
            escribir_log("personal", $leg, $_POST["legajo"], "emp_certificados", 1, $str_upd_cert, date("Y-m-d H:i:s"));
        }

        if($res_cons_cert)
        {
            $data["msg"] = "Certificados para el a&ntilde;o ".$_POST["anio"]." actualizados correctamente. ";
        }
        else
        {
            $data["success"] = false;
            $data["error"] = "No se pudieron actualizar los ceritificados para el agente. Error en la base de datos: ".$conn->error;   
        }
    }
    else
    {
        $data["msg"] = "";
    }
    

    //Actualizo las observaciones y suspension
    $str_upd_obs = "UPDATE general SET observaciones_tesoreria='".addslashes($_POST["observaciones_tesoreria"])."', susp_asignacion=".$suspendido." WHERE legajo=".$_POST["legajo"];
    $cons_upd_obs = $conn->query($str_upd_obs);
    if($data["success"] != true && !$cons_upd_obs)
    {
        $data["success"] = false;
        $data["error"] = "Error al guardar los datos en la base de datos. Error: ".$conn->error;
    }
    else
    {
        $data["msg"].= " Observaciones actualizadas correctamente.";

        //Escribir log
        $leg = isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
        
        escribir_log("personal", $leg, $_POST["legajo"], "general", 2, $str_upd_obs, date("Y-m-d H:i:s"));
    }
    echo json_encode($data);
}

function closeAllCertificados()
{
    global $conn;
    $data = array();
    
    
    //Finalizo todos los certificados
    $str_close = "UPDATE fam_certificados SET status='finalizado'";
    $cons_close = $conn->query($str_close);
    if($cons_close)
    {
        $data["success"] = true;
        $data["msg"] = "Certificados actuales finalizados correctamente";

        //Escribir log
        $leg = isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
        escribir_log("personal", $leg, 0, "fam_certificados", 2, $str_close, date("Y-m-d H:i:s"));
    }
    else
    {
        $data["success"] = false;
        $data["error"] = "Error al consultar la base de datos: ".$conn->error;
    }
    echo json_encode($data);
}
?>