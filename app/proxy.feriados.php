<?php
//session_set_cookie_params(1800);
//session_start();
require('system.config.php');
require('funciones.php');
$tarea = isset($_POST['tarea'])? $_POST['tarea']:(isset($_GET['tarea'])?$_GET['tarea']:'');
$anio = isset($_POST['anio'])? $_POST['anio']:'';

//echo $tarea;
switch ($tarea) {
	case 'diasLaborales':
		getDiasLaborales();
		break;

	case 'getFeriados':
		getFeriados();
		break;
	case 'setFeriados':
		setFeriados();
		break;
        default:
		# code...
		break;
}

function setFecha($fecha) {

	if (!empty($fecha)) {
		if (strpos($fecha, '/') > 0) {
			$arr = explode('/', $fecha);
		} else {
			$arr = explode('-', $fecha);
		}
		$str = $arr[2] . "-" . $arr[1] . "-" . $arr[0];
		return $str;
	} else {
		return "";
	}
}

function setFechaHora($fechahora) {

    if (!empty($fechahora)) 
    {
        $arr = explode(" ", $fechahora);
        if (strpos($arr[0], '/') > 0) 
        {
            $arr_2 = explode('/', $arr[0]);
        } 
        else 
        {
            $arr_2 = explode('-', $arr[0]);
        }
        $str = $arr_2[2] . "-" . $arr_2[1] . "-" . $arr_2[0]." ".$arr[1];
        return $str;
    } 
    else 
    {
        return "";
    }
}

function contarDiasLaborales($inicio, $fin){
//echo "entra al contardiaslaborales";
	//echo "Inicio: ".$inicio."<br>";

    $inicio = strtotime($inicio);
    $dia = 86400;
    $fin = strtotime($fin);
    // feriados = mes-año
    $feriados = array(
        '01-01',    // Año nuevo
        '04-05',    // semana santa
        '04-06',    // semana santa
        '05-01',    // dia del trabajador
        '06-29',    // dia de san pedro y san pablo
        '07-28',    // dia de la independencia
        '07-29',    // fiestas patrias
        '08-30',    // santa rosa
        '10-08',    // combate de angamos
        '11-01',    // todos los santos
        '12-08',    // inmaculada concepcion
        '12-25'     // navidad
    );
 
    $diaspasados = 0;
    //echo "Fechas ".$inicio." - ".$fin."<br>";
    while($inicio <= $fin){
        $mes_dia = date("m-d", $inicio);
        $dia_semana = date("N", $inicio) . " ";
        //echo $mes_dia . ' / ' . $dia_semana . '<br>';
        if (!in_array($mes_dia, $feriados) && $dia_semana < 6){ $diaspasados ++; }
        $inicio = $inicio + $dia;
    }
 
    return $diaspasados;
}

function getDiasLaborales() { 
    //echo "entra al getdiaslaborales";
	global $conn;
	$str = "SELECT * FROM feriados WHERE anio=".$_POST["anio"]." ORDER BY id DESC";
        //echo $str;
        
	$result = $conn->query($str);
	$nbrows = $result->num_rows;
	$arr = array();
	while ($obj = $result->fetch_object()){
		$arr[] = $obj;
                
	}
        
	echo "({ total: ".$nbrows.",  results: " . json_encode($arr) . "})";
}

function getFeriados() {
    //echo "entra al getferiados";
	global $conn;

	$str = "SELECT * FROM feriados WHERE YEAR(CURDATE())=anio ORDER BY fecha ASC";
	$result = $conn->query($str);
	$nbrows = $result->num_rows;
	$cad = "";
    $arr = array();
	while ($obj = $result->fetch_object()){
		
            $arr[]=$obj;
	}
	//echo json_encode($arr);
	echo " ({ total: ".$nbrows.", results: " . json_encode($arr) . "})";
}

function setFeriados() 
{
    //echo "entra al getferiados";
    global $conn;
    $registros = json_decode(stripslashes($_POST["records"])); 
    $registrosAEliminar = json_decode(stripslashes($_POST["del"]));
    //var_dump($registrosAEliminar);
    $data = array();
    $data["cambios"] = 0;
    $data["nuevos"] = 0;
    $data["eliminados"] = 0;
    foreach($registros as $registro)
    {
        //echo "ID registro: ".$registro->id;
        $date = new DateTime($registro->fecha);
        //echo $date->format('d-m-Y');
        $str_cons_ex = "SELECT * FROM feriados WHERE id=".$registro->id;
        //echo $str_cons_ex;
        if(!($res = $conn->query($str_cons_ex)))
        {
            $data["success"] = false;
            $data["error"] = $conn->error;
        }
        else
        {
            //echo "Cantidad de registros: ".$res->num_rows;
            if($res->num_rows>0)//Es porque existe el feriado (se ha modificado), por lo tanto actualizo
            {    
                $str = "UPDATE feriados SET anio=".substr($registro->fecha,0,4).", mes=".substr($registro->fecha,5,2).", dia=".substr($registro->fecha,8,2).", "
                        . " ocasion='".$registro->ocasion."', fecha='".setFecha($date->format('d-m-Y'))."' WHERE id=".$registro->id;
                if($conn->query($str))
                {
                    $data["success"] = true;
                    $data["cambios"]+= $conn->affected_rows;
                    $leg = isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
                    //escribir_log("personal", $leg, 0, "feriados", 2, $str, date("Y-m-d H:i:s"));
                    //echo json_encode($data);

                }
                else
                {
                    $data["success"] = false;
                    $data["error"] = $conn->error;
                    //echo json_encode($data);
                }
                //echo $registro->fecha;
            }
            else //se carg� un feriado nuevo
            {
                $str = "INSERT INTO feriados (anio, mes, dia, ocasion, fecha) VALUES (".substr($registro->fecha,0,4).", ".substr($registro->fecha,5,2).", ".substr($registro->fecha,8,2).", "
                        . " '".$registro->ocasion."', '".setFecha($date->format('d-m-Y'))."')";
                if($conn->query($str))
                {
                    $data["success"] = true;
                    $data["nuevos"]+= $conn->affected_rows;
                    $leg = isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
                    //escribir_log("personal", $leg, 0, "feriados", 1, $str, date("Y-m-d H:i:s"));
                    //echo json_encode($data);

                }
                else
                {
                    $data["success"] = false;
                    $data["error"] = $conn->error;
                    //echo json_encode($data);
                }
            }
        }
    }
    foreach($registrosAEliminar as $registroAEliminar)
    {
        
        $str_del = "DELETE FROM feriados WHERE id=".$registroAEliminar->id;
        if($conn->query($str_del))
        {
            $data["success"] = true;
            $data["eliminados"]+= $conn->affected_rows;
            $leg = isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
            //escribir_log("personal", $leg, 0, "feriados", 3, $str_del, date("Y-m-d H:i:s"));
        }
        else
        {
            $data["success"] = false;
            $data["error"]= $conn->error;
        }
        
        
    }
    echo json_encode($data);
}


?>