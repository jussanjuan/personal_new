<?php
session_set_cookie_params(0);
session_start();
require('system.config.php');
require('proxy.feriados.php');

$tarea = $_SERVER['REQUEST_METHOD'];
$tarea = isset($_POST["idFirmaDigitalElimin"])?'DELETE':$_SERVER['REQUEST_METHOD'];
$act = isset($_POST["act"])?$_POST["act"]:"";
//echo $tarea;
switch ($tarea) {
	case 'GET':
        $act = isset($_GET["act"])?$_GET["act"]:"";
        switch ($act)
        {
            case "":
                getFirmasDigitales();
                break;
            case "getTipoFirmaDigital":
                getTipoFirmaDigital();
                break;    
            case "getModalidadCapacitaciones":
                getModalidadCapacitaciones();
                break; 
            case "getOrigenCapacitaciones":
                getOrigenCapacitaciones();
                break;       
        }
        break;
	case 'POST':
        switch ($act)
        {
            case "setFirmaDigital": //Asigna una firma digital al agente. Mauro
                setFirmaDigital();
                break;
            case "editFirmaDigital": //Modifica una firma asignada al agente. Mauro
                editFirmaDigital();
                break;   
            case "deleteFirmaDigital": //Elimina una firma asignada al agente. Mauro
                deleteFirmaDigital();
                break;     
        }
		break;
	case 'PUT':
		break;
	case 'DELETE':
        break;
	default:
		echo "({ failure: 'Error por default'})";
		break;
}


function getFirmasDigitales() 
{
	global $conn;
    $arr = array();
    $nbrows = 0;
	if (isset($_GET["legajo"])) 
    {
		$str = "SELECT fd.*, fdt.nombre as tipoFirma, g.apellido, g.nombre FROM firmas_digitales fd LEFT JOIN firmas_digitalestipo fdt ON fd.tipo=fdt.id LEFT JOIN general g ON fd.legajo=g.legajo WHERE fd.legajo=".$_GET["legajo"]." ORDER BY fd.fecha_alta DESC";
        $result = $conn->query($str);
        $nbrows = $result->num_rows;
        while ($obj = $result->fetch_object())
        {
            $arr[] = $obj;
        }
	}
	echo "({ total: ".$nbrows.",  results: " . json_encode($arr) . "})";
}

function getTipoFirmaDigital() 
{
    global $conn;

    $str = "SELECT * FROM firmas_digitalestipo ORDER BY id ASC";
        
    $result = $conn->query($str);
    $nbrows = $result->num_rows;
    $arr = array();
    while ($obj = $result->fetch_object()){
        $arr[] = $obj;
    }

    echo "({ total: ".$nbrows.",  results: " . json_encode($arr) . "})";
}

function setFirmaDigital() 
{
	global $conn;
    $data = array();
    $ahora = date("Y-m-d H:i:s");
    $alta = new DateTime(setFechaHora($_POST["fechaAlta"]));
    $revocacion = date("Y-m-d H:i:s",strtotime($alta->format("Y-m-d H:i:s")."+ 2 year"));
    $str_ex = "SELECT * FROM firmas_digitales WHERE legajo=".$_POST["legajo"]." AND '".$ahora."' BETWEEN fecha_alta AND fecha_revocacion";
    $cons_ex = $conn->query($str_ex);
    if($cons_ex->num_rows>0)
    {
        $data["success"] = false;
        $data["error"] = "Error al cargar. El empleado ya tiene una firma digital vigente";
    }
    else
    {
        $str_ins = "INSERT INTO firmas_digitales (legajo, descripcion_firma, fecha_alta, fecha_revocacion, tipo) VALUES (".$_POST["legajo"].", '".$_POST["descripcionFirmaDigital"]."', '".$alta->format("Y-m-d H:i:s")."', '".$revocacion."', ".$_POST["tipoFirmaDigital"].")";
        $cons_ins = $conn->query($str_ins);
        if($cons_ins)
        {
            $data["success"] = true;
            $data["msg"] = "Firma cargada correctamente.";
    
            //Escribir log
            $legajo=isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
            escribir_log("personal", $legajo, $_POST["legajo"], "firmas_digitales", 1, $str_ins, date("Y-m-d H:i:s"));
    
        }
        else
        {
            $data["success"] = false;
            $data["error"] = $conn->error;
    
        }
    }
	echo json_encode($data);
}

function editFirmaDigital() 
{
    global $conn;
    $data = array();
    $idFirma = $_POST["idFirma"];
    $alta = new DateTime(setFechaHora($_POST["fechaAlta"]));
    //$revocacion = date("Y-m-d H:i:s",strtotime($alta->format("Y-m-d H:i:s")."+ 2 year"));
    $revocacion = new DateTime(setFechaHora($_POST["fechaRevocacion"]));
    $str_upd = "UPDATE firmas_digitales SET descripcion_firma='".$_POST["descripcionFirmaDigital"]."', fecha_alta='".$alta->format("Y-m-d H:i:s")."', fecha_revocacion='".$revocacion->format("Y-m-d H:i:s")."', tipo=".$_POST["tipoFirmaDigitalModificar"]." WHERE id=".$idFirma;
    
    $cons_upd = $conn->query($str_upd);
    if($cons_upd)
    {
        $data["success"] = true;
        $data["msg"] = "Firma modificada correctamente.";

        //Escribir log
        $legajo=isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
        escribir_log("personal", $legajo, 0, "firmas_digitales", 2, $str_upd, date("Y-m-d H:i:s"));

    }
    else
    {
        $data["success"] = false;
        $data["error"] = $conn->error;

    }
    echo json_encode($data);
}

//Des asigna una capacitacion a un agente
function deleteFirmaDigital()
{
    global $conn;
    $data = array();    
    $str_del = "DELETE FROM firmas_digitales WHERE id=".$_POST['idFirmaElimin'];
    $res_del = $conn->query($str_del);
    
    if($res_del)
    {
        $data["success"] = true;
        $data["msg"] = "Firma eliminada correctamente.";

        //Escribir log
        $legajo=isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
        escribir_log("personal", $legajo, 0, "firmas_digitales", 3, $str_del, date("Y-m-d H:i:s"));
    }
    else
    {
        $data["success"] = false;
        $data["error"] = $conn->error;

    }
    echo json_encode($data);
}

//Ingresa una sancion en la tabla de la bd. Mauro
function altaSancion()
{
    global $conn;
    $data = array();    
    $str_cons = "SELECT * FROM cargos WHERE cargo='".$_POST['nombreCargo']."'";
    //echo $str_cons;
    $res_cons = $conn->query($str_cons);
    if($res_cons->num_rows)
    {
         $data["success"] = false;
         $data["error"] = "Ya existe un cargo con el mismo nombre";
    }
    else
    {
        $str_cons = "SELECT * FROM cargos ORDER BY id DESC";
        $res_cons = $conn->query($str_cons);
        $fila_cons = $res_cons->fetch_object();
        
        $str_ins = "INSERT INTO cargos (nrocargo, cargo, escalafon) VALUES (".($fila_cons->nrocargo+1).", UPPER('".$_POST["nombreCargo"]."'), ".$_POST["escalafon"].")";
        $res_ins = $conn->query($str_ins);
        if(!$res_ins)
        {
            $data["success"] = false;
            $data["error"] = $conn->error;
        }
        else
        {
            $data["success"] = true;
            $data["msg"] = "Cargo dado de alta correctamente";
        }
        
    }
    echo json_encode($data);
    
}

//Elimina una sancion de la tabla de la bd. Mauro
function bajaSancion()
{
    global $conn;
    $data = array();    
    
    $str_nrocarg = "SELECT * FROM cargos WHERE id=".$_POST['cargoElimin'];
    $res_nrocarg = $conn->query($str_nrocarg);
    $fila_nrocarg = $res_nrocarg->fetch_object();
    //echo $str_nrocarg;
    $str_cons = "SELECT * FROM histocargo WHERE nrocargo=".$fila_nrocarg->nrocargo;
    //echo "<br/>".$str_cons;
    $res_ins = $conn->query($str_cons);
    if($res_ins->num_rows)
    {
        $data["success"] = false;
        $data["error"] = "Error al eliminar el cargo, hay agentes con dicho cargo asignado.";
    }
    else
    {
        $str_del = "DELETE FROM cargos WHERE id=".$_POST['cargoElimin'];
        //echo "<br/>".$str_del;
        $res_del = $conn->query($str_del);
        if(!$res_del)
        {
            $data["success"] = false;
            $data["error"] = "Error al eliminar el cargo. Intente nuevamente.";
        }
        else
        {    
            $data["success"] = true;
            $data["msg"] = "Cargo eliminado";
        }
    }
    echo json_encode($data);
    
}

//Modifica una sancion de la tabla de la bd. Mauro
function modificaSancion()
{
    global $conn;
    $data = array();    
    
    $str_carg = "SELECT * FROM cargos WHERE id=".$_POST['cargoModif'];
    $res_carg = $conn->query($str_carg);
    $fila_carg = $res_carg->fetch_object();
    //echo $str_nrocarg;
    
    $str_exist = "SELECT * FROM cargos WHERE cargo='".$_POST['nombreCargoModif']."' AND id<>".$_POST['cargoModif'];
    $res_exist = $conn->query($str_exist);
    
    //Si el nuevo nombre que quieren modificar es igual a algun otro lugar ya cargado, largo error.
    if($res_exist->num_rows > 0)
    {
        $data["success"] = false;
        $data["error"] = "El nombre ingresado ya existe.";
    }
    else
    {
        $str_del = "UPDATE cargos SET cargo=UPPER('".$_POST["nombreCargoModif"]."'), escalafon=".$_POST["escalafon"]." WHERE id=".$_POST['cargoModif'];
        //echo "<br/>".$str_del;
        $res_del = $conn->query($str_del);
        if(!$res_del)
        {
            $data["success"] = false;
            $data["error"] = "Error al modificar el cargo. Intente nuevamente.";
        }
        else
        {    
            $data["success"] = true;
            $data["msg"] = "Cargo modificado";
        }
    }
    echo json_encode($data);
    
}
?>