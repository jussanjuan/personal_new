<?php
session_set_cookie_params(0);
session_start();
require('system.config.php');
require('proxy.feriados.php');

$tarea = $_SERVER['REQUEST_METHOD'];
$tarea = isset($_POST["idLugarElimin"])?'DELETE':$_SERVER['REQUEST_METHOD'];
$act = isset($_POST["act"])?$_POST["act"]:"";
switch ($tarea) {
	case 'GET':
		getFuero();
		break;
	case 'POST':
		switch ($act)
        {
            case "": //Si es vacio, asigna un lugar al agente. Mauro
                setLugar();
                break;
            case "altaFuero": //Alta de un nuevo lugar en el sistema. Mauro
                altaFuero();
                break;
            case "bajaFuero": //Eliminar un lugar del sistema. Mauro
                bajaFuero();
                break;
            case "modificaFuero": //Modificar un lugar del sistema. Mauro
                modificaFuero();
                break;
        }
		break;
	case 'PUT':
		break;
	case 'DELETE':
                deleteLugar();
		break;
	default:
		echo "({ failure: 'Error por default'})";
		break;
}


function getFuero() 
{
    global $conn;

    if (isset($_GET["legajo"])) 
    {
        $str = "SELECT f.nombre, h.* FROM fuero f, histolugar h, lugar l WHERE h.legajo=".$_GET["legajo"]." AND h.nrolugar=l.nrolugar AND l.nro_fuero=f.id ORDER BY h.desde DESC";
    } 
    else 
    {
        $str = "SELECT * FROM fuero ORDER BY nombre ASC ";
    }
    
    $result = mysqli_query($conn, $str);
    $nbrows = mysqli_num_rows($result);
    $arr = array();
    while ($obj = mysqli_fetch_object($result)){
            $arr[] = $obj;

    }
    echo "({ total: ".$nbrows.",  results: " . json_encode($arr) . "})";
}

function setFuero() 
{
    global $conn;
    $flag = true;
    $err_flag = "Error:";
    $str = "UPDATE general SET lugaractu=".$_POST["lugaractu"]." WHERE legajo=".$_POST["legajo"];
    $result = mysqli_query($conn, $str);
    if(!$result)
    {
        $flag = false;
        $err_flag.=" 1";
    }

    // Escribir log
    //Cuando no esta seteada la variable de sesion, da error al insertar en la base de datos. Por lo tanto inicializo en 0 la variable. Mauro
    if(isset($_SESSION['legajo']))
        $legajo = $_SESSION['legajo'];
    else
        $legajo = 0;

    $str_log = "INSERT INTO usuarios_log (legajo, tabla, log, fecha) VALUES (".$legajo.", 'general', '".addslashes($str)."', '".date("Y-m-d H:i:s")."')";

    $result = mysqli_query($conn, $str_log);
    if(!$result)
    {
        $flag = false;
        $err_flag.=" 2";
        //echo "Error: ".mysqli_error($conn);
    }
    $str = "INSERT INTO histolugar(legajo, nrolugar, desde) ";
    $str.= "VALUES (".$_POST["legajo"].", ".$_POST["lugaractu"].", '".setFecha($_POST["desde"])."')";

    $result = mysqli_query($conn, $str);
    if(!$result)
    {
        $flag = false;
        $err_flag.=" 3";
    }

    // Escribir log
    $str_log= "INSERT INTO usuarios_log (legajo, tabla, log, fecha) VALUES (".$legajo.", 'histolugar', '".addslashes($str)."', '".date("Y-m-d H:i:s")."')";
    
    mysqli_query($conn, $str_log);
    if(!$result)
    {
        $flag = false;
        $err_flag.=" 4";//echo mysqli_error($conn);
    }

    if(!$flag)
        echo "({ success: false, error: ".$err_flag."})";
    else
        echo "({ success: true})";
}
//Des asigna un fuero a un lugar
function deleteFuero()
{
    global $conn;
    $data = array();    
    $str_cons = "SELECT * FROM histolugar WHERE id=".$_POST['idLugarElimin'];
    $res_cons = $conn->query($str_cons);
    $fila_cons = $res_cons->fetch_object();
    $legajo = $fila_cons->legajo;
    
    $str_cant = "SELECT * FROM histolugar WHERE legajo=".$legajo;
    $res_cant = $conn->query($str_cant);
    if($res_cant->num_rows < 2)
    {
        $data["success"] = false;
        $data["error"] = "No se puede eliminar, es el unico lugar que tiene asignado el agente ";
        echo json_encode($data);
    }
    else 
    {
        $str_del = "DELETE FROM histolugar WHERE id=".$_POST['idLugarElimin'];
        $res_del = $conn->query($str_del);
        if(!$res_del)
        {
            $data["success"] = false;
            $data["error"] = $conn->error;
            echo json_encode($data);
        }
        else
        {
            $str_search = "SELECT * FROM histolugar WHERE legajo=".$legajo." ORDER BY id DESC";
            $res_search = $conn->query($str_search);
            $fila_search = $res_search->fetch_object();
            $ult_lugar = $fila_search->nrolugar;
            $str_upd = "UPDATE general SET lugaractu=".$ult_lugar." WHERE legajo=".$legajo;
            if($conn->query($str_upd))
            {
                $data["success"] = true;
                $data["msg"] = "Lugar eliminado correctamente, asignado automaticamente el lugar anterior.";
                echo json_encode($data);
            }
            else
            {
                $data["success"] = false;
                $data["error"] = $conn->error;
                echo json_encode($data);
            }
        }
    }
    
}
//Agrega un fuero a la tabla de la bd. Mauro
function altaFuero()
{
    global $conn;
    $data = array();    
    $str_cons = "SELECT * FROM lugar WHERE nombrelugar='".$_POST['nombreLugar']."'";
    //echo $str_cons;
    $res_cons = $conn->query($str_cons);
    if($res_cons->num_rows)
    {
         $data["success"] = false;
         $data["error"] = "Ya existe un lugar con el mismo nombre";
    }
    else
    {
        $str_cons = "SELECT * FROM lugar ORDER BY id DESC";
        $res_cons = $conn->query($str_cons);
        $fila_cons = $res_cons->fetch_object();
        
        $str_ins = "INSERT INTO lugar (nrolugar, nombrelugar, domicilio, tel) VALUES (".($fila_cons->nrolugar+1).", UPPER('".$_POST["nombreLugar"]."'), "
                . "UPPER('".$_POST["domicilioLugar"]."'), '".$_POST["telLugar"]."')";
        $res_ins = $conn->query($str_ins);
        if(!$res_ins)
        {
            $data["success"] = false;
            $data["error"] = $conn->error;
        }
        else
        {
            $data["success"] = true;
            $data["msg"] = "Lugar dado de alta correctamente";
        }
        
    }
    echo json_encode($data);
    
}

//Elimina un lugar de la tabla de la bd. Mauro
function bajaLugar()
{
    global $conn;
    $data = array();    
    
    $str_nrolug = "SELECT * FROM lugar WHERE id=".$_POST['lugarElimin'];
    $res_nrolug = $conn->query($str_nrolug);
    $fila_nrolug = $res_nrolug->fetch_object();
    //echo $str_nrocarg;
    $str_cons = "SELECT * FROM histolugar WHERE nrolugar=".$fila_nrolug->nrolugar;
    //echo "<br/>".$str_cons;
    $res_ins = $conn->query($str_cons);
    if($res_ins->num_rows)
    {
        $data["success"] = false;
        $data["error"] = "Error al eliminar el lugar, hay agentes con dicho lugar asignado.";
    }
    else
    {
        $str_del = "DELETE FROM lugar WHERE id=".$_POST['lugarElimin'];
        //echo "<br/>".$str_del;
        $res_del = $conn->query($str_del);
        if(!$res_del)
        {
            $data["success"] = false;
            $data["error"] = "Error al eliminar el lugar. Intente nuevamente.";
        }
        else
        {    
            $data["success"] = true;
            $data["msg"] = "Lugar eliminado";
        }
    }
    echo json_encode($data);
    
}

//Modifica un lugar de la tabla de la bd. Mauro
function modificaLugar()
{
    global $conn;
    $data = array();    
    
    $str_lug = "SELECT * FROM lugar WHERE id=".$_POST['lugarModif'];
    $res_lug = $conn->query($str_lug);
    $fila_lug = $res_lug->fetch_object();
    //echo $str_nrocarg;
    
    $str_exist = "SELECT * FROM lugar WHERE nombrelugar='".$_POST['nombreLugarModif']."'";
    $res_exist = $conn->query($str_exist);
    
    //Si el nuevo nombre que quieren modificar es igual al que estaba, o es igual a algun otro lugar ya cargado, largo error.
    if($fila_lug->nombrelugar == $_POST["nombreLugarModif"] || $res_exist->num_rows > 0)
    {
        $data["success"] = false;
        $data["error"] = "El nombre ingresado ya existe.";
    }
    else
    {
        $str_del = "UPDATE lugar SET nombrelugar=UPPER('".$_POST["nombreLugarModif"]."') WHERE id=".$_POST['lugarModif'];
        //echo "<br/>".$str_del;
        $res_del = $conn->query($str_del);
        if(!$res_del)
        {
            $data["success"] = false;
            $data["error"] = "Error al modificar el lugar. Intente nuevamente.";
        }
        else
        {    
            $data["success"] = true;
            $data["msg"] = "Lugar modificado";
        }
    }
    echo json_encode($data);
    
}

?>