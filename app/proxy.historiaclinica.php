<?php
session_set_cookie_params(0);
session_start();

require('system.config.php');
require('proxy.feriados.php');

$request_method = $_SERVER['REQUEST_METHOD'];

switch ($request_method) 
{
	case "GET":	
		$tarea = $_GET['tarea'];
        switch($tarea)
    	{
            case 'getHistoriaClinica':
                getHistoriaClinica();
                break;
            
        }
        break;
    case "POST":	
		$tarea = $_POST['tarea'];
        switch($tarea)
    	{
            case 'saveRegistroHistoria':
                saveRegistroHistoria();
                break;
            case 'editRegistroHistoria':
                editRegistroHistoria();
                break;
            case 'deleteRegistroHistoria':
            	deleteRegistroHistoria();
            	break;        
            case 'deleteCertificadoHistoria':
            	deleteCertificadoHistoria();
            	break; 	
        }
        break;    
}

function getHistoriaClinica() 
{
	global $conn;
	$data = array();
	$arr = array();
	$str = "";
	try
	{
		$conn->query("SET NAMES UTF8");
		$strHist = "SELECT * FROM historiaClinica WHERE legajo=".$_GET["legajo"]." ORDER BY fecha_hora DESC";
		$consHist = $conn->query($strHist);
		if(!$consHist)
		{
			throw new Exception("Error al consultar la base de datos. Error DB: ".$conn->error);
		}
		$nbrows = $consHist->num_rows;
		
		while ($objHist = $consHist->fetch_object())
		{
			if($objHist->certificado==null)
			{
				$objHist->certificado = "No";
			}
			else
			{
				$objHist->certificado = '<a href="res/hist_clinica/'.$objHist->certificado.'" target="_blank"><img src="res/img/pdf.png" title="'.$objHist->certificado.'"></img>';
			}
			$arr[] = $objHist;
		}
		$data["success"] = true;
		$data["total"] = $nbrows;
		$data["results"] = $arr;
	}
	catch(Exception $e)
	{
		$data["success"] = false;
		$data["error"] = $e->getMessage();
	}
	echo json_encode($data);
}

function saveRegistroHistoria() 
{
	global $conn;
	$data = array();
	$arr = array();
	$str = "";
	$conn->autocommit(false);
	try
	{
		
		$conn->query("SET NAMES UTF8");
		$strIns = "INSERT INTO historiaClinica (legajo, observaciones, fecha_hora) VALUES (".$_POST["legajo"].", '".$_POST["observacionesHistoriaClinica"]."', '".setFechaHora($_POST["fechaHora"])."')";
		$consIns = $conn->query($strIns);
		
		if(!$consIns)
		{
			throw new Exception("Error al escribir la base de datos. Error DB: ".$conn->error);
		}
		if(isset($_FILES["certificadoHistoriaClinica"]) && $_FILES["certificadoHistoriaClinica"]["name"]!="")
		{
			if($_FILES["certificadoHistoriaClinica"]['size']>10485760) //Debe ser menor a 10 mb
			{
				throw new Exception("El archivo es muy grande");
			}
			if($_FILES["certificadoHistoriaClinica"]['type']!="application/pdf") //Debe ser PDF
			{
				throw new Exception("El formato del archivo es incorrecto. Subir solo PDF");
			}
			$id_hist = $conn->insert_id;
			//Carpeta de subida de los archivos
			$dir_subida = '../res/hist_clinica/';
			$nomb_archivo = "cert_".$id_hist.".pdf";
			$archivo_subido = $dir_subida.$nomb_archivo;
			if (move_uploaded_file($_FILES["certificadoHistoriaClinica"]["tmp_name"], $archivo_subido))
			{
				if(chmod($archivo_subido, 0755))
				{
				    $data["permisos"] = "Archivo subido correctamente, permisos asignados correctamente";
				}
				else
				{
				    $data["permisos"] = "Archivo subido correctamente, pero no se pudieron asignar permisos";
				}
				$str_upd = "UPDATE historiaClinica SET certificado='".$nomb_archivo."' WHERE id=".$id_hist;
				$cons_upd = $conn->query($str_upd);
				if(!$cons_upd)
				{
					throw new Exception("Error al escribir la base de datos. Error DB: ".$conn->error);
				}
			}
			else
			{
				throw new Exception("Error al subir el archivo");
			}
		}
		$conn->commit();
		$data["success"] = true;
		$data["msg"] = "Registro guardado correctamente.";
		$leg = isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
        escribir_log("personal", $leg, $_POST["legajo"], "historiaClinica", 1, $strIns, date("Y-m-d H:i:s"));
	}
	catch(Exception $e)
	{
		$conn->rollBack();
		$data["success"] = false;
		$data["error"] = $e->getMessage();
	}
	echo json_encode($data);
}

function editRegistroHistoria()
{
	global $conn;
	$data = array();
	$arr = array();
	$str = "";
	$conn->autocommit(false);
	try
	{
		$conn->query("SET NAMES UTF8");
		$strEdit = "UPDATE historiaClinica SET observaciones='".$_POST["observacionesHistoriaClinica"]."' WHERE id=".$_POST["idHistoria"];
		$consEdit = $conn->query($strEdit);
		if(!$consEdit)
		{
			throw new Exception("Error al escribir la base de datos. Error DB: ".$conn->error);
		}
		if(isset($_FILES["certificadoEditarHistoriaClinica"]) && $_FILES["certificadoEditarHistoriaClinica"]["name"]!="")
		{
			if($_FILES["certificadoEditarHistoriaClinica"]['size']>10485760) //Debe ser menor a 10 mb
			{
				throw new Exception("El archivo es muy grande. Debe pesar menos de 10 Mb.");
			}
			if($_FILES["certificadoEditarHistoriaClinica"]['type']!="application/pdf") //Debe ser PDF
			{
				throw new Exception("El formato del archivo es incorrecto. Subir solo PDF");
			}
			
			//Carpeta de subida de los archivos
			$dir_subida = '../res/hist_clinica/';
			$nomb_archivo = "cert_".$_POST["idHistoria"].".pdf";
			$archivo_subido = $dir_subida.$nomb_archivo;
			if (move_uploaded_file($_FILES["certificadoEditarHistoriaClinica"]["tmp_name"], $archivo_subido))
			{
				if(chmod($archivo_subido, 0755))
				{
				    $data["permisos"] = "Archivo subido correctamente, permisos asignados correctamente";
				}
				else
				{
				    $data["permisos"] = "Archivo subido correctamente, pero no se pudieron asignar permisos";
				}
				$str_upd = "UPDATE historiaClinica SET certificado='".$nomb_archivo."' WHERE id=".$_POST["idHistoria"];
				$cons_upd = $conn->query($str_upd);
				if(!$cons_upd)
				{
					throw new Exception("Error al escribir la base de datos. Error DB: ".$conn->error);
				}
			}
			else
			{
				throw new Exception("Error al subir el archivo");
			}
		}	
		$conn->commit();	
		$data["success"] = true;
		$data["msg"] = "Registro editado correctamente.";
		$leg = isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
        escribir_log("personal", $leg, $_POST["legajo"], "historiaClinica", 2, $strEdit, date("Y-m-d H:i:s"));
	}
	catch(Exception $e)
	{
		$data["success"] = false;
		$data["error"] = $e->getMessage();
	}
	echo json_encode($data);
}

function deleteRegistroHistoria()
{
	global $conn;
	$data = array();
	$arr = array();
	$str = "";
	try
	{
		$conn->query("SET NAMES UTF8");
		$strDel = "DELETE FROM historiaClinica WHERE id=".$_POST["idHistoria"];
		$consDel = $conn->query($strDel);
		if(!$consDel)
		{
			throw new Exception("Error al escribir la base de datos. Error DB: ".$conn->error);
		}
		$dir_subida = '../res/hist_clinica/';
		$nomb_archivo = "cert_".$_POST["idHistoria"].".pdf";
		$archivo_subido = $dir_subida.$nomb_archivo;
		$del = unlink($archivo_subido);
		if(!$del)
		{
			throw new Exception("Error al borrar el certificado: ".$archivo_subido);
		}
		$data["success"] = true;
		$data["msg"] = "Registro eliminado correctamente. ".$archivo_subido;
		$leg = isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
        escribir_log("personal", $leg, $_POST["legajo"], "historiaClinica", 3, $strDel, date("Y-m-d H:i:s"));
	}
	catch(Exception $e)
	{
		$data["success"] = false;
		$data["error"] = $e->getMessage();
	}
	echo json_encode($data);
}

function deleteCertificadoHistoria()
{
	global $conn;
	$data = array();
	$arr = array();
	$str = "";
	try
	{
		$conn->query("SET NAMES UTF8");
		$strDel = "UPDATE historiaClinica SET certificado=null WHERE id=".$_POST["idHistoria"];
		$consDel = $conn->query($strDel);
		if(!$consDel)
		{
			throw new Exception("Error al escribir la base de datos. Error DB: ".$conn->error);
		}
		$dir_subida = '../res/hist_clinica/';
		$nomb_archivo = "cert_".$_POST["idHistoria"].".pdf";
		$archivo_subido = $dir_subida.$nomb_archivo;
		$del = unlink($archivo_subido);
		if(!$del)
		{
			throw new Exception("Error al borrar el certificado: ".$archivo_subido);
		}
		$data["success"] = true;
		$data["msg"] = "Certificado eliminado correctamente.";
		$leg = isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
        escribir_log("personal", $leg, $_POST["legajo"], "historiaClinica", 2, $strDel, date("Y-m-d H:i:s"));
	}
	catch(Exception $e)
	{
		$data["success"] = false;
		$data["error"] = $e->getMessage();
	}
	echo json_encode($data);
}
?>