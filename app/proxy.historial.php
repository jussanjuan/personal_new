<?php
session_set_cookie_params(0);
session_start();

require('system.config.php');

$act = isset($_GET['act'])?$_GET["act"]:'';
$tarea = $_SERVER['REQUEST_METHOD'];
switch ($tarea) 
{
	case 'GET':
		switch($act)
        {
            case "":
            case "getDiasTrabajados":
                getDiasTrabajados();
                break;
            case 'getAllLicencias':
				getLicenciasByType();
				break;  	
			case 'getSolicitudes':	
				getSolicitudesByType();
				break;	 
        }
        break;  
	case 10:
		if ($_GET['opc'] == 'dias') 
		{
			getLicenciasByType($lic);
		} 
		else 
		{
			getCompensatoriaDiasFeria();
		}
		break;
	case 18:
		if ($_GET['opc'] == 'dias') 
		{
			getLicenciasByType($lic);
		} 
		else 
		{
			getDobleTurnoDiasDT();
		}
		break;
	    
	
	default:
		getLicenciasByType($lic);
		break;
}

function getLicenciasByType($lic = "all") 
{
	global $conn;
	$data = array();
	$arr = array();
	$str = "";
	try
	{
		$conn->query("SET NAMES UTF8");
		switch($lic)
		{
			case "all":
				$str = "SELECT l.*, tl.id as id_tipo_licencia, tl.nombre, tl.max_dias_anio, tl.dias_corridos, n.log as notificado FROM licencias l LEFT JOIN tipo_licencias tl ON l.tipo_licencia=tl.id LEFT JOIN notificaciones_licencias n ON l.id=n.id_licencia WHERE legajo=".$_GET["legajo"]." ORDER BY l.desde DESC";
				break;
			case 5: //Maternidad
				$str = "SELECT l.*, tl.id as id_tipo_licencia, tl.nombre, tl.max_dias_anio, tl.dias_corridos, lm.fpp, lm.certnac, lm.fp, lm.lactahasta, lm.lactahorario, n.log as notificado FROM licencias l LEFT JOIN tipo_licencias tl ON l.tipo_licencia=tl.id LEFT JOIN lic_maternidad lm ON l.id=lm.id_licencia LEFT JOIN notificaciones_licencias n ON l.id=n.id_licencia WHERE l.tipo_licencia=".$lic." AND l.legajo=".$_GET["legajo"]." ORDER BY l.desde DESC";
				break;	
			case 12: //Estudio
				$str = "SELECT l.*, tl.id as id_tipo_licencia, tl.nombre, tl.max_dias_anio, tl.dias_corridos, le.asignatura, le.carrera, le.fecha_examen, n.log as notificado FROM licencias l LEFT JOIN tipo_licencias tl ON l.tipo_licencia=tl.id LEFT JOIN lic_estudio le ON l.id=le.id_licencia LEFT JOIN notificaciones_licencias n ON l.id=n.id_licencia WHERE l.tipo_licencia=".$lic." AND l.legajo=".$_GET["legajo"]." ORDER BY l.desde DESC";
				break;	
			default:
				$str = "SELECT l.*, tl.id as id_tipo_licencia, tl.nombre, tl.max_dias_anio, tl.dias_corridos, n.log as notificado FROM licencias l LEFT JOIN tipo_licencias tl ON l.tipo_licencia=tl.id LEFT JOIN notificaciones_licencias n ON l.id=n.id_licencia WHERE tipo_licencia=".$lic." AND legajo=".$_GET["legajo"]." ORDER BY l.desde DESC";
				break;

		}
		//echo $str;
		$result = mysqli_query($conn, $str);
		if(!$result)
		{
			throw new Exception("Error al consultar la base de datos. Error DB: ".$conn->error);
		}
		$nbrows = mysqli_num_rows($result);
		
		while ($obj = mysqli_fetch_object($result))
		{
			$arr[] = $obj;
		}
		$data["success"] = true;
		$data["total"] = $nbrows;
		$data["results"] = $arr;
	}
	catch(Exception $e)
	{
		$data["success"] = false;
		$data["error"] = $e->getMessage();
	}
	echo json_encode($data);
}

function getSolicitudesByType($lic = "all") 
{
	global $conn;
	$data = array();
	$arr = array();
	$str = "";
	try
	{
		$conn->query("SET NAMES UTF8");
		switch($lic)
		{
			case "all":
				$str = "SELECT sl.*, tl.id as id_tipo_licencia, tl.nombre, tl.max_dias_anio, tl.dias_corridos, e.nombre as nombre_estado FROM solicitudLicencias sl LEFT JOIN tipo_licencias tl ON sl.tipo_licencia=tl.id LEFT JOIN general g ON sl.legajo=g.legajo LEFT JOIN estadoSolicitudLicencia e ON sl.estado=e.id WHERE sl.legajo=".$_GET["legajo"]." ORDER BY sl.desde DESC";
				break;
			case 5: //Maternidad
				$str = "SELECT l.*, tl.id as id_tipo_licencia, tl.nombre, tl.max_dias_anio, tl.dias_corridos, lm.fpp, lm.certnac, lm.fp, lm.lactahasta, lm.lactahorario FROM licencias l LEFT JOIN tipo_licencias tl ON l.tipo_licencia=tl.id LEFT JOIN lic_maternidad lm ON l.id=lm.id_licencia WHERE l.tipo_licencia=".$lic." AND l.legajo=".$_GET["legajo"]." ORDER BY l.desde DESC";
				break;	
			case 12: //Estudio
				$str = "SELECT l.*, tl.id as id_tipo_licencia, tl.nombre, tl.max_dias_anio, tl.dias_corridos, le.asignatura, le.carrera, le.fecha_examen FROM licencias l LEFT JOIN tipo_licencias tl ON l.tipo_licencia=tl.id LEFT JOIN lic_estudio le ON l.id=le.id_licencia WHERE l.tipo_licencia=".$lic." AND l.legajo=".$_GET["legajo"]." ORDER BY l.desde DESC";
				break;	
			default:
				$str = "SELECT l.*, tl.id as id_tipo_licencia, tl.nombre, tl.max_dias_anio, tl.dias_corridos FROM licencias l LEFT JOIN tipo_licencias tl ON l.tipo_licencia=tl.id WHERE tipo_licencia=".$lic." AND legajo=".$_GET["legajo"]." ORDER BY l.desde DESC";
				break;

		}
		//echo $str;
		$result = mysqli_query($conn, $str);
		if(!$result)
		{
			throw new Exception("Error al consultar la base de datos. Error DB: ".$conn->error);
		}
		$nbrows = mysqli_num_rows($result);
		
		while ($obj = mysqli_fetch_object($result))
		{
			$str_recep = "SELECT nombre, apellido FROM general WHERE legajo=".$obj->legajo_receptor;
			$cons_recep = $conn->query($str_recep);
			if($cons_recep)
			{
				$obj_recep = $cons_recep->fetch_object();
				$obj->nombre_receptor = $obj_recep->apellido.", ".$obj_recep->nombre;
			}
			else
			{
				$obj->nombre_receptor = "Error al cargar nombre";
			}
			$obj->nombre_estado = $obj->nombre_estado;
			$arr[] = $obj;
		}
		$data["success"] = true;
		$data["total"] = $nbrows;
		$data["results"] = $arr;
	}
	catch(Exception $e)
	{
		$data["success"] = false;
		$data["error"] = $e->getMessage();
	}
	echo json_encode($data);
}

function getCompensatoriaDiasFeria() 
{
	global $conn;
	$data = array();
	$arr = array();
	try
	{
		$conn->query("SET NAMES UTF8");
		$str = "SELECT * FROM lic_comp_dias WHERE legajo=".$_GET["legajo"]." ORDER BY id DESC";
		$result = mysqli_query($conn, $str);
		if(!$result)
		{
			throw new Exception("Error al consultar la base de datos. Error DB: ".$conn->error);
		}
		$nbrows = mysqli_num_rows($result);
		
		while ($obj = mysqli_fetch_object($result)){
			$arr[] = $obj;
		}
		$data["success"] = true;
		$data["total"] = $nbrows;
		$data["results"] = $arr;
	}
	catch(Exception $e)
	{
		$data["success"] = false;
		$data["error"] = $e->getMessage();
	}
	echo json_encode($data);
}

function getDobleTurnoDiasDT() 
{
	global $conn;
	$data = array();
	$arr = array();
	try
	{
		$conn->query("SET NAMES UTF8");
		$str = "SELECT * FROM lic_doble_turno_dias WHERE legajo=".$_GET["legajo"]." ORDER BY id DESC";
		$result = mysqli_query($conn, $str);
		if(!$result)
		{
			throw new Exception("Error al consultar la base de datos. Error DB: ".$conn->error);
		}
		$nbrows = mysqli_num_rows($result);
		
		while ($obj = mysqli_fetch_object($result)){
			$arr[] = $obj;
		}
		$data["success"] = true;
		$data["total"] = $nbrows;
		$data["results"] = $arr;
	}
	catch(Exception $e)
	{
		$data["success"] = false;
		$data["error"] = $e->getMessage();
	}
	echo json_encode($data);
}

function getDiasTrabajados() 
{
	global $conn;
	$data = array();
	$arr = array();
	try
	{
		$conn->query("SET NAMES UTF8");
		$str = "SELECT tl.nombre, dt.* FROM dias_trabajados dt LEFT JOIN tipo_licencias tl ON dt.tipo_licencia=tl.id WHERE legajo=".$_GET["legajo"]." AND (tipo_licencia=10 or tipo_licencia=18) ORDER BY desde DESC";
		$result = mysqli_query($conn, $str);
		if(!$result)
		{
			throw new Exception("Error al consultar la base de datos. Error DB: ".$conn->error);
		}
		$nbrows = mysqli_num_rows($result);
		
		while ($obj = mysqli_fetch_object($result)){
			$arr[] = $obj;
		}
		$data["success"] = true;
		$data["total"] = $nbrows;
		$data["results"] = $arr;
	}
	catch(Exception $e)
	{
		$data["success"] = false;
		$data["error"] = $e->getMessage();
	}
	echo json_encode($data);
}
?>