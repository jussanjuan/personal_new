<?php
session_set_cookie_params(0);
session_start();
require('system.config.php');
require('proxy.feriados.php');


$request_method = $_SERVER['REQUEST_METHOD'];

switch ($request_method) {
    case 'POST':
        $tarea = $_POST['tarea'];
        switch($tarea)
    	{
            case 'diasDisponibles':
                getDiasDisponibles();
                break;
            case 'setDiasCompensatoria':
                setDiasCompensatoria();
                break;
            case 'setDiasDobleTurno':
                setDiasDobleTurno();
                break;    
            case 'diasDisponiblesCompensatoria':
                getDiasDisponiblesCompensatoria();
                break;
            case 'diasDisponiblesDobleTurno':
                getDiasDisponiblesDobleTurno();
                break;    
            case 'saveLicencia':
                setLicencia();
                break;
            case 'saveComision':
                setComision();
                break;    
            case 'eliminarComision':
                eliminarComision();
                break;        
            case 'desisteLicencia':
                desisteLicencia();
                break; 
            case 'desisteDiasTrabajados':
                desisteDiasTrabajados();
                break; 
            case 'eliminarLicencia':
                eliminarLicencia();
                break;    
            case 'eliminarDiasTrabajados':
                eliminarDiasTrabajados();
                break;    
            case 'vaciarDiasTrabajados':
                vaciarDiasTrabajados();
                break;        
            case 'editLicenciaMaternidad':
                editLicenciaMaternidad();
                break;
            case 'editLicencia':
                editLicencia();
                break;    
            case 'editComision':
                editComision();
                break;    
            case 'saveSolicitudLicencia': 
                saveSolicitudLicencia();
                break; 
            case 'editSolicitudLicencia': 
                editSolicitudLicencia();
                break;        
            case 'deleteSolicitudLicencia': 
                deleteSolicitudLicencia();
                break;
            case 'confirmarSolicitudLicencia': 
                confirmarSolicitudLicencia();
                break;         
        }
        break;
    case 'GET':
        $tarea = $_GET['tarea'];

        switch($tarea)
        {
            case 'getLicenciasMasivasCompensatorias':
                getLicenciasMasivasCompensatorias();
                break;
            case 'getDatosAgenteById':
                getDatosAgenteById();
                break;  
            case 'getComisiones': 
                getComisiones();
                break;
            case 'getTipoLicenciaById':
                $idLic= isset($_GET["idLic"])?$_GET["idLic"]:0;
                getTipoLicenciaById($idLic);
                break;
            case 'getTipoLicencias':
                getTipoLicencias();
                break;    
        }
        break;
	default:
		# code...
		break;
}


function getDiasDisponibles() 
{
	global $conn;
    $data = array();

	// La cantidad de dias es por anio, al comenzar un anio nuevo, todo vuelve a cero
    //Consulta los dias tomados en el anio laboral actual
    try
    {
        $str = "SELECT * FROM licencias WHERE tipo_licencia=".$_POST['tipoLic']." AND legajo=".$_POST['legajo']." AND goce='S' AND YEAR(desde)=YEAR(CURDATE()) AND YEAR(hasta)=YEAR(CURDATE()) ORDER BY id DESC"; 
        $str_dias = "SELECT * FROM tipo_licencias WHERE id=".$_POST['tipoLic'];
        $cons_dias = $conn->query($str_dias);
        if(!$cons_dias || $cons_dias->num_rows==0)
        {
            throw new Exception("Error al leer la base de datos. Error BD: ".$conn->error);
        }
        $obj_dias = $cons_dias->fetch_object();
        $result = $conn->query($str);
        if(!$result)
        {
            throw new Exception("Error al leer la base de datos. Error BD: ".$conn->error);
        }
        $nbrows = $result->num_rows;
        $data["totalDias"] = $obj_dias->max_dias_anio;
        if ($nbrows > 0) 
        {
            $resultado = $result->fetch_object();
            $data["diasTomados"] = $resultado->suma_anio;
            $data["diasDisponibles"] = $obj_dias->max_dias_anio==0?"":($obj_dias->max_dias_anio-$resultado->suma_anio);
        } 
        else 
        {
            $data["diasTomados"] = 0;
            $data["diasDisponibles"] = $obj_dias->max_dias_anio;
        }
        $data["success"] = true;
    }
    catch(Exception $e)
    {
        $data["success"] = false;
        $data["error"] = $e->getMessage();
    }
	echo json_encode($data);
}

function getComisiones()
{
    global $conn;
    $data = array();
    $arr = array();
    $str_com = "SELECT * FROM comisiones WHERE legajo=".$_GET["legajo"];
    $cons_com = $conn->query($str_com);
    if(!$cons_com)
    {
        $data["success"] = false;
        $data["error"] = $conn->error;
    }
    else
    {
        $nbrows = $cons_com->num_rows;
        while($obj_com = $cons_com->fetch_object())
        {
            $arr[] = $obj_com;
        }
        $data["success"] = true;
        $data["results"] = $arr;
        $data["total"] = $nbrows;
    }
    echo json_encode($data);
}

function setDiasCompensatoria() 
{   
    global $conn;
    $data = array();
    $conn->autocommit(false);
    $leg = isset($_SESSION["legajo"])? $_SESSION["legajo"]:'';
    try
    {
        // Buscamos si la persona tiene dias de compensatoria 
        $str_cons = "SELECT * FROM dias_trabajados WHERE legajo=".$_POST["legajo"]." AND goce='S' AND (('".$_POST["anio"]."-01-".$_POST["desde"]."' BETWEEN desde AND hasta OR '".$_POST["anio"]."-01-".$_POST["hasta"]."' BETWEEN desde AND hasta) OR (desde BETWEEN '".$_POST["anio"]."-01-".$_POST["desde"]."' AND '".$_POST["anio"]."-01-".$_POST["hasta"]."' OR hasta BETWEEN '".$_POST["anio"]."-01-".$_POST["desde"]."' AND '".$_POST["anio"]."-01-".$_POST["hasta"]."'))";
        $res_cons = $conn->query($str_cons);

        if(!$res_cons)
        {
            throw new Exception("Error al consultar la base de datos. Intente nuevamente. Error BD: ".$conn->error);
        }
        if($res_cons->num_rows > 0) //Hay una compensatoria para ese a�o y esas fechas cargada, largo error
        {
            throw new Exception("No se puede cargar compensatoria, ya existe una compensatoria para la persona en las fechas seleccionadas.");
        }
        $str_ins = "INSERT INTO dias_trabajados (tipo_licencia, legajo, desde, hasta, goce) VALUES (10, ".$_POST["legajo"].", '".$_POST["anio"]."-01-".$_POST["desde"]."', '".$_POST["anio"]."-01-".$_POST["hasta"]."', 'S')";
        
        $res_ins = $conn->query($str_ins);
        if(!$res_ins)
        {
            throw new Exception("Error al consultar la base de datos. Intente nuevamente. Error BD: ".$conn->error);
        }
        //Escribo log
        
        escribir_log("personal", $leg, $_POST["legajo"], "dias_trabajados", 1, $str_ins, date("Y-m-d H:i:s"));
        $data["success"] = true;
        $data["msg"] = "Dias trabajados cargados con &eacute;xito";
        $conn->commit();
    }//End try
    catch(Exception $e)
    {
        $conn->rollBack();
        $data["success"] = false;
        $data["error"] = $e->getMessage();
    }
    echo json_encode($data);
}

function setDiasDobleTurno()
{
    global $conn;
    $data = array();
    $conn->autocommit(false);
    $leg = isset($_SESSION["legajo"])? $_SESSION["legajo"]:'';
    try
    {   //Buscamos si la persona tiene dias trabajados con doble turno 
        $str_cons = "SELECT * FROM dias_trabajados WHERE legajo=".$_POST["legajo"]." AND goce='S' AND (('".setFecha($_POST["DTDesde"])."' BETWEEN desde AND hasta OR '".setFecha($_POST["DTHasta"])."' BETWEEN desde AND hasta) OR (desde BETWEEN '".setFecha($_POST["DTDesde"])."' AND '".setFecha($_POST["DTHasta"])."' OR hasta BETWEEN '".setFecha($_POST["DTDesde"])."' AND '".setFecha($_POST["DTHasta"])."'))";
        //echo $str_cons;
        $res_cons = $conn->query($str_cons);
        if(!$res_cons)
        {
            throw new Exception("Error al consultar la base de datos. Intente nuevamente. Error BD: ".$conn->error);
        }
        if($res_cons->num_rows > 0) //Hay una licencia para  esas fechas cargadas, largo error
        {
            throw new Exception("No se puede cargar dias trabajados doble turno, ya se cargaron dias para la persona en las fechas seleccionadas.");
        }
        $str_ins = "INSERT INTO dias_trabajados (tipo_licencia, legajo, desde, hasta, goce) VALUES (18, ".$_POST["legajo"].", '".setFecha($_POST["DTDesde"])."', '".setFecha($_POST["DTHasta"])."', 'S')";
        $res_ins = $conn->query($str_ins);
        if(!$res_ins)
        {
            throw new Exception("Error al consultar la base de datos. Intente nuevamente. Error BD: ".$conn->error);
        }
        //Escribo log
        escribir_log("personal", $leg, $_POST["legajo"], "dias_trabajados", 1, $str_ins, date("Y-m-d H:i:s"));
        $data["success"] = true;
        $data["msg"] = "Dias cargados correctamente";
        $conn->commit();
    }
    catch(Exception $e)
    {
        $conn->rollBack();
        $data["success"] = false;
        $data["error"] = $e->getMessage();
    }
    echo json_encode($data);
}

function getDiasDisponiblesCompensatoria() 
{
	global $conn;
    $data = array();
    try
	{  
        // Obtener el total de dias de licencia compensatoria, sumando los dias disponibles de todos los anios 
        $str = "SELECT SUM(disponibles) AS total_dias_disponibles FROM dias_trabajados WHERE tipo_licencia=10 AND legajo=".$_POST["legajo"]." AND goce='S' ORDER BY desde DESC";
        $str2 = "SELECT SUM(totaldias) AS total_dias FROM dias_trabajados WHERE tipo_licencia=10 AND legajo=".$_POST["legajo"]." AND goce='S' ORDER BY desde DESC";
        $str3 = "SELECT SUM(dias_licencia) AS dias_tomados FROM licencias WHERE tipo_licencia=10 AND legajo=".$_POST["legajo"]." AND goce='S' ORDER BY desde DESC";
        $result = $conn->query($str);
        $result2 = $conn->query($str2);
        $result3 = $conn->query($str3);
        if(!$result || !$result2 || !$result3)
        {
            throw new Exception("Error al consultar la base de datos. Intente nuevamente. Error BD: ".$conn->error);
        }
        $resultado = $result->fetch_object();
        $resultado2 = $result2->fetch_object();
        $resultado3 = $result3->fetch_object();
        $data["success"] = true;
        $data["diasDisponibles"] = ($resultado->total_dias_disponibles == NULL) ? 0 : $resultado->total_dias_disponibles;
        $data["totalDias"] = ($resultado2->total_dias == NULL) ? 0 : $resultado2->total_dias;
        $data["diasTomados"] = ($resultado3->dias_tomados == NULL) ? 0 : $resultado3->dias_tomados;
    }
    catch(Exception $e)
    {
        $data = array();
        $data["success"] = false;
        $data["error"] = $e->getMessage();
    }
	echo json_encode($data);
}

function getDiasDisponiblesDobleTurno() 
{
    global $conn;
    $data = array();
    try
    {
        $str = "SELECT SUM(disponibles) AS total_dias_disponibles FROM dias_trabajados WHERE tipo_licencia=18 AND legajo=".$_POST["legajo"]." AND goce='S' ORDER BY desde DESC";
        $str2 = "SELECT SUM(totaldias) AS total_dias FROM dias_trabajados WHERE tipo_licencia=18 AND legajo=".$_POST["legajo"]." AND goce='S' ORDER BY desde DESC";
        $str3 = "SELECT SUM(dias_licencia) AS dias_tomados FROM licencias WHERE tipo_licencia=18 AND legajo=".$_POST["legajo"]." AND goce='S' ORDER BY desde DESC";
        $result = $conn->query($str);
        $result2 = $conn->query($str2);
        $result3 = $conn->query($str3);
        if(!$result || !$result2 || !$result3)
        {
            throw new Exception("Error al consultar la base de datos. Intente nuevamente. Error BD: ".$conn->error);
        }
        $resultado = $result->fetch_object();
        $resultado2 = $result2->fetch_object();
        $resultado3 = $result3->fetch_object();
        $data["success"] = true;
        $data["diasDisponibles"] = ($resultado->total_dias_disponibles == NULL) ? 0 : $resultado->total_dias_disponibles;
        $data["totalDias"] = ($resultado2->total_dias == NULL) ? 0 : $resultado2->total_dias;
        $data["diasTomados"] = ($resultado3->dias_tomados == NULL) ? 0 : $resultado3->dias_tomados;
    }
    catch(Exception $e)
    {
        $data = array();
        $data["success"] = false;
        $data["error"] = $e->getMessage();
    }
    echo json_encode($data);
}


function setLicencia() 
{
    global $conn;

    
    if(isset($_POST["mat_altaLicenciaCertificadoNacimiento"]))
    {
        $cert_nac = ($_POST["mat_altaLicenciaCertificadoNacimiento"] == "on") ? 'S' : 'N';
    }
    else
    {
        $cert_nac="S";
    }
    if(isset($_POST["altaLicenciaPresentismo"]))
    {
        $pierde_present = ($_POST["altaLicenciaPresentismo"] == "on")?1:0;
    }
    else
    {
        $pierde_present=0;
    }
    if(isset($_POST["notificacion_mail"]))
    {
        $notif = ($_POST["notificacion_mail"] == "on")?1:0;
    }
    else
    {
        $notif=0;
    }
    $causa = isset($_POST["altaLicenciaCausa"])?$_POST["altaLicenciaCausa"]:'';
    $observaciones = isset($_POST["altaLicenciaObservaciones"])?$_POST["altaLicenciaObservaciones"]:'';
    $expediente = isset($_POST["altaLicenciaExpediente"])?$_POST["altaLicenciaExpediente"]:'';
    $data = array();
    $leg = isset($_SESSION["legajo"])? $_SESSION["legajo"]:'';

    $conn->autocommit(false);
    $desde = $_POST["altaLicenciaDesde"];
    $hasta = $_POST["altaLicenciaHasta"];
    

    try
    {
        //Obtengo el nombre de la licencia
        $str_nombre_lic = "SELECT * FROM tipo_licencias WHERE id=".$_POST["tipoLic"];
        $cons_nombre_lic = $conn->query($str_nombre_lic);
        if(!$cons_nombre_lic)
        {
            throw new Exception("Error al leer la base de datos. Error BD: ".$conn->error);
        }
        $obj_nombre_lic = $cons_nombre_lic->fetch_object();
        $str_busq = "SELECT * FROM licencias l LEFT JOIN tipo_licencias tl ON l.tipo_licencia=tl.id WHERE legajo=".$_POST["legajo"]." AND goce='S' AND (('".setFecha($desde)."' BETWEEN desde AND hasta OR '".setFecha($hasta)."' BETWEEN desde AND hasta) OR (desde BETWEEN '".setFecha($desde)."' AND '".setFecha($hasta)."' OR hasta BETWEEN '".setFecha($desde)."' AND '".setFecha($hasta)."'))";
        $res_busq = $conn->query($str_busq);
        
        if($res_busq->num_rows>0)
        {
            $obj_busq = $res_busq->fetch_object();
            throw new Exception("Ya existe una licencia cargada en las fechas seleccionadas: ".$obj_busq->nombre." (".$obj_busq->desde." al ".$obj_busq->hasta.")");
        }
        
        //Inserto licencia en la tabla general de licencias
        $str_exist_lic = "SELECT * FROM licencias WHERE legajo=".$_POST["legajo"]." AND tipo_licencia=".$_POST["tipoLic"]." AND goce='S' AND YEAR(desde)=YEAR('".setFecha($desde)."') AND YEAR(hasta)=YEAR('".setFecha($hasta)."') ORDER BY id DESC";
        $cons_exist_lic = $conn->query($str_exist_lic);
        if($cons_exist_lic->num_rows==0) //Si es la primera licencia del anio para el empleado, inserto.
        {
            $str_ins_lic = "INSERT INTO licencias (tipo_licencia, legajo, desde, hasta, dias_licencia, suma_anio, causa, goce, observaciones, pierde_presentismo, expediente, created_at, updated_at) VALUES (".$_POST["tipoLic"].", ".$_POST["legajo"].", '".setFecha($desde)."', '".setFecha($hasta)."', ".$_POST["diasLicenciaAltaLicencia"].", ".$_POST["diasLicenciaAltaLicencia"].", '".$causa."', 'S', '".$observaciones."', ".$pierde_present.", '".$expediente."', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."')";
        }
        else //Si ya tiene una licencia en el anio, se le suman los dias
        {
            $fila_exist_lic = $cons_exist_lic->fetch_object();
            $str_ins_lic = "INSERT INTO licencias (tipo_licencia, legajo, desde, hasta, dias_licencia, suma_anio, causa, goce, observaciones, pierde_presentismo, expediente, created_at, updated_at) VALUES (".$_POST["tipoLic"].", ".$_POST["legajo"].", '".setFecha($desde)."', '".setFecha($hasta)."', ".$_POST["diasLicenciaAltaLicencia"].", ".($fila_exist_lic->suma_anio+$_POST["diasLicenciaAltaLicencia"]).", '".$causa."', 'S', '".$observaciones."', ".$pierde_present.", '".$expediente."', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."')";
        }
        
        $cons_ins_lic = $conn->query($str_ins_lic);
        if(!$cons_ins_lic)
        {
            throw new Exception("Error al cargar licencia. Error BD: ".$conn->error);
        }
        $id_lic = $conn->insert_id;   
        // Escribir log
        
        escribir_log("personal", $leg, $_POST["legajo"], $obj_nombre_lic->nombre, 1, $str_ins_lic, date("Y-m-d H:i:s"));
        
        //Abajo licencias especiales que requieren datos adicionales          
        switch ($_POST['tipoLic']) 
        {
            case 5: // Maternidad
                $str_lic = "INSERT INTO lic_maternidad (id_licencia, fpp, certnac, fp, lactahasta, lactahorario) VALUES (".$id_lic.", '".setFecha($_POST["mat_altaLicenciaFpp"])."', '".$cert_nac."', '".setFecha($_POST["mat_altaLicenciaFp"])."', '".setFecha($_POST["mat_altaLicenciaLactanciaHasta"])."', '".$_POST["mat_altaLicenciaLactanciaHorario"]."')";
                    
                $result = $conn->query($str_lic);
                if(!$result)
                {
                    throw new Exception("Error al cargar licencia. Error BD: ".$conn->error);
                }
                escribir_log("personal", $leg, $_POST["legajo"], $obj_nombre_lic->nombre, 1, $str_lic, date("Y-m-d H:i:s"));
                break; 
            case 10: // Compensatoria o Doble Turno (no se carga ningun dato extra, pero se restan los d�as disponibles que tiene el empleado)
            case 18: //Se descuentan desde los dias mas antiguos para adelante.
                //Ej: si un empleado tiene dias del 2015, 2016 y 2017, entonces se descuentan primero del 2015, luego del 2016 y as� sucesivamente.
                $str1 = "SELECT * FROM dias_trabajados WHERE tipo_licencia=".$_POST['tipoLic']." AND legajo=".$_POST["legajo"]." AND goce= 'S' ORDER BY desde ASC";
                $res = $conn->query($str1);

                //Sumo todos los dias disponibles, para ver si son mayores que los dias que se est� solicitando
                $str_sum = "SELECT SUM(disponibles) AS total_dias_disponibles FROM dias_trabajados WHERE tipo_licencia=".$_POST['tipoLic']." AND legajo=".$_POST["legajo"]." AND goce= 'S'";
                $cons_sum = $conn->query($str_sum);
                if(!$cons_sum)
                {
                    throw new Exception("Error al leer la base de datos. Error: ".$conn->error);
                }
                $fila_sum = $cons_sum->fetch_object();
                $pedidos = $_POST["diasLicenciaAltaLicencia"];
                $arr = array();
                $total = 0;

                if($fila_sum->total_dias_disponibles)
                {
                    if($fila_sum->total_dias_disponibles>=$pedidos)
                    {
                        while ($obj = $res->fetch_object())
                        {
                            $arr[] = $obj;
                            if ($obj->disponibles > 0) 
                            {
                                if ($pedidos <= $obj->disponibles) 
                                {
                                    $total = $obj->disponibles - $pedidos;
                                    $str_lic = "UPDATE dias_trabajados SET disponibles=".$total." WHERE id=".$obj->id;
                                    $conn->query($str_lic);
                                    break;
                                } 
                                else 
                                {
                                    $pedidos = $pedidos - $obj->disponibles;
                                    $str_lic = "UPDATE dias_trabajados SET disponibles=0 WHERE id=".$obj->id;
                                    $conn->query($str_lic);
                                }
                            }
                        }
                    } //end if($res_sum->total_dias_disponibles>=$pedidos)
                    else 
                    {
                        throw new Exception("No se puede pedir m&aacute;s d&iacute;as de los disponibles (".$fila_sum->total_dias_disponibles.")");
                    }
                }
                else 
                {
                    throw new Exception("No se puede cargar licencia porque no hay cargada ninguna compensatoria para el empleado");
                }
                escribir_log("personal", $leg, $_POST["legajo"], $obj_nombre_lic->nombre, 1, $str_lic, date("Y-m-d H:i:s"));
                break;     
            case 12: // Estudio
                $str_lic = "INSERT INTO lic_estudio (id_licencia, asignatura, carrera, fecha_examen) VALUES (".$id_lic.", '".$_POST["est_altaLicenciaAsignatura"]."', '".$_POST["est_altaLicenciaCarrera"]."', '".setFecha($_POST["est_altaLicenciaFechaExamen"])."')";
                
                $cons_estu = $conn->query($str_lic);
                if(!$cons_estu)
                {
                    throw new Exception("Error al cargar licencia. Error BD: ".$conn->error);
                }
                escribir_log("personal", $leg, $_POST["legajo"], "licencias", 1, $str_lic, date("Y-m-d H:i:s"));
                break;
        }//End switch
        $data["success"] = true;
        $data["msg"] = "Licencia cargada con &eacute;xito";
        $conn->commit();
        //Enviar notificacion por mail
        if($notif==1)
        {
            $envio_mail = sendMailCargaLicencia($_POST["legajo"], $id_lic);
            if(!$envio_mail["success"])
            {
                $data["msg"] = "Licencia cargada con &eacute;xito, pero hubo error al enviar notificaci&oacute;n por mail.";
            }
        }
    }//End try
    catch(Exception $e)
    {
        $conn->rollBack();
        $data["success"] = false;
        $data["error"] = $e->getMessage();
    }
    echo json_encode($data);
}

function setComision()//Son los mismos campos que una licencia, pero no es Licencia. Para separar conceptos se hace en una tabla aparte
{
    global $conn;
    $conn->autocommit(false);
    $data = array();
    $exp = $_POST["comisionExpediente"]==""?'NULL':$_POST["comisionExpediente"];
    try
    {
        $str_busq = "SELECT * FROM comisiones WHERE legajo=".$_POST["legajo"]." AND goce='S' AND (('".setFecha($_POST["comisionDesde"])."' BETWEEN desde AND hasta OR '".setFecha($_POST["comisionHasta"])."' BETWEEN desde AND hasta) OR (desde BETWEEN '".setFecha($_POST["comisionDesde"])."' AND '".setFecha($_POST["comisionHasta"])."' OR hasta BETWEEN '".setFecha($_POST["comisionDesde"])."' AND '".setFecha($_POST["comisionHasta"])."'))";
        $res_busq = $conn->query($str_busq);
        
        if($res_busq->num_rows>0)
        {
            throw new Exception("Ya existe una comisi&oacute;n cargada para ese rango de fechas");
        }

        $str = "INSERT INTO comisiones (legajo, desde, hasta, total_dias, causa, goce, expediente, observaciones) VALUES (".$_POST["legajo"].", '".setFecha($_POST["comisionDesde"])."', '".setFecha($_POST["comisionHasta"])."', ".$_POST["comisionTotalDias"].", '".$_POST["comisionCausa"]."', 'S', ".$exp.", '".$_POST["comisionObservaciones"]."')";
        $result = $conn->query($str);
        if(!$result)
        {
            throw new Exception($conn->error);
        }
        $conn->commit();
        $data["success"] = true;
        $data["msg"] = "Comisi&oacute;n cargada correctamente";
    }
    catch (Exception $e)
    {
        $conn->rollBack();
        $data["success"] = false;
        $data["error"] = $e->getMessage();
    }
    echo json_encode($data);
}

function eliminarComision() 
{
    global $conn;

    $data = array();
    $leg = isset($_SESSION["legajo"])? $_SESSION["legajo"]:'';

    $conn->autocommit(false);
    
    try
    {
        $str_sel = "SELECT * FROM comisiones WHERE id=".$_POST["id"];
        $cons_sel = $conn->query($str_sel);
        if(!$cons_sel)
        {
            throw new Exception("Error al consultar la base de datos. Detalle: ".$conn->error);
        }
        $obj_sel = $cons_sel->fetch_object();

        $str_del = "DELETE FROM comisiones WHERE id=".$_POST["id"];
        $res_del = $conn->query($str_del);
        if(!$res_del)
        {
            throw new Exception("Error al eliminar la comisi&oacute;n. Detalle: ".$conn->error);
        }
        
        $data["success"] = true;
        $data["msg"] = "Comisi&oacute;n eliminada con &eacute;xito";
        $conn->commit();
        escribir_log("personal", $leg, $obj_sel->legajo, "comisiones", 3, $str_del, date("Y-m-d H:i:s"));
    }//End try
    catch(Exception $e)
    {
        $conn->rollBack();
        $data["success"] = false;
        $data["error"] = $e->getMessage();
    }
    echo json_encode($data);
}

function editLicenciaMaternidad()
{
    global $conn;
    $conn->autocommit(false);
    $data = array();
    $leg = isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
    $registros = json_decode(stripslashes($_POST["records"])); 
    try
    {
        foreach($registros as $registro)
        {
            $fp = new DateTime($registro->fp);
            $lactahasta = new DateTime($registro->lactahasta);
            //echo "ID registro: ".$registro->id;
            $str_cons_ex = "SELECT * FROM lic_maternidad WHERE id_licencia=".$registro->id;
            $res = $conn->query($str_cons_ex);
            if($res->num_rows==0)
            {
                throw new Exception("Error al editar. No se encuentra la licencia en la base de datos.");
            }
            $str_upd = "UPDATE lic_maternidad SET fp='".setFecha($fp->format("d-m-Y"))."', lactahasta='".setFecha($lactahasta->format("d-m-Y"))."', lactahorario='".$registro->lactahorario."' WHERE id_licencia=".$registro->id;
            $cons_upd = $conn->query($str_upd);
            if(!$cons_upd)
            {
                throw new Exception("Error al consultar la base de datos. Error BD: ".$conn->error);
            }
            escribir_log("personal", $leg, 0, "lic_maternidad", 2, $str_upd, date("Y-m-d H:i:s"));
        }
        $data["success"] = true;
        $data["msg"] = "Cambios guardados correctamente";
        $conn->commit();
    }
    catch(Exception $e)
    {
        $conn->rollBack();
        $data["success"] = false;
        $data["error"] = $e->getMessage();
    }
    echo json_encode($data);
}

function editLicencia()
{
    global $conn;
    $conn->autocommit(false);
    $data = array();
    $leg = isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
    $presentismo = isset($_POST["editPierdePresentismo"]) && $_POST["editPierdePresentismo"]=='on'?1:0;
    try
    {
        $str_cons_ex = "SELECT * FROM licencias WHERE id=".$_POST["idEditLicencia"];
        $res = $conn->query($str_cons_ex);
        if($res->num_rows==0)
        {
            throw new Exception("Error al editar. No se encuentra la licencia en la base de datos.");
        }
        $str_upd = "UPDATE licencias SET causa='".$_POST["editCausa"]."', observaciones='".$_POST["editObservaciones"]."', expediente='".$_POST["editExpediente"]."', pierde_presentismo=".$presentismo.", updated_at='".date("Y-m-d H:i:s")."' WHERE id=".$_POST["idEditLicencia"];
        $cons_upd = $conn->query($str_upd);
        if(!$cons_upd)
        {
            throw new Exception("Error al escribir la base de datos. Error BD: ".$conn->error);
        }
        escribir_log("personal", $leg, $_POST["legajo"], "licencias", 2, $str_upd, date("Y-m-d H:i:s"));
        $data["success"] = true;
        $data["msg"] = "Cambios guardados correctamente";
        $conn->commit();
    }
    catch(Exception $e)
    {
        $conn->rollBack();
        $data["success"] = false;
        $data["error"] = $e->getMessage();
    }
    echo json_encode($data);
}

function editComision()
{
    global $conn;
    $conn->autocommit(false);
    $data = array();
    $leg = isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
    try
    {
        //Controlo que no haya cargada alguna licencia en la fecha editada
        $str_busq = "SELECT * FROM licencias WHERE legajo=".$_POST["legajo"]." AND goce='S' AND (('".setFecha($_POST["editarComisionDesde"])."' BETWEEN desde AND hasta OR '".setFecha($_POST["editarComisionHasta"])."' BETWEEN desde AND hasta) OR (desde BETWEEN '".setFecha($_POST["editarComisionDesde"])."' AND '".setFecha($_POST["editarComisionHasta"])."' OR hasta BETWEEN '".setFecha($_POST["editarComisionDesde"])."' AND '".setFecha($_POST["editarComisionHasta"])."'))";
        $res_busq = $conn->query($str_busq);
        
        if($res_busq->num_rows>0)
        {
            throw new Exception("No se puede cargar la comisi&oacute;n. Ya existe una licencia cargada para ese rango de fechas.");
        }
        $exp = $_POST["editarComisionExpediente"]==""?'NULL':$_POST["editarComisionExpediente"];
        $str_busq = "SELECT * FROM comisiones WHERE legajo=".$_POST["legajo"]." AND goce='S' AND id!=".$_POST["idLicencia"]." AND (('".setFecha($_POST["editarComisionDesde"])."' BETWEEN desde AND hasta OR '".setFecha($_POST["editarComisionHasta"])."' BETWEEN desde AND hasta) OR (desde BETWEEN '".setFecha($_POST["editarComisionDesde"])."' AND '".setFecha($_POST["editarComisionHasta"])."' OR hasta BETWEEN '".setFecha($_POST["editarComisionDesde"])."' AND '".setFecha($_POST["editarComisionHasta"])."'))";
        $res_busq = $conn->query($str_busq);
        
        if($res_busq->num_rows>0)
        {
            throw new Exception("Ya existe una comisi&oacute;n cargada para ese rango de fechas");
        }

        $str = "UPDATE comisiones SET desde='".setFecha($_POST["editarComisionDesde"])."', hasta='".setFecha($_POST["editarComisionHasta"])."', total_dias=".$_POST["editarComisionTotalDias"].", causa='".$_POST["editarComisionCausa"]."', goce='S', expediente=".$exp.", observaciones='".$_POST["editarComisionObservaciones"]."' WHERE id=".$_POST["idLicencia"];
        //echo $str;
        $result = $conn->query($str);
        if(!$result)
        {
            throw new Exception("Error al consultar la base de datos. Error BD: ".$conn->error);
        }
        $data["success"] = true;
        $data["msg"] = "Comisi&oacute;n editada correctamente.";
        $conn->commit();
    }
    catch(Exception $e)
    {
        $conn->rollBack();
        $data["success"] = false;
        $data["error"] = $e->getMessage();
    }
    echo json_encode($data);
}

function setMasivasCompensatorias()
{
    global $conn;
    //echo $tarea;
    $registros = json_decode(stripslashes($_POST["records"])); 
    $registrosAEliminar = json_decode(stripslashes($_POST["del"]));
    //var_dump($registrosAEliminar);
    $data = array();
    $data["nuevos"] = 0;
    $data["eliminados"] = 0;
    $data["modificados"] = 0;
    foreach($registros as $registro)
    {

        $str_cons_ex = "SELECT * FROM carga_masiva_comp WHERE id=".$registro->id;
        //echo $str_cons_ex;
        if(!($res = $conn->query($str_cons_ex)))
        {
            $data["success"] = false;
            $data["error"] = $conn->error;
        }
        else
        {
            if($res->num_rows==0)//Es porque la licencia es nueva (tiene id=0, nunca va a existir en la tabla de la bd)
            {
                $str = "INSERT INTO carga_masiva_comp (legajo, desde, hasta) VALUES (".$registro->legajo.", '".$registro->desde."', '".$registro->hasta."')";
                if($conn->query($str))
                {
                    $data["success"] = true;
                    $data["nuevos"]+= $conn->affected_rows;
                    
                    // Escribir log
                    $leg = isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
                    //$str_limpia = addslashes($str);
                    //$str_log = "INSERT INTO usuarios_log (legajo, tabla, log, fecha) VALUES (".$leg.", 'carga_masiva_comp', '".$str_limpia."', '".date("Y-m-d H:i:s")."')";
                    //$res_log = $conn->query($str_log);
                    escribir_log("personal", $leg, $registro->legajo, "carga_masiva_comp", 1, $str, date("Y-m-d H:i:s"));
                }
                else
                {
                    $data["success"] = false;
                    $data["error"] = $conn->error;
                    //echo json_encode($data);
                }
            }
            else
            {
                $str_upd = "UPDATE carga_masiva_comp SET legajo=".$registro->legajo.", desde='".$registro->desde."', hasta='".$registro->hasta."' WHERE id=".$registro->id;
                if($conn->query($str_upd))
                {
                    $data["success"] = true;
                    $data["modificados"]+= $conn->affected_rows;
                    
                    //Escribir log
                    $leg = isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
                    //$str_limpia = addslashes($str_upd);
                    //$str_log = "INSERT INTO usuarios_log (legajo, tabla, log, fecha) VALUES (".$leg.", 'carga_masiva_comp', '".$str_limpia."', '".date("Y-m-d H:i:s")."')";
                    //$res_log = $conn->query($str_log);
                    escribir_log("personal", $leg, $registro->legajo, "carga_masiva_comp", 2, $str_upd, date("Y-m-d H:i:s"));
                }
                else
                {
                    $data["success"] = false;
                    $data["error"] = $conn->error;
                    //echo json_encode($data);
                }
            }
        }
    }
    foreach($registrosAEliminar as $registroAEliminar)
    {
        
        $str_del = "DELETE FROM carga_masiva_comp WHERE id=".$registroAEliminar->id;
        if($conn->query($str_del))
        {
            $data["success"] = true;
            $data["eliminados"]+= $conn->affected_rows;

            // Escribir log
            $leg = isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
            //$str_limpia = addslashes($str_del);
            //$str_log = "INSERT INTO usuarios_log (legajo, tabla, log, fecha) VALUES (".$leg.", 'carga_masiva_comp', '".$str_limpia."', '".date("Y-m-d H:i:s")."')";
            //$res_log = $conn->query($str_log);
            escribir_log("personal", $leg, 0, "carga_masiva_comp", 3, $str_del, date("Y-m-d H:i:s"));
        }
        else
        {
            $data["success"] = false;
            $data["error"]= $conn->error;
        }
        
        
    }
    echo json_encode($data);
}

function sendMasivasCompensatorias()
{
    global $conn;
    //echo $tarea;
    $registros = json_decode(stripslashes($_POST["registros"])); 
    
    
    $data = array();
    $data["cargados"] = 0;
    $data["omitidos"] = 0;
    foreach($registros as $registro)
    {
        $reg_desde = new DateTime($registro->desde);
        $desde = $reg_desde->format("Y-m-d");
        $reg_hasta = new DateTime($registro->hasta);
        $hasta = $reg_hasta->format("Y-m-d");
        
        $str_cons = "SELECT * FROM lic_comp_dias2 WHERE legajo=".$registro->legajo." AND goce='S' AND (('".$desde."' BETWEEN desde AND hasta OR '".$hasta."' BETWEEN desde AND hasta) OR (desde BETWEEN '".$desde."' AND '".$hasta."' OR hasta BETWEEN '".$desde."' AND '".$hasta."'))";
        //echo $str;
        $res_cons = $conn->query($str_cons);
        
        if(!$res_cons)
        {
            $data["success"] = false;
            $data["error"] = "Error al consultar la base de datos. Intente nuevamente.";
            break;
        }
        else
        {
            if($res_cons->num_rows > 0) //Hay una compensatoria para ese a�o y esas fechas cargada, largo error
            {
                $data["omitidos"]++;
            }
            else
            {
                $str_ins = "INSERT INTO lic_comp_dias2 (legajo, desde, hasta, goce) VALUES (".$registro->legajo.", '".$desde."', '".$hasta."', 'S')";
                $res_ins = $conn->query($str_ins);
                if($res_ins)
                {
                    $data["success"] = true;
                    $data["cargados"]++;
                    
                    //Escribo log
                    $leg = isset($_SESSION["legajo"])? $_SESSION["legajo"]:'';
                    //$str_limpia = addslashes($str_ins);//Bug corregido Mauro
                    //$str_log = "INSERT INTO usuarios_log (legajo, tabla, log, fecha) VALUES (".$leg.", 'lic_comp_dias2', '".$str_limpia."', '".date("Y-m-d H:i:s")."')";
                    //$conn->query($str_log);
                    escribir_log("personal", $leg, $registro->legajo, "lic_comp_dias2", 1, $str_ins, date("Y-m-d H:i:s"));

                    //Borro la licencia de la tabla temporal
                    $str_del = "DELETE FROM carga_masiva_comp WHERE id=".$registro->id;
                    $cons_del = $conn->query($str_del);
                }
                else
                {
                    $data["omitidos"]++;
                }
            }
        }
    }
    echo json_encode($data);
}

function saveSolicitudLicencia() 
{
    global $conn;

    if(isset($_POST["pierdePresentismoSolicitud"]))
    {
        $pierde_present = ($_POST["pierdePresentismoSolicitud"] == "on")?1:0;
    }
    else
    {
        $pierde_present=0;
    }
    $causa = isset($_POST["solicitudCausa"])?addslashes($_POST["solicitudCausa"]):'';
    $observaciones = isset($_POST["solicitudObservaciones"])?addslashes($_POST["solicitudObservaciones"]):'';
    $expediente = isset($_POST["solicitudExpediente"])?$_POST["solicitudExpediente"]:'';
    $data = array();
    $leg = isset($_SESSION["legajo"])? $_SESSION["legajo"]:'';
    $domicilio = addslashes(trim($_POST["domicilioSolicitante"]));
    $conn->autocommit(false);
    $desde = $_POST["solicitudLicenciaDesde"];
    $solicitante = addslashes($_POST["solicitudSolicitante"]);
    try
    {
        $str_busq = "SELECT * FROM solicitudLicencias sl WHERE sl.legajo=".$_POST["legajo"]." AND desde='".setFecha($desde)."' AND estado!=1";
        $res_busq = $conn->query($str_busq);
        if($res_busq->num_rows>0)
        {
            throw new Exception("Ya existe una solicitud cargada para la fecha seleccionada");
        }
        
        //Inserto solicitud
        $str_ins_sol = "INSERT INTO solicitudLicencias (tipo_licencia, legajo, desde, causa, observaciones, pierde_presentismo, expediente, created_at, updated_at, hora_solicitud, nombre_solicitante, legajo_receptor, direccion) VALUES (".$_POST["tipoLicencia"].", ".$_POST["legajo"].", '".setFecha($desde)."', '".$causa."', '".$observaciones."', ".$pierde_present.", '".$expediente."', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."', '".$solicitante."', ".$leg.", '".$domicilio."')";
        $cons_ins_sol = $conn->query($str_ins_sol);
        if(!$cons_ins_sol)
        {
            throw new Exception("Error al cargar solicitud. Error de base de datos. Detalle: ".$conn->error);
        }
        
        $data["success"] = true;
        $data["msg"] = "Solicitud cargada con &eacute;xito";
        $conn->commit();
        escribir_log("personal", $leg, $_POST["legajo"], "solicitudLicencias", 1, $str_ins_sol, date("Y-m-d H:i:s"));
    }//End try
    catch(Exception $e)
    {
        $conn->rollBack();
        $data["success"] = false;
        $data["error"] = $e->getMessage();
    }
    echo json_encode($data);
}

function editSolicitudLicencia()
{
    global $conn;

    if(isset($_POST["pierdePresentismoSolicitud"]))
    {
        $pierde_present = ($_POST["pierdePresentismoSolicitud"] == "on")?1:0;
    }
    else
    {
        $pierde_present=0;
    }
    $causa = isset($_POST["solicitudCausa"])?addslashes($_POST["solicitudCausa"]):'';
    $observaciones = isset($_POST["solicitudObservaciones"])?addslashes($_POST["solicitudObservaciones"]):'';
    $expediente = isset($_POST["solicitudExpediente"])?$_POST["solicitudExpediente"]:'';
    $data = array();
    $leg = isset($_SESSION["legajo"])? $_SESSION["legajo"]:'';
    $domicilio = addslashes(trim($_POST["domicilioSolicitante"]));
    $conn->autocommit(false);
    $desde = $_POST["solicitudLicenciaDesde"];
    $solicitante = addslashes($_POST["solicitudSolicitante"]);   
    try
    {
        $str_sel = "SELECT * FROM solicitudLicencias WHERE id=".$_POST["idSolicitud"];
        $cons_sel = $conn->query($str_sel);
        if($cons_sel->num_rows==0)
        {
            throw new Exception("No se pudo encontrar la solicitud en la base de datos");
        }

        $str_busq = "SELECT * FROM solicitudLicencias sl WHERE id!=".$_POST["idSolicitud"]." AND sl.legajo=".$_POST["legajo"]." AND ('".setFecha($desde)."' BETWEEN desde AND hasta)";
        $res_busq = $conn->query($str_busq);
        if($res_busq->num_rows>0)
        {
            throw new Exception("Ya existe una solicitud cargada para la fecha seleccionada");
        }

        $str_upd = "UPDATE solicitudLicencias SET tipo_licencia=".$_POST["tipoLicencia"].", legajo=".$_POST["legajo"].", desde='".setFecha($desde)."', causa='".$causa."', observaciones='".$observaciones."', pierde_presentismo=".$pierde_present.", expediente='".$expediente."', updated_at='".date("Y-m-d H:i:s")."', nombre_solicitante='".$solicitante."', legajo_receptor=".$leg.", direccion='".$domicilio."' WHERE id=".$_POST["idSolicitud"];
        $res_upd = $conn->query($str_upd);
        //echo $str_upd;
        if(!$res_upd)
        {
            throw new Exception("Error al editar licencia. Detalle: ".$conn->error);
        }
        
        $data["success"] = true;
        $data["msg"] = "Solicitud modificada con &eacute;xito";
        $conn->commit();
        escribir_log("personal", $leg, $_POST["legajo"], "solicitudLicencias", 2, $str_upd, date("Y-m-d H:i:s"));
    }//End try
    catch(Exception $e)
    {
        $conn->rollBack();
        $data["success"] = false;
        $data["error"] = $e->getMessage();
    }
    echo json_encode($data);
}
function deleteSolicitudLicencia() 
{
    global $conn;

    $data = array();
    $leg = isset($_SESSION["legajo"])? $_SESSION["legajo"]:'';

    $conn->autocommit(false);
       
    try
    {
        $str_sel = "SELECT * FROM solicitudLicencias WHERE id=".$_POST["idSolicitud"];
        $cons_sel = $conn->query($str_sel);
        if(!$cons_sel)
        {
            throw new Exception("Error al consultar la base de datos. Detalle: ".$conn->error);
        }
        $obj_sel = $cons_sel->fetch_object();

        $str_del = "UPDATE solicitudLicencias SET estado=1 WHERE id=".$_POST["idSolicitud"];
        $res_del = $conn->query($str_del);
        if(!$res_del)
        {
            throw new Exception("Error al cancelar licencia. Detalle: ".$conn->error);
        }
        
        $data["success"] = true;
        $data["msg"] = "Solicitud cancelada con &eacute;xito";
        $conn->commit();
        escribir_log("personal", $leg, $obj_sel->legajo, "solicitudLicencias", 3, $str_del, date("Y-m-d H:i:s"));
    }//End try
    catch(Exception $e)
    {
        $conn->rollBack();
        $data["success"] = false;
        $data["error"] = $e->getMessage();
    }
    echo json_encode($data);
}

function confirmarSolicitudLicencia()
{
    global $conn;

    if(isset($_POST["pierdePresentismoSolicitud"]))
    {
        $pierde_present = ($_POST["pierdePresentismoSolicitud"] == "on")?1:0;
    }
    else
    {
        $pierde_present=0;
    }
    $causa = isset($_POST["solicitudCausa"])?addslashes($_POST["solicitudCausa"]):'';
    $observaciones = isset($_POST["solicitudObservaciones"])?addslashes($_POST["solicitudObservaciones"]):'';
    $expediente = isset($_POST["solicitudExpediente"])?$_POST["solicitudExpediente"]:'';
    $data = array();
    $leg = isset($_SESSION["legajo"])? $_SESSION["legajo"]:'';

    $conn->autocommit(false);
    $desde = $_POST["solicitudLicenciaDesde"];
    $hasta = $_POST["solicitudLicenciaHasta"];
       
    try
    {
        $str_sel = "SELECT * FROM solicitudLicencias WHERE id=".$_POST["idSolicitud"];
        $cons_sel = $conn->query($str_sel);
        if($cons_sel->num_rows==0)
        {
            throw new Exception("No se pudo encontrar la solicitud en la base de datos");
        }

        //Obtengo el nombre de la licencia
        $str_nombre_lic = "SELECT * FROM tipo_licencias WHERE id=".$_POST["tipoLicencia"];
        $cons_nombre_lic = $conn->query($str_nombre_lic);
        if(!$cons_nombre_lic)
        {
            throw new Exception("Error al leer la base de datos. Error BD: ".$conn->error);
        }
        $obj_nombre_lic = $cons_nombre_lic->fetch_object();

        $str_busq = "SELECT * FROM licencias WHERE legajo=".$_POST["legajo"]." AND (('".setFecha($desde)."' BETWEEN desde AND hasta OR '".setFecha($hasta)."' BETWEEN desde AND hasta) OR (desde BETWEEN '".setFecha($desde)."' AND '".setFecha($hasta)."' OR hasta BETWEEN '".setFecha($desde)."' AND '".setFecha($hasta)."')) AND goce='S'";
        $res_busq = $conn->query($str_busq);
        if($res_busq->num_rows>0)
        {
            $obj_busq = $res_busq->fetch_object();
            throw new Exception("Ya existe una licencia cargada en las fechas seleccionadas: ".$obj_nombre_lic->nombre." (".$obj_busq->desde." al ".$obj_busq->hasta.")");
        }
        
        //Inserto licencia en la tabla general de licencias
        $str_exist_lic = "SELECT * FROM licencias WHERE legajo=".$_POST["legajo"]." AND tipo_licencia=".$_POST["tipoLicencia"]." AND goce='S' AND YEAR(desde)=YEAR('".setFecha($desde)."') AND YEAR(hasta)=YEAR('".setFecha($desde)."') ORDER BY id DESC";
        $cons_exist_lic = $conn->query($str_exist_lic);
        if($cons_exist_lic->num_rows==0) //Si es la primera licencia del anio para el empleado, inserto.
        {
            $str_ins_lic = "INSERT INTO licencias (tipo_licencia, legajo, desde, hasta, dias_licencia, suma_anio, causa, goce, observaciones, pierde_presentismo, expediente, created_at, updated_at) VALUES (".$_POST["tipoLicencia"].", ".$_POST["legajo"].", '".setFecha($desde)."', '".setFecha($hasta)."', ".$_POST["solicitudTotalDias"].", ".$_POST["solicitudTotalDias"].", '".$causa."', 'S', '".$observaciones."', ".$pierde_present.", '".$expediente."', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."')";
        }
        else //Si ya tiene una licencia en el anio, se le suman los dias
        {
            $fila_exist_lic = $cons_exist_lic->fetch_object();
            $str_ins_lic = "INSERT INTO licencias (tipo_licencia, legajo, desde, hasta, dias_licencia, suma_anio, causa, goce, observaciones, pierde_presentismo, expediente, created_at, updated_at) VALUES (".$_POST["tipoLicencia"].", ".$_POST["legajo"].", '".setFecha($desde)."', '".setFecha($hasta)."', ".$_POST["solicitudTotalDias"].", ".($fila_exist_lic->suma_anio+$_POST["solicitudTotalDias"]).", '".$causa."', 'S', '".$observaciones."', ".$pierde_present.", '".$expediente."', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."')";
        }
        
        $cons_ins_lic = $conn->query($str_ins_lic);
        if(!$cons_ins_lic)
        {
            throw new Exception("Error al cargar licencia. Error BD: ".$conn->error);
        }
        $id_lic = $conn->insert_id;   
        // Escribir log
        
        escribir_log("personal", $leg, $_POST["legajo"], $obj_nombre_lic->nombre, 1, $str_ins_lic, date("Y-m-d H:i:s"));
        
        //Abajo licencias especiales que requieren datos adicionales          
        switch ($_POST['tipoLicencia']) 
        {
            case 5: // Maternidad
                $str_lic = "INSERT INTO lic_maternidad (id_licencia, fpp, certnac, fp, lactahasta, lactahorario) VALUES (".$id_lic.", '".setFecha($_POST["fpp"])."', '".$cert_nac."', '".setFecha($_POST["fp"])."', '".setFecha($_POST["lactahasta"])."', '".$_POST["lactahorario"]."')";
                    
                $result = $conn->query($str_lic);
                if(!$result)
                {
                    throw new Exception("Error al cargar licencia. Error BD: ".$conn->error);
                }
                escribir_log("personal", $leg, $_POST["legajo"], $obj_nombre_lic->nombre, 1, $str_lic, date("Y-m-d H:i:s"));
                break; 
               
            case 12: // Estudio
                $str_lic = "INSERT INTO lic_estudio (id_licencia, asignatura, carrera, fecha_examen) VALUES (".$id_lic.", '".$_POST["asignatura"]."', '".$_POST["carrera"]."', '".setFecha($_POST["fecha_examen"])."')";
                
                $cons_estu = $conn->query($str_lic);
                if(!$cons_estu)
                {
                    throw new Exception("Error al cargar licencia. Error BD: ".$conn->error);
                }
                escribir_log("personal", $leg, $_POST["legajo"], "licencias", 1, $str_lic, date("Y-m-d H:i:s"));
                break;
        }//End switch
        
        //Cambio status de la solicitud a "confirmada"
        $str_conf = "UPDATE solicitudLicencias SET estado=3 WHERE id=".$_POST["idSolicitud"];
        $cons_conf = $conn->query($str_conf);
        if(!$cons_conf)
        {
            throw new Exception("Error al cargar licencia. Error BD: ".$conn->error);
        }
        $data["success"] = true;
        $data["msg"] = "Licencia confirmada con &eacute;xito";
        $conn->commit();
    }//End try
    catch(Exception $e)
    {
        $conn->rollBack();
        $data["success"] = false;
        $data["error"] = $e->getMessage();
    }
    echo json_encode($data);
}

function desisteDiasTrabajados()
{
    global $conn;
    $data = array();
    $id = $_POST["idLic"];
    $conn->autocommit(false);
    $leg = isset($_SESSION["legajo"])? $_SESSION["legajo"]:'';
    $cant_sumar = 0;
    try
    {
        //Obtengo el nombre de la licencia en cuestion
        $str_nombre_lic = "SELECT * FROM tipo_licencias WHERE id=".$_POST['tipoLic'];
        $cons_nombre_lic = $conn->query($str_nombre_lic);
        if(!$cons_nombre_lic)
        {
            throw new Exception("Error al consultar la base de datos. Error BD: ".$conn->error);
        }
        $obj_nombre_lic = $cons_nombre_lic->fetch_object();
        switch ($_POST['tipoLic']) 
        {
            case 10: //Compensatoria dias feria
                $str_cons = "SELECT * FROM dias_trabajados WHERE id=".$id;
                $result_cons = $conn->query($str_cons);
                if(!$result_cons)
                {
                    throw new Exception("Error al consultar la base de datos. Error BD: ".$conn->error);
                }
                $fila = $result_cons->fetch_object();
                if($fila->goce!= 'N' && $fila->totaldias == $fila->disponibles)//Si la licencia no fue desistida y no se han consumido dias de ella
                {    
                    $hoy = date('Y-m-d');
                    $legajo = $fila->legajo;
                    //Elimino la licencia en cuestion (pongo goce en 'N')
                    $str_upd_lic_dias_comp = "UPDATE dias_trabajados SET goce='N' WHERE id=".$fila->id;
                    if(!$conn->query($str_upd_lic_dias_comp))
                    {
                        throw new Exception("Error al consultar la base de datos. Error BD: ".$conn->error);
                    }
                    $conn->commit();
                    $data["success"] = true;
                    $data["msg"] = "Dias trabajados cancelados.";
                    escribir_log("personal", $leg, $legajo, $obj_nombre_lic->nombre, 2, $str_upd_lic_dias_comp, date("Y-m-d H:i:s"));
                } //end if($fila->goce!= 'N')
                else
                {
                    throw new Exception("No se puede desistir. Dias ya desistidos anteriormente o ya consumidos");
                }
                break;
            case 18: //Doble turno dias trabajados
                $str_cons = "SELECT * FROM dias_trabajados WHERE id=".$id;
                $result_cons = $conn->query($str_cons);
                if(!$result_cons)
                {
                    throw new Exception("Error al consultar la base de datos. Error BD: ".$conn->error);
                }
                $fila = $result_cons->fetch_object();
                if($fila->goce!= 'N' && $fila->totaldias == $fila->disponibles)//Si la licencia no fue desistida y no se han consumido dias de ella
                {    
                    $hoy = date('Y-m-d');
                    $legajo = $fila->legajo;
                    //Elimino la licencia en cuestion (pongo goce en 'N')
                    $str_upd_doble_turno_dias_2 = "UPDATE dias_trabajados SET goce='N' WHERE id=".$fila->id;
                    if(!$conn->query($str_upd_doble_turno_dias_2))
                    {
                        throw new Exception("Error al consultar la base de datos. Error BD: ".$conn->error);
                    }
                    $conn->commit();
                    $data["success"] = true;
                    $data["msg"] = "Dias trabajados cancelados.";
                    escribir_log("personal", $leg, $legajo, $obj_nombre_lic->nombre, 2, $str_upd_doble_turno_dias_2, date("Y-m-d H:i:s"));
                } //end if($fila->goce!= 'N')
                else
                {
                    throw new Exception("No se puede desistir. Dias ya desistidos anteriormente o ya consumidos");
                }
                break;    
        }//End switch ($_POST['tipoLic'])
    }
    catch(Exception $e)
    {
        $conn->rollBack();
        $data["success"] = false;
        $data["error"] = $e->getMessage();
    }
    echo json_encode($data);
}

function desisteLicencia()
{
    global $conn;
    $data = array();
    $id = $_POST["idLic"];
    $conn->autocommit(false);
    $leg = isset($_SESSION["legajo"])? $_SESSION["legajo"]:'';
    $cant_sumar = 0;

    //Obtengo el nombre de la licencia en cuestion
    $str_nombre_lic = "SELECT * FROM tipo_licencias WHERE id=".$_POST['tipoLic'];
    $cons_nombre_lic = $conn->query($str_nombre_lic);
    if(!$cons_nombre_lic)
    {
        throw new Exception("Error al consultar la base de datos. Error BD: ".$conn->error);
    }
    $obj_nombre_lic = $cons_nombre_lic->fetch_object();
    try
    {
        //Actualizamos la cantidad de dias disponibles de la licencia
        $str_cons = "SELECT * FROM licencias WHERE tipo_licencia=".$_POST['tipoLic']." AND id=".$id;
        $result_cons = $conn->query($str_cons);
        if(!$result_cons)
        {
            throw new Exception($conn->error);
        }
        $fila = $result_cons->fetch_object();

        //Primero guardo la cantidad de dias a desistir (porque en el caso de licencia compensatoria o doble turno, se deben sumar a los dias disponibles)
        $cant_sumar = $fila->dias_licencia;
        $anio_lic = explode("-", $fila->desde);
        
        if($fila->goce!= 'N' && date($anio_lic[0]) == date("Y"))//Si la licencia no fue desistida y es de este a�o, puedo desistirla. Caso contrario arroja error
        {
            $hoy = date('Y-m-d');
            $legajo = $fila->legajo;
            
            //Primero elimino la licencia en cuestion (pongo goce en 'N' y la cantidad de dias 0)
            $str_desist = "UPDATE licencias SET goce='N', dias_licencia=0, updated_at='".date("Y-m-d H:i:s")."' WHERE id=".$fila->id;
            if(!$conn->query($str_desist))
            {
                throw new Exception("Error al consultar la base de datos. Error BD: ".$conn->error);
            }
            escribir_log("personal", $leg, $legajo, $obj_nombre_lic->nombre, 2, $str_desist, date("Y-m-d H:i:s"));
            //Luego calculo nuevamente los dias a todos los registros de dicho legajo (del a�o actual)
            $str_cons = "SELECT * FROM licencias WHERE tipo_licencia=".$_POST['tipoLic']." AND legajo=".$fila->legajo." AND goce='S' AND YEAR(desde)=YEAR(CURDATE()) AND YEAR(hasta)=YEAR(CURDATE()) ORDER BY id ASC";
            $result_cons = $conn->query($str_cons);
            if(!$result_cons)
            {
                throw new Exception("Error al consultar la base de datos. Error BD: ".$conn->error);
            }
            $cont_f = 0;

            $cant_reg = $result_cons->num_rows;
            while ($fila_new = $result_cons->fetch_object())
            {
                //Si hay 1 registro, hay solo 1 licencia activa, por lo tanto asigno a suma_anio el valor de diasotorgad
                if($cant_reg === 1)
                {
                    $str_upd_dias_lic = "UPDATE licencias SET suma_anio=".$fila_new->dias_licencia.", updated_at='".date("Y-m-d H:i:s")."' WHERE id=".$fila_new->id;
                    if(!$conn->query($str_upd_dias_lic))
                    {
                        throw new Exception("Error al consultar la base de datos. Error BD: ".$conn->error);
                    }
                    escribir_log("personal", $leg, $legajo, $obj_nombre_lic->nombre, 2, $str_upd_dias_lic, date("Y-m-d H:i:s"));
                }
                else
                {
                    //Primera pasada = primer registro = primer licencia cargada, solo guardo el total de dias para sumar en las siguientes pasadas
                    //Ademas actualizo el total de dias de dicha licencia, dado que puede no haber sido la primer licencia desde un comienzo
                    if($cont_f === 0) 
                    {
                        $str_upd_dias_lic = "UPDATE licencias SET suma_anio=".$fila_new->dias_licencia.", updated_at='".date("Y-m-d H:i:s")."' WHERE id=".$fila_new->id;
                        if(!$conn->query($str_upd_dias_lic))
                        {
                            throw new Exception("Error al consultar la base de datos. Error BD: ".$conn->error);
                        }
                        $tot_d_ant = $fila_new->dias_licencia;
                    }
                    else
                    {
                        $str_upd_dias_lic = "UPDATE licencias SET suma_anio=".($fila_new->dias_licencia+$tot_d_ant).", updated_at='".date("Y-m-d H:i:s")."' WHERE id=".$fila_new->id;
                        if(!$conn->query($str_upd_dias_lic))
                        {
                            throw new Exception("Error al consultar la base de datos. Error BD: ".$conn->error);
                        }        
                        $tot_d_ant = $fila_new->dias_licencia+$tot_d_ant;
                    }
                    escribir_log("personal", $leg, $legajo, $obj_nombre_lic->nombre, 2, $str_upd_dias_lic, date("Y-m-d H:i:s"));
                }
                $cont_f++;    
            }
        } //end if($fila->goce!= 'N')
        else
        {
            throw new Exception("No se puede desistir. Licencia ya desistida anteriormente o licencia obsoleta.");
        }
    
        //Casos especiales (Compensatoria y doble turno, se necesitan actualizar los dias disponibles)
        $hoy = date('Y-m-d');
        //Sumo los dias desistidos a la tabla lic_comp_dias, empezando por los dias mas recientes.
        //Ej: si a un empleado le deben dias de 2017 y dias de 2016, primero sumo la cantidad de dias desistidos al 2017, y luego para atras.
        $str_cons2 = "SELECT * FROM dias_trabajados WHERE legajo=".$legajo." and tipo_licencia=".$_POST['tipoLic']." AND disponibles<totaldias ORDER BY desde DESC";
        $result_cons2 = $conn->query($str_cons2);
        
        if(!$result_cons2)
        {
            throw new Exception("Error al consultar la base de datos. Error BD: ".$conn->error);
        }
        
        $cont_f = 0;
        $fila_new = $result_cons2->fetch_object();
        while ($fila_new && $cant_sumar !=0)
        {
            if($fila_new->totaldias <= $fila_new->disponibles+$cant_sumar)
            {
                $rest_disp = $fila_new->disponibles;
                $str_upd_dias_comp = "UPDATE dias_trabajados SET disponibles=".$fila_new->totaldias." WHERE id=".$fila_new->id;  
                if(!$conn->query($str_upd_dias_comp))
                {
                    throw new Exception("Error al consultar la base de datos. Error BD: ".$conn->error);
                }
                $cant_sumar = $cant_sumar-($fila_new->totaldias-$rest_disp);
                escribir_log("personal", $leg, $legajo, $obj_nombre_lic->nombre, 2, $str_upd_dias_comp, date("Y-m-d H:i:s"));
            }
            else
            {
                $str_upd_dias_comp = "UPDATE dias_trabajados SET disponibles=".($fila_new->disponibles+$cant_sumar)." WHERE id=".$fila_new->id;
                if(!$conn->query($str_upd_dias_comp))
                {
                    throw new Exception("Error al consultar la base de datos. Error BD: ".$conn->error);
                }
                $cant_sumar = 0;
                escribir_log("personal", $leg, $legajo, $obj_nombre_lic->nombre, 2, $str_upd_dias_comp, date("Y-m-d H:i:s"));
            }
            $fila_new = $result_cons2->fetch_object();
        } 
        $conn->commit();
        $data["success"] = true;
        $data["msg"] = "Licencia desistida con &eacute;xito";
        $envio_mail = sendMailDesisteLicencia($legajo, $_POST["idLic"]);
        if(!$envio_mail["success"])
        {
            $data["info"] = "Licencia desistida con &eacute;xito, pero hubo error al enviar notificaci&oacute;n por mail.";
        }
    }//End try
    catch(Exception $e)
    {
        $conn->rollBack();
        $data["success"] = false;
        $data["error"] = $e->getMessage();
    }
    echo json_encode($data);
}

function eliminarLicencia()
{
    global $conn;
    $data = array();
    $id = $_POST["idLic"];
    $conn->autocommit(false);
    $leg = isset($_SESSION["legajo"])? $_SESSION["legajo"]:'';
    $cant_sumar = 0;

    //Obtengo el nombre de la licencia en cuestion
    $str_nombre_lic = "SELECT * FROM tipo_licencias WHERE id=".$_POST['tipoLic'];
    $cons_nombre_lic = $conn->query($str_nombre_lic);
    if(!$cons_nombre_lic)
    {
        throw new Exception("Error al consultar la base de datos. Error BD: ".$conn->error);
    }
    $obj_nombre_lic = $cons_nombre_lic->fetch_object();
    try
    {
        //Actualizamos la cantidad de dias disponibles de la licencia
        $str_cons = "SELECT * FROM licencias WHERE id=".$id;
        $result_cons = $conn->query($str_cons);
        if(!$result_cons)
        {
            throw new Exception($conn->error);
        }

        $fila = $result_cons->fetch_object();

        //Primero guardo la cantidad de dias a desistir (porque en el caso de licencia compensatoria o doble turno, se deben sumar a los dias disponibles)
        $cant_sumar = $fila->dias_licencia;
        $anio_lic = explode("-", $fila->desde);
        if(date($anio_lic[0]) == date("Y"))//Si la licencia es de este a�o, puedo desistirla. Caso contrario arroja error
        {
            $hoy = date('Y-m-d');
            $legajo = $fila->legajo;
            
            //Primero elimino la licencia en cuestion
            $str_delete = "DELETE FROM licencias WHERE id=".$fila->id;
            if(!$conn->query($str_delete))
            {
                throw new Exception("Error al consultar la base de datos. Error BD: ".$conn->error);
            }
            escribir_log("personal", $leg, $legajo, $obj_nombre_lic->nombre, 3, $str_delete, date("Y-m-d H:i:s"));
            //Luego calculo nuevamente los dias a todos los registros de dicho legajo (del a�o actual)
            $str_cons = "SELECT * FROM licencias WHERE tipo_licencia=".$_POST['tipoLic']." AND legajo=".$fila->legajo." AND goce='S' AND YEAR(desde)=YEAR(CURDATE()) AND YEAR(hasta)=YEAR(CURDATE()) ORDER BY id ASC";
            $result_cons = $conn->query($str_cons);
            if(!$result_cons)
            {
                throw new Exception("Error al consultar la base de datos. Error BD: ".$conn->error);
            }
            $cont_f = 0;

            $cant_reg = $result_cons->num_rows;
            while ($fila_new = $result_cons->fetch_object())
            {
                //Si hay 1 registro, hay solo 1 licencia activa, por lo tanto asigno a suma_anio el valor de diasotorgad
                if($cant_reg === 1)
                {
                    $str_upd_dias_lic = "UPDATE licencias SET suma_anio=".$fila_new->dias_licencia.", updated_at='".date("Y-m-d H:i:s")."' WHERE id=".$fila_new->id;
                    if(!$conn->query($str_upd_dias_lic))
                    {
                        throw new Exception("Error al consultar la base de datos. Error BD: ".$conn->error);
                    }
                    escribir_log("personal", $leg, $legajo, $obj_nombre_lic->nombre, 2, $str_upd_dias_lic, date("Y-m-d H:i:s"));
                }
                else
                {
                    //Primera pasada = primer registro = primer licencia cargada, solo guardo el total de dias para sumar en las siguientes pasadas
                    //Ademas actualizo el total de dias de dicha licencia, dado que puede no haber sido la primer licencia desde un comienzo
                    if($cont_f === 0) 
                    {
                        $str_upd_dias_lic = "UPDATE licencias SET suma_anio=".$fila_new->dias_licencia.", updated_at='".date("Y-m-d H:i:s")."' WHERE id=".$fila_new->id;
                        if(!$conn->query($str_upd_dias_lic))
                        {
                            throw new Exception("Error al consultar la base de datos. Error BD: ".$conn->error);
                        }
                        $tot_d_ant = $fila_new->dias_licencia;
                    }
                    else
                    {
                        $str_upd_dias_lic = "UPDATE licencias SET suma_anio=".($fila_new->dias_licencia+$tot_d_ant).", updated_at='".date("Y-m-d H:i:s")."' WHERE id=".$fila_new->id;
                        if(!$conn->query($str_upd_dias_lic))
                        {
                            throw new Exception("Error al consultar la base de datos. Error BD: ".$conn->error);
                        }        
                        $tot_d_ant = $fila_new->dias_licencia+$tot_d_ant;
                    }
                    escribir_log("personal", $leg, $legajo, $obj_nombre_lic->nombre, 2, $str_upd_dias_lic, date("Y-m-d H:i:s"));
                }
                $cont_f++;    
            }
        } //end if(date($anio_lic[0]) == date("Y"))
        else
        {
            throw new Exception("No se puede eliminar. Licencia obsoleta.");
        }
    
        //Casos especiales (Compensatoria y doble turno)
        $hoy = date('Y-m-d');
        //Sumo los dias desistidos a la tabla lic_comp_dias, empezando por los dias mas recientes.
        //Ej: si a un empleado le deben dias de 2017 y dias de 2016, primero sumo la cantidad de dias desistidos al 2017, y luego para atras.
        $str_cons2 = "SELECT * FROM dias_trabajados WHERE legajo=".$legajo." AND tipo_licencia=".$_POST['tipoLic']." AND disponibles<totaldias ORDER BY desde DESC";
        $result_cons2 = $conn->query($str_cons2);
        
        if(!$result_cons2)
        {
            throw new Exception("Error al consultar la base de datos. Error BD: ".$conn->error);
        }
        
        $cont_f = 0;
        $fila_new = $result_cons2->fetch_object();
        while ($fila_new && $cant_sumar !=0)
        {
            if($fila_new->totaldias <= $fila_new->disponibles+$cant_sumar)
            {
                $rest_disp = $fila_new->disponibles;
                $str_upd_dias_comp = "UPDATE dias_trabajados SET disponibles=".$fila_new->totaldias." WHERE id=".$fila_new->id;  
                if(!$conn->query($str_upd_dias_comp))
                {
                    throw new Exception("Error al consultar la base de datos. Error BD: ".$conn->error);
                }
                $cant_sumar = $cant_sumar-($fila_new->totaldias-$rest_disp);
                escribir_log("personal", $leg, $legajo, $obj_nombre_lic->nombre, 2, $str_upd_dias_comp, date("Y-m-d H:i:s"));
            }
            else
            {
                $str_upd_dias_comp = "UPDATE dias_trabajados SET disponibles=".($fila_new->disponibles+$cant_sumar)." WHERE id=".$fila_new->id;
                if(!$conn->query($str_upd_dias_comp))
                {
                    throw new Exception("Error al consultar la base de datos. Error BD: ".$conn->error);
                }
                $cant_sumar = 0;
                escribir_log("personal", $leg, $legajo, $obj_nombre_lic->nombre, 2, $str_upd_dias_comp, date("Y-m-d H:i:s"));
            }
            $fila_new = $result_cons2->fetch_object();
        } 
        $conn->commit();
        $data["success"] = true;
        $data["info"] = "Licencia eliminada con &eacute;xito";
    }//End try
    catch(Exception $e)
    {
        $conn->rollBack();
        $data["success"] = false;
        $data["error"] = $e->getMessage();
    }
    echo json_encode($data);
}

function eliminarDiasTrabajados()
{
    global $conn;
    $data = array();
    $id = $_POST["idLic"];
    $conn->autocommit(false);
    $leg = isset($_SESSION["legajo"])? $_SESSION["legajo"]:'';
    $cant_sumar = 0;

    //Obtengo el nombre de la licencia en cuestion
    $str_nombre_lic = "SELECT * FROM tipo_licencias WHERE id=".$_POST['tipoLic'];
    $cons_nombre_lic = $conn->query($str_nombre_lic);
    if(!$cons_nombre_lic)
    {
        throw new Exception("Error al consultar la base de datos. Error BD: ".$conn->error);
    }
    $obj_nombre_lic = $cons_nombre_lic->fetch_object();
    try
    {
        switch ($_POST['tipoLic']) 
        {
            case 10: //Compensatoria dias feria
                $str_cons = "SELECT * FROM dias_trabajados WHERE id=".$id;
                $result_cons = $conn->query($str_cons);
                if(!$result_cons)
                {
                    throw new Exception("Error al consultar la base de datos. Error BD: ".$conn->error);
                }
                $fila = $result_cons->fetch_object();
                if($fila->goce!= 'N' && $fila->totaldias == $fila->disponibles)//Si la licencia no fue desistida y no se han consumido dias de ella
                {    
                    $hoy = date('Y-m-d');
                    $legajo = $fila->legajo;
                    //Elimino la licencia en cuestion
                    $str_del_lic_dias_comp = "DELETE FROM dias_trabajados WHERE id=".$fila->id;
                    if(!$conn->query($str_del_lic_dias_comp))
                    {
                        throw new Exception("Error al consultar la base de datos. Error BD: ".$conn->error);
                    }
                    escribir_log("personal", $leg, $legajo, $obj_nombre_lic->nombre, 3, $str_del_lic_dias_comp, date("Y-m-d H:i:s"));
                } //end if($fila->goce!= 'N')
                else
                {
                    throw new Exception("No se puede eliminar. Dias de feria ya consumidos anteriormente");
                }
                break;
            case 18: //Doble turno dias trabajados
                $str_cons = "SELECT * FROM dias_trabajados WHERE id=".$id;
                $result_cons = $conn->query($str_cons);
                if(!$result_cons)
                {
                    throw new Exception("Error al consultar la base de datos. Error BD: ".$conn->error);
                }
                $fila = $result_cons->fetch_object();
                if($fila->goce!= 'N' && $fila->totaldias == $fila->disponibles)//Si la licencia no fue desistida y no se han consumido dias de ella
                {    
                    $hoy = date('Y-m-d');
                    $legajo = $fila->legajo;
                    //Elimino la licencia en cuestion
                    $str_del_doble_turno_dias_2 = "DELETE FROM dias_trabajados WHERE id=".$fila->id;
                    if(!$conn->query($str_del_doble_turno_dias_2))
                    {
                        throw new Exception("Error al consultar la base de datos. Error BD: ".$conn->error);
                    }
                    escribir_log("personal", $leg, $legajo, $obj_nombre_lic->nombre, 3, $str_del_doble_turno_dias_2, date("Y-m-d H:i:s"));
                } //end if($fila->goce!= 'N')
                else
                {
                    throw new Exception("No se puede eliminar. Dias de doble turno ya consumidos anteriormente");
                }
                break;    
        }//End switch ($_POST['tipoLic']) 
        $conn->commit();
        $data["success"] = true;
        $data["info"] = "Dias trabajados eliminados con &eacute;xito";
    }//End try
    catch(Exception $e)
    {
        $conn->rollBack();
        $data["success"] = false;
        $data["error"] = $e->getMessage();
    }
    echo json_encode($data);
}

function vaciarDiasTrabajados()
{
    global $conn;
    $data = array();
    $id = $_POST["idLic"];
        
    $conn->autocommit(false);
    try
    {
        $str_cons = "SELECT * FROM dias_trabajados WHERE id=".$id;
        $result_cons = $conn->query($str_cons);
        if(!$result_cons)
        {
            throw new Exception($conn->error);
        }
        else
        {
            $fila = $result_cons->fetch_object();
            $hoy = date('Y-m-d');
            $legajo = $fila->legajo;
            
            //Poner en 0 los d�as trabajados
            $str_upd = "UPDATE dias_trabajados SET disponibles=0 WHERE id=".$fila->id;
            if(!$conn->query($str_upd))
            {
                throw new Exception($conn->error);
            }
            
            $conn->commit();
            $data["success"] = true;
            $data["info"] = "Licencia vaciada con &eacute;xito";
            $leg = isset($_SESSION["legajo"])? $_SESSION["legajo"]:'';
            escribir_log("personal", $leg, $legajo, "dias_trabajados", 2, $str_upd, date("Y-m-d H:i:s"));
        }
    }//end try
    catch(Exception $e)
    {
        $conn->rollBack();
        $data["success"] = false;
        $data["error"] = $e->getMessage();
    }
    echo json_encode($data);
    
}

function getLicenciasMasivasCompensatorias()
{
    global $conn;
    $arr = array();
    $data = array();
    $str_cons = "SELECT carga_masiva_comp.*, general.nombre, general.apellido FROM carga_masiva_comp, general WHERE general.legajo=carga_masiva_comp.legajo ORDER BY carga_masiva_comp.legajo ASC";
    $res_cons = $conn->query($str_cons);
    $nbrows = $res_cons->num_rows;
    if($res_cons->num_rows>0)
    {
        while($obj = $res_cons->fetch_object())
        {
            $obj->apellido_nombre = $obj->apellido.", ".$obj->nombre;
            $arr[] = $obj;
        }
    }
    $data["total"] = $nbrows;
    $data["results"] = $arr;
    //echo "({total: ".$nbrows.",  results: " . json_encode($arr) . "})"; 
    echo json_encode($data);
}

function getDatosAgenteById()
{
    global $conn;
    $arr = array();
    $data = array();
    $str_cons = "SELECT general.nombre, general.apellido, general.legajo FROM general WHERE general.id=".$_GET["idAgente"];
    $res_cons = $conn->query($str_cons);
    $nbrows = $res_cons->num_rows;
    if($res_cons->num_rows>0)
    {
        while($obj = $res_cons->fetch_object())
        {
            $arr[] = $obj;
        }
    }
    $data["total"] = $nbrows;
    $data["results"] = $arr;
    //echo "({total: ".$nbrows.",  results: " . json_encode($arr) . "})"; 
    echo json_encode($data);
}

function getTipoLicenciaById($idLic)
{
    global $conn;
    $data = array();
    $arr = array();
    try
    {
        $conn->query("SET NAMES UTF8");
        $str = "SELECT * FROM tipo_licencias WHERE id=".$idLic;
        $result = mysqli_query($conn, $str);
        if(!$result)
        {
            throw new Exception("Error al consultar la base de datos. Error DB: ".$conn->error);
        }
        $nbrows = mysqli_num_rows($result);
        
        while ($obj = mysqli_fetch_object($result))
        {
            $arr[] = $obj;
        }
        $data["success"] = true;
        $data["total"] = $nbrows;
        $data["results"] = $arr;
    }
    catch(Exception $e)
    {
        $data["success"] = false;
        $data["error"] = $e->getMessage();
    }
    echo json_encode($data);
}

function getTipoLicencias() 
{
    global $conn;
    $str = "SELECT * FROM tipo_licencias ORDER BY id ASC";
        
    $result = $conn->query($str);
    $nbrows = $result->num_rows;
    $arr = array();
    while ($obj = $result->fetch_object()){
        $arr[] = $obj;
    }
    echo "({ total: ".$nbrows.",  results: " . json_encode($arr) . "})";
}

?>