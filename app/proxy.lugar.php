<?php
session_set_cookie_params(0);
session_start();
require('system.config.php');
require('proxy.feriados.php');

$tarea = $_SERVER['REQUEST_METHOD'];
$tarea = isset($_POST["idLugarElimin"])?'DELETE':$_SERVER['REQUEST_METHOD'];
$act = isset($_POST["act"])?$_POST["act"]:"";
switch ($tarea) {
	case 'GET':
        $act = isset($_GET["act"])?$_GET["act"]:"";
        switch ($act)
        {
            case "": //Obtener todos los lugares de un legajo
                getLugar();
                break;
            case "getLugarById": //Obtener un lugar segun su ID
                getLugarById();
                break;    
        }
	    break;	
    case 'POST':
		switch ($act)
                {
                    case "": //Si es vacio, asigna un lugar al agente. Mauro
                        setLugar();
                        break;
                    case "altaLugar": //Alta de un nuevo lugar en el sistema. Mauro
                        altaLugar();
                        break;
                    case "bajaLugar": //Eliminar un lugar del sistema. Mauro
                        bajaLugar();
                        break;
                    case "modificaLugar": //Modificar un lugar del sistema. Mauro
                        modificaLugar();
                        break;
                }
		break;
	case 'PUT':
		break;
	case 'DELETE':
                deleteLugar();
		break;
	default:
		echo "({ failure: 'Error por default'})";
		break;
}


function getLugar() 
{
    global $conn;

    if (isset($_GET["legajo"])) 
    {
        $str = "SELECT l.nombrelugar, h.* FROM lugar l, histolugar h WHERE h.legajo=" . $_GET["legajo"] . " AND h.nrolugar=l.nrolugar ORDER BY h.desde DESC";
    } 
    else 
    {
        $str = "SELECT * FROM lugar ORDER BY nombrelugar ASC ";
    }
    
    $result = $conn->query($str);
    $nbrows = $result->num_rows;
    $arr = array();
    while ($obj = $result->fetch_object()){
            $arr[] = $obj;

    }

    echo "({ total: ".$nbrows.",  results: " . json_encode($arr) . "})";
}

function getLugarById()
{
    global $conn;
    $data = array();
    $str = "SELECT * FROM lugar WHERE id=".$_GET["idLugar"];
        
    $result = $conn->query($str);
    $nbrows = $result->num_rows;
    $arr = array();
    while ($obj = $result->fetch_object()){
            $arr[] = $obj;

    }
    $data["total"] = $nbrows;
    $data["results"] = $arr;
    echo json_encode($data);
}

function setLugar() 
{
    global $conn;
    $data = array();
    $flag = true;
    $err_flag = "Error:";

    //Obtengo la fecha "desde" del ultimo lugar en el que trabajó, para ser usada en distintas partes del script
    $str_cons_ex_lug_1 = "SELECT MAX(desde) as desde FROM histolugar WHERE legajo=".$_POST["legajo"];
    $res_cons_ex_lug_1 = $conn->query($str_cons_ex_lug_1);
    $obj_cons_ex_lug_1 = $res_cons_ex_lug_1->fetch_object();
    
    //Cuando no esta seteada la variable de sesion, da error al insertar en la base de datos. Por lo tanto inicializo en 0 la variable. Mauro
    if(isset($_SESSION['legajo']))
    {
        $legajo = $_SESSION['legajo'];
    }
    else
    {
        $legajo = 0;
    }
    
    //Si se cargó un lugar actual, controlo que la fecha "desde" sea posterior a la fecha "desde" del ultimo lugar en el que trabajó
    if(!$_POST["hasta"])
    {
        if(setFecha($_POST["desde"]) <= $obj_cons_ex_lug_1->desde)
        {
            $data["success"] = false;
            $data["error"] = "Si desea cargar un lugar actual, la fecha de inicio debe ser posterior a la fecha del lugar anterior.";
        }
        else
        {
            $str_upd_gen = "UPDATE general SET lugaractu=".$_POST["lugaractu"]." WHERE legajo=".$_POST["legajo"];
            $result_upd_gen = $conn->query($str_upd_gen);
            if(!$result_upd_gen)
            {
                $data["success"] = false;
                $data["error"] = $conn->error;
            }
            else
            {
                // Escribir log
                //$str_log_gen = "INSERT INTO usuarios_log (legajo, tabla, log, fecha) VALUES (".$legajo.", 'general', '".addslashes($str_upd_gen)."', '".date("Y-m-d H:i:s")."')";
                //$res_log_gen = $conn->query($str_log_gen);
                
                escribir_log("personal", $legajo, $_POST["legajo"], "general", 2, $str_upd_gen, date("Y-m-d H:i:s"));

                //Busco el ultimo lugar en el que estuvo para actualizar la fecha de fin
                $str_c_hist_lug = "SELECT id, desde FROM histolugar WHERE desde=(SELECT MAX(desde) FROM histolugar WHERE legajo=".$_POST["legajo"].") AND legajo=".$_POST["legajo"];
                $res_c_hist_lug = $conn->query($str_c_hist_lug);
                if($res_c_hist_lug->num_rows!=0)
                {
                    $obj_c_hist_lug = $res_c_hist_lug->fetch_object();
                    $id_c_hist_lug = $obj_c_hist_lug->id;
                    $fecha_ult_lugar_ant = strtotime('-1 day', strtotime(setFecha($_POST["desde"])));
                    $fecha_ult_lugar_ant = date('Y-m-d', $fecha_ult_lugar_ant);

                    $str_upd_hist_lug = "UPDATE histolugar SET hasta='".$fecha_ult_lugar_ant."' WHERE id=".$id_c_hist_lug;
                    $res_upd_hist_lug = $conn->query($str_upd_hist_lug);
                    escribir_log("personal", $legajo, $_POST["legajo"], "histolugar", 2, $str_upd_hist_lug, date("Y-m-d H:i:s"));
                }

                $str_hist_lug = "INSERT INTO histolugar(legajo, nrolugar, desde, hasta) VALUES (".$_POST["legajo"].", ".$_POST["lugaractu"].", '".setFecha($_POST["desde"])."', NULL)";
                $res_hist_lug = $conn->query($str_hist_lug);
                if(!$res_hist_lug)
                {
                    $data["success"] = false;
                    $data["error"] = $conn->error;
                }
                else
                {
                    $data["success"] = true;
                    $data["msg"] = "Lugar cargado correctamente";
                    escribir_log("personal", $legajo, $_POST["legajo"], "histolugar", 1, $str_hist_lug, date("Y-m-d H:i:s"));
                    
                    
                }
            }
        }
    }//End if(!$_POST["hasta"])    
    else //Se supone que se esta cargando un lugar anterior, dado que se ingresaron ambas fechas (desde y hasta)
    {
        //Controlo que la fecha en que se está cargando el lugar no coincida con fechas ya cargadas
        $str_cons_ex_lug_2 = "SELECT * FROM histolugar WHERE legajo=".$_POST["legajo"]." AND ('".setFecha($_POST["desde"])."' BETWEEN desde AND hasta OR '".setFecha($_POST["hasta"])."' BETWEEN desde AND hasta OR desde BETWEEN '".setFecha($_POST["desde"])."' AND '".setFecha($_POST["hasta"])."' OR hasta BETWEEN '".setFecha($_POST["desde"])."' AND '".setFecha($_POST["hasta"])."')";
        $res_cons_ex_lug_2 = $conn->query($str_cons_ex_lug_2);
        if($res_cons_ex_lug_2->num_rows>0)
        {
            $data["success"] = false;
            $data["error"] = "No se puede cargar el lugar, el periodo coincide con uno ya asignado al agente.";
        }
        else
        {
            //Validacion adicional, compruebo que la fecha desde no sea anterior a la de ingreso del empleado o posterior (dado que si es posterior, el usuario deberia cargarla como actual y dejar sin fecha "hasta")
            $str_ing = "SELECT fecha_carga FROM general WHERE legajo=".$_POST["legajo"]." AND '".setFecha($_POST["desde"])."' < DATE(fecha_carga)";
            $res_ing = $conn->query($str_ing);
            if($res_ing->num_rows>0 || setFecha($_POST["desde"]) >= $obj_cons_ex_lug_1->desde)
            {
                $data["success"] = false;
                $data["error"] = "No se puede cargar el lugar, la fecha de inicio del lugar es anterior a la fecha de ingreso del empleado al poder judicial o posterior a la ultima fecha de inicio del ultimo lugar.";
            }
            else
            {
                $str_hist_lug_2 = "INSERT INTO histolugar(legajo, nrolugar, desde, hasta) VALUES (".$_POST["legajo"].", ".$_POST["lugaractu"].", '".setFecha($_POST["desde"])."', '".setFecha($_POST["hasta"])."')";
                $res_hist_lug_2 = $conn->query($str_hist_lug_2);
                if(!$res_hist_lug_2)
                {
                    $data["success"] = false;
                    $data["error"] = $conn->error;
                }
                else
                {
                    $data["success"] = true;
                    $data["msg"] = "Lugar cargado correctamente";
                    escribir_log("personal", $legajo, $_POST["legajo"], "histolugar", 1, $str_hist_lug_2, date("Y-m-d H:i:s"));
                    
                }
            }
        }
    }
    echo json_encode($data);
}

//Des asigna un lugar a un agente
function deleteLugar()
{
    global $conn;
    $conn->autocommit(false);
    $data = array();
    try    
    {
        $str_cons = "SELECT * FROM histolugar WHERE id=".$_POST['idLugarElimin'];
        $res_cons = $conn->query($str_cons);
        if(!$res_cons)
        {
            throw new Exception("Error al encontrar el lugar. Error de BD: ".$conn->error);
        }
        $fila_cons = $res_cons->fetch_object();
        $legajo = $fila_cons->legajo;
        
        $str_del = "DELETE FROM histolugar WHERE id=".$_POST['idLugarElimin'];
        $res_del = $conn->query($str_del);
        if(!$res_del)
        {
            throw new Exception("Error al eliminar el lugar. Error de BD: ".$conn->error);
        }
        $leg = isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
        
        $str_search = "SELECT * FROM histolugar WHERE legajo=".$legajo." ORDER BY id DESC";
        $res_search = $conn->query($str_search);
        if(!$res_search)
        {
            throw new Exception("Error al consultar la base de datos. Error de BD: ".$conn->error);
        }
        if($res_search->num_rows==0)
        {
            $str_upd = "UPDATE general SET lugaractu=NULL WHERE legajo=".$legajo;
            $cons_upd = $conn->query($str_upd);
            if(!$cons_upd)
            {
                throw new Exception("Error al escribir la base de datos. Error de BD: ".$conn->error);
            }
        }
        else
        {
            $fila_search = $res_search->fetch_object();
            $ult_lugar = $fila_search->nrolugar;
            $str_upd = "UPDATE general SET lugaractu=".$ult_lugar." WHERE legajo=".$legajo;
            $cons_upd = $conn->query($str_upd);
            if(!$cons_upd)
            {
                throw new Exception("Error al escribir la base de datos. Error de BD: ".$conn->error);
            }
            $str_upd_hist_lug_2 = "UPDATE histolugar SET hasta=NULL WHERE id=".$fila_search->id;
            $res_upd_hist_lug_2 = $conn->query($str_upd_hist_lug_2);
            if(!$res_upd_hist_lug_2)
            {
                throw new Exception("Error al escribir la base de datos. Error de BD: ".$conn->error);
            }
            escribir_log("personal", $leg, $legajo, "histolugar", 2, $str_upd_hist_lug_2, date("Y-m-d H:i:s"));
        }
        escribir_log("personal", $leg, $legajo, "histolugar", 3, $str_del, date("Y-m-d H:i:s"));
        $conn->commit();
        $data["success"] = true;
        $data["msg"] = "Lugar eliminado correctamente.";
    }
    catch(Exception $e)
    {
        $conn->rollBack();
        $data["success"] = false;
        $data["error"] = $e->getMessage();
    }
    echo json_encode($data);
}
//Agrega un lugar a la tabla de la bd. Mauro
function altaLugar()
{
    global $conn;
    $data = array();    
    $str_cons = "SELECT * FROM lugar WHERE nombrelugar='".$_POST['nombreLugar']."'";
    //echo $str_cons;
    $res_cons = $conn->query($str_cons);
    if($res_cons->num_rows)
    {
         $data["success"] = false;
         $data["error"] = "Ya existe un lugar con el mismo nombre";
    }
    else
    {
        $str_cons = "SELECT * FROM lugar ORDER BY id DESC";
        $res_cons = $conn->query($str_cons);
        $fila_cons = $res_cons->fetch_object();
        
        $str_ins = "INSERT INTO lugar (nrolugar, nombrelugar, domicilio, tel, nro_fuero) VALUES (".($fila_cons->nrolugar+1).", UPPER('".$_POST["nombreLugar"]."'), UPPER('".$_POST["domicilioLugar"]."'), '".$_POST["telLugar"]."', '".$_POST["fuero"]."')";
        $res_ins = $conn->query($str_ins);
        if(!$res_ins)
        {
            $data["success"] = false;
            $data["error"] = $conn->error;
        }
        else
        {
            $data["success"] = true;
            $data["msg"] = "Lugar dado de alta correctamente";
            $leg = isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
            escribir_log("personal", $leg, 0, "lugar", 1, $str_ins, date("Y-m-d H:i:s"));
        }
        
    }
    echo json_encode($data);
}

//Elimina un lugar de la tabla de la bd. Mauro
function bajaLugar()
{
    global $conn;
    $data = array();    
    
    $str_nrolug = "SELECT * FROM lugar WHERE id=".$_POST['lugarElimin'];
    $res_nrolug = $conn->query($str_nrolug);
    $fila_nrolug = $res_nrolug->fetch_object();
    //echo $str_nrocarg;
    $str_cons = "SELECT * FROM histolugar WHERE nrolugar=".$fila_nrolug->nrolugar;
    //echo "<br/>".$str_cons;
    $res_ins = $conn->query($str_cons);
    if($res_ins->num_rows)
    {
        $data["success"] = false;
        $data["error"] = "Error al eliminar el lugar, hay agentes con dicho lugar asignado.";
    }
    else
    {
        $str_del = "DELETE FROM lugar WHERE id=".$_POST['lugarElimin'];
        //echo "<br/>".$str_del;
        $res_del = $conn->query($str_del);
        if(!$res_del)
        {
            $data["success"] = false;
            $data["error"] = "Error al eliminar el lugar. Intente nuevamente.";
        }
        else
        {    
            $data["success"] = true;
            $data["msg"] = "Lugar eliminado";
            $leg = isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
            escribir_log("personal", $leg, 0, "lugar", 3, $str_del, date("Y-m-d H:i:s"));
        }
    }
    echo json_encode($data);
}

//Modifica un lugar de la tabla de la bd. Mauro
function modificaLugar()
{
    global $conn;
    $data = array();    
    
    $str_lug = "SELECT * FROM lugar WHERE id=".$_POST['lugarModif'];
    $res_lug = $conn->query($str_lug);
    $fila_lug = $res_lug->fetch_object();
    //echo $str_nrocarg;
    
    $str_exist = "SELECT * FROM lugar WHERE nombrelugar='".$_POST['nombreLugarModif']."' AND id<>".$_POST['lugarModif'];
    $res_exist = $conn->query($str_exist);
    
    //Si el nuevo nombre que quieren modificar es igual a algun otro lugar ya cargado, largo error.
    if($res_exist->num_rows > 0)
    {
        $data["success"] = false;
        $data["error"] = "El nombre ingresado ya existe.";
    }
    else
    {
        $str_del = "UPDATE lugar SET nombrelugar=UPPER('".$_POST["nombreLugarModif"]."'), nro_fuero=".$_POST["fuero"]." WHERE id=".$_POST['lugarModif'];
        //echo $str_del;
        $res_del = $conn->query($str_del);
        if(!$res_del)
        {
            $data["success"] = false;
            $data["error"] = "Error al modificar el lugar. Intente nuevamente.";
        }
        else
        {    
            $data["success"] = true;
            $data["msg"] = "Lugar modificado";
            $leg = isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
            escribir_log("personal", $leg, 0, "lugar", 2, $str_del, date("Y-m-d H:i:s"));
        }
    }
    echo json_encode($data);
}
?>