<?php
session_set_cookie_params(0);
session_start();
require('system.config.php');

$tarea = $_SERVER['REQUEST_METHOD'];

switch ($tarea) {
	case 'GET':
        $act = isset($_GET["act"])?$_GET["act"]:"";
        switch ($act) 
        {
            case 'getPais':
                getPais();
                break;
            case 'getLocalidad':
                getLocalidad();
                break;    
        }
        break;
	default:
		echo "({ success: false, error: 1})";
		break;
}


function getPais() 
{
    global $conn;
    
    $str = "SELECT * FROM paises ORDER BY nombre_pais ASC";
    $result = $conn->query($str);

    if($result)
    {
        $nbrows = $result->num_rows;
        $arr = array();
        while($obj = $result->fetch_object())
        {
            $arr[] = $obj;
        }

    echo "({ total: ".$nbrows.",  results: ".json_encode($arr)."})";
    }
    else
    {
        echo "({ total: 0,  error: ".$conn->error."})";
    }
    
}

function getLocalidad() 
{
    global $conn;
    
    $str = "SELECT * FROM localidad ORDER BY nombre ASC";
    $result = $conn->query($str);

    if($result)
    {
        $nbrows = $result->num_rows;
        $arr = array();
        while($obj = $result->fetch_object())
        {
            $arr[] = $obj;
        }

    echo "({ total: ".$nbrows.",  results: ".json_encode($arr)."})";
    }
    else
    {
        echo "({ total: 0,  error: ".$conn->error."})";
    }
}
?>