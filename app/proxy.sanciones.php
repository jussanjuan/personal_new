<?php
session_set_cookie_params(0);
session_start();
require('system.config.php');
require('proxy.feriados.php');

$tarea = $_SERVER['REQUEST_METHOD'];
$tarea = isset($_POST["idSancionElimin"])?'DELETE':$_SERVER['REQUEST_METHOD'];
$act = isset($_POST["act"])?$_POST["act"]:"";
//echo $tarea;
switch ($tarea) {
	case 'GET':
        $act = isset($_GET["act"])?$_GET["act"]:"";
        switch ($act)
        {
            case "":
                getSanciones();
                break;
        }
        break;
	case 'POST':
        switch ($act)
        {
            case "": //Si es vacio, asigna una sancion al agente. Mauro
                setSancion();
                break;
            case "altaSancion": //Alta de una nueva sancion en el sistema. Mauro
                altaSancion();
                break;
            case "bajaSancion": //Eliminar una sancion del sistema. Mauro
                bajaSancion();
                break;
            case "modificaSancion": //Modifica una sancion del sistema. Mauro
                modificaSancion();
                break;
        }
		break;
	case 'PUT':
		break;
	case 'DELETE':
        deleteSancion(); //Des asigna una sancion a un agente
		break;
	default:
		echo "({ failure: 'Error por default'})";
		break;
}


function getSanciones() {
	global $conn;

	if (isset($_GET["legajo"])) {
		$str = "SELECT sanciones.*, sancionestipo.nombre, sancionestipo.descripcion FROM sanciones, sancionestipo WHERE sanciones.tipo=sancionestipo.id AND sanciones.legajo=".$_GET["legajo"]." ORDER BY sanciones.tipo ASC";
	} else {
		$str = "SELECT * FROM sancionestipo ORDER BY id ASC";
	}
	
	$result = $conn->query($str);
	$nbrows = $result->num_rows;
	$arr = array();
	while ($obj = $result->fetch_object()){
		$arr[] = $obj;
	}

	echo "({ total: ".$nbrows.",  results: " . json_encode($arr) . "})";
}

function getCargoById()
{
    global $conn;
    $data = array();
    $str = "SELECT * FROM cargos WHERE id=".$_GET["idCargo"];
        
    $result = $conn->query($str);
    $nbrows = $result->num_rows;
    $arr = array();
    while ($obj = $result->fetch_object()){
            $arr[] = $obj;

    }
    $data["total"] = $nbrows;
    $data["results"] = $arr;
    echo json_encode($data);
}


function setSancion() 
{
	global $conn;
    $data = array();
    $str_ins = "INSERT INTO sanciones (legajo, tipo, desde, hasta, dispuesta_por, expediente, observaciones) VALUES (".$_POST["legajo"].", ".$_POST["tipoSancion"].", '".setFecha($_POST["desde"])."', '".setFecha($_POST["hasta"])."', '".$_POST["dispuesta_por"]."', '".$_POST["expedienteSancion"]."', '".$_POST["observacionesSanciones"]."')";
    $cons_ins = $conn->query($str_ins);
    if($cons_ins)
    {
        $data["success"] = true;
        $data["msg"] = "Sanci&oacute;n cargada correctamente.";

        //Escribir log
        $legajo=isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
        //$str_log_ins = "INSERT INTO usuarios_log (legajo, tabla, log, fecha) VALUES (".$legajo.", 'sanciones', '".addslashes($str_ins)."', '".date("Y-m-d H:i:s")."')";
        //$res_log_gen = $conn->query($str_log_ins);
        escribir_log("personal", $legajo, $_POST["legajo"], "sanciones", 1, $str_ins, date("Y-m-d H:i:s"));
    }
    else
    {
        $data["success"] = false;
        $data["error"] = $conn->error;

    }
	echo json_encode($data);
}

//Des asigna un cargo a un agente
function deleteSancion()
{
    global $conn;
    $data = array();    
    $str_del = "DELETE FROM sanciones WHERE id=".$_POST['idSancionElimin'];
    $res_del = $conn->query($str_del);
    if($res_del)
    {
        $data["success"] = true;
        $data["msg"] = "Sanci&oacute;n eliminada correctamente.";

        //Escribir log
        $legajo=isset($_SESSION["legajo"])?$_SESSION["legajo"]:'';
        //$str_log_ins = "INSERT INTO usuarios_log (legajo, tabla, log, fecha) VALUES (".$legajo.", 'sanciones', '".addslashes($str_del)."', '".date("Y-m-d H:i:s")."')";
        //$res_log_gen = $conn->query($str_log_ins);
        escribir_log("personal", $legajo, 0, "sanciones", 3, $str_del, date("Y-m-d H:i:s"));
    }
    else
    {
        $data["success"] = false;
        $data["error"] = $conn->error;

    }
    echo json_encode($data);
}

//Ingresa una sancion en la tabla de la bd. Mauro
function altaSancion()
{
    global $conn;
    $data = array();    
    $str_cons = "SELECT * FROM cargos WHERE cargo='".$_POST['nombreCargo']."'";
    //echo $str_cons;
    $res_cons = $conn->query($str_cons);
    if($res_cons->num_rows)
    {
         $data["success"] = false;
         $data["error"] = "Ya existe un cargo con el mismo nombre";
    }
    else
    {
        $str_cons = "SELECT * FROM cargos ORDER BY id DESC";
        $res_cons = $conn->query($str_cons);
        $fila_cons = $res_cons->fetch_object();
        
        $str_ins = "INSERT INTO cargos (nrocargo, cargo, escalafon) VALUES (".($fila_cons->nrocargo+1).", UPPER('".$_POST["nombreCargo"]."'), ".$_POST["escalafon"].")";
        $res_ins = $conn->query($str_ins);
        if(!$res_ins)
        {
            $data["success"] = false;
            $data["error"] = $conn->error;
        }
        else
        {
            $data["success"] = true;
            $data["msg"] = "Cargo dado de alta correctamente";
        }
        
    }
    echo json_encode($data);
    
}

//Elimina una sancion de la tabla de la bd. Mauro
function bajaSancion()
{
    global $conn;
    $data = array();    
    
    $str_nrocarg = "SELECT * FROM cargos WHERE id=".$_POST['cargoElimin'];
    $res_nrocarg = $conn->query($str_nrocarg);
    $fila_nrocarg = $res_nrocarg->fetch_object();
    //echo $str_nrocarg;
    $str_cons = "SELECT * FROM histocargo WHERE nrocargo=".$fila_nrocarg->nrocargo;
    //echo "<br/>".$str_cons;
    $res_ins = $conn->query($str_cons);
    if($res_ins->num_rows)
    {
        $data["success"] = false;
        $data["error"] = "Error al eliminar el cargo, hay agentes con dicho cargo asignado.";
    }
    else
    {
        $str_del = "DELETE FROM cargos WHERE id=".$_POST['cargoElimin'];
        //echo "<br/>".$str_del;
        $res_del = $conn->query($str_del);
        if(!$res_del)
        {
            $data["success"] = false;
            $data["error"] = "Error al eliminar el cargo. Intente nuevamente.";
        }
        else
        {    
            $data["success"] = true;
            $data["msg"] = "Cargo eliminado";
        }
    }
    echo json_encode($data);
    
}

//Modifica una sancion de la tabla de la bd. Mauro
function modificaSancion()
{
    global $conn;
    $data = array();    
    
    $str_carg = "SELECT * FROM cargos WHERE id=".$_POST['cargoModif'];
    $res_carg = $conn->query($str_carg);
    $fila_carg = $res_carg->fetch_object();
    //echo $str_nrocarg;
    
    $str_exist = "SELECT * FROM cargos WHERE cargo='".$_POST['nombreCargoModif']."' AND id<>".$_POST['cargoModif'];
    $res_exist = $conn->query($str_exist);
    
    //Si el nuevo nombre que quieren modificar es igual a algun otro lugar ya cargado, largo error.
    if($res_exist->num_rows > 0)
    {
        $data["success"] = false;
        $data["error"] = "El nombre ingresado ya existe.";
    }
    else
    {
        $str_del = "UPDATE cargos SET cargo=UPPER('".$_POST["nombreCargoModif"]."'), escalafon=".$_POST["escalafon"]." WHERE id=".$_POST['cargoModif'];
        //echo "<br/>".$str_del;
        $res_del = $conn->query($str_del);
        if(!$res_del)
        {
            $data["success"] = false;
            $data["error"] = "Error al modificar el cargo. Intente nuevamente.";
        }
        else
        {    
            $data["success"] = true;
            $data["msg"] = "Cargo modificado";
        }
    }
    echo json_encode($data);
    
}
?>