<?php
session_set_cookie_params(0);
session_start();
require('system.config.php');
require('proxy.feriados.php');

$tarea = $_SERVER['REQUEST_METHOD'];

switch ($tarea) 
{
	case 'GET':
        $act = $_GET["act"];   
        switch ($act)
        {
            case 'getTiposPermiso':
                getTiposPermiso();
                break;
            case 'ntlm':
                getUserFromApacheHeaders();
                break;
            case 'ntlmv2':
                ntlmv2();
                break;
            case 'ldap_withpass':
                connectLDAP_withpass();
                break;    
            case 'ldap_matias':
                connectLDAP_matias();
                break;                    
            default:
                echo "({ failure: 'Error por default'})";
                break;
        }
        break;
    case 'POST':
        $act = $_POST["act"];
        switch ($act)
        {
            case 'chgPass':
                changePassword();
                break;
            case 'setPermisos':
                setPermisos();
                break;    
        }
        break;   
}


function changePassword() 
{
    global $conn;
    $data = array();
    if(!isset($_SESSION["legajo"]))
    {
        $data["success"] = false;
        $data["error"] = "Usuario no cargado correctamente, vuelva a iniciar sesi&oacute;n";
    }
    else
    {
        $str_cons = "SELECT * FROM usuarios WHERE legajo=".$_SESSION["legajo"]." AND password='".MD5($_POST["oldPass"])."'";
        $result_cons = $conn->query($str_cons);
        $nbrows = $result_cons->num_rows;
        
        if($nbrows == 0)
        {
            $data["success"] = false;
            $data["error"] = "La contrase&ntilde;a anterior es incorrecta";
            
        }
        else
        {
            $str_hist = "SELECT * FROM historial_passwords WHERE legajo=".$_SESSION["legajo"]." AND password='".MD5($_POST["newPass"])."'";
            $result_hist = $conn->query($str_hist);
            $nbrows_hist = $result_hist->num_rows;

            if($nbrows_hist != 0)
            {
                $data["success"] = false;
                $data["error"] = "Contrase&ntilde;a ya usada anteriormente. Elija una diferente.";

            }
            else
            {
                $str_upd = "UPDATE usuarios SET password='".MD5($_POST["newPass"])."', firstPass='no' WHERE legajo=".$_SESSION["legajo"];
                $result = $conn->query($str_upd);
                if(!$result)
                {
                    $data["success"]= false;
                    $data["error"] = "Error al cambiar la contrase&ntilde;a, intente nuevamente. Error: ".$conn->error;
                }
                else
                {
                    $data["success"] = true;
                    $data["msg"] = "Contrase&ntilde;a cambiada correctamente";
                    
                    //Insertar en la tabla historial_passwords
                    $str_upd_hist = "INSERT INTO historial_passwords (legajo, password) VALUES (".$_SESSION["legajo"].", '".MD5($_POST["newPass"])."')";
                    $conn->query($str_upd_hist);
                    
                    // Escribir log
                    $leg = isset($_SESSION["legajo"])? $_SESSION["legajo"]:'';
                    //$str_limpia = addslashes($str_upd);
                    //$str_log = "INSERT INTO usuarios_log (legajo, tabla, log, fecha) VALUES (".$leg.", 'usuarios', '".$str_limpia."', '".date("Y-m-d H:i:s")."')";
                    //$res_log = $conn->query($str_log);
                    escribir_log("personal", $leg, $leg, "usuarios", 2, $str_upd, date("Y-m-d H:i:s"));
                    escribir_log("personal", $leg, $leg, "historial_passwords", 1, $str_upd_hist, date("Y-m-d H:i:s"));
                }
            }
        }
    }
    echo json_encode($data);
}

function getTiposPermiso() 
{
    global $conn;
    $data = array();
    
    $str_cons = "SELECT * FROM usuarios WHERE legajo=".$_GET["leg"];
    $result_cons = $conn->query($str_cons);
    $nbrows = $result_cons->num_rows;
        
    if($nbrows == 0)
    {
        $data["success"] = false;
        $data["error"] = "No se encuentra el usuario";
        
    }
    else
    {
        $fila = $result_cons->fetch_object();
        $data["success"] = true;
        $data["permiso_pest"] = $fila->permiso;
        $data["permiso_vista"] = $fila->permiso_vista;
        
    }
    
    echo json_encode($data);
}

function setPermisos() 
{
    global $conn;
    $data = array();
    
    $str = "UPDATE usuarios SET permiso_vista=".$_POST["tipoPermVista"].", permiso=".$_POST["tipoPermPest"]." WHERE legajo=".$_POST["leg"];
    $result = $conn->query($str);
    if(!$result)
    {
        $data["success"] = false;
        $data["error"] = "No se encuentra el usuario";
        
    }
    else
    {
        $data["success"] = true;
        $data["msg"] = "Permisos del usuario editados correctamente";
    }
    
    echo json_encode($data);
}

function getUserFromApacheHeaders()
{
        
    // loune 25/3/2006, updated 22/08/2009
    // For more information see:
    // http://siphon9.net/loune/2007/10/simple-lightweight-ntlm-in-php/
    // 
    // This script is obsolete, you should see
    // http://siphon9.net/loune/2009/09/ntlm-authentication-in-php-now-with-ntlmv2-hash-checking/
    //
    // NTLM specs http://davenport.sourceforge.net/ntlm.html

    $data = array();
    $headers = apache_request_headers();
    if(!isset($headers['Authorization']))
    {
        header('HTTP/1.1 401 Unauthorized');
        header('WWW-Authenticate: NTLM');
        exit;
    }

    $auth = $headers['Authorization'];
    
    if (substr($auth,0,5) == 'NTLM ') 
    {
        $msg = base64_decode(substr($auth, 5));
        //echo $msg;
        if (substr($msg, 0, 8) != "NTLMSSP\x00")
        {
            $data["success"] = false;
            $data["error"]= "Cabecera no reconocida";
        }
        else
        {
            if ($msg[8] == "\x01") 
            {
                $msg2 = "NTLMSSP\x00\x02\x00\x00\x00".
                    "\x00\x00\x00\x00". // target name len/alloc
                  "\x00\x00\x00\x00". // target name offset
                  "\x01\x02\x81\x00". // flags
                  "\x00\x00\x00\x00\x00\x00\x00\x00". // challenge
                  "\x00\x00\x00\x00\x00\x00\x00\x00". // context
                  "\x00\x00\x00\x00\x00\x00\x00\x00"; // target info len/alloc/offset
                header('HTTP/1.1 401 Unauthorized');
                header('WWW-Authenticate: NTLM '.trim(base64_encode($msg2)));
                exit;
            }
            else if ($msg[8] == "\x03") 
            {
                function get_msg_str($msg, $start, $unicode = true) 
                {
                    $len = (ord($msg[$start+1]) * 256) + ord($msg[$start]);
                    $off = (ord($msg[$start+5]) * 256) + ord($msg[$start+4]);
                    if ($unicode)
                    {
                        return str_replace("\0", '', substr($msg, $off, $len));
                    }
                    else
                    {
                        return substr($msg, $off, $len);
                    }
                }
                $user = get_msg_str($msg, 36);
                $domain = get_msg_str($msg, 28);
                $workstation = get_msg_str($msg, 44);
                
                $data["user"] = $user;
                $data["domain"] = $domain;
                $data["workstation"] = $workstation;
                $res_LDAP = connectLDAP($data["user"]);
                //var_dump($res_LDAP);
                if($res_LDAP["success"]==true)
                {
                    $data["success"] = true;
                    $data["msg"] = "Usuario logueado correctamente";
                }
                else
                {
                    $data["success"] = false;
                    $data["error"]= "No se pudo conectar";
                }
            }
        }
    }
    else
    {
        $data["success"] = false;
        $data["error"]= "No se pudo conectar";
    }
    echo json_encode($data);
}

function connectLDAP_withpass()
{
    $user = $_GET["user"];
    $domain = $_GET["domain"];
    //Conexion a LDAP, funciona!
    
    $adServer = "ldap://jussanjuan.gov.ar";
    
    $ldap = ldap_connect($adServer);
    $username = $user;
    $dominio = $domain;
    $password = 'jusmba1986;)'; //Introducir password aca

    $ldaprdn = 'jussanjuan' . "\\" . $username;

    ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
    ldap_set_option($ldap, LDAP_OPT_REFERRALS, 0);

    $bind = @ldap_bind($ldap, $ldaprdn, $password);

    if ($bind) 
    {
        $user_totest = "matias.ferrari";
        $filter="(sAMAccountName=$user_totest)";
        $result = ldap_search($ldap,"dc=jussanjuan,dc=gov,dc=ar",$filter);
        
        ldap_sort($ldap,$result,"sn");
        $info = ldap_get_entries($ldap, $result);
        
        if($info["count"]!=1)
        {
            $msg = "No se encontro el usuario";
            echo $msg;
        }
        else
        {
            echo "<p>You are accessing <strong> ". $info[0]["sn"][0] .", " . $info[0]["givenname"][0] ."</strong><br /> (" . $info[0]["samaccountname"][0] .")</p>\n";
            echo '<pre>';
            //var_dump($info);
            echo '</pre>';
            $userDn = $info[0]["distinguishedname"][0]; 
        }
        @ldap_close($ldap);
    } 
    else 
    {
        $msg = "Invalid email address / password";
        echo $msg;
    }
}

function connectLDAP($userLDAP)
{
    $data = array();
    
    $adServer = "ldap://jussanjuan.gov.ar";
    
    $ldap = ldap_connect($adServer);
    
    //Usuario de aplicaciones utilizado para loguearnos al AD
    $usuario_autorizado = "svaplicacion";
    $pass_autorizado = "S4nJu4n2018";
    $dominio = "jussanjuan";
    
    $ldaprdn = $dominio."\\".$usuario_autorizado;

    ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
    ldap_set_option($ldap, LDAP_OPT_REFERRALS, 0);

    $bind = @ldap_bind($ldap, $ldaprdn, $pass_autorizado);

    if ($bind) 
    {
        //Usuario a buscar
        $user_totest = $userLDAP;
        $filter="(sAMAccountName=$user_totest)";
        $result = ldap_search($ldap,"dc=jussanjuan,dc=gov,dc=ar",$filter);
        
        //ldap_sort($ldap,$result,"sn");
        $info = ldap_get_entries($ldap, $result);
        
        if($info["count"]!=1)
        {
            $data["success"] = false;
            $data["error"] = "Usuario o password incorrecto";
        }
        else
        {
            $data["success"] = true;
            $data["msg"] = "Usuario conectado correctamente al Active Directory";
            /*echo "<p>You are accessing <strong> ". $info[0]["sn"][0] .", " . $info[0]["givenname"][0] ."</strong><br /> (" . $info[0]["samaccountname"][0] .")</p>\n";
            echo '<pre>';
            //var_dump($info);
            echo '</pre>';
            $userDn = $info[0]["distinguishedname"][0]; */
        }
        @ldap_close($ldap);
    } 
    else 
    {
        $data["success"] = false;
        $data["error"] = "No fue posible conectar con el Active Directory";
    }

    return $data;
}

function connectLDAP_matias(){
   //$user = $_GET["user"];
   $user = 'matias.ferrari2';
   //$domain = $_GET["domain"];
   $domain = 'jussanjuan.gov.ar';
   //Conexion a LDAP, funciona!
  
   $adServer = "ldap://jussanjuan.gov.ar";
   $adServer_2 = "10.107.180.2";   
   $ldap = ldap_connect($adServer);
   $username = $user;
   $dominio = $domain;
   $password = ''; //Introducir password aca

   $ldaprdn = 'jussanjuan' . "\\" . $username;
   $ldaprdn_2 = $username."@jussanjuan";
   ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
   ldap_set_option($ldap, LDAP_OPT_REFERRALS, 0);

   $bind = @ldap_bind($ldap, $ldaprdn_2, $password);

   echo $bind;
   if ($bind)
   {
       $filter="(sAMAccountName=$username)";
       $result = ldap_search($ldap,"dc=jussanjuan,dc=gov,dc=ar",$filter);
       ldap_sort($ldap,$result,"sn");
       $info = ldap_get_entries($ldap, $result);
      
       for ($i=0; $i<$info["count"]; $i++)
       {
           if($info['count'] > 1)
               break;
           echo "<p>You are accessing <strong> ". $info[$i]["sn"][0] .", " . $info[$i]["givenname"][0] ."</strong><br /> (" . $info[$i]["samaccountname"][0] .")</p>\n";
           echo '<pre>';
           var_dump($info);
           echo '</pre>';
           $userDn = $info[$i]["distinguishedname"][0];
       }
       @ldap_close($ldap);
   }
   else
   {
       $msg = "Invalid email address / password";
       echo $msg;
   }
}

function ntlmv2()
{
    include('ntlm.php');
    function get_ntlm_user_hash($user) 
    {
        $userdb = array('loune'=>'test', 'you'=>'gg', 'a'=> 'a');
        if (!isset($userdb[strtolower($user)]))
            return false;
        return mhash(MHASH_MD4, ntlm_utf8_to_utf16le($userdb[strtolower($user)]));
    }
    session_start();
    $auth = ntlm_prompt("testwebsite", "testdomain", "mycomputer", "testdomain.local", "mycomputer.local", "get_ntlm_user_hash");
    if ($auth['authenticated']) 
    {
        print "You are authenticated as $auth[username] from $auth[domain]/$auth[workstation]";
    }
}