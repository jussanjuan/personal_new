<?php
set_time_limit(0);
session_set_cookie_params(0);
session_start();
require('proxy.feriados.php');
//============================================================+
// File name   : example_061.php
// Begin       : 2010-05-24
// Last Update : 2014-01-25
//
// Description : Example 061 for TCPDF class
//               XHTML + CSS
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract Tabla
 * @author Mauro Avendaño
 * @since 2010-05-25
 */

// Include the main TCPDF library (search for installation path).
require_once('../res/TCPDF-master/tcpdf.php');

// FUNCIONES DE CONVERSION DE NUMEROS A LETRAS.
 
function centimos()
{
    global $importe_parcial;
    //echo $importe_parcial;
    $num_letra="";
    $importe_parcial = number_format($importe_parcial, 2, ".", "") * 100;
    if ($importe_parcial > 0)
        $num_letra = " con ".decena_centimos($importe_parcial);
    else
        $num_letra = "";
 
    return $num_letra;
}
 
function unidad_centimos($numero)
{
    $num_letra="";
    switch (trim($numero))
    {
        case 9:
        {
            $num_letra = "nueve centavos";
            break;
        }
        case 8:
        {
            $num_letra = "ocho centavos";
            break;
        }
        case 7:
        {
            $num_letra = "siete centavos";
            break;
        }
        case 6:
        {
            $num_letra = "seis centavos";
            break;
        }
        case 5:
        {
            $num_letra = "cinco centavos";
            break;
        }
        case 4:
        {
            $num_letra = "cuatro centavos";
            break;
        }
        case 3:
        {
            $num_letra = "tres centavos";
            break;
        }
        case 2:
        {
            $num_letra = "dos centavos";
            break;
        }
        case 1:
        {
            $num_letra = "un centavo";
            break;
        }
    }
    return $num_letra;
}
 
function decena_centimos($numero)
{   
    $numero = trim($numero);
    if ($numero >= 10)
    {
        if ($numero >= 90 && $numero <= 99)
        {
              if ($numero == 90)
                  return "noventa centavos";
              else if ($numero == 91)
                  return "noventa y un centavos";
              else
                  return "noventa y ".unidad_centimos($numero - 90);
        }
        if ($numero >= 80 && $numero <= 89)
        {
            if ($numero == 80)
                return "ochenta centavos";
            else if ($numero == 81)
                return "ochenta y un centavos";
            else
                return "ochenta y ".unidad_centimos($numero - 80);
        }
        if ($numero >= 70 && $numero <= 79)
        {
            if ($numero == 70)
                return "setenta centavos";
            else if ($numero == 71)
                return "setenta y un centavos";
            else
                return "setenta y ".unidad_centimos($numero - 70);
        }
        if ($numero >= 60 && $numero <= 69)
        {
            if ($numero == 60)
                return "sesenta centavos";
            else if ($numero == 61)
                return "sesenta y un centavos";
            else
                return "sesenta y ".unidad_centimos($numero - 60);
        }
        if ($numero >= 50 && $numero <= 59)
        {
            if ($numero == 50)
            {
                return "cincuenta centavos";
            }
            else if ($numero == 51)
            {
                return "cincuenta y un centavos";
            }
            else
            {
                return "cincuenta y ".unidad_centimos($numero - 50);
            }
        }
        if ($numero >= 40 && $numero <= 49)
        {
            if ($numero == 40)
                return "cuarenta centavos";
            else if ($numero == 41)
                return "cuarenta y un centavos";
            else
                return "cuarenta y ".unidad_centimos($numero - 40);
        }
        if ($numero >= 30 && $numero <= 39)
        {
            if ($numero == 30)
                return "treinta centavos";
            else if ($numero == 91)
                return "treinta y un centavos";
            else
                return "treinta y ".unidad_centimos($numero - 30);
        }
        if ($numero >= 20 && $numero <= 29)
        {
            if ($numero == 20)
                return "veinte centavos";
            else if ($numero == 21)
                return "veintiun centavos";
            else
                return "veinti".unidad_centimos($numero - 20);
        }
        if ($numero >= 10 && $numero <= 19)
        {
            if ($numero == 10)
                return "diez centavos";
            else if ($numero == 11)
                return "once centavos";
            else if ($numero == 12)
                return "doce centavos";
            else if ($numero == 13)
                return "trece centavos";
            else if ($numero == 14)
                return "catorce centavos";
            else if ($numero == 15)
                return "quince centavos";
            else if ($numero == 16)
                return "dieciseis centavos";
            else if ($numero == 17)
                return "diecisiete centavos";
            else if ($numero == 18)
                return "dieciocho centavos";
            else if ($numero == 19)
                return "diecinueve centavos";
        }
    }
    else
        return unidad_centimos($numero);
}
 
function unidad($numero)
{
    $num="";
    $numero = trim($numero);
    switch ($numero)
    {
        
        case 9:
        {
            $num = "nueve";
            break;
        }
        case 8:
        {
            $num = "ocho";
            break;
        }
        case 7:
        {
            $num = "siete";
            break;
        }
        case 6:
        {
            $num = "seis";
            break;
        }
        case 5:
        {
            $num = "cinco";
            break;
        }
        case 4:
        {
            $num = "cuatro";
            break;
        }
        case 3:
        {
            $num = "tres";
            break;
        }
        case 2:
        {
            $num = "dos";
            break;
        }
        case 1:
        {
            $num = "uno";
            break;
        }
    }
    return $num;
}
 
function decena($numero)
{
    $num_letra="";
    $numero = trim($numero);
    if ($numero >= 90 && $numero <= 99)
    {
        $num_letra = "noventa ";
 
        if ($numero > 90)
            $num_letra = $num_letra."y ".unidad($numero - 90);
    }
    else if ($numero >= 80 && $numero <= 89)
    {
        $num_letra = "ochenta ";
 
        if ($numero > 80)
            $num_letra = $num_letra."y ".unidad($numero - 80);
    }
    else if ($numero >= 70 && $numero <= 79)
    {
            $num_letra = "setenta ";
 
        if ($numero > 70)
            $num_letra = $num_letra."y ".unidad($numero - 70);
    }
    else if ($numero >= 60 && $numero <= 69)
    {
        $num_letra = "sesenta ";
 
        if ($numero > 60)
            $num_letra = $num_letra."y ".unidad($numero - 60);
    }
    else if ($numero >= 50 && $numero <= 59)
    {
        $num_letra = "cincuenta ";
 
        if ($numero > 50)
            $num_letra = $num_letra."y ".unidad($numero - 50);
    }
    else if ($numero >= 40 && $numero <= 49)
    {
        $num_letra = "cuarenta ";
 
        if ($numero > 40)
            $num_letra = $num_letra."y ".unidad($numero - 40);
    }
    else if ($numero >= 30 && $numero <= 39)
    {
        $num_letra = "treinta ";
 
        if ($numero > 30)
            $num_letra = $num_letra."y ".unidad($numero - 30);
    }
    else if ($numero >= 20 && $numero <= 29)
    {
        if ($numero == 20)
        {
            $num_letra = "veinte ";
        }
        else
        {
            $num_letra = "veinti".unidad($numero - 20);
        }
    }
    else if ($numero >= 10 && $numero <= 19)
    {
        switch ($numero)
        {
            case 10:
            {
                $num_letra = "diez ";
                break;
            }
            case 11:
            {
                $num_letra = "once ";
                break;
            }
            case 12:
            {
                $num_letra = "doce ";
                break;
            }
            case 13:
            {
                $num_letra = "trece ";
                break;
            }
            case 14:
            {
                $num_letra = "catorce ";
                break;
            }
            case 15:
            {
                $num_letra = "quince ";
                break;
            }
            case 16:
            {
                $num_letra = "dieciseis ";
                break;
            }
            case 17:
            {
                $num_letra = "diecisiete ";
                break;
            }
            case 18:
            {
                $num_letra = "dieciocho ";
                break;
            }
            case 19:
            {
                $num_letra = "diecinueve ";
                break;
            }
        }
    }
    else
        $num_letra = unidad($numero);
 
    return $num_letra;
}
 
function centena($numero)
{
    $num_letra="";
    $numero = trim($numero);
    if ($numero >= 100)
    {
        if ($numero >= 900 & $numero <= 999)
        {
            $num_letra = "novecientos ";
 
            if ($numero > 900)
            {
                $num_letra = $num_letra.decena($numero - 900);
            }
        }
        else if ($numero >= 800 && $numero <= 899)
        {
            $num_letra = "ochocientos ";
 
            if ($numero > 800)
                $num_letra = $num_letra.decena($numero - 800);
        }
        else if ($numero >= 700 && $numero <= 799)
        {
            $num_letra = "setecientos ";
 
            if ($numero > 700)
                $num_letra = $num_letra.decena($numero - 700);
        }
        else if ($numero >= 600 && $numero <= 699)
        {
            $num_letra = "seiscientos ";
 
            if ($numero > 600)
                $num_letra = $num_letra.decena($numero - 600);
        }
        else if ($numero >= 500 && $numero <= 599)
        {
            $num_letra = "quinientos ";
 
            if ($numero > 500)
                $num_letra = $num_letra.decena($numero - 500);
        }
        else if ($numero >= 400 && $numero <= 499)
        {
            $num_letra = "cuatrocientos ";
 
            if ($numero > 400)
                $num_letra = $num_letra.decena($numero - 400);
        }
        else if ($numero >= 300 && $numero <= 399)
        {
            $num_letra = "trescientos ";
 
            if ($numero > 300)
                $num_letra = $num_letra.decena($numero - 300);
        }
        else if ($numero >= 200 && $numero <= 299)
        {
            $num_letra = "doscientos ";
 
            if ($numero > 200)
                $num_letra = $num_letra.decena($numero - 200);
        }
        else if ($numero >= 100 && $numero <= 199)
        {
            if ($numero == 100)
                $num_letra = "cien ";
            else
                $num_letra = "ciento ".decena($numero - 100);
        }
    }
    else
    {
        $num_letra = decena($numero);

    }
 
    return $num_letra;
}
 
function cien()
{
    global $importe_parcial;
    $parcial = 0; $car = 0;
    $num_letra="";
    while (substr($importe_parcial, 0, 1) == 0)
        $importe_parcial = substr($importe_parcial, 1, strlen($importe_parcial) - 1);
 
    if ($importe_parcial >= 1 && $importe_parcial <= 9.99)
        $car = 1;
    else if ($importe_parcial >= 10 && $importe_parcial <= 99.99)
        $car = 2;
    else if ($importe_parcial >= 100 && $importe_parcial <= 999.99)
        $car = 3;
    
    $parcial = substr($importe_parcial, 0, $car);
    $importe_parcial = substr($importe_parcial, $car);
    $num_letra = centena($parcial).centimos();
 
    return $num_letra;
}
 
function cien_mil()
{
    global $importe_parcial;
 
    $parcial = 0; $car = 0;
    $num_letra="";
    while (substr($importe_parcial, 0, 1) == 0)
        $importe_parcial = substr($importe_parcial, 1, strlen($importe_parcial) - 1);
    if ($importe_parcial >= 1000 && $importe_parcial <= 9999.99)
    {
        $car = 1;
    }
    else if ($importe_parcial >= 10000 && $importe_parcial <= 99999.99)
    {
        $car = 2;
    }
    else if ($importe_parcial >= 100000 && $importe_parcial <= 999999.99)
    {
        $car = 3;
    }
 
    $parcial = substr($importe_parcial, 0, $car);
    
    $importe_parcial = substr($importe_parcial, $car);
    
    if ($parcial > 0)
    {
        if ($parcial == 1)
        {
            $num_letra = "mil ";
        }   
        else
        {
            //echo "Num_letra: ".$num_letra."<br>";
            $num_letra = centena($parcial)." mil ";
        }
    }
 
    return $num_letra;
}
 
 
function millon()
{
    global $importe_parcial;
 
    $parcial = 0; $car = 0;
    $num_letras="";
    while (substr($importe_parcial, 0, 1) == 0)
        $importe_parcial = substr($importe_parcial, 1, strlen($importe_parcial) - 1);
 
    if ($importe_parcial >= 1000000 && $importe_parcial <= 9999999.99)
        $car = 1;
    else if ($importe_parcial >= 10000000 && $importe_parcial <= 99999999.99)
        $car = 2;
    else if ($importe_parcial >= 100000000 && $importe_parcial <= 999999999.99)
        $car = 3;
 
    $parcial = substr($importe_parcial, 0, $car);
    $importe_parcial = substr($importe_parcial, $car);
 
    if ($parcial == 1)
        $num_letras = "un millón ";
    else
        $num_letras = centena($parcial)." millones ";
 
    return $num_letras;
}
 
function convertir_a_letras($numero)
{
    global $importe_parcial;
    
    if(!strpos($numero, "."))
    {
        $importe_parcial = $numero.".00";
    }
    else
    {
        $importe_parcial = $numero;
    }
    //echo $importe_parcial;
    $num_letras="";
    $numero = trim($numero);
    if ($numero < 1000000000)
    {
        if ($numero >= 1000000 && $numero <= 999999999.99)
        {
            $num_letras = millon().cien_mil().cien();
        }
        else if ($numero >= 1000 && $numero <= 999999.99)
        {
            $num_letras = cien_mil().cien();
        }
        else if ($numero >= 1 && $numero <= 999.99)
        {
            $num_letras = cien();
        }
        else if ($numero >= 0.01 && $numero <= 0.99)
        {
            if ($numero == 0.01)
                $num_letras = "un centavo";
            else
                $num_letras = convertir_a_letras(($numero * 100)."/100")." centavos";
        }
    }
    return $num_letras;
}



// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Poder Judicial de San Juan');
$pdf->SetTitle('Poder Judicial de San Juan');
$pdf->SetSubject('Liquidacion de haberes');
$pdf->SetKeywords('PJSJ, Liquidacion haberes');

// set default header data
//echo PDF_HEADER_LOGO;
$logo = 'logo_poder_judicial_2.png';
//$logo = 'tcpdf_logo.jpg';
$pdf->SetHeaderData('', 0, 'Liquidación de haberes', 'Poder Judicial de San Juan');
//var_dump ($pdf->getHeaderData());
// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', '15'));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', '', 8);

if(!isset($_GET["all"]))
{
    //Busco todos los datos del empleado y preparo el recibo
    $legajo = $_GET["leg"];
    $mes_liq = $_GET["mes_liq"];
    $anio_liq = $_GET["anio_liq"];

    $str_mes = "SELECT * FROM cont_liquidacion_ci WHERE id=".$mes_liq;
    $cons_mes = $conn->query($str_mes);
    $obj_mes = $cons_mes->fetch_object();
    $nombre_mes = $obj_mes->nombre;
    $str_leg = "SELECT general.nombre, general.apellido, general.nrodoc, general.cuil, general.padron, general.legajo, general.sexo, general.fecha_carga, general.fecnac, cargos.cargo, escalafon.nombre as escalafon, cont_cuenta.cuenta, cont_sucursal.nombre as sucursal, cont_banco.nombre as banco FROM general LEFT JOIN cargos ON general.cargoactu=cargos.nrocargo LEFT JOIN escalafon ON cargos.escalafon=escalafon.id LEFT JOIN cont_cuenta ON general.legajo=cont_cuenta.legajo LEFT JOIN cont_sucursal ON cont_cuenta.id_sucursal=cont_sucursal.id LEFT JOIN cont_banco ON cont_sucursal.id_banco=cont_banco.id WHERE general.legajo=".$legajo;
    
    $cons_leg = $conn->query($str_leg);

    
    if(!$cons_leg || $cons_leg->num_rows==0)
    {
        //echo $str_leg."<br>";
        echo "No se encuentra el recibo seleccionado.";
    }
    else
    {
        $obj_leg = $cons_leg->fetch_object();
        $padron_1 = substr($obj_leg->padron,0,6).substr($obj_leg->padron,-1);
        $sexo = $obj_leg->sexo=='M'?'Var&oacute;n':'Mujer';
        $padron_relleno = str_pad($padron_1,7,"0");
        $date_carga=date_create($obj_leg->fecha_carga);
        $fecha_carga = date_format($date_carga,"d-m-Y");
        $date_nac=date_create($obj_leg->fecnac);
        $fecha_nac = date_format($date_nac,"d-m-Y");
        $str_rec = "SELECT cont_ci_raw.codigo, cont_ci_raw.importe, cont_ci_raw.anio_liq, cont_ci_raw.categoria, cont_codigos_liquidacion.detalle_liquidacion, cont_codigos_liquidacion.haber_descuento FROM cont_ci_raw, cont_codigos_liquidacion WHERE anio_liq='".$anio_liq."' AND mes_liq='".$mes_liq."' AND padron='".$padron_relleno."' AND cont_ci_raw.codigo=cont_codigos_liquidacion.cod_liquidacion AND cont_codigos_liquidacion.recibo_sueldo=1 ORDER BY cont_codigos_liquidacion.haber_descuento DESC, cont_codigos_liquidacion.cod_liquidacion ASC";
        $cons_rec = $conn->query($str_rec);
        $a = strlen(trim($anio_liq))==1?("0".$anio_liq):$anio_liq;
        if(!$cons_rec || $cons_rec->num_rows==0)
        {
            //echo $str_rec."<br>";
            echo "Error al consultar la base de datos 2.";
        }
        else
        {
            $codigos="";
            $detalles="";
            $haberes="";
            $retenciones="";
            $espacios="<br>";
            $total_haberes=0;
            $total_retenciones=0;
            $band_car = 0;
            while($obj_rec = $cons_rec->fetch_object())
            {
                //Aca obtengo el cargo que tenia el empleado en la fecha del recibo seleccionada
                $str_car = "SELECT cargo FROM cargos WHERE categoria=".$obj_rec->categoria;
                $cons_car = $conn->query($str_car);
                if($cons_car && $cons_car->num_rows==1) //Se identifica al cargo unívocamente
                {
                    $obj_car = $cons_car->fetch_object();
                    $cargo = $obj_car->cargo;
                }
                else //Hay varios cargos con la misma categoría, trato de encontrar el cargo con las fechas de inicio y fin
                {
                    $a_l = strlen($anio_liq)==1?"200".$anio_liq:"20".$anio_liq;
                    $mm = $mes_liq>12?12:$mes_liq;
                    $m_l = strlen($mm)==1?"0".$mm:$mm;
                    $fecha_rec = $a_l."-".$m_l."-01";
                    $str_hist = "SELECT * FROM histocargo, cargos WHERE histocargo.nrocargo=cargos.nrocargo AND histocargo.legajo=".$legajo." AND (('".$fecha_rec."' BETWEEN histocargo.desde AND histocargo.hasta) OR ('".$fecha_rec."'>histocargo.desde AND histocargo.hasta is null))";
                    //echo $str_hist."<br>";
                    $cons_hist = $conn->query($str_hist);
                    if($cons_hist && $cons_hist->num_rows==1)
                    {
                        $obj_hist = $cons_hist->fetch_object();
                        $cargo = $obj_hist->cargo;
                    }
                    else
                    {
                        $cargo = $obj_leg->cargo;
                    }
                }
                $anio_liquidacion = strlen(trim($obj_rec->anio_liq))==1?("0".trim($obj_rec->anio_liq)):trim($obj_rec->anio_liq);
                $codigos.=$obj_rec->codigo."<br>";
                $detalles.=$obj_rec->detalle_liquidacion."<br>";
                if($obj_rec->haber_descuento==1) //haberes
                {
                    $espacios.="<br>";//number_format(round($no_alcanzada,2),2,",",".")
                    $haberes.=number_format($obj_rec->importe,2,",",".")."<br>";
                    
                    if($obj_rec->codigo!='B01') //Codigo de dias trabajados, no es un importe, sino cantidad de dias. No se suma
                    {
                        $total_haberes+=$obj_rec->importe;
                    }
                }
                else
                {
                    $retenciones.=number_format($obj_rec->importe,2,",",".")."<br>";
                    $total_retenciones+=$obj_rec->importe;
                }
            }
            //echo $total_haberes."<br>";
            //echo $total_retenciones."<br>";
            //echo convertir_a_letras(60775.76-23434.76);
            $neto_letra = convertir_a_letras($total_haberes-$total_retenciones);
            $neto = $total_haberes-$total_retenciones;
            // add a page
            $pdf->AddPage();

            $html = '
            <style type="text/css">
               
            .logo{
                border-style: solid;
                height: 90px;
                overflow: hidden;
                width: 70px;
            }
            .td_titulo{
                background-color: #D8D8D8;
                font-weight: bold;
                align: "center";
            }
            .importe{
                text-align: "right";
            }
            .letra_chica {
              font-size: 8px;
            }
            .letra_grande{
                font-size: 14px;
            }
            .negrita{
                font-weight: bold;
            }
            .infoExtra{
              min-height: 400px;
              height: auto;
            }

            </style>
            <table class="tabla_1" border="1" cellpadding="4">
                <tr>
                    <td class="logo" rowspan="4"><img id="logoEmpresa" class="img-rounded" src="../res/img/logo_100.png" alt=""></td>
                    <td align="center" class="td_titulo" style="width: 180px;">Apellido y Nombre</td>
                    <td align="center" class="td_titulo" style="width: 75px">Padr&oacute;n</td>
                    <td align="center" class="td_titulo" style="width: 100px">Legajo</td>
                    <td align="center" class="td_titulo" style="width: 215px" colspan="2">Escalaf&oacute;n</td>
                </tr>
                <tr>    
                    <td style="width: 180px" rowspan="3">'.$obj_leg->apellido.', '.$obj_leg->nombre.'</td>
                    <td style="width: 75px" rowspan="3">'.$obj_leg->padron.'</td>
                    <td style="width: 100px" rowspan="3">'.$obj_leg->legajo.'</td>
                    <td style="width: 215px" colspan="2">'.$obj_leg->escalafon.'</td>
                </tr>
                <tr>    
                    <td align="center" class="td_titulo" style="width: 215px" colspan="2">Categor&iacute;a</td>
                </tr>
                <tr>    
                    <td style="width: 215px" colspan="2">'.$cargo.'</td>
                </tr>
                <tr>    
                    <td align="center" class="td_titulo">A&ntilde;o liquidaci&oacute;n</td>
                    <td align="center" class="td_titulo">Mes liquidaci&oacute;n</td>
                    <td align="center" class="td_titulo">Fecha nacimiento</td>
                    <td align="center" class="td_titulo">DNI '.$sexo.'</td>
                    <td align="center" class="td_titulo">CUIL</td>
                    <td align="center" class="td_titulo">Fecha ingreso</td>

                </tr>
                <tr>    
                    <td class="letra_grande">20'.$anio_liquidacion.'</td>
                    <td class="letra_grande">'.$nombre_mes.'</td>
                    <td>'.$fecha_nac.'</td>
                    <td>'.$obj_leg->nrodoc.'</td>
                    <td>'.$obj_leg->cuil.'</td>
                    <td>'.$fecha_carga.'</td>
                </tr>
                <tr>    
                    <td align="center" class="td_titulo">C&oacute;digo</td>
                    <td align="center" class="td_titulo" colspan="3">Concepto</td>
                    <td align="center" class="td_titulo">Haberes</td>
                    <td align="center" class="td_titulo">Descuentos</td>
                    
                </tr>
                <tr>    
                    <td>'.$codigos.'</td>
                    <td colspan="3">'.$detalles.'</td>
                    <td align="right">'.$haberes.'</td>
                    <td align="right">'.$espacios.$retenciones.'</td>
                </tr>
                <tr>
                    <td colspan="4" rowspan="2"></td>
                    <td align="center" class="td_titulo">Total haberes</td>
                    <td align="center" class="td_titulo">Total descuentos</td>
                </tr>
                <tr>
                    <td align="right">'.number_format($total_haberes,2,",",".").'</td>
                    <td align="right">'.number_format($total_retenciones,2,",",".").'</td>
                </tr>
                <tr>
                    <td colspan="5"></td>
                    <td align="center" class="td_titulo">Neto a cobrar</td>
                </tr>
                <tr>
                    <td colspan="5">Son: Pesos '.$neto_letra.'</td>
                    <td align="right">'.number_format($neto,2,",",".").'</td>
                </tr>
                <tr>
                    <td colspan="6">EFECTIVO ACREDITADO EN CUENTA: '.$obj_leg->cuenta.' del '.$obj_leg->banco.' - SUCURSAL '.strtoupper($obj_leg->sucursal).'</td>
                    
                </tr>
            </table>';

            // output the HTML content
            $pdf->writeHTML($html, true, false, true, false, '');

            // reset pointer to the last page
            $pdf->lastPage();

            // ---------------------------------------------------------

            //Close and output PDF document
            $pdf->Output('recibo_sueldo.pdf', 'I');
        }
    
    }
    
}

