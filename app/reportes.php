<?php
session_set_cookie_params(0);
session_start();

require('proxy.feriados.php');
require('select_reportes.php');
if(!isset($_SESSION))
{
    echo "Se ha vencido la sesi&oacute;n. Vuelva a iniciar sesi&oacute;n para poder seguir.";
    exit;
}
function toMoney($val,$symbol='$',$r=2)
{
    $n = $val; 
    $c = is_float($n) ? 1 : number_format($n,$r);
    $d = '.';
    $t = ',';
    $sign = ($n < 0) ? '-' : '';
    $i = $n=number_format(abs($n),$r,",","."); 
    $j = (($j = strlen($i)) > 3) ? $j % 3 : 0; 
    return  $symbol.$sign .($j ? substr($i,0, $j) + $t : '').preg_replace('/(\d{3})(?=\d)/',"$1" + $t,substr($i,$j)) ;
}
$legajos_vista = "";
switch ($_SESSION["permiso_vista"]) 
{
    case '0': //permiso total (Ver todos los agentes)
        $str_filt_agentes = "";
        break;
    case '1': //permiso individual (permiso para ver solo el propio usuario)
        $legajos_vista.=$_SESSION["legajo"]; //Agrego el legajo del propio usuario
        $str_filt_agentes = " AND g.legajo IN (".$legajos_vista.")";
        break;
    case '2': //Permisos especificos (ver el propio usuario y ciertos agentes)
        $str_permisos = "SELECT usuarios.legajo, permisos_vista.legajo_vista FROM usuarios, permisos_vista WHERE usuarios.legajo =permisos_vista.permiso_vista AND usuarios.legajo=".$_SESSION["legajo"];
        $res_permisos = $conn->query($str_permisos);

        while($obj = $res_permisos->fetch_object())
        {
            $legajos_vista.=$obj->legajo_vista.",";
        }
        $legajos_vista.=$_SESSION["legajo"]; //Agrego el legajo del propio usuario
        $str_filt_agentes = " AND g.legajo IN (".$legajos_vista.")";
        break;
    case '3': //Permiso de area (Ver propio usuario y todos los agentes del area a la que pertenece)
        //Obtengo el area del agente
        $str_area = "SELECT lugaractu FROM general WHERE legajo=".$_SESSION["legajo"];
        $res_area = $conn->query($str_area);
        $obj_area = $res_area->fetch_object();
        $area = $obj_area->lugaractu;

        $str_permisos = "SELECT legajo FROM general WHERE lugaractu=".$area;
        $res_permisos = $conn->query($str_permisos);
        while($obj = $res_permisos->fetch_object())
        {
            $legajos_vista.=$obj->legajo.",";
        }
        $legajos_vista.=$_SESSION["legajo"]; //Agrego el legajo del propio usuario
        $str_filt_agentes = " AND g.legajo IN (".$legajos_vista.")";
        break;
    case '4': //Permiso de fuero (Ver propio usuario y todos los agentes del fuero al que pertenece)
        //Obtengo el fuero del agente
        $str_fuero = "SELECT nro_fuero FROM lugar, general WHERE legajo=".$_SESSION["legajo"]." AND lugaractu=nrolugar";
        $res_fuero = $conn->query($str_fuero);
        $obj_fuero = $res_fuero->fetch_object();
        $fuero = $obj_fuero->nro_fuero;

        $str_permisos = "SELECT legajo FROM general, lugar WHERE lugaractu=nrolugar AND nro_fuero=".$fuero;
        $res_permisos = $conn->query($str_permisos);
        while($obj = $res_permisos->fetch_object())
        {
            $legajos_vista.=$obj->legajo.",";
        }
        $legajos_vista.=$_SESSION["legajo"]; //Agrego el legajo del propio usuario
        $str_filt_agentes = " AND g.legajo IN (".$legajos_vista.")";
        break;
    default:
        echo "Error al cargar los permisos. Actualice la p&aacute;gina e intente nuevamente.";
        exit;    
}
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="../res/DataTablesV2/datatables.min.css"/>
<link rel="stylesheet" type="text/css" href="../res/DataTablesV2/Buttons-1.2.2/css/buttons.dataTables.min.css"/>
<link rel="stylesheet" type="text/css" href="../res/css/estilos.css"/>
<link rel="stylesheet" type="text/css" href="../res/css/bootstrap-3.3.7-dist/css/bootstrap.css"/>
<!--<link rel="stylesheet" type="text/css" href="../res/css/bootstrap4.min.css"/>-->
<link rel="stylesheet" type="text/css" href="../res/css/bootstrap-select.css"/>
<link href="http://fonts.googleapis.com/css?family=Ropa+Sans|Maven+Pro|Kite+One|Bangers" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="../res/css/font-awesome.min.css">
<link rel="stylesheet" href="../res/css/datepicker.css"/>

<style>
.label.badge-pill {
border-radius:1em;
margin:0 0.25em;
}

#modalEvaluacion .modal-title {
  font-size: 30px;
}
#modalEvaluacion .modal-dialog {
  width: 80%;
  height: auto;
  margin: auto;
  padding: 0;
}

#modalEvaluacion .modal-content {
  height: auto;
  min-height: 80%;
  border-radius: 0;
}

/* estilos para la espera mientras se cargan los datos de la tabla*/
#overlay {
    height: 100%;
    width: 100%;
    position: absolute;
    top: 0px;
    left: 0px;
    z-index: 99999;
    background-color: gray;
    filter: alpha(opacity=75);
    -moz-opacity: 0.75;
    opacity: 0.75;
    display: none;
}
#overlay h2 {
    position: fixed;
    margin-left: 40%;
    top: 40%;
}

.popover {
    max-width: 100%;
}
</style>

<!-- ***********************************MODAL PARA VER LOS DATOS DE LA EVALUACION ***********************************-->
<div class="modal fade" id="modalEvaluacion" tabindex="-1" role="dialog" aria-labelledby="modalEvaluacion" aria-hidden="true" data-keyboard="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="headerModalEvaluacion">Ver evaluaci&oacute;n</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
        <div class="modal-body">
          <div class="container-fluid">
              <div class="panel panel-default">
                  <div class="panel-body">
                      <ul class="list-group">
                        <li class="list-group-item col-lg-12" id="motivo"></li>
                        <li class="list-group-item col-lg-6" id="evaluador"></li>
                        <li class="list-group-item col-lg-6" id="evaluado"></li>
                        <li class="list-group-item col-lg-6" id="fecha_inicio"></li>
                        <li class="list-group-item col-lg-6" id="fecha_fin"></li>
                        <li class="list-group-item col-lg-6" id="promedio"></li>
                        <li class="list-group-item col-lg-6" id="aplazos"></li>
                        <li class="list-group-item col-lg-6" id="tardanzas"></li>
                        <li class="list-group-item col-lg-6" id="licencias"></li>
                        <li class="list-group-item col-lg-6" id="conducta"></li>
                        <li class="list-group-item col-lg-6" id="capacitacion"></li>
                        <li class="list-group-item col-lg-4" id="asistencia_puntualidad"></li>
                        <li class="list-group-item col-lg-4" id="antiguedad_categoria"></li>
                        <li class="list-group-item col-lg-4" id="antiguedad_escalafon"></li>
                        <li class="list-group-item col-lg-12" id="observaciones"></li>
                        <li class="list-group-item col-lg-12" id="puntaje_total"></li>
                      </ul>
                  </div>
                  <table class="table" id="items">
                  </table>
              </div>
          </div><!-- End div container -->
          <input type="hidden" id="idEval" value="">
          <input type="hidden" id="idEvalPers" value="">
        </div><!-- End div modal body -->
        <div class="modal-footer" id="modalFooter">
          <button type="button" class="btn btn-primary" id="printEvaluacion"><span class="glyphicon glyphicon-print"></span> Imprimir</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        </div>
      
    </div><!-- End div modal content -->
  </div><!-- End div modal dialog -->
</div><!-- End modal Edit -->

<!-- **************************************************************************************************************************-->

</head>
<body>
<?php
require('system.config.php');
//var_dump($_SESSION);
?>
<!--<div class="container-narrow">-->
<!--<div class="interior rounded">    -->
<!--<h1 class="titulo">Reportes</h1>-->
<!--<div class="col-md-5 col-md-offset-1">-->
<div class="container-fluid">
<form role="form" name="frmReportes" id="frmReportes" action="reportes.php" method="get">
<div class="row" style="margin-top: 10px;">
<div class="form-group col-lg-4">
<label for="cbReportes">Categor&iacute;a</label>
<select name="cbReportes" class="form-control" id="cbReportes">
    <option value="">--Seleccione una opcion--</option>
    <?php
    if($_SESSION["permisos"][0]->pesta_reportes_basico=="1")
    {
        ?>
        <option value="1">Listado por Organismo</option>
        <option value="2">Listado por Cargos</option>
        <option value="3">Listado por escalaf&oacute;n</option>
        <option value="7">Listado por Titulo</option>
        <option value="8">Listado de Empleados Abogados</option>
        <option value="9">Listado General</option>
        <option value="21">Listado General Planta</option>
        <option value="10">Listado de bajas</option>
        <option value="11">Empleados con compensatorias</option>
        <option value="12">&Uacute;ltimos ingresantes</option>
        <option value="13">Listado por fuero</option>
        <option value="23">Listado de comisiones</option>
        <?php
        
    }
    //Permisos especificos
    if($_SESSION["permisos"][0]->pesta_reportes_evaluaciones=="1")
    {
        ?>
        <option value="14">Evaluaciones de desempe&ntilde;o</option>
        <option value="17">Factores de ascenso</option>
        <option value="18">Listado final concurso ascensos 2017</option>
        <?php
    }
    
    //Permisos especificos
    if($_SESSION["permisos"][0]->pesta_reportes_tesoreria=="1")
    {
        ?>
        <option value="16">Liquidaciones a BSJ</option>
        
        <?php
    }
    //Permisos especificos
    if($_SESSION["permisos"][0]->pesta_reportes_contabilidad=="1")
    {
        ?>
        <option value="19">Listado de hijos</option>
        <option value="24">Certificados de empleados</option>
        <option value="20">Recibos de sueldo por mes</option>
        <option value="22">Recibos de sueldo por empleado</option>
        <?php
    }
    //Permisos especificos
    if($_SESSION["permisos"][0]->pesta_reportes_rrhh=="1")
    {
        ?>
        <option value="25">Listado de capacitaciones</option>
        <option value="27">Listado de sanciones</option>
        <?php
    }
    //Permisos especificos
    if($_SESSION["permisos"][0]->pesta_reportes_personal=="1")
    {
        ?>
        <option value="15">Listado de licencias</option>
        <option value="26">Listado de solicitudes de licencias</option>
        <?php
    }

    ?>
 
</select>
</div>
<div class="form-group col-lg-4">
<label for="subcat">Subcategor&iacute;a</label>
<select name="subcat" class="form-control selectpicker" id="subcat" disabled="disabled" data-live-search="true">
    <option value="">-Seleccione subcategor&iacute;a-</option>
    </select>
</div>
<div class="form-group col-lg-4">
<label for="subsubcat">Sub Subcategor&iacute;a</label>
<select name="subsubcat" class="form-control selectpicker" id="subsubcat" disabled="disabled" data-live-search="true">
<option value="">-Seleccione sub subcategor&iacute;a-</option>
</select>
</div>
</div>
<div class="row" id="campos_fecha" style="display: none;">
    <div class="form-group col-lg-2">
        <label for="fecha_desde">Desde</label>
        <input type="text" name="fecha_desde" class="form-control" id="fecha_desde" placeholder="DD-MM-AAAA" disabled="disabled">
    </div>
    <div class="form-group col-lg-2">
        <label for="fecha_hasta">Hasta</label>
        <input type="text" name="fecha_hasta" class="form-control" id="fecha_hasta" placeholder="DD-MM-AAAA" disabled="disabled">
    </div>
</div>
<div class="row" id="campos_fecha3" style="display: none;">
    <div class="form-group col-lg-2">
        <label for="fecha_desde3">Carga desde</label>
        <input type="text" name="fecha_desde3" class="form-control" id="fecha_desde3" placeholder="DD-MM-AAAA" disabled="disabled">
    </div>
    <div class="form-group col-lg-2">
        <label for="fecha_hasta3">Carga hasta</label>
        <input type="text" name="fecha_hasta3" class="form-control" id="fecha_hasta3" placeholder="DD-MM-AAAA" disabled="disabled">
    </div>
</div>
<div class="row" id="campos_fecha2" style="display: none;">
    <div class="form-check col-lg-2">
        <input type="checkbox" class="form-check-input" id="mayor_edad">
        <label class="form-check-label" for="mayor_edad">Cumplen mayor&iacute;a de edad</label>
    </div>
    <div class="form-group col-lg-2">
        <label for="fecha_desde2">Desde</label>
        <input type="text" name="fecha_desde2" class="form-control" id="fecha_desde2" placeholder="DD-MM-AAAA" disabled="disabled">
    </div>
    <div class="form-group col-lg-2">
        <label for="fecha_hasta2">Hasta</label>
        <input type="text" name="fecha_hasta2" class="form-control" id="fecha_hasta2" placeholder="DD-MM-AAAA" disabled="disabled">
    </div>
</div>
<div class="row" id="campo_tipo_liquidacion" style="display: none;">
    <div class="form-group col-lg-2">
        <label for="combo_tipo_liquidacion">Tipo liquidaci&oacute;n</label>
        <select name="combo_tipo_liquidacion" class="form-control selectpicker" id="combo_tipo_liquidacion">
            <option value="">-Seleccione tipo liquidaci&oacute;n-</option>
            <option value="1">Haberes</option>
            <option value="2">Cuota alimentaria</option>
            <option value="3">Embargos</option>
        </select>
    </div>
</div>
<div class="row">
<div class="form-group col-lg-12">
<button name="botonGenerarReporte" id="botonGenerarReporte" class='btn btn-primary btn-lg' type="submit"><span class="glyphicon glyphicon-flash"></span> Generar reporte</button>
</div>
</div>
</form>    
<!--</div>-->
<!--</div>-->
<!--</div>-->
</div>
<?php

$cat = isset($_GET["cbReportes"])?$_GET["cbReportes"]:0;
$subcat = isset($_GET["subcat"])?$_GET["subcat"]:0;
$subsubcat = isset($_GET["subsubcat"])?$_GET["subsubcat"]:0;
$fecha_desde = isset($_GET["fecha_desde"])?$_GET["fecha_desde"]:0;
$fecha_hasta = isset($_GET["fecha_hasta"])?$_GET["fecha_hasta"]:0;

//Para el reporte de certificados:
$fecha_desde2 = isset($_GET["fecha_desde2"])?$_GET["fecha_desde2"]:0;
$fecha_hasta2 = isset($_GET["fecha_hasta2"])?$_GET["fecha_hasta2"]:0;

//Para el reporte de licencias:
$fecha_desde3 = isset($_GET["fecha_desde3"])?$_GET["fecha_desde3"]:0;
$fecha_hasta3 = isset($_GET["fecha_hasta3"])?$_GET["fecha_hasta3"]:0;

$combo_liquidacion = isset($_GET["combo_tipo_liquidacion"])?$_GET["combo_tipo_liquidacion"]:0;
switch ($cat) 
{
    case 1: //por organismo
            if($subcat!=0)
            {
                $cabecera = "<thead><tr><th>Legajo</th><th>Apellido</th><th>Nombre</th><th>Cargo</th><th>T&iacute;tulo</th><th>&Uacute;ltimo ascenso</th><th>Ingreso</th></tr></thead>";
                $foot = "<tfoot><tr><th>Lugar</th><th>Apellido</th><th>Nombre</th><th>Cargo</th><th>T&iacute;tulo</th><th>&Uacute;ltimo ascenso</th><th>Ingreso</th></tr></tfoot>";
                $str_sub = "SELECT * FROM lugar WHERE nrolugar=".$_GET["subcat"];
                $res = $conn->query($str_sub);
                $fila = $res->fetch_object();
                if($subsubcat!=0)
                {
                    $str_sub_sub = "SELECT DISTINCT year(desde) as anio FROM histolugar WHERE YEAR(desde)>=1960 AND YEAR(desde)<=YEAR(NOW()) ORDER BY year(desde) ASC";
                    $res_sub_sub = $conn->query($str_sub_sub);
                    $i_sub=0;
                    while($obj = $res_sub_sub->fetch_object())
                    {
                        if($i_sub==$subsubcat)
                        {
                            $anio = $obj->anio;
                        }
                        $i_sub++;
                    }
                    $titulo = "<h2>Listado por Organismo | ".$fila->nombrelugar." | A&ntilde;o ".$anio."</h2>";
                    $anio_desde = $anio."-01-01";
                    $anio_hasta = $anio."-12-31";
                    $str = "organismo_historico";
                    
                    $cabecera = "<thead><tr><th>Legajo</th><th>Apellido</th><th>Nombre</th><th>Cargo</th><th>Desde</th><th>Hasta</th><th>Activo</th></tr></thead>";
                    $foot = "<tfoot><tr><th>Legajo</th><th>Apellido</th><th>Nombre</th><th>Cargo</th><th>Desde</th><th>Hasta</th><th>Activo</th></tr></tfoot>";
                }
                else
                {
                    $titulo = "<h2>Listado por Organismo | ".$fila->nombrelugar."</h2>";
                    $str = "SELECT g.legajo, g.apellido, g.nombre, c.cargo, te.titulo, DATE_FORMAT(tabla1.ultimo_ascenso, '%d-%m-%Y') AS xultimo_ascenso, DATE_FORMAT(tabla2.fecha_ingreso, '%d-%m-%Y') AS xfecha_ingreso 
                        FROM general g
                        LEFT JOIN (SELECT h.legajo, MAX( h.desde ) as ultimo_ascenso FROM histocargo h GROUP BY h.legajo) as tabla1 ON
                        g.legajo=tabla1.legajo
                        LEFT JOIN (SELECT h.legajo, MIN(h.desde) as fecha_ingreso FROM histocargo h GROUP BY h.legajo) as tabla2 ON
                        g.legajo=tabla2.legajo
                        LEFT JOIN lugar l ON
                        l.nrolugar=g.lugaractu
                        LEFT JOIN cargos c ON
                        g.cargoactu=c.nrocargo
                        LEFT JOIN titulo t ON
                        g.titulo=t.id
                        LEFT JOIN tipo_estudio te ON
                        t.codestud=te.codestudio
                        WHERE g.activa = 1 AND l.nrolugar = ".$fila->nrolugar." ".$str_filt_agentes." ORDER BY l.nrolugar, c.nrocargo";
                }   
            }
            else
            {
                $cabecera = "<thead><tr><th>Legajo</th><th>Apellido</th><th>Nombre</th><th>Cargo</th><th>T&iacute;tulo</th><th>&Uacute;ltimo ascenso</th><th>Ingreso</th></tr></thead>";
                $foot = "<tfoot><tr><th>Lugar</th><th>Apellido</th><th>Nombre</th><th>Cargo</th><th>T&iacute;tulo</th><th>&Uacute;ltimo ascenso</th><th>Ingreso</th></tr></tfoot>";
                $titulo =  "<h2>Listado por Organismo</h2>";
                $str = "SELECT g.legajo, g.apellido, g.nombre, c.cargo, te.titulo, DATE_FORMAT(tabla1.ultimo_ascenso, '%d-%m-%Y') AS xultimo_ascenso, DATE_FORMAT(tabla2.fecha_ingreso, '%d-%m-%Y') AS xfecha_ingreso
                    FROM general g 
                    LEFT JOIN (SELECT h.legajo, MAX( h.desde ) as ultimo_ascenso FROM histocargo h GROUP BY h.legajo) as tabla1 ON
                    g.legajo=tabla1.legajo
                    LEFT JOIN (SELECT h.legajo, MIN(h.desde) as fecha_ingreso FROM histocargo h GROUP BY h.legajo) as tabla2 ON
                    g.legajo=tabla2.legajo 
                    LEFT JOIN lugar l ON
                    l.nrolugar=g.lugaractu
                    LEFT JOIN cargos c ON
                    g.cargoactu=c.nrocargo
                    LEFT JOIN titulo t ON
                    g.titulo=t.id
                    LEFT JOIN tipo_estudio te ON
                    t.codestud=te.codestudio
                    WHERE g.activa=1 ".$str_filt_agentes." ORDER BY l.nrolugar, c.nrocargo";
            }
            
            break;
    case 2://Por cargo
            if($subcat!=0)
            {
                $str_sub = "SELECT * FROM cargos WHERE nrocargo=".$_GET["subcat"];
                $res = $conn->query($str_sub);
                $fila = $res->fetch_object(); 
            }
            $titulo = ($subcat ==0)?"<h2>Listado por Cargo</h2>":"<h2>Listado por Cargo | ".$fila->cargo."</h2>";

            $str=($subcat ==0)?"SELECT g.legajo, g.apellido, g.nombre, g.sexo, l.nombrelugar, DATE_FORMAT(tabla1.ultimo_ascenso, '%d-%m-%Y') AS xultimo_ascenso, DATE_FORMAT(tabla2.fecha_ingreso, '%d-%m-%Y') AS xfecha_ingreso  
            FROM general g 
            LEFT JOIN (SELECT h.legajo, MAX( h.desde ) as ultimo_ascenso FROM histocargo h GROUP BY h.legajo) as tabla1 ON
            g.legajo=tabla1.legajo
            LEFT JOIN (SELECT h.legajo, MIN(h.desde) as fecha_ingreso FROM histocargo h GROUP BY h.legajo) as tabla2 ON
            g.legajo=tabla2.legajo 
            RIGHT JOIN cargos c ON
            g.cargoactu=c.nrocargo
            LEFT JOIN lugar l ON
            g.lugaractu=l.nrolugar
            WHERE g.activa=1 ".$str_filt_agentes." ORDER BY c.nrocargo, g.legajo":"SELECT g.legajo, g.apellido, g.nombre, g.sexo, l.nombrelugar, DATE_FORMAT(tabla1.ultimo_ascenso, '%d-%m-%Y') AS xultimo_ascenso, DATE_FORMAT(tabla2.fecha_ingreso, '%d-%m-%Y') AS xfecha_ingreso  
            FROM general g 
            LEFT JOIN (SELECT h.legajo, MAX( h.desde ) as ultimo_ascenso FROM histocargo h GROUP BY h.legajo) as tabla1 ON
            g.legajo=tabla1.legajo
            LEFT JOIN (SELECT h.legajo, MIN(h.desde) as fecha_ingreso FROM histocargo h GROUP BY h.legajo) as tabla2 ON
            g.legajo=tabla2.legajo 
            RIGHT JOIN cargos c ON
            g.cargoactu=c.nrocargo
            LEFT JOIN lugar l ON
            g.lugaractu=l.nrolugar
            WHERE g.activa=1 AND c.nrocargo = ".$fila->nrocargo." ".$str_filt_agentes." ORDER BY c.nrocargo, g.legajo";
            $cabecera = "<thead><tr><th>Legajo</th><th>Apellido</th><th>Nombre</th><th>Sexo</th><th>Lugar</th><th>&Uacute;ltimo Ascenso</th><th>Ingreso</th></tr></thead>";
            $foot = "<tfoot><tr><th>Legajo</th><th>Apellido</th><th>Nombre</th><th>Sexo</th><th>Lugar</th><th>&Uacute;ltimo Ascenso</th><th>Ingreso</th></tr></tfoot>";
            break;
    case 3: //Por escalafon
            if($subcat!=0)
            {
                $str_sub = "SELECT * FROM cargos, escalafon WHERE cargos.escalafon=".$_GET["subcat"]." AND cargos.escalafon=escalafon.id";
                $res = $conn->query($str_sub);
                $fila = $res->fetch_object(); 
                $titulo = "<h2>Listado por escalaf&oacute;n | ".$fila->nombre."</h2>";
                $str = "SELECT g.legajo, g.apellido, g.nombre, g.sexo, c.cargo, l.nombrelugar FROM general g LEFT JOIN cargos c ON g.cargoactu=c.nrocargo LEFT JOIN lugar l ON g.lugaractu=l.nrolugar WHERE g.activa=1 AND c.escalafon=".$fila->escalafon." ".$str_filt_agentes." ORDER BY c.escalafon DESC, g.legajo ASC";
            }
            else
            {
                $titulo = "<h2>Listado por escalaf&oacute;n</h2>";
                $str = "SELECT g.legajo, g.apellido, g.nombre, g.sexo, c.cargo, l.nombrelugar FROM general g LEFT JOIN cargos c ON g.cargoactu=c.nrocargo LEFT JOIN lugar l ON g.lugaractu=l.nrolugar WHERE g.activa=1 ".$str_filt_agentes." ORDER BY c.escalafon DESC, g.legajo ASC";
            }

            $cabecera = "<thead><tr><th>Legajo</th><th>Apellido</th><th>Nombre</th><th>Sexo</th><th>Cargo</th><th>Lugar</th></tr></thead>";
            $foot = "<tfoot><tr><th>Legajo</th><th>Apellido</th><th>Nombre</th><th>Sexo</th><th>Cargo</th><th>Lugar</th></tr></tfoot>";
            break;
    case 7://Listado por titulo 
            if($subcat!=0)
            {
                $str_sub = "SELECT * FROM tipo_estudio WHERE codestudio=".$_GET["subcat"];
                $res = $conn->query($str_sub);
                $fila = $res->fetch_object(); 
            }
            $titulo = ($subcat ==0)?"<h2>Listado por t&iacute;tulo</h2>":"<h2>Listado por t&iacute;tulo | ".$fila->titulo."</h2>";
            $str=($subcat ==0)?"SELECT DISTINCT t.legajo, g.apellido, g.nombre, te.titulo, DATE_FORMAT(g.fecha_carga, '%d-%m-%Y') as fecha_carga, t.titulo as fecha_titulo, c.cargo FROM titulo t 
            INNER JOIN general g ON t.legajo=g.legajo
            LEFT JOIN tipo_estudio te ON t.codestud=te.codestudio
            LEFT JOIN cargos c ON g.cargoactu=c.nrocargo
            WHERE g.activa=1 ".$str_filt_agentes." ORDER BY te.titulo, g.legajo ASC":"SELECT DISTINCT t.legajo, g.apellido, g.nombre, te.titulo, DATE_FORMAT(g.fecha_carga, '%d-%m-%Y') as fecha_carga, t.titulo as fecha_titulo, c.cargo FROM titulo t 
            INNER JOIN general g ON (t.legajo=g.legajo) AND (t.codestud=".$fila->codestudio.") 
            LEFT JOIN tipo_estudio te ON (t.codestud=te.codestudio)
            LEFT JOIN cargos c ON g.cargoactu=c.nrocargo 
            WHERE g.activa=1 ".$str_filt_agentes." ORDER BY te.titulo, g.legajo ASC";
            $cabecera = "<thead><tr><th>Legajo</th><th>Apellido</th><th>Nombre</th><th>Titulo</th><th>Fecha ingreso</th><th>Fecha t&iacute;tulo</th><th>Cargo</th></tr></thead>";
            $foot = "<tfoot><tr><th>Legajo</th><th>Apellido</th><th>Nombre</th><th>Titulo</th><th>Fecha ingreso</th><th>Fecha t&iacute;tulo</th><th>Cargo</th></tr></tfoot>";
            break;
    case 8://Empleados abogados (solo empleados, este listado exceptua a magistrados, ministros, fiscales, etc)
            $titulo = "<h2>Listado de Empleados Abogados</h2>";
            $str="SELECT DISTINCT t.legajo, g.apellido, g.nombre, c.cargo, DATE_FORMAT(tabla1.ultimo_ascenso, '%d-%m-%Y') AS xultimo_ascenso, DATE_FORMAT(tabla2.fecha_ingreso, '%d-%m-%Y') AS xfecha_ingreso 
            FROM general g 
            RIGHT JOIN titulo t ON (g.legajo=t.legajo) AND (t.codestud=6) 
            LEFT JOIN cargos c ON
            g.cargoactu=c.nrocargo
            LEFT JOIN (SELECT h.legajo, MAX( h.desde ) as ultimo_ascenso FROM histocargo h GROUP BY h.legajo) as tabla1 ON
            g.legajo=tabla1.legajo
            LEFT JOIN (SELECT h.legajo, MIN(h.desde) as fecha_ingreso FROM histocargo h GROUP BY h.legajo) as tabla2 ON
            g.legajo=tabla2.legajo
            LEFT JOIN tipo_estudio te ON (t.codestud=te.codestudio) 
            WHERE g.activa=1 AND ((g.cargoactu BETWEEN 29 AND 31) OR (g.cargoactu BETWEEN 33 AND 54) OR (g.cargoactu BETWEEN 56 AND 59) OR g.cargoactu=65 OR g.cargoactu=66 OR (g.cargoactu BETWEEN 201 AND 218)) ".$str_filt_agentes." ORDER BY c.nrocargo, g.legajo ASC";

            $cabecera = "<thead><tr><th>Legajo</th><th>Apellido</th><th>Nombre</th><th>Cargo</th><th>&Uacute;ltimo Ascenso</th><th>Ingreso</th></tr></thead>";
            $foot = "<tfoot><tr><th>Legajo</th><th>Apellido</th><th>Nombre</th><th>Cargo</th><th>&Uacute;ltimo Ascenso</th><th>Ingreso</th></tr></tfoot>";
            break;
    case 9; default: 
            // Listado general 
            $titulo = "<h2>Listado General</h2>";
            if($fecha_desde != 0 && $fecha_hasta != 0 && setFecha($fecha_desde)<=setFecha($fecha_hasta))
            {
                $str="SELECT g.legajo, g.apellido, g.nombre, g.sexo, c.cargo, l.nombrelugar, DATE_FORMAT(tabla1.ultimo_ascenso, '%d-%m-%Y') AS xultimo_ascenso, DATE_FORMAT(tabla2.fecha_ingreso, '%d-%m-%Y') AS xfecha_ingreso, DATE_FORMAT(g.fecnac, '%d-%m-%Y'), g.mail_corporativo FROM general g LEFT JOIN (SELECT h.legajo, MAX( h.desde ) as ultimo_ascenso FROM histocargo h GROUP BY h.legajo) as tabla1 ON g.legajo=tabla1.legajo LEFT JOIN (SELECT h.legajo, MIN(h.desde) as fecha_ingreso FROM histocargo h GROUP BY h.legajo) as tabla2 ON g.legajo=tabla2.legajo LEFT JOIN cargos c ON g.cargoactu=c.nrocargo LEFT JOIN lugar l ON g.lugaractu=l.nrolugar WHERE g.activa=1 AND (g.fecha_carga BETWEEN '".setFecha($fecha_desde)."' AND '".setFecha($fecha_hasta)."') ".$str_filt_agentes." ORDER BY g.legajo ASC";
                $titulo = "<h2>Listado General | Ingresos desde ".$fecha_desde." hasta ".$fecha_hasta."</h2>";
            }
            else
            {
                $str="SELECT g.legajo, g.apellido, g.nombre, g.sexo, c.cargo, l.nombrelugar, DATE_FORMAT(tabla1.ultimo_ascenso, '%d-%m-%Y') AS xultimo_ascenso, DATE_FORMAT(tabla2.fecha_ingreso, '%d-%m-%Y') AS xfecha_ingreso, DATE_FORMAT(g.fecnac, '%d-%m-%Y'), g.mail_corporativo FROM general g LEFT JOIN (SELECT h.legajo, MAX( h.desde ) as ultimo_ascenso FROM histocargo h GROUP BY h.legajo) as tabla1 ON g.legajo=tabla1.legajo LEFT JOIN (SELECT h.legajo, MIN(h.desde) as fecha_ingreso FROM histocargo h GROUP BY h.legajo) as tabla2 ON g.legajo=tabla2.legajo LEFT JOIN cargos c ON g.cargoactu=c.nrocargo LEFT JOIN lugar l ON g.lugaractu=l.nrolugar WHERE g.activa=1 ".$str_filt_agentes." ORDER BY g.legajo ASC";
            }
            //echo $str;
            $cabecera = "<thead><tr><th>Legajo</th><th>Apellido</th><th>Nombre</th><th>Sexo</th><th>Cargo</th><th>Lugar</th><th>Ultimo Ascenso</th><th>Ingreso</th><th>Fecha nac.</th><th>Mail corp.</th></tr></thead>";
            $foot = "<tfoot><tr><th>Legajo</th><th>Apellido</th><th>Nombre</th><th>Sexo</th><th>Cargo</th><th>Lugar</th><th>Ultimo Ascenso</th><th>Ingreso</th><th>Fecha nac.</th><th>Mail corp.</th></tr></tfoot>";
            break;
    case 10: 
            // Listado de bajas
            $titulo = "<h2>Listado de Bajas</h2>";
            if($fecha_desde != 0 && $fecha_hasta != 0 && setFecha($fecha_desde)<=setFecha($fecha_hasta))
            {
                $str="SELECT g.legajo, g.apellido, g.nombre, DATE_FORMAT(g.fecha_carga, '%d-%m-%Y'), DATE_FORMAT(b.fecha, '%d-%m-%Y'), b.acordada, g.domic, g.telef FROM general g RIGHT JOIN bajas b ON g.legajo=b.legajo WHERE g.activa=0 AND (b.fecha BETWEEN '".setFecha($fecha_desde)."' AND '".setFecha($fecha_hasta)."') ".$str_filt_agentes." ORDER BY g.legajo ASC";    
                $titulo = "<h2>Listado de Bajas | Bajas desde ".$fecha_desde." hasta ".$fecha_hasta."</h2>";
            }
            else
            {
                $str="SELECT g.legajo, g.apellido, g.nombre, DATE_FORMAT(g.fecha_carga, '%d-%m-%Y'), DATE_FORMAT(b.fecha, '%d-%m-%Y'), b.acordada, g.domic, g.telef FROM general g RIGHT JOIN bajas b ON g.legajo=b.legajo WHERE g.activa=0 ".$str_filt_agentes." ORDER BY g.legajo ASC";
            }            
            //echo $str;
            $cabecera = "<thead><tr><th>Legajo</th><th>Apellido</th><th>Nombre</th><th>Fecha Ingreso</th><th>Fecha baja</th><th>Acordada</th><th>Domicilio</th><th>Tel&eacute;fono</th></tr></thead>";
            $foot = "<tfoot><tr><th>Legajo</th><th>Apellido</th><th>Nombre</th><th>Fecha Ingreso</th><th>Fecha baja</th><th>Acordada</th><th>Domicilio</th><th>Tel&eacute;fono</th></tr></tfoot>";
            break;
    case 11: 
            // Empleados con compensatorias
            if($subcat!=0)
            {
                $str_sub = "SELECT * FROM tipo_estudio WHERE codestudio=".$_GET["subcat"];
                $res = $conn->query($str_sub);
                $fila = $res->fetch_object(); 
            }

            switch($subcat)
            {
                case 0:
                case 1:
                    $titulo = "<h2>Empleados con licencias compensatorias | Totales disponibles por empleado</h2>";
                    $str = "comp1"; //Una cadena que funciona como bandera, dado que despues se analizara diferente.
                    $cabecera = "<thead><tr><th>Legajo</th><th>Apellido</th><th>Nombre</th><th>Cargo</th><th>Total dias</th><th>Disponibles</th><th>Lugar</th><th>Fuero</th></tr></thead>";
                    $foot = "<tfoot><tr><th>Legajo</th><th>Apellido</th><th>Nombre</th><th>Cargo</th><th>Total dias</th><th>Disponibles</th><th>Lugar</th><th>Fuero</th></tr></tfoot>";
                    break;
                case 2:
                    $titulo = "<h2>Empleados con licencias compensatorias | Disponibles por empleado por a&ntilde;o</h2>";
                    $str = "comp2"; //Una cadena que funciona como bandera, dado que despues se analizara diferente.
                    $cabecera = "<thead><tr><th>Legajo</th><th>Apellido</th><th>Nombre</th><th>Cargo</th><th>Desde</th><th>Hasta</th><th>Total dias</th><th>Disponibles</th><th>Lugar</th><th>Fuero</th></tr></thead>";
                    $foot = "<tfoot><tr><th>Legajo</th><th>Apellido</th><th>Nombre</th><th>Cargo</th><th>Desde</th><th>Hasta</th><th>Total dias</th><th>Disponibles</th><th>Lugar</th><th>Fuero</th></tr></tfoot>";
                    break;
            }
            break;
    case 12: 
            // Ultimos ingresantes
            switch($subcat)
                {
                    //Ultimo mes (por defecto si no selecciona subcategoria
                    case 0:
                    //Ultimo mes    
                    case 1:
                        $mesUlt = 1;
                        $titulo = "<h2>Ingresados el &uacute;ltimo mes</h2>";
                        break;
                    //Ultimos 6 meses
                    case 2:
                        $mesUlt = 6;
                        $titulo = "<h2>Ingresados los &uacute;ltimos 6 meses</h2>";
                        break;
                    //Ultimo año    
                    case 3:
                        $mesUlt = 12;
                        $titulo = "<h2>Ingresados el &uacute;ltimo a&ntilde;o</h2>";
                        break;
                }
            $str="SELECT g.legajo, g.apellido, g.nombre, g.padron, c.cargo, l.nombrelugar, DATE_FORMAT(tabla2.fecha_ingreso, '%d-%m-%Y') AS xfecha_ingreso  
            FROM general g 
            LEFT JOIN (SELECRT h.legajo FROM histocargo h GROUP BY h.legajo) as tabla1 ON
            g.legajo=tabla1.legajo
            LEFT JOIN (SELECT h.legajo, MIN(h.desde) as fecha_ingreso FROM histocargo h GROUP BY h.legajo) as tabla2 ON
            g.legajo=tabla2.legajo 
            LEFT JOIN cargos c ON
            g.cargoactu=c.nrocargo
            LEFT JOIN lugar l ON
            g.lugaractu=l.nrolugar
            WHERE g.activa=1 AND fecha_ingreso>= date_sub(curdate(), interval ".$mesUlt." month) ".$str_filt_agentes." ORDER BY g.apellido ASC";
            $cabecera = "<thead><tr><th>Legajo</th><th>Apellido</th><th>Nombre</th><th>Padron</th><th>Cargo</th><th>Lugar</th><th>Ingreso</th></tr></thead>";
            $foot = "<tfoot><tr><th>Legajo</th><th>Apellido</th><th>Nombre</th><th>Padron</th><th>Cargo</th><th>Lugar</th><th>Ingreso</th></tr></tfoot>";
            break;
    case 13: 
            // Listado por fueros
            if($subcat!=0)
            {
                $str_sub = "SELECT * FROM fuero WHERE id=".$_GET["subcat"];
                $res = $conn->query($str_sub);
                $fila = $res->fetch_object(); 
            }
            $titulo = ($subcat ==0)?"<h2>Listado por Fuero</h2>":"<h2>Listado por Fuero | ".$fila->nombre."</h2>";
            $str = ($subcat ==0)?"SELECT g.legajo, g.apellido, g.nombre, g.nrodoc, l.nombrelugar, c.cargo, te.titulo, DATE_FORMAT(tabla1.ultimo_ascenso, '%d-%m-%Y') AS xultimo_ascenso, DATE_FORMAT(tabla2.fecha_ingreso, '%d-%m-%Y') AS xfecha_ingreso
            FROM general g 
            LEFT JOIN (SELECT h.legajo, MAX( h.desde ) as ultimo_ascenso FROM histocargo h GROUP BY h.legajo) as tabla1 ON
            g.legajo=tabla1.legajo
            LEFT JOIN (SELECT h.legajo, MIN(h.desde) as fecha_ingreso FROM histocargo h GROUP BY h.legajo) as tabla2 ON
            g.legajo=tabla2.legajo 
            LEFT JOIN lugar l ON
            l.nrolugar=g.lugaractu
            LEFT JOIN fuero f ON
            l.nro_fuero=f.id
            LEFT JOIN cargos c ON
            g.cargoactu=c.nrocargo
            LEFT JOIN titulo t ON
            g.titulo=t.id
            LEFT JOIN tipo_estudio te ON
            t.codestud=te.codestudio
            WHERE g.activa=1 ".$str_filt_agentes." ORDER BY f.id, l.nrolugar, c.nrocargo":"SELECT g.legajo, g.apellido, g.nombre, g.nrodoc, l.nombrelugar, c.cargo, te.titulo, DATE_FORMAT(tabla1.ultimo_ascenso, '%d-%m-%Y') AS xultimo_ascenso, DATE_FORMAT(tabla2.fecha_ingreso, '%d-%m-%Y') AS xfecha_ingreso 
            FROM general g
            LEFT JOIN (SELECT h.legajo, MAX( h.desde ) as ultimo_ascenso FROM histocargo h GROUP BY h.legajo) as tabla1 ON
            g.legajo=tabla1.legajo
            LEFT JOIN (SELECT h.legajo, MIN(h.desde) as fecha_ingreso FROM histocargo h GROUP BY h.legajo) as tabla2 ON
            g.legajo=tabla2.legajo
            LEFT JOIN lugar l ON
            l.nrolugar=g.lugaractu
            LEFT JOIN fuero f ON
            l.nro_fuero=f.id
            LEFT JOIN cargos c ON
            g.cargoactu=c.nrocargo
            LEFT JOIN titulo t ON
            g.titulo=t.id
            LEFT JOIN tipo_estudio te ON
            t.codestud=te.codestudio
            WHERE g.activa = 1 AND f.id = ".$fila->id." ".$str_filt_agentes." ORDER BY l.nrolugar, c.nrocargo";
            $cabecera = "<thead><tr><th>Legajo</th><th>Apellido</th><th>Nombre</th><th>DNI</th><th>Lugar</th><th>Cargo</th><th>T&iacute;tulo</th><th>&Uacute;ltimo ascenso</th><th>Ingreso</th></tr></thead>";
            $foot = "<tfoot><tr><th>Lugar</th><th>Apellido</th><th>Nombre</th><th>DNI</th><th>Lugar</th><th>Cargo</th><th>T&iacute;tulo</th><th>&Uacute;ltimo ascenso</th><th>Ingreso</th></tr></tfoot>";
            break;  
    case 14: 
            // Listado de evaluacion de desempeño

            if($subcat!=0)
            {
                $str_sub = "SELECT * FROM eval_evaluacion WHERE id=".$_GET["subcat"];
                $res = $conn->query($str_sub);
                $fila = $res->fetch_object(); 
            }
            $titulo = ($subcat ==0)?"<h2>Listado de evaluaciones de desempe&ntilde;o</h2>":"<h2>Listado de evaluaciones de desempe&ntilde;o | ".$fila->nombre."</h2>";
            $str = "evaluacion"; //Una cadena que funciona como bandera, dado que despues se analizara diferente.
            $cabecera = "<thead><tr><th>Estado</th><th>Evaluador</th><th>Evaluado</th><th>Lugar</th><th>Cargo</th><th>Evaluacion</th><th>Fecha inicio</th><th>Fecha fin</th><th>Promedio</th><th>Aplazos</th><th>Tardanzas</th><th>Licencias</th><th>Conducta</th><th>Capacitaciones</th><th>Asistencia y puntualidad</th><th>Antiguedad categoria</th><th>Antiguedad escalafon</th><th>Puntaje final</th><th>Ver evaluaci&oacute;n</th></tr></thead>";
            $foot = "<tfoot><tr><th>Estado</th><th>Evaluador</th><th>Evaluado</th><th>Lugar</th><th>Cargo</th><th>Evaluacion</th><th>Fecha inicio</th><th>Fecha fin</th><th>Promedio</th><th>Aplazos</th><th>Tardanzas</th><th>Licencias</th><th>Conducta</th><th>Capacitaciones</th><th>Asistencia y puntualidad</th><th>Antiguedad categoria</th><th>Antiguedad escalafon</th><th>Puntaje final</th><th>Ver evaluaci&oacute;n</th></tr></tfoot>";
            break;   
    case 15: //Licencias
            $fecha_filt = "";
            if($fecha_desde != 0 && $fecha_hasta != 0 && setFecha($fecha_desde)<=setFecha($fecha_hasta))
            {
                $fecha_filt=" | Per&iacute;odo: ".$fecha_desde." al ".$fecha_hasta;
            }
            if($fecha_desde3 != 0 && $fecha_hasta3 != 0 && setFecha($fecha_desde3)<=setFecha($fecha_hasta3))
            {
                $fecha_filt.=" | Carga: ".$fecha_desde3." al ".$fecha_hasta3;
            }
            $titulo = "<h2>Listado de licencias".$fecha_filt."</h2>";
            
            if($subcat!="")
            {
                $str_tipo_lic = "SELECT * FROM tipo_licencias WHERE id=".$subcat;
                $cons_tipo_lic = $conn->query($str_tipo_lic);
                $obj_tipo_lic = $cons_tipo_lic->fetch_object();
                $titulo = "<h2>Listado de licencias | ".$obj_tipo_lic->nombre."</h2>";
                if(($fecha_desde != 0 && $fecha_hasta != 0 && setFecha($fecha_desde)<=setFecha($fecha_hasta)) || ($fecha_desde3 != 0 && $fecha_hasta3 != 0 && setFecha($fecha_desde3)<=setFecha($fecha_hasta3)))
                {
                    $titulo = "<h2>Listado de licencias | ".$obj_tipo_lic->nombre.$fecha_filt."</h2>";
                }
            }
            
            $str = "licencias"; //Una cadena que funciona como bandera, dado que despues se analizara diferente.
            $cabecera = "<thead><tr><th>Licencia</th><th>Legajo</th><th>Apellido</th><th>Nombre</th><th>Lugar</th><th>Desde</th><th>Hasta</th><th>Causa</th><th>Observaciones</th><th>Desiste</th><th>Pierde presentismo</th><th>Expediente</th><th>Fecha carga</th><th>Notif. mail</th></tr></thead>";
            $foot = "<tfoot><tr><th>Licencia</th><th>Legajo</th><th>Apellido</th><th>Nombre</th><th>Lugar</th><th>Desde</th><th>Hasta</th><th>Causa</th><th>Observaciones</th><th>Desiste</th><th>Pierde presentismo</th><th>Expediente</th><th>Fecha carga</th><th>Notif. mail</th></tr></tfoot>";
            break;  
    case 16: 
            // Liquidaciones
            $filtro_fecha = "";
            if($fecha_desde!=0 && $fecha_hasta!=0 && $fecha_desde<=$fecha_hasta)
            {
                $filtro_fecha.=" AND (fecha BETWEEN '".setFecha($fecha_desde)."' AND '".setFecha($fecha_hasta)."')";
            }
            if($subcat!=0)
            {
                if($subsubcat!=0)
                {
                    switch($combo_liquidacion)
                    {
                        case 1: default: //Haberes
                            $str_cons_liq = "SELECT cont_archivo_bsj.*, cont_liquidacion.nombre as mes_liq FROM cont_archivo_bsj, cont_liquidacion WHERE cont_archivo_bsj.liquidacion=cont_liquidacion.id AND cont_archivo_bsj.periodo='".$subcat."' AND cont_archivo_bsj.liquidacion=".$subsubcat." AND cont_archivo_bsj.tipo_acreditacion='02'".$filtro_fecha;
                            break;
                        case 2: //cuota alimentaria
                            $str_cons_liq = "SELECT cont_archivo_bsj.*, cont_liquidacion.nombre as mes_liq FROM cont_archivo_bsj, cont_liquidacion WHERE cont_archivo_bsj.liquidacion=cont_liquidacion.id AND cont_archivo_bsj.periodo='".$subcat."' AND cont_archivo_bsj.liquidacion=".$subsubcat." AND cont_archivo_bsj.tipo_acreditacion='11'".$filtro_fecha;
                            break;
                    }
                }
                else //si no selecciona una subsubcategoria, muestro por defecto la ultima liquidacion de haberes del año seleccionado (omito aguinaldos y ayudas extraordinarias)
                {
                    $str_ult_liq = "SELECT MAX(liquidacion) as ult_liq FROM (SELECT liquidacion FROM cont_archivo_bsj WHERE periodo='".$subcat."' AND liquidacion!=7 AND liquidacion!=13 AND liquidacion<15) AS tabla1";
                    $res_ult_liq = $conn->query($str_ult_liq);
                    $obj_ult_liq = $res_ult_liq->fetch_object();
                    $subsubcat = $obj_ult_liq->ult_liq;
                    $str_cons_liq = "SELECT cont_archivo_bsj.*, cont_liquidacion.nombre as mes_liq FROM cont_archivo_bsj, cont_liquidacion WHERE cont_archivo_bsj.liquidacion=cont_liquidacion.id AND cont_archivo_bsj.periodo='".$subcat."' AND cont_archivo_bsj.liquidacion=".$subsubcat." AND cont_archivo_bsj.tipo_acreditacion='02'".$filtro_fecha;
                }
            }
            else //si no selecciona subcategoria, muestro por defecto ultima liquidacion de haberes del ultimo año (omito aguinaldos y ayudas extraordinarias)
            {
                $str_ult_per = "SELECT MAX(periodo) as ult_per FROM cont_archivo_bsj";
                $res_ult_per = $conn->query($str_ult_per);
                $obj_ult_per = $res_ult_per->fetch_object();
                $subcat = $obj_ult_per->ult_per;

                $str_ult_liq = "SELECT MAX(liquidacion) as ult_liq FROM (SELECT liquidacion FROM cont_archivo_bsj WHERE periodo='".$subcat."' AND liquidacion!=7 AND liquidacion!=13 AND liquidacion<15) AS tabla1";
                $res_ult_liq = $conn->query($str_ult_liq);
                $obj_ult_liq = $res_ult_liq->fetch_object();
                $subsubcat = $obj_ult_liq->ult_liq;
                $str_cons_liq = "SELECT cont_archivo_bsj.*, cont_liquidacion.nombre as mes_liq FROM cont_archivo_bsj, cont_liquidacion WHERE cont_archivo_bsj.liquidacion=cont_liquidacion.id AND cont_archivo_bsj.periodo='".$subcat."' AND cont_archivo_bsj.liquidacion=".$subsubcat." AND cont_archivo_bsj.tipo_acreditacion='02'".$filtro_fecha;
            }

            $res_cons_liq = $conn->query($str_cons_liq);
            //echo $str_cons_liq;
            $sum_neto = 0;
            $mes_liq="";
            $per_liq="";
            while($obj = $res_cons_liq->fetch_object())
            {
                $neto = floatval($obj->neto)/100;
                $sum_neto+=$neto;
                $mes_liq = $obj->mes_liq;
                $per_liq = $obj->periodo;
            }
            $sum_neto = "$".number_format($sum_neto,2,",",".");
            $titulo = "<h2>Listado de acreditaciones autom&aacute;ticas de sueldo | Mes de liquidaci&oacute;n: ".$mes_liq."</h2>";
            $subtitulo = "<h3>Per&iacute;odo: ".$per_liq." | Neto total: ".$sum_neto."</h3>";
            switch($combo_liquidacion)
            {
                case 1: default: //Haberes
                    $cabecera = "<thead><tr><th>Cuenta</th><th>Neto</th><th>Nombre</th><th>Legajo</th><th>Padron</th><th>Documento</th><th>Escalafon</th><th>Recibo</th></tr></thead>";
                    $foot = "<tfoot><tr><th>Cuenta</th><th>Neto</th><th>Nombre</th><th>Legajo</th><th>Padron</th><th>Documento</th><th>Escalafon</th><th>Recibo</th></tr></tfoot>";
                    break;
                case 2:  //cuota alimentaria
                    $cod_emp = "273";
                    $tipo_acred = "11";
                    $str_liq_alim = "SELECT cuenta_acreditacion, neto, nombre, documento FROM cont_archivo_bsj WHERE periodo='".$subcat."' AND liquidacion=".$subsubcat." AND tipo_acreditacion=".$tipo_acred." AND codigo_empresa=".$cod_emp." ORDER BY nombre ASC";
                    $res_liq_alim = $conn->query($str_liq_alim);
                    while($obj_alim = $res_liq_alim->fetch_object())
                    {
                        $neto = floatval($obj_alim->neto)/100;
                        $sum_neto+=$neto;
                    }
                    $sum_neto = "$".number_format($sum_neto,2,",",".");

                    $cabecera = "<thead><tr><th>Cuenta</th><th>Neto</th><th>Nombre beneficiario</th><th>Documento</th></tr></thead>";
                    $foot = "<tfoot><tr><th>Cuenta</th><th>Neto</th><th>Nombre beneficiario</th><th>Documento</th></tr></tfoot>";
                    $titulo = "<h2>Listado de cuotas alimentarias | Mes de liquidaci&oacute;n: ".$mes_liq."</h2>";
                    $subtitulo = "<h3>Per&iacute;odo: ".$per_liq." | Neto total: ".$sum_neto."</h3>";
                    break;
                case 3: //embargos
                    $tipo_acred = "xxx";
                    break;
                /*default:
                    $tipo_acred = "02";
                    break;*/
            }
            $neto = '<div id="neto_total" style="display:none">'.$sum_neto.'</div>';
            $periodo = '<div id="periodo_liq" style="display:none">'.$per_liq.'</div>';
            $str = "liquidacion"; //Una cadena que funciona como bandera, dado que despues se analizara diferente.
            break;    
    case 17: 
            // Listado de factores de ascenso

            /*if($subcat!=0)
            {
                $str_sub = "SELECT * FROM eval_evaluacion WHERE id=".$_GET["subcat"];
                $res = $conn->query($str_sub);
                $fila = $res->fetch_object(); 
            }*/
            $titulo = "<h2>Listado de factores de ascenso</h2>";
            $str = "factores_ascenso"; //Una cadena que funciona como bandera, dado que despues se analizara diferente.
            $cabecera = "<thead><tr><th>Legajo</th><th>Apellido</th><th>Nombre</th><th>Lugar</th><th>Cargo</th><th>Escalaf&oacute;n</th><th>Titulo</th><th>Cant. tardanzas</th><th>Cant. licencias</th><th>Conducta</th><th>Capacitaci&oacute;n</th><th>Asist. y puntualidad</th><th>Antiguedad cargo</th><th>Puntaje cargo</th><th>Antiguedad escalaf&oacute;n</th><th>Puntaje escalaf&oacute;n</th><th>Puntaje evaluaci&oacute;n</th><th>Puntaje total</th><th>Ingreso</th><th>Ult. asc.</th></tr></thead>";
            $foot = "<tfoot><tr><th>Legajo</th><th>Apellido</th><th>Nombre</th><th>Lugar</th><th>Cargo</th><th>Escalaf&oacute;n</th><th>Titulo</th><th>Cant. tardanzas</th><th>Cant. licencias</th><th>Conducta</th><th>Capacitaci&oacute;n</th><th>Asist. y puntualidad</th><th>Antiguedad cargo</th><th>Puntaje cargo</th><th>Antiguedad escalaf&oacute;n</th><th>Puntaje escalaf&oacute;n</th><th>Puntaje evaluaci&oacute;n</th><th>Puntaje total</th><th>Ingreso</th><th>Ult. asc.</th></tr></tfoot>";
            break;
    case 18: 
            // Listado final concurso ascensos

            $titulo = "<h2>Listado final concurso ascensos</h2>";
            $str = "listado_final"; //Una cadena que funciona como bandera, dado que despues se analizara diferente.
            $cabecera = "<thead><tr><th>Apellido</th><th>Nombre</th><th>Cargo</th><th>Lugar</th><th>Evaluacion desempe&ntilde;o</th><th>Conducta</th><th>Capacitaci&oacute;n</th><th>Asist. y puntualidad</th><th>Antiguedad cargo</th><th>Cant. a&ntilde;os cargo</th><th>Antiguedad escalaf&oacute;n</th><th>Puntaje final</th></tr></thead>";
            $foot = "<tfoot><tr><th>Apellido</th><th>Nombre</th><th>Cargo</th><th>Lugar</th><th>Evaluacion desempe&ntilde;o</th><th>Conducta</th><th>Capacitaci&oacute;n</th><th>Asist. y puntualidad</th><th>Antiguedad cargo</th><th>Cant. a&ntilde;os cargo</th><th>Antiguedad escalaf&oacute;n</th><th>Puntaje final</th></tr></tfoot>";
            break;
    case 19: 
            // Listado de certificados
            $nacidos_entre="";
            if($fecha_desde != 0 && $fecha_hasta != 0 && setFecha($fecha_desde)<=setFecha($fecha_hasta))
            {
                $nacidos_entre.="nacidos entre ".$fecha_desde." y ".$fecha_hasta;
            }
            if($fecha_desde2 != 0 && $fecha_hasta2 != 0 && setFecha($fecha_desde2)<=setFecha($fecha_hasta2))
            {
                if($subcat==0)
                {
                    $titulo = "<h2>Listado de certificados ".$nacidos_entre." | Cumplen mayor&iacute;a de edad entre ".$fecha_desde2." y ".$fecha_hasta2."</h2>";
                }
                else
                {
                    $titulo = "<h2>Listado de certificados ".$subcat." ".$nacidos_entre." | Cumplen mayor&iacute;a de edad entre ".$fecha_desde2." y ".$fecha_hasta2."</h2>";
                }
            }
            else
            {
                if($subcat==0)
                {
                    $titulo = "<h2>Listado de certificados ".$nacidos_entre."</h2>";
                }
                else
                {
                    $titulo = "<h2>Listado de certificados ".$subcat." ".$nacidos_entre."</h2>";    
                }
            }
            $str = "listado_hijos"; //Una cadena que funciona como bandera, dado que despues se analizara diferente.
            $cabecera = "<thead><tr><th>Apellido</th><th>Nombre</th><th>Legajo</th><th>Familiar</th><th>DNI</th><th>Estudios</th><th>Discapacitado</th><th>Certificado inicio clases</th><th>Certificado fin de clases</th><th>Guardapolvo</th><th>A&ntilde;o</th><th>Suspendido</th></tr></thead>";
            $foot = "<tfoot><tr><th>Apellido</th><th>Nombre</th><th>Legajo</th><th>Familiar</th><th>DNI</th><th>Estudios</th><th>Discapacitado</th><th>Certificado inicio clases</th><th>Certificado fin de clases</th><th>Guardapolvo</th>     <th>A&ntilde;o</th><th>Suspendido</th></tr></tfoot>";
            break;
    case 20: 
            // Recibos de sueldo
            if($subcat!=0)
            {
                if($subsubcat!=0)
                {
                    $str_rec = "SELECT general.nombre, general.apellido, general.legajo, general.padron, cont_ci_raw.anio_liq, cont_ci_raw.mes_liq, cont_liquidacion_ci.nombre as mes_liq_nombre FROM general LEFT JOIN cont_ci_raw ON general.padron_ci=cont_ci_raw.padron LEFT JOIN cont_liquidacion_ci ON cont_ci_raw.mes_liq=cont_liquidacion_ci.id WHERE cont_ci_raw.anio_liq=".$subcat." AND cont_ci_raw.mes_liq=".$subsubcat." GROUP BY general.legajo";
                }
                else //si no selecciona una subsubcategoria, muestro todos los recibos del año seleccionado
                {
                    $str_rec = "SELECT general.nombre, general.apellido, general.legajo, general.padron, cont_ci_raw.anio_liq, cont_ci_raw.mes_liq, cont_liquidacion_ci.nombre as mes_liq_nombre FROM general LEFT JOIN cont_ci_raw ON general.padron_ci=cont_ci_raw.padron LEFT JOIN cont_liquidacion_ci ON cont_ci_raw.mes_liq=cont_liquidacion_ci.id WHERE cont_ci_raw.anio_liq=".$subcat." GROUP BY general.legajo ORDER BY cont_ci_raw.anio_liq DESC";
                }
                /*else //si no selecciona una subsubcategoria, muestro por defecto la ultima liquidacion de haberes del año seleccionado
                {
                    $str_ult_liq = "SELECT MAX(mes_liq) as ult_liq FROM (SELECT mes_liq FROM cont_ci_raw WHERE anio_liq='".$subcat."') AS tabla1";
                    $res_ult_liq = $conn->query($str_ult_liq);
                    $obj_ult_liq = $res_ult_liq->fetch_object();
                    $subsubcat = $obj_ult_liq->ult_liq;
                    $str_rec = "SELECT general.nombre, general.apellido, general.legajo, general.padron, cont_ci_raw.anio_liq, cont_ci_raw.mes_liq, cont_liquidacion_ci.nombre as mes_liq_nombre FROM general LEFT JOIN cont_ci_raw ON general.padron_ci=cont_ci_raw.padron LEFT JOIN cont_liquidacion_ci ON cont_ci_raw.mes_liq=cont_liquidacion_ci.id WHERE cont_ci_raw.anio_liq=".$subcat." AND cont_ci_raw.mes_liq=".$subsubcat." GROUP BY general.legajo";
                }*/
            }
            else //si no selecciona subcategoria, muestro por defecto ultima liquidacion de haberes del ultimo año
            {
                $str_ult_per = "SELECT MAX(anio_liq) as ult_per FROM cont_ci_raw";
                $res_ult_per = $conn->query($str_ult_per);
                $obj_ult_per = $res_ult_per->fetch_object();
                $subcat = $obj_ult_per->ult_per;

                $str_ult_liq = "SELECT MAX(mes_liq) as ult_liq FROM (SELECT mes_liq FROM cont_ci_raw WHERE anio_liq='".$subcat."') AS tabla1";
                $res_ult_liq = $conn->query($str_ult_liq);
                $obj_ult_liq = $res_ult_liq->fetch_object();
                $subsubcat = $obj_ult_liq->ult_liq;
                $str_rec = "SELECT general.nombre, general.apellido, general.legajo, general.padron, cont_ci_raw.anio_liq, cont_ci_raw.mes_liq, cont_liquidacion_ci.nombre as mes_liq_nombre FROM general LEFT JOIN cont_ci_raw ON general.padron_ci=cont_ci_raw.padron LEFT JOIN cont_liquidacion_ci ON cont_ci_raw.mes_liq=cont_liquidacion_ci.id WHERE cont_ci_raw.anio_liq=".$subcat." AND cont_ci_raw.mes_liq=".$subsubcat." GROUP BY general.legajo";
            }

            $res_rec = $conn->query($str_rec);
            //echo $str_cons_liq;
            $sum_neto = 0;
            $mes_liq="";
            $per_liq="";
            while($obj = $res_rec->fetch_object())
            {
                $mes_liq = $obj->mes_liq_nombre;
                $per_liq = $obj->anio_liq;
                if(strlen(trim($per_liq))==1)
                {
                    $per_liq = "0".$per_liq;
                }
            }
            $sum_neto = "$".number_format($sum_neto,2,",",".");
            $titulo = "<h2>Listado de liquidaciones | ".$mes_liq." 20".$per_liq."</h2>";
            $subtitulo = "<h3>Per&iacute;odo: ".$per_liq."</h3>";
            $cabecera = "<thead><tr><th>Apellido</th><th>Nombre</th><th>Legajo</th><th>Padron</th><th>Periodo liq.</th><th>Mes liq.</th><th>Recibo</th></tr></thead>";
            $foot = "<tfoot><tr><th>Apellido</th><th>Nombre</th><th>Legajo</th><th>Padron</th><th>Periodo liq.</th><th>Mes liq.</th><th>Recibo</th></tr></tfoot>";
            
            /*$neto = '<div id="neto_total" style="display:none">'.$sum_neto.'</div>';
            $periodo = '<div id="periodo_liq" style="display:none">'.$per_liq.'</div>';*/
            $str = "recibo_sueldo"; //Una cadena que funciona como bandera, dado que despues se analizara diferente.
            break;        
    case 21: //Listado general sin adscriptos
            $titulo = "<h2>Listado General Planta</h2>";
            if($fecha_desde != 0 && $fecha_hasta != 0 && setFecha($fecha_desde)<=setFecha($fecha_hasta))
            {
                $str="SELECT g.legajo, g.apellido, g.nombre, g.padron, g.nrodoc, g.sexo, c.cargo, esc.nombre as escalafon, l.nombrelugar, DATE_FORMAT(tabla1.ultimo_ascenso, '%d-%m-%Y') AS xultimo_ascenso, DATE_FORMAT(tabla2.fecha_ingreso, '%d-%m-%Y') AS xfecha_ingreso, DATE_FORMAT(g.fecnac, '%d-%m-%Y'), g.antiguedad, TIMESTAMPDIFF(YEAR, g.fecnac, CURDATE()) AS edad, g.mail_corporativo FROM general g LEFT JOIN (SELECT h.legajo, MAX( h.desde ) as ultimo_ascenso FROM histocargo h GROUP BY h.legajo) as tabla1 ON g.legajo=tabla1.legajo LEFT JOIN (SELECT h.legajo, MIN(h.desde) as fecha_ingreso FROM histocargo h GROUP BY h.legajo) as tabla2 ON g.legajo=tabla2.legajo LEFT JOIN cargos c ON g.cargoactu=c.nrocargo LEFT JOIN escalafon esc ON c.escalafon=esc.id LEFT JOIN lugar l ON g.lugaractu=l.nrolugar WHERE g.activa=1 AND g.cargoactu!=55 AND (g.fecha_carga BETWEEN '".setFecha($fecha_desde)."' AND '".setFecha($fecha_hasta)."') ".$str_filt_agentes." ORDER BY g.legajo ASC";
                $titulo = "<h2>Listado General Planta | Ingresos desde ".$fecha_desde." hasta ".$fecha_hasta."</h2>";
            }
            else
            {
                if($fecha_hasta!=0 && $fecha_desde==0) //Si solo tiene fecha hasta, saco una foto de la planta a esa fecha
                {
                    $str="SELECT g.legajo, g.apellido, g.nombre, g.padron, g.nrodoc, g.sexo, c.cargo, esc.nombre as escalafon, l.nombrelugar, DATE_FORMAT(tabla1.ultimo_ascenso, '%d-%m-%Y') AS xultimo_ascenso, DATE_FORMAT(tabla2.fecha_ingreso, '%d-%m-%Y') AS xfecha_ingreso, DATE_FORMAT(g.fecnac, '%d-%m-%Y'), g.antiguedad, TIMESTAMPDIFF(YEAR, g.fecnac, CURDATE()) AS edad, g.mail_corporativo FROM general g LEFT JOIN (SELECT h.legajo, MAX( h.desde ) as ultimo_ascenso FROM histocargo h GROUP BY h.legajo) as tabla1 ON g.legajo=tabla1.legajo LEFT JOIN (SELECT h.legajo, MIN(h.desde) as fecha_ingreso FROM histocargo h GROUP BY h.legajo) as tabla2 ON g.legajo=tabla2.legajo LEFT JOIN cargos c ON g.cargoactu=c.nrocargo LEFT JOIN escalafon esc ON c.escalafon=esc.id LEFT JOIN lugar l ON g.lugaractu=l.nrolugar LEFT JOIN bajas b ON g.legajo=b.legajo WHERE g.cargoactu!=55 AND ((b.id IS NOT NULL AND fecha IS NOT NULL AND '".setFecha($fecha_hasta)."'>= g.fecha_carga AND '".setFecha($fecha_hasta)."'< b.fecha) OR b.id IS NULL AND '".setFecha($fecha_hasta)."'>=g.fecha_carga) ".$str_filt_agentes." ORDER BY g.legajo ASC";
                    $titulo = "<h2>Listado General Planta | Al ".$fecha_hasta."</h2>";
                }
                else
                {
                    $str="SELECT g.legajo, g.apellido, g.nombre, g.padron, g.nrodoc, g.sexo, c.cargo, esc.nombre as escalafon, l.nombrelugar, DATE_FORMAT(tabla1.ultimo_ascenso, '%d-%m-%Y') AS xultimo_ascenso, DATE_FORMAT(tabla2.fecha_ingreso, '%d-%m-%Y') AS xfecha_ingreso, DATE_FORMAT(g.fecnac, '%d-%m-%Y'), g.antiguedad, TIMESTAMPDIFF(YEAR, g.fecnac, CURDATE()) AS edad, g.mail_corporativo FROM general g LEFT JOIN (SELECT h.legajo, MAX( h.desde ) as ultimo_ascenso FROM histocargo h GROUP BY h.legajo) as tabla1 ON g.legajo=tabla1.legajo LEFT JOIN (SELECT h.legajo, MIN(h.desde) as fecha_ingreso FROM histocargo h GROUP BY h.legajo) as tabla2 ON g.legajo=tabla2.legajo LEFT JOIN cargos c ON g.cargoactu=c.nrocargo LEFT JOIN escalafon esc ON c.escalafon=esc.id LEFT JOIN lugar l ON g.lugaractu=l.nrolugar WHERE g.activa=1 AND g.cargoactu!=55 ".$str_filt_agentes." ORDER BY g.legajo ASC";
                }
            }
            //echo $str;
            $cabecera = "<thead><tr><th>Legajo</th><th>Apellido</th><th>Nombre</th><th>Padr&oacute;n</th><th>DNI</th><th>Sexo</th><th>Cargo</th><th>Escalaf&oacute;n</th><th>Lugar</th><th>Ultimo Ascenso</th><th>Ingreso</th><th>Fecha nac.</th><th>Antiguedad</th><th>Edad</th><th>Mail corp.</th></tr></thead>";
            $foot = "<tfoot><tr><th>Legajo</th><th>Apellido</th><th>Nombre</th><th>Padr&oacute;n</th><th>DNI</th><th>Sexo</th><th>Cargo</th><th>Escalaf&oacute;n</th><th>Lugar</th><th>Ultimo Ascenso</th><th>Ingreso</th><th>Fecha nac.</th><th>Antiguedad</th><th>Edad</th><th>Mail corp.</th></tr></tfoot>";
            break; 
    case 22: 
            // Recibos de sueldo por empleado por año
            if($subcat!=0)
            {
                if($subsubcat!=0)
                {
                    $str_rec = "SELECT general.nombre, general.apellido, general.legajo, general.padron, cont_ci_raw.anio_liq, cont_ci_raw.mes_liq, cont_liquidacion_ci.nombre as mes_liq_nombre FROM general LEFT JOIN cont_ci_raw ON general.padron_ci=cont_ci_raw.padron LEFT JOIN cont_liquidacion_ci ON cont_ci_raw.mes_liq=cont_liquidacion_ci.id WHERE general.legajo=".$subcat." AND cont_ci_raw.anio_liq=".$subsubcat." GROUP BY cont_ci_raw.mes_liq ORDER BY cont_ci_raw.anio_liq DESC, cont_ci_raw.mes_liq DESC";
                    $per_liq = $subsubcat;
                    if(strlen(trim($per_liq))==1)
                    {
                        $per_liq = "| Periodo: 200".$per_liq;
                    }
                    else
                    {
                        $per_liq = "| Periodo: 20".$per_liq;
                    }
                }
                else //si no selecciona una subsubcategoria, muestro todos los recibos de todos los años para el empleado seleccionado
                {
                    $str_rec = "SELECT general.nombre, general.apellido, general.legajo, general.padron, cont_ci_raw.anio_liq, cont_ci_raw.mes_liq, cont_liquidacion_ci.nombre as mes_liq_nombre FROM general LEFT JOIN cont_ci_raw ON general.padron_ci=cont_ci_raw.padron LEFT JOIN cont_liquidacion_ci ON cont_ci_raw.mes_liq=cont_liquidacion_ci.id WHERE general.legajo=".$subcat." GROUP BY cont_ci_raw.anio_liq, cont_ci_raw.mes_liq ORDER BY cont_ci_raw.anio_liq DESC, cont_ci_raw.mes_liq DESC";
                    $per_liq = "| Periodo: TODOS";
                }
            }
            else //si no selecciona subcategoria, no muestro nada
            {
                $str_rec = "SELECT * FROM general WHERE legajo=34234234213541453453245";
            }

            $res_rec = $conn->query($str_rec);
            $leg_liq="";
            while($obj = $res_rec->fetch_object())
            {
                $leg_liq = "| ".$obj->apellido.", ".$obj->nombre;
            }
            //|$sum_neto = "$".number_format($sum_neto,2,",",".");
            $titulo = "<h2>Listado de liquidaciones ".$per_liq." ".$leg_liq."</h2>";
            //$subtitulo = "<h3>Per&iacute;odo: ".$per_liq."</h3>";
            $cabecera = "<thead><tr><th>Apellido</th><th>Nombre</th><th>Legajo</th><th>Padron</th><th>Periodo liq.</th><th>Mes liq.</th><th>Recibo</th></tr></thead>";
            $foot = "<tfoot><tr><th>Apellido</th><th>Nombre</th><th>Legajo</th><th>Padron</th><th>Periodo liq.</th><th>Mes liq.</th><th>Recibo</th></tr></tfoot>";
            
            $str = "recibo_sueldo"; //Una cadena que funciona como bandera, dado que despues se analizara diferente.
            break;  
    case 23: 
            $fecha_filt = "";
            if($fecha_desde != 0 && $fecha_hasta != 0 && setFecha($fecha_desde)<=setFecha($fecha_hasta))
            {
                $fecha_filt=" | Per&iacute;odo: ".$fecha_desde." al ".$fecha_hasta;
            }
            $titulo = "<h2>Listado de comisiones".$fecha_filt."</h2>";
            
            if($fecha_desde != 0 && $fecha_hasta != 0 && setFecha($fecha_desde)<=setFecha($fecha_hasta))
            {
                $titulo = "<h2>Listado de comisiones ".$fecha_filt."</h2>";
            }
            
            $str = "comisiones"; //Una cadena que funciona como bandera, dado que despues se analizara diferente.
            $cabecera = "<thead><tr><th>Legajo</th><th>Apellido</th><th>Nombre</th><th>Desde</th><th>Hasta</th><th>Total dias</th><th>Causa</th><th>Exp.</th><th>Observaciones</th></tr></thead>";
            $foot = "<tfoot><tr><th>Legajo</th><th>Apellido</th><th>Nombre</th><th>Desde</th><th>Hasta</th><th>Total dias</th><th>Causa</th><th>Exp.</th><th>Observaciones</th></tr></tfoot>";
            break; 
    case 24: 
            // Listado de certificados de los empleados
            if($subcat==0)
            {
                $titulo = "<h2>Listado de certificados</h2>";
            }
            else
            {
                $titulo = "<h2>Listado de certificados | Per&iacute;odo ".$subcat."</h2>";    
            }
            $str = "listado_certificados_empleados"; //Una cadena que funciona como bandera, dado que despues se analizara diferente.
            $cabecera = "<thead><tr><th>Apellido</th><th>Nombre</th><th>Legajo</th><th>Padr&oacute;n</th><th>Declar. jurada</th><th>Cert. Neg. ANSES</th><th>C&oacute;nyuge</th><th>A&ntilde;o</th></tr></thead>";
            $foot = "<tfoot><tr><th>Apellido</th><th>Nombre</th><th>Legajo</th><th>Padr&oacute;n</th><th>Declar. jurada</th><th>Cert. Neg. ANSES</th><th>C&oacute;nyuge</th><th>A&ntilde;o</th></tr></tfoot>";
            break;   
    case 25: 
            $fecha_filt = "";
            if($fecha_desde != 0 && $fecha_hasta != 0 && setFecha($fecha_desde)<=setFecha($fecha_hasta))
            {
                $fecha_filt=" | Per&iacute;odo: ".$fecha_desde." al ".$fecha_hasta;
            }
            $titulo = "<h2>Listado de capacitaciones".$fecha_filt."</h2>";
            
            if($fecha_desde != 0 && $fecha_hasta != 0 && setFecha($fecha_desde)<=setFecha($fecha_hasta))
            {
                $titulo = "<h2>Listado de capacitaciones ".$fecha_filt."</h2>";
            }
            
            $str = "capacitaciones"; //Una cadena que funciona como bandera, dado que despues se analizara diferente.
            $cabecera = "<thead><tr><th>Legajo</th><th>Apellido</th><th>Nombre</th><th>Nombre cap.</th><th>Tipo</th><th>Fecha</th><th>Cant. horas</th><th>Modalidad</th><th>Origen</th><th>Inherencia</th><th>Evluacion</th><th>Nota</th></tr></thead>";
            $foot = "<tfoot><tr><th>Legajo</th><th>Apellido</th><th>Nombre</th><th>Nombre cap.</th><th>Tipo</th><th>Fecha</th><th>Cant. horas</th><th>Modalidad</th><th>Origen</th><th>Inherencia</th><th>Evluacion</th><th>Nota</th></tr></tfoot>";
            break;
    case 26: //Solicitudes de licencias
            $fecha_filt = "";
            if($fecha_desde != 0 && $fecha_hasta != 0 && setFecha($fecha_desde)<=setFecha($fecha_hasta))
            {
                $fecha_filt=" | Per&iacute;odo: ".$fecha_desde." al ".$fecha_hasta;
            }
            if($fecha_desde3 != 0 && $fecha_hasta3 != 0 && setFecha($fecha_desde3)<=setFecha($fecha_hasta3))
            {
                $fecha_filt.=" | Carga: ".$fecha_desde3." al ".$fecha_hasta3;
            }
            $titulo = "<h2>Listado de llamados de licencias".$fecha_filt."</h2>";
            
            if($subcat!="")
            {
                $str_tipo_lic = "SELECT * FROM tipo_licencias WHERE id=".$subcat;
                $cons_tipo_lic = $conn->query($str_tipo_lic);
                $obj_tipo_lic = $cons_tipo_lic->fetch_object();
                $titulo = "<h2>Listado de llamados de licencias | ".$obj_tipo_lic->nombre."</h2>";
                if(($fecha_desde != 0 && $fecha_hasta != 0 && setFecha($fecha_desde)<=setFecha($fecha_hasta)) || ($fecha_desde3 != 0 && $fecha_hasta3 != 0 && setFecha($fecha_desde3)<=setFecha($fecha_hasta3)))
                {
                    $titulo = "<h2>Listado de llamados de licencias | ".$obj_tipo_lic->nombre.$fecha_filt."</h2>";
                }
            }
            
            $str = "solicitudes_licencias"; //Una cadena que funciona como bandera, dado que despues se analizara diferente.
            $cabecera = "<thead><tr><th>Estado</th><th>Licencia</th><th>Legajo</th><th>Apellido</th><th>Nombre</th><th>Lugar</th><th>Domicilio declarado</th><th>Desde</th><th>Causa</th><th>Observaciones</th><th>Pierde presentismo</th><th>Expediente</th><th>Fecha carga</th></tr></thead>";
            $foot = "<tfoot><tr><th>Estado</th><th>Licencia</th><th>Legajo</th><th>Apellido</th><th>Nombre</th><th>Lugar</th><th>Domicilio declarado</th><th>Desde</th><th>Causa</th><th>Observaciones</th><th>Pierde presentismo</th><th>Expediente</th><th>Fecha carga</th></tr></tfoot>";
            break; 
    case 27: //Listado de sanciones
            $fecha_filt = "";
            if($fecha_desde != 0 && $fecha_hasta != 0 && setFecha($fecha_desde)<=setFecha($fecha_hasta))
            {
                $fecha_filt=" | Per&iacute;odo: ".$fecha_desde." al ".$fecha_hasta;
            }
            $titulo = "<h2>Listado de sanciones".$fecha_filt."</h2>";
            
            if($fecha_desde != 0 && $fecha_hasta != 0 && setFecha($fecha_desde)<=setFecha($fecha_hasta))
            {
                $titulo = "<h2>Listado de sanciones ".$fecha_filt."</h2>";
            }
            
            $str = "sanciones"; //Una cadena que funciona como bandera, dado que despues se analizara diferente.
            $cabecera = "<thead><tr><th>Legajo</th><th>Apellido</th><th>Nombre</th><th>Tipo</th><th>Desde</th><th>Hasta</th><th>Dispuesta por</th><th>Expediente</th><th>Observaciones</th></tr></thead>";
            $foot = "<tfoot><tr><th>Legajo</th><th>Apellido</th><th>Nombre</th><th>Tipo</th><th>Desde</th><th>Hasta</th><th>Dispuesta por</th><th>Expediente</th><th>Observaciones</th></tr></tfoot>";
            break;                                 
    }

echo '<div class="divtabla" id="divtabla">';
echo $titulo;
if($cat==16 && $subcat!=0 && $subsubcat != 0)
{
    echo $subtitulo;
    echo $neto;
    echo $periodo;
}
echo '<table id="tablaReportes" class="display cell-border compact hover nowrap row-border stripe" cellspacing="0" width="100%">';
//echo '<table id="tablaReportes" class="display cell-border compact hover row-border stripe">';
echo $cabecera;
echo '<tbody>';
//echo $str;
$tot_reg = 0;
switch($str)
{
    case "comp1":
        $str="SELECT g.legajo, g.apellido, g.nombre, c.cargo, SUM(li.totaldias) as totaldias, SUM(li.disponibles) as totaldisponibles, l.nombrelugar, f.nombre
        FROM general g 
        LEFT JOIN lic_comp_dias li 
        ON g.legajo=li.legajo 
        RIGHT JOIN cargos c 
        ON g.cargoactu=c.nrocargo 
        RIGHT JOIN lugar l 
        ON g.lugaractu=l.nrolugar
        RIGHT JOIN fuero f 
        ON l.nro_fuero = f.id
        WHERE g.activa=1 ".$str_filt_agentes." AND goce='S'
        GROUP BY li.legajo 
        ORDER BY g.apellido ASC";
        $result = mysqli_query($conn, $str);
        $nbrows = mysqli_num_rows($result);
        $rows = array();
        
        while ($row = mysqli_fetch_row($result))
        {
            
            $cont=count($row);
            if($row[4]!=0) //Oculto cuando los disponibles son 0
            {
                $tot_reg++;
                echo "<tr>";
                for ($i=0; $i <= count($row)-1; $i++) 
                {
                    echo "<td>".$row[$i]."</td>";
                }
                echo "</tr>";
            }
        }
        break;            
    case "comp2":
        $str = "SELECT g.legajo, g.apellido, g.nombre, c.cargo, DATE_FORMAT(desde, '%d-%m-%Y'), DATE_FORMAT(hasta, '%d-%m-%Y'), totaldias, disponibles, lugar.nombrelugar, fuero.nombre as nombre_fuero FROM general g, lic_comp_dias, lugar, fuero, cargos c WHERE g.legajo = lic_comp_dias.legajo AND g.lugaractu = lugar.nrolugar AND lugar.nro_fuero = fuero.id AND g.cargoactu=c.nrocargo AND g.activa=1 AND disponibles>0 AND goce='S' ".$str_filt_agentes." ORDER BY g.apellido ASC, desde ASC";
        $res_ = $conn->query($str);
        //Cuento la cantidad de registros para cada legajo
        $str_count = "SELECT legajo, count(*) as cant FROM lic_comp_dias GROUP BY legajo ORDER BY id ASC";
        $res_count = $conn->query($str_count);
        //2 arreglos paralelos, en uno guardo el legajo y en otro la cantidad de registros por cada legajo
        $arr_legaj = array();
        $arr_cant = array();
        $k=0;
        while($fila_count = $res_count->fetch_object())
        {
            $arr_legaj[$k] = $fila_count->legajo;
            $arr_cant[$k] = $fila_count->cant;
        }
        $j=0;
        
        while ($row = $res_->fetch_row())
        {
            $tot_reg++;
            $cont=count($row);
            echo "<tr>";
            for ($i=0; $i <= count($row)-1; $i++) 
            {
                echo "<td>".$row[$i]."</td>";

            }
            echo "</tr>";
            $j++;
        }
        break;
    case "evaluacion":
        //Consulta anterior

        //$str_ev = ($subcat ==0)?"SELECT ev.id, e.legajo_evaluador as evaluador, e.legajo_evaluado as evaluado, l.nombrelugar, c.cargo, ev.nombre, DATE_FORMAT(ev.fecha_inicio, '%d-%m-%Y') AS fecha_inicio, DATE_FORMAT(ev.fecha_fin, '%d-%m-%Y') AS fecha_fin, ev.motivo, ep.id as idper, ep.nota_promedio, ep.cant_aplazos, ep.observaciones, ef.* FROM eval_evaluadores e LEFT JOIN eval_evaluacion_persona ep ON e.legajo_evaluado=ep.legajo_evaluado LEFT JOIN eval_evaluacion ev ON ep.id_evaluacion=ev.id LEFT JOIN general g ON e.legajo_evaluado=g.legajo LEFT JOIN lugar l ON g.lugaractu=l.nrolugar LEFT JOIN cargos c ON g.cargoactu=c.nrocargo LEFT JOIN eval_factores ef ON e.legajo_evaluado=ef.legajo WHERE (ev.id=ef.id_evaluacion OR ef.id_evaluacion is NULL) AND g.activa=1 ORDER BY ep.id":"SELECT ev.id, e.legajo_evaluador as evaluador, e.legajo_evaluado as evaluado, l.nombrelugar, c.cargo, ev.nombre, DATE_FORMAT(ev.fecha_inicio, '%d-%m-%Y') AS fecha_inicio, DATE_FORMAT(ev.fecha_fin, '%d-%m-%Y') AS fecha_fin, ev.motivo, ep.id as idper, ep.nota_promedio, ep.cant_aplazos, ep.observaciones, ef.* FROM eval_evaluadores e LEFT JOIN eval_evaluacion_persona ep ON e.legajo_evaluado=ep.legajo_evaluado LEFT JOIN eval_evaluacion ev ON ep.id_evaluacion=ev.id LEFT JOIN general g ON e.legajo_evaluado=g.legajo LEFT JOIN lugar l ON g.lugaractu=l.nrolugar LEFT JOIN cargos c ON g.cargoactu=c.nrocargo LEFT JOIN eval_factores ef ON e.legajo_evaluado=ef.legajo WHERE (ev.id is NULL or ev.id=".$subcat.") AND (ev.id=ef.id_evaluacion OR ef.id_evaluacion is NULL) AND g.activa=1 ORDER BY ep.id";
        //echo $str_ev;

        //Consulta nueva

        //Obtengo la ultima fecha en la que se calcularon los factores, dado que hay factores calculados de varios periodos. Obtengo la fecha del ultimo
        $str_fact = "SELECT MAX(fecha_inicio) as fecha_inicio FROM eval_factores";
        $cons_fact = $conn->query($str_fact);
        $obj_fact = $cons_fact->fetch_object();
        $ul_fecha = $obj_fact->fecha_inicio;
        //$str_ev = ($subcat ==0)?"SELECT ev.id, e.legajo_evaluador as evaluador, e.legajo_evaluado as evaluado, l.nombrelugar, c.cargo, ev.nombre, DATE_FORMAT(ev.fecha_inicio, '%d-%m-%Y') AS fecha_inicio, DATE_FORMAT(ev.fecha_fin, '%d-%m-%Y') AS fecha_fin, ev.motivo, ep.id as idper, ep.nota_promedio, ep.cant_aplazos, ep.observaciones, ef.* FROM eval_evaluadores e LEFT JOIN eval_evaluacion_persona ep ON e.legajo_evaluado=ep.legajo_evaluado LEFT JOIN eval_evaluacion ev ON ep.id_evaluacion=ev.id LEFT JOIN general g ON e.legajo_evaluado=g.legajo LEFT JOIN lugar l ON g.lugaractu=l.nrolugar LEFT JOIN cargos c ON g.cargoactu=c.nrocargo LEFT JOIN eval_factores ef ON e.legajo_evaluado=ef.legajo WHERE ef.fecha_inicio='".$ul_fecha."' ORDER BY ep.id":"SELECT ev.id, e.legajo_evaluador as evaluador, e.legajo_evaluado as evaluado, l.nombrelugar, c.cargo, ev.nombre, DATE_FORMAT(ev.fecha_inicio, '%d-%m-%Y') AS fecha_inicio, DATE_FORMAT(ev.fecha_fin, '%d-%m-%Y') AS fecha_fin, ev.motivo, ep.id as idper, ep.nota_promedio, ep.cant_aplazos, ep.observaciones, ef.* FROM eval_evaluadores e LEFT JOIN eval_evaluacion_persona ep ON e.legajo_evaluado=ep.legajo_evaluado LEFT JOIN eval_evaluacion ev ON ep.id_evaluacion=ev.id LEFT JOIN general g ON e.legajo_evaluado=g.legajo LEFT JOIN lugar l ON g.lugaractu=l.nrolugar LEFT JOIN cargos c ON g.cargoactu=c.nrocargo LEFT JOIN eval_factores ef ON e.legajo_evaluado=ef.legajo WHERE (ev.id is NULL or ev.id=".$subcat.") AND ef.fecha_inicio='".$ul_fecha."' ORDER BY ep.id";
        $str_ev = ($subcat ==0)?"SELECT ev.id, ep.legajo_evaluador as evaluador, ep.legajo_evaluado as evaluado, l.nombrelugar, c.cargo, ev.nombre, DATE_FORMAT(ev.fecha_inicio, '%d-%m-%Y') AS fecha_inicio, DATE_FORMAT(ev.fecha_fin, '%d-%m-%Y') AS fecha_fin, ev.motivo, ep.id as idper, ep.nota_promedio, ep.cant_aplazos, ep.observaciones, ef.* FROM eval_evaluacion_persona ep LEFT JOIN eval_evaluacion ev ON ep.id_evaluacion=ev.id LEFT JOIN general g ON ep.legajo_evaluado=g.legajo LEFT JOIN lugar l ON g.lugaractu=l.nrolugar LEFT JOIN cargos c ON g.cargoactu=c.nrocargo LEFT JOIN eval_factores ef ON ep.legajo_evaluado=ef.legajo WHERE ef.fecha_inicio='".$ul_fecha."' ORDER BY ep.id":"SELECT ev.id, ep.legajo_evaluador as evaluador, ep.legajo_evaluado as evaluado, l.nombrelugar, c.cargo, ev.nombre, DATE_FORMAT(ev.fecha_inicio, '%d-%m-%Y') AS fecha_inicio, DATE_FORMAT(ev.fecha_fin, '%d-%m-%Y') AS fecha_fin, ev.motivo, ep.id as idper, ep.nota_promedio, ep.cant_aplazos, ep.observaciones, ef.* FROM eval_evaluacion_persona ep LEFT JOIN eval_evaluacion ev ON ep.id_evaluacion=ev.id LEFT JOIN general g ON ep.legajo_evaluado=g.legajo LEFT JOIN lugar l ON g.lugaractu=l.nrolugar LEFT JOIN cargos c ON g.cargoactu=c.nrocargo LEFT JOIN eval_factores ef ON ep.legajo_evaluado=ef.legajo WHERE (ev.id is NULL or ev.id=".$subcat.") AND ef.fecha_inicio='".$ul_fecha."' ".$str_filt_agentes." ORDER BY ep.id";
        $res_ev = $conn->query($str_ev);
        
        if($res_ev)
        {
            $i=0;
            while($obj = $res_ev->fetch_object())
            {
                $res_nomb_evaluador = $conn->query("SELECT apellido, nombre FROM general WHERE legajo=".$obj->evaluador);
                $obj_nombre_evaluador = $res_nomb_evaluador->fetch_object();
                $nombre_evaluador = $obj_nombre_evaluador->apellido.", ".$obj_nombre_evaluador->nombre;
                $res_nomb_evaluado = $conn->query("SELECT apellido, nombre, nombrelugar FROM general, lugar WHERE lugaractu=nrolugar AND legajo=".$obj->evaluado);
                $obj_nombre_evaluado = $res_nomb_evaluado->fetch_object();
                $nombre_evaluado = $obj_nombre_evaluado->apellido.", ".$obj_nombre_evaluado->nombre;
                $lugar_evaludo = $obj_nombre_evaluado->nombrelugar;
                if(strlen($obj->motivo)>25)
                {
                    $mot = substr($obj->motivo,0,25)." ...";
                }
                else
                {
                    $mot = $obj->motivo;
                }
                if(strlen($obj->observaciones)>60)
                {
                    $obs = substr($obj->observaciones,0,60)." ...";
                }
                else
                {
                    $obs = $obj->observaciones;
                }
                
                
                if($obj->nombre)
                {
                    echo "<tr>";
                    echo "<td>Evaluado</td>";
                    echo "<td>".$nombre_evaluador."</td>";
                    echo "<td>".$nombre_evaluado."</td>";
                    echo "<td>".$lugar_evaludo."</td>";    
                    echo "<td>".$obj->cargo."</td>";  
                    echo "<td>".$obj->nombre."</td>";
                    echo "<td>".$obj->fecha_inicio."</td>";
                    echo "<td>".$obj->fecha_fin."</td>";
                    echo "<td>".$obj->nota_promedio."</td>";
                    echo "<td>".$obj->cant_aplazos."</td>";
                    echo "<td>".$obj->cant_tardanzas."</td>";
                    echo "<td>".$obj->cant_licencias."</td>";
                    echo "<td>".$obj->conducta."</td>";
                    echo "<td>".$obj->capacitacion."</td>";
                    echo "<td>".$obj->asist_puntualidad."</td>";
                    echo "<td>".$obj->antiguedad_cargo."</td>";
                    echo "<td>".$obj->antiguedad_escalafon."</td>";
                    echo "<td>".$obj->puntaje_total."</td>";
                    $dataModal = 'data-nombre="'.$obj->nombre.'"';
                    $dataModal.= ' data-motivo="'.$obj->motivo.'"';
                    $dataModal.= ' data-fecha_inicio="'.$obj->fecha_inicio.'"';
                    $dataModal.= ' data-fecha_fin="'.$obj->fecha_fin.'"';
                    $dataModal.= ' data-nombre_evaluador="'.$nombre_evaluador.'"';
                    $dataModal.= ' data-nombre_evaluado="'.$nombre_evaluado.'"';
                    $dataModal.= ' data-promedio="'.$obj->nota_promedio.'"';
                    $dataModal.= ' data-aplazos="'.$obj->cant_aplazos.'"';
                    $dataModal.= ' data-observaciones="'.$obj->observaciones.'" ';
                    $dataModal.= ' data-tardanzas="'.$obj->cant_tardanzas.'" ';
                    $dataModal.= ' data-licencias="'.$obj->cant_licencias.'" ';
                    $dataModal.= ' data-conducta="'.$obj->conducta.'" ';
                    $dataModal.= ' data-capacitacion="'.$obj->capacitacion.'" ';
                    $dataModal.= ' data-asistencia_puntualidad="'.$obj->asist_puntualidad.'" ';
                    $dataModal.= ' data-antiguedad_cargo="'.$obj->antiguedad_cargo.'" ';
                    $dataModal.= ' data-antiguedad_escalafon="'.$obj->antiguedad_escalafon.'" ';
                    $dataModal.= ' data-puntaje_total="'.$obj->puntaje_total.'" ';
                    $dataModal.=' data-ideval="'.$obj->id.'" ';
                    $dataModal.=' data-idevalper="'.$obj->idper.'" ';
                    
                    echo '<td> <button type="button" id="button_'.$i.'" name="ver_evaluacion" class="btn btn-info btn-xs" data-toggle="modal" data-target="#modalEvaluacion" '.$dataModal.' title="Ver evaluaci&oacute;n"><span class="glyphicon glyphicon-list-alt"></span> Ver</button></td>';
                }
                else
                {
                    echo "<tr style='background-color: #ffe2e2;'>";
                    echo "<td>No evaluado</td>";
                    echo "<td>".$nombre_evaluador."</td>";
                    echo "<td>".$nombre_evaluado."</td>";
                    echo "<td>".$lugar_evaludo."</td>";
                    echo "<td>".$obj->cargo."</td>";
                    echo "<td>No evaluado</td>";
                    echo "<td>No evaluado</td>";
                    echo "<td>No evaluado</td>";
                    echo "<td>No evaluado</td>";
                    echo "<td>No evaluado</td>";
                    echo "<td>".$obj->cant_tardanzas."</td>";
                    echo "<td>".$obj->cant_licencias."</td>";
                    echo "<td>".$obj->conducta."</td>";
                    echo "<td>".$obj->capacitacion."</td>";
                    echo "<td>".$obj->asist_puntualidad."</td>";
                    echo "<td>".$obj->antiguedad_cargo."</td>";
                    echo "<td>".$obj->antiguedad_escalafon."</td>";
                    echo "<td>No evaluado</td>";
                    echo "<td>No evaluado</td>";
                }
                echo "</tr>";
                $i++;    
            }
        }
        break;
    case 'factores_ascenso':
        $str_fact_as = "SELECT ef.*, g.nombre, g.apellido, DATE_FORMAT(g.fecha_carga, '%d-%m-%Y') AS fecha_ingreso, l.nombrelugar, c.cargo, e.nombre as nombre_escalafon, DATE_FORMAT(tabla1.ultimo_ascenso, '%d-%m-%Y') AS xultimo_ascenso, te.titulo FROM inscriptos_2019 ins LEFT JOIN general g ON ins.legajo=g.legajo LEFT JOIN titulo t ON g.legajo=t.legajo LEFT JOIN tipo_estudio te ON t.codestud=te.codestudio LEFT JOIN (SELECT h.legajo, MAX( h.desde ) as ultimo_ascenso FROM histocargo h GROUP BY h.legajo) as tabla1 ON g.legajo=tabla1.legajo LEFT JOIN eval_factores ef ON g.legajo=ef.legajo LEFT JOIN lugar l ON g.lugaractu=l.nrolugar LEFT JOIN cargos c ON g.cargoactu=c.nrocargo LEFT JOIN escalafon e ON c.escalafon=e.id WHERE ef.fecha_inicio=(SELECT MAX(fecha_inicio) FROM eval_factores) AND c.escalafon!=4 AND (c.escalafon!=3 || (c.escalafon=3 && c.nrocargo IN (20, 26, 32))) ".$str_filt_agentes." ORDER BY apellido, nombre";
        $cons_fact_as = $conn->query($str_fact_as);
        while($obj_fact_as = $cons_fact_as->fetch_object())
        {
            $ant_cargo = $obj_fact_as->antiguedad_cargo*5/100;
            $ant_esca = 2*round($obj_fact_as->antiguedad_escalafon/5)/100;
            echo "<tr>";
            echo "<td>".$obj_fact_as->legajo."</td>";
            echo "<td>".$obj_fact_as->apellido."</td>";
            echo "<td>".$obj_fact_as->nombre."</td>";
            echo "<td>".$obj_fact_as->nombrelugar."</td>";   
            echo "<td>".$obj_fact_as->cargo."</td>";
            echo "<td>".$obj_fact_as->nombre_escalafon."</td>";
            echo "<td>".$obj_fact_as->titulo."</td>";
            echo "<td>".$obj_fact_as->cant_tardanzas."</td>";
            echo "<td>".$obj_fact_as->cant_licencias."</td>";
            echo "<td>".$obj_fact_as->conducta."</td>";
            echo "<td>".$obj_fact_as->capacitacion."</td>";
            echo "<td>".$obj_fact_as->asist_puntualidad."</td>";
            echo "<td>".$obj_fact_as->antiguedad_cargo."</td>";
            echo "<td>".$ant_cargo."</td>";
            echo "<td>".$obj_fact_as->antiguedad_escalafon."</td>";
            echo "<td>".$ant_esca."</td>";
            echo "<td>".$obj_fact_as->puntaje_evaluacion."</td>";
            echo "<td>".$obj_fact_as->puntaje_total."</td>";
            echo "<td>".$obj_fact_as->fecha_ingreso."</td>";
            echo "<td>".$obj_fact_as->xultimo_ascenso."</td>";
            echo "</tr>";
        }
        break;   
    case 'listado_final':
        $str_list_final = "SELECT general.apellido, general.nombre, cargos.cargo, lugar.nombrelugar, eval_evaluacion_persona.nota_promedio, eval_factores.asist_puntualidad, eval_factores.conducta, eval_factores.capacitacion, eval_factores.antiguedad_cargo, eval_factores.antiguedad_escalafon, eval_factores.puntaje_total FROM ascensos_2017 LEFT JOIN general on ascensos_2017.legajo=general.legajo LEFT JOIN cargos ON general.cargoactu=cargos.nrocargo LEFT JOIN lugar ON general.lugaractu=lugar.nrolugar LEFT JOIN eval_factores on general.legajo=eval_factores.legajo LEFT JOIN eval_evaluacion_persona on eval_factores.legajo=eval_evaluacion_persona.legajo_evaluado WHERE (eval_evaluacion_persona.legajo_evaluado=eval_factores.legajo OR eval_evaluacion_persona.legajo_evaluado is null) AND general.activa=1 AND general.legajo IN (".$legajos_vista.") ORDER BY general.apellido ASC";
        $cons_list_final = $conn->query($str_list_final);
        while($obj_final = $cons_list_final->fetch_object())
        {
            $ant_cargo = $obj_final->antiguedad_cargo*5/100;
            $ant_esca = 2*round($obj_final->antiguedad_escalafon/5)/100;
            echo "<tr>";
            echo "<td>".$obj_final->apellido."</td>";
            echo "<td>".$obj_final->nombre."</td>";
            echo "<td>".$obj_final->cargo."</td>";
            echo "<td>".$obj_final->nombrelugar."</td>";
            if($obj_final->nota_promedio)
            {
                echo "<td>".$obj_final->nota_promedio."</td>";
            }
            else
            {
                echo "<td>0 (no evaluado)</td>";
            }
            echo "<td>".$obj_final->conducta."</td>";
            echo "<td>".$obj_final->capacitacion."</td>";
            echo "<td>".$obj_final->asist_puntualidad."</td>";
            echo "<td>".$ant_cargo."</td>";
            echo "<td>".$obj_final->antiguedad_cargo."</td>";
            echo "<td>".$ant_esca."</td>";
            echo "<td>".$obj_final->puntaje_total."</td>";
            echo "</tr>";
        }
        break;        
    case 'organismo_historico':
        $str = "SELECT g.legajo, g.apellido, g.nombre, tabla_cargo.cargo, tabla_lugar.desde, tabla_lugar.hasta, g.activa FROM general g LEFT JOIN (SELECT h.legajo, h.nrolugar, h.desde as desde, h.hasta as hasta FROM histolugar h WHERE h.nrolugar=".$fila->nrolugar." AND ((h.desde BETWEEN '".$anio_desde."' AND '".$anio_hasta."') OR (h.hasta BETWEEN '".$anio_desde."' AND '".$anio_hasta."') OR ('".$anio_desde."' BETWEEN h.desde AND h.hasta) OR ('".$anio_hasta."' BETWEEN h.desde AND h.hasta))) as tabla_lugar ON g.legajo=tabla_lugar.legajo LEFT JOIN (SELECT hc.legajo, hc.nrocargo, hcc.cargo as cargo FROM histocargo hc LEFT JOIN cargos hcc ON hc.nrocargo=hcc.nrocargo WHERE (hc.desde BETWEEN '".$anio_desde."' AND '".$anio_hasta."') OR (hc.hasta BETWEEN '".$anio_desde."' AND '".$anio_hasta."') OR ('".$anio_desde."' BETWEEN hc.desde AND hc.hasta) OR ('".$anio_hasta."' BETWEEN hc.desde AND hc.hasta)) as tabla_cargo ON g.legajo=tabla_cargo.legajo WHERE tabla_lugar.nrolugar=".$fila->nrolugar." ORDER BY g.apellido";
        $str = "SELECT g.legajo, g.apellido, g.nombre, tabla_lugar.desde, tabla_lugar.hasta, g.activa FROM general g LEFT JOIN (SELECT h.legajo, h.nrolugar, h.desde as desde, h.hasta as hasta FROM histolugar h WHERE h.nrolugar=".$fila->nrolugar.") as tabla_lugar ON g.legajo=tabla_lugar.legajo WHERE tabla_lugar.nrolugar=".$fila->nrolugar." ".$str_filt_agentes." ORDER BY g.apellido, tabla_lugar.desde";
        //echo $str;

        $res_org_hist = $conn->query($str);
        while($obj = $res_org_hist->fetch_object())
        {
            if(!$obj->hasta)
            {
                $trab_hasta = date("Y-m-d");
            }
            else
            {
                $trab_hasta = $obj->hasta;
            }
            if((($obj->desde>=$anio_desde) && ($obj->desde<=$anio_hasta)) || (($trab_hasta>=$anio_desde) && ($trab_hasta<=$anio_hasta)) || (($anio_desde>=$obj->desde) && ($anio_desde<=$trab_hasta)) || (($anio_hasta>=$obj->desde) && ($anio_hasta<=$trab_hasta)))
            {
                $str_cargo = "SELECT c.cargo FROM histocargo hc LEFT JOIN cargos c ON hc.nrocargo=c.nrocargo WHERE legajo=".$obj->legajo." AND ((hc.desde BETWEEN '".$anio_desde."' AND '".$anio_hasta."') OR (hc.hasta BETWEEN '".$anio_desde."' AND '".$anio_hasta."') OR ('".$anio_desde."' BETWEEN hc.desde AND hc.hasta) OR ('".$anio_hasta."' BETWEEN hc.desde AND hc.hasta) OR ('".$anio_desde."'> hc.desde AND hc.hasta is null) OR ('".$anio_hasta."' > hc.desde AND hc.hasta is null) OR ('".$trab_hasta."' > hc.desde AND hc.hasta is null))";
                $cons_cargo = $conn->query($str_cargo);
                $obj_cargo = $cons_cargo->fetch_object();
                echo "<tr>";
                echo "<td>".$obj->legajo."</td>";
                echo "<td>".$obj->apellido."</td>";
                echo "<td>".$obj->nombre."</td>";
                echo "<td>".$obj_cargo->cargo."</td>";
                echo "<td>".$obj->desde."</td>";
                if($obj->hasta==NULL)
                {
                    echo "<td>Actualmente</td>";
                }
                else
                {
                    echo "<td>".$obj->hasta."</td>";
                }
                if($obj->activa==1)
                {
                    echo "<td>Activo</td>";
                }
                else
                {
                    echo "<td>Retirado</td>";
                }
                echo "</tr>";
            }
        }
        break;
    case "licencias":
        if($fecha_desde3 != 0 && $fecha_hasta3 != 0 && setFecha($fecha_desde3)<=setFecha($fecha_hasta3))
        {
            $str_carga = " l.created_at BETWEEN '".setFecha($fecha_desde3)." 00:00:00' AND '".setFecha($fecha_hasta3)." 23:59:59'";
        }
        else
        {
            $str_carga = "1=1";
        }
        if($fecha_desde != 0 && $fecha_hasta != 0 && setFecha($fecha_desde)<=setFecha($fecha_hasta))
        {
            $str_lic = ($subcat=="")?"SELECT l.*, DATE_FORMAT(l.desde, '%d-%m-%Y') AS fdesde, DATE_FORMAT(l.hasta, '%d-%m-%Y') AS fhasta, DATE_FORMAT(l.created_at, '%d-%m-%Y') AS fecha_carga, g.nombre, g.apellido, lu.nombrelugar, tl.nombre as nombre_licencia, DATE_FORMAT(n.log, '%d-%m-%Y %H:%i:%s') as notificado FROM licencias l LEFT JOIN general g ON l.legajo=g.legajo LEFT JOIN tipo_licencias tl ON l.tipo_licencia=tl.id LEFT JOIN lugar lu ON g.lugaractu=lu.nrolugar LEFT JOIN notificaciones_licencias n ON l.id=n.id_licencia WHERE ((l.desde BETWEEN '".setFecha($_GET["fecha_desde"])."' AND '".setFecha($_GET["fecha_hasta"])."') OR (l.hasta BETWEEN '".setFecha($_GET["fecha_desde"])."' AND '".setFecha($_GET["fecha_hasta"])."') OR ('".setFecha($_GET["fecha_desde"])."' BETWEEN l.desde AND l.hasta) OR ('".setFecha($_GET["fecha_hasta"])."' BETWEEN l.desde AND l.hasta)) AND ".$str_carga." ".$str_filt_agentes." ORDER BY l.desde DESC":"SELECT l.*, DATE_FORMAT(l.desde, '%d-%m-%Y') AS fdesde, DATE_FORMAT(l.hasta, '%d-%m-%Y') AS fhasta, DATE_FORMAT(l.created_at, '%d-%m-%Y') AS fecha_carga, g.nombre, g.apellido, lu.nombrelugar, tl.nombre as nombre_licencia, DATE_FORMAT(n.log, '%d-%m-%Y %H:%i:%s') as notificado FROM licencias l LEFT JOIN general g ON l.legajo=g.legajo LEFT JOIN tipo_licencias tl ON l.tipo_licencia=tl.id LEFT JOIN lugar lu ON g.lugaractu=lu.nrolugar LEFT JOIN notificaciones_licencias n ON l.id=n.id_licencia WHERE l.tipo_licencia=".$subcat." AND ((l.desde BETWEEN '".setFecha($_GET["fecha_desde"])."' AND '".setFecha($_GET["fecha_hasta"])."') OR (l.hasta BETWEEN '".setFecha($_GET["fecha_desde"])."' AND '".setFecha($_GET["fecha_hasta"])."') OR ('".setFecha($_GET["fecha_desde"])."' BETWEEN l.desde AND l.hasta) OR ('".setFecha($_GET["fecha_hasta"])."' BETWEEN l.desde AND l.hasta)) AND ".$str_carga." ".$str_filt_agentes." ORDER BY l.desde DESC";
        }
        else
        {
            $str_lic = ($subcat=="")?"SELECT l.*, DATE_FORMAT(l.desde, '%d-%m-%Y') AS fdesde, DATE_FORMAT(l.hasta, '%d-%m-%Y') AS fhasta, DATE_FORMAT(l.created_at, '%d-%m-%Y') AS fecha_carga, g.nombre, g.apellido, lu.nombrelugar, tl.nombre as nombre_licencia, DATE_FORMAT(n.log, '%d-%m-%Y %H:%i:%s') as notificado FROM licencias l LEFT JOIN general g ON l.legajo=g.legajo LEFT JOIN tipo_licencias tl ON l.tipo_licencia=tl.id LEFT JOIN lugar lu ON g.lugaractu=lu.nrolugar LEFT JOIN notificaciones_licencias n ON l.id=n.id_licencia WHERE ".$str_carga." ".$str_filt_agentes." ORDER BY l.desde DESC":"SELECT l.*, DATE_FORMAT(l.desde, '%d-%m-%Y') AS fdesde, DATE_FORMAT(l.hasta, '%d-%m-%Y') AS fhasta, DATE_FORMAT(l.created_at, '%d-%m-%Y') AS fecha_carga, g.nombre, g.apellido, lu.nombrelugar, tl.nombre as nombre_licencia, DATE_FORMAT(n.log, '%d-%m-%Y %H:%i:%s') as notificado FROM licencias l LEFT JOIN general g ON l.legajo=g.legajo LEFT JOIN tipo_licencias tl ON l.tipo_licencia=tl.id LEFT JOIN lugar lu ON g.lugaractu=lu.nrolugar LEFT JOIN notificaciones_licencias n ON l.id=n.id_licencia WHERE l.tipo_licencia=".$subcat." AND ".$str_carga." ".$str_filt_agentes." ORDER BY l.desde DESC";
        }
        //echo $str_lic;
        $cons_lic = $conn->query($str_lic);
        while($obj = $cons_lic->fetch_object())
        {
            echo "<tr>";
            echo "<td>".$obj->nombre_licencia."</td>";
            echo "<td>".$obj->legajo."</td>";
            echo "<td>".$obj->apellido."</td>";
            echo "<td>".$obj->nombre."</td>";
            echo "<td>".$obj->nombrelugar."</td>";
            echo "<td>".$obj->fdesde."</td>";
            echo "<td>".$obj->fhasta."</td>";
            echo "<td>".$obj->causa."</td>";  
            echo "<td>".$obj->observaciones."</td>";
            $desiste = ($obj->goce=='S')?'NO':'SI';
            echo "<td>".$desiste."</td>";
            $pierde_preset = ($obj->pierde_presentismo==0)?'NO':'SI';
            echo "<td>".$pierde_preset."</td>";
            echo "<td>".$obj->expediente."</td>";
            echo "<td>".$obj->fecha_carga."</td>";
            $notif = $obj->notificado?$obj->notificado:'NO';
            echo "<td>".$notif."</td>";
            echo "</tr>";
        }
        break;  
    case "solicitudes_licencias":
        if($fecha_desde3 != 0 && $fecha_hasta3 != 0 && setFecha($fecha_desde3)<=setFecha($fecha_hasta3))
        {
            $str_carga = " l.created_at BETWEEN '".setFecha($fecha_desde3)." 00:00:00"."' AND '".setFecha($fecha_hasta3)." 23:59:59"."'";
        }
        else
        {
            $str_carga = "1=1";
        }
        if($fecha_desde != 0)
        {
            $str_lic = ($subcat=="")?"SELECT l.*, DATE_FORMAT(l.desde, '%d-%m-%Y') AS fdesde, DATE_FORMAT(l.created_at, '%d-%m-%Y') AS fecha_carga, g.nombre, g.apellido, lu.nombrelugar, tl.nombre as nombre_licencia, estSol.nombre AS estadoLicencia FROM solicitudLicencias l LEFT JOIN general g ON l.legajo=g.legajo LEFT JOIN tipo_licencias tl ON l.tipo_licencia=tl.id LEFT JOIN lugar lu ON g.lugaractu=lu.nrolugar LEFT JOIN estadoSolicitudLicencia estSol ON l.estado=estSol.id WHERE l.desde='".setFecha($_GET["fecha_desde"])."' AND ".$str_carga." ".$str_filt_agentes." ORDER BY l.desde DESC, l.hora_solicitud DESC":"SELECT l.*, DATE_FORMAT(l.desde, '%d-%m-%Y') AS fdesde, DATE_FORMAT(l.created_at, '%d-%m-%Y') AS fecha_carga, g.nombre, g.apellido, lu.nombrelugar, tl.nombre as nombre_licencia, estSol.nombre AS estadoLicencia FROM solicitudLicencias l LEFT JOIN general g ON l.legajo=g.legajo LEFT JOIN tipo_licencias tl ON l.tipo_licencia=tl.id LEFT JOIN lugar lu ON g.lugaractu=lu.nrolugar LEFT JOIN estadoSolicitudLicencia estSol ON l.estado=estSol.id WHERE l.tipo_licencia=".$subcat." AND l.desde='".setFecha($_GET["fecha_desde"])."' AND ".$str_carga." ".$str_filt_agentes." ORDER BY l.desde DESC, l.hora_solicitud DESC";
        }
        else
        {
            $str_lic = ($subcat=="")?"SELECT l.*, DATE_FORMAT(l.desde, '%d-%m-%Y') AS fdesde, DATE_FORMAT(l.created_at, '%d-%m-%Y') AS fecha_carga, g.nombre, g.apellido, lu.nombrelugar, tl.nombre as nombre_licencia, estSol.nombre AS estadoLicencia FROM solicitudLicencias l LEFT JOIN general g ON l.legajo=g.legajo LEFT JOIN tipo_licencias tl ON l.tipo_licencia=tl.id LEFT JOIN lugar lu ON g.lugaractu=lu.nrolugar LEFT JOIN estadoSolicitudLicencia estSol ON l.estado=estSol.id WHERE ".$str_carga." ".$str_filt_agentes." ORDER BY l.desde DESC, l.hora_solicitud DESC":"SELECT l.*, DATE_FORMAT(l.desde, '%d-%m-%Y') AS fdesde, DATE_FORMAT(l.created_at, '%d-%m-%Y') AS fecha_carga, g.nombre, g.apellido, lu.nombrelugar, tl.nombre as nombre_licencia, estSol.nombre AS estadoLicencia FROM solicitudLicencias l LEFT JOIN general g ON l.legajo=g.legajo LEFT JOIN tipo_licencias tl ON l.tipo_licencia=tl.id LEFT JOIN lugar lu ON g.lugaractu=lu.nrolugar LEFT JOIN estadoSolicitudLicencia estSol ON l.estado=estSol.id WHERE l.tipo_licencia=".$subcat." AND ".$str_carga." ".$str_filt_agentes." ORDER BY l.desde DESC, l.hora_solicitud DESC";
        }
        //echo $str_lic;
        $cons_lic = $conn->query($str_lic);
        while($obj = $cons_lic->fetch_object())
        {
            $estado = "";
            switch($obj->estado)
            {
                case 1: 
                    //$estado = "<span style='color: #d9534f;'>CANCELADA</span>";
                    $estado = "CANCELADA";
                    break;
                case 2: 
                    //$estado = "<span style='color: #f0ad4e;'>PENDIENTE</span>";
                    $estado = "PENDIENTE";
                    break;
                case 3: 
                    //$estado = "<span style='color: #5cb85c;'>CONFIRMADA</span>";
                    $estado = "CONFIRMADA";
                    break;
            }   
            
            echo "<tr>";
            echo "<td>".$estado."</td>";
            echo "<td>".$obj->nombre_licencia."</td>";
            echo "<td>".$obj->legajo."</td>";
            echo "<td>".$obj->apellido."</td>";
            echo "<td>".$obj->nombre."</td>";
            echo "<td>".$obj->nombrelugar."</td>";
            echo "<td>".$obj->direccion."</td>";
            echo "<td>".$obj->fdesde."</td>";
            echo "<td>".$obj->causa."</td>";  
            echo "<td>".$obj->observaciones."</td>";
            $pierde_preset = ($obj->pierde_presentismo==0)?'NO':'SI';
            echo "<td>".$pierde_preset."</td>";
            echo "<td>".$obj->expediente."</td>";
            echo "<td>".$obj->fecha_carga."</td>";
            echo "</tr>";
        }
        break;    
    case 'liquidacion':
        $str_liq = "SELECT cont_archivo_bsj.* ,general.legajo, general.padron, general.nrodoc, escalafon.nombre as escalafon FROM cont_archivo_bsj, general, cargos, escalafon WHERE cont_archivo_bsj.periodo='".$subcat."' AND cont_archivo_bsj.liquidacion=".$subsubcat." AND cont_archivo_bsj.tipo_acreditacion='02' AND general.legajo=cont_archivo_bsj.legajo AND general.cargoactu=cargos.nrocargo AND cargos.escalafon=escalafon.id ORDER BY cont_archivo_bsj.nombre ASC";
        switch($combo_liquidacion)
        {
            case 1: default: 
                $tipo_acred = "02";
                $cod_emp = "008";
                $mes_liq_ci = 0;
                $anio_liq_ci = 0;
                switch($subsubcat)
                {
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                        $mes_liq_ci = $subsubcat;
                        break;
                    case 8:
                    case 9:
                    case 10:
                    case 11:
                    case 12:
                        $mes_liq_ci = $subsubcat-1;
                        break;
                    case 7:   
                        $mes_liq_ci = 13;
                        break;  
                    case 13:   
                        $mes_liq_ci = 14;
                        break;    
                }
                $anio_liq_ci = substr($subcat, 2,2);
                
                if($fecha_desde != 0 && $fecha_hasta!=0 && $fecha_desde<=$fecha_hasta)
                {
                    $str_liq = "SELECT cont_archivo_bsj.* ,general.legajo, general.padron, general.nrodoc, escalafon.nombre as escalafon FROM cont_archivo_bsj, general, cargos, escalafon WHERE cont_archivo_bsj.periodo='".$subcat."' AND cont_archivo_bsj.liquidacion=".$subsubcat." AND cont_archivo_bsj.tipo_acreditacion=".$tipo_acred." AND cont_archivo_bsj.codigo_empresa=".$cod_emp." AND general.legajo=cont_archivo_bsj.legajo AND general.cargoactu=cargos.nrocargo AND cargos.escalafon=escalafon.id ".$filtro_fecha." ORDER BY cont_archivo_bsj.nombre ASC";
                }
                else
                {
                    $str_liq = "SELECT cont_archivo_bsj.* ,general.legajo, general.padron, general.nrodoc, escalafon.nombre as escalafon FROM cont_archivo_bsj, general, cargos, escalafon WHERE cont_archivo_bsj.periodo='".$subcat."' AND cont_archivo_bsj.liquidacion=".$subsubcat." AND cont_archivo_bsj.tipo_acreditacion=".$tipo_acred." AND cont_archivo_bsj.codigo_empresa=".$cod_emp." AND general.legajo=cont_archivo_bsj.legajo AND general.cargoactu=cargos.nrocargo AND cargos.escalafon=escalafon.id ORDER BY cont_archivo_bsj.nombre ASC";
                }
                $res_liq = $conn->query($str_liq);
                //echo $str_liq;      
                while($obj = $res_liq->fetch_object())
                {
                    $cuenta = substr($obj->cuenta_acreditacion,0, 7)."/".substr($obj->cuenta_acreditacion, -1);
                    $neto = floatval($obj->neto)/100;
                    $neto = "$".number_format($neto,2,",",".");
                    echo "<tr>";
                    echo "<td>".$cuenta."</td>";
                    echo "<td>".$neto."</td>";
                    echo "<td>".$obj->nombre."</td>";
                    echo "<td>".$obj->legajo."</td>";
                    echo "<td>".$obj->padron."</td>";
                    echo "<td>".$obj->nrodoc."</td>";
                    echo "<td>".$obj->escalafon."</td>";
                    echo '<td><a href="recibo_sueldo.php?leg='.$obj->legajo.'&anio_liq='.$anio_liq_ci.'&mes_liq='.$mes_liq_ci.'" target="_blank"><img src="../res/img/pdf.png" alt="Ver recibo"></img></a></td>';
                    echo "</tr>";
                }
                break;
            case 2: 
                $tipo_acred = "11";
                $cod_emp = "273";
                $str_liq = "SELECT cuenta_acreditacion, neto, nombre, documento  FROM cont_archivo_bsj WHERE periodo='".$subcat."' AND liquidacion=".$subsubcat." AND tipo_acreditacion=".$tipo_acred." AND codigo_empresa=".$cod_emp." ORDER BY nombre ASC";
                
                $res_liq = $conn->query($str_liq);

                while($obj = $res_liq->fetch_object())
                {
                    $cuenta = substr($obj->cuenta_acreditacion,0, 7)."/".substr($obj->cuenta_acreditacion, -1);
                    $neto = floatval($obj->neto)/100;
                    $neto = "$".number_format($neto,2,",",".");
                    echo "<tr>";
                    echo "<td>".$cuenta."</td>";
                    echo "<td>".$neto."</td>";
                    echo "<td>".$obj->nombre."</td>";
                    echo "<td>".$obj->documento."</td>";
                    echo "</tr>";
                }
                break;
            case 3:
                $tipo_acred = "xxx";
                break;
            /*default:
                $tipo_acred = "02";
                break;*/
                        
        }
        break;
    case 'recibo_sueldo':
        $cons_rec_final = $conn->query($str_rec);
        while($obj_rec_final = $cons_rec_final->fetch_object())
        {
            if(strlen(trim($obj_rec_final->anio_liq))==1)
            {
                $a = "0".$obj_rec_final->anio_liq;
            }
            else
            {
                $a = $obj_rec_final->anio_liq;
            }
            echo "<tr>";
            echo "<td>".$obj_rec_final->apellido."</td>";
            echo "<td>".$obj_rec_final->nombre."</td>";
            echo "<td>".$obj_rec_final->legajo."</td>";
            echo "<td>".$obj_rec_final->padron."</td>";
            echo "<td>20".$a."</td>";
            echo "<td>".$obj_rec_final->mes_liq_nombre."</td>";
            echo '<td><a href="recibo_sueldo.php?leg='.$obj_rec_final->legajo.'&anio_liq='.$obj_rec_final->anio_liq.'&mes_liq='.$obj_rec_final->mes_liq.'" target="_blank"><img src="../res/img/pdf.png" alt="Ver recibo"></img></a></td>';
            echo "</tr>";
        }
        break;    
    case 'listado_hijos':
        $nacidos = "";
        if($subcat!=0)
        {
            if($fecha_desde != 0 && $fecha_hasta != 0 && setFecha($fecha_desde)<=setFecha($fecha_hasta))
            {
                $nacidos.=" AND (fam_familiares.fecha_nac BETWEEN '".setFecha($fecha_desde)."' AND '".setFecha($fecha_hasta)."')";
            }

            if($fecha_desde2 != 0 && $fecha_hasta2 != 0 && setFecha($fecha_desde2)<=setFecha($fecha_hasta2))
            {
                $str_cert = "SELECT fam_certificados.*, fam_familiares.nombre as nombre_familiar, fam_familiares.apellido as apellido_familiar, fam_familiares.dni, fam_familiares.nivel_estudios, fam_familiares.discapacitado, general.nombre, general.apellido, general.legajo, general.susp_asignacion, nivel_estudios.nombre as nivel_estudios FROM fam_familiares LEFT JOIN fam_certificados ON fam_familiares.id=fam_certificados.id_familiar AND fam_certificados.anio='".$subcat."' LEFT JOIN general ON fam_familiares.legajo=general.legajo LEFT JOIN nivel_estudios ON fam_familiares.nivel_estudios=nivel_estudios.id OR nivel_estudios.id=null LEFT JOIN fam_relacion ON fam_familiares.relacion=fam_relacion.id WHERE general.activa=1 AND (fam_familiares.relacion=2 OR fam_familiares.relacion=7) AND (DATE_FORMAT(fecha_nac, '%m%d') BETWEEN DATE_FORMAT('".setFecha($fecha_desde2)."','%m%d') AND DATE_FORMAT('".setFecha($fecha_hasta2)."','%m%d')) AND TIMESTAMPDIFF(YEAR,fecha_nac,'".setFecha($fecha_hasta2)."')=18 ".$nacidos." ORDER BY fam_familiares.apellido, fam_familiares.nombre";
            }
            else
            {
                $str_cert = "SELECT fam_certificados.*, fam_familiares.nombre as nombre_familiar, fam_familiares.apellido as apellido_familiar, fam_familiares.dni, fam_familiares.nivel_estudios, fam_familiares.discapacitado, general.nombre, general.apellido, general.legajo, general.susp_asignacion, nivel_estudios.nombre as nivel_estudios FROM fam_familiares LEFT JOIN fam_certificados ON fam_familiares.id=fam_certificados.id_familiar AND fam_certificados.anio='".$subcat."' LEFT JOIN general ON fam_familiares.legajo=general.legajo LEFT JOIN nivel_estudios ON fam_familiares.nivel_estudios=nivel_estudios.id OR nivel_estudios.id=null LEFT JOIN fam_relacion ON fam_familiares.relacion=fam_relacion.id WHERE general.activa=1 AND (fam_familiares.relacion=2 OR fam_familiares.relacion=7) ".$nacidos." ORDER BY fam_familiares.apellido, fam_familiares.nombre";
            }
            //echo $str_cert;

        }
        $cons_cert = $conn->query($str_cert);
        while($obj_cert = $cons_cert->fetch_object())
        {
            $cert_inicio = $obj_cert->certificado_inicio==1?'Si':'No';
            $cert_fin = $obj_cert->certificado_fin==1?'Si':'No';
            $guardap = $obj_cert->guardapolvo==1?'Si':'No';
            $suspendido = $obj_cert->susp_asignacion==1?'Si':'No';
            $anio = $obj_cert->anio!=""?$obj_cert->anio:$subcat;
            $discapacitado = $obj_cert->discapacitado==1?'Si':'No';
            echo "<tr>";
            echo "<td>".$obj_cert->apellido."</td>";
            echo "<td>".$obj_cert->nombre."</td>";
            echo "<td>".$obj_cert->legajo."</td>";
            echo "<td>".$obj_cert->apellido_familiar.", ".$obj_cert->nombre_familiar."</td>";
            echo "<td>".$obj_cert->dni."</td>";
            echo "<td>".$obj_cert->nivel_estudios."</td>";
            echo "<td>".$discapacitado."</td>";
            echo "<td>".$cert_inicio."</td>";
            echo "<td>".$cert_fin."</td>";
            echo "<td>".$guardap."</td>";
            echo "<td>".$anio."</td>";
            echo "<td>".$suspendido."</td>";
            echo "</tr>";
        }
        break; 
    case "comisiones":
        if($fecha_desde != 0 && $fecha_hasta != 0 && setFecha($fecha_desde)<=setFecha($fecha_hasta))
        {
            $str_com = "SELECT g.legajo, g.apellido, g.nombre, DATE_FORMAT(c.desde, '%d-%m-%Y') as fdesde, DATE_FORMAT(c.hasta, '%d-%m-%Y') as fhasta, c.total_dias, c.causa, c.expediente, c.observaciones FROM comisiones c left join general g ON c.legajo=g.legajo WHERE (desde BETWEEN '".setFecha($fecha_desde)."' AND '".setFecha($fecha_hasta)."') OR (hasta BETWEEN '".setFecha($fecha_desde)."' AND '".setFecha($fecha_hasta)."') OR ('".setFecha($fecha_desde)."' BETWEEN desde AND hasta) OR ('".setFecha($fecha_hasta)."' BETWEEN desde AND hasta) ".$str_filt_agentes." ORDER BY desde DESC";
        }
        else
        {
            $str_com = "SELECT g.legajo, g.apellido, g.nombre, DATE_FORMAT(c.desde, '%d-%m-%Y') as fdesde, DATE_FORMAT(c.hasta, '%d-%m-%Y') as fhasta, c.total_dias, c.causa, c.expediente, c.observaciones FROM comisiones c left join general g ON c.legajo=g.legajo ".$str_filt_agentes." ORDER BY desde DESC";
        }
        //echo $str_com;
        $cons_com = $conn->query($str_com);
        while($obj = $cons_com->fetch_object())
        {
            echo "<tr>";
            echo "<td>".$obj->legajo."</td>";
            echo "<td>".$obj->apellido."</td>";
            echo "<td>".$obj->nombre."</td>";
            echo "<td>".$obj->fdesde."</td>";
            echo "<td>".$obj->fhasta."</td>";
            echo "<td>".$obj->total_dias."</td>";
            echo "<td>".$obj->causa."</td>";  
            echo "<td>".$obj->expediente."</td>";  
            echo "<td>".$obj->observaciones."</td>";
            echo "</tr>";
        }
        break;  
    case "capacitaciones":
        if($fecha_desde != 0 && $fecha_hasta != 0 && setFecha($fecha_desde)<=setFecha($fecha_hasta))
        {
            $str_com = "SELECT g.legajo, g.apellido, g.nombre, t.nombre as tipo_cap, c.nombre as nombre_cap, DATE_FORMAT(c.fecha, '%d-%m-%Y') as fecha_cap, c.cant_horas, m.nombre as modalidad_cap, o.nombre as origen_cap, c.inherencia, c.evaluacion, c.nota_evaluacion FROM capacitaciones c LEFT JOIN general g ON c.legajo=g.legajo LEFT JOIN capacitacionestipo t ON c.tipo=t.id LEFT JOIN capacitacionesorigen o ON c.origen=o.id LEFT JOIN capacitacionesmodalidad m ON c.modalidad=m.id WHERE (fecha BETWEEN '".setFecha($fecha_desde)."' AND '".setFecha($fecha_hasta)."') ".$str_filt_agentes." ORDER BY fecha DESC";
        }
        else
        {
            $str_com = "SELECT g.legajo, g.apellido, g.nombre, t.nombre as tipo_cap, c.nombre as nombre_cap, DATE_FORMAT(c.fecha, '%d-%m-%Y') as fecha_cap, c.cant_horas, m.nombre as modalidad_cap, o.nombre as origen_cap, c.inherencia, c.evaluacion, c.nota_evaluacion FROM capacitaciones c LEFT JOIN general g ON c.legajo=g.legajo LEFT JOIN capacitacionestipo t ON c.tipo=t.id LEFT JOIN capacitacionesorigen o ON c.origen=o.id LEFT JOIN capacitacionesmodalidad m ON c.modalidad=m.id ".$str_filt_agentes." ORDER BY fecha DESC";
        }
        //echo $str_com;
        $cons_com = $conn->query($str_com);
        while($obj = $cons_com->fetch_object())
        {
            $ev = $obj->evaluacion==1?"Si":"No";
            echo "<tr>";
            echo "<td>".$obj->legajo."</td>";
            echo "<td>".$obj->apellido."</td>";
            echo "<td>".$obj->nombre."</td>";
            echo "<td>".$obj->nombre_cap."</td>";
            echo "<td>".$obj->tipo_cap."</td>";
            echo "<td>".$obj->fecha_cap."</td>";
            echo "<td>".$obj->cant_horas."</td>";
            echo "<td>".$obj->modalidad_cap."</td>";  
            echo "<td>".$obj->origen_cap."</td>";  
            echo "<td>".$obj->inherencia."</td>";
            echo "<td>".$ev."</td>";
            echo "<td>".$obj->nota_evaluacion."</td>";
            echo "</tr>";
        }
        break;     
    case 'listado_certificados_empleados':
        if($subcat!=0)
        {
            $str_cert_emp = "SELECT e.*, g.nombre, g.apellido, g.legajo, g.padron FROM emp_certificados e LEFT JOIN general g ON e.legajo=g.legajo WHERE e.anio='".$subcat."' AND g.activa=1 AND g.susp_asignacion=0 ORDER BY g.apellido, g.nombre";

        }
        else
        {
            $str_cert_emp = "SELECT e.*, g.nombre, g.apellido, g.legajo, g.padron FROM emp_certificados e LEFT JOIN general g ON e.legajo=g.legajo WHERE g.activa=1 AND g.susp_asignacion=0 ORDER BY g.apellido, g.nombre";
        }
        $cons_cert_emp = $conn->query($str_cert_emp);
        while($obj_cert_emp = $cons_cert_emp->fetch_object())
        {
            $cert_asign_fam = $obj_cert_emp->asignacion_familiar==1?'Si':'No';
            $cert_anses = $obj_cert_emp->cert_neg_anses==1?'Si':'No';
            $cert_conyuge = $obj_cert_emp->declaracion_conyuge==1?'Si':'No';
            echo "<tr>";
            echo "<td>".$obj_cert_emp->apellido."</td>";
            echo "<td>".$obj_cert_emp->nombre."</td>";
            echo "<td>".$obj_cert_emp->legajo."</td>";
            echo "<td>".$obj_cert_emp->padron."</td>";
            echo "<td>".$cert_asign_fam."</td>";
            echo "<td>".$cert_anses."</td>";
            echo "<td>".$cert_conyuge."</td>";
            echo "<td>".$obj_cert_emp->anio."</td>";
            echo "</tr>";
        }
        break;  
    case "sanciones":
        if($fecha_desde != 0 && $fecha_hasta != 0 && setFecha($fecha_desde)<=setFecha($fecha_hasta))
        {
            $str_com = "SELECT g.legajo, g.apellido, g.nombre, DATE_FORMAT(s.desde, '%d-%m-%Y') as fecha_desde, DATE_FORMAT(s.hasta, '%d-%m-%Y') as fecha_hasta, s.dispuesta_por, s.expediente, s.observaciones, t.nombre as tipo_sancion FROM sanciones s LEFT JOIN general g ON s.legajo=g.legajo LEFT JOIN sancionestipo t ON s.tipo=t.id WHERE ((desde BETWEEN '".setFecha($fecha_desde)."' AND '".setFecha($fecha_hasta)."') OR (hasta BETWEEN '".setFecha($fecha_desde)."' AND '".setFecha($fecha_hasta)."') OR ('".setFecha($fecha_desde)."' BETWEEN desde AND hasta) OR ('".setFecha($fecha_hasta)."' BETWEEN desde AND hasta)) ".$str_filt_agentes." ORDER BY desde DESC";
        }
        else
        {
            $str_com = "SELECT g.legajo, g.apellido, g.nombre, DATE_FORMAT(s.desde, '%d-%m-%Y') as fecha_desde, DATE_FORMAT(s.hasta, '%d-%m-%Y') as fecha_hasta, s.dispuesta_por, s.expediente, s.observaciones, t.nombre as tipo_sancion FROM sanciones s LEFT JOIN general g ON s.legajo=g.legajo LEFT JOIN sancionestipo t ON s.tipo=t.id WHERE 1=1 ".$str_filt_agentes." ORDER BY desde DESC";
        }
        //echo $str_com;
        $cons_com = $conn->query($str_com);
        while($obj = $cons_com->fetch_object())
        {
            echo "<tr>";
            echo "<td>".$obj->legajo."</td>";
            echo "<td>".$obj->apellido."</td>";
            echo "<td>".$obj->nombre."</td>";
            echo "<td>".$obj->tipo_sancion."</td>";
            echo "<td>".$obj->fecha_desde."</td>";
            echo "<td>".$obj->fecha_hasta."</td>";
            echo "<td>".$obj->dispuesta_por."</td>";
            echo "<td>".$obj->expediente."</td>";
            echo "<td>".$obj->observaciones."</td>";  
            echo "</tr>";
        }
        break;                             
    default: //todos los demas reportes
        if($cat!=0)
        {
            $result = mysqli_query($conn, $str);
            $nbrows = mysqli_num_rows($result);
            $rows = array();

            while ($row = mysqli_fetch_row($result))
            {
                $datos = getDataEmployee($row[0]);

                $tot_reg++;
                $cont=count($row);
                echo '<tr data-toggle="popover" data-trigger="hover" data-placement="auto" data-html="true" title="'.$row[1].', '.$row[2].'" data-content="'.$datos.'" href="">';
                for ($i=0; $i <= count($row)-1; $i++) 
                {
                    echo "<td>".$row[$i]."</td>";                    
                }
                echo "</tr>";
            }
        }
        break;
}


echo '</tbody>';
echo $foot;
echo "</table>";
echo '</div>'; //end div class=tabla

function getDataEmployee($legajo)
{
    global $conn;
    $data = "";
    $lugares = "";
    $cargo = "";
    $nombre = "";
    $anio_actual = date("Y");
    $hoy = date("Y-m-d");
    $fecha_ingreso = "";
    $fecha_nac = "";
    $licencia = 'Empleado presente';
    $str_emp = "SELECT general.nombre, general.apellido, general.fecha_carga, general.fecnac, cargos.cargo, histolugar.desde, histolugar.hasta, lugar.nombrelugar FROM general, cargos, histolugar, lugar WHERE general.cargoactu=cargos.nrocargo AND general.legajo=histolugar.legajo AND general.legajo=histolugar.legajo AND histolugar.nrolugar=lugar.nrolugar AND general.legajo=".$legajo." ORDER BY histolugar.desde ASC";
    $cons_emp = $conn->query($str_emp);
    while($obj_emp = $cons_emp->fetch_object())
    {
        $nombre = $obj_emp->apellido.", ".$obj_emp->nombre;
        $cargo = $obj_emp->cargo;

        $fecha_ingreso = new DateTime($obj_emp->fecha_carga);
        $fecha_ingreso = $fecha_ingreso->format("d-m-Y");
        $fecha_nac = new DateTime($obj_emp->fecnac);
        $fecha_nac = $fecha_nac->format("d-m-Y");
        if($obj_emp->hasta==null)
        {
            $hasta = "Actualmente";
        }
        else
        {
            $hasta = setFecha($obj_emp->hasta);
        }
        $lugares.= "(".setFecha($obj_emp->desde)." - ".$hasta.") ".$obj_emp->nombrelugar."<br>";
    }

    //Informo si el empleado tiene alguna licencia al dia de hoy
    $tablas_licencias = ["lic_salud", "lic_salud_lt", "lic_salud_fam", "lic_salud_at", "lic_maternidad", "lic_fallecimiento", "lic_comp", "lic_culturales", "lic_gremiales", "lic_deportivas", "lic_electivas", "lic_estudio", "lic_extraordinaria", "lic_matrimonio", "lic_nacimiento", "lic_particular", "lic_singoce", "lic_doble_turno"];
    $nombres_licencias = ["Salud personal", "Salud largo tratamiento", "Salud familiar", "Salud accidente de trabajo", "Maternidad", "Fallecimiento", "Compensatoria", "Cultural", "Gremial", "Deportiva", "Electiva", "Estudio", "Extrordinaria", "Matrimonio", "Nacimiento", "Particular", "Sin goce", "Doble turno"];
    

    for($i=0;$i<count($tablas_licencias);$i++)
    {
        $cons_lic = "SELECT * FROM ".$tablas_licencias[$i]." WHERE legajo=".$legajo." AND goce='S' AND ('".$hoy."' BETWEEN desde AND hasta)";
        $res_lic = $conn->query($cons_lic);
        if($res_lic->num_rows>0)
        {
            $obj_lic = $res_lic->fetch_object();
            $licencia="Empleado actualmente con licencia ".$nombres_licencias[$i]." (".setFecha($obj_lic->desde)." - ".setFecha($obj_lic->hasta).")";
        }
    }

    $data = "Cargo actual: ".$cargo."<br>"."Fecha nacimiento: ".$fecha_nac."<br>Ingreso: ".$fecha_ingreso."<br>".$lugares."<br>".$licencia;
    return $data;
}
?>
</body>
</html>

<script type="text/javascript" src="../res/DataTablesV2/jQuery-1.12.3/jquery-1.12.3.js"></script>


<!--<script type="text/javascript" src="../res/js/bootstrapValidator.min.js"></script>
<script type="text/javascript" src="../res/js/moment-with-locales.js"></script>-->
<!--<script type="text/javascript" src="../res/js/popper.min.js"></script>-->
<!--<script type="text/javascript" src="../res/js/bootstrap4.min.js"></script>-->
<script type="text/javascript" src="../res/css/bootstrap-3.3.7-dist/js/bootstrap.js"></script>

<script type="text/javascript" src="../res/js/bootstrap-select.js"></script>


<script type="text/javascript" src="../res/DataTablesV2/datatables.min.js"></script>

<script type="text/javascript" src="../res/DataTablesV2/jquery.dataTables.yadcf.js"></script>

<script type="text/javascript" src="../res/DataTablesV2/Buttons-1.2.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="../res/DataTablesV2/Buttons-1.2.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="../res/DataTablesV2/Buttons-1.2.2/js/buttons.print.min.js"></script>
<script type="text/javascript" src="../res/DataTablesV2/Buttons-1.2.2/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="../res/js/bootstrap-datepicker.js" charset="UTF-8"></script>
<script type="text/javascript" src="../res/js/bootstrap-datepicker.es.js" charset="UTF-8"></script>
<script src="../res/js/moment.min.js"></script>
<script type="text/javascript" src="../res/js/datetime-moment.js"></script>
<script>
$(function()
{

    $('#fecha_desde').datepicker({
      format: 'dd-mm-yyyy',
      autoclose: true,
      language: 'es'
    }).on('changeDate show', function(e) {
        console.log("cambio");
        // Revalidar nuevamente el campo
        //$('#frmReportes').bootstrapValidator('revalidateField', 'fecha_desde');
        //jQuery.noConflict();
        //$('#fecha_hasta').datepicker('setStartDate', e.date);
    });
    
    $('#fecha_hasta').datepicker({
      format: 'dd-mm-yyyy',
      autoclose: true,
      language: 'es'
    }).on('changeDate show', function(e) {
        // Revalidar nuevamente el campo
        //$('#frmReportes').bootstrapValidator('revalidateField', 'fecha_hasta');
        //$('#fecha_desde').datepicker('setEndDate', e.date);
    });

    $('#fecha_desde3').datepicker({
      format: 'dd-mm-yyyy',
      autoclose: true,
      language: 'es'
    }).on('changeDate show', function(e) {
        console.log("cambio");
        // Revalidar nuevamente el campo
        //$('#frmReportes').bootstrapValidator('revalidateField', 'fecha_desde');
        //jQuery.noConflict();
        //$('#fecha_hasta').datepicker('setStartDate', e.date);
    });
    
    $('#fecha_hasta3').datepicker({
      format: 'dd-mm-yyyy',
      autoclose: true,
      language: 'es'
    }).on('changeDate show', function(e) {
        // Revalidar nuevamente el campo
        //$('#frmReportes').bootstrapValidator('revalidateField', 'fecha_hasta');
        //$('#fecha_desde').datepicker('setEndDate', e.date);
    });

    $('#fecha_desde2').datepicker({
      format: 'dd-mm-yyyy',
      autoclose: true,
      language: 'es'
    }).on('changeDate show', function(e) {
        
        
    });
    
    $('#fecha_hasta2').datepicker({
      format: 'dd-mm-yyyy',
      autoclose: true,
      language: 'es'
    }).on('changeDate show', function(e) {
        
    });

    
    $.fn.dataTable.moment('DD-MM-YYYY');
    //Script para la tabla
    var table = $('#tablaReportes').DataTable({
        info: true,
        lengthMenu: [[50, 100, 500, -1], [50, 100, 500, "Todos"]],
        displayLength: -1,
        dom: 'Blfrtip',
        order: [],

        buttons: [
            {
                name: 'expExcel',
                extend: 'excel',
                text: "Exportar a Excel",
                filename: 'listado',
                extension: '.xlsx',
                className: 'red',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                text: 'Imprimir',
                extend: 'print',
                title: $(document.body).find('h2').html().toString(),
                message: function(api, boton, objeto){
                        console.log(api.page.info());
                        if($('#neto_total').length>0)
                        {
                            console.log($('#neto_total').html());
                            return "Cantidad de registros: "+api.page.info().recordsDisplay+" | PERIODO: "+$('#periodo_liq').html()+" | NETO TOTAL: "+$('#neto_total').html().toString();
                        }
                        else
                        {
                            return "Cantidad de registros: "+api.page.info().recordsDisplay;
                        }
                    
                },
                exportOptions: {
                    columns: ':visible'
                },
                customize: function (win) {
                    /*$(win.document.body)
                        .css( 'font-size', '10pt' )
                        .prepend(
                            '<img src="" style="position:absolute; top:0; left:0;" />'
                        );
                    */
                    /*$(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );*/
                   $(win.document.body).find('h1').css('text-align','center');
                   $(win.document.body).find('h1').css('font-size','15pt');
                   }
            },
            {
                extend: 'colvis',
                text: "Visibilidad columnas"
            }],
        
    language: {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ning&uacute;n dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
        //Este plugin crea listas abajo de cada columna, para filtrar por un valor en particular (simil autofilter de Excel)
    initComplete: function () 
    {
        var esto = this;
        this.api().columns().every(function () 
        {
            var column = this;
            var select = $('<select><option value="">Mostrar todos</option></select>')
                .appendTo( $(column.footer()).empty() )
                .on( 'change', function () {
                    var val = esto.dataTable.util.escapeRegex($(this).val());
                    column
                        .search( val ? '^'+val+'$' : '', true, false )
                        .draw();
                } );
            column.data().unique().sort().each( function ( d, j ) 
            {
                //console.log(d);
                select.append('<option value="'+d+'">'+d+'</option>');
            });
        });
    },
    drawCallback: function( settings ) {
    var api = this.api();
    
    // Output the data for the visible rows to the browser's console
    console.log(api.page.info().recordsDisplay);
    //$(document.body).find('h3').html("Total: "+api.page.info().recordsDisplay);
    }
    });
    //yadcf.init(table,[{column_number: 0 }]);
    
    $("#subcat").attr('disabled', true);

    function ejecutar(obj1, obj2, task, rep) 
    {
        //alert($(obj1).val())
        $('<img />', {
        class: 'loading',
        src: '../../lib/ext-4.1.1a/docs/extjs/resources/themes/images/default/tree/loading.gif',
        style: 'display:inline'
        }).insertAfter(obj1);

        $.ajax({
        type: "POST",
        url: "select_reportes.php",
        dataType: "html",
        data: "rep="+rep+"&task=" + task + "&id=" + $(obj1).val(),
        success: function(msg) {
        
        $(obj1).next('img').remove();
        $(obj2).html(msg).attr("disabled", false);
        jQuery.noConflict();
        $(obj2).selectpicker("refresh");
        
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log("entra al error: "+textStatus);
        $(obj1).next('img').remove();
        alert("Error al ejecutar = " + textStatus + " – " + errorThrown);
        }
        });
    }

    $("#cbReportes").change(function()
    {
        console.log("cambio reporrtes");
        //Muestro los campos de fecha solo para reportes general, bajas, hijos y licencias
        if($("#cbReportes").val()==15 || $("#cbReportes").val()==9 || $("#cbReportes").val()==10 || $("#cbReportes").val()==19 || $("#cbReportes").val()==21 || $("#cbReportes").val()==23 || $("#cbReportes").val()==25 || $("#cbReportes").val()==26 || $("#cbReportes").val()==27)
        {
            $('#fecha_desde').attr('disabled', false);
            $('#fecha_hasta').attr('disabled', false);
            $('#campos_fecha').show(500, 'swing');
            //$('#campos_fecha').attr('style', 'display:block');
        }
        else
        {
            $('#fecha_desde').attr('disabled', true);
            $('#fecha_hasta').attr('disabled', true);
            $('#campos_fecha').hide(500, 'swing');
        }
        if($("#cbReportes").val()==15 || $("#cbReportes").val()==26)
        {
            $('#fecha_desde3').attr('disabled', false);
            $('#fecha_hasta3').attr('disabled', false);
            $('#campos_fecha3').show(500, 'swing');
            //$('#campos_fecha').attr('style', 'display:block');
        }
        else
        {
            $('#campos_fecha3').hide(500, 'swing');
            $('#fecha_desde3').attr('disabled', true);
            $('#fecha_hasta3').attr('disabled', true);
        }

        if($("#cbReportes").val()==19)
        {
            $('#campos_fecha2').show(500, 'swing');
            //$('#campos_fecha').attr('style', 'display:block');
        }
        else
        {
            $('#campos_fecha2').hide(500, 'swing');
        }

        //Muestro campo de tipo de liquidacion para los reportes de liquidaciones del BSJ
        if($("#cbReportes").val()==16)
        {
            $('#combo_tipo_liquidacion').attr('disabled', false);
            $('#campo_tipo_liquidacion').show(500, 'swing');
            
        }
        else
        {
            $('#combo_tipo_liquidacion').attr('disabled', true);
            $('#campo_tipo_liquidacion').hide(500, 'swing');
        }


        


        $("#subcat").attr('disabled', true);
        $("#subsubcat").attr('disabled', true);
        $("#subsubcat").selectpicker('refresh');
        if ($(this).val().trim() != "") 
        {
            ejecutar($(this), $("#subcat"), "getSubcat");
        }
        else
        {
            $("#subcat").val("Seleccione subcategor&iacute;a");
            jQuery.noConflict();
            $("#subcat").selectpicker('refresh');
        }
    });
    jQuery.noConflict();
    $("#subsubcat").change(function()
    {
        console.log("hola");
        //Muestro campo de periodo de liquidacion para los reportes de liquidaciones del BSJ vacaciones proporcionales
        if($("#cbReportes").val()==16 && $("#subsubcat").val()==18)
        {
            $('#fecha_desde').attr('disabled', false);
            $('#fecha_hasta').attr('disabled', false);
            $('#campos_fecha').show(500, 'swing');
            
        }
        else
        {
            $('#fecha_desde').attr('disabled', true);
            $('#fecha_hasta').attr('disabled', true);
            $('#campos_fecha').hide(500, 'swing');
        }
    });

    $("#mayor_edad").change(function()
    {
        if(this.checked)
        {
            $('#fecha_desde2').attr('disabled', false);
            $('#fecha_hasta2').attr('disabled', false);
        }
        else
        {
            $('#fecha_desde2').attr('disabled', true);
            $('#fecha_hasta2').attr('disabled', true);
        }
    });

    jQuery.noConflict();
    $('#subcat').on('change', function () 
    {
        
        $("#subsubcat").attr('disabled', true);
        if ($(this).val().trim() != "" && ($("#cbReportes").val()=="1" || $("#cbReportes").val()=="16") || $("#cbReportes").val()=="20" || $("#cbReportes").val()=="22") 
        {

            ejecutar($(this), $("#subsubcat"), "getSubSubcat", $("#cbReportes").val());
        }
        else
        {
            $("#subsubcat").val("Seleccione sub subcategor&iacute;a");
            jQuery.noConflict();
            $("#subsubcat").selectpicker('refresh');
        }
    
    });


    jQuery.noConflict();
    $("subcat").selectpicker("refresh");
    /*$('#subsubcat').on('change', function () 
    {
        
        //Muestro campo de tipo de liquidacion para los reportes de liquidaciones del BSJ
        if($("#cbReportes").val()==16)
        {
            $('#combo_tipo_liquidacion').attr('disabled', false);
            $('#campo_tipo_liquidacion').show(500, 'swing');
            
        }
        else
        {
            $('#combo_tipo_liquidacion').attr('disabled', true);
            $('#campo_tipo_liquidacion').hide(500, 'swing');
        }
    });*/
    

    $('button[name=ver_evaluacion]').on('click', function(e)
    {
        var link = $(e.currentTarget);
        //console.log(link);
        
        var idEval = link.data('ideval');
        var idEvalPer = link.data('idevalper');
        var nombre = link.data('nombre');
        var motivo = link.data('motivo');
        var fecha_inicio = link.data('fecha_inicio');
        var fecha_fin = link.data('fecha_fin');
        var nombre_evaluador = link.data('nombre_evaluador');
        var nombre_evaluado = link.data('nombre_evaluado');
        var promedio = link.data('promedio');
        var aplazos = link.data('aplazos');
        var observaciones = link.data('observaciones');
        var tardanzas = link.data('tardanzas');
        var licencias = link.data('licencias');
        var antiguedad_cargo = 5*link.data('antiguedad_cargo')/100;
        var antiguedad_escalafon = 2*Math.round(link.data('antiguedad_escalafon')/5)/100;
        var asistencia_puntualidad = link.data('asistencia_puntualidad');
        var conducta = link.data('conducta');
        var capacitacion = link.data('capacitacion');
        var puntaje_total = link.data('puntaje_total');

        $.ajax({
            type: "GET",
            url: "getItemsEvaluacion.php",
            dataType: "html",
            data: "idEvaluacion="+idEvalPer,
            success: function(msg) {
            res = $.parseJSON(msg);
            var headerTable = '<tr><th>Nombre item</th><th>Descripci&oacute;n</th><th>Nota</th></tr>';
            var bodyTable = '';
            
            $.each(res, function(index, item)
            {
                //console.log(item.nombre+' '+item.nota_item);
                if(item.nota_item<4)
                {
                    bodyTable+='<tr class="danger"><td>'+item.nombre+'</td><td>'+item.descripcion+'</td><td>'+item.nota_item+'</td></tr>';    
                }
                else
                {
                    bodyTable+='<tr><td>'+item.nombre+'</td><td>'+item.descripcion+'</td><td>'+item.nota_item+'</td></tr>';    
                }
                
                
            });
            var tablaItems = headerTable+bodyTable;
            console.log(bodyTable);
            modal.find('.modal-body #items').html(tablaItems);
            
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("entra al error: "+textStatus);
            
            }
        });
        
        //Calculo el puntaje total!!
        var asist_puntualidad = 100;
        if(licencias==0)
        {
            asist_puntualidad = 100;    
        }
        else
        {
            if(licencias>0 && licencias<=5)
            {
                asist_puntualidad = 70;
            }
            else
            {
                if(licencias>5 && licencias<=10)
                {
                    asist_puntualidad = 40;
                }
                else
                {
                    if(licencias>10 && licencias<=15)
                    {
                        asist_puntualidad = 10;
                    }
                    else
                    {
                        asist_puntualidad = 0;
                    }
                }
            }
        }
        asist_puntualidad = asist_puntualidad - (tardanzas*2);
        asist_puntualidad = asist_puntualidad/100;
        
        //Cargo todos los datos en cada div del modal  
        var modal = $('#modalEvaluacion');
        modal.find('.modal-title').text(nombre);

        modal.find('.modal-body #evaluador').text('Evaluador: '+nombre_evaluador);
        modal.find('.modal-body #evaluado').text('Evaluado: '+nombre_evaluado);
        modal.find('.modal-body #fecha_inicio').text('Fecha inicio: '+fecha_inicio);
        modal.find('.modal-body #fecha_fin').text('Fecha fin: '+fecha_fin);
        modal.find('.modal-body #motivo').text('Motivo: '+motivo);
        modal.find('.modal-body #promedio').text('Promedio: '+promedio);
        modal.find('.modal-body #aplazos').text('Aplazos: '+aplazos);
        modal.find('.modal-body #observaciones').text('Observaciones: '+observaciones);
        modal.find('.modal-body #tardanzas').text('Tardanzas: '+tardanzas);
        modal.find('.modal-body #licencias').text('Licencias tomadas: '+licencias);
        var text_conducta = conducta!=0?'Conducta: -'+conducta:'Conducta: '+conducta;
        modal.find('.modal-body #conducta').text(text_conducta);
        modal.find('.modal-body #capacitacion').text('Capacitacion: '+capacitacion);
        modal.find('.modal-body #asistencia_puntualidad').text('Asistencia y puntualidad: '+asist_puntualidad);
        modal.find('.modal-body #antiguedad_categoria').text('Antiguedad en la categoria: '+antiguedad_cargo);
        modal.find('.modal-body #antiguedad_escalafon').text('Antiguedad en el escalafon: '+antiguedad_escalafon);
        modal.find('.modal-body #puntaje_total').text('Puntaje total: '+puntaje_total);
        modal.find('.modal-body #idEval').val(idEval);
        modal.find('.modal-body #idEvalPers').val(idEvalPer);
        //jQuery.noConflict()
        //$('#modalEvaluacion').modal('show');
    });

    $('#printEvaluacion').on('click', function(e)
    {
        window.open("verEvaluacion.php?evalId="+$('#idEvalPers').val(),"_blank");
    });
     
});

$(document).ready(function(){
//Inicializo popover
    $('[data-toggle="popover"]').popover({
        delay: { "show": 1000, "hide": 100 }
    });

    //tecla enter para generar reporte
    $(document).keypress(function(e){
        var key = e.which;
        if(key == 13) //Tecla enter
        {
            console.log("se hizo click en enter");
            $("#botonGenerarReporte").click();
        }
    
    });
});


</script>
