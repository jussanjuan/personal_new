<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">


<link rel="stylesheet" type="text/css" href="../res/DataTables/datatables.min.css"/>
<link rel="stylesheet" type="text/css" href="../res/css/estilos.css"/>
<script type="text/javascript" src="../res/DataTables/datatables.min.js"></script>
<script type="text/javascript" src="../res/DataTables/jQuery-1.12.3/jquery-1.12.3"></script>
<script>
    $(document).ready(function(){
    $('#tablaReportes').DataTable();
});
</script>
</head>
<body>
<?php
require('system.config.php');
?>
<form name="frmReportes" action="reportes.php" method="get">
<select name="cbReportes" onchange="javascript: location=document.frmReportes.cbReportes.options[document.frmReportes.cbReportes.selectedIndex].value">
	<option value="reportes.php">--Seleccione una opcion--</option>
	<option value="reportes.php?t=1">Listado por Organismo</option>
	<option value="reportes.php?t=2">Listado por Cargos</option>
	<option value="reportes.php?t=3">Listado de Magistrados</option>
	<option value="reportes.php?t=4">Listado de Funcionarios</option>
	<option value="reportes.php?t=5">Listado de Empleados Abogados</option>
	<option value="reportes.php?t=6">Listado General</option>
</select>
<input type="button" name="imprimir" value="Imprimir" onclick="window.print();">
</form>
<?php

$op = isset($_GET["t"])?$_GET["t"]:0;
switch ($op) {
	case 1:
		echo "<h2>Listado por Organismo</h2>";
		// Listado de agentes por organismos ordenado por lugar y cargo
		$str="SELECT l.nombrelugar, g.apellido, g.nombre, c.cargo
		FROM general g 
		RIGHT JOIN lugar l ON
		l.nrolugar=g.lugaractu
		LEFT JOIN cargos c ON
		g.cargoactu=c.nrocargo
		ORDER BY l.nrolugar, c.nrocargo";
		$cabecera = "<thead><tr><th>Lugar</th><th>Apellido</th><th>Nombre</th><th>Cargo</th></tr></thead>";
		break;
	case 2:
		echo "<h2>Listado por Cargos</h2>";
		// Listado por cargos (legajo, fecha ingreso, ultimo ascenso, dependencia)
		$str="SELECT g.legajo, g.apellido, g.nombre, c.cargo, l.nombrelugar, DATE_FORMAT(tabla1.ultimo_ascenso, '%d-%m-%Y') AS xultimo_ascenso, DATE_FORMAT(tabla2.fecha_ingreso, '%d-%m-%Y') AS xfecha_ingreso  
		FROM general g 
		LEFT JOIN (SELECT h.legajo, MAX( h.desde ) as ultimo_ascenso FROM histocargo h GROUP BY h.legajo) as tabla1 ON
		g.legajo=tabla1.legajo
		LEFT JOIN (SELECT h.legajo, MIN(h.desde) as fecha_ingreso FROM histocargo h GROUP BY h.legajo) as tabla2 ON
		g.legajo=tabla2.legajo 
		RIGHT JOIN cargos c ON
		g.cargoactu=c.nrocargo
		LEFT JOIN lugar l ON
		g.lugaractu=l.nrolugar
		ORDER BY c.nrocargo";
		$cabecera = "<thead><tr><th>Legajo</th><th>Apellido</th><th>Nombre</th><th>Cargo</th><th>Lugar</th><th>Ascenso</th><th>Ingreso</th></tr></thead>";
		break;
	case 3:
		echo "<h2>Listado de Magistrados</h2>";
		$str = "SELECT g.legajo, g.apellido, g.nombre, c.cargo  
		FROM general g 
		LEFT JOIN cargos c ON
		g.cargoactu=c.nrocargo 
		WHERE g.activa=1 AND g.cargoactu IN (1,2,3,4,8,9,13)
		ORDER BY c.nrocargo ASC ";
		$cabecera = "<thead><tr><th>Legajo</th><th>Apellido</th><th>Nombre</th><th>Cargo</th></tr></thead>";
		break;
	case 4:
		echo "<h2>Listado de Funcionarios</h2>";
		$str = "SELECT g.legajo, g.apellido, g.nombre, c.cargo  
		FROM general g 
		LEFT JOIN cargos c ON
		g.cargoactu=c.nrocargo 
		WHERE g.activa=1 AND g.cargoactu IN (5,6,7,10,11,14,15,20,22,24,25,26,27,28,32,60,61,62,63,64,234)
		ORDER BY c.nrocargo ASC ";
		$cabecera = "<thead><tr><th>Legajo</th><th>Apellido</th><th>Nombre</th><th>Cargo</th></tr></thead>";
		break;
	case 5:
		echo "<h2>Listado de Empleados Abogados</h2>";
		$str="SELECT DISTINCT t.legajo, g.apellido, g.nombre, te.titulo FROM titulo t 
		INNER JOIN general g ON (t.legajo=g.legajo) AND (t.codestud=6) 
		LEFT JOIN tipo_estudio te ON (t.codestud=te.codestudio) 
		ORDER BY g.apellido ASC";
		$cabecera = "<thead><tr><th>Legajo</th><th>Apellido</th><th>Nombre</th><th>Titulo</th></tr></thead>";
		break;
        case 6; default: 
		// Listado general (legajo-ultimo ascenso-dependencia-cargo)
		echo "<h2>Listado General</h2>";
		/*$str="SELECT g.legajo, g.apellido, g.nombre, c.cargo, l.nombrelugar, DATE_FORMAT(tabla1.ultimo_ascenso, '%d-%m-%Y') AS xultimo_ascenso, DATE_FORMAT(tabla2.fecha_ingreso, '%d-%m-%Y') AS xfecha_ingreso  
		FROM general g 
		LEFT JOIN (SELECT h.legajo, MAX( h.desde ) as ultimo_ascenso FROM histocargo h GROUP BY h.legajo) as tabla1 ON
		g.legajo=tabla1.legajo
		LEFT JOIN (SELECT h.legajo, MIN(h.desde) as fecha_ingreso FROM histocargo h GROUP BY h.legajo) as tabla2 ON
		g.legajo=tabla2.legajo 
		INNER JOIN cargos c ON
		g.cargoactu=c.nrocargo
		LEFT JOIN lugar l ON
		g.lugaractu=l.nrolugar
		ORDER BY g.apellido ASC"; */
		$str="SELECT g.legajo, g.apellido, g.nombre, c.cargo, l.nombrelugar, DATE_FORMAT(tabla1.ultimo_ascenso, '%d-%m-%Y') AS xultimo_ascenso, DATE_FORMAT(tabla2.fecha_ingreso, '%d-%m-%Y') AS xfecha_ingreso  
		FROM general g 
		LEFT JOIN (SELECT h.legajo, MAX( h.desde ) as ultimo_ascenso FROM histocargo h GROUP BY h.legajo) as tabla1 ON
		g.legajo=tabla1.legajo
		LEFT JOIN (SELECT h.legajo, MIN(h.desde) as fecha_ingreso FROM histocargo h GROUP BY h.legajo) as tabla2 ON
		g.legajo=tabla2.legajo 
		RIGHT JOIN cargos c ON
		g.cargoactu=c.nrocargo
		LEFT JOIN lugar l ON
		g.lugaractu=l.nrolugar
		WHERE g.activa=1 
		ORDER BY g.apellido ASC";
            


/*select g.legajo, c.cargo, g.apellido, g.nombre  
from cargos c
left join (select h.legajo, h.nrocargo, min(h.nrocargo) as ultimo from histocargo h group by h.legajo) as tabla1
on c.nrocargo=tabla1.ultimo 
left join general g 
on g.legajo=tabla1.legajo 
order by g.legajo asc*/


		$cabecera = "<thead><tr><th>Legajo</th><th>Apellido</th><th>Nombre</th><th>Cargo</th><th>Lugar</th><th>Ultimo Ascenso</th><th>Ingreso</th></tr></thead>";
		break;
}

global $conn;
echo '<div class="divtabla">';
echo '<table id="tablaReportes" class="display cell-border compact hover nowrap order-column row-border stripe">';
echo $cabecera;
echo '<tbody>';
$result = mysqli_query($conn, $str);
$nbrows = mysqli_num_rows($result);
$rows = array();
while ($row = mysqli_fetch_row($result)){
	echo "<tr>";
	for ($i=0; $i <= count($row)-1; $i++) {
    	echo "<td>".$row[$i]."</td>";
	}
	echo "</tr>";
}
echo '</tbody>';
echo "</table>";
echo '</div>';
// Listado de Magistrados

// Listado de Funcionarios

// Listado de todos los empleados abogados, sin orden
// SELECT general.apellido, general.nombre, titulo.*, tipo_estudio.* FROM general, titulo, tipo_estudio WHERE (titulo.codestud = tipo_estudio.codestudio) and (tipo_estudio.codestudio=6) and (general.legajo=titulo.legajo)

?>
</body>
</html>