<?php

require('system.config.php');

 
function getSubcat($idcat, $idsubcat) {
global $conn;
$ic = $idcat!=""?$idcat:$_POST["id"];
$isc = $idsubcat;
switch($ic)
{
    case 1://Listado por organismo
        $str="SELECT * FROM lugar ORDER BY nombrelugar ASC";
        $result = $conn->query($str);
        $resp = '<option value="">-Seleccione organismo-</option>';
        while ($value = $result->fetch_object()) 
        {
            if($isc!="" && $value==$isc)
            {
                $resp.= '<option selected value='.$value->nrolugar.'>'.$value->nombrelugar.'</option>';
            }
            else
            {
                $resp.= '<option value='.$value->nrolugar.'>'.$value->nombrelugar.'</option>';
            }
        }
        break;
    case 2://Listado por cargos
        $str="SELECT * FROM cargos ORDER BY cargo ASC";
        $result = $conn->query($str);
        $resp = '<option value="">-Seleccione cargo-</option>';
        while ($value = $result->fetch_object()) {
            $resp.= '<option value='.$value->nrocargo.'>'.$value->cargo.'</option>';
        }
        break;
    case 3: //listado por escalafon
        $str="SELECT * FROM escalafon ORDER BY id DESC"; 
        $result = $conn->query($str);
        echo $conn->error;
        $resp = '<option value="">-Seleccione escalaf&oacute;n-</option>';
        while ($value = $result->fetch_object()) {
            $resp.= '<option value='.$value->id.'>'.$value->nombre.'</option>';
        }
        break;
    case 4: //Listado por funcionarios
        $str="SELECT * FROM cargos WHERE nrocargo=5 OR nrocargo=6 OR nrocargo=7 OR nrocargo=10 OR nrocargo=11 OR nrocargo=14 OR nrocargo=15";
        $str.=" OR nrocargo=20 OR nrocargo=22 OR nrocargo=24 OR nrocargo=25 OR nrocargo=26 OR nrocargo=27 OR nrocargo=28 OR nrocargo=32"; 
        $str.=" OR nrocargo=60 OR nrocargo=61 OR nrocargo=62 OR nrocargo=63 OR nrocargo=64 OR nrocargo=234 ORDER BY cargo ASC";
        $result = $conn->query($str);
        echo $conn->error;
        $resp = '<option value="">-Seleccione funcionario-</option>';
        while ($value = $result->fetch_object()) {
            $resp.= '<option value='.$value->nrocargo.'>'.$value->cargo.'</option>';
        }
        break;
    case 7: //Listado por titulo
        $str="SELECT * FROM tipo_estudio ORDER BY titulo ASC";
        $result = $conn->query($str);
        echo $conn->error;
        $resp = '<option value="">-Seleccione titulo-</option>';
        while ($value = $result->fetch_object()) {
            $resp.= '<option value='.$value->codestudio.'>'.$value->titulo.'</option>';
        }
        break;
    case 11: //Listado de empleados con compensatorias
        $resp = '<option value="">-Seleccione opcion-</option><option value="1">Totales disponibles por empleado</option><option value="2">Disponibles por empleado por a&ntilde;o</option>';
        break;
    case 12: //Ultimos ingresados
        $resp = '<option value="">-Seleccione opcion-</option><option value="1">Ingresados el &uacute;ltimo mes</option><option value="2">Ingresados los &uacute;ltimos 6 meses</option><option value="3">Ingresados el &uacute;ltimo a&ntilde;o</option>';
        break;
    case 5;6; default:
        $resp='<option value="">-No hay categor&iacute;as disponibles-</option>';
        break;
    case 13://Listado por fuero
        $str="SELECT * FROM fuero ORDER BY nombre ASC";
        $result = $conn->query($str);
        $resp = '<option value="">-Seleccione fuero-</option>';
        while ($value = $result->fetch_object()) {
            $resp.= '<option value='.$value->id.'>'.$value->nombre.'</option>';
        }
        break;
    case 14://Listado por evaluacion
        $str="SELECT * FROM eval_evaluacion ORDER BY fecha_inicio ASC";
        $result = $conn->query($str);
        $resp = '<option value="">-Seleccione evaluaci&oacute;n-</option>';
        while ($value = $result->fetch_object()) {
            $resp.= '<option value='.$value->id.'>'.$value->nombre.'</option>';
        }
        break;
    case 15://Listado licencias
    case 26://Listado solicitudes licencia
        $str_tipo_lic = "SELECT * FROM tipo_licencias";
        $cons_tipo_lic = $conn->query($str_tipo_lic);
        $resp = '<option value="">-Seleccione licencia-</option>';
        while($obj_tipo_lic = $cons_tipo_lic->fetch_object())
        {
            $resp.= '<option value='.$obj_tipo_lic->id.'>'.$obj_tipo_lic->nombre.'</option>';
        }
        break;
     case 16://Listado liquidaciones
        $str_per = "SELECT DISTINCT periodo FROM cont_archivo_bsj ORDER BY periodo DESC";
        $cons_per = $conn->query($str_per);

        $resp = '<option value="">-Seleccione periodo-</option>';
        $i=0;
        while($obj = $cons_per->fetch_object())
        {
            $resp.= '<option value='.$obj->periodo.'>'.$obj->periodo.'</option>';
            $i++;
        }
        break;            
    case 19://Listado certificados
        $str_anio = "SELECT DISTINCT anio FROM fam_certificados ORDER BY anio ASC";
        $cons_anio = $conn->query($str_anio);

        $resp = '<option value="">-Seleccione periodo-</option>';
        $i=0;
        while($obj = $cons_anio->fetch_object())
        {
            $resp.= '<option value='.$obj->anio.'>'.$obj->anio.'</option>';
            $i++;
        }
        break;
    case 20://Listado recibos de sueldo por año y mes
        $str_per = "SELECT DISTINCT anio_liq FROM cont_ci_raw ORDER BY anio_liq DESC";
        $cons_per = $conn->query($str_per);

        $resp = '<option value="">-Seleccione periodo-</option>';
        $i=0;
        while($obj = $cons_per->fetch_object())
        {
            if(strlen(trim($obj->anio_liq))==1)
            {
                $a_l = "0".$obj->anio_liq;
            }
            else
            {
                $a_l = $obj->anio_liq;
            }
            $resp.= '<option value='.$obj->anio_liq.'>20'.$a_l.'</option>';
            $i++;
        }
        break;
    case 22://Listado recibos de sueldo por empleado y año
        $str="SELECT general.apellido, general.nombre, general.legajo, general.padron FROM general ORDER BY general.apellido, general.nombre";
        $result = $conn->query($str);
        $resp = '<option value="">-Seleccione empleado-</option>';
        while($obj = $result->fetch_object())
        {
            $resp.='<option data-subtext="Leg.: '.$obj->legajo.' | Pad.: '.$obj->padron.'" value='.$obj->legajo.'>'.$obj->apellido.', '.$obj->nombre.'</option>';
        }
        break;  
    case 24://Listado certificados de empleados
        $str_anio = "SELECT DISTINCT anio FROM emp_certificados ORDER BY anio ASC";
        $cons_anio = $conn->query($str_anio);

        $resp = '<option value="">-Seleccione periodo-</option>';
        $i=0;
        while($obj = $cons_anio->fetch_object())
        {
            $resp.= '<option value='.$obj->anio.'>'.$obj->anio.'</option>';
            $i++;
        }
        break;      
}
echo $resp;

}
 
function getSubSubcat() 
{
    global $conn;
    switch($_POST["rep"])
    {
        case 1:
            $str="SELECT DISTINCT year(desde) as anio FROM histolugar WHERE YEAR(desde)>=1960 AND YEAR(desde)<=YEAR(NOW()) ORDER BY year(desde) ASC";
            $result = $conn->query($str);
            $resp = '<option value="">-Seleccione periodo-</option>';
            $i = 0;
            while($obj = $result->fetch_object())
            {
                $resp.='<option value='.$i.'>'.$obj->anio.'</option>';
                $i++;
            }
            break;
        case 16:
            $str="SELECT DISTINCT liquidacion, cont_liquidacion.nombre FROM cont_archivo_bsj, cont_liquidacion WHERE cont_archivo_bsj.periodo=".$_POST["id"]." AND cont_archivo_bsj.liquidacion=cont_liquidacion.id ORDER BY cont_liquidacion.id ASC";
            $result = $conn->query($str);
            $resp = '<option value="">-Seleccione liquidaci&oacute;n-</option>';
            while($obj = $result->fetch_object())
            {
                $resp.='<option value='.$obj->liquidacion.'>'.$obj->nombre.'</option>';
            }
            break;
        case 20: //Recibo de sueldo por mes
            $str="SELECT DISTINCT mes_liq, cont_liquidacion_ci.nombre FROM cont_ci_raw, cont_liquidacion_ci WHERE cont_ci_raw.anio_liq=".$_POST["id"]." AND cont_ci_raw.mes_liq=cont_liquidacion_ci.id ORDER BY cont_liquidacion_ci.id_ordenamiento DESC";
            $result = $conn->query($str);
            $resp = '<option value="">-Seleccione mes-</option>';
            while($obj = $result->fetch_object())
            {
                $resp.='<option value='.$obj->mes_liq.'>'.$obj->nombre.'</option>';
            }
            break;
        case 22: //Recibo de sueldo por empleado
            $str="SELECT DISTINCT anio_liq FROM cont_ci_raw ORDER BY anio_liq DESC";
            $result = $conn->query($str);
            $resp = '<option value="">-Seleccione periodo-</option>';
            while($obj = $result->fetch_object())
            {
                if(strlen(trim($obj->anio_liq))==1)
                {
                    $a_l = "0".$obj->anio_liq;
                }
                else
                {
                    $a_l = $obj->anio_liq;
                }
                $resp.= '<option value='.$obj->anio_liq.'>20'.$a_l.'</option>';
            }
            break;        
    }
    
    echo $resp;
}    

function getDataTable()
{
    global $conn;
    switch($_POST["id"])
    {
        case 1:
		$titulo = "<h2>Listado por Organismo</h2>";
		// Listado de agentes por organismos ordenado por lugar y cargo
		$str="SELECT l.nombrelugar, g.apellido, g.nombre, c.cargo
		FROM general g 
		RIGHT JOIN lugar l ON
		l.nrolugar=g.lugaractu
		LEFT JOIN cargos c ON
		g.cargoactu=c.nrocargo
		ORDER BY l.nrolugar, c.nrocargo";
		$cabecera = "<thead><tr><th>Lugar</th><th>Apellido</th><th>Nombre</th><th>Cargo</th></tr></thead>";
		break;
	case 2:
		echo "<h2>Listado por Cargos</h2>";
		// Listado por cargos (legajo, fecha ingreso, ultimo ascenso, dependencia)
		$str="SELECT g.legajo, g.apellido, g.nombre, c.cargo, l.nombrelugar, DATE_FORMAT(tabla1.ultimo_ascenso, '%d-%m-%Y') AS xultimo_ascenso, DATE_FORMAT(tabla2.fecha_ingreso, '%d-%m-%Y') AS xfecha_ingreso  
		FROM general g 
		LEFT JOIN (SELECT h.legajo, MAX( h.desde ) as ultimo_ascenso FROM histocargo h GROUP BY h.legajo) as tabla1 ON
		g.legajo=tabla1.legajo
		LEFT JOIN (SELECT h.legajo, MIN(h.desde) as fecha_ingreso FROM histocargo h GROUP BY h.legajo) as tabla2 ON
		g.legajo=tabla2.legajo 
		RIGHT JOIN cargos c ON
		g.cargoactu=c.nrocargo
		LEFT JOIN lugar l ON
		g.lugaractu=l.nrolugar
		ORDER BY c.nrocargo";
		$cabecera = "<thead><tr><th>Legajo</th><th>Apellido</th><th>Nombre</th><th>Cargo</th><th>Lugar</th><th>Ascenso</th><th>Ingreso</th></tr></thead>";
		break;
	case 3:
		echo "<h2>Listado de Magistrados</h2>";
		$str = "SELECT g.legajo, g.apellido, g.nombre, c.cargo  
		FROM general g 
		LEFT JOIN cargos c ON
		g.cargoactu=c.nrocargo 
		WHERE g.activa=1 AND g.cargoactu IN (1,2,3,4,8,9,13)
		ORDER BY c.nrocargo ASC ";
		$cabecera = "<thead><tr><th>Legajo</th><th>Apellido</th><th>Nombre</th><th>Cargo</th></tr></thead>";
		break;
	case 4:
		echo "<h2>Listado de Funcionarios</h2>";
		$str = "SELECT g.legajo, g.apellido, g.nombre, c.cargo  
		FROM general g 
		LEFT JOIN cargos c ON
		g.cargoactu=c.nrocargo 
		WHERE g.activa=1 AND g.cargoactu IN (5,6,7,10,11,14,15,20,22,24,25,26,27,28,32,60,61,62,63,64,234)
		ORDER BY c.nrocargo ASC ";
		$cabecera = "<thead><tr><th>Legajo</th><th>Apellido</th><th>Nombre</th><th>Cargo</th></tr></thead>";
		break;
        case 5:
		echo "<h2>Listado por Titulo</h2>";
		$str="SELECT DISTINCT t.legajo, g.apellido, g.nombre, te.titulo FROM titulo t 
		INNER JOIN general g ON (t.legajo=g.legajo) 
		LEFT JOIN tipo_estudio te ON (t.codestud=te.codestudio) 
		ORDER BY g.apellido ASC";
		$cabecera = "<thead><tr><th>Legajo</th><th>Apellido</th><th>Nombre</th><th>Titulo</th></tr></thead>";
		break;
	case 6:
		echo "<h2>Listado de Empleados Abogados</h2>";
		$str="SELECT DISTINCT t.legajo, g.apellido, g.nombre, te.titulo FROM titulo t 
		INNER JOIN general g ON (t.legajo=g.legajo) AND (t.codestud=6) 
		LEFT JOIN tipo_estudio te ON (t.codestud=te.codestudio) 
		ORDER BY g.apellido ASC";
		$cabecera = "<thead><tr><th>Legajo</th><th>Apellido</th><th>Nombre</th><th>Titulo</th></tr></thead>";
		break;
        case 9: 
		// Empleados con compensatorias
		echo "<h2>Empleados con licencias compensatorias</h2>";
		
		$str="SELECT g.legajo, g.apellido, g.nombre, c.cargo, DATE_FORMAT(li.desde, '%d-%m-%Y'), DATE_FORMAT(li.hasta, '%d-%m-%Y'), SUM(li.disponibles) as totaldisponibles 
                FROM general g 
                LEFT JOIN lic_comp_dias2 li 
                ON g.legajo=li.legajo 
                RIGHT JOIN cargos c 
                ON g.cargoactu=c.nrocargo 
                WHERE g.activa=1 
                GROUP BY li.legajo 
		ORDER BY g.apellido ASC";
            
                $cabecera = "<thead><tr><th>Legajo</th><th>Apellido</th><th>Nombre</th><th>Cargo</th><th>Desde</th><th>Hasta</th><th>Total disponibles</th></tr></thead>";
		break;
        case 7; default: 
		// Listado general (legajo-ultimo ascenso-dependencia-cargo)
		echo "<h2>Listado General</h2>";
		/*$str="SELECT g.legajo, g.apellido, g.nombre, c.cargo, l.nombrelugar, DATE_FORMAT(tabla1.ultimo_ascenso, '%d-%m-%Y') AS xultimo_ascenso, DATE_FORMAT(tabla2.fecha_ingreso, '%d-%m-%Y') AS xfecha_ingreso  
		FROM general g 
		LEFT JOIN (SELECT h.legajo, MAX( h.desde ) as ultimo_ascenso FROM histocargo h GROUP BY h.legajo) as tabla1 ON
		g.legajo=tabla1.legajo
		LEFT JOIN (SELECT h.legajo, MIN(h.desde) as fecha_ingreso FROM histocargo h GROUP BY h.legajo) as tabla2 ON
		g.legajo=tabla2.legajo 
		INNER JOIN cargos c ON
		g.cargoactu=c.nrocargo
		LEFT JOIN lugar l ON
		g.lugaractu=l.nrolugar
		ORDER BY g.apellido ASC"; */
		$str="SELECT g.legajo, g.apellido, g.nombre, c.cargo, l.nombrelugar, DATE_FORMAT(tabla1.ultimo_ascenso, '%d-%m-%Y') AS xultimo_ascenso, DATE_FORMAT(tabla2.fecha_ingreso, '%d-%m-%Y') AS xfecha_ingreso  
		FROM general g 
		LEFT JOIN (SELECT h.legajo, MAX( h.desde ) as ultimo_ascenso FROM histocargo h GROUP BY h.legajo) as tabla1 ON
		g.legajo=tabla1.legajo
		LEFT JOIN (SELECT h.legajo, MIN(h.desde) as fecha_ingreso FROM histocargo h GROUP BY h.legajo) as tabla2 ON
		g.legajo=tabla2.legajo 
		RIGHT JOIN cargos c ON
		g.cargoactu=c.nrocargo
		LEFT JOIN lugar l ON
		g.lugaractu=l.nrolugar
		WHERE g.activa=1 
		ORDER BY g.apellido ASC";
    }//End switch
    
    echo $cabecera;
    echo '<tbody>';
    $result = $conn->query($str);
    $nbrows = $result->num_rows;
    $rows = array();
    while ($row = $result->fetch_row())
    {
	echo "<tr>";
	for ($i=0; $i <= count($row)-1; $i++) 
        {
            echo "<td>".$row[$i]."</td>";
	}
	echo "</tr>";
    }
echo '</tbody>';

}
 
if ($_POST) 
{
    switch ($_POST["task"]) 
    {
        case "getSubcat":
            getSubcat("","");
            break;
        case "getSubSubcat":
            getSubSubcat();
            break;
        case "getDataTable":
            getDataTable();
    }
}