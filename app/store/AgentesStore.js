Ext.define('personal.store.AgentesStore', {
	extend: 'Ext.data.Store',
	model: 'personal.model.Agentes',

	//autoLoad: true,
    //autoSync: true,
    proxy: {
        type: 'ajax',
        url: 'app/proxy.agentes.php',
        method: 'GET',
        extraParams: {
            act: 'all',
            activos: 'si',
            start: 0,
            limit: 999999
        },
        reader: {
            type: 'json',
            rootProperty: 'results',
            successProperty: 'success'
        }
    }

});