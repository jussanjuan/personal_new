Ext.define('personal.store.AniosCertificadosStore', {
	extend: 'Ext.data.Store',
	model: 'personal.model.AniosCertificados',
    alias: 'store.AniosCertificadosStore',
	autoLoad: true,
    autoSync: true    

});