Ext.define('RH.store.BancoStore', {
	extend: 'Ext.data.Store',
	model: 'RH.model.Banco',

	autoLoad: true,
    autoSync: true,

    proxy: {
        type: 'ajax',
        method: 'GET',
        extraParams: {
            act: 'getBancos'
        },
        url: 'app/proxy.contabilidad.php',
        reader: {
            type: 'json',
            rootProperty: 'results',
            successProperty: 'success'
        }
    }

});