Ext.define('RH.store.BenefactoresStore', {
	extend: 'Ext.data.Store',
	model: 'RH.model.Benefactor',

	//autoLoad: true,
    //autoSync: true,
    proxy: {
        type: 'ajax',
        url: 'app/proxy.agentes.php',
        method: 'GET',
        extraParams: {
            act: 'all',
            start: 0,
            limit: 999999
        },
        reader: {
            type: 'json',
            root: 'results',
            successProperty: 'success'
        }
    }

});