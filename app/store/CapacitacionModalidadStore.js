Ext.define('personal.store.CapacitacionModalidadStore', {
	extend: 'Ext.data.Store',
	model: 'personal.model.CapacitacionModalidad',
    storeId: 'CapacitacionModalidadStore',
    alias: 'store.CapacitacionModalidadStore',
	autoLoad: true,
    autoSync: true,

    proxy: {
        type: 'ajax',
        method: 'POST',
        extraParams: {
            act: 'getModalidadCapacitaciones'
        },
        url: 'app/proxy.capacitaciones.php',
        reader: {
            type: 'json',
            rootProperty: 'results',
            successProperty: 'success'
        }
    }

});