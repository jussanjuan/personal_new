Ext.define('personal.store.CapacitacionOrigenStore', {
	extend: 'Ext.data.Store',
	model: 'personal.model.CapacitacionOrigen',
    storeId: 'CapacitacionOrigenStore',
    alias: 'store.CapacitacionOrigenStore',
	autoLoad: true,
    autoSync: true,

    proxy: {
        type: 'ajax',
        method: 'GET',
        extraParams: {
            act: 'getOrigenCapacitaciones'
        },
        url: 'app/proxy.capacitaciones.php',
        reader: {
            type: 'json',
            rootProperty: 'results',
            successProperty: 'success'
        }
    }

});