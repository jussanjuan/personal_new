Ext.define('personal.store.CapacitacionStore', {
	extend: 'Ext.data.Store',
	model: 'personal.model.Capacitacion',
    storeId: 'CapacitacionStore',
    alias: 'store.CapacitacionStore',
	//autoLoad: true,
    autoSync: true,

    proxy: {
        type: 'ajax',
        method: 'POST',
        url: 'app/proxy.capacitaciones.php',
        reader: {
            type: 'json',
            rootProperty: 'results',
            successProperty: 'success'
        }
    }

});