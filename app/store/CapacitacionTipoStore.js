Ext.define('personal.store.CapacitacionTipoStore', {
	extend: 'Ext.data.Store',
	model: 'personal.model.CapacitacionTipo',
    storeId: 'CapacitacionTipoStore',
    alias: 'store.CapacitacionTipoStore',
	autoLoad: true,
    autoSync: true,

    proxy: {
        type: 'ajax',
        method: 'GET',
        extraParams: {
            act: 'getTipoCapacitaciones'
        },
        url: 'app/proxy.capacitaciones.php',
        reader: {
            type: 'json',
            rootProperty: 'results',
            successProperty: 'success'
        }
    }

});