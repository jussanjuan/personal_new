Ext.define('personal.store.CargosStore', {
	extend: 'Ext.data.Store',
	model: 'personal.model.Cargos',
    storeId: 'CargosStore',
    alias: 'store.CargosStore',
	autoLoad: true,
    autoSync: true,
    proxy: {
        type: 'ajax',
        method: 'POST',
        url: 'app/proxy.cargos.php',
        reader: {
            type: 'json',
            rootProperty: 'results',
            successProperty: 'success'
        }
    }

    

});