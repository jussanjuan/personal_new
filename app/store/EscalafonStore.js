Ext.define('personal.store.EscalafonStore', {
	extend: 'Ext.data.Store',
	model: 'personal.model.Escalafon',
    alias: 'store.EscalafonStore',
    //storeId: 'EscalafonStore',
	autoLoad: true,
    autoSync: true,

    proxy: {
        type: 'ajax',
        method: 'POST',
        url: 'app/proxy.escalafon.php',
        reader: {
            type: 'json',
            rootProperty: 'results',
            successProperty: 'success'
        }
    }

});