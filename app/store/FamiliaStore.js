Ext.define('personal.store.FamiliaStore', {
	extend: 'Ext.data.Store',
	model: 'personal.model.Familia',
	alias: 'store.FamiliaStore',
	storeId: 'FamiliaStore',
	autoLoad: true,
    autoSync: true,
    proxy: {
        type: 'ajax',
        method: 'POST',
        url: 'app/proxy.familia.php',
        extraParams:{
            act: 'getFamiliares'
        },
        reader: {
            type: 'json',
            rootProperty: 'results',
            successProperty: 'success'
        }
    }

});