Ext.define('personal.store.FeriadosStore', {
	extend: 'Ext.data.Store',
	model: 'personal.model.Feriado',
    alias: 'store.FeriadosStore',
    storeId: 'FeriadosStore',
    autoSync: true,
    autoLoad: true,
	proxy: {
        type: 'ajax',
        method: 'GET',
        url: 'app/proxy.feriados.php',
        extraParams: {
            tarea: 'getFeriados'
        },
        reader: {
            type: 'json',
            rootProperty: 'results',
            successProperty: 'success'
        }
    }

});