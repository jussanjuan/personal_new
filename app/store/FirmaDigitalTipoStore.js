Ext.define('personal.store.FirmaDigitalTipoStore', {
	extend: 'Ext.data.Store',
	model: 'personal.model.FirmaDigitalTipo',
    storeId: 'FirmaDigitalTipoStore',
    alias: 'store.FirmaDigitalTipoStore',
	autoLoad: true,
    autoSync: true,

    proxy: {
        type: 'ajax',
        method: 'GET',
        extraParams: {
            act: 'getTipoFirmaDigital'
        },
        url: 'app/proxy.firmasdigitales.php',
        reader: {
            type: 'json',
            rootProperty: 'results',
            successProperty: 'success'
        }
    }

});