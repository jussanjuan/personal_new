Ext.define('personal.store.FueroStore', {
	extend: 'Ext.data.Store',
	model: 'personal.model.Fuero',
    storeId: 'FueroStore',
    alias: 'store.FueroStore',
	autoLoad: true,
    autoSync: true,
    proxy: {
        type: 'ajax',
        method: 'POST',
        url: 'app/proxy.fuero.php',
        reader: {
            type: 'json',
            rootProperty: 'results',
            successProperty: 'success'
        }
    }

});