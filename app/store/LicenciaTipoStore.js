Ext.define('personal.store.LicenciaTipoStore', {
	extend: 'Ext.data.Store',
	model: 'personal.model.LicenciaTipo',
    storeId: 'LicenciaTipoStore',
    alias: 'store.LicenciaTipoStore',
	autoLoad: true,
    autoSync: true,

    proxy: {
        type: 'ajax',
        method: 'GET',
        extraParams: {
            tarea: 'getTipoLicencias'
        },
        url: 'app/proxy.licencias.php',
        reader: {
            type: 'json',
            rootProperty: 'results',
            successProperty: 'success'
        }
    }

});