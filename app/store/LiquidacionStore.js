Ext.define('RH.store.LiquidacionStore', {
	extend: 'Ext.data.Store',
	model: 'RH.model.Liquidacion',

	autoLoad: true,
    autoSync: true,

    proxy: {
        type: 'ajax',
        method: 'GET',
        
        url: 'app/proxy.liquidacion.php',
        reader: {
            type: 'json',
            root: 'results',
            successProperty: 'success'
        }
    }

});