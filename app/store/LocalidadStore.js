Ext.define('personal.store.LocalidadStore', {
	extend: 'Ext.data.Store',
	model: 'personal.model.Localidad',
    alias: 'store.LocalidadStore',
	autoLoad: true,
    autoSync: true,

    proxy: {
        type: 'ajax',
        url: 'app/proxy.paises.php',
        extraParams:{
            act: 'getLocalidad'
        },
        reader: {
            type: 'json',
            rootProperty: 'results',
            successProperty: 'success'
        }
    }

});