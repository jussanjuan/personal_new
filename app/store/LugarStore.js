Ext.define('personal.store.LugarStore', {
	extend: 'Ext.data.Store',
	model: 'personal.model.Lugar',
    storeId: 'LugarStore',
    alias: 'store.LugarStore',
	//autoLoad: true,
    autoSync: true,

    proxy: {
        type: 'ajax',
        method: 'POST',
        url: 'app/proxy.lugar.php',
        reader: {
            type: 'json',
            rootProperty: 'results',
            successProperty: 'success'
        }
    }

});