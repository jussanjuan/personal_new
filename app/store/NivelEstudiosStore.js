Ext.define('personal.store.NivelEstudiosStore', {
	extend: 'Ext.data.Store',
	model: 'personal.model.NivelEstudios',
    alias: 'store.NivelEstudios',
    storeId: 'NivelEstudios',
	autoLoad: true,
    autoSync: true,

    proxy: {
        type: 'ajax',
        method: 'GET',
        url: 'app/proxy.titulo.php',
        extraParams:{
            accion: 'getNivelEstudios'
        },
        reader: {
            type: 'json',
            rootProperty: 'results',
            successProperty: 'success'
        }
    }

});