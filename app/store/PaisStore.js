Ext.define('personal.store.PaisStore', {
	extend: 'Ext.data.Store',
	model: 'personal.model.Pais',
    alias: 'store.PaisStore',
	autoLoad: true,
    autoSync: true,

    proxy: {
        type: 'ajax',
        url: 'app/proxy.paises.php',
        reader: {
            type: 'json',
            rootProperty: 'results',
            successProperty: 'success'
        }
    }

});