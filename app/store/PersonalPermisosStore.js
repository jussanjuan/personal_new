Ext.define('RH.store.PersonalPermisosStore', {
	extend: 'Ext.data.Store',
	model: 'RH.model.Persona',

	autoLoad: true,
    autoSync: true,
    
    proxy: {
        type: 'ajax',
        method: 'GET',
        extraParams: {
            act: 'getAgentesPermisos'
        },
        url: 'app/proxy.agentes.php',
        reader: {
            type: 'json',
            root: 'results',
            successProperty: 'success'
        }
    }

});