Ext.define('personal.store.PersonalStore', {
	extend: 'Ext.data.Store',
	alias: 'store.PersonalStore',
    model: 'personal.model.Persona',
    storeId: 'PersonalStore',
	autoLoad: true,
    autoSync: true,
    pageSize: 0
    //remoteFilter: true
    /*proxy: {
        type: 'ajax',
        url: 'app/proxy.agentes.php',
        extraParams:
        {
            act: AppGlobals.showAgentes
        },
        reader: {
            type: 'json',
            rootProperty: 'results',
            successProperty: 'success'
        }
    }*/

});