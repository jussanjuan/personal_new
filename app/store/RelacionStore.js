Ext.define('personal.store.RelacionStore', {
	extend: 'Ext.data.Store',
	model: 'personal.model.Relacion',
    alias: 'store.RelacionStore',
    storeId: 'RelacionStore',
	autoLoad: true,
    autoSync: true,

    proxy: {
        type: 'ajax',
        method: 'POST',
        url: 'app/proxy.familia.php',
        extraParams:{
            act: 'getRelaciones'
        },
        reader: {
            type: 'json',
            rootProperty: 'results',
            successProperty: 'success'
        }
    }

});