Ext.define('personal.store.SancionStore', {
	extend: 'Ext.data.Store',
	model: 'personal.model.Sancion',
    storeId: 'SancionStore',
    alias: 'store.SancionStore',
	autoLoad: true,
    autoSync: true,

    proxy: {
        type: 'ajax',
        method: 'POST',
        url: 'app/proxy.sanciones.php',
        reader: {
            type: 'json',
            rootProperty: 'results',
            successProperty: 'success'
        }
    }

});