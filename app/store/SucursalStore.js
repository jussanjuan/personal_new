Ext.define('personal.store.SucursalStore', {
	extend: 'Ext.data.Store',
	model: 'personal.model.Sucursal',

	autoLoad: true,
    autoSync: true,

    proxy: {
        type: 'ajax',
        method: 'GET',
        extraParams: {
            act: 'getSucursales'
        },
        url: 'app/proxy.contabilidad.php',
        reader: {
            type: 'json',
            rootProperty: 'results',
            successProperty: 'success'
        }
    }

});