Ext.define('RH.store.TipoCuentaStore', {
	extend: 'Ext.data.Store',
	model: 'RH.model.TipoCuenta',

	autoLoad: true,
    autoSync: true,

    proxy: {
        type: 'ajax',
        method: 'GET',
        extraParams: {
            act: 'getTipoCuentas'
        },
        url: 'app/proxy.contabilidad.php',
        reader: {
            type: 'json',
            root: 'results',
            successProperty: 'success'
        }
    }

});