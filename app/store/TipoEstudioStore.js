Ext.define('personal.store.TipoEstudioStore', {
	extend: 'Ext.data.Store',
	model: 'personal.model.TipoEstudio',
    storeId: 'TipoEstudioStore',
    alias: 'store.TipoEstudioStore',
	autoLoad: true,
    autoSync: true,

    proxy: {
        type: 'ajax',
        url: 'app/proxy.titulo.php',
        params: {
            accion: "getTipoEstudio"
        },
        reader: {
            type: 'json',
            rootProperty: 'results',
            successProperty: 'success'
        }
    }

});