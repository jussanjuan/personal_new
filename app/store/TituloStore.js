Ext.define('personal.store.TituloStore', {
	extend: 'Ext.data.Store',
	model: 'personal.model.Titulo',
    storeId: 'TituloStore',
    alias: 'store.TituloStore',
	autoLoad: true,
    autoSync: true,

    proxy: {
        type: 'ajax',
        url: 'app/proxy.titulo.php',
        params: {
            legajo: AppGlobals.legAgente
        },
        reader: {
            type: 'json',
            rootProperty: 'results',
            successProperty: 'success'
        }
    }

});