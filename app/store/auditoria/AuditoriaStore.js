Ext.define('personal.store.auditoria.AuditoriaStore', {
	extend: 'Ext.data.Store',
	alias: 'store.AuditoriaStore',
    storeId: 'AuditoriaStore',
    autoSync: true,
    autoLoad: true,
    pageSize: AppGlobals.itemsPorPagina,
    proxy: {
        type: 'ajax',
        url: 'app/proxy.auditoria.php',
        extraParams: {
            sist: 'personal'
        },
        reader: {
            type: 'json',
            rootProperty: 'results',
            successProperty: 'success'
        }
    }

});