Ext.define('personal.store.historial.HCapacitacionStore', {
	extend: 'Ext.data.Store',
	alias: 'store.HCapacitacionStore',
	storeId: 'HCapacitacionStore',
    model: 'personal.model.historial.HCapacitacion',
	autoLoad: true,
    autoSync: true

});