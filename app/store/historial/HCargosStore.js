Ext.define('personal.store.historial.HCargosStore', 
{
	extend: 'Ext.data.Store',
	alias: 'store.HCargosStore',
	storeId: 'HCargosStore',
	model: 'personal.model.historial.HCargos',
	autoSync: true

});