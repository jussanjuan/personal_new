Ext.define('RH.store.historial.HCompensatoriaStore', {
	extend: 'Ext.data.Store',
	model: 'RH.model.historial.HCompensatoria',

	//autoLoad: true,
    autoSync: true,

    proxy: {
        type: 'ajax',
        //method: 'POST',
        url: 'app/proxy.historial.php',
        reader: {
            type: 'json',
            rootProperty: 'results',
            successProperty: 'success'
        }
    }

});