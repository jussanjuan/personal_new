Ext.define('personal.store.historial.HCompensatoriaStoreDiasFeria', 
{
	extend: 'Ext.data.Store',
	alias: 'store.HCompensatoriaStoreDiasFeria',
    storeId: 'HCompensatoriaStoreDiasFeria',
    model: 'personal.model.historial.HCompensatoriaDiasFeria',
    autoSync: true
});