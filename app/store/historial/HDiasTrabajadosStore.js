Ext.define('personal.store.historial.HDiasTrabajadosStore', 
{
	extend: 'Ext.data.Store',
	alias: 'store.HDiasTrabajadosStore',
    storeId: 'HDiasTrabajadosStore',
    model: 'personal.model.historial.HDiasTrabajados',
    autoSync: true,
    groupField: 'nombre',
    proxy: {
        type: 'ajax',
        method: 'POST',
        url: 'app/proxy.historial.php',
        extraParams:{
            act: 'getDiasTrabajados'
        },
        reader: 
        {
            type: 'json',
            rootProperty: 'results',
            successProperty: 'success'
        }
    }

});