Ext.define('personal.store.historial.HDobleTurnoStore', {
	extend: 'Ext.data.Store',
	model: 'personal.model.historial.HDobleTurno',
    alias: 'store.HDobleTurnoStore',
    storeId: 'HDobleTurnoStore',
	//autoLoad: true,
    autoSync: true,

    proxy: {
        type: 'ajax',
        //method: 'POST',
        url: 'app/proxy.historial.php',
        reader: {
            type: 'json',
            root: 'results',
            successProperty: 'success'
        }
    }

});