Ext.define('personal.store.historial.HFirmaDigitalStore', {
	extend: 'Ext.data.Store',
	alias: 'store.HFirmaDigitalStore',
	storeId: 'HFirmaDigitalStore',
    model: 'personal.model.historial.HFirmaDigital',
	autoLoad: true,
    autoSync: true,
    proxy: {
        type: 'ajax',
        method: 'GET',
        url: 'app/proxy.firmasdigitales.php',
        reader: {
            type: 'json',
            rootProperty: 'results',
            successProperty: 'success'
        }
    }

});