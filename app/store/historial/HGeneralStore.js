Ext.define('personal.store.historial.HGeneralStore', {
	extend: 'Ext.data.Store',
	model: 'personal.model.historial.HGeneral',
        autoSync: true,

    proxy: {
        type: 'ajax',
        method: 'POST',
        url: 'app/proxy.historial.php',
        /*params: {
            tabla: 'lic_salud',
            legajo: AppGlobals.legAgente
        },*/
        reader: {
            type: 'json',
            rootProperty: 'results',
            successProperty: 'success'
        }
    }

});