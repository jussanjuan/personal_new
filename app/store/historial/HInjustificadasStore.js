Ext.define('RH.store.historial.HInjustificadasStore', {
	extend: 'Ext.data.Store',
	model: 'RH.model.historial.HInjustificadas',

	//autoLoad: true,
    autoSync: true,

    proxy: {
        type: 'ajax',
        method: 'POST',
        url: 'app/proxy.historial.php',
        /*params: {
            tabla: 'lic_salud',
            legajo: AppGlobals.legAgente
        },*/
        reader: {
            type: 'json',
            root: 'results',
            successProperty: 'success'
        }
    }

});