Ext.define('personal.store.historial.HLicenciaStore', 
{
	extend: 'Ext.data.Store',
	alias: 'store.HLicenciaStore',
    storeId: 'HLicenciaStore',
    model: 'personal.model.historial.HLicencia',
    autoSync: true,
    proxy: {
        type: 'ajax',
        method: 'POST',
        url: 'app/proxy.historial.php',
        reader: 
        {
            type: 'json',
            rootProperty: 'results',
            successProperty: 'success'
        }
    }

});