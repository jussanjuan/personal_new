Ext.define('personal.store.historial.HLugarStore', {
	extend: 'Ext.data.Store',
	alias: 'store.HLugarStore',
	storeId: 'HLugarStore',
	model: 'personal.model.historial.HLugar',
	autoLoad: true,
    autoSync: true

});