Ext.define('personal.store.historial.HSancionStore', {
	extend: 'Ext.data.Store',
	alias: 'store.HSancionStore',
	storeId: 'HSancionStore',
    model: 'personal.model.historial.HSancion',
	autoLoad: true,
    autoSync: true,
    proxy: {
        type: 'ajax',
        method: 'POST',
        url: 'app/proxy.sanciones.php',
        reader: {
            type: 'json',
            rootProperty: 'results',
            successProperty: 'success'
        }
    }

});