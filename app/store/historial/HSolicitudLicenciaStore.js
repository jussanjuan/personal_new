Ext.define('personal.store.historial.HSolicitudLicenciaStore', 
{
	extend: 'Ext.data.Store',
	model: 'personal.model.historial.HSolicitudLicencia',
    alias: 'store.HSolicitudLicenciaStore',
    storeId: 'HSolicitudLicenciaStore',
    autoSync: true,
    proxy: {
        type: 'ajax',
        method: 'POST',
        url: 'app/proxy.historial.php',
        reader: 
        {
            type: 'json',
            rootProperty: 'results',
            successProperty: 'success'
        }
    }

});