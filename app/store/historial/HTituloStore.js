Ext.define('personal.store.historial.HTituloStore', {
	extend: 'Ext.data.Store',
	alias: 'store.HTituloStore',
    storeId: 'HTituloStore',
    model: 'personal.model.historial.HTitulo',
    autoLoad: true,
    autoSync: true

});