Ext.define('personal.store.historial.HistoriaClinicaStore', 
{
	extend: 'Ext.data.Store',
	model: 'personal.model.historial.HistoriaClinica',
    alias: 'store.HistoriaClinicaStore',
    storeId: 'HistoriaClinicaStore',
    autoSync: true,
    proxy: {
        type: 'ajax',
        method: 'POST',
        url: 'app/proxy.historiaclinica.php',
        reader: 
        {
            type: 'json',
            rootProperty: 'results',
            successProperty: 'success'
        }
    }

});