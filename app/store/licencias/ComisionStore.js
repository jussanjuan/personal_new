Ext.define('personal.store.licencias.ComisionStore', {
	extend: 'Ext.data.Store',
	model: 'personal.model.licencias.Comision',
        autoSync: true,
    alias: 'store.ComisionStore',
    storeId: 'ComisionStore',
    proxy: {
        type: 'ajax',
        method: 'GET',
        url: 'app/proxy.licencias.php',
        reader: {
            type: 'json',
            rootProperty: 'results',
            successProperty: 'success'
        }
    }

});