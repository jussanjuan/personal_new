Ext.define('RH.store.licencias.CompensatoriaMasivaStore', {
	extend: 'Ext.data.Store',
	model: 'RH.model.licencias.CompensatoriaMasiva',
	autoLoad: true,
	autoSync: false,

	 proxy: {
        type: 'ajax',
     	url: 'app/proxy.licencias.php',
        extraParams: {
            tarea: 'getLicenciasMasivasCompensatorias'
        },
        reader: {
            type: 'json',
            root: 'results',
            successProperty: 'success'
            }
        }
});