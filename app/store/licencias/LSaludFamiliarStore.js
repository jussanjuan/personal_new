Ext.define('RH.store.licencias.LSaludFamiliarStore', {
	extend: 'Ext.data.Store',
	model: 'RH.model.licencias.HSaludPersonal',

	//autoLoad: true,
    autoSync: true,

    proxy: {
        type: 'ajax',
        method: 'POST',
        url: 'app/proxy.historial.php',
        /*params: {
            tabla: 'lic_salud',
            legajo: AppGlobals.legAgente
        },*/
        reader: {
            type: 'json',
            root: 'results',
            successProperty: 'success'
        }
    }

});