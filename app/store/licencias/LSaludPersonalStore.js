Ext.define('RH.store.licencias.LSaludPersonalStore', {
	extend: 'Ext.data.Store',
	model: 'RH.model.licencias.LSaludPersonal',

	//autoLoad: true,
    autoSync: true,

    proxy: {
        type: 'ajax',
        method: 'POST',
        url: 'app/proxy.historial.php',
        /*params: {
            tabla: 'lic_salud',
            legajo: AppGlobals.legAgente
        },*/
        reader: {
            type: 'json',
            root: 'results',
            successProperty: 'success'
        }
    }

});