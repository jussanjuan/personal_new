<?php 

$file = $_FILES['archivoDPI'];
if($file['name'] != "")
{
    //Carpeta de subida de los archivos
    $dir_subida = '../res/archivos/';
    if(checkFile($file))
    {
        $archivo_subido = $dir_subida.basename($file["name"]);
        if (move_uploaded_file($file["tmp_name"], $archivo_subido))
        {
            if(chmod($archivo_subido, 0755))
            {
                $data["success"] = true;
                $data["msg"] = "Archivo subido correctamente, permisos asignados correctamente";
            }
            else
            {
                $data["success"] = true;
                $data["msg"] = "Archivo subido correctamente, pero no se pudieron asignar permisos";
            }
            
        } 
        else 
        {
            $data["success"] = false;
            $data["error"] = "Error al subir el archivo";
        }
    }//end if(file (validaciones))
    else
    {
        $data["success"] = false;
        $data["error"] = "Archivo no v&aacute;lido. Subir solo archivos enviados por la DPI";
    }
}//end if($_FILES['archivo']['name'][0] != "")
else
{
    $data["success"] = false;
    $data["error"] = "No se subi&oacute; ning&uacute;n archivo";
}
echo json_encode($data);


function checkFile($file)
{
    $ext_permitidas = array("txt");
    $mimes_permitidos = array("text/plain");
    $finfo = finfo_open(FILEINFO_MIME_TYPE);
    $mtype = finfo_file( $finfo, $file["tmp_name"]);
    //echo $file["name"];
    if(in_array(substr($file["name"], -3), $ext_permitidas) && in_array($mtype, $mimes_permitidos))
    {
        return true;
    }
    else
    {
        return false;
    }
    finfo_close($finfo);
}
?>