<?php
session_set_cookie_params(0);
session_start();
$data = array();

if(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true)
{
    $data["success"] = true;
    $data["sessionID"] = session_id();
    $data["usuario"] = $_SESSION["usuario"];
    $data["nombre_real"] = $_SESSION["nombre"] . " " . $_SESSION["apellido"];
    $data["lugar"] = $_SESSION["lugar"];
    $data["permiso"] = $_SESSION["permiso"];
    $data["evaluador"] = $_SESSION["evaluador"];
    $data["primeraContr"] = $_SESSION["firstPass"];
    $data["legajo"] = $_SESSION["legajo"];
    $data["rrhh"] = $_SESSION["rrhh"];
    $data["dpto_personal"] = $_SESSION["dpto_personal"];
    $data["exist_evaluacion"] = $_SESSION["exist_evaluacion"];
    $data["permisos"] = empty($_SESSION["permisos"])?array():$_SESSION["permisos"];
    
    
}
else
{
    $data["success"] = false;
    $data["error"] = "Inicie sesi&oacute;n";
}
    
echo json_encode($data);
/*$accion = $_GET["accion"];

echo " ACCION: ".$accion;

if (!isset($accion)) { $accion = 1; }

if ($accion == 0) {
    // Destruir la sesion
    session_set_cookie_params(0);
    session_start();
    session_unset();
    session_destroy();
    header('Location: index.php');
} else {
    session_start();

    // Calculamos el tiempo transcurrido
    $fechaGuardada = $_SESSION["ultimoAcceso"];
    $ahora = date("Y-n-j H:i:s");
    $tiempo_transcurrido = (strtotime($ahora) - strtotime($fechaGuardada));

    // Comparamos el tiempo transcurrido
    if ($tiempo_transcurrido >= 600) {
            session_unset();
            session_destroy();
            header('Location: index.php');
    } else {
            $_SESSION["ultimoAcceso"] = $ahora;
    }
}*/
?>