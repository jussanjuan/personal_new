<?php
session_start();
require('system.config.php');
$evalId = $_GET["evalId"];
if($_SESSION["loggedin"] != "si")
{
    header("location:index.php");
}
if($evalId==0)
{
    echo "Error al abrir la evaluaci&oacute;n";
}
else
{
    //Obtengo la ultima fecha en la que se calcularon los factores, dado que hay factores calculados de varios periodos. Obtengo la fecha del ultimo
    $str_fact = "SELECT MAX(fecha_inicio) as fecha_inicio FROM eval_factores";
    $cons_fact = $conn->query($str_fact);
    $obj_fact = $cons_fact->fetch_object();
    $ul_fecha = $obj_fact->fecha_inicio;

    //$str_evaluacion = "SELECT * FROM eval_evaluacion, eval_evaluacion_persona, eval_factores WHERE eval_evaluacion.id=eval_evaluacion_persona.id_evaluacion AND eval_evaluacion.id=eval_factores.id_evaluacion AND eval_evaluacion_persona.legajo_evaluado=eval_factores.legajo AND eval_evaluacion_persona.id=".$evalId;
    $str_evaluacion = "SELECT * FROM eval_evaluacion, eval_evaluacion_persona, eval_factores WHERE eval_evaluacion.id=eval_evaluacion_persona.id_evaluacion AND eval_evaluacion_persona.legajo_evaluado=eval_factores.legajo AND eval_factores.fecha_inicio='".$ul_fecha."' AND eval_evaluacion_persona.id=".$evalId;
    $res_evaluacion = $conn->query($str_evaluacion);
    
    
    if(!$res_evaluacion || $res_evaluacion->num_rows==0)
    {
        echo "Error al abrir la evaluacion";
    }
    else 
    {
        $obj_evaluacion = $res_evaluacion->fetch_object();
        //Obtengo los items
        $str_items = "SELECT nombre, descripcion, nota_item FROM eval_item, eval_item_evaluacion_persona WHERE eval_item_evaluacion_persona.id_item=eval_item.id AND id_evaluacion_pers=".$evalId;

        $res_items = $conn->query($str_items);
        $arr_items = array();
        while($obj_items = $res_items->fetch_object())
        {
            $arr_items[]=$obj_items;
        }
        
        $res_nomb_evaluador = $conn->query("SELECT apellido, nombre FROM general WHERE legajo=".$obj_evaluacion->legajo_evaluador);
        $obj_nombre_evaluador = $res_nomb_evaluador->fetch_object();
        $nombre_evaluador = $obj_nombre_evaluador->apellido.", ".$obj_nombre_evaluador->nombre;
        $res_nomb_evaluado = $conn->query("SELECT apellido, nombre FROM general WHERE legajo=".$obj_evaluacion->legajo_evaluado);
        $obj_nombre_evaluado = $res_nomb_evaluado->fetch_object();
        $nombre_evaluado = $obj_nombre_evaluado->apellido.", ".$obj_nombre_evaluado->nombre;
        
        $title_page = "Evaluacion: ".$obj_evaluacion->nombre;
        $title_header = $obj_evaluacion->nombre;

        $antiguedad_cargo = 5*$obj_evaluacion->antiguedad_cargo;
        $antiguedad_evaluacion = 2*round($obj_evaluacion->antiguedad_escalafon/5);
        ?>
        <!DOCTYPE html>
            <html>
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <meta http-equiv="Expires" content="Tue, 01 Jan 1980 1:00:00 GMT">
                <meta http-equiv="cache-control" content="no-cache">
                <meta http-equiv="pragma" content="no-cache">
                <META NAME="ROBOTS" CONTENT="NONE"> 
                <META NAME="GOOGLEBOT" CONTENT="NOARCHIVE">
                <title><?php echo $title_page; ?></title>
                <link rel="shortcut icon" type="image/x-icon" href="../res/img/favicon.ico">
                <link rel="stylesheet" type="text/css" href="../res/css/estilos.css"/>
                <script type="text/javascript" src="../res/DataTablesV2/jQuery-1.12.3/jquery-1.12.3.js"></script>
                <link rel="stylesheet" type="text/css" href="../res/css/bootstrap-3.3.7-dist/css/bootstrap.css"/>
                <script type="text/javascript" src="../res/css/bootstrap-3.3.7-dist/js/bootstrap.js"></script>
                
            </head>
        <body>
            <div class="container-fluid">
              <div class="panel panel-primary">
                  <div class="panel-heading">
                      <h3><?php echo $title_header; ?></h3>
                  </div>  
                  <div class="panel-body">
                      <ul class="list-group">
                        <li class="list-group-item col-lg-12" id="motivo"><?php echo "Motivo: ".$obj_evaluacion->motivo; ?></li>
                        <li class="list-group-item col-lg-6" id="evaluador"><?php echo "Evaluador: ".$nombre_evaluador; ?></li>
                        <li class="list-group-item col-lg-6" id="evaluado"><?php echo "Evaluado: ".$nombre_evaluado; ?></li>
                        <li class="list-group-item col-lg-6" id="fecha_inicio"><?php echo "Fecha inicio: ".$obj_evaluacion->fecha_inicio; ?></li>
                        <li class="list-group-item col-lg-6" id="fecha_fin"><?php echo "Fecha fin: ".$obj_evaluacion->fecha_fin; ?></li>
                        <li class="list-group-item col-lg-6" id="promedio"><?php echo "Promedio: ".$obj_evaluacion->nota_promedio; ?></li>
                        <li class="list-group-item col-lg-6" id="aplazos"><?php echo "Aplazos: ".$obj_evaluacion->cant_aplazos; ?></li>
                        <li class="list-group-item col-lg-6" id="tardanzas"><?php echo "Tardanzas: ".$obj_evaluacion->cant_tardanzas; ?></li>
                        <li class="list-group-item col-lg-6" id="licencias"><?php echo "Licencias: ".$obj_evaluacion->cant_licencias; ?></li>
                        <li class="list-group-item col-lg-6" id="conducta"><?php echo "Conducta: ".$obj_evaluacion->conducta; ?></li>
                        <li class="list-group-item col-lg-6" id="capacitacion"><?php echo "Capacitacion: ".$obj_evaluacion->capacitacion; ?></li>
                        <li class="list-group-item col-lg-4" id="asistencia_puntualidad"><?php echo "Asistencia y puntualidad: ".$obj_evaluacion->asist_puntualidad; ?></li>
                        <li class="list-group-item col-lg-4" id="antiguedad_categoria"><?php echo "Antiguedad en el cargo: ".$antiguedad_cargo; ?></li>
                        <li class="list-group-item col-lg-4" id="antiguedad_escalafon"><?php echo "Antiguedad en el escalafon: ".$antiguedad_evaluacion; ?></li>
                        <li class="list-group-item col-lg-12" id="observaciones"><?php echo "Observaciones: ".$obj_evaluacion->observaciones; ?></li>
                        <li class="list-group-item col-lg-12" id="puntaje_total"><?php echo "Puntaje total: ".$obj_evaluacion->puntaje_total; ?></li>
                      </ul>
                  </div>
                  <table class="table" id="items">
                    <tr><th>Nombre item</th><th>Descripci&oacute;n</th><th>Nota</th></tr>
                    <?php
                    foreach($arr_items as $item)
                    {
                        if($item->nota_item<4)
                        {
                            echo '<tr class="danger"><td>'.$item->nombre.'</td><td>'.$item->descripcion.'</td><td>'.$item->nota_item.'</td></tr>';    
                        }
                        else
                        {
                            echo '<tr><td>'.$item->nombre.'</td><td>'.$item->descripcion.'</td><td>'.$item->nota_item.'</td></tr>';    
                        }
                    }
                    ?>
                  </table>
              </div>
          </div><!-- End div container -->    
        </body>
        
    <?php
    }
}
?>
</html>
<script type="text/javascript">
$(function()
{
    window.print();
});
</script>