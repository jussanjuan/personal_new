Ext.define('personal.view.ConfirmarSolicitudLicencia', {
	extend: 'Ext.window.Window',
	alias: 'widget.ConfirmarSolicitudLicencia',
    reference: 'ConfirmarSolicitudLicencia',
	title: 'Confirmar solicitud de licencia',
	defaultType: 'textfield',
    closeAction: 'destroy',
    border: false,
    modal: true,
    width: 500,
    frame: true,
    layout: 'fit',
    //minHeight: 500,
    //maxHeight: 1000,
    fieldDefaults: {
       	anchor: '100%',
       	labelAlign: 'top'
    },

    items: [{
        xtype: 'form',
        bodyPadding: 5,
        method: 'POST',
        id: 'formConfirmarSolicitudLicencia',
        items: [{
            xtype: 'combo',
            name: 'tipoLicencia',
            id: 'confirmarTipoLicencia',
            displayField: 'nombre',
            valueField: 'id',
            fieldLabel: 'Licencia',
            store: Ext.create('store.LicenciaTipoStore'),
            validateOnChange: 'true',
            validator: function(value){
            if (!value)
                return ("Seleccione licencia");
            else
                return true;
            },
            anchor: '95%',
            allowBlank: false,
            readOnly: true
        },
        {
            xtype: 'textfield',
            id: 'confirmarLegajoSolicitudLicencia',
            name: 'legajo',
            fieldLabel: 'Legajo',
            readOnly: true,
            allowBlank: false,
            anchor: '95%'
            
        },{
            id: 'confirmarSolicitudLicenciaDesde',
    	    name: 'solicitudLicenciaDesde',
            xtype: 'datefield',
            fieldLabel: 'Desde',
            format: 'd/m/Y',
            //minValue: new Date(),
            value: new Date(),
            anchor: '95%',
            allowBlank: false
        }, {
            id: 'confirmarSolicitudLicenciaHasta',
            name: 'solicitudLicenciaHasta',
            xtype: 'datefield',
            fieldLabel: 'Hasta',
            format: 'd/m/Y',
            allowBlank: false,
            //minValue: new Date(),
            anchor: '95%'
        }, {
            id: 'confirmarSolicitudTotalDias',
            xtype: 'textfield',
            name: 'solicitudTotalDias',
            //maskRe: new RegExp(/^\d{1}$/),
            fieldLabel: 'Total de dias',
            readOnly: true,
            allowBlank: false,
            anchor: '95%'
        }, {
            id: 'confirmarSolicitudCausa',
            xtype: 'textfield',
            name: 'solicitudCausa',
            fieldLabel: 'Causa',
            anchor: '95%'
        }, {
            id: 'confirmarSolicitudExpediente',
            xtype: 'textfield',
            name: 'solicitudExpediente',
            //maskRe: new RegExp(/^\d{1}$/),
            fieldLabel: 'Expediente',
            anchor: '95%'
        }, {
            id: 'confirmarSolicitudObservaciones',
            xtype: 'textareafield',
            grow: true,
            anchor: '95%',
            name: 'solicitudObservaciones',
            fieldLabel: 'Observaciones'
        },{
            id: 'confirmarSolicitudSolicitante',
            xtype: 'textfield',
            name: 'solicitudSolicitante',
            fieldLabel: 'Solicitante',
            anchor: '95%',
            allowBlank: false
        },{
            id: 'confirmarDomicilioSolicitante',
            xtype: 'textfield',
            name: 'domicilioSolicitante',
            fieldLabel: 'Domicilio',
            anchor: '95%',
            allowBlank: false
        }/*,{
            xtype: 'timefield',
            id: 'confirmapersonaloraSolicitud',
            name: 'horaSolicitud',
            fieldLabel: 'Horario llamado',
            minValue: '6:00 AM',
            maxValue: '1:00 PM',
            value: '9:00 AM',
            increment: 5,
            anchor: '95%',
            invalidText: 'El formato no es correcto!',
            allowBlank: false,
            format: 'H:i'
        }*/,{
            id: 'confirmarPierdePresentismoSolicitud',
            name: 'pierdePresentismoSolicitud',
            xtype: 'checkbox',
            boxLabel: 'Pierde presentismo',
            checked: false
        },{
            id: 'habiles_confirmar_licencia',
            name: 'habiles',
            xtype: 'checkbox',
            boxLabel: 'Habiles',
            checked: false
        }]
    }],

    buttons: [{
    	text: 'Guardar',
        action: 'confirmarSaveSolicitudLicencia',
        iconCls: 'save2'
        
    }, {
    	text: 'Cancelar',
        action: 'cancelar',
        iconCls: 'cancel2'
        
    }],

	initComponent: function() {

		this.callParent(arguments);
	}
});