Ext.define('personal.view.FeriadosGrilla', 
{
    extend: 'Ext.grid.Panel',
    xtype: 'FeriadosGrilla',
    requires: [
        'personal.store.FeriadosStore'
    ],
    store: {
        type: 'FeriadosStore'
    },
    reference: 'FeriadosGrilla',
    plugins: {
        ptype: 'rowediting',
        clicksToEdit: 2,
        id: 'pluginRowEditing'
    },
    tbar: [{
            text: 'Agregar',
            iconCls: 'add',
            tooltip: 'Agregar un feriado',
            action: 'addFeriado'
        }, {
            //id: 'deleteFeriado',
            text: 'Eliminar',
            iconCls: 'delete',
            name: 'eliminarFeriado',
            tooltip: 'Eliminar feriado',
            action: 'eliminarFeriado',
            disabled: true
        }, {
            name: 'guardarFeriados',
            text: 'Guardar cambios',
            iconCls: 'save2',
            tooltip: 'Guardar cambios',
            action: 'saveFeriados'
        }],
    flex: 1,
    columns:[
        {text: 'Feriado', dataIndex: 'ocasion', flex: 1, editor: {xtype: 'textfield', allowBlank: false}},
        {text: 'Fecha', dataIndex: 'fecha', renderer: Ext.util.Format.dateRenderer('d-m-Y'), width: 70, flex: 1, editor: 
            {xtype: 'datefield',
                allowBlank: false,
                format: 'd-m-Y'
            }
        }
    ]
});

Ext.define('personal.view.Feriados', {
	extend: 'Ext.window.Window',
	alias: 'widget.Feriados',
	title: 'Feriados',
    id: 'idVentanaFeriados',
    reference: 'ventanaFeriados',
    frame: true,
    closeAction: 'destroy',
    modal: true,
    layout: 'fit',
    width: 900,
    minHeight: 180,
    items: [{
        xtype: 'FeriadosGrilla'
    }],
    buttons: [{
        text: 'Cerrar',
        action: 'cancelaFeriados',
        iconCls: 'cancel2'
        
    }],
    initComponent: function() 
    {
        this.callParent(arguments);
	}
});