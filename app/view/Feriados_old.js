Ext.require([
    'Ext.grid.*',
    'Ext.data.*',
    'Ext.util.*',
    'Ext.state.*',
    'Ext.form.*'
]);


Ext.define('personal.view.Feriados', {
	extend: 'Ext.grid.Panel',
	xtype: 'Feriados',
    requires: [
        'personal.store.FeriadosStore',
    ],
    store: {
        type: 'FeriadosStore'
    },
	title: 'Editar feriados',
    id: 'gridFeriados',
            
    border: false,
    flex: 1,
	
	columns:[
        {header: 'Feriado', dataIndex: 'ocasion', flex: 1, editor: {allowBlank: false}},
        {header: 'Fecha', dataIndex: 'fecha', renderer: Ext.util.Format.dateRenderer('d-m-Y'), width: 70, flex: 1, editor: {xtype: 'datefield',
                allowBlank: false,
                format: 'd-m-Y'
                }}
        ],
        tbar: [{
            //text: 'Agregar feriado',
            iconCls: 'add',
            tooltip: 'Agregar un feriado',
            handler : function() {
                rowEditing.cancelEdit();
                this.up().down('#guardarFeriados').setDisabled(false);
                var r = Ext.create('personal.model.Feriado', 
                        {id: 0,
                        anio: '2017',
                        mes: '12',
                        dia: '12',
                        ocasion: 'Nombre feriado',
                        fecha: Ext.Date.clearTime(new Date())
                                                        
                });
               Ext.getStore('FeriadosStore').insert(0, r);
               rowEditing.startEdit(0, 0);
                
            }
        }, {
            itemId: 'eliminarFeriado',
            //text: 'Eliminar feriado',
            iconCls: 'delete',
            tooltip: 'Eliminar feriado',
            handler: function() {
                
                var sm = this.up('Feriados').getSelectionModel();
                var arr = sm.getSelection();
                this.up().down('#guardarFeriados').setDisabled(false);
                //console.log(this.up('Feriados').getStore());
                //console.log(this.up('Feriados').getSelectionModel().view);
                this.up('Feriados').getSelectionModel().view.addRowCls(2, 'hidden');
                rowEditing.cancelEdit();
                Ext.getStore('FeriadosStore').remove(sm.getSelection());
                if (Ext.getStore('FeriadosStore').getCount() > 0) {
                    sm.select(0);
                }
                
            },
            disabled: true
        }, {
            itemId: 'guardarFeriados',
            //text: 'Guardar cambios',
            iconCls: 'save2',
            tooltip: 'Guardar cambios',
            handler: function() 
            {
                
                console.log('*** Save Feriados');
                var modified = this.up('Feriados').getStore().getModifiedRecords();
                
                //Si hubo modificaciones, guardo los cambios
                //Hago un arreglo de los registros eliminados
                var eliminados = this.up('Feriados').getStore().removed;
                var recordsToDelete = [];
                for(i=0;i<eliminados.length;i++)
                {
                    //console.log(eliminados[i].internalId);
                    recordsToDelete.push(Ext.apply({id:eliminados[i].internalId},eliminados[i].data));
                }
                recordsToDelete = Ext.encode(recordsToDelete);
                
                if(!Ext.isEmpty(modified) || !Ext.isEmpty(eliminados))
                {
                    var recordsToSend = [];
                    Ext.each(modified, function(record) 
                    { 
                        console.log(record.data);
                        recordsToSend.push(Ext.apply({id:record.id},record.data));
                    });
                    this.up('Feriados').el.mask('Guardando…', 'x-mask-loading'); 
                    
                        
                    recordsToSend = Ext.encode(recordsToSend); 
                    console.log(recordsToSend);
                    //console.log("Tipo de datos: "+typeof recordsToSend);
                    Ext.Ajax.request({ 		
                    url : 'app/proxy.feriados.php',
                    method: 'POST',
                    
                    params :{
                        records : recordsToSend,
                        del: recordsToDelete,
                        tarea: 'setFeriados'
                    },
                    scope:this,
                    success : function(response) {
                        var obj = Ext.decode(response.responseText);
                        console.log(response.responseText);
                        console.log("Entra al success: "+obj.cambios);
            			this.up('Feriados').el.unmask();
            			this.up('Feriados').getStore().commitChanges();
                        Ext.example.msg("Informaci&oacute;n", "Cambios guardados correctamente: \n"+obj.cambios+" feriados(s) modificado(s), \n"+obj.nuevos+" feriado(s) nuevo(s), \n"+obj.eliminados+" feriado(s) eliminado(s)");
                        //Ext.Msg.alert("Informacion", "Cambios guardados correctamente: \n"+obj.cambios+" feriados(s) modificado(s), \n"+obj.nuevos+" feriado(s) nuevo(s), \n"+obj.eliminados+" feriado(s) eliminado(s)");
                        
                        // Recargo nuevamente los feriados con los cambios realizados
                        Ext.Ajax.request(
                        {
                            url: 'app/proxy.feriados.php',
                            params: {
                                tarea: 'diasLaborales',
                                anio: new Date().getFullYear()
                            },
                            success:function(response){
                                //console.log(response.responseText)
                                _myApp.onCargaFeriados(response);
                            },
                            failure: function(response, opts) {
                                Ext.Msg.alert('Informacion', 'Error de servidor. <br>Estado: ' + response.status);
                                console.log('server-side failure with status code ' + response.status);
                            }
                        });
                        },
                    failure: function(response){
                        var obj = Ext.decode(response.responseText);
                        console.log("Entra al failure "+response.responseText);
                        this.up('Feriados').el.unmask();
			this.up('Feriados').getStore().commitChanges();
                        Ext.Msg.alert("Error", "Error al guardar cambios: "+obj.error);
                    }
                    });
                }
                else
                {
                    Ext.Msg.alert("Error", "No hay cambios para guardar");
                }
                
            }
            
        }],
        //plugins: [rowEditing],
        listeners: {
            
            'selectionchange': function(view, records) {
                
                this.down('#eliminarFeriado').setDisabled(!records.length);
                
            }
        }/*,

	initComponent: function() {

		this.callParent(arguments);
	}*/
});
