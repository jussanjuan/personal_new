Ext.define('personal.view.ModificarLicencia', {
	extend: 'Ext.window.Window',
	alias: 'widget.ModificarLicencia',
	title: 'Modificar datos de licencia',
	defaultType: 'textfield',
    closeAction: 'destroy',
    border: false,
    modal: true,
    width: 500,
    frame: true,
    layout: 'fit',
    //minHeight: 500,
    //maxHeight: 1000,
    fieldDefaults: {
       	anchor: '100%',
       	labelAlign: 'top'
    },
    requires: [
        'personal.store.LicenciaTipoStore'
    ],
    items: [{
        xtype: 'form',
        bodyPadding: 5,
        method: 'POST',
        id: 'formModificarLicencia',
        items: [{
            xtype: 'combo',
            name: 'tipoLicencia',
            id: 'editTipoLicencia',
            displayField: 'nombre',
            valueField: 'id',
            fieldLabel: 'Licencia',
            store: Ext.create('personal.store.LicenciaTipoStore'),
            validateOnChange: 'true',
            validator: function(value){
            if (!value)
                return ("Seleccione licencia");
            else
                return true;
            },
            anchor: '95%',
            allowBlank: false,
            readOnly: true
        },
        {
            xtype: 'textfield',
            id: 'editLegajoLicencia',
            name: 'legajo',
            fieldLabel: 'Legajo',
            readOnly: true,
            allowBlank: false,
            anchor: '95%'
            
        },{
            id: 'editCausa',
            xtype: 'textfield',
            name: 'editCausa',
            fieldLabel: 'Causa',
            anchor: '95%'
        },{
            id: 'editExpediente',
            xtype: 'textfield',
            name: 'editExpediente',
            //maskRe: new RegExp(/^\d{1}$/),
            fieldLabel: 'Expediente',
            anchor: '95%'
        },{
            id: 'editObservaciones',
            xtype: 'textareafield',
            grow: true,
            anchor: '95%',
            name: 'editObservaciones',
            fieldLabel: 'Observaciones'
        },{
            id: 'editPierdePresentismo',
            name: 'editPierdePresentismo',
            xtype: 'checkbox',
            boxLabel: 'Pierde presentismo',
            checked: false
        },{
            id: 'idEditLicencia',
            name: 'idEditLicencia',
            xtype: 'hidden'
        }]
    }],

    buttons: [{
    	text: 'Guardar',
        action: 'modificarSaveLicencia',
        iconCls: 'save2'
        
    }, {
    	text: 'Cancelar',
        action: 'cancelar',
        iconCls: 'cancel2'
        
    }],

	initComponent: function() {

		this.callParent(arguments);
	}
});