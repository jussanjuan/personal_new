Ext.define('personal.view.PersonalAgregarCargo', {
	extend: 'Ext.window.Window',
	alias: 'widget.PersonalAgregarCargo',
	title: 'Agregar Cargo',
	defaultType: 'textfield',
    closeAction: 'destroy',
    requires: [
        'personal.store.CargosStore'
    ],
    reference: 'PersonalAgregarCargo',
    border: false,
    modal: true,
    width: 350,
    items: [{
        xtype: 'form',
        fieldDefaults: {
        anchor: '90%',
        labelAlign: 'top'
        },
        bodyPadding: 5,
        method: 'POST',

        items: [{
            xtype: 'textfield',
            name: 'acuerdo',
            fieldLabel: 'Acuerdo',
            validateOnChange: 'true',
            validator: function(value){
            if (!value)
                return ("Este campo no puede estar vac&iacute;o");
            else
                return true;
            },
            anchor: '100%'
        }, {
    	    xtype: 'datefield',
            name: 'desde',
            id: 'fechaDesdeAgregarCargo',
    	    fieldLabel: 'Desde',
            format: 'd-m-Y',
            maskRe: new RegExp(/^\d{1}$/),
            validateOnChange: 'true',
            validator: function(value){
            if (!value)
                return ("Este campo no puede estar vac&iacute;o");
            else
                return true;
            },
            anchor: '100%'
            //value: new Date( '01/01/' + (new Date().getFullYear() + 1) )
        },{
            xtype: 'datefield',
            name: 'hasta',
            id: 'fechaHastaAgregarCargo',
            fieldLabel: 'Hasta (dejar vac&iacute;o si es actual)',
            format: 'd-m-Y',
            maskRe: new RegExp(/^\d{1}$/),
            validateOnChange: 'true',
            anchor: '100%'
            
        }, {
            xtype: 'combo',
            name: 'cargoactu',
            displayField: 'cargo',
            valueField: 'nrocargo',
            fieldLabel: 'Cargo',
            store: Ext.create('personal.store.CargosStore'),
            forceSelection: true,
            queryMode: 'local',
            validateOnChange: 'true',
            validator: function(value){
             if (!value)
                return ("Seleccione cargo");
            else
                return true;
            },
            anchor: '100%'
        }, {
            xtype: 'combo',
            name: 'sitlabo',
            fieldLabel: 'Situacion laboral',
            editable: false,
            store: [['P', 'Permanente'], ['I', 'Interino']],
            queryMode: 'local',
            validateOnChange: 'true',
            validator: function(value){
            if (!value)
                return ("Seleccione situaci&oacute;n laboral");
            else
                return true;
            },
            anchor: '100%'
        }]
    }],

    buttons: [{
    	text: 'Guardar',
        action: 'guardar',
        id: 'guardarAsigCargo',
        iconCls: 'save2'
    },
    {
    	text: 'Cancelar',
        action: 'cancelar',
        iconCls: 'cancel2'
    }
],
//funcion para obtener los campos invalidos
    getInvalidFields: function() {
            console.log('Entra al getinvalid');
           
        var invalidFields = [];
        Ext.suspendLayouts();
        var me = this.down('form');
        
        me.getForm().getFields().filterBy(function(field) {
            if (field.validate()) return;
            invalidFields.push(field);
        });
        Ext.resumeLayouts(true);
        return invalidFields;
    }, 
    
	initComponent: function() {

		this.callParent(arguments);
	}
});