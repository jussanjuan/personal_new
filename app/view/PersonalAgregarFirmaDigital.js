Ext.define('personal.view.PersonalAgregarFirmaDigital', {
	extend: 'Ext.window.Window',
	alias: 'widget.PersonalAgregarFirmaDigital',
	title: 'Agregar firma digital',
	defaultType: 'textfield',
    closeAction: 'destroy',
    border: false,
    reference: 'PersonalAgregarFirmaDigital',
    modal: true,
    width: 500,
    frame: true,
    closeAction: 'destroy',
    layout: 'fit',
    requires: [
        'personal.store.FirmaDigitalTipoStore'
    ],
    fieldDefaults: {
       	anchor: '100%',
       	labelAlign: 'top'
    },

    items: [{
        xtype: 'form',
        bodyPadding: 5,
        method: 'POST',
        id: 'formFirmaDigital',
        items: [
        {
            xtype: 'combo',
            name: 'tipoFirmaDigital',
            displayField: 'nombre',
            valueField: 'id',
            fieldLabel: 'Tipo',
            store: Ext.create('personal.store.FirmaDigitalTipoStore'),
            validateOnChange: 'true',
            validator: function(value){
            if (!value)
                return ("Seleccione tipo");
            else
                return true;
            },
            anchor: '95%'
        },{
            xtype: 'textfield',
            id: 'descripcionFirmaDigital',
            name: 'descripcionFirmaDigital',
            fieldLabel: 'Descripci&oacute;n',
            allowBlank: true,
            anchor: '95%'
            
        },{
            id: 'fechaAlta',
    	    name: 'fechaAlta',
            xtype: 'datefield',
            fieldLabel: 'Fecha alta',
            format: 'd/m/Y H:i:s',
            //minValue: new Date(),
            value: new Date(),
            allowBlank: false,
            anchor: '95%'
        }]
    }],

    buttons: [{
    	text: 'Guardar',
        action: 'saveFirmaDigital',
        id: 'saveFirmaDigital',
        iconCls: 'save2'
        
    }, {
    	text: 'Cancelar',
        action: 'cancelar',
        iconCls: 'cancel2'
        
    }],
    //funcion para obtener los campos invalidos
    getInvalidFields: function() {
        
           
        var invalidFields = [];
        Ext.suspendLayouts();
        var me = this.down('form');
        
        me.getForm().getFields().filterBy(function(field) {
            if (field.validate()) return;
            invalidFields.push(field);
        });
        Ext.resumeLayouts(true);
        return invalidFields;
    },
	initComponent: function() {

		this.callParent(arguments);
	}
});