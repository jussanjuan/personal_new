Ext.define('personal.view.PersonalAgregarLugar', {
	extend: 'Ext.window.Window',
	alias: 'widget.PersonalAgregarLugar',
	title: 'Agregar Lugar',
	//frame: true,
    //border: false,
    //flex: 1,
    reference: 'PersonalAgregarLugar',
    requires: [
        'personal.store.LugarStore'
    ],
    defaultType: 'textfield',
    closeAction: 'destroy',
    border: false,
    modal: true,
    

    width: 350,
    resizable: false,
    items: [{
        xtype: 'form',
        bodyPadding: 5,
        method: 'POST',
        fieldDefaults: {
        anchor: '50%',
        labelAlign: 'top'
        },
        items: [{
    	    xtype: 'datefield',
            name: 'desde',
            id: 'fechaDesdeAgregarLugar',
    	    fieldLabel: 'Desde',
            format: 'd-m-Y',
            maskRe: new RegExp(/^\d{1}$/),
            validateOnChange: 'true',
            validator: function(value){
            if (!value)
                return ("Este campo no puede estar vac&iacute;o");
            else
                return true;
            },
            anchor: '100%'
            
        },{
            xtype: 'datefield',
            name: 'hasta',
            id: 'fechaHastaAgregarLugar',
            fieldLabel: 'Hasta (dejar vac&iacute;o si es actual)',
            format: 'd-m-Y',
            maskRe: new RegExp(/^\d{1}$/),
            validateOnChange: 'true',
            anchor: '100%'
            
        }, {
            xtype: 'combo',
            name: 'lugaractu',
            displayField: 'nombrelugar',
            valueField: 'nrolugar',
            fieldLabel: 'Lugar',
            store: Ext.create('personal.store.LugarStore'),
            forceSelection: true,
            queryMode: 'local',
            validateOnChange: 'true',
            validator: function(value){
            if (!value)
                return ("Seleccione lugar");
            else
                return true;
            },
            anchor: '100%'
        }]
    }],

    buttons: [{
    	text: 'Guardar',
        action: 'guardar',
        id: 'guardarAsigLugar',
        iconCls: 'save2'
    }, {
    	text: 'Cancelar',
        action: 'cancelar',
        iconCls: 'cancel2'
    }],
//funcion para obtener los campos invalidos
    getInvalidFields: function() {
            console.log('Entra al getinvalid');
           
        var invalidFields = [];
        Ext.suspendLayouts();
        var me = this.down('form');
        
        me.getForm().getFields().filterBy(function(field) {
            if (field.validate()) return;
            invalidFields.push(field);
        });
        Ext.resumeLayouts(true);
        return invalidFields;
    },

	initComponent: function() {

		this.callParent(arguments);
	}
});