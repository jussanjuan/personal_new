Ext.define('personal.view.PersonalAgregarSancion', {
	extend: 'Ext.window.Window',
	alias: 'widget.PersonalAgregarSancion',
	title: 'Agregar sanci&oacute;n',
	defaultType: 'textfield',
    closeAction: 'destroy',
    border: false,
    modal: true,
    reference: 'PersonalAgregarSancion',
    requires: [
        'personal.store.SancionStore'
    ],
    width: 350,
    resizable: false,
    items: [{
        xtype: 'form',
        bodyPadding: 5,
        method: 'POST',
        fieldDefaults: {
        anchor: '50%',
        labelAlign: 'top'
        },
        items: [{
            xtype: 'combo',
            name: 'tipoSancion',
            displayField: 'nombre',
            valueField: 'id',
            fieldLabel: 'Sancion',
            store: Ext.create('personal.store.SancionStore'),
            validateOnChange: 'true',
            validator: function(value){
            if (!value)
                return ("Seleccione sanci&oacute;n");
            else
                return true;
            },
            anchor: '100%'
        }, {
    	    xtype: 'datefield',
            name: 'desde',
            id: 'fechaDesdeAgregarSancion',
    	    fieldLabel: 'Desde',
            format: 'd-m-Y',
            maskRe: new RegExp(/^\d{1}$/),
            validateOnChange: 'true',
            validator: function(value){
            if (!value)
                return ("Este campo no puede estar vac&iacute;o");
            else
                return true;
            },
            anchor: '100%'
            
        },{
            xtype: 'datefield',
            name: 'hasta',
            id: 'fechaHastaAgregarSancion',
            fieldLabel: 'Hasta',
            format: 'd-m-Y',
            maskRe: new RegExp(/^\d{1}$/),
            validateOnChange: 'true',
            validator: function(value){
            if (!value)
                return ("Este campo no puede estar vac&iacute;o");
            else
                return true;
            },
            anchor: '100%'
            
        }, {
            xtype: 'textfield',
            name: 'dispuesta_por',
            id: 'dispuesta_por',
            fieldLabel: 'Dispuesta por',
            validateOnChange: 'true',
            validator: function(value){
            if (!value)
                return ("Este campo no puede estar vac&iacute;o");
            else
                return true;
            },
            anchor: '100%'
            
        }, {
            xtype: 'textfield',
            name: 'expedienteSancion',
            id: 'expedienteSancion',
            fieldLabel: 'Nro. exp.',
            anchor: '100%'
            
        },{
            xtype: 'textareafield',
            grow: false,
            anchor: '100%',
            name: 'observacionesSanciones',
            fieldLabel: 'Observaciones'
        }]
    }],

    buttons: [{
    	text: 'Guardar',
        action: 'guardar',
        id: 'guardarAsigSancion',
        iconCls: 'save2'
    }, {
    	text: 'Cancelar',
        action: 'cancelar',
        iconCls: 'cancel2'
    }],
//funcion para obtener los campos invalidos
    getInvalidFields: function() {
            console.log('Entra al getinvalid');
           
        var invalidFields = [];
        Ext.suspendLayouts();
        var me = this.down('form');
        
        me.getForm().getFields().filterBy(function(field) {
            if (field.validate()) return;
            invalidFields.push(field);
        });
        Ext.resumeLayouts(true);
        return invalidFields;
    },

	initComponent: function() {

		this.callParent(arguments);
	}
});