Ext.define('personal.view.PersonalAgregarTitulo', {
	extend: 'Ext.window.Window',
	alias: 'widget.PersonalAgregarTitulo',
	title: 'Agregar Titulo',
	reference: 'PersonalAgregarTitulo',
    defaultType: 'textfield',
    closeAction: 'destroy',
    border: false,
    modal: true,
    fieldDefaults: {
       	anchor: '90%',
       	labelAlign: 'top'
    },
    requires: [
        'personal.store.TipoEstudioStore'
    ],
    width: 370,

    items: [{
        xtype: 'form',
        bodyPadding: 5,
        method: 'POST',

        items: [{
            xtype: 'combo',
            name: 'titulo',
            displayField: 'titulo',
            valueField: 'codestudio',
            fieldLabel: 'Titulo',
            store: Ext.create('personal.store.TipoEstudioStore'),
            forceSelection: true,
            queryMode: 'local',
            validateOnChange: 'true',
            validator: function(value){
            if (!value)
                return ("Seleccione titulo");
            else
                return true;
            },
            anchor: '100%'
        },
        {
    	    xtype: 'datefield',
            name: 'fechaTitulo',
    	    fieldLabel: 'Fecha',
            format: 'd-m-Y',
            maskRe: new RegExp(/^\d{1}$/),
            validateOnChange: 'true',
            validator: function(value){
            if (!value)
                return ("Este campo no puede estar vac&iacute;o");
            else
                return true;
            },
            anchor: '100%'
            
        },
        {
    	    xtype: 'textfield',
            name: 'establecimiento',
    	    fieldLabel: 'Establecimiento',
            validateOnChange: 'true',
            validator: function(value){
            if (!value)
                return ("Este campo no puede estar vac&iacute;o");
            else
                return true;
            },
            anchor: '100%'
            
        }]
    }],

    buttons: [{
    	text: 'Guardar',
        action: 'guardar',
        id: 'guardarAsigTitulo',
        iconCls: 'save2'
    }, {
    	text: 'Cancelar',
        action: 'cancelar',
        iconCls: 'cancel2'
    }],
//funcion para obtener los campos invalidos
    getInvalidFields: function() {
            console.log('Entra al getinvalid');
           
        var invalidFields = [];
        Ext.suspendLayouts();
        var me = this.down('form');
        
        me.getForm().getFields().filterBy(function(field) {
            if (field.validate()) return;
            invalidFields.push(field);
        });
        Ext.resumeLayouts(true);
        return invalidFields;
    },

	initComponent: function() {

		this.callParent(arguments);
	}
});