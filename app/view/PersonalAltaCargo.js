Ext.define('personal.view.PersonalAltaCargo', {
	extend: 'Ext.window.Window',
	alias: 'widget.PersonalAltaCargo',
	title: 'Alta de cargo en el sistema',
	//frame: true,
    //border: false,
    //flex: 1,
    defaultType: 'textfield',
    closeAction: 'destroy',
    border: false,
    modal: true,
    fieldDefaults: {
       	anchor: '90%',
       	labelAlign: 'top'
    },
    requires: [
        'personal.store.EscalafonStore'
    ],
    reference: 'PersonalAltaCargo',
    width: 370,

    items: [{
        xtype: 'form',
        bodyPadding: 5,
        method: 'POST',

        items: [{
            xtype: 'textfield',
            name: 'nombreCargo',
            fieldLabel: 'Nombre del cargo',
            validateOnChange: 'true',
            validator: function(value){
            if (!value)
                return ("Este campo no puede estar vac&iacute;o");
            else
                return true;
            },
            anchor: '100%'
        },{
            xtype: 'combo',
            name: 'escalafon',
            displayField: 'nombre',
            valueField: 'id',
            fieldLabel: 'Seleccione el escalaf&oacute;n',
            store: Ext.create('personal.store.EscalafonStore'),
            forceSelection: true,
            queryMode: 'local',
            validateOnChange: 'true',
            validator: function(value){
             if (!value)
                return ("Seleccione escalaf&oacute;n");
            else
                return true;
            }
        }]
    }],

    buttons: [{
    	text: 'Guardar',
        action: 'guardar',
        iconCls: 'save2'
    },
    {
    	text: 'Cancelar',
        action: 'cancelar',
        iconCls: 'cancel2'
    }],
//funcion para obtener los campos invalidos
    getInvalidFields: function() {
            console.log('Entra al getinvalid');
           
        var invalidFields = [];
        Ext.suspendLayouts();
        var me = this.down('form');
        
        me.getForm().getFields().filterBy(function(field) {
            if (field.validate()) return;
            invalidFields.push(field);
        });
        Ext.resumeLayouts(true);
        return invalidFields;
    }, 
    
	initComponent: function() {

		this.callParent(arguments);
	}
});