Ext.define('personal.view.PersonalAltaLugar', {
	extend: 'Ext.window.Window',
	alias: 'widget.PersonalAltaLugar',
	title: 'Alta de lugar en el sistema',
	reference: 'PersonalAltaLugar',
    defaultType: 'textfield',
    closeAction: 'destroy',
    border: false,
    modal: true,
    fieldDefaults: {
       	anchor: '90%',
       	labelAlign: 'top'
    },
    requires: [
        'personal.store.FueroStore'
    ],
    width: 370,

    items: [{
        xtype: 'form',
        bodyPadding: 5,
        method: 'POST',

        items: [{
            xtype: 'textfield',
            name: 'nombreLugar',
            fieldLabel: 'Nombre del lugar',
            validateOnChange: 'true',
            validator: function(value){
            if (!value)
                return ("Este campo no puede estar vac&iacute;o");
            else
                return true;
            },
            anchor: '100%'
        },
        {
            xtype: 'textfield',
            name: 'domicilioLugar',
            fieldLabel: 'Domicilio',
            anchor: '100%'
        },
        {
            xtype: 'textfield',
            name: 'telLugar',
            fieldLabel: 'Telefono',
            anchor: '100%'
        },{
            xtype: 'combo',
            name: 'fuero',
            displayField: 'nombre',
            valueField: 'id',
            fieldLabel: 'Fuero',
            store: Ext.create('personal.store.FueroStore'),
            validateOnChange: 'true',
            validator: function(value){
             if (!value)
                return ("Seleccione fuero");
            else
                return true;
            },
            anchor: '100%'
        }]
    }],

    buttons: [{
    	text: 'Guardar',
        action: 'guardar',
        iconCls: 'save2'
    },
    {
    	text: 'Cancelar',
        action: 'cancelar',
        iconCls: 'cancel2'
    }
],
//funcion para obtener los campos invalidos
    getInvalidFields: function() {
            console.log('Entra al getinvalid');
           
        var invalidFields = [];
        Ext.suspendLayouts();
        var me = this.down('form');
        
        me.getForm().getFields().filterBy(function(field) {
            if (field.validate()) return;
            invalidFields.push(field);
        });
        Ext.resumeLayouts(true);
        return invalidFields;
    }, 
    
	initComponent: function() {

		this.callParent(arguments);
	}
});