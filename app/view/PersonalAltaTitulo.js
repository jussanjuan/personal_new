Ext.define('personal.view.PersonalAltaTitulo', {
	extend: 'Ext.window.Window',
	alias: 'widget.PersonalAltaTitulo',
	title: 'Alta de titulo en el sistema',
	reference: 'PersonalAltaTitulo',
    defaultType: 'textfield',
    closeAction: 'destroy',
    border: false,
    modal: true,
    fieldDefaults: {
       	anchor: '90%',
       	labelAlign: 'top'
    },

    width: 370,

    items: [{
        xtype: 'form',
        bodyPadding: 5,
        method: 'POST',

        items: [{
            xtype: 'textfield',
            name: 'nombreTitulo',
            fieldLabel: 'Titulo',
            validateOnChange: 'true',
            validator: function(value){
            if (!value)
                return ("Este campo no puede estar vac&iacute;o");
            else
                return true;
            },
            anchor: '100%'
        }]
    }],

    buttons: [{
    	text: 'Guardar',
        action: 'guardar',
        iconCls: 'save2'
    },
    {
    	text: 'Cancelar',
        action: 'cancelar',
        iconCls: 'cancel2'
    }
],
//funcion para obtener los campos invalidos
    getInvalidFields: function() {
            console.log('Entra al getinvalid');
           
        var invalidFields = [];
        Ext.suspendLayouts();
        var me = this.down('form');
        
        me.getForm().getFields().filterBy(function(field) {
            if (field.validate()) return;
            invalidFields.push(field);
        });
        Ext.resumeLayouts(true);
        return invalidFields;
    }, 
    
	initComponent: function() {

		this.callParent(arguments);
	}
});