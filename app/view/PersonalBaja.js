Ext.define('personal.view.PersonalBaja', {
	extend: 'Ext.window.Window',
	alias: 'widget.PersonalBaja',
	title: 'Dar de baja al agente',
	//frame: true,
    //border: false,
    //flex: 1,
    defaultType: 'textfield',
    closeAction: 'destroy',
    border: false,
    modal: true,
    fieldDefaults: {
       	anchor: '90%',
       	labelAlign: 'top'
    },

    items: [{
        xtype: 'form',
        bodyPadding: 5,
        method: 'POST',

        items: [{
            id: 'legajo',
            xtype: 'textfield',
            name: 'legajo',
            fieldLabel: 'Legajo',
            readOnly: 'true'
            
        }, {
            xtype: 'textfield',
            name: 'acordada',
            fieldLabel: 'Acordada',
            id: 'acordada_baja',
            validateOnChange: 'true',
            validator: function(value)
            {
                if (!value)
                    return ("Complete los datos de la acordada");
                else
                      return true;
            }
            
        }, {
    	    id: 'fecha_baja',
            name: 'fecha_baja',
            xtype: 'datefield',
            fieldLabel: 'Fecha de baja',
            format: 'd-m-Y',
            validateOnChange: 'true',
            validator: function(value)
            {
                if (!value)
                    return ("Seleccione fecha de baja");
                else
                      return true;
            }
        },{
            id: 'tramite_baja',
            name: 'tramite_baja',
            xtype: 'checkbox',
            boxLabel: 'Solo iniciar tr&aacute;mite',
            checked: false
        }]
    }],

    buttons: [{
    	text: 'Guardar',
        action: 'saveBaja' //'saveCompensatoria'
    }, {
    	text: 'Cancelar',
        action: 'cancelar'//'cancelar'
    }],

	initComponent: function() {

		this.callParent(arguments);
	}
});