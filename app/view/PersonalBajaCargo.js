
Ext.define('personal.view.PersonalBajaCargo', {
	extend: 'Ext.window.Window',
	alias: 'widget.PersonalBajaCargo',
	title: 'Eliminar un cargo del sistema',
	reference: 'PersonalBajaCargo',
    defaultType: 'textfield',
    closeAction: 'destroy',
    border: false,
    modal: true,
    fieldDefaults: {
       	anchor: '90%',
       	labelAlign: 'top'
    },
    requires: [
        'personal.store.CargosStore'
    ],
    width: 370,

    items: [{
        xtype: 'form',
        bodyPadding: 5,
        method: 'POST',
        
        items: [{
            xtype: 'combo',
            name: 'cargoElimin',
            displayField: 'cargo',
            valueField: 'id',
            fieldLabel: 'Seleccione el cargo',
            store: Ext.create('personal.store.CargosStore'),
            forceSelection: true,
            queryMode: 'local',
            validateOnChange: 'true',
            validator: function(value){
             if (!value)
                return ("Seleccione cargo");
            else
                return true;
            },
            anchor: '100%'
        }]
    }],

    buttons: [{
    	text: 'Eliminar',
        action: 'eliminar',
        iconCls: 'save2'
    },
    {
    	text: 'Cancelar',
        action: 'cancelar',
        iconCls: 'cancel2'
    }
],
//funcion para obtener los campos invalidos
    getInvalidFields: function() {
            console.log('Entra al getinvalid');
           
        var invalidFields = [];
        Ext.suspendLayouts();
        var me = this.down('form');
        
        me.getForm().getFields().filterBy(function(field) {
            if (field.validate()) return;
            invalidFields.push(field);
        });
        Ext.resumeLayouts(true);
        return invalidFields;
    }, 
    
	initComponent: function() {

		this.callParent(arguments);
	}
});