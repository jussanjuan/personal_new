Ext.define('personal.view.PersonalBajaLugar', {
	extend: 'Ext.window.Window',
	alias: 'widget.PersonalBajaLugar',
	title: 'Eliminar un lugar del sistema',
	reference: 'PersonalBajaLugar',
    defaultType: 'textfield',
    closeAction: 'destroy',
    border: false,
    modal: true,
    fieldDefaults: {
       	anchor: '90%',
       	labelAlign: 'top'
    },

    width: 370,

    items: [{
        xtype: 'form',
        bodyPadding: 5,
        method: 'POST',

        items: [{
            xtype: 'combo',
            name: 'lugarElimin',
            displayField: 'nombrelugar',
            valueField: 'id',
            fieldLabel: 'Seleccione lugar',
            store: 'LugarStore',
            queryMode: 'local',
            validateOnChange: 'true',
            validator: function(value){
             if (!value)
                return ("Seleccione lugar");
            else
                return true;
            },
            anchor: '100%'
        }]
    }],

    buttons: [{
    	text: 'Eliminar',
        action: 'eliminar',
        iconCls: 'save2'
    },
    {
    	text: 'Cancelar',
        action: 'cancelar',
        iconCls: 'cancel2'
    }
],
//funcion para obtener los campos invalidos
    getInvalidFields: function() {
            console.log('Entra al getinvalid');
           
        var invalidFields = [];
        Ext.suspendLayouts();
        var me = this.down('form');
        
        me.getForm().getFields().filterBy(function(field) {
            if (field.validate()) return;
            invalidFields.push(field);
        });
        Ext.resumeLayouts(true);
        return invalidFields;
    }, 
    
	initComponent: function() {

		this.callParent(arguments);
	}
});