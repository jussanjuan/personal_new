Ext.define('personal.view.PersonalCapacitaciones', 
{
    extend: 'Ext.grid.Panel',
    xtype: 'PersonalCapacitaciones',
    requires: [
        'personal.store.historial.HCapacitacionStore',
        'personal.view.PersonalAgregarCapacitacion',
        'personal.view.PersonalModificarCapacitacion'
    ],
    store: {
        type: 'HCapacitacionStore'
    },
    reference: 'PersonalCapacitaciones',
    title: 'Listado de capacitaciones del Agente',
    itemId: 'PersonalCapacitaciones',
    flex: 1,
    tbar:[],
    columns: [
        {header: 'Tipo', dataIndex: 'tipo_capacitacion', flex: 1},
        {header: 'Nombre', dataIndex: 'nombre', flex: 3},
		{header: 'Fecha', dataIndex: 'fecha', renderer: Ext.util.Format.dateRenderer('d-m-Y'), flex: 1},
        {header: 'Cant. horas', dataIndex: 'cant_horas', flex: 0.5},
        {header: 'Modalidad', dataIndex: 'modalidad', flex: 1},
        {header: 'Origen', dataIndex: 'origen', flex: 2},
        {header: 'Inherencia', dataIndex: 'inherencia', flex: 0.5},
        {header: 'Evaluacion', dataIndex: 'evaluacion', flex: 1},
        {header: 'Nota', dataIndex: 'nota_evaluacion', flex: 0.5}/*,
        {header: 'Obs.', dataIndex: 'observaciones', flex: 2}*/
	],
    listeners: 
    {
            
        'selectionchange': function(view, records) 
        {
            if(AppGlobals.permisos[0].pesta_legajo_capacitaciones_editar == 1)//if(AppGlobals.permiso == "1" || AppGlobals.rrhh=="si" || AppGlobals.legajoUser=="1982" || AppGlobals.legajoUser=="1341")
            {
                this.down('#eliminarCapacitacion').setDisabled(!records.length);
                this.down('#editarCapacitacion').setDisabled(!records.length);
                //Guardo el registro seleccionado, para rellenar la ventana con los datos si el usuario quiere editar la capacitacion
                AppGlobals.recordEditCapacitacion = records;
                
            }
        }
    },
    initComponent: function() 
    {
        var items=[]; //Arreglo de items
        //Si el permiso es igual a 1, el usuario es administrador, o es analia perez o RpersonalH. Lleno el arreglo con todos los botones
        if(AppGlobals.permisos[0].pesta_legajo_capacitaciones_editar == 1)//if(AppGlobals.permiso == "1" || AppGlobals.rrhh=="si" || AppGlobals.legajoUser=="1982" || AppGlobals.legajoUser=="1341") 
        {
            items.push(
            {
            text: 'Agregar',
            action: 'agregarCapacitacion',
            iconCls: 'agregar',
            tooltip: 'Asigna una nueva capacitaci&oacute;n para el agente'
            },'-',{
                text: 'Editar',
                action: 'editarCapacitacion',
                itemId: 'editarCapacitacion',
                iconCls: 'folderedit',
                disabled: true,
                tooltip: 'Editar una capacitaci&oacute;n'
            },'-',{
                text: 'Eliminar',
                action: 'eliminarCapacitacion',
                itemId: 'eliminarCapacitacion',
                iconCls: 'quitar',
                disabled: true,
                tooltip: 'Quita una capacitaci&oacute;n asignada al agente'
            });
        //Aplico el arreglo de botones a la vista
        Ext.apply(this, {tbar: items});    
        }
        this.callParent(arguments);
    }
});