Ext.define('personal.view.PersonalCargos', 
{
    extend: 'Ext.grid.Panel',
    xtype: 'PersonalCargos',
    requires: [
        'personal.store.historial.HCargosStore',
        'personal.view.PersonalAgregarCargo',
        'personal.view.PersonalAltaCargo',
        'personal.view.PersonalModificarCargo',
        'personal.view.PersonalBajaCargo'
    ],
    store: {
        type: 'HCargosStore'
    },
    reference: 'PersonalCargos',
    title: 'Cargos del Agente',
    //id: 'PersonalCargos',
    flex: 1,
    tbar:[],
    columns: [
		{text: 'Cargo', dataIndex: 'cargo', flex: 1},
		{text: 'Acuerdo', dataIndex: 'acuerdo', width: 60},
		{text: 'Desde', dataIndex: 'desde', renderer: Ext.util.Format.dateRenderer('d-m-Y'), width: 70},
        {text: 'Hasta', dataIndex: 'hasta', renderer: function(val)
        {
            if(val!='Actualidad')
            {
                var fecha = new Date(val);
                var date = Ext.Date.add(new Date(fecha), Ext.Date.DAY,1);//Incremento un dia, debido a que un bug de Google Chrome decrementa la fecha
                var date = Ext.util.Format.date(date, 'd-m-Y');
                return date;
            }
            else
            {
                return val;
            }
        }, width: 70},
		{text: 'Sit. Lab.', dataIndex: 'sitlabo', width: 45}
	],
    initComponent: function() 
    {
        var items=[]; //Arreglo de items

        //Si el permiso aplica, lleno el arreglo con todos los botones
        if(AppGlobals.permisos[0].pesta_legajo_cargos_editar == 1)//if(AppGlobals.permiso == "1") 
        {
            items.push(
            {
                text: 'Agregar',
                action: 'agregar',
                iconCls: 'agregar',
                tooltip: 'Asigna un nuevo cargo para el agente'
            },'-',{
                    text: 'Eliminar',
                    action: 'eliminarCargo',
                    //id: 'eliminarCargo',
                    iconCls: 'quitar',
                    disabled: true,
                    tooltip: 'Quita un cargo asignado al agente'
            },'->', {
                    text: 'Alta cargo',
                    action: 'altaCargo',
                    //id: 'altaCargo',
                    iconCls: 'add',
                    tooltip: 'Alta de cargo en el sistema'

            },'-', {
                    text: 'Baja cargo',
                    action: 'bajaCargo',
                    //id: 'bajaCargo',
                    iconCls: 'delete',
                    tooltip: 'Elimina un cargo del sistema'

            },'-', {
                text: 'Modificar cargo',
                action: 'modificarCargo',
                //id: 'modificarCargo',
                iconCls: 'edit',
                tooltip: 'Modifica cargo en el sistema'

                });
        //Aplico el arreglo de botones a la vista
        Ext.apply(this, {tbar: items});
        }

        this.callParent(arguments);
        
    }
});