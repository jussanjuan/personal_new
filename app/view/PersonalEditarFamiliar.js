Ext.define('personal.view.PersonalEditarFamiliar', {
	extend: 'Ext.window.Window',
	alias: 'widget.PersonalEditarFamiliar',
	title: 'Editar familiar',
	defaultType: 'textfield',
    closeAction: 'destroy',
    reference: 'PersonalEditarFamiliar',
    resizable: false,
    border: false,
    modal: true,
    width: 400,
    items: [{
        xtype: 'form',
        fieldDefaults: {
        anchor: '90%',
        labelAlign: 'top'
        },
        bodyPadding: 5,
        method: 'POST',

        items: [{
            xtype: 'combo',
            name: 'relacion',
            id: 'relacion_familiar',
            displayField: 'nombre',
            valueField: 'id',
            fieldLabel: 'Relaci&oacute;n',
            store: Ext.create('personal.store.RelacionStore'),
            forceSelection: true,
            queryMode: 'local',
            validateOnChange: 'true',
            validator: function(value){
             if (!value)
                return ("Seleccione relacion");
            else
                return true;
            },
            anchor: '100%'
        },{
            xtype: 'textfield',
            name: 'apellido',
            fieldLabel: 'Apellido',
            validateOnChange: 'true',
            fieldStyle: 'text-transform:uppercase',
            validator: function(value){
            if (!value)
                return ("Este campo no puede estar vac&iacute;o");
            else
                return true;
            },
            anchor: '100%'
        },{
            xtype: 'textfield',
            name: 'nombre',
            fieldLabel: 'Nombre',
            fieldStyle: 'text-transform:uppercase',
            validateOnChange: 'true',
            validator: function(value){
            if (!value)
                return ("Este campo no puede estar vac&iacute;o");
            else
                return true;
            },
            anchor: '100%'
        },{
            xtype: 'numberfield',
            name: 'dni',
            id: 'dni_fam',
            fieldLabel: 'DNI',
            minValue: 0, //prevents negative numbers
            // Remove spinner buttons, and arrow key and mouse wheel listeners
            hideTrigger: true,
            keyNavEnabled: false,
            mouseWheelEnabled: false,
            validateOnChange: 'true',
            validator: function(value){
            if (!value)
                return ("Este campo no puede estar vac&iacute;o");
            else
                return true;
            }
        },{
    	    xtype: 'datefield',
            name: 'fecha_nac',
            id: 'fecha_nac_fam',
    	    fieldLabel: 'Fecha de nacimiento',
            format: 'd-m-Y',
            maskRe: new RegExp(/^\d{1}$/),
            /*validateOnChange: 'true',
            validator: function(value){
            if (!value)
                return ("Este campo no puede estar vac&iacute;o");
            else
                return true;
            },*/
            anchor: '100%'
            //value: new Date( '01/01/' + (new Date().getFullYear() + 1) )
        },{
            xtype: 'combo',
            name: 'nivel_estudios',
            id: 'nivel_estudios',
            displayField: 'nombre',
            valueField: 'id',
            fieldLabel: 'Nivel de estudios',
            store: Ext.create('personal.store.NivelEstudiosStore'),
            forceSelection: true,
            queryMode: 'local',
            /*validateOnChange: 'true',
            validator: function(value){
             if (!value)
                return ("Seleccione estudios");
            else
                return true;
            },*/
            anchor: '100%'
        }, {
            xtype: 'checkboxgroup',
            //fieldLabel: 'Varios',
            // Distribute controls across 3 even columns, filling each column
            // from top to bottom before starting the next column
            columns: 2,
            allowBlank: 'false',
            msgTarget: 'qtip',
            vertical: true,
            items: [
                {boxLabel: 'Cert. inicio clases', name: 'certificado_inicio', uncheckedValue: 'false', id: 'certificado_inicio'},
                {boxLabel: 'Cert. fin clases', name: 'certificado_fin', uncheckedValue: 'false', id: 'certificado_fin'},
                {boxLabel: 'Guardapolvo', name: 'guardapolvo', uncheckedValue: 'false', id: 'guardapolvo'},
                {boxLabel: 'Discapacitado', name: 'discapacitado', uncheckedValue: 'false', id: 'discapacitado'}
            ]
        }]
    }],

    buttons: [{
    	text: 'Guardar',
        action: 'guardarEditFamiliar',
        id: 'guardarEditFamiliar',
        iconCls: 'save2'
    },
    {
    	text: 'Cancelar',
        action: 'cancelar',
        iconCls: 'cancel2'
    }
],
//funcion para obtener los campos invalidos
    getInvalidFields: function() {
            console.log('Entra al getinvalid');
           
        var invalidFields = [];
        Ext.suspendLayouts();
        var me = this.down('form');
        
        me.getForm().getFields().filterBy(function(field) {
            if (field.validate()) return;
            invalidFields.push(field);
        });
        Ext.resumeLayouts(true);
        return invalidFields;
    }, 
    
	initComponent: function() {

		this.callParent(arguments);
	}
});