Ext.define('personal.view.PersonalEditarHistoriaClinica', {
	extend: 'Ext.window.Window',
	alias: 'widget.PersonalEditarHistoriaClinica',
	title: 'Editar registro historia cl&iacute;nica',
	defaultType: 'textfield',
    closeAction: 'destroy',
    border: false,
    modal: true,
    width: 500,
    frame: true,
    closeAction: 'destroy',
    layout: 'fit',
    //minHeight: 500,
    //maxHeight: 1000,
    fieldDefaults: {
       	anchor: '100%',
       	labelAlign: 'top'
    },

    items: [{
        xtype: 'form',
        bodyPadding: 5,
        method: 'POST',
        id: 'formHistoriaClinica',
        items: [{
            xtype: 'textfield',
            id: 'legajoEditarHistoriaClinica',
            name: 'legajo',
            fieldLabel: 'Legajo',
            readOnly: true,
            allowBlank: false,
            anchor: '95%'
            
        },{
            id: 'fechaHoraEditarHistoriaClinica',
    	    name: 'fechaHora',
            xtype: 'datefield',
            fieldLabel: 'Fecha y hora',
            format: 'd/m/Y H:i:s',
            readOnly: true,
            value: new Date(),
            allowBlank: false,
            anchor: '95%'
        }, {
            id: 'observacionesEditarHistoriaClinica',
            xtype: 'textareafield',
            grow: true,
            anchor: '95%',
            height: 300,
            name: 'observacionesHistoriaClinica',
            fieldLabel: 'Observaciones'
        },{
            xtype: 'filefield',
            id: 'certificadoEditarHistoriaClinica',
            name: 'certificadoEditarHistoriaClinica',
            fieldLabel: 'Certificado',
            //labelWidth: 50,
            msgTarget: 'side',
            anchor: '95%',
            buttonText: 'Seleccione certificado'
        }]
    }],

    buttons: [{
    	text: 'Guardar',
        action: 'saveEditarRegistroHistoria',
        iconCls: 'save2'
        
    }, {
    	text: 'Cancelar',
        action: 'cancelar',
        iconCls: 'cancel2'
        
    }],

	initComponent: function() {

		this.callParent(arguments);
	}
});