Ext.define('personal.view.PersonalFamilia', 
{
    extend: 'Ext.Container',
    xtype: 'PersonalFamilia',
    layout: {
        type: 'vbox',       
        align: 'stretch',    
        padding: 5
    },
    title: 'Familiares',
    itemId: 'PersonalFamilia',
    flex: 1,
    requires: [
        'personal.view.PersonalGrillaFamiliares',
        'personal.store.AniosCertificadosStore',
        'personal.view.PersonalAgregarFamiliar',
        'personal.view.PersonalEditarFamiliar'
    ],        
    reference: 'PersonalFamilia',
    items:[],
    
   //funcion para obtener los campos invalidos
    getInvalidFields: function() {
        console.log('Entra al getinvalid');
           
        var invalidFields = [];
        Ext.suspendLayouts();
        var me = this.down('form');
        
        me.getForm().getFields().filterBy(function(field) {
            if (field.validate()) return;
            invalidFields.push(field);
        });
        Ext.resumeLayouts(true);
        return invalidFields;
    }, 
    initComponent: function() 
    {
        var items=[]; //Arreglo de items
        if(AppGlobals.permisos[0].pesta_legajo_familiares_editar == 1)
        {
            items.push({
                xtype: 'form',
                itemId: 'formCertificados',
                border: false,
                autoScroll: true,
                flex: 4,
                fieldDefaults: 
                {
                    anchor: '90%',
                    labelAlign: 'top'
                },
                defaultType: 'textfield',
                bodyPadding: 10,
                tbar: [{
                        itemId: 'guardarCert',
                        //text: 'Guardar',
                        iconCls: 'save',
                        scale: 'large',
                        tooltip: 'Guardar',
                        action: 'guardarCert',
                        disabled: true
                    },'-',{
                        itemId: 'btncambiarAnioCertificados',
                        //text: 'Editar Feriados',
                        action: 'cambiarAnioCertificados',
                        iconCls: 'edit_calendar',
                        scale: 'large',
                        tooltip: 'Limpiar los certificados actuales y pasar a un nuevo per&iacute;odo'
                    }],
                items:[
                    {
                    xtype: 'container',
                    layout: 'anchor',
                    flex: 2,
                    //padding: 10,
                    items: [
                    {
                        xtype: 'combo',
                        name: 'anioCertificado',
                        //itemId: 'anioCertificado',
                        width: 10,
                        editable: false,
                        valueField: 'id',
                        displayField: 'anio',
                        store: Ext.create('personal.store.AniosCertificadosStore'),
                        queryMode: 'local',
                        validateOnChange: 'true',
                        validator: function(value)
                        {
                            if (!value)
                            {
                                return ("Seleccione tipo de permiso de vista");
                            }
                            else
                            {
                                return true;
                            }
                        },
                        fieldLabel: 'Seleccione per&iacute;odo'
                        
                    },{
                        xtype: 'checkboxgroup',
                        
                        // Distribute controls across 3 even columns, filling each column
                        // from top to bottom before starting the next column
                        columns: 3,
                        allowBlank: 'false',
                        msgTarget: 'qtip',
                        vertical: true,
                        items: [
                            {boxLabel: 'Declaraci&oacute;n jurada de asignaci&oacute;n familiar', itemId: 'declaracion_jurada_asig', name: 'declaracion_jurada_asig', uncheckedValue: 'false', disabled: true},
                            //{boxLabel: 'Declaraci&oacute;n jurada c&oacute;nyuge', name: 'declaracion_jurada_conyuge', uncheckedValue: 'false', itemId: 'declaracion_jurada_conyuge', disabled: true},
                            {boxLabel: 'Certificaci&oacute;n negativa ANSES', itemId: 'certificacion_anses', name: 'certificacion_anses', uncheckedValue: 'false', disabled: true}

                            
                        ]
                },
                {
                        xtype: 'checkboxgroup',
                        
                        // Distribute controls across 3 even columns, filling each column
                        // from top to bottom before starting the next column
                        columns: 1,
                        allowBlank: 'false',
                        msgTarget: 'qtip',
                        vertical: true,
                        items: [
                            {boxLabel: 'Suspendido', name: 'suspendido_asignacion', uncheckedValue: 'false', itemId: 'suspendido_asignacion'}
                            ]
                },
                
                {
                    xtype: 'textareafield',
                    grow: false,
                    anchor: '95%',
                    height: 200,
                    name: 'observaciones_tesoreria',
                    itemId: 'observaciones_tesoreria',
                    fieldLabel: 'Observaciones'
                }]//End items
            }]//End items
        }, {
                xtype: 'splitter'
        });
    }
    items.push({
            xtype: 'PersonalGrillaFamiliares',
            flex: 3
            
        });
    
    //Aplico el arreglo de botones
    //console.log(this.items);
    Ext.apply(this, {items: items});

    this.callParent(arguments);
    }
});


/*Ext.define('personal.view.PersonalFamilia', 
{
    extend: 'Ext.grid.Panel',
    alias: 'widget.PersonalFamilia',
    store: 'FamiliaStore',
    title: 'Listado de familiares del agente',
    itemId: 'PersonalFamilia',
    flex: 1,
    columns: [
		{header: 'Relaci&oacute;n', dataIndex: 'relacion_nombre', flex: 2},
		{header: 'Nombre', dataIndex: 'nombre', flex: 4},
		{header: 'Apellido', dataIndex: 'apellido', flex: 4},
        {header: 'DNI', dataIndex: 'dni', flex: 2},
        {header: 'Fecha nacimiento', dataIndex: 'fecha_nac', flex: 2, renderer: Ext.util.Format.dateRenderer('d-m-Y')},
        {header: 'Cert. Inicio', dataIndex: 'certificado_inicio', flex: 1},
        {header: 'Cert. Fin', dataIndex: 'certificado_fin', flex: 1},
        {header: 'Guardap.', dataIndex: 'guardapolvo', flex: 1}
    ],
    viewConfig: { 
        stripeRows: false, 
        getRowClass: function(record) {
        var status = 0;
        status = (record.get('certificado_inicio')=='No' && record.get('certificado_fin')=='No')?'0':((record.get('certificado_inicio')=='Si' && record.get('certificado_fin')=='No')?'1':'2');
        console.log(status);
        switch(status)
        {
            case '0':
                return 'sin_cert-row';
                break;
            case '1':
                return 'cert_ini-row';
                break;
            case '2':
                return 'cert_fin-row';
                break;        
        }
        } 
    }, 
    initComponent: function() 
    {
        var items=[]; //Arreglo de items

        //Si el permiso es igual a 1, el usuario es administrador. Lleno el arreglo con todos los botones
        if(AppGlobals.permiso == "1") 
        {
            items.push(
            {
                text: 'Agregar',
                action: 'agregar',
                iconCls: 'agregar',
                tooltip: 'Asigna un nuevo familiar al agente'
            },'-',{
                    text: 'Eliminar',
                    action: 'eliminarFamiliar',
                    itemId: 'eliminarFamiliar',
                    iconCls: 'quitar',
                    disabled: true,
                    tooltip: 'Quita un familiar asignado al agente'
            },'-',{
                    text: 'Modificar familiar',
                    action: 'openWindowModificarFamiliar',
                    itemId: 'modificarFamiliar',
                    iconCls: 'edit',
                    disabled: true,
                    tooltip: 'Modifica datos del familiar'

                });
        //Aplico el arreglo de botones a la vista
        Ext.apply(this, {tbar: items});
        }
        this.callParent(arguments);
    }
});*/