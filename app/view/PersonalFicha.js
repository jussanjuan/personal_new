Ext.define('personal.view.PersonalFicha', 
{
    extend: 'Ext.tab.Panel',
    id: 'tabPanelFichaLegajo',
    xtype: 'PersonalFicha',
    flex: 1,
    tabBarPosition: 'top',
    reference: 'PersonalFicha',
    requires: [
        'personal.view.PersonalFichaLegajo',
        'personal.view.PersonalFichaLicencia',
        'personal.view.PersonalFichaComisiones',
        'personal.view.PersonalFichaHistoriaClinica'
    ],
    
    defaults: 
    {
        listeners: 
        {
            activate: 'onActivateFicha'
        }
    },
    initComponent: function() 
    {
        var items=[]; //Arreglo de items
        
        console.log("Init ficha");
        console.log(AppGlobals.permisos);
        if(AppGlobals.permisos[0].pesta_legajo == 1) 
        {
            items.push(
            {
                xtype: 'PersonalFichaLegajo',
                id: 'PersonalFichaLegajo',
                disabled: true
            });
        }

        if(AppGlobals.permisos[0].pesta_licencias == 1)
        {
            items.push({
                xtype: 'PersonalFichaLicencia',
                id: 'PersonalFichaLicencia',
                disabled: true
            });
        }
        if(AppGlobals.permisos[0].pesta_comisiones_ver == 1)
        {
            items.push({
                xtype: 'PersonalFichaComisiones',
                id: 'PersonalFichaComisiones',
                disabled: true
            });
        }
        if(AppGlobals.permisos[0].pesta_historiaclinica_ver == 1)
        {
            items.push({
                xtype: 'PersonalFichaHistoriaClinica',
                id: 'PersonalFichaHistoriaClinica',
                disabled: true
            });
        }
        //Aplico el arreglo de botones a la vista para mostrar
        Ext.apply(this, {items: items});

        this.callParent(arguments);

    }
});
