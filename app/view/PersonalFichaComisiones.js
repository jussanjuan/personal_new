Ext.define('personal.view.PersonalFichaComisiones', {
	extend: 'Ext.grid.Panel',
	xtype: 'PersonalFichaComisiones',
	reference: 'PersonalFichaComisiones',
	requires:[
		'personal.store.licencias.ComisionStore',
		'personal.view.licencias.LComision',
		'personal.view.licencias.editarLComision'
	],
	store: {
		type: 'ComisionStore'
	},
	title: 'Comisiones',

    flex: 1,
           
    columns: [
		{text: 'Desde', dataIndex: 'desde',  renderer: Ext.util.Format.dateRenderer('d-m-Y'), width: 70},
		{text: 'Hasta', dataIndex: 'hasta',  renderer: Ext.util.Format.dateRenderer('d-m-Y'), width: 70},
		{text: 'Total dias', dataIndex: 'total_dias', width: 70},
        {text: 'Desiste', dataIndex: 'goce', width: 50},
		{text: 'Causa', dataIndex: 'causa', flex: 1},
		{text: 'Observaciones', dataIndex: 'observaciones', flex: 1},
		{text: 'Expediente', dataIndex: 'expediente', flex: 1}
	],
	

	initComponent: function() 
	{
		var items=[]; //Arreglo de items
        //Si el permiso es igual a 1, el usuario es administrador, personal o RpersonalH. Lleno el arreglo con todos los botones
        if(AppGlobals.permisos[0].pesta_comisiones_editar == 1)//if(AppGlobals.permiso == "1" || AppGlobals.rrhh=="si" || AppGlobals.dpto_personal=="si") 
        {
            items.push(
            {
			    id: 'agregarComision',
			    iconCls: 'agregarComision',
			    action: 'agregarComision',
		        scale: 'large',
		        tooltip: 'Agregar nueva comisi&oacute;n'
		    },'-',
		    {
			    id: 'editarComision',
			    iconCls: 'editarComision',
			    action: 'editarComision',
		        scale: 'large',
		        disabled: true,
		        tooltip: 'Editar una comisi&oacute;n'
			},'-',
			{
			    id: 'quitarComision',
			    iconCls: 'quitarComision',
			    action: 'quitarComision',
		        scale: 'large',
		        disabled: true,
		        tooltip: 'Eliminar una comisi&oacute;n'
			});
        //Aplico el arreglo de botones a la vista
        Ext.apply(this, {tbar: items});    
        }
        

		this.callParent(arguments);
	}
});
