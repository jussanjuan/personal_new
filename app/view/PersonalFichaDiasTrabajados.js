Ext.define('personal.view.PersonalFichaDiasTrabajados', {
	extend: 'Ext.grid.Panel',
	xtype: 'PersonalFichaDiasTrabajados',
	reference: 'PersonalFichaDiasTrabajados',
	requires: [
        'personal.store.historial.HDiasTrabajadosStore',
        'personal.view.licencias.PersonalCompensatoria',
        'personal.view.licencias.PersonalDobleTurno'
    ],
	store: {
		type:'HDiasTrabajadosStore'
	},
	title: 'Dias trabajados',
    flex: 1,
    tbar: [],
    features: [{
        groupHeaderTpl: 'Tipo: {nombre}',
        ftype: 'groupingsummary'
    }],
    columns: [
		{text: 'Licencia', dataIndex: 'nombre', flex: 4, summaryType: 'count', summaryRenderer: function(value, summaryData, dataIndex) {
            return value+" licencias";}},
		{text: 'Desde', dataIndex: 'desde',  renderer: Ext.util.Format.dateRenderer('d-m-Y'), flex: 3},
		{text: 'Hasta', dataIndex: 'hasta',  renderer: Ext.util.Format.dateRenderer('d-m-Y'), flex: 3},
		{text: 'Total', dataIndex: 'totaldias', flex: 2, summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
            return value;}},
        {text: 'Disponibles', dataIndex: 'disponibles', flex: 2, summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
                    return value;}},
        {text: 'Desiste', dataIndex: 'goce', flex: 1}
	],
	

	initComponent: function() {

		var items=[]; //Arreglo de items

        if(AppGlobals.permisos[0].pesta_licencias == 1)
        {
            items.push({
                id: 'btnAddCompensatorias',
                //text: 'Cargar Compensatorias',
                iconCls: 'add_data',
                scale: 'medium',
                action: 'addCompensatoria',
                tooltip: 'Agrega compensatorias (d&iacute;as trabajados en feria)'
            },'-', 
            {
                id: 'btnAddDobleTurno',
                //text: 'Cargar Compensatorias',
                iconCls: 'dobleTurno',
                scale: 'medium',
                action: 'addDobleTurno',
                tooltip: 'Agrega licencias por doble turno (d&iacute;as trabajados doble jornada)'
            },'-',{
                id: 'btnVaciarDias',
                iconCls: 'cancel1',
                scale: 'medium',
                tooltip: 'Vaciar d&iacute;as trabajados',
                action: 'vaciar_dias_trabajados',
                disabled: true,
            });
        }
        
        Ext.apply(this, {tbar: items});
        this.callParent(arguments);
	}
});
