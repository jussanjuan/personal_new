Ext.define('personal.view.PersonalFichaHistoriaClinica', {
	extend: 'Ext.grid.Panel',
	xtype: 'PersonalFichaHistoriaClinica',
	requires:[
		'personal.store.historial.HistoriaClinicaStore',
		'personal.view.PersonalHistoriaClinica',
		'personal.view.PersonalEditarHistoriaClinica',
		'personal.view.PersonalVerHistoriaClinica'
	],
	store: {
		type: 'HistoriaClinicaStore'
	},
	
	reference: 'PersonalFichaHistoriaClinica',
	title: 'Historia cl&iacute;nica',
    flex: 1,
	columns: [
		{text: 'Fecha y hora', dataIndex: 'fecha_hora',  renderer: Ext.util.Format.dateRenderer('d-m-Y H:i:s'), flex: 2},
		{text: 'Observaciones', dataIndex: 'observaciones', flex: 5},
		{text: 'Certificado', dataIndex: 'certificado', flex: 1}
	],
	/*plugins: [ Ext.create('Ext.ux.grid.Exporter', 
    {
        autoFocus: true,
	   	width: 180
    })],
	*/
	initComponent: function() 
	{
		var items=[]; //Arreglo de items
        items.push(
        {
		    id: 'verRegistroHistoria',
		    iconCls: 'verRegistroHistoria',
		    action: 'verRegistroHistoria',
	        scale: 'large',
	        disabled: true,
	        tooltip: 'Ver registro'
	    });
        if(AppGlobals.permisos[0].pesta_historiaclinica_editar == 1)
        {
            items.push('-',
            {
			    id: 'agregarRegistroHistoria',
			    iconCls: 'agregarComision',
			    action: 'agregarRegistroHistoria',
		        scale: 'large',
		        tooltip: 'Agregar nuevo registro'
		    },'-',
		    {
			    id: 'editarRegistroHistoria',
			    iconCls: 'editarComision',
			    action: 'editarRegistroHistoria',
		        scale: 'large',
		        disabled: true,
		        tooltip: 'Editar un registro'
			},'-',
			{
			    id: 'quitarRegistroHistoria',
			    iconCls: 'quitarComision',
			    action: 'quitarRegistroHistoria',
		        scale: 'large',
		        disabled: true,
		        tooltip: 'Eliminar un registro'
			},'-',
			{
			    id: 'borrarCertificadoHistoria',
			    iconCls: 'borrarCertificado',
			    action: 'borrarCertificadoHistoria',
		        scale: 'large',
		        disabled: true,
		        tooltip: 'Eliminar el certificado cargado'
			});
        //Aplico el arreglo de botones a la vista
        }
        Ext.apply(this, {tbar: items}); 
		this.callParent(arguments);
	}
});
