Ext.define('personal.view.PersonalFichaHistorial', 
{
    extend: 'Ext.Panel',
    xtype: 'PersonalFichaHistorial',
    reference: 'PersonalFichaHistorial',
    title: 'Historial',
    reference: 'PersonalFichaHistorial',
    flex: 1,
    layout: {
        type: 'hbox',       // Arrange child items vertically
        align: 'stretch',    // Each takes up full width
        padding: 0,
        border: 0
    },

    requires: [
        'personal.view.historial.HLicencia',
        'personal.view.historial.HMaternidad',
        'personal.view.historial.HEstudio',
        'personal.view.historial.HCompensatoria',
        'personal.view.historial.HDobleTurno'
    ],
    items: [
    {
        xtype: 'treelist',
        id: 'treeHistorial',
        /*border: false,
        useArrows: true,
        rootVisible: false,
        width: 180,*/
        store:
        {
            root: 
            {
                expanded: true,
                children:[
                    { 
                        text: 'Salud', expanded: true, iconCls: 'x-fa fa-heartbeat', children: [
                        { id: 'Personal', text: 'Personal', leaf: true },
                        { id: 'Familiar', text: 'Familiar', leaf: true},
                        { id: 'LargoTratamiento', text: 'Largo tratamiento', leaf: true },
                        { id: 'AccidentesDeTrabajo', text: 'Accidentes de Trabajo', leaf: true }
                    ]},
                    { id: 'Maternidad', text: 'Maternidad', leaf: true },
                    { id: 'Nacimiento', text: 'Nacimiento', leaf: true },
                    { id: 'Fallecimiento', text: 'Fallecimiento', leaf: true },
                    { id: 'Particulares', text: 'Particulares', leaf: true },
                    { id: 'SinGoce', text: 'Sin goce', leaf: true },
                    { id: 'Compensatorias', text: 'Compensatorias', leaf: true },
                    { id: 'Culturales', text: 'Culturales', leaf: true },
                    { id: 'Estudio', text: 'Estudio', leaf: true },
                    { id: 'Gremiales', text: 'Gremiales', leaf: true },
                    { id: 'Electivas', text: 'Electivas', leaf: true },
                    { id: 'Matrimonio', text: 'Matrimonio', leaf: true },
                    { id: 'Deportivas', text: 'Deportivas', leaf: true },
                    { id: 'Extraordinarias', text: 'Extraordinarias', leaf: true },
                    { id: 'DobleTurno', text: 'Doble Turno', leaf: true },
                    { id: 'Injustificadas', text: 'Injustificadas', leaf: true },
                    { id: 'Religion', text: 'Religi&oacute;n', leaf: true },
                    { id: 'Adopcion', text: 'Adopci&oacute;n', leaf: true }
                ]
            }
        }
    }, 
    {
        xtype: 'splitter'
    }, 
    {
        xtype: 'panel',
        id: 'pnlHistorialCard',
        layout: 'card',
        flex: 1,
        border: false,

        items:[{
                id: 'cardHZero',
                html: '<h3>Por favor, seleccione una licencia</h3>'
        }, {
                id: 'hLicencia',
                xtype: 'HLicencia'
        }, {
                id: 'hMaternidad',
                xtype: 'HMaternidad'
        }, {
                id: 'hEstudio',
                xtype: 'HEstudio'
        }, {
                id: 'hCompensatoria',
                xtype: 'HCompensatoria'
        },
        {
                id: 'hdobleTurno',
                xtype: 'HDobleTurno'
        }]
    }],	
    initComponent: function() 
    {
        /*var items=[]; //Arreglo de items

        //Si el permiso es igual a 1, el usuario es administrador. Lleno el arreglo con todos los botones
        //Caso contrario, quedar� el arreglo vac�o
        if(AppGlobals.permisos[0].pesta_historial_editar == 1)
        {
            items.push(
            {
                disabled: true,
                id: 'btnDesiste',
                //text: 'Desiste',
                iconCls: 'desiste',
                action: 'desistir',
                scale: 'large',
                tooltip: 'Desistir de la licencia seleccionada'
            },{
                disabled: true,
                id: 'btnEliminaLicencia',
                //text: 'Desiste',
                iconCls: 'delete_licencia',
                action: 'eliminar_licencia',
                scale: 'large',
                tooltip: 'Eliminar la licencia seleccionada'
            },{
                disabled: true,
                id: 'btnEditLicencia',
                //text: 'Desiste',
                iconCls: 'edit_licencia',
                action: 'edit_licencia',
                scale: 'large',
                tooltip: 'Editar algunos datos de la licencia seleccionada'
            });
            //Aplico el arreglo de botones a la vista
            Ext.apply(this, {tbar: items});
        }*/
        this.callParent(arguments);
    }
});
