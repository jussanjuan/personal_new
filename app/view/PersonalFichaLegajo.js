Ext.define('personal.view.PersonalFichaLegajo', 
{
    extend: 'Ext.tab.Panel',
    xtype: 'PersonalFichaLegajo',
    title: 'Legajo',
    autoScroll: true,
    overflow: 'auto',
    tabPosition: 'bottom',
    reference: 'PersonalFichaLegajo',
    requires: [
        'personal.view.PersonalCargos',
        'personal.view.PersonalFamilia',
        'personal.view.PersonalLugares',
        'personal.view.PersonalTitulos',
        'personal.view.PersonalSanciones',
        'personal.view.PersonalCapacitaciones',
        'personal.view.PersonalFirmasDigitales',
        'personal.store.PaisStore',
        'personal.store.LocalidadStore',
    ],
    
    items: [{
        title: 'Ficha',
        xtype: 'form',
        reference: 'PersonalFichaLegajoForm',
        border: false,
        autoScroll: true,
        flex: 1,
        fieldDefaults: 
        {
            anchor: '90%',
            labelAlign: 'top'
        },
        defaultType: 'textfield',
        bodyPadding: 5,

        items: [
        {
            xtype: 'container',
            layout: 'hbox',
            anchor: '100%',
            items: [
            {
                xtype: 'container',
                layout: 'anchor',
                flex: 1,
                items: [
                {
                    xtype: 'fieldcontainer',
                    fieldLabel: 'Legajo y Padron',
                    layout: 'hbox',
                    labelAlign: 'top',
                    items: [
                    {
                        xtype: 'textfield',
                        name: 'legajo',
                        allowBlank: false,
                        blankText:'Debe completar este campo',
                        validateOnChange: 'true',
                        validator: function(value){
                            var stringNum = /^([0-9])+$/;
                        if (!stringNum.test(value))
                            return ("Introducir s&oacute;lo n&uacute;meros");
                        else
                              return true;
                          },
                        flex: 1
                    }, 
                    {
                        xtype: 'textfield', 
                        name: 'padron',
                        flex: 1,
                        validateOnChange: 'true',
                        validator: function(value){
                            var stringPad=/^([0-9]){6}[-]{1}[0-9]{1}$/;
                        if (!stringPad.test(value))
                            return ("Introducir padron con formato XXXXXX-X");
                        else
                              return true;
                          }
                          
                    }]
                }, 
                {
                    xtype: 'textfield',
                    name: 'apellido',
                    validateOnChange: 'true',
                    validator: function(value)
                    {
                        var stringPad=/([^0-9])+$/;
                        if (!stringPad.test(value))
                        {
                            return ("Introducir solo caracteres alfab&eacute;ticos");
                        }
                        else
                        {
                            return true;
                        }
                    },
                    fieldLabel: 'Apellido',
                    fieldStyle: 'text-transform:uppercase'
                }, 
                {
                    xtype: 'textfield',
                    name: 'nombre',
                    validateOnChange: 'true',
                    validator: function(value)
                    {
                        var stringPad=/([^0-9])+$/;
                        if (!stringPad.test(value))
                        {
                            return ("Introducir solo caracteres alfab&eacute;ticos");
                        }
                        else
                        {
                            return true;
                        }
                    },
                    fieldLabel: 'Nombre',
                    fieldStyle: 'text-transform:uppercase'
                }, 
                {
                    xtype: 'textfield',
                    validateOnChange: 'true',
                    validator: function(value)
                    {
                        if (!value)
                        {
                            return ("Este campo no puede estar vac&iacute;o");
                        }
                        else
                        {
                            return true;
                        }
                    },
                    name: 'domic',
                    fieldLabel: 'Domicilio',
                    fieldStyle: 'text-transform:uppercase'
                }, 
                {
                    xtype: 'combo',
                    name: 'localidad',
                    displayField: 'nombre',
                    valueField: 'id',
                    fieldLabel: 'Localidad de residencia',
                    queryMode: 'local',
                    forceSelection: true,
                    store: Ext.create('personal.store.LocalidadStore'),
                    allowBlank: false
                },
                {
                    xtype: 'combobox',
                    fieldLabel: 'Tipo doc.',
                    name: 'tipodoc',
                    width: 50,
                    editable: false,
                    store: ['LE', 'LC', 'DNI'],
                    queryMode: 'local',
                    validateOnChange: 'true',
                    validator: function(value)
                    {
                        if (!value)
                        {
                            return ("Seleccione tipo de DNI");
                        }
                        else
                        {
                            return true;
                        }
                    }
                }, 
                    {
                        xtype: 'textfield',
                        name: 'nrodoc',
                        fieldLabel: 'Nro. doc',
                        blankText:'Debe completar este campo',
                        validateOnChange: 'true',
                        validator: function(value)
                        {
                            var stringNum = /^([0-9])+$/;
                            if (!stringNum.test(value))
                            {
                                return ("Introducir s&oacute;lo n&uacute;meros");
                            }
                            else
                            {
                                return true;
                            }
                        },
                        flex: 1
                    }, 
                    {
                        xtype: 'combobox',
                        name: 'sexo',
                        fieldLabel: 'Sexo',
                        width: 40,
                        editable: false,
                        store: ['M', 'F'],
                        queryMode: 'local',
                        validateOnChange: 'true',
                        validator: function(value)
                        {
                            if (!value)
                            {
                                return ("Seleccione sexo");
                            }
                            else
                            {
                                return true;
                            }
                        }
                    }, {
                    xtype: 'textfield',
                    name: 'cuil',
                    fieldLabel: 'CUIL',
                    validateOnChange: 'true',
                    validator: function(value)
                    {
                        var stringNum = /^\d{2}\-\d{8}\-\d{1}$/;
                        if(value)
                        {
                            if (!stringNum.test(value))
                            {
                                return ("Introducir siguiendo el formato XX-DNI-X");
                            }
                            else
                            {
                                return true;
                            }
                        }
                        else
                            return true;
                    }
                },
                {
                    xtype: 'textfield',
                    name: 'telef',
                    fieldLabel: 'Telefono',
                    validateOnChange: 'true',
                    validator: function(value)
                    {
                        var stringNum = /^([0-9])+$/;
                        if (!stringNum.test(value))
                        {
                            return ("Introducir s&oacute;lo n&uacute;meros");
                        }
                        else
                        {
                            return true;
                        }
                    }
                }, 
                {
                    xtype: 'datefield',
                    name: 'fecnac',
                    fieldLabel: 'Fecha de nacimiento',
                    format: 'd-m-Y',
                    validateOnChange: 'true',
                    validator: function(value)
                    {
                        if (!value)
                        {
                            return ("Seleccione fecha de nacimiento");
                        }
                        else
                        {
                            return true;
                        }
                    }
                }, 
                {
                    xtype: 'combobox',
                    name: 'estaciv',
                    fieldLabel: 'Estado civil',
                    editable: false,
                    store: [['S', 'Soltero/a'], ['C', 'Casado/a'], ['D', 'Divorciado/a'], ['V', 'Viudo/a'], ['N', 'Conviviente'], ['U', 'Union Convivencial']],
                    queryMode: 'local',
                    validateOnChange: 'true',
                    validator: function(value)
                    {
                        if (!value)
                        {
                            return ("Seleccione estado civil");
                        }
                        else
                        {
                            return true;
                        }
                    }
                }, 
                {
                    xtype: 'fieldcontainer',
                    fieldLabel: 'Grupo sangu&iacute;neo',
                    layout: 'hbox',
                    labelAlign: 'top',
                    items: [
                    {
                        xtype: 'combobox',
                        name: 'sangre',
                        width: 40,
                        editable: false,
                        store: ['A', 'B', 'AB', 'O'],
                        queryMode: 'local'
                    },
                    {
                        xtype: 'combobox',
                        name: 'factor',
                        width: 80,
                        editable: false,
                        store: ['+', '-'],
                        queryMode: 'local'
                    }]                
                },{
                    xtype: 'textfield',
                    name: 'mail_personal',
                    fieldLabel: 'Mail personal',
                    validateOnChange: 'true',
                    validator: function(value)
                    {
                        var stringNum = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
                        if (value!="" && !stringNum.test(value))
                        {
                            return ("Introducir un mail v&aacute;lido");
                        }
                        else
                        {
                            return true;
                        }
                    }
                },{
                    xtype: 'textfield',
                    name: 'mail_corporativo',
                    fieldLabel: 'Mail corporativo',
                    validateOnChange: 'true',
                    validator: function(value)
                    {
                        var stringNum = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@jussanjuan.gov.ar$/;
                        if (value!="" && !stringNum.test(value))
                        {
                            return ("Introducir un mail v&aacute;lido");
                        }
                        else
                        {
                            return true;
                        }
                    }
                },{
                    xtype: 'textfield',
                    name: 'usuario_dominio',
                    fieldLabel: 'Usuario de dominio'
                }]
            }, 
            {
                xtype: 'container',
                layout: 'anchor',
                flex: 1,
                items: [
                {
                    xtype: 'container',
                    height: 17
                }, 
                {
                    id: 'foto',
                    xtype: 'image',
                    mode : 'image',
                    style: 
                    {
                        'maxHeight': '168px'
                        //'width': '168px',
                        //'height': '168px'
                    },
                    
                    src: 'res/img/profile1.jpg',
                    listeners: 
                    {
                        render : function(c) 
                        {
                            c.getEl().on('click', function(){ this.fireEvent('click', c); }, c);
                        }
                    }
                },  
                {
                    xtype: 'textfield',
                    name: 'cargo',
                    fieldLabel: 'Cargo actual',
                    validateOnChange: 'true',
                    readOnly: true
                }, 
                {
                    xtype: 'textfield',
                    name: 'nombrelugar',
                    fieldLabel: 'Lugar actual',
                    readOnly: true
                },
                {
                    xtype: 'textfield',
                    name: 'titulo',
                    id: 'titulo',
                    readOnly: true,
                    fieldLabel: 'Titulo',
                    flex: 1
                }, 
                {
                    xtype: 'datefield',
                    name: 'fecha_carga',
                    fieldLabel: 'Fecha de ingreso',
                    value: new Date(),
                    format: 'd-m-Y',
                    allowBlank: false
                },{
                    xtype: 'datefield',
                    name: 'fecha_baja',
                    fieldLabel: 'Fecha de baja',
                    //value: new Date(),
                    format: 'd-m-Y',
                    allowBlank: true
                },{
                    xtype: 'textfield',
                    name: 'acordada_baja',
                    fieldLabel: 'Acordada de baja',
                    allowBlank: true
                },{
                    xtype: 'combobox',
                    name: 'nacionalidad',
                    displayField: 'nombre_pais',
                    valueField: 'codigo_pais',
                    fieldLabel: 'Nacionalidad',
                    store: Ext.create('personal.store.PaisStore'),
                    allowBlank: false
                },{
                    xtype: 'textfield',
                    name: 'antiguedad',
                    fieldLabel: 'Antigüedad',
                    validateOnChange: 'true',
                    validator: function(value)
                    {
                        var stringNum = /^[0-9]{1,2}$/;
                        if(value)
                        {
                            if (!stringNum.test(value))
                            {
                                return ("Introducir solo números");
                            }
                            else
                            {
                                return true;
                            }
                        }
                        else
                            return true;
                    }
                    
                }]
            }]
        }, 
        {
            xtype: 'checkboxgroup',
            //fieldLabel: 'Varios',
            // Distribute controls across 3 even columns, filling each column
            // from top to bottom before starting the next column
            columns: 3,
            allowBlank: 'false',
            msgTarget: 'qtip',
            vertical: true,
            items: [
                {boxLabel: 'Firma autorizada', name: 'firma_autorizada', uncheckedValue: 'false', id: 'firma_autorizada'},
                {boxLabel: 'Retenci&oacute;n ganancias', name: 'retencion', uncheckedValue: 'false', id: 'retencion_ganancias', checked: true}
            ]
        }, 
        {
            xtype: 'textareafield',
            grow: true,
            anchor: '95%',
            name: 'obs',
            fieldLabel: 'Observaciones'
        }]
    }],
    
    //funcion para obtener los campos invalidos
    getInvalidFields: function() 
    {
        console.log('Entra al getinvalid');
        var invalidFields = [];
        Ext.suspendLayouts();
        var me = this.getActiveTab(0);
        me.getForm().getFields().filterBy(function(field) 
        {
            if (field.validate()) 
            {
                return;
            }
            invalidFields.push(field);
        });
        Ext.resumeLayouts(true);
        return invalidFields;
    }, 
    initComponent: function() 
    {
        var items=[]; //Arreglo de items
        
        items.push(
        {
            id: 'btnGuardar',
            //text: 'Guardar',
            iconCls: 'save',
            scale: 'large',
            tooltip: 'Guardar',
            disabled: true,
            action: 'guardar'
        },'-', {
            id: 'btnCancelar',
            //text: 'Cancelar',
            iconCls: 'cancel',
            scale: 'large',
            tooltip: 'Cancelar edici&oacute;n',
            disabled: true,
            action: 'cancelar'
        },'-', {
            id: 'btnLimpiar',
            //text: 'Limpiar',
            iconCls: 'clear',
            scale: 'large',
            tooltip: 'Limpiar formulario',
            disabled: true,
            action: 'limpiar'
        });
        //Referencia al objeto Ficha, el cual tiene la barra de botones
        var itemFicha = this.items[0];
        //console.log(this.items[0]);
        //Aplico el arreglo de botones a la ficha
        Ext.apply(itemFicha, {tbar: items});

        if(AppGlobals.permisos[0].pesta_legajo_cargos_ver == 1)
        {
            this.items.push({
                title: 'Cargos',
                xtype: 'PersonalCargos'
            });
        }

        if(AppGlobals.permisos[0].pesta_legajo_lugares_ver == 1)
        {
            this.items.push({
                title: 'Lugares',
                xtype: 'PersonalLugares'
            });
        }
        
        if(AppGlobals.permisos[0].pesta_legajo_titulos_ver == 1)
        {
            this.items.push({
                title: 'Titulos',
                xtype: 'PersonalTitulos'
            });
        }
        if(AppGlobals.permisos[0].pesta_legajo_familiares_ver == 1)
        {
            this.items.push({
                title: 'Familiares',
                xtype: 'PersonalFamilia'
            });
        }

        if(AppGlobals.permisos[0].pesta_legajo_sanciones_ver == 1)
        {
            this.items.push({
                title: 'Sanciones',
                xtype: 'PersonalSanciones'
            });
        }

        if(AppGlobals.permisos[0].pesta_legajo_capacitaciones_ver == 1)
        {
            this.items.push({
                title: 'Capacitaciones',
                xtype: 'PersonalCapacitaciones'
            });
        }
        if(AppGlobals.permisos[0].pesta_legajo_firmasdigitales_ver == 1)
        {
            this.items.push({
                title: 'Firmas digitales',
                xtype: 'PersonalFirmasDigitales'
            });
        }

        this.callParent(arguments);

        var foto = Ext.getCmp('foto');
    }
});