Ext.define('personal.view.PersonalFichaLicencia', 
{
    extend: 'Ext.tab.Panel',
    //id: 'tabPanelFichaLicencia',
    xtype: 'PersonalFichaLicencia',
    flex: 1,
    tabBarPosition: 'top',
    reference: 'PersonalFichaLicencia',
    requires: [
        'personal.view.PersonalFichaLicencias',
        'personal.view.PersonalFichaSolicitudLicencias',
        'personal.view.PersonalFichaDiasTrabajados'

    ],
    title: 'Licencias',
    tabPosition: 'bottom',
    tbar: [],
    defaults: 
    {
        listeners: 
        {
            activate: 'onActivateFicha'
            /*activate: function(tab, eOpts) 
            {
                
            }*/
        }
    },
    initComponent: function() 
    {
        var items=[]; //Arreglo de tabs
        var itemsBarra = []; //Arreglo de botones
        if(AppGlobals.permisos[0].pesta_licencias == 1)
        {
            items.push({
                xtype: 'PersonalFichaLicencias',
                id: 'PersonalFichaLicencias'
            },{
                xtype: 'PersonalFichaSolicitudLicencias',
                id: 'PersonalFichaSolicitudLicencias'
            },{
                xtype: 'PersonalFichaDiasTrabajados',
                id: 'PersonalFichaDiasTrabajados'
            });
        }
        if(AppGlobals.permisos[0].pesta_historial_editar == 1)
        {
            itemsBarra.push(
            {
                disabled: true,
                id: 'btnDesiste',
                //text: 'Desiste',
                iconCls: 'desiste',
                action: 'desistir',
                scale: 'large',
                tooltip: 'Desistir de la licencia seleccionada'
            },'-',{
                disabled: true,
                id: 'btnEliminaLicencia',
                //text: 'Desiste',
                iconCls: 'delete_licencia',
                action: 'eliminar_licencia',
                scale: 'large',
                tooltip: 'Eliminar la licencia seleccionada'
            });
            //Aplico el arreglo de botones a la vista
            Ext.apply(this, {tbar: itemsBarra});
        }
        //Aplico el arreglo de botones a la vista para mostrar
        Ext.apply(this, {items: items});

        this.callParent(arguments);

    }
});
