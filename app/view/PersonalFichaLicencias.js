Ext.define('personal.view.PersonalFichaLicencias', {
	extend: 'Ext.grid.Panel',
	xtype: 'PersonalFichaLicencias',
	reference: 'PersonalFichaLicencias',
	requires: [
        'personal.store.historial.HLicenciaStore',
        'personal.view.ModificarLicencia',
        'personal.view.licencias.AltaLicencia',
        'personal.view.licencias.PersonalCompensatoria',
        'personal.view.licencias.PersonalDobleTurno'
    ],
	store: {
		type:'HLicenciaStore'
	},
	title: 'Tomadas',

    flex: 1,
    tbar: [],
    columns: [
		{text: 'Licencia', dataIndex: 'nombre', width: 180},
		{text: 'Desde', dataIndex: 'desde',  renderer: Ext.util.Format.dateRenderer('d-m-Y'), width: 70},
		{text: 'Hasta', dataIndex: 'hasta',  renderer: Ext.util.Format.dateRenderer('d-m-Y'), width: 70},
		{text: 'Dias otorg.', dataIndex: 'dias_licencia', width: 70},
        {text: 'Desiste', dataIndex: 'goce', width: 50},
        {text: 'Pierde Presentismo', dataIndex: 'pierde_presentismo', width: 50},
		{text: 'Causa', dataIndex: 'causa', flex: 1},
        {text: 'Notif. mail', dataIndex: 'notificado', flex: 1}
	],
	

	initComponent: function() {

		var items=[]; //Arreglo de items

        if(AppGlobals.permisos[0].pesta_licencias == 1)
        {
            items.push({
                id: 'agregarLicencia',
                iconCls: 'agregarSolicitudLicencia',
                action: 'agregarLicencia',
                scale: 'medium',
                tooltip: 'Agregar nueva licencia'
            });
        }
        if(AppGlobals.permisos[0].pesta_historial_editar == 1)
        {
            items.push(
            {
                disabled: true,
                id: 'btnEditLicencia',
                //text: 'Desiste',
                iconCls: 'edit_licencia',
                action: 'edit_licencia',
                scale: 'medium',
                tooltip: 'Editar algunos datos de la licencia seleccionada'
            });
            //Aplico el arreglo de botones a la vista
            Ext.apply(this, {tbar: items});
        }
        this.callParent(arguments);
	}
});
