Ext.define('personal.view.PersonalFichaLicencias', 
{
    extend: 'Ext.Panel',
    xtype: 'PersonalFichaLicencias',
    id: 'PersonalFichaLicencias',
    title: 'Licencias',
    reference: 'PersonalFichaLicencias',
    flex: 1,
    layout: {
        type: 'hbox',       // Arrange child items vertically
        align: 'stretch',    // Each takes up full width
        padding: 0,
        border: 0
    },
    
    requires: [
    	'personal.view.Feriados',
    	'personal.view.licencias.PersonalCompensatoria',
        'personal.view.licencias.PersonalDobleTurno',
        'personal.view.licencias.LMaternidad',
        //'personal.view.licencias.altaLicencia',
        'personal.view.licencias.LEstudio'
    ],

    items: [
    {
        xtype: 'treepanel',
	id: 'treeLicencias',
        border: false,
        useArrows: true,
        rootVisible: false,
        activeItem: 0,
        width: 180,
        root: 
        {
            expanded: true,
        	children: [
            	{ text: 'Salud', expanded: true, children: [
                	{ id: 'Personal', text: 'Personal', leaf: true },
                	{ id: 'Familiar', text: 'Familiar', leaf: true},
                	{ id: 'LargoTratamiento', text: 'Largo tratamiento', leaf: true },
                	{ id: 'AccidentesDeTrabajo', text: 'Accidentes de Trabajo', leaf: true }
            	] },
            	{ id: 'Maternidad', text: 'Maternidad', leaf: true },
            	{ id: 'Nacimiento', text: 'Nacimiento', leaf: true },
            	{ id: 'Fallecimiento', text: 'Fallecimiento', leaf: true },
            	{ id: 'Particulares', text: 'Particulares', leaf: true },
            	{ id: 'SinGoce', text: 'Sin goce', leaf: true },
            	{ id: 'Compensatorias', text: 'Compensatorias', leaf: true },
            	{ id: 'Culturales', text: 'Culturales', leaf: true },
            	{ id: 'Estudio', text: 'Estudio', leaf: true },
            	{ id: 'Gremiales', text: 'Gremiales', leaf: true },
            	{ id: 'Electivas', text: 'Electivas', leaf: true },
            	{ id: 'Matrimonio', text: 'Matrimonio', leaf: true },
            	{ id: 'Deportivas', text: 'Deportivas', leaf: true },
            	{ id: 'Extraordinarias', text: 'Extraordinarias', leaf: true },
                { id: 'DobleTurno', text: 'Doble Turno', leaf: true },
                { id: 'Injustificada', text: 'Injustificada', leaf: true },
                { id: 'Religion', text: 'Religi&oacute;n', leaf: true },
                { id: 'Adopcion', text: 'Adopci&oacute;n', leaf: true }
        	]
        }
    }, 
    {
        xtype: 'splitter'
    }, 
    { 
        xtype: 'panel',
        id: 'pnlLicenciasCard',
        layout: 'card',

        flex: 1,
        border: false,


        items:[{
                id: 'cardZero',
                html: '<h3>Por favor, seleccione una licencia</h3>'
        }, /*{
                id: 'cardAltaLicencia',
                xtype: 'altaLicencia'
        },*/ {
                id: 'cardMaternidad',
                xtype: 'LMaternidad'
        }, {
                id: 'cardEstudio',
                xtype: 'LEstudio'
        }, {
                id: 'cardFeriados',
                xtype: 'Feriados'
        }] 

    }],

    initComponent: function() 
    {
             
        
        this.callParent(arguments);
    }
});
