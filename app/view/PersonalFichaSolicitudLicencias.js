Ext.define('personal.view.PersonalFichaSolicitudLicencias', {
	extend: 'Ext.grid.Panel',
	xtype: 'PersonalFichaSolicitudLicencias',
	reference: 'PersonalFichaSolicitudLicencias',
	//id: 'PersonalFichaSolicitudLicencias',
	requires:[
		'personal.store.historial.HSolicitudLicenciaStore',
		'personal.view.SolicitudLicencia',
		'personal.view.ConfirmarSolicitudLicencia',
		'personal.view.ModificarSolicitudLicencia'
	],
	store: {
		type: 'HSolicitudLicenciaStore'
	},
	title: 'Solicitudes',
    flex: 1,
           

    columns: [
		{header: 'Licencia', dataIndex: 'nombre', width: 70},
		{header: 'Desde', dataIndex: 'desde',  renderer: Ext.util.Format.dateRenderer('d-m-Y'), width: 70},
		{header: 'Hasta', dataIndex: 'hasta',  renderer: Ext.util.Format.dateRenderer('d-m-Y'), width: 70},
		{header: 'Causa', dataIndex: 'causa', flex: 1},
		{header: 'Observaciones', dataIndex: 'observaciones', flex: 1},
		{header: 'Hora solicit.', dataIndex: 'hora_solicitud',  renderer: Ext.util.Format.dateRenderer('H:i'), width: 70},
		{header: 'Solicitante', dataIndex: 'nombre_solicitante',  flex: 1},
		{header: 'Receptor', dataIndex: 'nombre_receptor',  flex: 1},
		{header: 'Direcci&oacute;n', dataIndex: 'direccion',  flex: 1},
		{header: 'Estado', dataIndex: 'nombre_estado',  flex: 1}
	],
	viewConfig: { 
        stripeRows: false, 
        getRowClass: function(record) 
        {
            var status = "2";
            var css = "";
            status = record.get('estado');
            console.log(record.get('estado'));
            switch(status)
            {
            	case "1":
            		css = 'final-row-solicitud';
            		break;
            	case "2":
            		css = 'en_cur-row-solicitud';
            		break;	
            	case "3":
            		css = 'pend-row-solicitud';
            		break;

            }
            return css;
        }
    }, 

	initComponent: function() 
	{
		var items=[]; //Arreglo de items
        if(AppGlobals.permisos[0].pesta_solicitud_licencias_editar == 1)
        {
            items.push(
            {
			    id: 'agregarSolicitudLicencia',
			    iconCls: 'agregarSolicitudLicencia',
			    action: 'agregarSolicitudLicencia',
		        scale: 'medium',
		        tooltip: 'Agregar nueva solicitud'
		    },'-',
		    {
			    id: 'editarSolicitudLicencia',
			    iconCls: 'editarSolicitudLicencia',
			    action: 'editarSolicitudLicencia',
		        scale: 'medium',
		        disabled: true,
		        tooltip: 'Editar una solicitud de licencia'
			},'-',
			{
			    id: 'quitarSolicitudLicencia',
			    iconCls: 'quitarSolicitudLicencia',
			    action: 'quitarSolicitudLicencia',
		        scale: 'medium',
		        disabled: true,
		        tooltip: 'Eliminar una solicitud de licencia'
			},'-',
			{
			    id: 'confirmarSolicitudLicencia',
			    iconCls: 'confirmarSolicitudLicencia',
			    action: 'confirmarSolicitudLicencia',
		        scale: 'medium',
		        disabled: true,
		        tooltip: 'Confirmar la solicitud (pasar&aacute; a ser licencia)'
			});
        //Aplico el arreglo de botones a la vista
        Ext.apply(this, {tbar: items});    
        }

		this.callParent(arguments);
	}
});
