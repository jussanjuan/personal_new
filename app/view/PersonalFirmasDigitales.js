Ext.define('personal.view.PersonalFirmasDigitales', 
{
    extend: 'Ext.grid.Panel',
    xtype: 'PersonalFirmasDigitales',
    requires: [
        'personal.store.historial.HFirmaDigitalStore',
        'personal.view.PersonalAgregarFirmaDigital',
        'personal.view.PersonalModificarFirmaDigital'
    ],
    store: {
        type: 'HFirmaDigitalStore'
    },
    reference: 'PersonalFirmasDigitales',
    title: 'Listado de firmas digitales del Agente',
    itemId: 'PersonalFirmasDigitales',
    flex: 1,
    columns: [
        {header: 'Tipo', dataIndex: 'tipoFirma', flex: 1},
        {header: 'Descripci&oacute;n', dataIndex: 'descripcion_firma', flex: 3},
		{header: 'Fecha alta', dataIndex: 'fecha_alta', renderer: Ext.util.Format.dateRenderer('d-m-Y H:i:s'), flex: 1},
        {header: 'Fecha revocaci&oacute;n', dataIndex: 'fecha_revocacion', renderer: Ext.util.Format.dateRenderer('d-m-Y H:i:s'), flex: 1}
    ],
    listeners: 
    {
        'selectionchange': function(view, records) 
        {
            if(AppGlobals.permisos[0].pesta_legajo_firmasdigitales_editar == 1)
            {
                this.down('#eliminarFirmaDigital').setDisabled(!records.length);
                this.down('#editarFirmaDigital').setDisabled(!records.length);
                //Guardo el registro seleccionado, para rellenar la ventana con los datos si el usuario quiere editar la capacitacion
                AppGlobals.recordEditFirmaDigital = records;
                
            }
        }
    },
    initComponent: function() 
    {
        var items=[]; //Arreglo de items
        
        if(AppGlobals.permisos[0].pesta_legajo_firmasdigitales_editar == 1)
        {
            items.push(
            {
            text: 'Agregar',
            action: 'agregarFirmaDigital',
            iconCls: 'agregar',
            tooltip: 'Asigna una nueva firma digital para el agente'
            },'-',{
                text: 'Editar',
                action: 'editarFirmaDigital',
                itemId: 'editarFirmaDigital',
                iconCls: 'folderedit',
                disabled: true,
                tooltip: 'Editar una firma digital'
            },'-',{
                text: 'Eliminar',
                action: 'eliminarFirmaDigital',
                itemId: 'eliminarFirmaDigital',
                iconCls: 'quitar',
                disabled: true,
                tooltip: 'Quita una firma digital asignada al agente'
            });
        //Aplico el arreglo de botones a la vista
        Ext.apply(this, {tbar: items});    
        }
        this.callParent(arguments);
    }
});