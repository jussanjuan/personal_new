/*Ext.define('Sandbox.view.SearchTrigger', {
    extend: 'Ext.form.field.Text',
    alias: 'widget.searchtrigger',
    triggers:{
        search: {
            cls: 'x-form-search-trigger',
            handler: function() {
                console.log("holis");
                //this.setFilter(this.up().dataIndex, this.getValue())
                //this.searchEngine(this.up().dataIndex, this.getValue())
            }
        },
        clear: {
            cls: 'x-form-clear-trigger',
            handler: function() {
                this.setValue('')
                //if(!this.autoSearch) this.setFilter(this.up().dataIndex, '')
                if(!this.autoSearch) this.searchEngine(this.up().dataIndex, '')
            }
        }
    },
    setFilter: function(filterId, value){
        var store = this.up('grid').getStore();
        if(value){
            store.removeFilter(filterId, false);
            //console.log(this);
            var filter = {id: filterId, property: filterId, value: value};
            //filter.anyMatch = true;
            if(this.anyMatch) filter.anyMatch = this.anyMatch
            if(this.caseSensitive) filter.caseSensitive = this.caseSensitive
            if(this.exactMatch) filter.exactMatch = this.exactMatch
            if(this.operator) filter.operator = this.operator
            console.log(filter)
            store.addFilter(filter)
        } else {
            store.filters.removeAtKey(filterId)
            store.reload()
        }
    },
    searchEngine: function(field, query)
    {
        var store = this.up('grid').getStore();
        console.log("Consulta: "+query);
        var fields = ["nrodoc","legajo","padron","apellido","nombre","telef","cuil"];
        store.load({
            params: {
                fields: fields,
                method: 'GET',
                query: 'asdfsdf'
                }
             })
        
    },
    listeners: {
        render: function(){
            var me = this;
            me.ownerCt.on('resize', function(){
                me.setWidth(this.getEl().getWidth())
            })
        },
        change: function() {
            //if(this.autoSearch) this.setFilter(this.up().dataIndex, this.getValue())
            if(this.autoSearch) 
            {
                this.searchEngine(this.up().dataIndex, this.getValue())
            }
        },
        specialkey: function(field, e)
        {
            if (e.getKey() == e.ENTER) 
            {
                //this.searchEngine(this.up().dataIndex, this.getValue());
            }

        }
    }
});*/

Ext.define('personal.view.PersonalGrilla', 
{
    extend: 'Ext.grid.Panel',
    xtype: 'PersonalGrilla',
    requires: [
        'personal.store.PersonalStore',
    ],
    store: {
        type: 'PersonalStore'
    },
    plugins: 'gridfilters',
    title: 'Listado de Agentes',
    flex: 1,
    listeners: {
        itemdblclick: 'controlEditing'
    },
    reference: 'PersonalGrilla',
    bbar: {
        xtype: 'pagingtoolbar',
        displayInfo: true,
        beforePageText: 'P&aacute;gina',
        afterPageText: 'de {0}',
        displayMsg: 'Mostrando {0} - {1} de {2}',
        emptyMsg: "No hay agentes para mostrar",
    },
    columns: [
            {
                text: 'DNI', dataIndex: 'nrodoc', flex: 1,
                /*items: [{
                    xtype: 'searchtrigger',
                    //autoSearch: true,
                    anyMatch: true
                }],*/
                flex: 1/*,
                filter: {
                    // required configs
                    type: 'number',
                    // optional configs
                    //value: 'star',  // setting a value makes the filter active.
                    itemDefaults: {
                        // any Ext.form.field.Text configs accepted
                    }
                }*/
                
            },
            {
                text: 'Legajo', dataIndex: 'legajo', flex: 1,
                /*items: [{
                    xtype: 'searchtrigger',
                    //autoSearch: true
                }],*/
                flex: 1,
                
            },
            {
                text: 'Padron', dataIndex: 'padron', flex: 1,
                /*items: [{
                    xtype: 'searchtrigger',
                    //autoSearch: true
                }],*/
                flex: 1,
                 
            },
            {
                text: 'Apellido', dataIndex: 'apellido', flex: 1,
                /*items: [{
                    xtype: 'searchtrigger',
                    //autoSearch: true
                }],*/
                flex: 1,
                 
            },
            {
                text: 'Nombre', dataIndex: 'nombre', flex: 1,
                /*items: [{
                    xtype: 'searchtrigger',
                    //autoSearch: true
                }],*/
                flex: 1,
                
            },		
            {
                text: 'Telefono', dataIndex: 'telef', flex: 1,
                /*items: [{
                    xtype: 'searchtrigger',
                    //autoSearch: true
                }],*/
                flex: 1,
                
            },
            {
                text: 'CUIL', dataIndex: 'cuil', flex: 1,
                flex: 1,
                /*items: [{
                    xtype: 'searchtrigger',
                    //autoSearch: true,
                    anyMatch: true
                }],*/
                 
            }
    ],
    
    viewConfig: { 
        stripeRows: false,
        forceFit: true, 
        getRowClass: function(record) 
        {
            var status = 0;
            status = record.get('activa');
            switch (status)
            {
                case "1": 
                    return 'activos-row';
                    break;
                case "2":
                    return 'tramite-jubilacion-row'
                    break;
                case "3":
                    return 'bajas-row';
                    break;
            }
        }
    }, 
    
        
    initComponent: function() 
    {
        //Controlo los permisos de usuario, y en base a eso muestro los botones

        var items=[]; //Arreglo de items
        console.log("init grilla");
        //Si el permiso aplica, lleno el arreglo con todos los botones
        
        if(AppGlobals.permisos[0].pesta_licencias == 1)//if(AppGlobals.permiso == "1") 
        {
            items.push(
            {
                id: 'btnEditFeriados',
                //text: 'Editar Feriados',
                action: 'editFeriados',
                iconCls: 'edit_calendar',
                scale: 'large',
                tooltip: 'Agrega, modifica o quita feriados'
            });
        }
        if(AppGlobals.permisos[0].cargar_agente == 1)
        {
            items.push(
            {
                id: 'btnAgregarAgente',
                //text: 'Agregar',
                action: 'agregar',
                iconCls: 'add_user',
                scale: 'large',
                tooltip: 'Agrega nuevo empleado'
            });
            
        }
        items.push(
        {
            id: 'btnEditarAgente',
            //text: 'Editar',
            action: 'editar',
            iconCls: 'edit_user',
            scale: 'large',
            disabled: true,
            tooltip: 'Editar mis datos'
        });
        
        if(AppGlobals.permisos[0].baja_agente == 1)
        {
            items.push(
            {
                id: 'btnBajaAgente',
                //text: 'Dar de baja',
                action: 'darBaja',
                iconCls: 'delete_user',
                scale: 'large',
                disabled: true,
                tooltip: 'Da de baja al empleado'
            });
        }
        items.push('-',{
            xtype: 'textfield',
            name: 'searchPersonalGrilla',
            width: '55%',
            fieldLabel: 'Buscar',
            labelAlign: 'left',
            labelWidth: 45
        });

        if(AppGlobals.permisos[0].actualizar_antiguedades == 1)
        {
            items.push(
            '->',{
                id: 'btnActualizarAntiguedades',
                //text: 'Editar',
                action: 'actualizar_antiguedades',
                iconCls: 'antiguedades',
                scale: 'large',
                tooltip: 'Actualizar todas las antiguedades'
            });
        }

        //Aplico el arreglo de botones a la vista para mostrar
        Ext.apply(this, {tbar: items});
        this.callParent(arguments);
                
    }
});