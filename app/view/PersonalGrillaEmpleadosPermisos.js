
Ext.define('personal.view.PersonalGrillaEmpleadosPermisos', {
	extend: 'Ext.grid.Panel',
	alias: 'widget.PersonalGrillaEmpleadosPermisos',
    store: 'PersonalPermisosStore',
	title: 'Seleccione los empleados a los que quiere que el empleado actual tenga acceso',
    id: 'idPersonalGrillaEmpleadosPermisos',
    frame: true,
    disabled: true,
    closeAction: 'destroy',
    modal: true,
    layout: 'fit',
    width: 900,
    minHeight: 180,
    items: [],
    /*selModel: selModel,
    plugins: [ Ext.create('Ext.ux.grid.Search', 
    {
        autoFocus: true,
    width: 180
    })],*/
    columns: [
            {header: 'Legajo', dataIndex: 'legajo', flex: 2},
            {header: 'Padron', dataIndex: 'padron', flex: 2},
            {header: 'Apellido', dataIndex: 'apellido', flex: 2},
            {header: 'Nombre', dataIndex: 'nombre', flex: 2},
            {header: 'DNI', dataIndex: 'nrodoc', flex: 2}
                    
            ],
    initComponent: function() {
        
        
        
        /*var gridItems = Ext.create('Ext.grid.Panel', 
        {
            store: 'PersonalStore',
            id: 'grillaPersonasPermisos',
            alias: 'grillaPersonasPermisos',
            markDirty: true,
            renderTo: Ext.getCmp('idPersonalGrillaEmpleadosPermisos'),
            tbar: [{
                id: 'guardarCambiosItem',
                text: 'Guardar cambios',
                iconCls: 'save2',
                tooltip: 'Guardar los cambios realizados.',
                action: 'guardarCambiosItem'

            }],
            
            
        });
        var items = [];
        items.push(gridItems);
        Ext.apply(this.items,items);
        //console.log(Ext.getStore('evaluaciones.ItemStore'));*/
		this.callParent(arguments);
	}
});