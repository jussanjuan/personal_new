
Ext.define('personal.view.PersonalGrillaFamiliares', {
	extend: 'Ext.grid.Panel',
	xtype: 'PersonalGrillaFamiliares',
    requires: [
        'personal.store.FamiliaStore',
    ],
    store: {
        type: 'FamiliaStore'
    },
    title: 'Familiares',
    reference: 'PersonalGrillaFamiliares',
    itemId: 'idPersonalGrillaFamiliares',
    frame: true,
    closeAction: 'destroy',
    modal: true,
    layout: 'fit',
    width: 900,
    minHeight: 180,
    tbar: [],
    columns: [
        {header: 'Relaci&oacute;n', dataIndex: 'relacion_nombre', flex: 2},
        {header: 'Nombre', dataIndex: 'nombre', flex: 4},
        {header: 'Apellido', dataIndex: 'apellido', flex: 4},
        {header: 'DNI', dataIndex: 'dni', flex: 2},
        {header: 'Fecha nacimiento', dataIndex: 'fecha_nac', flex: 2, renderer: Ext.util.Format.dateRenderer('d-m-Y')},
        {header: 'Cert. Inicio', dataIndex: 'certificado_inicio', flex: 1},
        {header: 'Cert. Fin', dataIndex: 'certificado_fin', flex: 1},
        {header: 'Guardap.', dataIndex: 'guardapolvo', flex: 1}
    ],
    viewConfig: { 
        stripeRows: false, 
        getRowClass: function(record) {
        var status = 0;
        status = (record.get('certificado_inicio')=='No' && record.get('certificado_fin')=='No')?'0':((record.get('certificado_inicio')=='Si' && record.get('certificado_fin')=='No')?'1':'2');
        console.log(status);
        switch(status)
        {
            case '0':
                return 'sin_cert-row';
                break;
            case '1':
                return 'cert_ini-row';
                break;
            case '2':
                return 'cert_fin-row';
                break;        
        }
        } 
    },
    initComponent: function() {
        var items = [];
        if(AppGlobals.permisos[0].pesta_legajo_familiares_editar == 1)
        {
            items.push({
                text: 'Agregar',
                action: 'agregar',
                iconCls: 'agregar',
                tooltip: 'Asigna un nuevo familiar al agente'
            },'-',
            {
                text: 'Eliminar',
                action: 'eliminarFamiliar',
                itemId: 'eliminarFamiliar',
                iconCls: 'quitar',
                disabled: true,
                tooltip: 'Quita un familiar asignado al agente'
            },'-',{
                text: 'Modificar familiar',
                action: 'openWindowModificarFamiliar',
                itemId: 'modificarFamiliar',
                iconCls: 'edit',
                disabled: true,
                tooltip: 'Modifica datos del familiar'
            });
        }
        //console.log(items);
        Ext.apply(this, {tbar: items});
        this.callParent(arguments);
	}
});