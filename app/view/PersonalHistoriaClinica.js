Ext.define('personal.view.PersonalHistoriaClinica', {
	extend: 'Ext.window.Window',
	alias: 'widget.PersonalHistoriaClinica',
	title: 'Agregar registro historia cl&iacute;nica',
	defaultType: 'textfield',
    closeAction: 'destroy',
    border: false,
    modal: true,
    width: 500,
    frame: true,
    closeAction: 'destroy',
    layout: 'fit',
    //minHeight: 500,
    //maxHeight: 1000,
    fieldDefaults: {
       	anchor: '100%',
       	labelAlign: 'top'
    },

    items: [{
        xtype: 'form',
        bodyPadding: 5,
        method: 'POST',
        id: 'formHistoriaClinica',
        items: [{
            xtype: 'textfield',
            id: 'legajoHistoriaClinica',
            name: 'legajo',
            fieldLabel: 'Legajo',
            readOnly: true,
            allowBlank: false,
            anchor: '95%'
            
        },{
            id: 'fechaHoraHistoriaClinica',
    	    name: 'fechaHora',
            xtype: 'datefield',
            fieldLabel: 'Fecha y hora',
            format: 'd/m/Y H:i:s',
            //minValue: new Date(),
            value: new Date(),
            allowBlank: false,
            anchor: '95%'
        }, {
            id: 'observacionesHistoriaClinica',
            xtype: 'textareafield',
            grow: true,
            anchor: '95%',
            height: 300,
            name: 'observacionesHistoriaClinica',
            fieldLabel: 'Observaciones'
        },{
            xtype: 'filefield',
            name: 'certificadoHistoriaClinica',
            fieldLabel: 'Certificado',
            //labelWidth: 50,
            msgTarget: 'side',
            anchor: '95%',
            buttonText: 'Seleccione certificado'
        }]
    }],

    buttons: [{
    	text: 'Guardar',
        action: 'saveRegistroHistoria',
        iconCls: 'save2'
        
    }, {
    	text: 'Cancelar',
        action: 'cancelar',
        iconCls: 'cancel2'
        
    }],

	initComponent: function() {

		this.callParent(arguments);
	}
});