Ext.define('personal.view.PersonalLugares', 
{
    extend: 'Ext.grid.Panel',
    xtype: 'PersonalLugares',
    requires: [
        'personal.store.historial.HLugarStore',
        'personal.view.PersonalAgregarLugar',
        'personal.view.PersonalAltaLugar',
        'personal.view.PersonalModificarLugar',
        'personal.view.PersonalBajaLugar'
    ],
    store: {
        type: 'HLugarStore'
    },
    reference: 'PersonalLugares',
    title: 'Listado de Lugares del Agente',
    //id: 'PersonalLugares',
    flex: 1,
    tbar:[],
    columns: [
		{header: 'Lugar', dataIndex: 'nombrelugar', flex: 1},
		{header: 'Desde', dataIndex: 'desde', renderer: Ext.util.Format.dateRenderer('d-m-Y'), flex: 1},
        {header: 'Hasta', dataIndex: 'hasta', renderer: function(val)
        {
            if(val!='Actualidad')
            {
                var fecha = new Date(val);
                var date = Ext.Date.add(new Date(fecha), Ext.Date.DAY,1);//Incremento un dia, debido a que un bug de Google Chrome decrementa la fecha
                var date = Ext.util.Format.date(date, 'd-m-Y');
                return date;
            }
            else
            {
                return val;
            }
        }, flex: 1}
	],
    
    initComponent: function() 
    {
        var items=[]; //Arreglo de items
        //Si el permiso es igual a 1, el usuario es administrador. Lleno el arreglo con todos los botones
        if(AppGlobals.permisos[0].pesta_legajo_lugares_editar == 1)//if(AppGlobals.permiso == "1") 
        {
            items.push(
            {
                text: 'Agregar',
                action: 'agregar',
                iconCls: 'agregar',
                tooltip: 'Asigna un nuevo lugar para el agente'
                },'-',{
                text: 'Eliminar',
                action: 'eliminarLugar',
                //id: 'eliminarLugar',
                iconCls: 'quitar',
                disabled: true,
                tooltip: 'Quita un lugar asignado al agente'
                },'->', {
                text: 'Alta lugar',
                action: 'altaLugar',
                //id: 'altaLugar',
                iconCls: 'add',
                tooltip: 'Alta de lugar en el sistema'

                },'-', {
                text: 'Baja lugar',
                action: 'bajaLugar',
                //id: 'bajaLugar',
                iconCls: 'delete',
                tooltip: 'Elimina un lugar del sistema'

                },'-', {
                text: 'Modificar lugar',
                action: 'modificarLugar',
                //id: 'modificarLugar',
                iconCls: 'edit',
                tooltip: 'Modifica lugar en el sistema'
                });
        //Aplico el arreglo de botones a la vista
        Ext.apply(this, {tbar: items});
        }
        this.callParent(arguments);
    }
});