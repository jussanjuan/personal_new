Ext.define('personal.view.PersonalModificarCapacitacion', {
    extend: 'Ext.window.Window',
    alias: 'widget.PersonalModificarCapacitacion',
    title: 'Modificar capacitaci&oacute;n',
    defaultType: 'textfield',
    closeAction: 'destroy',
    reference: 'PersonalModificarCapacitacion',
    border: false,
    modal: true,
    width: 600,
    resizable: false,
    requires: [
        'personal.store.CapacitacionTipoStore',
        'personal.store.CapacitacionOrigenStore',
        'personal.store.CapacitacionModalidadStore'
    ],
    items: [{
        xtype: 'form',
        bodyPadding: 5,
        method: 'POST',
        fieldDefaults: {
        anchor: '50%',
        layout: 'hbox',
        labelAlign: 'top'
        },
        items: [
        {
            xtype: 'container',
            layout: 'hbox',
            anchor: '100%',
            items: [
            {
                xtype: 'container',
                layout: 'anchor',
                flex: 1,
                padding: 5,
                items: [
                {
                    xtype: 'combo',
                    name: 'tipo',
                    displayField: 'nombre',
                    id: 'tipoCapacitacionEdit',
                    valueField: 'id',
                    fieldLabel: 'Tipo',
                    store: 'CapacitacionTipoStore',
                    validateOnChange: 'true',
                    validator: function(value){
                    if (!value)
                        return ("Seleccione capacitaci&oacute;n");
                    else
                        return true;
                    },
                    anchor: '100%'
                }, {
                    xtype: 'combo',
                    name: 'origen',
                    displayField: 'nombre',
                    id: 'origenCapacitacionEdit',
                    valueField: 'id',
                    fieldLabel: 'Origen',
                    store: 'CapacitacionOrigenStore',
                    validateOnChange: 'true',
                    validator: function(value){
                    if (!value)
                        return ("Seleccione entidad");
                    else
                        return true;
                    },
                    anchor: '100%'
                }, {
                    xtype: 'datefield',
                    name: 'fecha',
                    id: 'fechaCapacitacionEdit',
                    fieldLabel: 'Fecha',
                    format: 'd-m-Y',
                    maskRe: new RegExp(/^\d{1}$/),
                    validateOnChange: 'true',
                    validator: function(value){
                    if (!value)
                        return ("Este campo no puede estar vac&iacute;o");
                    else
                        return true;
                    },
                    anchor: '100%'
                    
                }, {
                    xtype: 'combo',
                    name: 'modalidad',
                    displayField: 'nombre',
                    id: 'modalidadCapacitacionEdit',
                    valueField: 'id',
                    fieldLabel: 'Modalidad',
                    store: 'CapacitacionModalidadStore',
                    validateOnChange: 'true',
                    validator: function(value){
                    if (!value)
                        return ("Seleccione modalidad");
                    else
                        return true;
                    },
                    anchor: '100%'
                }, {
                    xtype: 'numberfield',
                    name: 'nota_evaluacion',
                    id: 'notaCapacitacionEdit',
                    fieldLabel: 'Nota',
                    value: 0,
                    maxValue: 10,
                    minValue: 0,
                    hideTrigger: true,
                    allowDecimals: true,
                    anchor: '100%'
                    
                }]
            },
            {
                xtype: 'container',
                layout: 'anchor',
                flex: 1,
                padding: 5,
                items: [
                {
                    xtype: 'textfield',
                    name: 'nombre',
                    id: 'nombreCapacitacionEdit',
                    fieldLabel: 'Nombre',
                    validateOnChange: 'true',
                    validator: function(value){
                    if (!value)
                        return ("Este campo no puede estar vac&iacute;o");
                    else
                        return true;
                    },
                    anchor: '100%'
                    
                },
                {
                    xtype: 'combo',
                    name: 'inherencia',
                    store: ['Si', 'No'],
                    queryMode: 'local',
                    fieldLabel: 'Inherencia',
                    id: 'inherenciaCapacitacionEdit',
                    /*validateOnChange: 'true',
                    validator: function(value){
                    if (!value)
                        return ("Seleccione inherencia");
                    else
                        return true;
                    },*/
                    anchor: '100%'
                }, {
                    xtype: 'numberfield',
                    name: 'cant_horas',
                    id: 'cantHorasCapacitacionEdit',
                    fieldLabel: 'Cantidad de horas',
                    value: 4,
                    maxValue: 50000,
                    minValue: 1,
                    anchor: '100%',
                    validateOnChange: 'true',
                    validator: function(value){
                    if (!value)
                        return ("Ingrese cantidad de horas");
                    else
                        return true;
                    }
                }, 
                {
                    xtype: 'combo',
                    name: 'evaluacion',
                    id: 'evaluacionCapacitacionEdit',
                    store: ['Si', 'No'],
                    queryMode: 'local',
                    fieldLabel: 'Evaluacion?',
                    validateOnChange: 'true',
                    validator: function(value){
                    if (!value)
                        return ("Seleccione evaluacion");
                    else
                        return true;
                    },
                    anchor: '100%'
                }]
            }]
        },
        {
            xtype: 'textareafield',
            grow: false,
            anchor: '100%',
            name: 'observaciones',
            id: 'observacionesCapacitacionEdit',
            fieldLabel: 'Observaciones'
        }]       
    }],
    buttons: [{
        text: 'Guardar',
        action: 'guardar',
        id: 'editarAsigCapacitacion',
        iconCls: 'save2'
    }, {
        text: 'Cancelar',
        action: 'cancelar',
        iconCls: 'cancel2'
    }],
//funcion para obtener los campos invalidos
    getInvalidFields: function() {
            console.log('Entra al getinvalid');
           
        var invalidFields = [];
        Ext.suspendLayouts();
        var me = this.down('form');
        
        me.getForm().getFields().filterBy(function(field) {
            if (field.validate()) return;
            invalidFields.push(field);
        });
        Ext.resumeLayouts(true);
        return invalidFields;
    },

    initComponent: function() {

        this.callParent(arguments);
        
        
    }
});