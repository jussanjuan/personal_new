Ext.define('personal.view.PersonalModificarCargo', {
	extend: 'Ext.window.Window',
	alias: 'widget.PersonalModificarCargo',
	title: 'Modificar cargo',
	reference: 'PersonalModificarCargo',
    defaultType: 'textfield',
    closeAction: 'destroy',
    border: false,
    modal: true,
    fieldDefaults: {
       	anchor: '90%',
       	labelAlign: 'top'
    },
    requires: [
        'personal.store.CargosStore',
        'personal.store.EscalafonStore'
    ],
    width: 370,

    items: [{
        xtype: 'form',
        bodyPadding: 5,
        method: 'POST',

        items: [{
            xtype: 'combo',
            id: 'cargoModifCombo',
            name: 'cargoModif',
            displayField: 'cargo',
            valueField: 'id',
            fieldLabel: 'Seleccione el cargo',
            store: Ext.create('personal.store.CargosStore'),
            forceSelection: true,
            validateOnChange: 'true',
            queryMode: 'local',
            validator: function(value){
             if (!value)
                return ("Seleccione cargo");
            else
                return true;
            },
            anchor: '100%'
        },{
            xtype: 'textfield',
            name: 'nombreCargoModif',
            id: 'nombreCargoModif',
            fieldLabel: 'Nuevo nombre',
            validateOnChange: 'true',
            validator: function(value){
            if (!value)
                return ("Este campo no puede estar vac&iacute;o");
            else
                return true;
            },
            anchor: '100%'
        },{
            xtype: 'combo',
            name: 'escalafon',
            id: 'comboEscalafonModifCargo',
            displayField: 'nombre',
            valueField: 'id',
            fieldLabel: 'Escalafon',
            store: Ext.create('personal.store.EscalafonStore'),
            validateOnChange: 'true',
            validator: function(value){
             if (!value)
                return ("Seleccione escalaf&oacute;n");
            else
                return true;
            },
            
            anchor: '100%'
        }]
    }],

    buttons: [{
    	text: 'Modificar',
        action: 'modificar',
        iconCls: 'save2'
    },
    {
    	text: 'Cancelar',
        action: 'cancelar',
        iconCls: 'cancel2'
    }
],
//funcion para obtener los campos invalidos
    getInvalidFields: function() {
            console.log('Entra al getinvalid');
           
        var invalidFields = [];
        Ext.suspendLayouts();
        var me = this.down('form');
        
        me.getForm().getFields().filterBy(function(field) {
            if (field.validate()) return;
            invalidFields.push(field);
        });
        Ext.resumeLayouts(true);
        return invalidFields;
    }, 
    
	initComponent: function() {

		this.callParent(arguments);
	}
});