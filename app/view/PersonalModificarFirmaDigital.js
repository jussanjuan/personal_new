Ext.define('personal.view.PersonalModificarFirmaDigital', {
    extend: 'Ext.window.Window',
    alias: 'widget.PersonalModificarFirmaDigital',
    title: 'Modificar firma digital',
    defaultType: 'textfield',
    closeAction: 'destroy',
    reference: 'PersonalModificarFirmaDigital',
    border: false,
    modal: true,
    width: 500,
    frame: true,
    closeAction: 'destroy',
    layout: 'fit',
    //minHeight: 500,
    //maxHeight: 1000,
    fieldDefaults: {
        anchor: '100%',
        labelAlign: 'top'
    },

    items: [{
        xtype: 'form',
        bodyPadding: 5,
        method: 'POST',
        id: 'formModificarFirmaDigital',
        items: [
        {
            xtype: 'combo',
            name: 'tipoFirmaDigitalModificar',
            id: 'tipoFirmaDigitalModificar',
            displayField: 'nombre',
            valueField: 'id',
            fieldLabel: 'Tipo',
            store: 'FirmaDigitalTipoStore',
            validateOnChange: 'true',
            validator: function(value){
            if (!value)
                return ("Seleccione tipo");
            else
                return true;
            },
            anchor: '95%'
        },{
            xtype: 'textfield',
            id: 'descripcionFirmaDigitalModificar',
            name: 'descripcionFirmaDigital',
            fieldLabel: 'Descripci&oacute;n',
            allowBlank: true,
            anchor: '95%'
            
        },{
            id: 'fechaAltaModificar',
            name: 'fechaAlta',
            xtype: 'datefield',
            fieldLabel: 'Fecha alta',
            format: 'd/m/Y H:i:s',
            //minValue: new Date(),
            value: new Date(),
            allowBlank: false,
            anchor: '95%'
        },{
            id: 'fechaRevocacionModificar',
            name: 'fechaRevocacion',
            xtype: 'datefield',
            fieldLabel: 'Fecha revocac&oacute;n',
            format: 'd/m/Y H:i:s',
            //minValue: new Date(),
            allowBlank: false,
            anchor: '95%'
        }]
    }],

    buttons: [{
        text: 'Guardar',
        action: 'editarFirmaDigital',
        id: 'saveEditarFirmaDigital',
        iconCls: 'save2'
        
    }, {
        text: 'Cancelar',
        action: 'cancelar',
        iconCls: 'cancel2'
        
    }],
    //funcion para obtener los campos invalidos
    getInvalidFields: function() {
        
           
        var invalidFields = [];
        Ext.suspendLayouts();
        var me = this.down('form');
        
        me.getForm().getFields().filterBy(function(field) {
            if (field.validate()) return;
            invalidFields.push(field);
        });
        Ext.resumeLayouts(true);
        return invalidFields;
    },
    initComponent: function() {

        this.callParent(arguments);
    }
});