Ext.define('personal.view.PersonalModificarLugar', {
	extend: 'Ext.window.Window',
	alias: 'widget.PersonalModificarLugar',
	title: 'Modificar lugar',
	reference: 'PersonalModificarLugar',
    defaultType: 'textfield',
    closeAction: 'destroy',
    border: false,
    modal: true,
    fieldDefaults: {
       	anchor: '90%',
       	labelAlign: 'top'
    },

    width: 370,

    items: [{
        xtype: 'form',
        bodyPadding: 5,
        method: 'POST',

        items: [{
            xtype: 'combo',
            name: 'lugarModif',
            id: 'lugarModifCombo',
            displayField: 'nombrelugar',
            valueField: 'id',
            fieldLabel: 'Seleccione el lugar',
            store: 'LugarStore',
            queryMode: 'local',
            validateOnChange: 'true',
            validator: function(value){
             if (!value)
                return ("Seleccione lugar");
            else
                return true;
            },
            anchor: '100%'
        },{
            xtype: 'textfield',
            name: 'nombreLugarModif',
            id: 'nombreLugarModif',
            fieldLabel: 'Nuevo nombre',
            validateOnChange: 'true',
            validator: function(value){
            if (!value)
                return ("Este campo no puede estar vac&iacute;o");
            else
                return true;
            },
            anchor: '100%'
        },{
            xtype: 'combo',
            name: 'fuero',
            id: 'comboFueroModifLugar',
            displayField: 'nombre',
            valueField: 'id',
            fieldLabel: 'Fuero',
            store: 'FueroStore',
            validateOnChange: 'true',
            validator: function(value){
             if (!value)
                return ("Seleccione fuero");
            else
                return true;
            },
            
            anchor: '100%'
        }]
    }],

    buttons: [{
    	text: 'Modificar',
        action: 'modificar',
        iconCls: 'save2'
    },
    {
    	text: 'Cancelar',
        action: 'cancelar',
        iconCls: 'cancel2'
    }
],
//funcion para obtener los campos invalidos
    getInvalidFields: function() {
            console.log('Entra al getinvalid');
           
        var invalidFields = [];
        Ext.suspendLayouts();
        var me = this.down('form');
        
        me.getForm().getFields().filterBy(function(field) {
            if (field.validate()) return;
            invalidFields.push(field);
        });
        Ext.resumeLayouts(true);
        return invalidFields;
    }, 
    
	initComponent: function() {

		this.callParent(arguments);
	}
});