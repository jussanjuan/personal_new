Ext.define('personal.view.PersonalModificarTitulo', {
	extend: 'Ext.window.Window',
	alias: 'widget.PersonalModificarTitulo',
	title: 'Modificar titulo',
	reference: 'PersonalModificarTitulo',
    defaultType: 'textfield',
    closeAction: 'destroy',
    border: false,
    modal: true,
    fieldDefaults: {
       	anchor: '90%',
       	labelAlign: 'top'
    },
    requires: [
        'personal.store.TipoEstudioStore'
    ],
    width: 370,

    items: [{
        xtype: 'form',
        bodyPadding: 5,
        method: 'POST',

        items: [{
            xtype: 'combo',
            name: 'tituloModif',
            displayField: 'titulo',
            valueField: 'id',
            fieldLabel: 'Seleccione el titulo',
            store: 'TipoEstudioStore',
            forceSelection: true,
            queryMode: 'local',
            validateOnChange: 'true',
            validator: function(value){
             if (!value)
                return ("Seleccione titulo");
            else
                return true;
            },
            anchor: '100%'
        },{
            xtype: 'textfield',
            name: 'nombreTituloModif',
            fieldLabel: 'Nuevo nombre',
            validateOnChange: 'true',
            validator: function(value){
            if (!value)
                return ("Este campo no puede estar vac&iacute;o");
            else
                return true;
            },
            anchor: '100%'
        }]
    }],

    buttons: [{
    	text: 'Modificar',
        action: 'modificar',
        iconCls: 'save2'
    },
    {
    	text: 'Cancelar',
        action: 'cancelar',
        iconCls: 'cancel2'
    }
],
//funcion para obtener los campos invalidos
    getInvalidFields: function() {
            console.log('Entra al getinvalid');
           
        var invalidFields = [];
        Ext.suspendLayouts();
        var me = this.down('form');
        
        me.getForm().getFields().filterBy(function(field) {
            if (field.validate()) return;
            invalidFields.push(field);
        });
        Ext.resumeLayouts(true);
        return invalidFields;
    }, 
    
	initComponent: function() {

		this.callParent(arguments);
	}
});