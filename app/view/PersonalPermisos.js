/*var selModel = Ext.create('Ext.selection.CheckboxModel',
{
    checkOnly: true,
    mode: 'MULTI',
    renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
    {
        var baseCSSPrefix = Ext.baseCSSPrefix;
        metaData.tdCls = baseCSSPrefix + 'grid-cell-special ' + baseCSSPrefix + 'grid-cell-row-checker';
        return record.get('id') == 1 ? '' : '<div class="' + baseCSSPrefix + 'grid-row-checker">&#160;</div>';
    }
});*/

/*var gridItems = Ext.create('Ext.grid.Panel', 
{
    store: 'PersonalStore',
    id: 'grillaPersonasPermisos',
    alias: 'grillaPersonasPermisos',
    markDirty: true,
    renderTo: Ext.getCmp('idPersonalGrillaEmpleadosPermisos'),
    tbar: [{
        id: 'guardarCambiosItem',
        text: 'Guardar cambios',
        iconCls: 'save2',
        tooltip: 'Guardar los cambios realizados.',
        action: 'guardarCambiosItem'

    }],
    selModel: selModel,
    columns: [
            {header: 'Legajo', dataIndex: 'legajo', flex: 2},
            {header: 'Padron', dataIndex: 'padron', flex: 2},
            {header: 'Apellido', dataIndex: 'apellido', flex: 2},
            {header: 'Nombre', dataIndex: 'nombre', flex: 2},
            {header: 'DNI', dataIndex: 'nrodoc', flex: 2}
            
    ]
    
});*/

Ext.define('personal.view.PersonalPermisos', 
{
    extend: 'Ext.Container',
    alias: 'widget.PersonalPermisos',
    layout: {
        type: 'vbox',       
        align: 'stretch',    
        padding: 5
    },
    title: 'Permisos',
    id: 'PersonalPermisos',
    flex: 1,
    tbar: [{
                id: 'btnGuardarPerm',
                //text: 'Guardar',
                iconCls: 'save',
                scale: 'large',
                tooltip: 'Guardar',
                action: 'guardarPerm'
            }],
    requires: [
        'personal.view.PersonalGrillaEmpleadosPermisos'
    ],        
    items:[
    {
        //title: 'Asignar, modificar y denegar permisos del usuario en el sistema',
        xtype: 'form',
        
        border: false,
        autoScroll: true,
        flex: 1,
        fieldDefaults: 
        {
            anchor: '90%',
            labelAlign: 'top'
        },
        defaultType: 'textfield',
        bodyPadding: 10,
        items:[
            {
            xtype: 'container',
            layout: 'anchor',
            flex: 1,
            //padding: 10,
            items: [
            {
                xtype: 'combo',
                name: 'tipoPermVista',
                id: 'tipoPermVista',
                width: 50,
                editable: false,
                store: [['0', 'Permiso total (todos los agentes activos)'], ['4', 'Permiso de fuero'], ['3', 'Permiso de area'], ['2', 'Permiso agentes especificos'], ['1', 'Permiso individual (Permiso por defecto. Solo propio legajo)']],
                queryMode: 'local',
                validateOnChange: 'true',
                validator: function(value)
                {
                    if (!value)
                    {
                        return ("Seleccione tipo de permiso de vista");
                    }
                    else
                    {
                        return true;
                    }
                },
                fieldLabel: 'Vista de listado de agentes'
                
            }, 
            {
                xtype: 'combo',
                name: 'tipoPermPest',
                id: 'tipoPermPest',
                width: 50,
                editable: false,
                store: [['1', 'Administrador (todas las pestañas, carga de licencias, edicion total de legajos y edicion de permisos)'], ['2', 'Usuario avanzado (todas las pestañas y edicion de legajo propio)'], ['3', 'Usuario basico (Permiso por defecto. Solo pestaña de Personal y Evaluacion (si aplica))']],
                queryMode: 'local',
                validateOnChange: 'true',
                validator: function(value)
                {
                    if (!value)
                    {
                        return ("Seleccione tipo de permiso de vista");
                    }
                    else
                    {
                        return true;
                    }
                },
                fieldLabel: 'Vista de pestañas'
                
            }]//End items
        }]//End items
    }, {
            xtype: 'splitter'
    }, {
            xtype: 'PersonalGrillaEmpleadosPermisos',
            flex: 3.5
            
        }],
    
   //funcion para obtener los campos invalidos
    getInvalidFields: function() {
            console.log('Entra al getinvalid');
           
        var invalidFields = [];
        Ext.suspendLayouts();
        var me = this.down('form');
        
        me.getForm().getFields().filterBy(function(field) {
            if (field.validate()) return;
            invalidFields.push(field);
        });
        Ext.resumeLayouts(true);
        return invalidFields;
    }, 
    initComponent: function() 
    {
        
        this.callParent(arguments);
    }
});