Ext.define('personal.view.PersonalSanciones', 
{
    extend: 'Ext.grid.Panel',
    xtype: 'PersonalSanciones',
    requires: [
        'personal.store.historial.HSancionStore',
        'personal.view.PersonalAgregarSancion'
    ],
    store: {
        type: 'HSancionStore'
    },
    reference: 'PersonalSanciones',
    title: 'Listado de sanciones del Agente',
    itemId: 'PersonalSanciones',
    flex: 1,
    tbar:[],
    columns: [
        {header: 'Causa', dataIndex: 'nombre', flex: 1},
		{header: 'Desde', dataIndex: 'desde', renderer: Ext.util.Format.dateRenderer('d-m-Y'), flex: 1},
        {header: 'Hasta', dataIndex: 'hasta', renderer: Ext.util.Format.dateRenderer('d-m-Y'), flex: 1},
        {header: 'Dispuesta por', dataIndex: 'dispuesta_por', flex: 1},
        {header: 'Nro. exp.', dataIndex: 'expediente', flex: 1},
        {header: 'Observaciones', dataIndex: 'observaciones', flex: 7}
	],
    listeners: 
    {
            
        'selectionchange': function(view, records) 
        {
            if(AppGlobals.permisos[0].pesta_legajo_sanciones_editar == 1)//if(AppGlobals.permiso == "1")
            {
                this.down('#eliminarSancion').setDisabled(!records.length);
            }
        }
    },
    initComponent: function() 
    {
        var items=[]; //Arreglo de items
        //Si el permiso aplica, lleno el arreglo con todos los botones
        if(AppGlobals.permisos[0].pesta_legajo_sanciones_editar == 1)
        {
            items.push(
            {
                text: 'Agregar',
                action: 'agregarSancion',
                iconCls: 'agregar',
                tooltip: 'Asigna una nueva sanci&oacute;n para el agente'
            },'-',
            {
                text: 'Eliminar',
                action: 'eliminarSancion',
                itemId: 'eliminarSancion',
                iconCls: 'quitar',
                disabled: true,
                tooltip: 'Quita una sanci&oacute;n asignada al agente'
            });
        //Aplico el arreglo de botones a la vista
        Ext.apply(this, {tbar: items});    
        }
        this.callParent(arguments);
    }
});