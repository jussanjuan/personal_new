Ext.define('personal.view.PersonalTitulos', 
{
    extend: 'Ext.grid.Panel',
    xtype: 'PersonalTitulos',
    requires: [
        'personal.store.historial.HTituloStore',
        'personal.view.PersonalAgregarTitulo',
        'personal.view.PersonalAltaTitulo',
        'personal.view.PersonalBajaTitulo',
        'personal.view.PersonalModificarTitulo'
    ],
    store: {
        type: 'HTituloStore'
    },
    reference: 'PersonalTitulos',
    title: 'Listado de titulos del Agente',
    //id: 'PersonalTitulos',
    flex: 1,
    tbar:[],
    plugins: {
        ptype: 'rowediting',
        clicksToEdit: 2
    },
    columns: [
            {text: 'Titulo', dataIndex: 'titulo', flex: 2},
            {text: 'Establecimiento', dataIndex: 'establec', flex: 2, editor: {xtype: 'textfield',
                allowBlank: false
                }},
            {text: 'Fecha', dataIndex: 'fecha_titulo', flex: 1, renderer: Ext.util.Format.dateRenderer('d-m-Y'), editor: {xtype: 'datefield',
                allowBlank: false,
                format: 'd-m-Y'
                }}
	],
    
    initComponent: function() 
    {
        var items=[]; //Arreglo de items
        //Si el permiso aplica, lleno el arreglo con todos los botones
        if(AppGlobals.permisos[0].pesta_legajo_titulos_editar == 1)
        {
            items.push(
            {
            text: 'Agregar',
            action: 'agregarTitulo',
            iconCls: 'agregar',
            tooltip: 'Asigna un nuevo t&iacute;tulo para el agente'
            },'-',{
                text: 'Modificar',
                action: 'editarTitulo',
                //id: 'editarTitulo',
                iconCls: 'folderedit',
                disabled: true,
                tooltip: 'Modifica el t&iacute;tulo asignado al agente'
            },'-',{
                text: 'Eliminar',
                action: 'eliminarTitulo',
                //id: 'eliminarTitulo',
                iconCls: 'quitar',
                disabled: true,
                tooltip: 'Quita un t&iacute;tulo asignado al agente'
            },'-',{
                disabled: true,
                //id: 'btnGuardarCambiosTitulo',
                //text: 'Desiste',
                iconCls: 'save2',
                action: 'guardarCambiosTitulo',
                //scale: 'medium',
                tooltip: 'Guardar cambios'
            },'->',{
                text: 'Alta titulo',
                action: 'altaTitulo',
                //id: 'altaTitulo',
                iconCls: 'add',
                tooltip: 'Alta de t&iacute;tulo en el sistema'

            },'-',{
                text: 'Baja titulo',
                action: 'bajaTitulo',
                //id: 'bajaTitulo',
                iconCls: 'delete',
                tooltip: 'Elimina un t&iacute;tulo del sistema'

            },'-',{
                text: 'Modificar titulo',
                action: 'modificarTitulo',
                //id: 'modificarTitulo',
                iconCls: 'edit',
                tooltip: 'Modifica t&iacute;tulo en el sistema'

            });
        //Aplico el arreglo de botones a la vista
        Ext.apply(this, {tbar: items});    
        }
        this.callParent(arguments);
    }
});