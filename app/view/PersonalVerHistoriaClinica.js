Ext.define('personal.view.PersonalVerHistoriaClinica', {
	extend: 'Ext.window.Window',
	alias: 'widget.PersonalVerHistoriaClinica',
	title: 'Ver registro historia cl&iacute;nica',
	defaultType: 'textfield',
    closeAction: 'destroy',
    border: false,
    modal: true,
    width: 500,
    frame: true,
    closeAction: 'destroy',
    layout: 'fit',
    //minHeight: 500,
    //maxHeight: 1000,
    fieldDefaults: {
       	anchor: '100%',
       	labelAlign: 'top'
    },

    items: [{
        xtype: 'form',
        bodyPadding: 5,
        method: 'POST',
        id: 'formVerHistoriaClinica',
        items: [{
            xtype: 'textfield',
            id: 'legajoVerHistoriaClinica',
            name: 'legajo',
            fieldLabel: 'Legajo',
            readOnly: true,
            allowBlank: false,
            anchor: '95%'
            
        },{
            id: 'fechaHoraVerHistoriaClinica',
    	    name: 'fechaHora',
            xtype: 'datefield',
            fieldLabel: 'Fecha y hora',
            format: 'd/m/Y H:i:s',
            readOnly: true,
            value: new Date(),
            allowBlank: false,
            anchor: '95%'
        }, {
            id: 'observacionesVerHistoriaClinica',
            xtype: 'textareafield',
            grow: true,
            anchor: '95%',
            height: 300,
            readOnly: true,
            name: 'observacionesHistoriaClinica',
            fieldLabel: 'Observaciones'
        }]
    }],

    buttons: [{
    	text: 'Cerrar',
        action: 'cancelar',
        iconCls: 'cancel2'
        
    }],

	initComponent: function() {

		this.callParent(arguments);
	}
});