Ext.define('personal.view.PersonalWebcam', {
	extend: 'Ext.window.Window',
	alias: 'widget.PersonalWebcam',
	title: 'Hacer foto',
	//frame: true,
    //border: false,
    //flex: 1,
    defaultType: 'textfield',
    closeAction: 'destroy',
    border: false,
    modal: true,
    fieldDefaults: {
       	anchor: '90%',
       	labelAlign: 'top'
    },

    width: 347,
    height: 330,

    items : [{
        xtype : 'component',
        bodyPadding: 5,
        layout : 'fit',
        id: 'Cam',
        
        autoEl : {
            name: 'iCam',
            tag : "iframe",
            frameborder: 0,
            width: '100%',
            height: '100%',
            scrolling: 'no',
            src : "camara/camara.php"
        }
    }],

    /*buttons: [{
    	text: 'Guardar',
        action: 'guardar'
    }],*/

	initComponent: function() {

		this.callParent(arguments);
	}
});