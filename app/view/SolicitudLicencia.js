Ext.define('personal.view.SolicitudLicencia', {
	extend: 'Ext.window.Window',
	alias: 'widget.SolicitudLicencia',
	title: 'Agregar solicitud de licencia',
	defaultType: 'textfield',
    closeAction: 'destroy',
    border: false,
    modal: true,
    width: 500,
    frame: true,
    layout: 'fit',
    //minHeight: 500,
    //maxHeight: 1000,
    fieldDefaults: {
       	anchor: '100%',
       	labelAlign: 'top'
    },
    requires:[
        'personal.store.LicenciaTipoStore'        
    ],
    items: [{
        xtype: 'form',
        bodyPadding: 5,
        method: 'POST',
        id: 'formSolicitudLicencia',
        items: [{
            xtype: 'combo',
            name: 'tipoLicencia',
            displayField: 'nombre',
            valueField: 'id',
            fieldLabel: 'Licencia',
            store: Ext.create('personal.store.LicenciaTipoStore'),
            forceSelection: true,
            validateOnChange: 'true',
            validator: function(value){
            if (!value)
                return ("Seleccione licencia");
            else
                return true;
            },
            anchor: '95%',
            allowBlank: false
        },
        {
            xtype: 'textfield',
            id: 'legajoSolicitudLicencia',
            name: 'legajo',
            fieldLabel: 'Legajo',
            readOnly: true,
            allowBlank: false,
            anchor: '95%'
            
        },{
            id: 'solicitudLicenciaDesde',
    	    name: 'solicitudLicenciaDesde',
            xtype: 'datefield',
            fieldLabel: 'Desde',
            format: 'd/m/Y',
            //minValue: new Date(),
            value: new Date(),
            anchor: '95%',
            allowBlank: false
        },{
            id: 'solicitudCausa',
            xtype: 'textfield',
            name: 'solicitudCausa',
            fieldLabel: 'Causa',
            anchor: '95%'
        },{
            id: 'solicitudExpediente',
            xtype: 'textfield',
            name: 'solicitudExpediente',
            //maskRe: new RegExp(/^\d{1}$/),
            fieldLabel: 'Expediente',
            anchor: '95%'
        },{
            id: 'solicitudObservaciones',
            xtype: 'textareafield',
            grow: true,
            anchor: '95%',
            name: 'solicitudObservaciones',
            fieldLabel: 'Observaciones'
        },{
            id: 'solicitudSolicitante',
            xtype: 'textfield',
            name: 'solicitudSolicitante',
            fieldLabel: 'Solicitante',
            anchor: '95%',
            allowBlank: false
        },{
            id: 'domicilioSolicitante',
            xtype: 'textfield',
            name: 'domicilioSolicitante',
            fieldLabel: 'Domicilio',
            anchor: '95%',
            allowBlank: false
        }/*,{
            xtype: 'timefield',
            name: 'horaSolicitud',
            fieldLabel: 'Horario llamado',
            minValue: '6:00 AM',
            maxValue: '1:00 PM',
            value: '9:00 AM',
            increment: 5,
            anchor: '95%',
            invalidText: 'El formato no es correcto!',
            allowBlank: false,
            format: 'H:i'
        }*/,{
            id: 'pierdePresentismoSolicitud',
            name: 'pierdePresentismoSolicitud',
            xtype: 'checkbox',
            boxLabel: 'Pierde presentismo',
            checked: false
        }]
    }],

    buttons: [{
    	text: 'Guardar',
        action: 'saveSolicitudLicencia',
        iconCls: 'save2'
        
    }, {
    	text: 'Cancelar',
        action: 'cancelar',
        iconCls: 'cancel2'
        
    }],

	initComponent: function() {

		this.callParent(arguments);
	}
});