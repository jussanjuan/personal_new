Ext.define('personal.view.auditoria.AuditoriaPanel', 
{
	extend: 'Ext.grid.Panel',
	xtype: 'AuditoriaPanel',
    id: 'auditoriaPanel',
	requires: [
        'personal.store.auditoria.AuditoriaStore'
    ],
    store: {
        type: 'AuditoriaStore'
    },
    reference: 'AuditoriaPanel',
    title: 'Auditoria',
    flex: 1,
    tbar: [],
    bbar: {
        xtype: 'pagingtoolbar',
        displayInfo: true,
        beforePageText: 'P&aacute;gina',
        afterPageText: 'de {0}',
        displayMsg: 'Mostrando {0} - {1} de {2}',
        emptyMsg: "No hay agentes para mostrar",
    },   
    columns: [
		{text: 'Legajo', dataIndex: 'legajo', flex: 1},
		{text: 'Apellido', dataIndex: 'apellido', flex: 2},
		{text: 'Nombre', dataIndex: 'nombreEmpleado', flex: 2},
		{text: 'Legajo obj.', dataIndex: 'legajo_objetivo', flex: 1},
		{text: 'Apellido obj.', dataIndex: 'apellidoObjetivo', flex: 2},
		{text: 'Nombre obj.', dataIndex: 'nombreObjetivo', flex: 2},
		{text: 'Tabla', dataIndex: 'tabla', flex: 1},
		{text: 'Operacion', dataIndex: 'nombre', flex: 1},
		{text: 'Consulta', dataIndex: 'log', flex: 10},
		{text: 'Fecha y hora', dataIndex: 'fecha',  renderer: Ext.util.Format.dateRenderer('d-m-Y H:i:s'), flex: 2}
	],
    initComponent: function() 
    {
        var items=[]; //Arreglo de items
        
        
        items.push({
            xtype: 'textfield',
            name: 'searchAuditoriaGrilla',
            fieldLabel: 'Buscar',
            labelAlign: 'left',
            width: '100%',
            labelWidth: 45
        });
        
        //Aplico el arreglo de botones a la vista para mostrar
        Ext.apply(this, {tbar: items});
        this.callParent(arguments);
                
    }
});
