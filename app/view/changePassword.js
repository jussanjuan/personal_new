Ext.define('personal.view.changePassword', {
	extend: 'Ext.window.Window',
	alias: 'widget.changePassword',
	title: 'Cambiar contrase&ntilde;a',
	reference: 'changePassword',
    defaultType: 'textfield',
    closeAction: 'destroy',
    border: false,
    modal: true,
    resizable: false,
    closable: false,
    fieldDefaults: {
       	anchor: '90%',
       	labelAlign: 'top'
    },

    width: 400,
    

    items: [{
        xtype: 'form',
        bodyPadding: 5,
        method: 'POST',
        monitorValid: true,
        items: [{
            xtype: 'textfield',
            name: 'oldPass',
            fieldLabel: 'Contrase&ntilde;a anterior',
            inputType:'password',
            allowBlank: false,
            validateOnChange: 'true',
            validator: function(value){
            if (!value)
                return ("Este campo no puede estar vac&iacute;o");
            else
                return true;
            },
            anchor: '100%'
        },{
            xtype: 'textfield',
            name: 'newPass',
            id: 'newPass',
            fieldLabel: 'Contrase&ntilde;a nueva',
            inputType:'password',
            allowBlank: false,
            validateOnChange: 'true',
            validator: function(value){
            if (!value)
                return ("Este campo no puede estar vac&iacute;o");
            else
                return true;
            },
            anchor: '100%'
        },{
            xtype: 'textfield',
            name: 'repeatNewPass',
            id: 'repeatNewPass',
            inputType:'password',
            allowBlank: false,
            fieldLabel: 'Confirmar contrase&ntilde;a nueva',
            validateOnChange: 'true',
            validator: function(value){
            if (!value)
            {
                return ("Este campo no puede estar vac&iacute;o");
            }
            else
            {
                if(value != Ext.getCmp('newPass').value)
                    return ("Las contrase&ntilde;as no coinciden");
                else
                {
                    return true;
                }
            }
            },
            anchor: '100%'
        }]
    }],

    buttons: [{
    	text: 'Guardar',
        action: 'guardar',
        iconCls: 'save2'
        //disabled: true
    },
    {
    	text: 'Cancelar',
        action: 'cancelar',
        iconCls: 'cancel2'
        //disabled: true
    }
],
//funcion para obtener los campos invalidos
    getInvalidFields: function() {
            console.log('Entra al getinvalid');
           
        var invalidFields = [];
        Ext.suspendLayouts();
        var me = this.down('form');
        
        me.getForm().getFields().filterBy(function(field) {
            if (field.validate()) return;
            invalidFields.push(field);
        });
        Ext.resumeLayouts(true);
        return invalidFields;
    }, 
    
	initComponent: function() {

		this.callParent(arguments);
	}
});