Ext.require('Ext.chart.*');


getDataFromDB = function(){
  
  var data = [];
  var arrProp = [];
  var arr='';
  Ext.Ajax.request(
    {
    url: 'app/proxy.charts.php',
    method: 'GET',
    params: {
            chart: 'ausentismo',
            hist: 'si'
            
            
    },
    success: function(response, opts) {
    var obj = Ext.decode(response.responseText);
    Ext.each(obj.results, function(licencia){
        data.push(licencia);
    });
    //console.log(data);
    var a = Object.getOwnPropertyNames(data[1]);
    AppGlobals.arrFieldsHist = Object.getOwnPropertyNames(data[1]);
    console.log(AppGlobals.arrFieldsHist);
    var b= Object.keys(a).map(function (key) { return a[key]; });
    //console.log(a);
    for(var i=0; i<a.length;i++)
    {
        arr=arr+a[i];
        if(i!=a.length-1)
            arr = arr+"-";
    }
    Ext.each(Object.getOwnPropertyNames(data[1]), function(propiedad){
        arr[i]=String(propiedad);
        i++;
        
    });
    
    
    
    },
    failure: function(response, opts) {
    Ext.Msg.alert('Informacion', 'Error de servidor. <br>Estado: ' + response.status);
    console.log('server-side failure with status code ' + response.status);
    }
});

return AppGlobals.arrFieldsHist;
};



/*var chartLicSalud = Ext.create('Ext.chart.Chart',{
            xtype: 'chart',
            animate: true,
            store: 'estadisticas.licencias.ELicenciasStore',
            insetPadding: 30,
            width: 500,
            height: 500,
            
            axes: [{
                type: 'Numeric',
                minimum: 0,
                position: 'left',
                fields: ['cantidad'],
                title: "Cantidad de licencias",
                grid: true,
                label: {
                    renderer: Ext.util.Format.numberRenderer('0,0'),
                    font: '10px Arial'
                }
            }, {
                type: 'Category',
                position: 'bottom',
                fields: ['mes'],
                title: "Mes",
                label: {
                    font: '11px Arial'
                    
                },
                label: {
                    rotate: {
                        degrees: 270
                    }
                }
            }],
            series: [{
                type: 'line',
                axis: 'left',
                xField: 'mes',
                yField: 'cantidad',
                fill: true,
                fillOpacity: 0.5,
                tips: {
                    trackMouse: true,
                    width: 200,
                    height: 55,
                    renderer: function(storeItem, item) {
                        this.setTitle(storeItem.get('cantidad') + ' licencias en ' + storeItem.get('mes'));
                    }
                },
                style: {
                    fill: '#38B8BF',
                    stroke: '#38B8BF',
                    'stroke-width': 3
                },
                markerConfig: {
                    type: 'circle',
                    size: 4,
                    radius: 4,
                    'stroke-width': 0,
                    fill: '#38B8BF',
                    stroke: '#38B8BF'
                }
            }]
        });*/

var chartAusentismo = Ext.create({
            xtype: 'cartesian',
            animate: true,
            store:[],
            insetPadding: 30,
            width: 500,
            height: 500,
            legend: {
                position: 'right'
            },
            axes: [{
                type: 'Numeric',
                minimum: 0,
                position: 'left',
                fields: ['cantidad'],
                title: "Cantidad de licencias",
                grid: true,
                label: {
                    renderer: Ext.util.Format.numberRenderer('0,0'),
                    font: '10px Arial'
                }
            }, {
                type: 'Category',
                position: 'bottom',
                fields: ['anio'],
                title: "Año",
                label: {
                    font: '11px Arial'
                    
                },
                label: {
                    rotate: {
                        degrees: 270
                    }
                }
            }],
            series: []
        });

    
Ext.define('RH.view.estadisticas.EAusentismoChart',
{
    extend: 'Ext.Panel',
    alias: 'widget.EAusentismoChart',
    
    bodyPadding: 2,
    layout: {
            type: 'hbox',
            align: 'stretch'
        },
    items:[
        {
            title: 'Cantidad de licencias por a&ntilde;o',
            flex: 1,
            //collapseDirection: 'left',
            tbar: [{
                text: 'Descargar gr&aacute;fico',
                handler: function() {
                    Ext.MessageBox.confirm('Confirmar descarga', 'Quiere descargar el gr&aacute;fico como im&aacute;gen?', function(choice){
                        if(choice == 'yes'){
                            chartAusentismo.save({
                                type: 'image/png'
                            });
                        }
                    });
                }
            }],
            items: chartAusentismo
        }
    ],
    
    initComponent: function()
    {
        var sto=Ext.getStore('estadisticas.EAusentismoStore');
        sto.load({
                params: {
                        chart: 'ausentismo',
                        hist: 'si'
                        
                }
        });
        
        var items = this.items;
        Ext.apply(chartAusentismo, {store: sto});
                
        this.callParent(arguments);
    }    
});