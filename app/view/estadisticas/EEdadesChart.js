Ext.require('Ext.chart.*');
Ext.require(['Ext.Window', 'Ext.layout.container.Fit', 'Ext.fx.target.Sprite', 'Ext.window.MessageBox']);


var colors = [
    'url(#v-1)',
    'url(#v-2)',
    'url(#v-3)',
    'url(#v-4)',
    'url(#v-5)',
    'url(#v-6)',
    'url(#v-7)'];
    
    var baseColor = '#eee';
    
    Ext.define('Ext.chart.theme.Fancy', {
        extend: 'Ext.chart.theme.Base',
        
        constructor: function(config) {
            this.callParent([Ext.apply({
                axis: {
                    fill: baseColor,
                    stroke: baseColor
                },
                axisLabelLeft: {
                    fill: baseColor
                },
                axisLabelBottom: {
                    fill: baseColor
                },
                axisTitleLeft: {
                    fill: baseColor
                },
                axisTitleBottom: {
                    fill: baseColor
                },
                colors: colors
            }, config)]);
        }
    });
var chartEdades = Ext.create('Ext.chart.Chart', {
            id: 'chartEdad',
            xtype: 'chart',
            theme: 'Fancy',
            width: 600,
            height: 500,
            minHeight: 400,
            minWidth: 450,
            insetPadding: 30,
            hidden: false,
            maximizable: true,
            animate: {
                easing: 'bounceOut',
                duration: 750
            },
            store: 'estadisticas.EEdadesStore',
            background: {
                fill: 'rgb(17, 17, 17)'
            },
            gradients: [
            {
                'id': 'v-1',
                'angle': 0,
                stops: {
                    0: {
                        color: 'rgb(212, 40, 40)'
                    },
                    100: {
                        color: 'rgb(117, 14, 14)'
                    }
                }
            },
            {
                'id': 'v-2',
                'angle': 0,
                stops: {
                    0: {
                        color: 'rgb(180, 216, 42)'
                    },
                    100: {
                        color: 'rgb(94, 114, 13)'
                    }
                }
            },
            {
                'id': 'v-3',
                'angle': 0,
                stops: {
                    0: {
                        color: 'rgb(43, 221, 115)'
                    },
                    100: {
                        color: 'rgb(14, 117, 56)'
                    }
                }
            },
            {
                'id': 'v-4',
                'angle': 0,
                stops: {
                    0: {
                        color: 'rgb(45, 117, 226)'
                    },
                    100: {
                        color: 'rgb(14, 56, 117)'
                    }
                }
            },
            {
                'id': 'v-5',
                'angle': 0,
                stops: {
                    0: {
                        color: 'rgb(187, 45, 222)'
                    },
                    100: {
                        color: 'rgb(85, 10, 103)'
                    }
                }
            },
            {
                'id': 'v-6',
                'angle': 0,
                stops: {
                    0: {
                        color: 'rgb(114, 192, 202)'
                    },
                    100: {
                        color: 'rgb(9, 218, 243)'
                    }
                }
            },
            {
                'id': 'v-7',
                'angle': 0,
                stops: {
                    0: {
                        color: 'rgb(229, 179, 123)'
                    },
                    100: {
                        color: 'rgb(245, 133, 6)'
                    }
                }
            }],
            axes: [{
                type: 'Numeric',
                position: 'left',
                fields: ['cantidad'],
                minimum: 0,
                //maximum: 1000,
                label: {
                    renderer: Ext.util.Format.numberRenderer('0,0')
                },
                title: 'Cantidad de empleados',
                grid: {
                    odd: {
                        stroke: '#555'
                    },
                    even: {
                        stroke: '#555'
                    }
                }
            }, {
                type: 'Category',
                position: 'bottom',
                fields: ['edad'],
                title: 'Rango de edades'
            }],
            series: [{
                type: 'column',
                axis: 'left',
                highlight: true,
                label: {
                  display: 'insideEnd',
                  'text-anchor': 'middle',
                    field: 'cantidad',
                    orientation: 'horizontal',
                    fill: '#fff',
                    font: '17px Arial'
                },
                renderer: function(sprite, storeItem, barAttr, i, store) {
                    barAttr.fill = colors[i % colors.length];
                    return barAttr;
                },
                style: {
                    opacity: 0.95
                },
                xField: 'edad',
                yField: 'cantidad'
            }]
        });
var chartEdadesRetiro = Ext.create('Ext.chart.Chart', {
            id: 'chartEdadRetiro',
            xtype: 'chart',
            theme: 'Fancy',
            width: 600,
            height: 500,
            minHeight: 400,
            minWidth: 450,
            insetPadding: 30,
            hidden: false,
            maximizable: true,
            animate: {
                easing: 'bounceOut',
                duration: 750
            },
            store: 'estadisticas.EEdadesRetiroStore',
            background: {
                fill: 'rgb(17, 17, 17)'
            },
            gradients: [
            {
                'id': 'v-1',
                'angle': 0,
                stops: {
                    0: {
                        color: 'rgb(212, 40, 40)'
                    },
                    100: {
                        color: 'rgb(117, 14, 14)'
                    }
                }
            },
            {
                'id': 'v-2',
                'angle': 0,
                stops: {
                    0: {
                        color: 'rgb(180, 216, 42)'
                    },
                    100: {
                        color: 'rgb(94, 114, 13)'
                    }
                }
            },
            {
                'id': 'v-3',
                'angle': 0,
                stops: {
                    0: {
                        color: 'rgb(43, 221, 115)'
                    },
                    100: {
                        color: 'rgb(14, 117, 56)'
                    }
                }
            },
            {
                'id': 'v-4',
                'angle': 0,
                stops: {
                    0: {
                        color: 'rgb(45, 117, 226)'
                    },
                    100: {
                        color: 'rgb(14, 56, 117)'
                    }
                }
            },
            {
                'id': 'v-5',
                'angle': 0,
                stops: {
                    0: {
                        color: 'rgb(187, 45, 222)'
                    },
                    100: {
                        color: 'rgb(85, 10, 103)'
                    }
                }
            },
            {
                'id': 'v-6',
                'angle': 0,
                stops: {
                    0: {
                        color: 'rgb(114, 192, 202)'
                    },
                    100: {
                        color: 'rgb(9, 218, 243)'
                    }
                }
            },
            {
                'id': 'v-7',
                'angle': 0,
                stops: {
                    0: {
                        color: 'rgb(229, 179, 123)'
                    },
                    100: {
                        color: 'rgb(245, 133, 6)'
                    }
                }
            }],
            axes: [{
                type: 'Numeric',
                position: 'left',
                fields: ['cantidad'],
                minimum: 0,
                //maximum: 1000,
                label: {
                    renderer: Ext.util.Format.numberRenderer('0,0')
                },
                title: 'Cantidad de empleados',
                grid: {
                    odd: {
                        stroke: '#555'
                    },
                    even: {
                        stroke: '#555'
                    }
                }
            }, {
                type: 'Category',
                position: 'bottom',
                fields: ['edad'],
                title: 'Rango de edades'
            }],
            series: [{
                type: 'column',
                axis: 'left',
                highlight: true,
                label: {
                  display: 'insideEnd',
                  'text-anchor': 'middle',
                    field: 'cantidad',
                    orientation: 'horizontal',
                    fill: '#fff',
                    font: '17px Arial'
                },
                renderer: function(sprite, storeItem, barAttr, i, store) {
                    barAttr.fill = colors[i % colors.length];
                    return barAttr;
                },
                style: {
                    opacity: 0.95
                },
                xField: 'edad',
                yField: 'cantidad'
            }]
        });
Ext.define('RH.view.estadisticas.EEdadesChart',
{
    extend: 'Ext.Panel',
    alias: 'widget.EEdadesChart',
    title: 'Edades',
    layout: {
            type: 'hbox',
            align: 'stretch'
        },
    bodyPadding: 2,
    items: [
        {
            title: 'Cantidad de empleados activos por edades',
            flex: 1,
            tbar: [{
            text: 'Descargar gr&aacute;fico',
            handler: function() {
                Ext.MessageBox.confirm('Confirmar descarga', 'Quiere descargar el gr&aacute;fico como im&aacute;gen?', function(choice){
                    if(choice == 'yes'){
                        chartEdades.save({
                            type: 'image/png'
                        });
                    }
                });
            }
            }],
            items: chartEdades
        },
        {xtype: 'splitter'},
        {
            title: 'Cantidad de empleados retirados por edades',
            flex: 1,
            tbar: [{
            text: 'Descargar gr&aacute;fico',
            handler: function() {
                Ext.MessageBox.confirm('Confirmar descarga', 'Quiere descargar el gr&aacute;fico como im&aacute;gen?', function(choice){
                    if(choice == 'yes'){
                        chartEdadesRetiro.save({
                            type: 'image/png'
                        });
                    }
                });
            }
            }],
            items: chartEdadesRetiro
        },
    ],
    initComponent: function(){
        var stoEdad=Ext.getStore('estadisticas.EEdadesStore');
        stoEdad.load({
                params: {
                        chart: 'edades'
                        
                }
        });
        var stoEdadRetiro=Ext.getStore('estadisticas.EEdadesRetiroStore');
        stoEdadRetiro.load({
                params: {
                        chart: 'edadesRetiro'
                        
                }
        });
        //console.log(stoEdadRetiro);
        //console.log(stoEdad);
        Ext.apply(chartEdades, {store: stoEdad});
        Ext.apply(chartEdadesRetiro, {store: stoEdadRetiro});
        this.callParent(arguments);
    }    
});