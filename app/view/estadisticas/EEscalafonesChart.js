Ext.require('Ext.chart.*');
Ext.require(['Ext.layout.container.Fit', 'Ext.window.MessageBox']);


var donut = false,
    chartEsc = Ext.create('Ext.chart.Chart', {
    xtype: 'chart',
    width: 900,
    height: 600,
    id: 'escalafonesCmp',
    animate: true,
    store: 'estadisticas.EEscalafonesStore',
    shadow: true,
    legend: 
        {
            position: 'right'
        },
    insetPadding: 60,
    theme: 'Base:gradients',
    interactions: ['rotate'],
    series: [{
                type: 'pie',
                field: 'cantidad',
                showInLegend: true,
                donut: donut,
                tips: {
                  trackMouse: true,
                  width: 200,
                  height: 50,
                  renderer: function(storeItem, item) {
                    //Calcula porcentaje.
                    var total = 0;
                    Ext.getStore('estadisticas.EEscalafonesStore').each(function(rec) {
                        total += rec.get('cantidad');
                    });
                    
                    this.setTitle(storeItem.get('escalafon') + ': ' + Math.round(storeItem.get('cantidad') / total * 100 * 100)/100 + '%'+' ('+storeItem.get('cantidad')+' de '+total+')');
                  }
                },
                highlight: {
                  segment: {
                    margin: 20
                  }
                },
                label: {
                    field: 'escalafon',
                    display: 'rotate',
                    contrast: true,
                    
                    font: '12px Arial',
                    color: '#333',
                    //Hago un renderer para mostrar el porcentaje al lado de cada etiqueta en la torta
                    renderer: function(text){
                        var total = 0;
                        var cant;
                        Ext.getStore('estadisticas.EEscalafonesStore').each(function(rec) {
                        total += rec.get('cantidad');
                        if(text == rec.get('escalafon'))
                        {
                            cant = rec.get('cantidad');
                        }
                        });
                        
                        return text+": "+Math.round(cant / total * 100 * 100)/100 + '%';
                    }
                    
                }
                
            }]
    
    });
Ext.define('RH.view.estadisticas.EEscalafonesChart',
{
    extend: 'Ext.Panel',
    alias: 'widget.EEscalafonesChart',
    title: 'Escalafones',
    layout: {
            type: 'hbox',
            align: 'stretch'
        },
    requires: [
        'RH.view.estadisticas.grillaEscalafonesChart'
    ],   
    
    items: [
        {
            
            flex: 1,
            tbar: [{
            text: 'Descargar gr&aacute;fico',
            handler: function() {
                Ext.MessageBox.confirm('Confirmar descarga', 'Quiere descargar el gr&aacute;fico como im&aacute;gen?', function(choice){
                    if(choice == 'yes'){
                        chartEsc.save({
                            type: 'image/png'
                        });
                    }
                });
            }
            }],
            items: chartEsc
        },
        {xtype: 'splitter'},
        {
            xtype: 'grillaEscalafonesChart',
            flex: 0.3
            
        }
    ],    
    initComponent: function(){
        var sto=Ext.getStore('estadisticas.EEscalafonesStore');
        sto.load({
                params: {
                        chart: 'escalafones'
                        
                }
        });

        var stoGrilla = Ext.getStore('estadisticas.EEscalafonesStoreGrilla');
        stoGrilla.load({
                params: {
                        chart: 'escalafones',
                        dest: 'grilla'
                        
                }
        });
        console.log(sto);
        Ext.apply(chartEsc, {store: sto});
        this.callParent(arguments);
    }    
});