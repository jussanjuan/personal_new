Ext.require('Ext.chart.*');

var chartIng = Ext.create('Ext.chart.Chart',{
            xtype: 'chart',
            animate: true,
            store: 'estadisticas.EIngresosStore',
            insetPadding: 30,
            width: 1100,
            height: 500,
            axes: [{
                type: 'Numeric',
                minimum: 0,
                position: 'left',
                fields: ['cantidad'],
                title: "Ingresantes",
                grid: true,
                label: {
                    renderer: Ext.util.Format.numberRenderer('0,0'),
                    font: '10px Arial'
                }
            }, {
                type: 'Category',
                position: 'bottom',
                fields: ['anio'],
                title: "Tiempo",
                label: {
                    font: '11px Arial'
                   
                },
                
                label: {
                    rotate: {
                        degrees: 270
                    }
                }
            }],
            series: [{
                type: 'line',
                axis: 'left',
                xField: 'anio',
                yField: 'cantidad',
                fill: true,
                fillOpacity: 0.5,
                tips: {
                    trackMouse: true,
                    width: 200,
                    height: 55,
                    renderer: function(storeItem, item) {
                        this.setTitle(storeItem.get('cantidad') + ' ingresos a&ntilde;o ' + storeItem.get('anio'));
                    }
                },
                style: {
                    fill: '#38B8BF',
                    stroke: '#38B8BF',
                    'stroke-width': 3
                },
                markerConfig: {
                    type: 'circle',
                    size: 4,
                    radius: 4,
                    'stroke-width': 0,
                    fill: '#38B8BF',
                    stroke: '#38B8BF'
                }
            }]
        });

    
Ext.define('RH.view.estadisticas.EIngresosChart',
{
    extend: 'Ext.Panel',
    alias: 'widget.EIngresosChart',
    title: 'Cantidad de ingresantes a trav&eacute;s de los a&ntilde;os',
    bodyPadding: 2,
    layout: {
            type: 'hbox',
            align: 'stretch'
        },
    requires: [
        'RH.view.estadisticas.grillaIngresosChart'
    ],   
    items: [{
                flex: 1,
                //width: 900,
                tbar: [{
                        iconCls: 'download',
                        tooltip: 'Descargar gr&aacute;fico',
                        handler: function() {
                            Ext.MessageBox.confirm('Confirmar descarga', 'Quiere descargar el gr&aacute;fico como im&aacute;gen?', function(choice){
                                if(choice == 'yes'){
                                    chartIng.save({
                                    type: 'image/png'
                            });
                        }
                    });
                }
            }],
                title: 'Gr&aacute;fico',
                items: chartIng
            },{xtype: 'splitter'},
        {
            xtype: 'grillaIngresosChart',
            flex: 0.3
            
        },],
    initComponent: function(){
        var me = this;
        var array_items=[];
        //console.log(Ext.getStore('estadisticas.EIngresosStore').data);
        var storeIng=Ext.getStore('estadisticas.EIngresosStore');
        storeIng.load({
                params: {
                        chart: 'ingresos'
                        
                }
        });
        var stoGrilla = Ext.getStore('estadisticas.EIngresosStoreGrilla');
        stoGrilla.load({
                params: {
                        chart: 'ingresos',
                        dest: 'grilla'
                        
                }
        });
        Ext.apply(chartIng, {store: storeIng});
        this.callParent(arguments);
    }    
});