Ext.require('Ext.chart.*');

var chartInt = Ext.create('Ext.chart.Chart', {
            id: 'usoCmp',
            width: 900,
            height: 520,
            xtype: 'chart',
            style: 'background:#fff',
            animate: true,
            shadow: true,
            store: 'estadisticas.EInteraccionesStore',
            axes: [{
                type: 'Numeric',
                position: 'left',
                fields: ['cantidad'],
                label: {
                    renderer: Ext.util.Format.numberRenderer('0,0')
                },
                title: 'Numero de interacciones',
                grid: true,
                minimum: 0
            }, {
                type: 'Category',
                position: 'bottom',
                fields: ['nombre'],
                title: 'Usuario',
                label: {
                    rotate: {
                        degrees: 270
                    }
                    
                }
            }],
            series: [{
                type: 'column',
                axis: 'left',
                highlight: true,
                tips: {
                  trackMouse: true,
                  width: 200,
                  height: 50,
                  renderer: function(storeItem, item) {
                      //Calcula porcentaje.
                        var total = 0;
                        Ext.getStore('estadisticas.EInteraccionesStore').each(function(rec) {
                        total += rec.get('cantidad');
                    });
                    this.setTitle(storeItem.get('nombre') + ': ' + storeItem.get('cantidad') + ' interacciones ('+ Math.round(storeItem.get('cantidad') / total * 100)+'%)');
                  }
                },
                label: {
                  display: 'insideEnd',
                  'text-anchor': 'middle',
                    field: 'cantidad',
                    //Hago un renderer para mostrar el porcentaje al lado de cada etiqueta en las barras
                    renderer: function(text){
                        var total = 0;
                        
                        Ext.getStore('estadisticas.EInteraccionesStore').each(function(rec) {
                        total += rec.get('cantidad');
                        
                        });
                                                
                        return text+" ("+Math.round(text / total * 100) + '%)';
                    },
                    //renderer: Ext.util.Format.numberRenderer('0'),
                    orientation: 'horizontal',
                    color: '#333'
                },
                xField: 'nombre',
                yField: 'cantidad'
            }]
        });
Ext.define('RH.view.estadisticas.EInteraccionesChart',
{
    extend: 'Ext.Panel',
    alias: 'widget.EInteraccionesChart',
    title: 'Interacciones con el sistema',
    layout: {
            type: 'hbox',
            align: 'stretch'
        },
    requires: [
        'RH.view.estadisticas.grillaInteraccionesChart'
    ], 
    
    items: [
        {
            
            flex: 1,
            tbar: [{
            text: 'Descargar gr&aacute;fico',
            handler: function() {
                Ext.MessageBox.confirm('Confirmar descarga', 'Quiere descargar el gr&aacute;fico como im&aacute;gen?', function(choice){
                    if(choice == 'yes'){
                        chartInt.save({
                            type: 'image/png'
                        });
                    }
                });
            }
            }],
            items: chartInt
        },
        {xtype: 'splitter'},
        {
            xtype: 'grillaInteraccionesChart',
            flex: 0.3
            
        },
    ], 
    initComponent: function(){
        var sto=Ext.getStore('estadisticas.EInteraccionesStore');
        sto.load({
                params: {
                        chart: 'interacciones'
                        
                }
        });
        var stoGrilla = Ext.getStore('estadisticas.EInteraccionesStoreGrilla');
        stoGrilla.load({
                params: {
                        chart: 'interacciones',
                        dest: 'grilla'
                        
                }
        });
        console.log(sto);
        Ext.apply(chartInt, {store: sto});
        this.callParent(arguments);
    }    
});