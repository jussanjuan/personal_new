Ext.require('Ext.chart.*');
Ext.require(['Ext.layout.container.Fit', 'Ext.window.MessageBox']);


var donut = false,
    chartTit = Ext.create('Ext.chart.Chart', {
    xtype: 'chart',
    width: 900,
    height: 600,
    id: 'titulosCmp',
    animate: true,
    store: 'estadisticas.ETitulosStore',
    shadow: true,
    legend: 
        {
            position: 'right'
        },
    insetPadding: 60,
    theme: 'Base:gradients',
    interactions: ['rotate'],
    series: [{
                type: 'pie',
                field: 'cantidad',
                showInLegend: true,
                donut: donut,
                tips: {
                  trackMouse: true,
                  width: 200,
                  height: 50,
                  renderer: function(storeItem, item) {
                    //Calcula porcentaje.
                    var total = 0;
                    Ext.getStore('estadisticas.ETitulosStore').each(function(rec) {
                        total += rec.get('cantidad');
                    });
                    
                    this.setTitle(storeItem.get('titulo') + ': ' + Math.round(storeItem.get('cantidad') / total * 100 * 100)/100 + '%'+' ('+storeItem.get('cantidad')+' de '+total+')');
                  }
                },
                highlight: {
                  segment: {
                    margin: 20
                  }
                },
                label: {
                    field: 'titulo',
                    display: 'rotate',
                    contrast: true,
                    
                    font: '15px Arial',
                    //Hago un renderer para mostrar el porcentaje al lado de cada etiqueta en la torta
                    renderer: function(text){
                        var total = 0;
                        var cant;
                        Ext.getStore('estadisticas.ETitulosStore').each(function(rec) {
                        total += rec.get('cantidad');
                        if(text == rec.get('titulo'))
                        {
                            cant = rec.get('cantidad');
                        }
                        });
                        
                        return text+": "+Math.round(cant / total * 100 * 100)/100 + '%';
                    }
                    
                }
                
            }]
    
    });
Ext.define('RH.view.estadisticas.ETitulosChart',
{
    extend: 'Ext.Panel',
    alias: 'widget.ETitulosChart',
    title: 'Titulos predominantes',
    layout: {
            type: 'hbox',
            align: 'stretch'
        },
    requires: [
        'RH.view.estadisticas.grillaTitulosChart'
    ],   
    
    items: [
        {
            
            flex: 1,
            tbar: [{
            text: 'Descargar gr&aacute;fico',
            handler: function() {
                Ext.MessageBox.confirm('Confirmar descarga', 'Quiere descargar el gr&aacute;fico como im&aacute;gen?', function(choice){
                    if(choice == 'yes'){
                        chartTit.save({
                            type: 'image/png'
                        });
                    }
                });
            }
            }],
            items: chartTit
        },
        {xtype: 'splitter'},
        {
            xtype: 'grillaTitulosChart',
            flex: 0.3
            
        },
    ],    
    initComponent: function(){
        var sto=Ext.getStore('estadisticas.ETitulosStore');
        sto.load({
                params: {
                        chart: 'titulos'
                        
                }
        });

        var stoGrilla = Ext.getStore('estadisticas.ETitulosStoreGrilla');
        stoGrilla.load({
                params: {
                        chart: 'titulos',
                        dest: 'grilla'
                        
                }
        });
        console.log(sto);
        Ext.apply(chartTit, {store: sto});
        this.callParent(arguments);
    }    
});