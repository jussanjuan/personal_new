Ext.define('personal.view.estadisticas.EstadisticasPanel', 
{
    id: 'estadisticasPnl',
    title: 'Estad&iacute;sticas',
    extend: 'Ext.Panel',
    alias: 'widget.Estadisticas',
    flex: 1,
    layout: {
        type: 'hbox',       // Arrange child items vertically
        align: 'stretch',    // Each takes up full width
        padding: 0,
        border: 0
    },
	xtype: 'EstadisticasPanel',
    id: 'estadisticasPnl',
	requires: [
        //'Ext.chart.series.Series',
        /*'personal.view.estadisticas.licencias.ELicAdopcionChart',
        'personal.view.estadisticas.licencias.ELicCompensatoriasChart',
        'personal.view.estadisticas.licencias.ELicCulturalesChart',
        'personal.view.estadisticas.licencias.ELicDeportivasChart',
        'personal.view.estadisticas.licencias.ELicDobleTurnoChart',
        'personal.view.estadisticas.licencias.ELicElectivasChart',
        'personal.view.estadisticas.licencias.ELicEstudioChart',
        'personal.view.estadisticas.licencias.ELicExtraordinariaChart',
        'personal.view.estadisticas.licencias.ELicFallecimientoChart',
        'personal.view.estadisticas.licencias.ELicGremialesChart',
        'personal.view.estadisticas.licencias.ELicMaternidadChart',
        'personal.view.estadisticas.licencias.ELicMatrimonioChart',
        'personal.view.estadisticas.licencias.ELicNacimientoChart',
        'personal.view.estadisticas.licencias.ELicParticularesChart',
        'personal.view.estadisticas.licencias.ELicReligionChart',
        'personal.view.estadisticas.licencias.ELicSaludAtChart',*/
        //'personal.view.estadisticas.licencias.ELicSaludChart',
        /*'personal.view.estadisticas.licencias.ELicSaludFamChart',
        'personal.view.estadisticas.licencias.ELicSaludLtChart',
        'personal.view.estadisticas.licencias.ELicSingoceChart',
        'personal.view.estadisticas.EAusentismoChart',
        'personal.view.estadisticas.ECargosChart',
        'personal.view.estadisticas.EEdadesChart',
        'personal.view.estadisticas.EEscalafonesChart',
        'personal.view.estadisticas.EInteraccionesChart',
        'personal.view.estadisticas.ETitulosChart',
        'personal.view.estadisticas.grillaCargosChart',
        'personal.view.estadisticas.grillaEscalafonesChart',
        'personal.view.estadisticas.grillaIngresosChart',
        'personal.view.estadisticas.grillaInteraccionesChart',
        'personal.view.estadisticas.grillaTitulosChart'*/

    ],
    reference: 'EstadisticasPanel',
    tbar: [],
    items:[
    {
        xtype: 'treepanel',
        id: 'treeCharts',
        border: false,
        useArrows: true,
        rootVisible: false,
        activeItem: 0,
        width: 180,
        flex: 1,
        root: 
        {
            expanded: true,
            children: [{
                text: 'Gr&aacute;ficos', expanded: true, children:
                [
                    { text: 'Licencias', expanded: true, children:
                    [
                        /*{ id: 'licSaludGraph', text: 'Salud personal', leaf: true, indexLeaf: 2},
                        { id: 'licSaludFamGraph', text: 'Salud familiar', leaf: true, indexLeaf: 3 },
                        { id: 'licSaludLtGraph', text: 'Salud largo tratamiento', leaf: true, indexLeaf: 4 },
                        { id: 'licSaludAtGraph', text: 'Salud accidente de trabajo', leaf: true, indexLeaf: 5 },
                        { id: 'licMatGraph', text: 'Maternidad', leaf: true, indexLeaf: 6 },
                        { id: 'licNacGraph', text: 'Nacimiento', leaf: true, indexLeaf: 7 },
                        { id: 'licFallGraph', text: 'Fallecimiento', leaf: true, indexLeaf: 8 },
                        { id: 'licPartGraph', text: 'Particulares', leaf: true, indexLeaf: 9 },
                        { id: 'licSingoGraph', text: 'Sin goce', leaf: true, indexLeaf: 10 },
                        { id: 'licCompGraph', text: 'Compensatorias', leaf: true, indexLeaf: 11 },
                        { id: 'licCultGraph', text: 'Culturales', leaf: true, indexLeaf: 12 },
                        { id: 'licEstGraph', text: 'Estudio', leaf: true, indexLeaf: 13 },
                        { id: 'licGremGraph', text: 'Gremiales', leaf: true, indexLeaf: 14 },
                        { id: 'licElectGraph', text: 'Electivas', leaf: true, indexLeaf: 15 },
                        { id: 'licMatriGraph', text: 'Matrimonio', leaf: true, indexLeaf: 16 },
                        { id: 'licDeporGraph', text: 'Deportivas', leaf: true, indexLeaf: 17 },
                        { id: 'licExtGraph', text: 'Extraordinarias', leaf: true, indexLeaf: 18 },
                        { id: 'licDTGraph', text: 'Doble turno', leaf: true, indexLeaf: 19 },
                        { id: 'licRegGraph', text: 'D&iacute;a religioso', leaf: true, indexLeaf: 20 },
                        { id: 'licAdopGraph', text: 'Adopci&oacute;n', leaf: true, indexLeaf: 21 }*/
                    ]},
                /*{ id: 'titGraph', text: 'T&iacute;tulos', leaf: true, indexLeaf: 22 },
                { id: 'edaGraph', text: 'Edades', leaf: true, indexLeaf: 23 },
                { id: 'intGraph', text: 'Interacciones', leaf: true, indexLeaf: 24},
                { id: 'ingGraph', text: 'Ingresantes', leaf: true, indexLeaf: 25 },
                { id: 'carGraph', text: 'Cargos', leaf: true, indexLeaf: 26 },
                { id: 'escGraph', text: 'Escalafones', leaf: true, indexLeaf: 27 }*/
            ]}]
        }
    }, 
    {
        xtype: 'splitter'
        
    }, 
    { 
        xtype: 'panel',
        id: 'pnlChartsCard',
        layout: 'card',
        flex: 4,
        border: false,


        items:[{
                id: 'cardEstadZero',
                html: '<h3>Seleccione un gr&aacute;fico.</h3>'
                
        }, {
                id: 'cardEstadZeroSalud',
                html: '<h3>Seleccione un item de licencia.</h3>'
                
        }/*, {
                id: 'licSaludChart',
                xtype: 'ELicSaludChart'
                
        }, {
                id: 'licSaludFamChart',
                xtype: 'ELicSaludFamChart'
                
        }, {
                id: 'licSaludLtChart',
                xtype: 'ELicSaludLtChart'
                
        }, {
                id: 'licSaludAtChart',
                xtype: 'ELicSaludAtChart'
                
        }, {
                id: 'licMaternidadChart',
                xtype: 'ELicMaternidadChart'
                
        },{
                id: 'licNacimientoChart',
                xtype: 'ELicNacimientoChart'
               
        },{
                id: 'licFallecimientoChart',
                xtype: 'ELicFallecimientoChart'
                
        },{
                id: 'licParticularesChart',
                xtype: 'ELicParticularesChart'
                
        },{
                id: 'licSingoceChart',
                xtype: 'ELicSingoceChart'
                
        },{
                id: 'licCompensatoriasChart',
                xtype: 'ELicCompensatoriasChart'
                
        },{
                id: 'licCulturalesChart',
                xtype: 'ELicCulturalesChart'
                
        },{
                id: 'licEstudioChart',
                xtype: 'ELicEstudioChart'
                
        },{
                id: 'licGremialesChart',
                xtype: 'ELicGremialesChart'
                
        },{
                id: 'licElectivasChart',
                xtype: 'ELicElectivasChart'
                
        },{
                id: 'licMatrimonioChart',
                xtype: 'ELicMatrimonioChart'
                
        },{
                id: 'licDeportivasChart',
                xtype: 'ELicDeportivasChart'
                
        },{
                id: 'licExtraordinariaChart',
                xtype: 'ELicExtraordinariaChart'
                
        },{
                id: 'licDobleTurnoChart',
                xtype: 'ELicDobleTurnoChart'
                
        },{
                id: 'licReligionChart',
                xtype: 'ELicReligionChart'
                
        },{
                id: 'licAdopcionChart',
                xtype: 'ELicAdopcionChart'
                
        },{
                id: 'titChart',
                xtype: 'ETitulosChart'
                
        },{
                id: 'edadesChart',
                xtype: 'EEdadesChart'
                
        },{
                id: 'interactChart',
                xtype: 'EInteraccionesChart'
                
        }, {
                id: 'ingresosChart',
                xtype: 'EIngresosChart'
                
        }, {
                id: 'cargosChart',
                xtype: 'ECargosChart'
                
        }, {
                id: 'escalafonesChart',
                xtype: 'EEscalafonesChart'
                
        }*/] 

    }]
});
