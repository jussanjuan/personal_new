Ext.define('RH.view.estadisticas.grillaCargosChart', {
	extend: 'Ext.grid.Panel',
	alias: 'widget.grillaCargosChart',
    store: 'estadisticas.ECargosStoreGrilla',
	id: 'idgrillaCargosChart',
    frame: true,
    layout: 'fit',
    features : [ {
       ftype : 'summary'
    }],
    columns: [
            {header: 'Cargo', dataIndex: 'cargo', flex: 3, summaryType: 'count', summaryRenderer: function(value, summaryData, dataIndex) {
                    return value+' cargos';
                }},
            {header: 'Cant', dataIndex: 'cantidad', flex: 1, summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
                return value;
            }},
            {header: '%', dataIndex: 'porc', flex: 1, 
                renderer: function(value, metaData, record, rowIndex, colIndex, store)
                {
                    return value+'%';
                },
                summaryType: 'sum',
                summaryRenderer: function(value, summaryData, dataIndex) {
                    return Math.round(value)+'%';
                }
            }
            ],
    initComponent: function() {
       this.callParent(arguments);
	}
});