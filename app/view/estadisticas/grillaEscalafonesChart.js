Ext.define('RH.view.estadisticas.grillaEscalafonesChart', {
	extend: 'Ext.grid.Panel',
	alias: 'widget.grillaEscalafonesChart',
    store: 'estadisticas.EEscalafonesStoreGrilla',
	id: 'idgrillaEscalafonesChart',
    frame: true,
    layout: 'fit',
    features : [ {
       ftype : 'summary'
    }],
    columns: [
            {header: 'Escalafon', dataIndex: 'escalafon', flex: 3, summaryType: 'count', summaryRenderer: function(value, summaryData, dataIndex) {
                    return value+' escalafones';
                }},
            {header: 'Cant', dataIndex: 'cantidad', flex: 1, summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
                return value;
            }},
            {header: '%', dataIndex: 'porc', flex: 1, 
                renderer: function(value, metaData, record, rowIndex, colIndex, store)
                {
                    return value+'%';
                },
                summaryType: 'sum',
                summaryRenderer: function(value, summaryData, dataIndex) {
                    return Math.round(value)+'%';
                }
            }
            ],
    initComponent: function() {
       this.callParent(arguments);
	}
});