Ext.define('RH.view.estadisticas.grillaIngresosChart', {
	extend: 'Ext.grid.Panel',
	alias: 'widget.grillaIngresosChart',
    store: 'estadisticas.EIngresosStoreGrilla',
	id: 'idgrillaIngresosChart',
    frame: true,
    layout: 'fit',
    columns: [
        {header: 'A&ntilde;o', dataIndex: 'anio', flex: 3},
        {header: 'Cant', dataIndex: 'cantidad', flex: 1}
    ],
    initComponent: function() {
       this.callParent(arguments);
	}
});