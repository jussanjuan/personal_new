Ext.define('RH.view.estadisticas.grillaInteraccionesChart', {
	extend: 'Ext.grid.Panel',
	alias: 'widget.grillaInteraccionesChart',
    store: 'estadisticas.EInteraccionesStoreGrilla',
	id: 'idgrillaInteraccionesChart',
    frame: true,
    layout: 'fit',
    features : [ {
       ftype : 'summary'
    }],
    columns: [
            {header: 'Apellido', dataIndex: 'apellido', flex: 3, summaryType: 'count', summaryRenderer: function(value, summaryData, dataIndex) {
                    return value+' usuarios';
                }},
            {header: 'Nombre', dataIndex: 'nombre', flex: 3, summaryType: 'count', summaryRenderer: function(value, summaryData, dataIndex) {
                    return value+' usuarios';
                }},    
            {header: 'Cant', dataIndex: 'cantidad', flex: 1, summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
                return value;
            }},
            {header: '%', dataIndex: 'porc', flex: 1, 
                renderer: function(value, metaData, record, rowIndex, colIndex, store)
                {
                    return value+'%';
                },
                summaryType: 'sum',
                summaryRenderer: function(value, summaryData, dataIndex) {
                    return Math.round(value)+'%';
                }
            }
            ],
    initComponent: function() {
       this.callParent(arguments);
	}
});