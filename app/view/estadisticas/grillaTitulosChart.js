Ext.define('RH.view.estadisticas.grillaTitulosChart', {
	extend: 'Ext.grid.Panel',
	alias: 'widget.grillaTitulosChart',
    store: 'estadisticas.ETitulosStoreGrilla',
	id: 'idgrillaTitulosChart',
    frame: true,
    layout: 'fit',
    features : [ {
       ftype : 'summary'
    }],
    columns: [
            {header: 'Titulo', dataIndex: 'titulo', flex: 3, summaryType: 'count', summaryRenderer: function(value, summaryData, dataIndex) {
                    return value+' titulos';
                }},
            {header: 'Cant', dataIndex: 'cantidad', flex: 1, summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
                return value;
            }},
            {header: '%', dataIndex: 'porc', flex: 1, 
                renderer: function(value, metaData, record, rowIndex, colIndex, store)
                {
                    return value+'%';
                },
                summaryType: 'sum',
                summaryRenderer: function(value, summaryData, dataIndex) {
                    return Math.round(value)+'%';
                }
            }
            ],
    initComponent: function() {
       this.callParent(arguments);
	}
});