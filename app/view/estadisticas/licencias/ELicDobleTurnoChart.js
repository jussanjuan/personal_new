Ext.require('Ext.chart.*');

var chartLicDobleTurno = Ext.create('Ext.chart.Chart',{
            xtype: 'chart',
            animate: true,
            store: 'estadisticas.licencias.ELicenciasStore',
            insetPadding: 30,
            width: 500,
            height: 500,
            axes: [{
                type: 'Numeric',
                minimum: 0,
                position: 'left',
                fields: ['porc'],
                title: "% de licencias",
                grid: true,
                label: {
                    renderer: Ext.util.Format.numberRenderer('0,0'),
                    font: '10px Arial'
                }
            }, {
                type: 'Category',
                position: 'bottom',
                fields: ['mes'],
                title: "Mes",
                label: {
                    font: '11px Arial'
                    
                },
                label: {
                    rotate: {
                        degrees: 270
                    }
                }
            }],
            series: [{
                type: 'line',
                axis: 'left',
                xField: 'mes',
                yField: 'porc',
                fill: true,
                fillOpacity: 0.5,
                tips: {
                    trackMouse: true,
                    width: 200,
                    height: 55,
                    renderer: function(storeItem, item) {
                        this.setTitle(storeItem.get('porc') + '% de licencias en ' + storeItem.get('mes')+'. Total agentes: '+ storeItem.get('planta_mes'));
                    }
                },
                style: {
                    fill: '#38B8BF',
                    stroke: '#38B8BF',
                    'stroke-width': 3
                },
                markerConfig: {
                    type: 'circle',
                    size: 4,
                    radius: 4,
                    'stroke-width': 0,
                    fill: '#38B8BF',
                    stroke: '#38B8BF'
                }
            }]
        });
var chartLicDobleTurnoHist = Ext.create('Ext.chart.Chart',{
            xtype: 'chart',
            animate: true,
            store:[],
            insetPadding: 30,
            width: 500,
            height: 500,
            legend: {
                position: 'right'
            },
            axes: [{
                type: 'Numeric',
                minimum: 0,
                position: 'left',
                fields: [],
                title: "Cantidad de licencias",
                grid: true,
                label: {
                    renderer: Ext.util.Format.numberRenderer('0,0'),
                    font: '10px Arial'
                }
            }, {
                type: 'Category',
                position: 'bottom',
                fields: ['mes'],
                title: "Mes",
                label: {
                    font: '11px Arial'
                    
                },
                label: {
                    rotate: {
                        degrees: 270
                    }
                }
            }],
            series: []
        });
    
Ext.define('RH.view.estadisticas.licencias.ELicDobleTurnoChart',
{
    extend: 'Ext.Panel',
    alias: 'widget.ELicDobleTurnoChart',
    
    bodyPadding: 2,
    layout: {
            type: 'hbox',
            align: 'stretch'
        },
    items:[
        {
        title: 'Cantidad de licencias por doble turno del a&ntilde;o actual por mes',
        flex: 1,
        tbar: [{
            text: 'Descargar gr&aacute;fico',
            handler: function() {
                Ext.MessageBox.confirm('Confirmar descarga', 'Quiere descargar el gr&aacute;fico como im&aacute;gen?', function(choice){
                    if(choice == 'yes'){
                        chartLicDobleTurno.save({
                            type: 'image/png'
                        });
                    }
                });
            }
        }],
        items: chartLicDobleTurno
        },
        {xtype: 'splitter'},
        {
            title: 'Cantidad de licencias por doble turno hist&oacute;rico',
            flex: 1,
            tbar: [{
            text: 'Descargar gr&aacute;fico',
            handler: function() {
                Ext.MessageBox.confirm('Confirmar descarga', 'Quiere descargar el gr&aacute;fico como im&aacute;gen?', function(choice){
                    if(choice == 'yes'){
                        chartLicDobleTurnoHist.save({
                            type: 'image/png'
                        });
                    }
                });
            }
            }],
            items: chartLicDobleTurnoHist
        },
    ],
    
    initComponent: function(){
        
        var sto=Ext.getStore('estadisticas.licencias.ELicenciasStore');
        sto.load({
                params: {
                        chart: 18
                        
                }
        });
        //console.log(sto);
        Ext.apply(chartLicDobleTurno, {store: sto});
        
        this.callParent(arguments);
    }    
});