Ext.define('personal.view.historial.HCompensatoria', {
	extend: 'Ext.grid.Panel',
	xtype: 'HCompensatoria',
	reference: 'HCompensatoria',
	title: 'Compensatoria',
	border: false,
	layout: 'fit',

	requires: [
		'personal.view.historial.HCompensatoriaDias',
		'personal.view.historial.HCompensatoriaDiasFeria'
	],

	items: [{
		xtype: 'HCompensatoriaDias'
	}, {
		id: 'hCompensatoriaDiasFeria',
		xtype: 'HCompensatoriaDiasFeria'
	}],

	/*extend: 'Ext.grid.Panel',
	alias: 'widget.HCompensatoria',
	store: 'historial.HCompensatoriaStore',
	title: 'Licencias compensatorias',
    border: false,
    flex: 1,
	
    tbar: [{
    	text: 'Año'
    }],

	columns: [
		{header: 'Desde', dataIndex: 'desde', renderer: Ext.util.Format.dateRenderer('d-m-Y'), flex: 1 },
		{header: 'Hasta', dataIndex: 'hasta', renderer: Ext.util.Format.dateRenderer('d-m-Y'), flex: 1 },
		{header: 'Total', dataIndex: 'otorgados' }
	],*/

	initComponent: function() {

		this.callParent(arguments);
	}
});