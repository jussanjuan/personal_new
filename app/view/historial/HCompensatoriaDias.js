Ext.define('personal.view.historial.HCompensatoriaDias', {
	extend: 'Ext.grid.Panel',
	xtype: 'HCompensatoriaDias',
	requires: [
        'personal.store.historial.HLicenciaStore'
    ],
	store: {
        type: 'HLicenciaStore'
    },
	title: 'Dias pedidos',
    border: false,
    flex: 1,
	
    tbar: [],

	columns: [
		{header: 'Desde', dataIndex: 'desde', renderer: Ext.util.Format.dateRenderer('d-m-Y'), flex: 1},
		{header: 'Hasta', dataIndex: 'hasta', renderer: Ext.util.Format.dateRenderer('d-m-Y'), flex: 1},
		{header: 'Total', dataIndex: 'dias_licencia'},
        {header: 'Desiste', dataIndex: 'goce', width: 50}
	],

	initComponent: function() {

		this.callParent(arguments);
	}
});