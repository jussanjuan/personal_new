Ext.define('personal.view.historial.HCompensatoriaDiasFeria', {
	extend: 'Ext.grid.Panel',
	xtype: 'HCompensatoriaDiasFeria',
	requires: [
        'personal.store.historial.HCompensatoriaStoreDiasFeria'
    ],
	store: {
        type: 'HCompensatoriaStoreDiasFeria'
    },
	store: 'historial.HCompensatoriaStoreDiasFeria',
	title: 'Dias trabajados',
    border: false,
    flex: 1,
	
    tbar: [/*{
            //text: 'Imprimir',
            iconCls: 'print2',
            scale: 'medium',
            tooltip: 'Imprimir grilla',
	    	handler: function(btn) {
               Ext.ux.grid.Printer.print(this.up().up());
               //console.log(this.up().up());
            }
	},*/
	{
		id: 'btnVaciarDias',
        iconCls: 'cancel1',
        scale: 'medium',
        tooltip: 'Vaciar d&iacute;as trabajados',
    	action: 'vaciar_dias_trabajados',
    	disabled: true,
	}],

	columns: [
		{header: 'Desde', dataIndex: 'desde', renderer: Ext.util.Format.dateRenderer('d-m-Y'), flex: 1 },
		{header: 'Hasta', dataIndex: 'hasta', renderer: Ext.util.Format.dateRenderer('d-m-Y'), flex: 1 },
		//{header: 'Año', dataIndex: 'anio'},
		{header: 'Total', dataIndex: 'totaldias' },
        {header: 'Disponibles', dataIndex: 'disponibles'},
        {header: 'Autorizada', dataIndex: 'goce'}
	],

	initComponent: function() {

		this.callParent(arguments);
	}
});