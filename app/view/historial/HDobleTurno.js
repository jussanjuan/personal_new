Ext.define('personal.view.historial.HDobleTurno', {
	extend: 'Ext.tab.Panel',
	xtype: 'HDobleTurno',
	reference: 'HDobleTurno',
	title: 'Doble turno',
	border: false,
	layout: 'fit',

	requires: [
		'personal.view.historial.HDobleTurnoDias',
		'personal.view.historial.HDobleTurnoDiasDT'
	],

	items: [{
		xtype: 'HDobleTurnoDias'
	}, {
		xtype: 'HDobleTurnoDiasDT'
	}],

	initComponent: function() {

		this.callParent(arguments);
	}
});