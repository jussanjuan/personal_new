Ext.define('personal.view.historial.HDobleTurnoDias', {
	extend: 'Ext.grid.Panel',
	xtype: 'HDobleTurnoDias',
	reference: 'HDobleTurnoDias',
	requires: [
        'personal.store.historial.HLicenciaStore'
    ],
	store: {
        type: 'HLicenciaStore'
    },
	title: 'Dias pedidos',
    border: false,
    flex: 1,
	
    tbar: [],

	columns: [
		{text: 'Desde', dataIndex: 'desde', renderer: Ext.util.Format.dateRenderer('d-m-Y'), flex: 1 },
		{text: 'Hasta', dataIndex: 'hasta', renderer: Ext.util.Format.dateRenderer('d-m-Y'), flex: 1 },
		{text: 'Total', dataIndex: 'dias_licencia' },
        {text: 'Desiste', dataIndex: 'goce', width: 50}
	],

	initComponent: function() {

		this.callParent(arguments);
	}
});