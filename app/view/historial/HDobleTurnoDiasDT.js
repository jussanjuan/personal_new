Ext.define('personal.view.historial.HDobleTurnoDiasDT', {
	extend: 'Ext.grid.Panel',
	xtype: 'HDobleTurnoDiasDT',
	reference: 'HDobleTurnoDiasDT',
	requires: [
        'personal.store.historial.HDobleTurnoStoreDiasDT'
    ],
	store: {
        type: 'HDobleTurnoStoreDiasDT'
    },
	title: 'Dias trabajados',
    border: false,
    flex: 1,
	
    tbar: [],

	columns: [
		{text: 'Desde', dataIndex: 'desde', renderer: Ext.util.Format.dateRenderer('d-m-Y'), flex: 1 },
		{text: 'Hasta', dataIndex: 'hasta', renderer: Ext.util.Format.dateRenderer('d-m-Y'), flex: 1 },
		//{header: 'Año', dataIndex: 'anio'},
		{text: 'Total', dataIndex: 'dias_licencia' },
        {text: 'Disponibles', dataIndex: 'disponibles'},
        {text: 'Autorizada', dataIndex: 'goce'}
	],

	initComponent: function() {

		this.callParent(arguments);
	}
});