Ext.define('personal.view.historial.HElectivas', {
	extend: 'Ext.grid.Grid',
	alias: 'widget.HElectivas',
	store: 'historial.HElectivasStore',
	title: 'Licencias Electivas',
    border: false,
    flex: 1,
    tbar: [/*{
            //text: 'Imprimir',
            iconCls: 'print2',
            scale: 'medium',
            tooltip: 'Imprimir grilla',
	    handler: function(btn) {
               Ext.ux.grid.Printer.print(this.up().up());
               //console.log(this.up().up());
            }
	}*/],
	columns: [
		{header: 'Desde', dataIndex: 'desde',  renderer: Ext.util.Format.dateRenderer('d-m-Y'), width: 70},
		{header: 'Hasta', dataIndex: 'hasta',  renderer: Ext.util.Format.dateRenderer('d-m-Y'), width: 70},
		{header: 'Otorg.', dataIndex: 'diasotorgad', width: 45},
		{header: 'Cargo', dataIndex: 'cargo', flex: 1},
                {header: 'Desiste', dataIndex: 'goce', flex: 1}
	],

	initComponent: function() {

		this.callParent(arguments);
	}
});