Ext.define('personal.view.historial.HEstudio', {
	extend: 'Ext.grid.Panel',
	xtype: 'HEstudio',
	reference: 'HEstudio',
	requires: [
        'personal.store.historial.HLicenciaStore'
    ],
    store: {
        type: 'HLicenciaStore'
    },
	
	title: 'Estudio',
    border: false,
    flex: 1,
    tbar: [],
	columns: [
		{header: 'Desde', dataIndex: 'desde',  renderer: Ext.util.Format.dateRenderer('d-m-Y'), width: 70},
		{header: 'Hasta', dataIndex: 'hasta',  renderer: Ext.util.Format.dateRenderer('d-m-Y'), width: 70},
        {header: 'Otorg.', dataIndex: 'dias_licencia', width: 70},
		{header: 'Examen', dataIndex: 'fecha_examen', renderer: Ext.util.Format.dateRenderer('d-m-Y'), width: 70},
		{header: 'Carrera', dataIndex: 'carrera', width: 70},
		{header: 'Materia', dataIndex: 'asignatura', flex: 1},
        {header: 'Desiste', dataIndex: 'goce', width: 45},
        {header: 'Notif. mail', dataIndex: 'notificado', flex: 1}
	],

	initComponent: function() {

		this.callParent(arguments);
	}
});