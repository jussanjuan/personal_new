Ext.define('personal.view.historial.HExtraordinarias', {
	extend: 'Ext.grid.Grid',
	alias: 'widget.HExtraordinarias',
	store: 'historial.HExtraordinariasStore',
	title: 'Licencias Extraordinarias',
    border: false,
    flex: 1,
    tbar: [/*{
            //text: 'Imprimir',
            iconCls: 'print2',
            scale: 'medium',
            tooltip: 'Imprimir grilla',
	    handler: function(btn) {
               Ext.ux.grid.Printer.print(this.up().up());
               //console.log(this.up().up());
            }
	}*/],
	columns: [
		{header: 'Desde', dataIndex: 'desde',  renderer: Ext.util.Format.dateRenderer('d-m-Y'), width: 70},
		{header: 'Hasta', dataIndex: 'hasta',  renderer: Ext.util.Format.dateRenderer('d-m-Y'), width: 70},
		{header: 'Otorg.', dataIndex: 'diasotorgad', width: 45},
                {header: 'Observaciones', dataIndex: 'observaciones', width: 45},
		{header: 'Desiste', dataIndex: 'goce', width: 45}
		
	],

	initComponent: function() {

		this.callParent(arguments);
	}
});