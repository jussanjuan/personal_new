Ext.define('personal.view.historial.HLicencia', {
	extend: 'Ext.grid.Panel',
	xtype: 'HLicencia',
	reference: 'HLicencia',
	id: 'idPanelHLicencia',
	requires: [
        'personal.store.historial.HLicenciaStore'
    ],
	store: {
        type: 'HLicenciaStore'
    },
	title: 'Licencias',
    border: false,
    flex: 1,
    tbar: [],
	columns: [
		{text: 'Desde', dataIndex: 'desde',  renderer: Ext.util.Format.dateRenderer('d-m-Y'), flex: 3},
		{text: 'Hasta', dataIndex: 'hasta',  renderer: Ext.util.Format.dateRenderer('d-m-Y'), flex: 3},
		{text: 'D&iacute;as', dataIndex: 'dias_licencia', flex: 1},
        {text: 'Desiste', dataIndex: 'goce', flex: 1},
		{text: 'Causa', dataIndex: 'causa', flex: 7},
		{text: 'Obs.', dataIndex: 'observaciones', flex: 7},
		{text: 'Pierde present.', dataIndex: 'pierde_presentismo', flex: 2},
		{text: 'Exp.', dataIndex: 'expediente', flex: 2},
		{text: 'Notif. mail', dataIndex: 'notificado', flex: 1}
                
	],

	initComponent: function() {

		this.callParent(arguments);
	}
});