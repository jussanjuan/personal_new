Ext.define('personal.view.historial.HMaternidad', {
	extend: 'Ext.grid.Panel',
	xtype: 'HMaternidad',
    reference: 'HMaternidad',
	id: 'idHMaternidadGrid',
    requires: [
        'personal.store.historial.HLicenciaStore'
    ],
    store: {
        type: 'HLicenciaStore'
    },
	title: 'Maternidad',
    border: false,
    flex: 1,
    tbar: [],
    plugins: [],
	columns: [
		{header: 'FPP', dataIndex: 'fpp', renderer: Ext.util.Format.dateRenderer('d-m-Y'), flex: 2},
		{header: 'Parto', dataIndex: 'fp', renderer: Ext.util.Format.dateRenderer('d-m-Y'), flex: 2, editor: {xtype: 'datefield',
                allowBlank: false,
                format: 'd-m-Y'
                }},
		{header: 'Desde', dataIndex: 'desde',  renderer: Ext.util.Format.dateRenderer('d-m-Y'), flex: 2},
		{header: 'Hasta', dataIndex: 'hasta',  renderer: Ext.util.Format.dateRenderer('d-m-Y'), flex: 2},
		{header: 'Lacta', dataIndex: 'lactahasta', renderer: Ext.util.Format.dateRenderer('d-m-Y'), flex: 2, editor: {xtype: 'datefield',
                allowBlank: false,
                format: 'd-m-Y'
                }},
		{header: 'Horario', dataIndex: 'lactahorario', flex: 2, editor: {allowBlank: false}},
        {header: 'Otorg.', dataIndex: 'dias_licencia', flex: 1},
        {header: 'Desiste', dataIndex: 'goce', flex: 1},
        {header: 'Notif. mail', dataIndex: 'notificado', flex: 1}
	],

	initComponent: function() {
		/*var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
        clicksToMoveEditor: 1,
        autoCancel: false
        });*/

        var plugins = [];
        //plugins.push(rowEditing);
        Ext.apply(this.plugins,plugins);

		var items=[/*{
            //text: 'Imprimir',
            iconCls: 'print2',
            scale: 'medium',
            tooltip: 'Imprimir grilla',
	    	handler: function(btn) {
               Ext.ux.grid.Printer.print(this.up().up());
               //console.log(this.up().up());
            }
		}*/]; //Arreglo de items
		if(AppGlobals.permisos[0].pesta_licencias == 1)//if(AppGlobals.permiso == "1") 
        {
            items.push(
            {
                disabled: true,
                id: 'btnEditarMaternidad',
                //text: 'Desiste',
                iconCls: 'edit3',
                action: 'editarMaternidad',
                scale: 'medium',
                tooltip: 'Editar fechas'
            },{
                disabled: true,
                id: 'btnGuardarMaternidad',
                //text: 'Desiste',
                iconCls: 'save3',
                action: 'guardarCambiosMaternidad',
                scale: 'medium',
                tooltip: 'Guardar cambios'
            });
            //Aplico el arreglo de botones a la vista
            Ext.apply(this, {tbar: items});
        }
		this.callParent(arguments);
	}
});