Ext.define('personal.view.historial.HSaludLargoTratamiento', {
	extend: 'Ext.grid.Grid',
	alias: 'widget.HSaludLargoTratamiento',
	store: 'historial.HSaludLargoTratamientoStore',
	title: 'Licencias Salud por Largo Tratamiento',
    border: false,
    flex: 1,
    tbar: [/*{
            //text: 'Imprimir',
            iconCls: 'print2',
            scale: 'medium',
            tooltip: 'Imprimir grilla',
	    handler: function(btn) {
               Ext.ux.grid.Printer.print(this.up().up());
               //console.log(this.up().up());
            }
	}*/],
	columns: [
		{header: 'Desde', dataIndex: 'desde',  renderer: Ext.util.Format.dateRenderer('d-m-Y'), width: 70},
		{header: 'Hasta', dataIndex: 'hasta',  renderer: Ext.util.Format.dateRenderer('d-m-Y'), width: 70},
		//{header: 'Ped.', dataIndex: 'diaspedidos', width: 30},
		{header: 'Otorg.', dataIndex: 'diasotorgad', width: 50},
                {header: 'Desiste', dataIndex: 'goce', width: 50},
		{header: 'Causa', dataIndex: 'causa', flex: 1}
	],

	initComponent: function() {

		this.callParent(arguments);
	}
});