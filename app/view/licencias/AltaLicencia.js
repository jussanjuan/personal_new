Ext.define('personal.view.licencias.AltaLicencia', {
	extend: 'Ext.window.Window',
	alias: 'widget.AltaLicencia',
	title: 'Cargar nueva licencia',
	defaultType: 'textfield',
    closeAction: 'destroy',
    border: false,
    modal: true,
    width: 500,
    frame: true,
    layout: 'fit',
    //minHeight: 500,
    //maxHeight: 1000,
    fieldDefaults: {
       	anchor: '100%',
       	labelAlign: 'top'
    },
    requires:[
        'personal.store.LicenciaTipoStore'        
    ],
    items: [{
        xtype: 'form',
        bodyPadding: 5,
        method: 'POST',
        id: 'formAltaLicencia',
        items: [{
            xtype: 'combo',
            id: 'comboTipoLicenciaAltaLicencia',
            name: 'tipoLicencia',
            displayField: 'nombre',
            valueField: 'id',
            fieldLabel: 'Licencia',
            store: Ext.create('personal.store.LicenciaTipoStore'),
            forceSelection: true,
            queryMode: 'local',
            validateOnChange: 'true',
            validator: function(value){
            if (!value)
                return ("Seleccione licencia");
            else
                return true;
            },
            anchor: '95%',
            allowBlank: false
        },
        {
            xtype: 'hidden',
            id: 'legajoAltaLicencia',
            name: 'legajo',
        },
        {   
            xtype: 'fieldcontainer',
            anchor: '95%',
            layout: 'hbox',
            items:[{
                id: 'disponiblesAltaLicencia',
                name: 'disponiblesAltaLicencia',
                xtype: 'displayfield',
                fieldLabel: 'Disponibles',
                flex: 1
                
            },{
                id: 'tomadosAltaLicencia',
                name: 'tomadosAltaLicencia',
                xtype: 'displayfield',
                fieldLabel: 'Tomados',
                flex: 1 
                
            }]
        },
        {   
            xtype: 'fieldcontainer',
            anchor: '95%',
            layout: 'hbox',
            items:[{
                id: 'altaLicenciaDesde',
                name: 'altaLicenciaDesde',
                xtype: 'datefield',
                fieldLabel: 'Desde',
                format: 'd/m/Y',
                //minValue: new Date(),
                //value: new Date(),
                flex: 1,
                allowBlank: false
            },{
                id: 'altaLicenciaHasta',
                name: 'altaLicenciaHasta',
                xtype: 'datefield',
                fieldLabel: 'Hasta',
                format: 'd/m/Y',
                //minValue: new Date(),
                flex: 1,
                allowBlank: false
            }]
        },
        {
            id: 'diasLicenciaAltaLicencia',
            name: 'diasLicenciaAltaLicencia',
            xtype: 'displayfield',
            fieldLabel: 'Total d&iacute;as'
            
        },{
            xtype: 'label',
            html: '',
            id: 'errorLicencia',
            height: 50,
            border: false,
            hidden: true

        },{
            id: 'altaLicenciaCausa',
            xtype: 'textfield',
            name: 'altaLicenciaCausa',
            fieldLabel: 'Causa',
            anchor: '95%'
        },{
            id: 'altaLicenciaExpediente',
            xtype: 'textfield',
            name: 'altaLicenciaExpediente',
            //maskRe: new RegExp(/^\d{1}$/),
            fieldLabel: 'Expediente',
            anchor: '95%'
        },{
            id: 'altaLicenciaObservaciones',
            xtype: 'textareafield',
            grow: true,
            anchor: '95%',
            name: 'altaLicenciaObservaciones',
            fieldLabel: 'Observaciones'
        },
        {
            xtype: 'checkboxgroup',
            columns: 3,
            vertical: true,
            items: [{
                id: 'altaLicenciaPresentismo',
                name: 'altaLicenciaPresentismo',
                xtype: 'checkbox',
                boxLabel: 'Pierde presentismo',
                checked: false
            },{
                id: 'altaLicenciaHabiles',
                name: 'altaLicenciaHabiles',
                xtype: 'checkbox',
                boxLabel: 'H&aacute;biles',
                checked: false
            },{
                id: 'notificacion_mail',
                name: 'notificacion_mail',
                xtype: 'checkbox',
                boxLabel: 'Notificar por correo',
                checked: false
            }]
        },
        {
            xtype: 'fieldcontainer',
            anchor: '95%',
            id: 'estFieldsAltaLicencia',
            hidden: true,
            items: [{
                id: 'est_altaLicenciaAsignatura',
                name:'est_altaLicenciaAsignatura',
                xtype: 'textfield',
                fieldLabel: 'Asignatura',
                allowBlank: false,
                anchor: '100%',
                maskRe: /[a-zA-Z\s]+/,
                //hidden: true
            },{
                id: 'est_altaLicenciaCarrera',
                name:'est_altaLicenciaCarrera',
                xtype: 'textfield',
                fieldLabel: 'Carrera',
                allowBlank: false,
                anchor: '100%',
                maskRe: /[a-zA-Z\s]+/,
                //hidden: true
            },{
                id: 'est_altaLicenciaFechaExamen',
                name: 'est_altaLicenciaFechaExamen',
                xtype: 'datefield',
                fieldLabel: 'Fecha Examen',
                format: 'd-m-Y',
                //minValue: new Date(),
                value: new Date(),
                anchor: '100%',
                allowBlank: false,
                //hidden: true
            }]
        },
        {
            xtype: 'fieldcontainer',
            anchor: '95%',
            layout: 'hbox',
            id: 'matFieldsAltaLicencia_1',
            hidden: true,
            items:[{
                id: 'mat_altaLicenciaFpp',
                name: 'mat_altaLicenciaFpp',
                xtype: 'datefield',
                fieldLabel: 'Fecha Probable de Parto',
                format: 'd-m-Y',
                //minValue: new Date(),
                allowBlank: false,
                flex: 1,
                //hidden: true
            },{
                id: 'mat_altaLicenciaFp',
                name: 'mat_altaLicenciaFp',
                xtype: 'datefield',
                fieldLabel: 'Fecha de Parto',
                format: 'd-m-Y',
                flex: 1,
                //hidden: true
                //minValue: new Date(),
                //allowBlank: false
            }]
        },
        {
            xtype: 'fieldcontainer',
            anchor: '95%',
            layout: 'hbox',
            id: 'matFieldsAltaLicencia_2',
            hidden: true,
            items: [{
                id: 'mat_altaLicenciaLactanciaHasta',
                name: 'mat_altaLicenciaLactanciaHasta',
                xtype: 'datefield',
                fieldLabel: 'Lact. hasta',
                format: 'd-m-Y',
                flex: 2,
                labelWidth: '20%'
                //hidden: true
                //minValue: new Date(),
                //allowBlank: false
            }, {
                id: 'mat_altaLicenciaLactanciaHorario',
                name: 'mat_altaLicenciaLactanciaHorario',
                xtype: 'textfield',
                fieldLabel: 'Hs. lact.',
                maskRe: /[a-zA-Z\s]+/,
                flex: 2,
                labelWidth: '20%'
                //hidden: true
            }, {
                id: 'mat_altaLicenciaCertificadoNacimiento',
                name: 'mat_altaLicenciaCertificadoNacimiento',
                xtype: 'checkbox',
                boxLabel: 'Cert. nac.',
                checked: false,
                labelWidth: '20%',
                //hidden: true,
                flex: 1
            }]
        }]
    }],

    buttons: [{
    	text: 'Guardar',
        id: 'saveAltaLicencia',
        action: 'saveAltaLicencia',
        iconCls: 'save2'
        
    }, {
    	text: 'Cancelar',
        action: 'cancelar',
        iconCls: 'cancel2'
        
    }],

	initComponent: function() {

		this.callParent(arguments);
	}
});