/***

Licencias

***/

Ext.define('personal.view.licencias.altaLicencia', {
	extend: 'Ext.form.Panel',
	alias: 'widget.altaLicencia',
	//store: 'PersonalStore',
	title: 'Licencia',
    border: false,
    flex: 1,
    defaultType: 'textfield',
    trackResetOnLoad: true,
    bodyPadding: 5,
    fieldDefaults: {
       	anchor: '90%',
       	labelAlign: 'top'
    },
    items: [{
                id: 'disponibles',
                name: 'disponibles',
                xtype: 'textfield',
                fieldLabel: 'Disponibles',
                readOnly: true
            }, {
                id: 'tomados',
                name: 'tomados',
                xtype: 'textfield',
                fieldLabel: 'Tomados',
                readOnly: true
            }, {
                id: 'desde',
                name: 'desde',
                xtype: 'datefield',
                fieldLabel: 'Desde',
                format: 'd-m-Y',
                submitFormat: 'd-m-Y',
                value: new Date(),
                allowBlank: false
            }, {
                id: 'hasta',
                name: 'hasta',
                xtype: 'datefield',
                fieldLabel: 'Hasta',
                format: 'd-m-Y',
                submitFormat: 'd-m-Y',
                //minValue: new Date(),
                allowBlank: false
            }, {
                id: 'diasLicencia',
                name: 'diasLicencia',
                xtype: 'textfield',
                fieldLabel: 'Dias otorgados',
                readOnly: true,
                allowBlank: false
            }, {
                id: 'causa',
                name: 'causa',
                xtype: 'textfield',
                fieldLabel: 'Causa',
                maskRe: /[a-zA-Z\s]+/
            }, {
                id: 'observaciones',
                name: 'observaciones',
                xtype: 'textfield',
                fieldLabel: 'Observaciones',
                maskRe: /[a-zA-Z\s]+/
            }, {
                id: 'expediente',
                name: 'expediente',
                xtype: 'textfield',
                fieldLabel: 'Expediente'
            }, {
                id: 'pierde_presentismo',
                name: 'pierde_presentismo',
                xtype: 'checkbox',
                boxLabel: 'Pierde presentismo',
                checked: false
            }, {
                id: 'habiles',
                name: 'habiles',
                xtype: 'checkbox',
                boxLabel: 'Habiles',
                checked: false
            }],
                        

    buttons: [{
    	text: 'Guardar',
        action: 'guardar',
        id: 'saveLicencia',
        iconCls: 'save',
        scale: 'large'
        
    }/*, {
        id: '1_btnlimpiar',
    	text: 'Limpiar',
        action: 'limpiar',
        iconCls: 'clear',
        scale: 'large'
    }*/],

	initComponent: function() {
           
		this.callParent(arguments);
	}
        
});