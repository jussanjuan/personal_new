Ext.define('personal.view.licencias.LComision', {
	extend: 'Ext.window.Window',
	alias: 'widget.LComision',
	title: 'Agregar comisi&oacute;n',
	defaultType: 'textfield',
    closeAction: 'destroy',
    border: false,
    modal: true,
    width: 500,
    frame: true,
    closeAction: 'destroy',
    layout: 'fit',
    //minHeight: 500,
    //maxHeight: 1000,
    fieldDefaults: {
       	anchor: '100%',
       	labelAlign: 'top'
    },

    items: [{
        xtype: 'form',
        bodyPadding: 5,
        method: 'POST',
        id: 'formComision',
        items: [{
            xtype: 'textfield',
            id: 'legajoComision',
            name: 'legajo',
            fieldLabel: 'Legajo',
            readOnly: true,
            allowBlank: false,
            anchor: '95%'
            
        },{
            id: 'comisionDesde',
    	    name: 'comisionDesde',
            xtype: 'datefield',
            fieldLabel: 'Trabaja desde',
            format: 'd/m/Y',
            //minValue: new Date(),
            value: new Date(),
            allowBlank: false,
            anchor: '95%'
        }, {
            id: 'comisionHasta',
    	    name: 'comisionHasta',
            xtype: 'datefield',
            fieldLabel: 'Trabaja hasta',
            format: 'd/m/Y',
            //minValue: new Date(),
            allowBlank: false,
            anchor: '95%'
        }, {
            id: 'comisionTotalDias',
    	    xtype: 'textfield',
            name: 'comisionTotalDias',
            //maskRe: new RegExp(/^\d{1}$/),
    	    fieldLabel: 'Total de dias',
            allowBlank: false,
            readOnly: true,
            anchor: '95%'
        },{
            id: 'comisionCausa',
            xtype: 'textfield',
            name: 'comisionCausa',
            fieldLabel: 'Causa',
            anchor: '95%'
        },{
            id: 'comisionExpediente',
            xtype: 'textfield',
            name: 'comisionExpediente',
            maskRe: new RegExp(/^\d{1}$/),
            fieldLabel: 'Expediente',
            anchor: '95%'
        },{
            id: 'comisionObservaciones',
            xtype: 'textareafield',
            grow: true,
            anchor: '95%',
            name: 'comisionObservaciones',
            fieldLabel: 'Observaciones'
        }]
    }],

    buttons: [{
    	text: 'Guardar',
        action: 'saveComision',
        iconCls: 'save2'
        
    }, {
    	text: 'Cancelar',
        action: 'cancelar',
        iconCls: 'cancel2'
        
    }],

	initComponent: function() {

		this.callParent(arguments);
	}
});