/***

Licencias Culturales - Cod 11

***/

Ext.define('personal.view.licencias.LCulturales', {
	extend: 'Ext.form.Panel',
	alias: 'widget.LCulturales',
	//store: 'PersonalStore',
	title: 'Licencia Culturales',
    border: false,
    flex: 1,
    defaultType: 'textfield',
    bodyPadding: 5,
    fieldDefaults: {
       	anchor: '90%',
       	labelAlign: 'top'
    },

    items: [{
        id: '11_Disponibles',
        name: 'disponibles',
        xtype: 'textfield',
        fieldLabel: 'Dias tomados',
        readOnly: true
    }, {
        id: '11_Desde',
        name: 'desde',
        xtype: 'datefield',
        fieldLabel: 'Desde',
        format: 'd-m-Y',
        value: new Date(),
        //minValue: new Date(),
        allowBlank: false
    }, {
        id: '11_Hasta',
        name: 'hasta',
        xtype: 'datefield',
        fieldLabel: 'Hasta',
        format: 'd-m-Y',
        //minValue: new Date(),
        allowBlank: false
    }, {
        id: '11_DiasOtorgados',
        name: 'diasotorgados',
        fieldLabel: 'Dias otorgados',
        xtype: 'textfield',
        readOnly: true
    }, {
        id: '11_Causa',
        name:'causa',
        xtype: 'textfield',
        fieldLabel: 'Causa',
        maskRe: /[a-zA-Z\s]+/
    } ],

    buttons: [{
    	text: 'Guardar',
        action: 'guardar',
        iconCls: 'save',
        scale: 'large'
    }/*, {
    	text: 'Limpiar',
        action: 'limpiar',
        iconCls: 'clear',
        scale: 'large'
    }*/],

	initComponent: function() {

		this.callParent(arguments);
	}
});