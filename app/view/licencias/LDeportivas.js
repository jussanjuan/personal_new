/***

Licencias Deportivas - Cod 16

***/

Ext.define('personal.view.licencias.LDeportivas', {
	extend: 'Ext.form.Panel',
	alias: 'widget.LDeportivas',
	//store: 'PersonalStore',
	title: 'Licencia Deportivas',
    border: false,
    flex: 1,
    defaultType: 'textfield',
    bodyPadding: 5,
    fieldDefaults: {
       	anchor: '90%',
       	labelAlign: 'top'
    },

    items: [{
        id: '16_Disponibles',
        name: 'disponibles',
        xtype: 'textfield',
        fieldLabel: 'Dias tomados',
        readOnly: true
    }, {
        id: '16_Desde',
        name: 'desde',
        xtype: 'datefield',
        fieldLabel: 'Desde',
        format: 'd-m-Y',
        value: new Date(),
        //minValue: new Date(),
        allowBlank: false
    }, {
        id: '16_Hasta',
        name: 'hasta',
        xtype: 'datefield',
        fieldLabel: 'Hasta',
        format: 'd-m-Y',
        //minValue: new Date(),
        allowBlank: false
    }, {
        id: '16_DiasOtorgados',
        name: 'diasotorgados',
        fieldLabel: 'Dias otorgados',
        xtype: 'textfield',
        readOnly: true
    }, {
        id: '16_Causa',
        name:'causa',
        xtype: 'textfield',
        fieldLabel: 'Motivo',
        maskRe: /[a-zA-Z\s]+/
    }],

    buttons: [{
    	text: 'Guardar',
        action: 'guardar',
        iconCls: 'save',
        scale: 'large'
    }/*, {
    	text: 'Limpiar',
        action: 'limpiar',
        iconCls: 'clear',
        scale: 'large'
    }*/],

	initComponent: function() {

		this.callParent(arguments);
	}
});