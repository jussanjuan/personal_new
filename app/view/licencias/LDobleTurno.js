/***

Licencias Compensatorias - Cod 10

20 dias anuales y 3 por mes maximo. Si pasa los 8 dias ya corresponde a largo tratamiento

***/

Ext.define('personal.view.licencias.LDobleTurno', {
	extend: 'Ext.form.Panel',
	alias: 'widget.LDobleTurno',
	title: 'Licencia por doble turno',
    border: false,
    flex: 1,
    defaultType: 'textfield',
    bodyPadding: 5,
    fieldDefaults: {
       	anchor: '90%',
       	labelAlign: 'top'
    },
    config: {
        diasLic: 20
    },

    items: [{
        id: '18_Disponibles',
        name: 'disponibles',
        xtype: 'textfield',
        fieldLabel: 'Dias disponibles',
        readOnly: true
        
    }, {
        id: '18_Desde',
        name: 'desde',
        xtype: 'datefield',
        fieldLabel: 'Desde',
        format: 'd/m/Y',
        //minValue: new Date(),
        value: new Date(),
        allowBlank: false
    }, {
        id: '18_Hasta',
        name: 'hasta',
        xtype: 'datefield',
        fieldLabel: 'Hasta',
        format: 'd/m/Y',
        //minValue: new Date(),
        allowBlank: false
    }, {
        id: '18_DiasOtorgados',
        name: 'diasotorgados',
        xtype: 'textfield',
        fieldLabel: 'Dias otorgados',
        readOnly: true
    }, {
        id: '18_Causa',
        name: 'causa',
        xtype: 'textfield',
        fieldLabel: 'Causa',
        maskRe: /[a-zA-Z\s]+/
    }],

    buttons: [{
    	text: 'Guardar',
        action: 'guardar',
        iconCls: 'save',
        scale: 'large'
        
    }/*, {
    	text: 'Limpiar',
        action: 'limpiar'
    }*/],

	initComponent: function() {

		this.callParent(arguments);


	}
});