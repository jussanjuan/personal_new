/***

Licencias por Estudio - Cod 12

***/

Ext.define('personal.view.licencias.LEstudio', {
	extend: 'Ext.form.Panel',
	alias: 'widget.LEstudio',
	//store: 'PersonalStore',
	title: 'Alta licencia Estudio',
    border: false,
    flex: 1,
    defaultType: 'textfield',
    bodyPadding: 5,
    fieldDefaults: {
       	anchor: '90%',
       	labelAlign: 'top'
    },

    items: [{
        id: 'est_disponibles',
        name: 'disponibles',
        xtype: 'textfield',
        fieldLabel: 'Disponibles',
        readOnly: true
    }, {
        id: 'est_tomados',
        name: 'tomados',
        xtype: 'textfield',
        fieldLabel: 'Tomados',
        readOnly: true
    },{
        id: 'est_asignatura',
        name:'asignatura',
        xtype: 'textfield',
        fieldLabel: 'Asignatura',
        allowBlank: false,
        maskRe: /[a-zA-Z\s]+/
    }, {
        id: 'est_carrera',
        name:'carrera',
        xtype: 'textfield',
        fieldLabel: 'Carrera',
        allowBlank: false,
        maskRe: /[a-zA-Z\s]+/
    }, {
        id: 'est_fecha_examen',
        name: 'fecha_examen',
        xtype: 'datefield',
        fieldLabel: 'Fecha Examen',
        format: 'd-m-Y',
        //minValue: new Date(),
        value: new Date(),
        allowBlank: false
    }, {
        id: 'est_Desde',
        name: 'desde',
        xtype: 'datefield',
        fieldLabel: 'Desde',
        format: 'd-m-Y',
        //minValue: new Date(),
        value: new Date(),
        //minValue: new Date(),
        allowBlank: false
    }, {
        id: 'est_Hasta',
        name: 'hasta',
        xtype: 'datefield',
        fieldLabel: 'Hasta',
        format: 'd-m-Y',
        //minValue: new Date(),
        allowBlank: false
    }, {
        id: 'est_diasLicencia',
        name: 'diasLicencia',
        xtype: 'textfield',
        fieldLabel: 'Dias otorgados',
        readOnly: true,
        allowBlank: false
    }, {
        id: 'est_causa',
        name: 'causa',
        xtype: 'textfield',
        fieldLabel: 'Causa',
        maskRe: /[a-zA-Z\s]+/
    }, {
        id: 'est_observaciones',
        name: 'observaciones',
        xtype: 'textfield',
        fieldLabel: 'Observaciones',
        maskRe: /[a-zA-Z\s]+/
    }, {
        id: 'est_expediente',
        name: 'expediente',
        xtype: 'textfield',
        fieldLabel: 'Expediente'
    }, {
        id: 'est_pierde_presentismo',
        name: 'pierde_presentismo',
        xtype: 'checkbox',
        boxLabel: 'Pierde presentismo',
        checked: false
    }],

    buttons: [{
    	text: 'Guardar',
        action: 'guardar',
        iconCls: 'save',
        scale: 'large'
    }/*, {
    	text: 'Limpiar',
        action: 'limpiar',
        iconCls: 'clear',
        scale: 'large'
    }*/],

	initComponent: function() {

		this.callParent(arguments);
	}
});