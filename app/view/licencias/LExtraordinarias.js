/***

Licencias Extraordinarias - Cod 17

***/

Ext.define('personal.view.licencias.LExtraordinarias', {
	extend: 'Ext.form.Panel',
	alias: 'widget.LExtraordinarias',
	//store: 'PersonalStore',
	title: 'Licencia Extraordinaria',
    border: false,
    flex: 1,
    defaultType: 'textfield',
    bodyPadding: 5,
    fieldDefaults: {
       	anchor: '90%',
       	labelAlign: 'top'
    },

    items: [{
        id: '17_Desde',
        name: 'desde',
        xtype: 'datefield',
        fieldLabel: 'Desde',
        format: 'd-m-Y',
        value: new Date(),
        //minValue: new Date(),
        allowBlank: false
    }, {
        id: '17_Hasta',
        name: 'hasta',
        xtype: 'datefield',
        fieldLabel: 'Hasta',
        format: 'd-m-Y',
        //minValue: new Date(),
        allowBlank: false
    }, {
        id: '17_DiasOtorgados',
        name: 'diasotorgados',
        fieldLabel: 'Dias otorgados',
        xtype: 'textfield',
        readOnly: true
    }, {
        id: '17_Observaciones',
        name:'observaciones',
        xtype: 'textfield',
        fieldLabel: 'Observaciones',
        maskRe: /[a-zA-Z\s]+/
    }],

    buttons: [{
    	text: 'Guardar',
        action: 'guardar',
        iconCls: 'save',
        scale: 'large'
    }/*, {
    	text: 'Limpiar',
        action: 'limpiar',
        iconCls: 'clear',
        scale: 'large'
    }*/],

	initComponent: function() {

		this.callParent(arguments);
	}
});