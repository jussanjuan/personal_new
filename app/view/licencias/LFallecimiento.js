/***

Licencias por Fallecimiento - cod 7

***/

Ext.define('personal.view.licencias.LFallecimiento', {
	extend: 'Ext.form.Panel',
	alias: 'widget.LFallecimiento',
	//store: 'PersonalStore',
	title: 'Licencia por Fallecimiento',
    border: false,
    flex: 1,
    defaultType: 'textfield',
    bodyPadding: 5,
    fieldDefaults: {
       	anchor: '90%',
       	labelAlign: 'top'
    },

    items: [{
        id: '7_Desde',
        name: 'desde',
        xtype: 'datefield',
        fieldLabel: 'Desde',
        format: 'd-m-Y',
        //minValue: new Date(),
        allowBlank: false
    }, {
        id: '7_Hasta',
        name: 'hasta',
        xtype: 'datefield',
        fieldLabel: 'Hasta',
        format: 'd-m-Y',
        //minValue: new Date(),
        allowBlank: false
    }, {
        id: '7_DiasOtorgados',
        name: 'diasotorgados',
        fieldLabel: 'Dias otorgados',
        xtype: 'textfield',
        readOnly: true
    }, {
        id: '7_Observaciones',
        name: 'observaciones',
        xtype: 'textfield',
        fieldLabel: 'Observaciones',
        maskRe: /[a-zA-Z\s]+/
    }, {
        id: '7_Habiles',
        name: 'habiles',
        xtype: 'checkbox',
        boxLabel: 'Habiles',
        checked: false
    }],

    buttons: [{
    	text: 'Guardar',
        action: 'guardar',
        iconCls: 'save',
        scale: 'large'
    }/*, {
    	text: 'Limpiar',
        action: 'limpiar',
        iconCls: 'clear',
        scale: 'large'
    }*/],

	initComponent: function() {

		this.callParent(arguments);
	}
});