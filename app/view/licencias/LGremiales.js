/***

Licencias Gremiales - Cod 13

***/

Ext.define('personal.view.licencias.LGremiales', {
	extend: 'Ext.form.Panel',
	alias: 'widget.LGremiales',
	store: 'PersonalStore',
	title: 'Licencia Gremiales',
    border: false,
    flex: 1,
    defaultType: 'textfield',
    trackResetOnLoad: true,
    bodyPadding: 5,
    fieldDefaults: {
       	anchor: '90%',
       	labelAlign: 'top'
    },
    config: {
        diasLic: 20
    },

    items: [{
        id: '13_Disponibles',
        name: 'disponibles',
        xtype: 'textfield',
        fieldLabel: 'Dias tomados',
        readOnly: true
    }, {
        id: '13_Desde',
        name: 'desde',
        xtype: 'datefield',
        fieldLabel: 'Desde',
        format: 'd-m-Y',
        //minValue: new Date(),
        value: new Date(),
        allowBlank: false
    }, {
        id: '13_Hasta',
        name: 'hasta',
        xtype: 'datefield',
        fieldLabel: 'Hasta',
        format: 'd-m-Y',
        //minValue: new Date(),
        allowBlank: false
    }, {
        id: '13_DiasOtorgados',
        name: 'diasotorgados',
        fieldLabel: 'Dias otorgados',
        xtype: 'textfield',
        readOnly: true
    }, {
        id: '13_Causa',
        name: 'causa',
        xtype: 'textfield',
        fieldLabel: 'Causa',
        maskRe: /[a-zA-Z\s]+/
    } ],

    buttons: [{
    	text: 'Guardar',
        action: 'guardar',
        iconCls: 'save',
        scale: 'large'
    }/*, {
    	text: 'Limpiar',
        action: 'limpiar',
        iconCls: 'clear',
        scale: 'large'
    }*/],

	initComponent: function() {

		this.callParent(arguments);


	}
});