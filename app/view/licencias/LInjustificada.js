/***

Licencias por Salud Personal - Cod 1

20 dias anuales y 3 por mes maximo. Si pasa los 8 dias ya corresponde a largo tratamiento

***/

Ext.define('personal.view.licencias.LInjustificada', {
	extend: 'Ext.form.Panel',
	alias: 'widget.LInjustificada',
	//store: 'PersonalStore',
	title: 'Licencia injustificada (falta sin aviso ni certificado)',
    border: false,
    flex: 1,
    defaultType: 'textfield',
    trackResetOnLoad: true,
    bodyPadding: 5,
    fieldDefaults: {
       	anchor: '90%',
       	labelAlign: 'top'
    },
    config: {
        diasLic: 20
    },
    
    items: [{
                id: '19_Disponibles',
                name: 'disponibles',
                xtype: 'textfield',
                fieldLabel: 'Dias tomados',
                readOnly: true
            }, {
                id: '19_Desde',
                name: 'desde',
                xtype: 'datefield',
                fieldLabel: 'Desde',
                format: 'd-m-Y',
                submitFormat: 'd-m-Y',
                
                value: new Date(),
                allowBlank: false
            }, {
                id: '19_Hasta',
                name: 'hasta',
                xtype: 'datefield',
                fieldLabel: 'Hasta',
                format: 'd-m-Y',
                submitFormat: 'd-m-Y',
                //minValue: new Date(),
                allowBlank: false
            }, {
                id: '19_DiasOtorgados',
                name: 'diasotorgados',
                xtype: 'textfield',
                fieldLabel: 'Dias otorgados',
                readOnly: true,
                allowBlank: false
            }, {
                id: '19_Causa',
                name: 'causa',
                xtype: 'textfield',
                fieldLabel: 'Causa',
                maskRe: /[a-zA-Z\s]+/
            }],

    buttons: [{
    	text: 'Guardar',
        action: 'guardar',
        id: '19_btnGuardar',
        iconCls: 'save',
        scale: 'large'
        
    }/*, {
        id: '1_btnlimpiar',
    	text: 'Limpiar',
        action: 'limpiar',
        iconCls: 'clear',
        scale: 'large'
    }*/],

	initComponent: function() {
           
		this.callParent(arguments);
	}
        
});