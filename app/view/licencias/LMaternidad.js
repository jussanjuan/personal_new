/***

Licencias por Maternidad - Cod 5

Fecha probable de parto - Certificado - Horario de lactancia - Fin lactancia - Fecha de parto - Desde, hasta

***/

Ext.define('personal.view.licencias.LMaternidad', {
	extend: 'Ext.form.Panel',
	alias: 'widget.LMaternidad',
	//store: 'PersonalStore',
	title: 'Alta licencia Maternidad',
    border: false,
    flex: 1,
    defaultType: 'textfield',
    bodyPadding: 5,
    fieldDefaults: {
       	anchor: '90%',
       	labelAlign: 'top'
    },

    items: [/*{
        id: '5_disponibles',
        name: 'disponibles',
        xtype: 'textfield',
        fieldLabel: 'Disponibles',
        readOnly: true
    }, {
        id: '5_tomados',
        name: 'tomados',
        xtype: 'textfield',
        fieldLabel: 'Tomados',
        readOnly: true
    },*/{
        id: 'fpp',
        name: 'fpp',
        xtype: 'datefield',
        fieldLabel: 'Fecha Probable de Parto',
        format: 'd-m-Y',
        //minValue: new Date(),
        allowBlank: false
    }, {
        id: 'fp',
        name: 'fp',
        xtype: 'datefield',
        fieldLabel: 'Fecha de Parto',
        format: 'd-m-Y'
        //minValue: new Date(),
        //allowBlank: false
    }, {
        id: 'mat_Desde',
        name: 'desde',
        xtype: 'datefield',
        fieldLabel: 'Desde',
        format: 'd-m-Y',
        //minValue: new Date(),
        allowBlank: false
    }, {
        id: 'mat_Hasta',
        name: 'hasta',
        xtype: 'datefield',
        fieldLabel: 'Hasta',
        format: 'd-m-Y',
        //minValue: new Date(),
        allowBlank: false
    }, {
        id: 'mat_diasLicencia',
        name: 'diasLicencia',
        fieldLabel: 'Dias otorgados',
        xtype: 'textfield',
        readOnly: true
    },{
        id: 'lactahasta',
        name: 'lactahasta',
        xtype: 'datefield',
        fieldLabel: 'Lactancia Hasta',
        format: 'd-m-Y'
        //minValue: new Date(),
        //allowBlank: false
    }, {
        id: 'lactahorario',
        name: 'lactahorario',
        xtype: 'textfield',
        fieldLabel: 'Horario de Lactancia',
        maskRe: /[a-zA-Z\s]+/
    }, {
        id: 'certnac',
        name: 'certnac',
        xtype: 'checkbox',
        boxLabel: 'Cert. Nacimiento',
        checked: false
    },{
        id: 'mat_causa',
        name: 'causa',
        xtype: 'textfield',
        fieldLabel: 'Causa',
        maskRe: /[a-zA-Z\s]+/
    }, {
        id: 'mat_observaciones',
        name: 'observaciones',
        xtype: 'textfield',
        fieldLabel: 'Observaciones',
        maskRe: /[a-zA-Z\s]+/
    }, {
        id: 'mat_expediente',
        name: 'expediente',
        xtype: 'textfield',
        fieldLabel: 'Expediente'
    }, {
        id: 'mat_pierde_presentismo',
        name: 'pierde_presentismo',
        xtype: 'checkbox',
        boxLabel: 'Pierde presentismo',
        checked: false
    }],

    buttons: [{
    	text: 'Guardar',
        action: 'guardar',
        iconCls: 'save',
        scale: 'large'
    }/*, {
    	text: 'Limpiar',
        action: 'limpiar',
        iconCls: 'clear',
        scale: 'large'
    }*/],

	initComponent: function() {

		this.callParent(arguments);
	}
});