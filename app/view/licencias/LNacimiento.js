/***

Licencias por Nacimiento - Cod 6

Exclusivo para varones, 2 dias habiles (ahora creo que son 5 dias habiles)

***/

Ext.define('personal.view.licencias.LNacimiento', {
	extend: 'Ext.form.Panel',
	alias: 'widget.LNacimiento',
	//store: 'PersonalStore',
	title: 'Licencia por Nacimiento',
    border: false,
    flex: 1,
    defaultType: 'textfield',
    bodyPadding: 5,
    fieldDefaults: {
       	anchor: '90%',
       	labelAlign: 'top'
    },

    items: [{
        id: '6_Desde',
        name: 'desde',
        xtype: 'datefield',
        fieldLabel: 'Desde',
        format: 'd-m-Y',
        //minValue: new Date(),
        allowBlank: false
    }, {
        id: '6_Hasta',
        name: 'hasta',
        xtype: 'datefield',
        fieldLabel: 'Hasta',
        format: 'd-m-Y',
        //minValue: new Date(),
        allowBlank: false
    },{
        id: '6_DiasOtorgados',
        name: 'diasotorgados',
        fieldLabel: 'Dias otorgados',
        xtype: 'textfield',
        readOnly: true
    }, {
        id: '6_Observaciones',
        name: 'observaciones',
        xtype: 'textfield',
        fieldLabel: 'Observaciones',
        maskRe: /[a-zA-Z\s]+/
    }, {
        id: '6_Acredito',
        name: 'acredito',
        xtype: 'checkbox',
        boxLabel: 'Acredito',
        checked: false
    }],

    buttons: [{
    	text: 'Guardar',
        action: 'guardar',
        iconCls: 'save',
        scale: 'large'
    }/*, {
    	text: 'Limpiar',
        action: 'limpiar',
        iconCls: 'clear',
        scale: 'large'
    }*/],

	initComponent: function() {

		this.callParent(arguments);
	}
});