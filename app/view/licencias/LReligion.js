/***

Licencias por Salud Personal - Cod 1

20 dias anuales y 3 por mes maximo. Si pasa los 8 dias ya corresponde a largo tratamiento

***/

Ext.define('personal.view.licencias.LReligion', {
	extend: 'Ext.form.Panel',
	alias: 'widget.LReligion',
	//store: 'PersonalStore',
	title: 'Licencia por d&iacute;a religioso',
    border: false,
    flex: 1,
    defaultType: 'textfield',
    trackResetOnLoad: true,
    bodyPadding: 5,
    fieldDefaults: {
       	anchor: '90%',
       	labelAlign: 'top'
    },
    config: {
        diasLic: 20
    },
    
    items: [{
                id: '20_Disponibles',
                name: 'disponibles',
                xtype: 'textfield',
                fieldLabel: 'Dias tomados',
                readOnly: true
            }, {
                id: '20_Desde',
                name: 'desde',
                xtype: 'datefield',
                fieldLabel: 'Desde',
                format: 'd-m-Y',
                submitFormat: 'd-m-Y',
                
                value: new Date(),
                allowBlank: false
            }, {
                id: '20_Hasta',
                name: 'hasta',
                xtype: 'datefield',
                fieldLabel: 'Hasta',
                format: 'd-m-Y',
                submitFormat: 'd-m-Y',
                //minValue: new Date(),
                allowBlank: false
            }, {
                id: '20_DiasOtorgados',
                name: 'diasotorgados',
                xtype: 'textfield',
                fieldLabel: 'Dias otorgados',
                readOnly: true,
                allowBlank: false
            }, {
                id: '20_Causa',
                name: 'causa',
                xtype: 'textfield',
                fieldLabel: 'Causa',
                maskRe: /[a-zA-Z\s]+/
            }],

    buttons: [{
    	text: 'Guardar',
        action: 'guardar',
        id: '20_btnGuardar',
        iconCls: 'save',
        scale: 'large'
        
    }/*, {
        id: '1_btnlimpiar',
    	text: 'Limpiar',
        action: 'limpiar',
        iconCls: 'clear',
        scale: 'large'
    }*/],

	initComponent: function() {
           
		this.callParent(arguments);
	}
        
});