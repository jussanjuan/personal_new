/***

Licencias por Salud por accidente de trabajo - Cod 4

Tener en cuenta por el presentismo

***/

Ext.define('personal.view.licencias.LSaludAccidente', {
	extend: 'Ext.form.Panel',
	alias: 'widget.LSaludAccidente',
	//store: 'PersonalStore',
	title: 'Licencia por Salud - Accidente de Trabajo',
    border: false,
    flex: 1,
    defaultType: 'textfield',
    bodyPadding: 5,
    fieldDefaults: {
       	anchor: '90%',
       	labelAlign: 'top'
    },

    items: [{
        id: '4_Desde',
        name: 'desde',
        xtype: 'datefield',
        fieldLabel: 'Desde',
        format: 'd-m-Y',
        value: new Date(),
        //minValue: new Date(),
        allowBlank: false
    }, {
        id: '4_Hasta',
        name: 'hasta',
        xtype: 'datefield',
        fieldLabel: 'Hasta',
        format: 'd-m-Y',
        //minValue: new Date(),
        allowBlank: false
    }, {
        id: '4_DiasOtorgados',
        name: 'diasotorgados',
        xtype: 'textfield',
        fieldLabel: 'Dias otorgados',
        readOnly: 'true'
        
    }, {
        id: '4_Causa',
        name: 'causa',
        xtype: 'textfield',
        fieldLabel: 'Causa',
        maskRe: /[a-zA-Z\s]+/
    }],

    buttons: [{
    	text: 'Guardar',
        action: 'guardar',
        iconCls: 'save',
        scale: 'large'
    }/*, {
    	text: 'Limpiar',
        action: 'limpiar',
        iconCls: 'clear',
        scale: 'large'
    }*/],

	initComponent: function() {

		this.callParent(arguments);
	}
});