/***

Licencias por Salud Familiar - Cod 2

10 dias anuales y 3 por mes maximo. 

***/

Ext.define('personal.view.licencias.LSaludFamiliar', {
	extend: 'Ext.form.Panel',
	alias: 'widget.LSaludFamiliar',
	//store: 'PersonalStore',
	title: 'Licencia por Salud - Familiar',
    border: false,
    flex: 1,
    defaultType: 'textfield',
    bodyPadding: 5,
    fieldDefaults: {
       	anchor: '90%',
       	labelAlign: 'top'
    },

    items: [{
        id: '2_Disponibles',
        name: 'disponibles',
        xtype: 'numberfield',
        fieldLabel: 'Dias tomados',
        readOnly: true
    }, {
        id: '2_Desde',
        name: 'desde',
        xtype: 'datefield',
        fieldLabel: 'Desde',
        format: 'd-m-Y',
        value: new Date(),
        //minValue: new Date(),
        allowBlank: false
    }, {
        id: '2_Hasta',
        name: 'hasta',
        xtype: 'datefield',
        fieldLabel: 'Hasta',
        format: 'd-m-Y',
        //minValue: new Date(),
        allowBlank: false
    },{
        id: '2_DiasOtorgados',
        name: 'diasotorgados',
        xtype: 'textfield',
        fieldLabel: 'Dias otorgados',
        readOnly: true,
        allowBlank: false
    },  {
        id: '2_Causa',
        name: 'causa',
        xtype: 'textfield',
        fieldLabel: 'Causa',
        maskRe: /[a-zA-Z\s]+/
    }],

    buttons: [{
    	text: 'Guardar',
        action: 'guardar',
        iconCls: 'save',
        scale: 'large'
    }/*, {
    	text: 'Limpiar',
        action: 'limpiar',
        iconCls: 'clear',
        scale: 'large'
    }*/],

	initComponent: function() {

		this.callParent(arguments);
	}
});