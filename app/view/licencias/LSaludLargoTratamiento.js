/***

Licencias por Salud de Largo Tratamiento - Cod 3

Mas de 8 dias

***/

Ext.define('personal.view.licencias.LSaludLargoTratamiento', {
	extend: 'Ext.form.Panel',
	alias: 'widget.LSaludLargoTratamiento',
	//store: 'PersonalStore',
	title: 'Licencia por Salud - Largo Tratamiento',
    border: false,
    flex: 1,
    defaultType: 'textfield',
    bodyPadding: 5,
    fieldDefaults: {
       	anchor: '90%',
       	labelAlign: 'top'
    },

    items: [{
        id: '3_Desde',
        name: 'desde',
        xtype: 'datefield',
        fieldLabel: 'Desde',
        format: 'd-m-Y',
        value: new Date(),
        //minValue: new Date(),
        allowBlank: false
    }, {
        id: '3_Hasta',
        name: 'hasta',
        xtype: 'datefield',
        fieldLabel: 'Hasta',
        format: 'd-m-Y',
        //minValue: new Date(),
        allowBlank: false
    }, {
        id: '3_DiasOtorgados',
        name: 'diasotorgados',
        xtype: 'textfield',
        fieldLabel: 'Dias otorgados',
        readOnly: 'true'
        
    }, {
        id: '3_Causa',
        name: 'causa',
        xtype: 'textfield',
        fieldLabel: 'Causa',
        maskRe: /[a-zA-Z\s]+/
    }],

    buttons: [{
    	text: 'Guardar',
        action: 'guardar',
        iconCls: 'save',
        scale: 'large'
    }/*, {
    	text: 'Limpiar',
        action: 'limpiar',
        iconCls: 'clear',
        scale: 'large'
    }*/],

	initComponent: function() {

		this.callParent(arguments);
	}
});