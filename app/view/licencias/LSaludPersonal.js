/***

Licencias por Salud Personal - Cod 1

20 dias anuales y 3 por mes maximo. Si pasa los 8 dias ya corresponde a largo tratamiento

***/

Ext.define('personal.view.licencias.LSaludPersonal', {
	extend: 'Ext.form.Panel',
	alias: 'widget.LSaludPersonal',
	//store: 'PersonalStore',
	title: 'Licencia por Salud - Personal',
    border: false,
    flex: 1,
    defaultType: 'textfield',
    trackResetOnLoad: true,
    bodyPadding: 5,
    fieldDefaults: {
       	anchor: '90%',
       	labelAlign: 'top'
    },
    config: {
        diasLic: 20
    },
    
    items: [{
                id: '1_Disponibles',
                name: 'disponibles',
                xtype: 'textfield',
                fieldLabel: 'Dias tomados',
                readOnly: true
            }, {
                id: '1_Desde',
                name: 'desde',
                xtype: 'datefield',
                fieldLabel: 'Desde',
                format: 'd-m-Y',
                submitFormat: 'd-m-Y',
                
                value: new Date(),
                allowBlank: false
            }, {
                id: '1_Hasta',
                name: 'hasta',
                xtype: 'datefield',
                fieldLabel: 'Hasta',
                format: 'd-m-Y',
                submitFormat: 'd-m-Y',
                //minValue: new Date(),
                allowBlank: false
            }, {
                id: '1_DiasOtorgados',
                name: 'diasotorgados',
                xtype: 'textfield',
                fieldLabel: 'Dias otorgados',
                readOnly: true,
                allowBlank: false
            }, {
                id: '1_Causa',
                name: 'causa',
                xtype: 'textfield',
                fieldLabel: 'Causa',
                maskRe: /[a-zA-Z\s]+/
            }],

    buttons: [{
    	text: 'Guardar',
        action: 'guardar',
        id: '1_btnGuardar',
        iconCls: 'save',
        scale: 'large'
        
    }/*, {
        id: '1_btnlimpiar',
    	text: 'Limpiar',
        action: 'limpiar',
        iconCls: 'clear',
        scale: 'large'
    }*/],

	initComponent: function() {
           
		this.callParent(arguments);
	}
        
});