Ext.define('personal.view.licencias.LicenciasController', {
	extend: 'Ext.app.ViewController',
	alias: 'controller.licencias',
	views: [
		
		'PersonalFichaLicencias',
		'licencias.PersonalCompensatoria',
		'licencias.LCompensatoria',
		'licencias.LSaludPersonal',
		'Feriados',
        'licencias.MasivasCompensatorias',
        'PersonalFichaComisiones',
        'licencias.LComision',
        'licencias.editarLComision',
        'SolicitudLicencia',
        'ModificarSolicitudLicencia',
        'ConfirmarSolicitudLicencia'

	],

	stores: [
		'PersonalStore',
		'historial.HCompensatoriaStore',
        'FeriadosStore',
        'licencias.CompensatoriaMasivaStore',
        'AgentesStore',
        'licencias.ComisionStore',
        'LicenciaTipoStore',
        'historial.HSolicitudLicenciaStore'
	],

	models:[
		'Persona',
		'historial.HCompensatoria',
		//'licencias.LCompensatoria',
		'licencias.LSaludPersonal',
        'Feriado',
        'licencias.CompensatoriaMasiva',
        'Agentes',
        'licencias.Comision',
        'LicenciaTipo',
        'historial.HSolicitudLicencia'
	],

	refs: [{
		ref: 'Personalcompensatoria',
		selector: 'licencias.PersonalCompensatoria'
	}, {
		ref: 'saludPersonal',
		selector: 'licencias.LSaludPersonal'
	}],

	init: function(){
		console.log('*** Controlador Licencias inicializados!');

		this.control({
			
		});
	},

	clearForm: function(button) {
		console.log('*** Limpiar form: ');
                button.up('form').getForm().reset();
                
	},
        
        contarDiasLaborales: function(pInicio, pFin, pDiasHabiles, pMismoAnio)
        {
            // pInicio: fecha de inicio para contar
            // pFin: fecha de fin hasta donde contar
            // pDiasHabiles: si son dias habiles o corridos, default: true (habiles) 
            // Todo debe ser dentro del mismo aÃ±o, salvo excepciones

            //Primero que todo, controlo que ambos campos esten completos, si no no hago nada. Bug corregido Mauro
            if(pInicio !== "" && pInicio !== null && pFin !== "" && pFin !== null)
            {
                var pHoy = new Date();
                
                //Obtengo el numero de días redondeado, sin contar horas, minutos, segundos ni milisegundos. 
                var pHoy_redond = Math.floor(pHoy.getTime()/1000/60/60/24);
                var pInicio_redond = Math.floor(pInicio.getTime()/1000/60/60/24);
                var pFin_redond = Math.floor(pFin.getTime()/1000/60/60/24);
                /*console.log("Hoy: "+pHoy_redond);
                console.log("Inicio: "+pInicio_redond);
                console.log("Fin: "+pFin_redond);*/
                
                //Luego controlo que el rango sea correcto. Bug corregido Mauro
                //if(pInicio_redond >= pHoy_redond && pFin_redond >= pHoy_redond)
                //{
                if(pInicio_redond>pFin_redond)
                {
                    Ext.Msg.alert('Error', 'Rango incorrecto de fechas');
                    return 0;
                }
                else
                {
                    //pMismoAnio = pMismoAnio || true; // Inicializar el parametro
                    var dia = 86400;
                    var inicio = new Date(pInicio).getTime() / 1000;
                    var fin = new Date(pFin).getTime() / 1000;

                    var feriados = AppGlobals.feriados;

                    var dt1 = new Date(inicio * 1000);
                    var dt2 = new Date(fin * 1000);
                    
                    //Si es true, controla que los dias no pasen de un año a otro, o sea, que sean en un mismo año
                    if(pMismoAnio)
                    {
                        if (dt1.getYear() != dt2.getYear()) 
                        {
                            console.log(dt1);        
                            console.log(dt1.getYear());
                            console.log(dt2);
                            console.log(dt2.getYear());
                            Ext.Msg.alert('Informacion', 'Las fechas deben ser dentro del mismo aÃ±o');
                            return 0;
                        }
                    }    

                    var anio_actual = new Date().getFullYear();
                    var anio_licencia = dt1.getFullYear();
                    var arreglo_feriados = new Array();

                    Ext.Ajax.request({
		            url: 'app/proxy.feriados.php',
		            async: false,
		            params: {
		                tarea: 'diasLaborales',
		                method: 'GET',
		                anio: anio_licencia
		            },
		            success: function(response){
		                var obj = Ext.decode(response.responseText);
		                console.log("hola");
		                console.log(obj);
		                for (i=0; i < obj.results.length; i++) 
		                {
				            arreglo_feriados.push(obj.results[i].mes+'-'+obj.results[i].dia);
				        }
		                
		            },
		            failure: function(response, opts) {
		                Ext.Msg.alert('Error', 'No se pudo conectar a la base de datos para cargar los feriados del a&ntilde;o actual. Error de servidor. <br>Estado: ' + response.status);
		                console.log('server-side failure with status code ' + response.status);
		            }
		        	});
                    console.log(arreglo_feriados);
                    var _mes;
                    var _dia;
                    var total_dias = 0;
                    var diaspasados = 0;
                    while( inicio <= fin) 
                    {
                        var date = new Date(inicio * 1000);
                        _mes = date.getMonth() + 1; //getMonth devuelve el mes empezando por 0
                        _dia = date.getDate(); //getDate devuelve el dia del mes       
                        mes_dia = _mes + '-' + _dia;
                        dia_semana = date.getDay();
                        //console.log(mes_dia + ' # ' + dia_semana + ' | ' + Ext.Array.indexOf(feriados, mes_dia));
                        if (pDiasHabiles) 
                        {
                            if ( (Ext.Array.indexOf(arreglo_feriados, mes_dia) == -1 ) && (dia_semana > 0 && dia_semana < 6)) 
                        	{ 
                        		diaspasados ++; 
                        	}
                        } 
                        else 
                        {
                            diaspasados++;	
                            //if ( (Ext.Array.indexOf(feriados, mes_dia) == -1 ) && (dia_semana > 0 && dia_semana < 6)) { diaspasados ++; }
                        }

                        inicio = inicio + dia;
                        total_dias ++;
                    }
                    console.log(pDiasHabiles);
                    console.log('Total de dias: ' + total_dias + ' Laborables: ' + diaspasados);

                    return diaspasados;
                }//end else
                //}//end if(pIn >= hoy && pFi >= hoy)
                /*else  //Comentado porque Carlos Rollan quiere cargar licencias anteriores en caso de 
                {
                    Ext.Msg.alert("Error", "Las fechas tienen que ser desde hoy en adelante");
                    return 0;
                }*/
            } //end if(pInicio !== "" && pInicio !== null && pFin !== "" && pFin !== null)
        },
        refrescarHistStores: function()
        {
            Ext.getStore('historial.HLicenciaStore').reload();
            Ext.getStore('historial.HMaternidadStore').reload();
            //Ext.getStore('historial.HEstudioStore').reload();
        },
        
	diasDisponibles: function(item) {
		
		AppGlobals.diasDisponibles = 0;
        switch(item)
        {
        	case 10://Compensatoria
        		var	tarea = 'diasDisponiblesCompensatoria';
        		break;
        	case 18: //Doble turno
        		var	tarea = 'diasDisponiblesDobleTurno';
        		break;
        	default:
        		var tarea = 'diasDisponibles';
        		break;	
        }        
		Ext.Ajax.request(
		{
			url: 'app/proxy.licencias.php',
			params: {
				tarea: tarea,
				tipoLic: item,
				legajo: AppGlobals.legAgente
			},
			success: function(response, opts) 
			{
				var obj = Ext.decode(response.responseText);
				if(obj.success==true)
                {
                	console.log("Success: "+response.responseText);
					var tomados = parseInt(obj.diasTomados);
					var disponibles = obj.diasDisponibles==""?"":parseInt(obj.diasDisponibles);
					var totalDias = typeof obj.totalDias!='undefined'?parseInt(totalDias):0;
					Ext.getCmp('disponibles').setValue(disponibles);
					Ext.getCmp('tomados').setValue(tomados);
					AppGlobals.diasDisponibles = disponibles;
					AppGlobals.diasLicPorAnio = totalDias;
                	_myApp.getController('Licencias').refrescarHistStores();
                }
            	else
            	{
            		Ext.Msg.alert('Error', obj.error);
            	}
    		},
    		failure: function(response, opts) 
    		{
    			Ext.Msg.alert('Error', 'Error de servidor. <br>Estado: ' + response.status);
        		console.log('server-side failure with status code ' + response.status);
    		}
		});
	},

	saveLicencia: function(frm, tipolic, button) 
	{
        console.log("Entra al savelicencia");
        var form = frm;
        var me = this;
        
        if(frm.isValid())
        {
			frm.submit({
			url: 'app/proxy.licencias.php',
			method: 'POST',
			params: {
				tarea: 'saveLicencia',
				tipoLic: tipolic,
				legajo: AppGlobals.legAgente
				},
			success: function(frm, action) 
			{
				_myApp.getController('Licencias').diasDisponibles(tipolic);
	            //console.log("***Success: "+action.response.responseText);
	            /*if(tipolic != 3 && tipolic != 4 && tipolic != 5 && tipolic != 6 && tipolic != 7 && tipolic != 9 && tipolic != 14 && tipolic != 17) //Bug corregido Mauro
	            {
	                
	                _myApp.getController('Licencias').diasDisponibles(tipolic);
	            }*/
				Ext.example.msg('Informaci&oacute;n','Se guard&oacute; la licencia en el sistema');
	            button.setDisabled(false); //Activo el botón de guardar nuevamente
	            button.setText("Guardar");
	            //_myApp.getController('Licencias').refrescarHistStores();
	            //frm.doLayout();
	            
	            _myApp.getController('Licencias').reiniciarFormLicencias(tipolic);
	            //Vuelvo el panel a cero para ocultar el formulario de carga
	            var cardLic = Ext.getCmp('pnlLicenciasCard');
	            //cardLic.getLayout().setActiveItem('cardZero'); 
	        },
			failure: function(frm, action) 
			{
	            console.log("***Failure: "+action.response.responseText);
	            var obj = Ext.decode(action.response.responseText);
	            console.log(obj);
				switch (action.failureType) 
				{
	        		case Ext.form.action.Action.CLIENT_INVALID:
	            		Ext.Msg.alert('Error', 'Complete los campos con valores v&aacute;lidos.');
	            		break;
	        		case Ext.form.action.Action.CONNECT_FAILURE:
	            		Ext.Msg.alert('Error', 'Error en la comunicacion con la base de datos. Recargue la p&aacute;gina (F5) e intente nuevamente. ('+action.response.statusText+')');
	            		break;
	        		case Ext.form.action.Action.SERVER_INVALID:
	           			Ext.Msg.alert('Error', action.result.msg);
	                    default:
	                            Ext.Msg.alert('Error', obj.error);
	   			}
	            button.setDisabled(false);//Activo el botón de guardar nuevamente
	            button.setText("Guardar");
			}
	});
        
        }
        else
        {
            Ext.Msg.alert('Error', 'Complete los campos con valores v&aacute;lidos.');
            button.setDisabled(false);//Activo el botón de guardar nuevamente
            button.setText("Guardar");
        }
             
	},
        
        
	reiniciarFormLicencias: function(index)
	{
		console.log("Entra a reiniciarFormLicencias");
		//Ext.getCmp(index + '_Desde').setValue(new Date());
		Ext.getCmp('hasta').setValue("");
		Ext.getCmp('diasLicencia').setValue("");
		Ext.getCmp('observaciones').setValue("");
		Ext.getCmp('causa').setValue("");
		Ext.getCmp('expediente').setValue("");
		switch(index)
		{
			case 5: //Maternidad
			{
				Ext.getCmp('fpp').setValue("");
				Ext.getCmp('fp').setValue("");
				Ext.getCmp('lactahasta').setValue("");
				Ext.getCmp('lactahorario').setValue("");
				break;
			}
			case 12: //Estudio
			{
				Ext.getCmp('12_asignatura').setValue("");
				Ext.getCmp('12_carrera').setValue("");
				Ext.getCmp('12_fecha_examen').setValue("");
				break;
			}
		}
	},
        
	onClickLicencia: function(t, record, item, index, ex, eOpts)
	{
		console.log('*** Click en licencia'+index);
		AppGlobals.licenciaSeleccionadaAlta = index;
		_myApp.getController('Licencias').reiniciarFormLicencias(index);
        // Muestra el panel de la lic seleccionada
		var pnl = Ext.getCmp('pnlLicenciasCard');

		//Obtengo el nombre de la licencia
        var nombre_licencia = "Alta licencia ";
        Ext.Ajax.request(
        {      
            url : 'app/proxy.licencias.php',
            method: 'GET',
            async: false,
            params :{
                idLic : index,
                tarea: 'getTipoLicenciaById'
            },
            
            success : function(response) 
            {
                var obj = Ext.decode(response.responseText);
                nombre_licencia+= obj.results[0].nombre;
                AppGlobals.licenciaSeleccionadaAltaCorridos = obj.results[0].dias_corridos;
                AppGlobals.licenciaSeleccionadaMismoAnio = obj.results[0].mismo_anio;
                console.log("Dias habiles o corridos: "+AppGlobals.licenciaSeleccionadaAltaCorridos);
                var checked = AppGlobals.licenciaSeleccionadaAltaCorridos==1?false:true;
                Ext.getCmp('habiles').setValue(checked);
            },
            failure: function(response)
            {
                //var obj = Ext.decode(response.responseText);
                console.log("Entra al failure "+response.responseText);
                Ext.example.msg("Error", "Error al guardar cambios: "+obj.error);
            }
        });
		switch (index) 
		{
			case 5: //Maternidad
				pnl.getLayout().setActiveItem(2);
				break;
			case 12://Estudio
				pnl.getLayout().setActiveItem(3);
				break;
			default: //Todas las demas usan el mismo grid para mostrarse
                pnl.getLayout().setActiveItem(1);
                pnl.getLayout().activeItem.header.setTitle(nombre_licencia);
                break;
                
        }
        
        AppGlobals.diasDisponibles = 0;
		
		switch(index)
		{
			case 10: //Compensatoria
				var tarea = 'diasDisponiblesCompensatoria';
				break;
			case 18: //Doble Turno
				var tarea = 'diasDisponiblesDobleTurno';
				break;
			default://Todas las demas
				var tarea = 'diasDisponibles';
				break;		
		}

		Ext.Ajax.request({
			url: 'app/proxy.licencias.php',
			params:
			{
				tarea: tarea,
				tipoLic: index,
				legajo: AppGlobals.legAgente
			},
			success: function(response)
			{
				console.log("Success: "+response.responseText);
				var obj = Ext.decode(response.responseText);
				var tomados = parseInt(obj.diasTomados);
				var disponibles = obj.diasDisponibles==""?"":parseInt(obj.diasDisponibles);
				var totalDias = typeof obj.totalDias!='undefined'?parseInt(totalDias):0;
				switch(index)
				{
					case 5: //Maternidad
						break;
					case 12: //Estudio
						Ext.getCmp('12_disponibles').setValue(disponibles);
						Ext.getCmp('12_tomados').setValue(tomados);
						break;
					default:
						Ext.getCmp('disponibles').setValue(disponibles);
						Ext.getCmp('tomados').setValue(tomados);
						break;
				}
				
				AppGlobals.diasDisponibles = disponibles;
				AppGlobals.diasLicPorAnio = totalDias;

				switch(index)
				{
					case 10: //Compensatorias, acá SI bloqueo si se ha tomado todos los dias
					{
						if(disponibles<=0)
						{
							Ext.Msg.alert('Informaci&oacute;n', 'No tiene dias disponibles, cargue compensatoria primero');
							Ext.getCmp('desde').setReadOnly(true);
							Ext.getCmp('hasta').setReadOnly(true);
							Ext.getCmp('causa').setReadOnly(true);
						}
						else
						{
							Ext.getCmp('desde').setReadOnly(false);
							Ext.getCmp('hasta').setReadOnly(false);
							Ext.getCmp('causa').setReadOnly(false);
						}
						break;
					}
					case 18: //Doble Turno, aca si bloqueo si se ha tomado todos los dias
					{
						if(disponibles<=0)
						{
							Ext.Msg.alert('Informaci&oacute;n', 'No tiene dias disponibles, cargue dias trabajados doble turno primero');
							Ext.getCmp('desde').setReadOnly(true);
							Ext.getCmp('hasta').setReadOnly(true);
							Ext.getCmp('causa').setReadOnly(true);
						}
						else
						{
							Ext.getCmp('desde').setReadOnly(false);
							Ext.getCmp('hasta').setReadOnly(false);
							Ext.getCmp('causa').setReadOnly(false);
						}
						break;
					}
					default:
					{
						if(tomados>totalDias) //Si se tomó mas dias que los disponbiles, largo una alerta pero igual dejo seguir cargando.
						{
							Ext.Msg.alert('Advertencia', 'Atenci&oacute;n, el agente ya se tom&oacute; todos los d&iacute;as que ten&iacute;a disponibles en el año');
						}
						break;
					}
				} //end //end switch(index)
			}, //End success
			failure: function(response)
			{
				console.log("Failure: "+response.responseText);
				Ext.Msg.alert('Informacion', 'Error de servidor. <br>Estado: ' + response.status);
				console.log('server-side failure with status code ' + response.status);
			}
		});
	},



	/* Ventana de Feriados  */
        
	onAddFeriado: function(button)
	{
		console.log('*** Add Feriado');
		var grid = button.up('window').down('propertygrid');
		console.log(grid.source);
    },

	onClearFeriados: function(button)
	{
		console.log('*** Clear Feriados');
	},
        
	onCancelaFeriados: function(button)
	{
		console.log('*** Cierra ventana de agregar feriados');
		Ext.ComponentQuery.query('Feriados')[0].close();
	},

	onSaveFeriados: function(button)
	{
		console.log('*** Save Feriados');
		console.log(button.up('gridFeriados'));

	},

	onEditFeriados: function(button)
	{
		console.log('*** Edit Feriados');
        var pnl = Ext.getCmp('pnlLicenciasCard');
		pnl.getLayout().setActiveItem('cardFeriados');
		var sto = Ext.getStore('FeriadosStore');
		console.log(sto);
		sto.load({
		   params: {
			   tarea: 'getFeriados'
		   }
		});
    },
        

	/* Ventana de Lic Compensatoria */

	onAddCompensatoria: function(button)
	{
		console.log('*** Click Agregar Compensatoria');
		var win = Ext.widget('PersonalCompensatoria');
		win.show();
		Ext.getCmp("legajo").setValue(AppGlobals.legAgente);
    },


	onSaveCompensatoria: function (button)
	{
		console.log('*** Save Compensatoria');
		var frm = button.up('window').down('form').getForm();
                 
		if (frm.isValid())
		{
			//Valido nuevamente que la cantidad de días sea la correcta (de 1 a 31 dias de licencia)
			if(Ext.getCmp('perComDias').getValue()>0 && Ext.getCmp('perComDias').getValue()<32)
			{
				Ext.Msg.show({
				title:'Guardar licencia',
				msg: 'Guardar licencia compensatoria desde el '+Ext.getCmp('perComDesde').getValue()+' hasta el '+Ext.getCmp('perComHasta').getValue()+' de enero de '+Ext.getCmp('perComAnio').getValue()+', total '+Ext.getCmp('perComDias').getValue()+' dias?',
				buttons: Ext.Msg.YESNOCANCEL,
				buttonText :
				{
					yes : 'Si',
					no : 'No',
					cancel : 'Cancelar'
				},
				icon: Ext.Msg.QUESTION,
				fn: function(buttonId){

					if(buttonId === 'yes')
					{
						button.setDisabled(true); //Desactivo el boton para que no haya multiples cargas. Bug corregido Mauro
						frm.submit({
						url: 'app/proxy.licencias.php',
						method: 'POST',
						params: {
								tarea: 'setDiasCompensatoria',
								legajo: AppGlobals.legAgente
						},
						success: function(frm, action) {
								console.log("Entro al success: "+action.response.responseText);
								Ext.example.msg('Informaci&oacute;n','D&iacute;as cargados correctamente');
								frm.reset();
								button.setDisabled(false); //Habilito nuevamente el boton
								Ext.ComponentQuery.query('PersonalCompensatoria')[0].close();
								_myApp.getController('Licencias').diasDisponibles(10); //Actualizo dias disponibles. Bug corregido Mauro
								//Desbloqueo los campos
								Ext.getCmp('10_Desde').setReadOnly(false);
								Ext.getCmp('10_Hasta').setReadOnly(false);
								Ext.getCmp('10_DiasOtorgados').setReadOnly(false);
								Ext.getCmp('10_Causa').setReadOnly(false);

						},
						failure: function(frm, action) {
							var obj = Ext.decode(action.response.responseText);
								console.log("Entro al failure: "+action.response.responseText);
								Ext.Msg.alert('Error', obj.error);
								button.setDisabled(false); //Habilito nuevamente el boton
						}
					});
					}// end if(buttonId === 'yes')
				}
			});
			}
		}
    },

    cancelaCompensatoria: function()
	{
        console.log('*** Cierra ventana de agregar compensatoria');
		Ext.ComponentQuery.query('PersonalCompensatoria')[0].close();
		//Ext.widget('PersonalCompensatoria').close();
	},
	
	/*Ventana de carga masiva de compensatorias */
	openWindowMasivaCompensatorias: function(button)
	{
		console.log('*** Click Agregar compensatorias masivamente');
		var win = Ext.widget('MasivasCompensatorias');
		win.show();
    },

    altaItemCompensatoriaMasiva: function(button)
    {
        console.log("Alta Item compensatoria masiva");
        Ext.getCmp('grillaMasivaCompensatorias').plugins[0].cancelEdit();
        Ext.getCmp('guardarCargaMasivaCompensatorias').setDisabled(false);
        var r = Ext.create('personal.model.licencias.CompensatoriaMasiva', 
        {
            id: 0,
            nombre_item: 'Coloque aqui el nombre',
            descripcion_item: 'Coloque aqui la descripcion'
        });
       	Ext.getStore('licencias.CompensatoriaMasivaStore').insert(0, r);
       	Ext.getCmp('grillaMasivaCompensatorias').plugins[0].startEdit(0, 0);
    },

    bajaItemCompensatoriaMasiva: function(button)
    {
        console.log("Baja Item compensatoria masiva");
        Ext.getCmp('guardarCargaMasivaCompensatorias').setDisabled(false);
        var sm = button.up('#grillaMasivaCompensatorias').getSelectionModel();
        var arr = sm.getSelection();
        
        sm.view.addRowCls(2, 'hidden');
        var sto = Ext.getStore('licencias.CompensatoriaMasivaStore');
        Ext.getCmp('grillaMasivaCompensatorias').plugins[0].cancelEdit();
        sto.remove(arr);
        console.log(sto.removed);
        
    },

    guardarCargaMasivaCompensatorias: function(button)
    {
    	console.log("Guardar cambios en la grilla");
    	Ext.getCmp('cargaMasivaLicenciasComp').setDisabled(false);
    	console.log(button.up().up());
    	var newRecords = button.up().up().getStore('licencias.CompensatoriaMasivaStore').getModifiedRecords();
        var sto = button.up().up().getStore('licencias.CompensatoriaMasivaStore');
        
        console.log(newRecords);
        //Si hubo modificaciones, guardo los cambios
        //Hago un arreglo de los registros eliminados
        var eliminados = sto.removed;
        //console.log(eliminados);
        var recordsToDelete = [];
        for(i=0;i<eliminados.length;i++)
        {
            //console.log(eliminados[i].internalId);
            recordsToDelete.push(Ext.apply({id:eliminados[i].internalId},eliminados[i].data));
        }
        recordsToDelete = Ext.encode(recordsToDelete);
        
        if(!Ext.isEmpty(newRecords) || !Ext.isEmpty(eliminados))
        {
            var recordsToSend = [];
            sto.each(function(record)
            {
                recordsToSend.push(record.data);
            });
            
            button.up().up().el.mask('Guardando…', 'x-mask-loading'); 
                            
            recordsToSend = Ext.encode(recordsToSend); 
            console.log(recordsToSend);
            //console.log("Tipo de datos: "+typeof recordsToSend);
            Ext.Ajax.request({      
            url : 'app/proxy.licencias.php',
            method: 'POST',
            params :{
                records : recordsToSend,
                del: recordsToDelete,
                tarea: 'setMasivasCompensatorias'
            },
            
            success : function(response) 
            {
                var obj = Ext.decode(response.responseText);
                console.log(response.responseText);
                
                button.up().up().el.unmask();
                button.up().up().getStore().commitChanges();
                Ext.example.msg("Informaci&oacute;n", "Cambios guardados correctamente: \n"+obj.nuevos+" licencia(s) nueva(s), \n"+obj.eliminados+" licencias(s) eliminada(s) y \n"+obj.modificados+" licencias(s) modificada(s)");
                
                //Recargo la grilla 
                Ext.getStore('licencias.CompensatoriaMasivaStore').reload();
            },
            failure: function(response)
            {
                //var obj = Ext.decode(response.responseText);
                console.log("Entra al failure "+response.responseText);
                button.up().up().el.unmask();
                button.up().up().getStore().commitChanges();
                Ext.example.msg("Error", "Error al guardar cambios: "+obj.error);
            }
            });
        }
        else
        {
            Ext.example.msg("Error", "No hay cambios para guardar");
        }
    },

    enviarCargaMasiva: function(button)
    {
    	console.log("enviar carga masiva");
    	var records = [];
    	var store = button.up().up().getStore('licencias.CompensatoriaMasivaStore');
    	store.each(function(rec){
    		records.push(rec.data);
    	})
    	records = Ext.encode(records);
    	console.log(typeof records);
    	if(records.length>0)
    	{
    		Ext.Msg.show({
	            title:'Guardar licencias',
	            msg: 'Desea enviar todas las licencias para su carga masiva? Una vez confirmado este paso, se proceder&aacute; a su carga y se borrar&aacute; el listado',
	            buttons: Ext.Msg.YESNO,
	            buttonText : 
		        {
		            yes : 'Si',
		            no : 'No'
		        },
	            icon: Ext.Msg.QUESTION,
	            fn: function(buttonId)
	            {
	                if(buttonId === 'yes')
	                {
	                    button.setDisabled(true); //Desactivo el boton para que no haya multiples cargas
	                    Ext.Ajax.request({
	                    url: 'app/proxy.licencias.php',
	                    method: 'POST',
	                    params: {
	                            tarea: 'sendMasivasCompensatorias',
	                            registros: records
	                    },
	                    success: function(response) 
	                    {
	                        console.log("Entro al success: "+response.responseText);
	                        var obj = Ext.decode(response.responseText);
	                        console.log(obj);
	                        Ext.example.msg('Informaci&oacute;n',obj.cargados+' licencias cargadas y '+obj.omitidos+' licencias omitidas por coincidencia de fechas.');
	                        button.setDisabled(false); //Habilito nuevamente el boton
	                        Ext.getCmp('cargaMasivaLicenciasComp').setDisabled(true); //Deshabilito el boton para subir
	                        Ext.getStore('licencias.CompensatoriaMasivaStore').reload();

	                    },
	                    failure: function(response) 
	                    {
	                        var obj = Ext.decode(response.responseText);
	                        console.log("Entro al failure: "+response.responseText);
	                        Ext.Msg.alert('Error', obj.error);
	                        button.setDisabled(false); //Habilito nuevamente el boton
	                    }
	                });
	                }// end if(buttonId === 'yes')
	            }
	        });
    	}
    	else
    	{
    		Ext.example.msg('Error','No hay licencias cargadas para enviar.');
    	}
    },

    getDatosAgente: function()
    {
    	console.log("cambio el combo");
    	Ext.getCmp('agregarAgenteComp').setDisabled(false);
    	console.log(Ext.getCmp('comboAgenteComp').getValue());

    },

    agregarAgenteComp: function()
    {
    	console.log("agrega agente a la carga masiva de compensatorias");
    	Ext.Ajax.request({
			url: 'app/proxy.licencias.php',
			method: 'GET',
			params: {
				tarea: 'getDatosAgenteById',
				idAgente: Ext.getCmp('comboAgenteComp').getValue()
			},
			success: function(response, opts) {
        		var obj = Ext.decode(response.responseText);
                var res = obj.results[0];
                console.log(res);
                var flagEmp=0;
                var sto = Ext.getStore('licencias.CompensatoriaMasivaStore');
                sto.each(function(record)
                {
                    if(record.data.legajo==res.legajo) 
                    {
                        flagEmp = 1;
                    }
                });
                if(flagEmp==0)
                {
                	Ext.getCmp('grillaMasivaCompensatorias').plugins[0].cancelEdit();
    		        Ext.getCmp('guardarCargaMasivaCompensatorias').setDisabled(false);
    		        var r = Ext.create('personal.model.licencias.CompensatoriaMasiva', 
    		        {
    		            id: 0,
    		            legajo: res.legajo,
    		            apellido_nombre: res.apellido+", "+res.nombre
    		        });
    		       	Ext.getStore('licencias.CompensatoriaMasivaStore').insert(0, r);
    		       	Ext.getCmp('grillaMasivaCompensatorias').plugins[0].startEdit(0, 0);
		        }
		        else
		        {
		        	Ext.example.msg("Error", "El agente ya se a&ntilde;adi&oacute; a la lista");
		        }
            },
    		failure: function(response, opts) 
    		{
    			Ext.Msg.alert('Informacion', 'Error de servidor. <br>Estado: ' + response.status);
        		console.log('server-side failure with status code ' + response.status);
    		}
		});
    },

    activateBtnBajaItemComp: function(sm, selections)
    {
        Ext.getCmp('grillaMasivaCompensatorias').down('#eliminaLicencia').setDisabled(selections.length == 0);
        //this.activateBtnCargarItems(sm, selections);
        
        //this.activateBtnModificarItem(sm, selections);
    },
    cancelaCompensatoriaMasiva: function() {
        console.log('*** Cierra ventana de agregar compensatorias masivas');
		Ext.ComponentQuery.query('MasivasCompensatorias')[0].close();
		//Ext.widget('PersonalCompensatoria').close();
	},
	/* Ventana de Lic Doble Turno */

	onAddDobleTurno: function(button) {
		console.log('*** Click Agregar Doble Turno');
		var win = Ext.widget('PersonalDobleTurno');
		win.show();
        Ext.getCmp("legajo").setValue(AppGlobals.legAgente);
                
	},


	onSaveDobleTurno: function (button) 
	{
		console.log('*** Save Doble Turno');
		var frm = button.up('window').down('form').getForm();
                 
		if (frm.isValid())
        {
            //Valido nuevamente que la cantidad de días sea la correcta (de 1 a 31 dias de licencia)
            if(Ext.getCmp('perDTDias').getValue()>0)
            {
                Ext.Msg.show({
                title:'Guardar licencia por doble turno',
                msg: 'Guardar licencia por doble turno desde el '+Ext.Date.format(Ext.getCmp('perDTDesde').getValue(),"d-m-Y")+' hasta el '+Ext.Date.format(Ext.getCmp('perDTHasta').getValue(),"d-m-Y")+', total '+Ext.getCmp('perDTDias').getValue()+' dias?',
                buttons: Ext.Msg.YESNOCANCEL,
                buttonText : 
		        {
		            yes : 'Si',
		            no : 'No',
		            cancel : 'Cancelar'
		        },
                icon: Ext.Msg.QUESTION,
                fn: function(buttonId)
                {
                    if(buttonId === 'yes')
                    {
                        button.setDisabled(true); //Desactivo el boton para que no haya multiples cargas. Bug corregido Mauro
                        frm.submit({
                        url: 'app/proxy.licencias.php',
                        method: 'POST',
                        params: {
                                tarea: 'setDiasDobleTurno',
                                legajo: AppGlobals.legAgente
                        },
                        success: function(frm, action) {
                                console.log("Entro al success: "+action.response.responseText);
                                Ext.example.msg('Informaci&oacute;n','D&iacute;as cargados correctamente');
                                frm.reset();
                                button.setDisabled(false); //Habilito nuevamente el boton
                                Ext.ComponentQuery.query('PersonalDobleTurno')[0].close();
                                _myApp.getController('Licencias').diasDisponibles(18); //Actualizo dias disponibles. Bug corregido Mauro
                        },
                        failure: function(frm, action) {
                            var obj = Ext.decode(action.response.responseText);
                                console.log("Entro al failure: "+action.response.responseText);
                                Ext.Msg.alert('Error', obj.error);
                                button.setDisabled(false); //Habilito nuevamente el boton
                        }
                    });
                    }// end if(buttonId === 'yes')
                }
            });
            }
		}
		else
		{
			Ext.Msg.alert("Error", "Complete correcatamente los campos");
		}
                
		
	},
    cancelaDobleTurno: function() {
    console.log('*** Cierra ventana de agregar doble turno');
	Ext.ComponentQuery.query('PersonalDobleTurno')[0].close();
	
	},

	/* Ventana agregar comision */

	onAgregarComision: function(button) {
		console.log('*** Click Agregar Comision');
		var win = Ext.widget('LComision');
		win.show();
        Ext.getCmp("legajoComision").setValue(AppGlobals.legAgente);
                
	},

	/* Ventana editar comision */

	onEditarComision: function(button) {
		console.log('*** Click editar Comision');
		var win = Ext.widget('editarLComision');
		win.show();
        //Relleno el formulario con los datos
        //var f = this.getPersonalModificarcapacitacion();
        //var frm = f.down('form').getForm();
        console.log(AppGlobals.recordEditComision[0].raw);
        Ext.getCmp("legajoEditarComision").setValue(AppGlobals.legAgente);
        Ext.getCmp('editarComisionDesde').setValue(AppGlobals.recordEditComision[0].raw.desde);
        Ext.getCmp('editarComisionHasta').setValue(AppGlobals.recordEditComision[0].raw.hasta);
        Ext.getCmp('editarComisionTotalDias').setValue(AppGlobals.recordEditComision[0].raw.total_dias);
        Ext.getCmp('editarComisionCausa').setValue(AppGlobals.recordEditComision[0].raw.causa);
        Ext.getCmp('editarComisionExpediente').setValue(AppGlobals.recordEditComision[0].raw.expediente);
        Ext.getCmp('editarComisionObservaciones').setValue(AppGlobals.recordEditComision[0].raw.observaciones);
    },

    onEliminarComision: function(button)
    {
    	console.log('*** Click eliminar Comision');
        
        console.log(AppGlobals.recordEditComision[0].raw.id);
        var sto = Ext.getStore('licencias.ComisionStore');                 
        Ext.Msg.show({
            title:'Eliminar comision',
            msg: 'Seguro que desea eliminar la comisi&oacute;n?',
            buttons: Ext.Msg.YESNOCANCEL,
            buttonText : 
            {
                yes : 'Si',
                no : 'No',
                cancel : 'Cancelar'
            },
            icon: Ext.Msg.QUESTION,
            fn: function(buttonId)
            {
                if(buttonId === 'yes')
                {
                    button.setDisabled(true); //Desactivo el boton para que no haya multiples cargas. Bug corregido Mauro
                    Ext.Ajax.request({
                    url: 'app/proxy.licencias.php',
                    method: 'POST',
                    params: {
                            tarea: 'eliminarComision',
                            id: AppGlobals.recordEditComision[0].raw.id,
                    },
                    success: function(action) 
                    {
                        var obj = Ext.decode(action.responseText);
                        if(obj.success)
                        {
                            Ext.example.msg('Informaci&oacute;n',obj.msg);
                        }
                        else
                        {
                            Ext.example.msg('Error',obj.error);
                        }
                        button.setDisabled(false); //Habilito nuevamente el boton
                        sto.reload();
                            
                    },
                    failure: function(action) 
                    {
                        var obj = Ext.decode(action.responseText);
                        console.log("Entro al failure: "+action.responseText);
                        Ext.Msg.alert('Error', obj.error);
                        button.setDisabled(false); //Habilito nuevamente el boton
                    }
                });
                }// end if(buttonId === 'yes')
            }
        });
        
    },
	/* Calcular cantidad de dias en funcion del periodo qu ese elige para la comision */
	
	onChangeComisionHasta: function(field, newValue, oldValue, eOpts) 
	{
		var fechaDesde = Ext.getCmp('comisionDesde').getValue();
        var fechaHasta = Ext.getCmp('comisionHasta').getValue();
        if(fechaDesde!="" && fechaDesde!=null && fechaHasta!="" && fechaHasta!=null)
        {
            var diasLaborables = this.contarDiasLaborales(Ext.getCmp('comisionDesde').getValue(), Ext.getCmp('comisionHasta').getValue(), false, true);//Bug corregido Mauro
            if(typeof (diasLaborables) != 'undefined') //Si es undefined, es porque no se ha calculado aun. Bug corregido Mauro
            {
                Ext.getCmp('comisionTotalDias').setValue(diasLaborables);
            }
        }
	},
	onChangeEditarComisionHasta: function(field, newValue, oldValue, eOpts) 
	{
		var fechaDesde = Ext.getCmp('editarComisionDesde').getValue();
        var fechaHasta = Ext.getCmp('editarComisionHasta').getValue();
        if(fechaDesde!="" && fechaDesde!=null && fechaHasta!="" && fechaHasta!=null)
        {
            var diasLaborables = this.contarDiasLaborales(Ext.getCmp('editarComisionDesde').getValue(), Ext.getCmp('editarComisionHasta').getValue(), false, true);//Bug corregido Mauro
            if(typeof (diasLaborables) != 'undefined') //Si es undefined, es porque no se ha calculado aun. Bug corregido Mauro
            {
                Ext.getCmp('editarComisionTotalDias').setValue(diasLaborables);
            }
        }
	},

	onSaveComision: function (button) 
	{
		console.log('*** Save Comision');
		var frm = button.up('window').down('form').getForm();
		var sto = Ext.getStore('licencias.ComisionStore');                 
		if (frm.isValid())
        {
            //Valido nuevamente que la cantidad de días sea la correcta (de 1 a 31 dias de licencia)
            if(Ext.getCmp('comisionTotalDias').getValue()>0)
            {
                Ext.Msg.show({
                title:'Guardar comisi&oacute;n',
                msg: 'Guardar comisi&oacute;n desde el '+Ext.Date.format(Ext.getCmp('comisionDesde').getValue(),"d-m-Y")+' hasta el '+Ext.Date.format(Ext.getCmp('comisionHasta').getValue(),"d-m-Y")+', total '+Ext.getCmp('comisionTotalDias').getValue()+' dias?',
                buttons: Ext.Msg.YESNOCANCEL,
                buttonText : 
		        {
		            yes : 'Si',
		            no : 'No',
		            cancel : 'Cancelar'
		        },
                icon: Ext.Msg.QUESTION,
                fn: function(buttonId)
                {
                    if(buttonId === 'yes')
                    {
                        button.setDisabled(true); //Desactivo el boton para que no haya multiples cargas. Bug corregido Mauro
                        frm.submit({
                        url: 'app/proxy.licencias.php',
                        method: 'POST',
                        params: {
                                tarea: 'saveComision',
                                legajo: AppGlobals.legAgente
                        },
                        success: function(frm, action) {
                                console.log("Entro al success: "+action.response.responseText);
                                Ext.example.msg('Informaci&oacute;n','Comisi&oacute;n cargada correctamente');
                                button.setDisabled(false); //Habilito nuevamente el boton
                                Ext.ComponentQuery.query('LComision')[0].close();
                                sto.reload();
                                
                        },
                        failure: function(frm, action) {
                            var obj = Ext.decode(action.response.responseText);
                                console.log("Entro al failure: "+action.response.responseText);
                                Ext.Msg.alert('Error', obj.error);
                                button.setDisabled(false); //Habilito nuevamente el boton
                        }
                    });
                    }// end if(buttonId === 'yes')
                }
            });
            }
            else
            {
            	Ext.Msg.alert("Error", "Complete correcatamente los campos");
            }
		}
		else
		{
			Ext.Msg.alert("Error", "Complete correcatamente los campos");
		}
                
		
	},

	onSaveEditarComision: function (button) 
	{
		console.log('*** Save Editar Comision');
		var frm = button.up('window').down('form').getForm();
        var sto = Ext.getStore('licencias.ComisionStore'); 
		if (frm.isValid())
        {
            //Valido nuevamente que la cantidad de días sea la correcta (de 1 a 31 dias de licencia)
            if(Ext.getCmp('editarComisionTotalDias').getValue()>0)
            {
                Ext.Msg.show({
                title:'Guardar comisi&oacute;n',
                msg: 'Guardar comisi&oacute;n desde el '+Ext.Date.format(Ext.getCmp('editarComisionDesde').getValue(),"d-m-Y")+' hasta el '+Ext.Date.format(Ext.getCmp('editarComisionHasta').getValue(),"d-m-Y")+', total '+Ext.getCmp('editarComisionTotalDias').getValue()+' dias?',
                buttons: Ext.Msg.YESNOCANCEL,
                buttonText : 
		        {
		            yes : 'Si',
		            no : 'No',
		            cancel : 'Cancelar'
		        },
                icon: Ext.Msg.QUESTION,
                fn: function(buttonId)
                {
                    if(buttonId === 'yes')
                    {
                        button.setDisabled(true); //Desactivo el boton para que no haya multiples cargas. Bug corregido Mauro
                        frm.submit({
                        url: 'app/proxy.licencias.php',
                        method: 'POST',
                        params: {
                                tarea: 'editComision',
                                idLicencia: AppGlobals.recordEditComision[0].raw.id

                        },
                        success: function(frm, action) {
                                console.log("Entro al success: "+action.response.responseText);
                                Ext.example.msg('Informaci&oacute;n','Comisi&oacute;n editada correctamente');
                                button.setDisabled(false); //Habilito nuevamente el boton
                                Ext.ComponentQuery.query('editarLComision')[0].close();
                                sto.reload();
                                
                        },
                        failure: function(frm, action) {
                            var obj = Ext.decode(action.response.responseText);
                                console.log("Entro al failure: "+action.response.responseText);
                                Ext.Msg.alert('Error', obj.error);
                                button.setDisabled(false); //Habilito nuevamente el boton
                        }
                    });
                    }// end if(buttonId === 'yes')
                }
            });
            }
            else
            {
            	Ext.Msg.alert("Error", "Complete correcatamente los campos");
            }
		}
		else
		{
			Ext.Msg.alert("Error", "Complete correcatamente los campos");
		}
                
		
	},
    
    cancelaComision: function() 
    {
	    console.log('*** Cierra ventana de agregar comision');
		Ext.ComponentQuery.query('LComision')[0].close();
	},

	cancelaEditarComision: function() 
    {
	    console.log('*** Cierra ventana de editar comision');
		Ext.ComponentQuery.query('editarLComision')[0].close();
	},

	selectComision: function (selectionModel, records)
	{
		console.log('*** Selecciona comision');
		Ext.getCmp('editarComision').setDisabled(!records.length);
        Ext.getCmp('quitarComision').setDisabled(!records.length);
        //Guardo el registro seleccionado, para rellenar la ventana con los datos si el usuario quiere editar la comision
        AppGlobals.recordEditComision = records;
        console.log(AppGlobals.recordEditComision);
    },
	/* ################################ LICENCIAS ################################*/

	/*** Licencias***/

	onChangeHasta: function(field, newValue, oldValue, eOpts) 
	{
		console.log("Pick date licencia");
		var fechaDesde = Ext.getCmp('desde').getValue();
        var fechaHasta = Ext.getCmp('hasta').getValue();
        var dias_habiles = AppGlobals.licenciaSeleccionadaAltaCorridos==1?false:true;
        var mismo_anio = AppGlobals.licenciaSeleccionadaMismoAnio==1?true:false;
        if(fechaDesde!="" && fechaDesde!=null && fechaHasta!="" && fechaHasta!=null)
        {
            var diasLaborables = this.contarDiasLaborales(Ext.getCmp('desde').getValue(), Ext.getCmp('hasta').getValue(), Ext.getCmp('habiles').getValue(), mismo_anio);
            if(typeof (diasLaborables) != 'undefined') //Si es undefined, es porque no se ha calculado aun. Bug corregido Mauro
            {
                if (diasLaborables > AppGlobals.diasDisponibles) 
                {
                    Ext.Msg.alert('Atencion', 'Esta a punto de cargar m&aacute;s d&iacute;as de los que tiene disponibles.'); 
                } 
                Ext.getCmp('diasLicencia').setValue(diasLaborables);
            }
        }
	},
        
	onSaveLicencia: function(button) 
    {
        var frm = button.up('form').getForm();
        //console.log(Ext.getCmp('1_DiasOtorgados').getValue());
        var diasLicencia = 0;
        switch(AppGlobals.licenciaSeleccionadaAlta)
        {
        	case 5://Maternidad
        		diasLicencia = Ext.getCmp('5_diasLicencia').getValue();
        		break;
        	case 12://Estudio
        		diasLicencia = Ext.getCmp('12_diasLicencia').getValue();
        		break;	
        	default:
        		diasLicencia = Ext.getCmp('diasLicencia').getValue();
        }

        if(diasLicencia == 0)
        {
            Ext.Msg.alert('Error', 'Los dias otorgados son incorrectos, cargue otra fecha');
        }
        else
        {
            button.setDisabled(true); //Desactivo el boton guardar para evitar multiples cargas
            button.setText("Espere...");
            console.log('*** clicked the Save button');
            this.saveLicencia(frm, AppGlobals.licenciaSeleccionadaAlta, button);
        }
    },
    /* Ventana agregar solicitud licencia */

    onAgregarSolicitudLicencia: function(button) {
        console.log('*** Click Agregar solicitud licencia');
        var win = Ext.widget('SolicitudLicencia');

        //Busco el domicilio del agente
        Ext.Ajax.request({
            url: 'app/proxy.agentes.php',
            method: 'GET',
            params: {
                    act: 'getAgenteByLegajo',
                    leg: AppGlobals.legAgente
            },
            success: function(action) 
            {
                var obj = Ext.decode(action.responseText);
                console.log(obj);
                Ext.getCmp("domicilioSolicitante").setValue(obj.results[0].domic);
            },
            failure: function(action) {
                var obj = Ext.decode(action.responseText);
                console.log("Entro al failure: "+action.responseText);
                Ext.example.msg('Error','No se pudo cargar el domicilio del agente. Por favor cargarlo manualmente.');
            }
        });
        Ext.getCmp("legajoSolicitudLicencia").setValue(AppGlobals.legAgente);
        win.show();
    },

    /* Ventana editar solicitud licencia */

    onEditarSolicitudLicencia: function(button) {
        console.log('*** Click editar solicitud licencia');
        var win = Ext.widget('ModificarSolicitudLicencia');
        var reg = Ext.getCmp('PersonalFichaSolicitudLicencias').getSelectionModel().getSelection();
        console.log(reg);
        win.show();
        //Relleno el formulario con los datos
        Ext.getCmp("modificarTipoLicencia").setValue(reg[0].data.id_tipo_licencia);
        Ext.getCmp('modificarLegajoSolicitudLicencia').setValue(AppGlobals.legAgente);
        Ext.getCmp('modificarSolicitudLicenciaDesde').setValue(reg[0].data.desde);
        Ext.getCmp('modificarSolicitudCausa').setValue(reg[0].data.causa);
        Ext.getCmp('modificarSolicitudExpediente').setValue(reg[0].data.expediente);
        Ext.getCmp('modificarSolicitudObservaciones').setValue(reg[0].data.observaciones);
        Ext.getCmp('modificarSolicitudSolicitante').setValue(reg[0].data.nombre_solicitante);
        Ext.getCmp('modificarDomicilioSolicitante').setValue(reg[0].data.direccion);
        //Ext.getCmp('modificarHoraSolicitud').setValue(reg[0].data.hora_solicitud);
        Ext.getCmp('modificarPierdePresentismoSolicitud').setValue(reg[0].raw.pierde_presentismo==1?true:false);
        
    },

    onEliminarSolicitudLicencia: function(button)
    {
        console.log('*** Delete solicitud licencia');
        var sto = Ext.getStore('historial.HSolicitudLicenciaStore');                 
        Ext.Msg.show({
                title:'Eliminar solicitud de licencia',
                msg: 'Seguro que desea eliminar la solicitud?',
                buttons: Ext.Msg.YESNOCANCEL,
                buttonText : 
                {
                    yes : 'Si',
                    no : 'No',
                    cancel : 'Cancelar'
                },
                icon: Ext.Msg.QUESTION,
                fn: function(buttonId)
                {
                    if(buttonId === 'yes')
                    {
                        button.setDisabled(true); //Desactivo el boton para que no haya multiples cargas. Bug corregido Mauro
                        Ext.Ajax.request({
                        url: 'app/proxy.licencias.php',
                        method: 'POST',
                        params: {
                                tarea: 'deleteSolicitudLicencia',
                                idSolicitud: AppGlobals.recordEditSolicitudLicencia[0].raw.id,
                                legajo: AppGlobals.legAgente
                        },
                        success: function(action) {
                                var obj = Ext.decode(action.responseText);
                                if(obj.success)
                                {
                                	Ext.example.msg('Informaci&oacute;n',obj.msg);
                                }
                                else
                                {
                                	Ext.example.msg('Error',obj.error);
                                }
                                button.setDisabled(false); //Habilito nuevamente el boton
                                sto.reload();
                                
                        },
                        failure: function(action) {
                            var obj = Ext.decode(action.responseText);
                                console.log("Entro al failure: "+action.responseText);
                                Ext.Msg.alert('Error', obj.error);
                                button.setDisabled(false); //Habilito nuevamente el boton
                        }
                    });
                    }// end if(buttonId === 'yes')
                }
            });
    },
    onConfirmarSolicitudLicencia: function(button)
    {
    	console.log('*** Click confirmar solicitud licencia');
    	var win = Ext.widget('ConfirmarSolicitudLicencia');
        var reg = Ext.getCmp('PersonalFichaSolicitudLicencias').getSelectionModel().getSelection();
        console.log(reg);
        win.show();
        
        //Obtengo los dias disponibles
    	Ext.Ajax.request({
			url: 'app/proxy.licencias.php',
			params:
			{
				tarea: 'diasDisponibles',
				tipoLic: reg[0].data.id_tipo_licencia,
				legajo: AppGlobals.legAgente
			},
			success: function(response)
			{
				console.log("Success dias disponibles: "+response.responseText);
				var obj = Ext.decode(response.responseText);
				var tomados = parseInt(obj.diasTomados);
				var disponibles = obj.diasDisponibles==""?"":parseInt(obj.diasDisponibles);
				var totalDias = typeof obj.totalDias!='undefined'?parseInt(totalDias):0;
				AppGlobals.diasDisponibles = disponibles;
				AppGlobals.diasLicPorAnio = totalDias;

				if(tomados>totalDias) //Si se tomó mas dias que los disponbiles, largo una alerta pero igual dejo seguir cargando.
				{
					Ext.Msg.alert('Advertencia', 'Atenci&oacute;n, el agente ya se tom&oacute; todos los d&iacute;as que ten&iacute;a disponibles en el año');
				}
			}, //End success
			failure: function(response)
			{
				console.log("Failure: "+response.responseText);
				Ext.Msg.alert('Informacion', 'Error de servidor. <br>Estado: ' + response.status);
				console.log('server-side failure with status code ' + response.status);
			}
		});
        //Relleno el formulario con los datos
        Ext.getCmp("confirmarTipoLicencia").setValue(reg[0].data.id_tipo_licencia);
        Ext.getCmp('confirmarLegajoSolicitudLicencia').setValue(AppGlobals.legAgente);
        Ext.getCmp('confirmarSolicitudLicenciaDesde').setValue(reg[0].data.desde);
        Ext.getCmp('confirmarSolicitudCausa').setValue(reg[0].data.causa);
        Ext.getCmp('confirmarSolicitudExpediente').setValue(reg[0].data.expediente);
        Ext.getCmp('confirmarSolicitudObservaciones').setValue(reg[0].data.observaciones);
        Ext.getCmp('confirmarSolicitudSolicitante').setValue(reg[0].data.nombre_solicitante);
        Ext.getCmp('confirmarDomicilioSolicitante').setValue(reg[0].data.direccion);
        //Ext.getCmp('confirmarHoraSolicitud').setValue(reg[0].data.hora_solicitud);
        Ext.getCmp('confirmarPierdePresentismoSolicitud').setValue(reg[0].raw.pierde_presentismo==1?true:false);
        Ext.getCmp('habiles_confirmar_licencia').setValue(reg[0].raw.dias_corridos==1?false:true);
        AppGlobals.confirmarLicenciaAltaCorridos = reg[0].raw.dias_corridos;
        console.log("Dias habiles o corridos: "+AppGlobals.confirmarLicenciaAltaCorridos);
        /*Ext.Ajax.request(
        {      
            url : 'app/proxy.licencias.php',
            method: 'GET',
            async: false,
            params :{
                idLic : reg[0].data.id_tipo_licencia,
                tarea: 'getTipoLicenciaById'
            },
            
            success : function(response) 
            {
                var obj = Ext.decode(response.responseText);
                AppGlobals.confirmarLicenciaAltaCorridos = obj.results[0].dias_corridos;
                console.log("Dias habiles o corridos: "+AppGlobals.confirmarLicenciaAltaCorridos);
            },
            failure: function(response)
            {
                //var obj = Ext.decode(response.responseText);
                console.log("Entra al failure "+response.responseText);
                Ext.example.msg("Error", "Error al guardar cambios: "+obj.error);
            }
        });*/
    },
    onSaveSolicitudLicencia: function (button) 
    {
        console.log('*** Save solicitud licencia');
        var frm = button.up('window').down('form').getForm();
        var sto = Ext.getStore('historial.HSolicitudLicenciaStore');                 
        if (frm.isValid())
        {
            Ext.Msg.show({
                title:'Guardar solicitud de licencia',
                msg: 'Guardar solicitud de licencia?',
                buttons: Ext.Msg.YESNOCANCEL,
                buttonText : 
                {
                    yes : 'Si',
                    no : 'No',
                    cancel : 'Cancelar'
                },
                icon: Ext.Msg.QUESTION,
                fn: function(buttonId)
                {
                    if(buttonId === 'yes')
                    {
                        button.setDisabled(true); //Desactivo el boton para que no haya multiples cargas. Bug corregido Mauro
                        frm.submit({
                        url: 'app/proxy.licencias.php',
                        method: 'POST',
                        params: {
                                tarea: 'saveSolicitudLicencia',
                                legajo: AppGlobals.legAgente
                        },
                        success: function(frm, action) {
                                console.log("Entro al success: "+action.response.responseText);
                                Ext.example.msg('Informaci&oacute;n','Solicitud cargada correctamente');
                                button.setDisabled(false); //Habilito nuevamente el boton
                                Ext.ComponentQuery.query('SolicitudLicencia')[0].close();
                                sto.reload();
                                
                        },
                        failure: function(frm, action) {
                            var obj = Ext.decode(action.response.responseText);
                                console.log("Entro al failure: "+action.response.responseText);
                                Ext.Msg.alert('Error', obj.error);
                                button.setDisabled(false); //Habilito nuevamente el boton
                        }
                    });
                    }// end if(buttonId === 'yes')
                }
            });
        }
        else
        {
            Ext.Msg.alert("Error", "Complete correcatamente los campos");
        }
    },

    cancelaSolicitudLicencia: function() 
    {
        console.log('*** Cierra ventana de agregar solicitud de licencia');
        Ext.ComponentQuery.query('SolicitudLicencia')[0].close();
    },

    cancelaEditarSolicitudLicencia: function() 
    {
        console.log('*** Cierra ventana de editar solicitud de licencia');
        Ext.ComponentQuery.query('ModificarSolicitudLicencia')[0].close();
    },
    cancelaConfirmarSolicitudLicencia: function() 
    {
        console.log('*** Cierra ventana de confirmar solicitud de licencia');
        Ext.ComponentQuery.query('ConfirmarSolicitudLicencia')[0].close();
    },
    selectSolicitudLicencia: function (selectionModel, records)
    {
        console.log('*** Selecciona solicitud licencia');
        if(AppGlobals.permisos[0].pesta_solicitud_licencias_editar==1)
        {
        	if(records.length)
        	{
        		if(records[0].data.estado!=1 && records[0].data.estado!=3)
	            {
	            	Ext.getCmp('editarSolicitudLicencia').setDisabled(!records.length);
	                Ext.getCmp('quitarSolicitudLicencia').setDisabled(!records.length);
	                Ext.getCmp('confirmarSolicitudLicencia').setDisabled(!records.length);
	            }
	            else
	            {
	            	Ext.getCmp('editarSolicitudLicencia').setDisabled(true);
	                Ext.getCmp('quitarSolicitudLicencia').setDisabled(true);
	                Ext.getCmp('confirmarSolicitudLicencia').setDisabled(true);
	            }
	        }
	        else
        	{
        		Ext.getCmp('editarSolicitudLicencia').setDisabled(true);
                Ext.getCmp('quitarSolicitudLicencia').setDisabled(true);
                Ext.getCmp('confirmarSolicitudLicencia').setDisabled(true);
            }
        }
        //Guardo el registro seleccionado, para rellenar la ventana con los datos si el usuario quiere editar la solicitud de licencia
        AppGlobals.recordEditSolicitudLicencia = records;
    },
    onSaveEditarSolicitudLicencia: function (button) 
    {
        console.log('*** Save edicion solicitud licencia');
        var frm = button.up('window').down('form').getForm();
        var sto = Ext.getStore('historial.HSolicitudLicenciaStore');                 
        if (frm.isValid())
        {
            Ext.Msg.show({
                title:'Guardar cambios en solicitud de licencia',
                msg: 'Guardar cambios en solicitud de licencia?',
                buttons: Ext.Msg.YESNOCANCEL,
                buttonText : 
                {
                    yes : 'Si',
                    no : 'No',
                    cancel : 'Cancelar'
                },
                icon: Ext.Msg.QUESTION,
                fn: function(buttonId)
                {
                    if(buttonId === 'yes')
                    {
                        button.setDisabled(true); //Desactivo el boton para que no haya multiples cargas. Bug corregido Mauro
                        frm.submit({
                        url: 'app/proxy.licencias.php',
                        method: 'POST',
                        params: {
                                tarea: 'editSolicitudLicencia',
                                idSolicitud: AppGlobals.recordEditSolicitudLicencia[0].raw.id,
                                legajo: AppGlobals.legAgente
                        },
                        success: function(frm, action) {
                                console.log("Entro al success: "+action.response.responseText);
                                Ext.example.msg('Informaci&oacute;n','Solicitud cargada correctamente');
                                button.setDisabled(false); //Habilito nuevamente el boton
                                Ext.ComponentQuery.query('ModificarSolicitudLicencia')[0].close();
                                sto.reload();
                                
                        },
                        failure: function(frm, action) {
                            var obj = Ext.decode(action.response.responseText);
                                console.log("Entro al failure: "+action.response.responseText);
                                Ext.Msg.alert('Error', obj.error);
                                button.setDisabled(false); //Habilito nuevamente el boton
                        }
                    });
                    }// end if(buttonId === 'yes')
                }
            });
        }
        else
        {
            Ext.Msg.alert("Error", "Complete correcatamente los campos");
        }
    },

    onSaveConfirmarSolicitudLicencia: function (button) 
    {
        console.log('*** Confirmacion de licencia');
        var frm = button.up('window').down('form').getForm();
        var sto = Ext.getStore('historial.HSolicitudLicenciaStore');                 
        if (frm.isValid() && Ext.getCmp('confirmarSolicitudTotalDias').getValue() != 0)
        {
        	Ext.Msg.show({
                title:'Confirmar la licencia',
                msg: 'Confirmar la licencia?',
                buttons: Ext.Msg.YESNOCANCEL,
                buttonText : 
                {
                    yes : 'Si',
                    no : 'No',
                    cancel : 'Cancelar'
                },
                icon: Ext.Msg.QUESTION,
                fn: function(buttonId)
                {
                    if(buttonId === 'yes')
                    {
                        button.setDisabled(true); //Desactivo el boton para que no haya multiples cargas. Bug corregido Mauro
                        frm.submit({
                        url: 'app/proxy.licencias.php',
                        method: 'POST',
                        params: {
                                tarea: 'confirmarSolicitudLicencia',
                                idSolicitud: AppGlobals.recordEditSolicitudLicencia[0].raw.id,
                                legajo: AppGlobals.legAgente
                        },
                        success: function(frm, action) {
                                console.log("Entro al success: "+action.response.responseText);
                                Ext.example.msg('Informaci&oacute;n','Solicitud cargada correctamente');
                                button.setDisabled(false); //Habilito nuevamente el boton
                                Ext.ComponentQuery.query('ConfirmarSolicitudLicencia')[0].close();
                                sto.reload();
                                
                        },
                        failure: function(frm, action) {
                            var obj = Ext.decode(action.response.responseText);
                                console.log("Entro al failure: "+action.response.responseText);
                                Ext.Msg.alert('Error', obj.error);
                                button.setDisabled(false); //Habilito nuevamente el boton
                        }
                    });
                    }// end if(buttonId === 'yes')
                }
            });
        }
        else
        {
            Ext.Msg.alert("Error", "Complete correcatamente los campos");
        }
    },

	onChangeSolicitudLicenciahasta: function(field, newValue, oldValue, eOpts) {
		var fechaDesde = Ext.getCmp('confirmarSolicitudLicenciaDesde').getValue();
        var fechaHasta = Ext.getCmp('confirmarSolicitudLicenciaHasta').getValue();
        var habiles = AppGlobals.confirmarLicenciaAltaCorridos==1?false:true;
        
        
        if(fechaDesde!="" && fechaDesde!=null && fechaHasta!="" && fechaHasta!=null)
        {
            var diasLaborables = this.contarDiasLaborales(Ext.getCmp('confirmarSolicitudLicenciaDesde').getValue(), Ext.getCmp('confirmarSolicitudLicenciaHasta').getValue(), Ext.getCmp('habiles_confirmar_licencia').getValue(), true);
            if(typeof diasLaborables !== "undefined") //Si es undefined, es porque no se ha calculado aun. Bug corregido Mauro
            {
                if (diasLaborables > AppGlobals.diasDisponibles) 
                {
                    Ext.Msg.alert('Atencion', 'Esta a punto de cargar m&aacute;s d&iacute;as de los que tiene disponibles.'); //Seteo nuevamente las fechas. Bug corregido Mauro

                } 
                Ext.getCmp('confirmarSolicitudTotalDias').setValue(diasLaborables);
            }
        }
	},
	/* Familiar */
	onChangeLSFhasta: function(field, newValue, oldValue, eOpts) {
		console.log('*** changeLSFhasta ');
		var fechaDesde = Ext.getCmp('2_Desde').getValue();
                var fechaHasta = Ext.getCmp('2_Hasta').getValue();
                if(fechaDesde!="" && fechaDesde!=null && fechaHasta!="" && fechaHasta!=null)
                {
                    var diasLaborables = this.contarDiasLaborales(Ext.getCmp('2_Desde').getValue(), Ext.getCmp('2_Hasta').getValue(), true, true);
                    if(typeof diasLaborables !== "undefined") //Si es undefined, es porque no se ha calculado aun. Bug corregido Mauro
                    {
                        if (diasLaborables > AppGlobals.diasDisponibles) 
                        {
                            Ext.Msg.alert('Atencion', 'Esta a punto de cargar m&aacute;s d&iacute;as de los que tiene disponibles.'); //Seteo nuevamente las fechas. Bug corregido Mauro

                        } 
                        Ext.getCmp('2_DiasOtorgados').setValue(diasLaborables);
                    }
                }
	},

	onSaveLSF: function(button) {
		console.log('*** Guardar pedido licencia familiar');
		var frm = button.up('form').getForm();
                if(Ext.getCmp('2_DiasOtorgados').getValue() == 0)
                {
                    Ext.Msg.alert('Error', 'Los dias otorgados son incorrectos, cargue otra fecha');
                }
                else
                {
                    button.setDisabled(true); //Desactivo el boton guardar para evitar multiples cargas
                    button.setText("Espere...");
                    console.log('*** clicked the Save button');
                    this.saveLicencia(frm, 2, button);
                }
		
	},

	/* Largo Tratamiento */
	onChangeLSLThasta: function(field, newValue, oldValue, eOpts) {
		console.log('*** changeLSLThasta ');
		var diasLaborables = this.contarDiasLaborales(Ext.getCmp('3_Desde').getValue(), Ext.getCmp('3_Hasta').getValue(), false, false);
                if(typeof diasLaborables !== "undefined") //Si es undefined, es porque no se ha calculado aun. Bug corregido Mauro
                {
                    Ext.getCmp('3_DiasOtorgados').setValue(diasLaborables);
                }
		
	},

	onSaveLSLT: function(button) {
		console.log('*** Guardar licencia largo tratamiento');
		var frm = button.up('form').getForm();
		if(Ext.getCmp('3_DiasOtorgados').getValue() == 0)
                {
                    Ext.Msg.alert('Error', 'Los dias otorgados son incorrectos, cargue otra fecha');
                }
                else
                {
                    button.setDisabled(true); //Desactivo el boton guardar para evitar multiples cargas
                    button.setText("Espere...");
                    console.log('*** clicked the Save button');
                    this.saveLicencia(frm, 3, button);
                }
	},

	/* Acc de trabajo */
	onChangeLSAhasta: function(field, newValue, oldValue, eOpts) {
		console.log('*** changeLSAhasta ');
		var diasLaborables = this.contarDiasLaborales(Ext.getCmp('4_Desde').getValue(), Ext.getCmp('4_Hasta').getValue(), false, false);
		if(typeof diasLaborables !== "undefined") //Si es undefined, es porque no se ha calculado aun. Bug corregido Mauro
                {
                    Ext.getCmp('4_DiasOtorgados').setValue(diasLaborables);
                }
	},

	onSaveLSA: function(button) {
		console.log('*** Guardar licencia accidente');
		var frm = button.up('form').getForm();
		if(Ext.getCmp('4_DiasOtorgados').getValue() == 0)
                {
                    Ext.Msg.alert('Error', 'Los dias otorgados son incorrectos, cargue otra fecha');
                }
                else
                {
                    button.setDisabled(true); //Desactivo el boton guardar para evitar multiples cargas
                    button.setText("Espere...");
                    console.log('*** clicked the Save button');
                    this.saveLicencia(frm, 4, button);
                }
	},


	/*** Maternidad ***/
    onChangeLMaternidadHasta: function(field, newValue, oldValue, eOpts) 
    {
		console.log('*** changeLMaternidadhasta ');
		var dias_habiles = AppGlobals.licenciaSeleccionadaAltaCorridos==1?false:true;
		var diasLaborables = this.contarDiasLaborales(Ext.getCmp('5_Desde').getValue(), Ext.getCmp('5_Hasta').getValue(), dias_habiles, false);
		if(typeof diasLaborables !== "undefined") //Si es undefined, es porque no se ha calculado aun. Bug corregido Mauro
		{
		    Ext.getCmp('5_diasLicencia').setValue(diasLaborables);
		}
	},
	onSaveLMaternidad: function(button) {
		console.log('*** Guardar licencia Maternidad');
		var frm = button.up('form').getForm();
		if(Ext.getCmp('5_diasLicencia').getValue() == 0)
        {
            Ext.Msg.alert('Error', 'Los dias otorgados son incorrectos, cargue otra fecha');
        }
        else
        {
            button.setDisabled(true); //Desactivo el boton guardar para evitar multiples cargas
            button.setText("Espere...");
            console.log('*** clicked the Save button');
            this.saveLicencia(frm, 5, button);
        }
	},


	/*** Nacimiento ***/
	onChangeLNacHasta: function(field, newValue, oldValue, eOpts) {
		console.log('*** changeLNacHasta ');
		var diasLaborables = this.contarDiasLaborales(Ext.getCmp('6_Desde').getValue(), Ext.getCmp('6_Hasta').getValue(), true);
		if(typeof diasLaborables !== "undefined") //Si es undefined, es porque no se ha calculado aun. Bug corregido Mauro
                {
                    Ext.getCmp('6_DiasOtorgados').setValue(diasLaborables);
                }
	},

	onSaveLNacimiento: function(button) {
		console.log('*** Guardar licencia por Nacimiento');
		var frm = button.up('form').getForm();
		if(Ext.getCmp('6_DiasOtorgados').getValue() == 0)
                {
                    Ext.Msg.alert('Error', 'Los dias otorgados son incorrectos, cargue otra fecha');
                }
                else
                {
                    button.setDisabled(true); //Desactivo el boton guardar para evitar multiples cargas
                    button.setText("Espere...");
                    console.log('*** clicked the Save button');
                    this.saveLicencia(frm, 6, button);
                }
	},


	/*** Fallecimiento ***/
	onChangeLFallecHasta: function(field, newValue, oldValue, eOpts) {
		console.log('*** changeLFallecHasta ');
        console.log('Habiles: '+Ext.getCmp('7_Habiles').getValue());
		var diasLaborables = this.contarDiasLaborales(Ext.getCmp('7_Desde').getValue(), Ext.getCmp('7_Hasta').getValue(), Ext.getCmp('7_Habiles').getValue(), true); //Envío el valor de habiles (true o false)
		Ext.getCmp('7_DiasOtorgados').setValue(diasLaborables);
	},
	onSaveLFallecimiento: function(button) {
		console.log('*** Guardar licencia por Fallecimiento');
		var frm = button.up('form').getForm();
		if(Ext.getCmp('7_DiasOtorgados').getValue() == 0)
        {
            Ext.Msg.alert('Error', 'Los dias otorgados son incorrectos, cargue otra fecha');
        }
        else
        {
            button.setDisabled(true); //Desactivo el boton guardar para evitar multiples cargas
            button.setText("Espere...");
            console.log('*** clicked the Save button');
            this.saveLicencia(frm, 7, button);
        }
	},


	/*** Particulares ***/
	onChangeLParticularHasta: function(field, newValue, oldValue, eOpts) {
		console.log('*** onChangeLParticularHasta');
		var diasLaborables = this.contarDiasLaborales(Ext.getCmp('8_Desde').getValue(), Ext.getCmp('8_Hasta').getValue(), true, true);
		if(typeof diasLaborables !== "undefined") //Si es undefined, es porque no se ha calculado aun. Bug corregido Mauro
                {
                    Ext.getCmp('8_DiasOtorgados').setValue(diasLaborables);
                }
	},

	onSaveLParticular: function(button) {
		console.log('*** Guardar licencia por Particular');
		var frm = button.up('form').getForm();
		if(Ext.getCmp('8_DiasOtorgados').getValue() == 0)
                {
                    Ext.Msg.alert('Error', 'Los dias otorgados son incorrectos, cargue otra fecha');
                }
                else
                {
                    button.setDisabled(true); //Desactivo el boton guardar para evitar multiples cargas
                    button.setText("Espere...");
                    console.log('*** clicked the Save button');
                    this.saveLicencia(frm, 8, button);
                }
	},

	/*** Sin Goce ***/
	onChangeLSinGoceHasta: function(field, newValue, oldValue, eOpts) {
		console.log('*** changeLSinGoceHasta ');
		var diasLaborables = this.contarDiasLaborales(Ext.getCmp('9_Desde').getValue(), Ext.getCmp('9_Hasta').getValue(), false, false);
		if(typeof diasLaborables !== "undefined") //Si es undefined, es porque no se ha calculado aun. Bug corregido Mauro
                {
                    Ext.getCmp('9_DiasOtorgados').setValue(diasLaborables);
                }
	},
	onSaveLSinGoce: function(button) {
		console.log('*** Guardar Lic Sin Goce');
		var frm = button.up('form').getForm();
		if(Ext.getCmp('9_DiasOtorgados').getValue() == 0)
                {
                    Ext.Msg.alert('Error', 'Los dias otorgados son incorrectos, cargue otra fecha');
                }
                else
                {
                    button.setDisabled(true); //Desactivo el boton guardar para evitar multiples cargas
                    button.setText("Espere...");
                    console.log('*** clicked the Save button');
                    this.saveLicencia(frm, 9, button);
                }
	},

	/*** Compensatorias ***/

	onChangeLComphasta: function() {
		console.log('*** changeLComphasta ');
		
		var fechaDesde = Ext.getCmp('10_Desde').getValue();
                var fechaHasta = Ext.getCmp('10_Hasta').getValue();
                if(fechaDesde!="" && fechaDesde!=null && fechaHasta!="" && fechaHasta!=null)
                {
                    var diasLaborables = this.contarDiasLaborales(Ext.getCmp('10_Desde').getValue(), Ext.getCmp('10_Hasta').getValue(), false, true); //Bug corregido Mauro
                    if(typeof diasLaborables !== "undefined") //Si es undefined, es porque no se ha calculado aun. Bug corregido Mauro
                    {
                        if (diasLaborables <= AppGlobals.diasDisponibles) 
                        {
                            Ext.getCmp('10_DiasOtorgados').setValue(diasLaborables);
                        } 
                        else 
                        {
                            Ext.Msg.alert('Atencion', 'No puede pedir mas dias de los disponibles.'); //Seteo nuevamente las fechas. Bug corregido Mauro
                            Ext.getCmp('10_Hasta').setValue("");
                            Ext.getCmp('10_DiasOtorgados').setValue(0);
                        }
                    }
                }
	},

	onSaveLComp: function(button) {
		console.log('*** Guardar lic compensatoria');
		var frm = button.up('form').getForm();
		if(Ext.getCmp('10_DiasOtorgados').getValue() == 0)
        {
            Ext.Msg.alert('Error', 'Los dias otorgados son incorrectos, cargue otra fecha');
        }
        else
        {
            button.setDisabled(true); //Desactivo el boton guardar para evitar multiples cargas
            button.setText("Espere...");
            console.log('*** clicked the Save button');
            this.saveLicencia(frm, 10, button);
        }
	},


	/*** Culturales ***/
	onChangeLCulturalesHasta: function(field, newValue, oldValue, eOpts) {
		console.log('*** changeLCulturalesHasta ');
		var diasLaborables = this.contarDiasLaborales(Ext.getCmp('11_Desde').getValue(), Ext.getCmp('11_Hasta').getValue(), true, true);
		if(typeof diasLaborables !== "undefined") //Si es undefined, es porque no se ha calculado aun. Bug corregido Mauro
                {
                    if (diasLaborables > AppGlobals.diasDisponibles) 
                    {
                        Ext.Msg.alert('Atencion', 'Esta a punto de cargar m&aacute;s d&iacute;as de los que tiene disponibles.'); 
                    }
                    Ext.getCmp('11_DiasOtorgados').setValue(diasLaborables);
                }
	},
	onSaveLCulturales: function(button) {
		console.log('*** Guardar Lic Culturales');
		var frm = button.up('form').getForm();
		if(Ext.getCmp('11_DiasOtorgados').getValue() == 0)
                {
                    Ext.Msg.alert('Error', 'Los dias otorgados son incorrectos, cargue otra fecha');
                }
                else
                {
                    button.setDisabled(true); //Desactivo el boton guardar para evitar multiples cargas
                    button.setText("Espere...");
                    console.log('*** clicked the Save button');
                    this.saveLicencia(frm, 11, button);
                }
	},


	/*** Estudio ***/
	onChangeLEstudioHasta: function(field, newValue, oldValue, eOpts) 
	{
		console.log('*** changeLEstudioHasta ');
		var dias_habiles = AppGlobals.licenciaSeleccionadaAltaCorridos==1?false:true;
		var diasLaborables = this.contarDiasLaborales(Ext.getCmp('12_Desde').getValue(), Ext.getCmp('12_Hasta').getValue(), dias_habiles, true);
        console.log(AppGlobals.diasDisponibles);
		if(typeof diasLaborables !== "undefined") //Si es undefined, es porque no se ha calculado aun. Bug corregido Mauro
        {
            if (diasLaborables > AppGlobals.diasDisponibles) 
            {
                Ext.Msg.alert('Atencion', 'Esta a punto de cargar m&aacute;s d&iacute;as de los que tiene disponibles.'); 
            }
            Ext.getCmp('12_diasLicencia').setValue(diasLaborables);
        }
	},
	onSaveLEstudio: function(button) {
		console.log('*** Guardar Lic Estudio');
		var frm = button.up('form').getForm();
		if(Ext.getCmp('12_DiasOtorgados').getValue() == 0)
        {
            Ext.Msg.alert('Error', 'Los dias otorgados son incorrectos, cargue otra fecha');
        }
        else
        {
            button.setDisabled(true); //Desactivo el boton guardar para evitar multiples cargas
            button.setText("Espere...");
            console.log('*** clicked the Save button');
            this.saveLicencia(frm, 12, button);
        }
	},


	/*** Gremiales ***/
	onChangeLGremialesHasta: function(field, newValue, oldValue, eOpts) {
		console.log('*** changeLGremialesHasta ');
		var diasLaborables = this.contarDiasLaborales(Ext.getCmp('13_Desde').getValue(), Ext.getCmp('13_Hasta').getValue(), true, true);
		if(typeof diasLaborables !== "undefined") //Si es undefined, es porque no se ha calculado aun. Bug corregido Mauro
                {
                    if (diasLaborables > AppGlobals.diasDisponibles) 
                    {
                        Ext.Msg.alert('Atencion', 'Esta a punto de cargar m&aacute;s d&iacute;as de los que tiene disponibles.'); 
                    }
                    Ext.getCmp('13_DiasOtorgados').setValue(diasLaborables);
                }
	},	
	onSaveLGremiales: function(button) {
		console.log('*** Guardar Lic Gremiales');
		var frm = button.up('form').getForm();
		if(Ext.getCmp('13_DiasOtorgados').getValue() == 0)
                {
                    Ext.Msg.alert('Error', 'Los dias otorgados son incorrectos, cargue otra fecha');
                }
                else
                {
                    button.setDisabled(true); //Desactivo el boton guardar para evitar multiples cargas
                    button.setText("Espere...");
                    console.log('*** clicked the Save button');
                    this.saveLicencia(frm, 13, button);
                }
	},


	/*** Electivas ***/
	onChangeLElectivasHasta: function(field, newValue, oldValue, eOpts) {
		console.log('*** changeLElectivasHasta ');
		var diasLaborables = this.contarDiasLaborales(Ext.getCmp('14_Desde').getValue(), Ext.getCmp('14_Hasta').getValue(), false, false);
		if(typeof diasLaborables !== "undefined") //Si es undefined, es porque no se ha calculado aun. Bug corregido Mauro
                {
                    Ext.getCmp('14_DiasOtorgados').setValue(diasLaborables);
                }
	},
	onSaveLElectivas: function(button) {
		console.log('*** Guardar Lic Electivas');
		var frm = button.up('form').getForm();
		if(Ext.getCmp('14_DiasOtorgados').getValue() == 0)
                {
                    Ext.Msg.alert('Error', 'Los dias otorgados son incorrectos, cargue otra fecha');
                }
                else
                {
                    button.setDisabled(true); //Desactivo el boton guardar para evitar multiples cargas
                    button.setText("Espere...");
                    console.log('*** clicked the Save button');
                    this.saveLicencia(frm, 14, button);
                }
	},


	/*** Matrimonio ***/
        
	onChangeLMatrimonioHasta: function(field, newValue, oldValue, eOpts) {
		console.log('*** changeLMatrimonioHasta ');
		var diasLaborables = this.contarDiasLaborales(Ext.getCmp('15_Desde').getValue(), Ext.getCmp('15_Hasta').getValue(), true, false);
		if(typeof diasLaborables !== "undefined") //Si es undefined, es porque no se ha calculado aun. Bug corregido Mauro
                {
                    if (diasLaborables > AppGlobals.diasDisponibles) 
                    {
                        Ext.Msg.alert('Atencion', 'Esta a punto de cargar m&aacute;s d&iacute;as de los que tiene disponibles.'); 
                    }
                    Ext.getCmp('15_DiasOtorgados').setValue(diasLaborables);
                }
	},
	onSaveLMatrimonio: function(button) {
		console.log('*** Guardar Lic por Matrimonio');
		var frm = button.up('form').getForm();
		if(Ext.getCmp('15_DiasOtorgados').getValue() == 0)
                {
                    Ext.Msg.alert('Error', 'Los dias otorgados son incorrectos, cargue otra fecha');
                }
                else
                {
                    button.setDisabled(true); //Desactivo el boton guardar para evitar multiples cargas
                    button.setText("Espere...");
                    console.log('*** clicked the Save button');
                    this.saveLicencia(frm, 15, button);
                }
	},


	/*** Deportivas ***/
	onChangeLDeportivasHasta: function(field, newValue, oldValue, eOpts) {
		console.log('*** changeLDeportivasHasta ');
		var diasLaborables = this.contarDiasLaborales(Ext.getCmp('16_Desde').getValue(), Ext.getCmp('16_Hasta').getValue(), false, true);
		if(typeof diasLaborables !== "undefined") //Si es undefined, es porque no se ha calculado aun. Bug corregido Mauro
                {
                    if (diasLaborables > AppGlobals.diasDisponibles) 
                    {
                        Ext.Msg.alert('Atencion', 'Esta a punto de cargar m&aacute;s d&iacute;as de los que tiene disponibles.'); 
                    }
                    Ext.getCmp('16_DiasOtorgados').setValue(diasLaborables);
                }
	},
	onSaveLDeportivas: function(button) {
		console.log('*** Guardar Lic Deportiva');
		var frm = button.up('form').getForm();
		if(Ext.getCmp('16_DiasOtorgados').getValue() == 0)
                {
                    Ext.Msg.alert('Error', 'Los dias otorgados son incorrectos, cargue otra fecha');
                }
                else
                {
                    button.setDisabled(true); //Desactivo el boton guardar para evitar multiples cargas
                    button.setText("Espere...");
                    console.log('*** clicked the Save button');
                    this.saveLicencia(frm, 16, button);
                }
	},


	/*** Extraordinarias ***/
	onChangeLExtraordinariaHasta: function(field, newValue, oldValue, eOpts) {
		console.log('*** changeLExtraordinariaHasta ');
		var diasLaborables = this.contarDiasLaborales(Ext.getCmp('17_Desde').getValue(), Ext.getCmp('17_Hasta').getValue(), false, false);
		if(typeof diasLaborables !== "undefined") //Si es undefined, es porque no se ha calculado aun. Bug corregido Mauro
        {
            Ext.getCmp('17_DiasOtorgados').setValue(diasLaborables);
        }
	},

	/* Doble turno  */
	onChangeLDobleTurnoHasta: function() {
		console.log('*** change Doble turno ');
		var fechaDesde = Ext.getCmp('18_Desde').getValue();
        var fechaHasta = Ext.getCmp('18_Hasta').getValue();
        if(fechaDesde!="" && fechaDesde!=null && fechaHasta!="" && fechaHasta!=null)
        {
            var diasLaborables = this.contarDiasLaborales(Ext.getCmp('18_Desde').getValue(), Ext.getCmp('18_Hasta').getValue(), false, false); //Bug corregido Mauro
            if(typeof diasLaborables !== "undefined") //Si es undefined, es porque no se ha calculado aun. Bug corregido Mauro
            {
                if (diasLaborables <= AppGlobals.diasDisponiblesDobleTurno) 
                {
                    Ext.getCmp('18_DiasOtorgados').setValue(diasLaborables);
                } 
                else 
                {
                    Ext.Msg.alert('Atencion', 'No puede pedir mas dias de los disponibles.'); //Seteo nuevamente las fechas. Bug corregido Mauro
                    Ext.getCmp('18_Hasta').setValue("");
                    Ext.getCmp('18_DiasOtorgados').setValue(0);
                }
            }
        }
	},

	/* Injustificada  */
	onChangeLInjustificadaHasta: function() {
		console.log('*** change Injustificada ');
		var fechaDesde = Ext.getCmp('19_Desde').getValue();
        var fechaHasta = Ext.getCmp('19_Hasta').getValue();
        if(fechaDesde!="" && fechaDesde!=null && fechaHasta!="" && fechaHasta!=null)
        {
            var diasLaborables = this.contarDiasLaborales(Ext.getCmp('19_Desde').getValue(), Ext.getCmp('19_Hasta').getValue(), true, false); //Bug corregido Mauro
            if(typeof diasLaborables !== "undefined") //Si es undefined, es porque no se ha calculado aun. Bug corregido Mauro
            {
                Ext.getCmp('19_DiasOtorgados').setValue(diasLaborables);
            }
        }
	},

	/* Religion  */
	onChangeLReligionHasta: function() {
		console.log('*** change religion ');
		var fechaDesde = Ext.getCmp('20_Desde').getValue();
        var fechaHasta = Ext.getCmp('20_Hasta').getValue();
        if(fechaDesde!="" && fechaDesde!=null && fechaHasta!="" && fechaHasta!=null)
        {
            var diasLaborables = this.contarDiasLaborales(Ext.getCmp('20_Desde').getValue(), Ext.getCmp('20_Hasta').getValue(), true, false); //Bug corregido Mauro
            if(typeof diasLaborables !== "undefined") //Si es undefined, es porque no se ha calculado aun. Bug corregido Mauro
            {
                Ext.getCmp('20_DiasOtorgados').setValue(diasLaborables);
            }
        }
	},

	/* Adopcion  */
	onChangeLAdopcionHasta: function()
	{
		console.log('*** change adopcion ');
		var fechaDesde = Ext.getCmp('21_Desde').getValue();
        var fechaHasta = Ext.getCmp('21_Hasta').getValue();
        if(fechaDesde!="" && fechaDesde!=null && fechaHasta!="" && fechaHasta!=null)
        {
            var diasLaborables = this.contarDiasLaborales(Ext.getCmp('21_Desde').getValue(), Ext.getCmp('21_Hasta').getValue(), false, false); //Bug corregido Mauro
            if(typeof diasLaborables !== "undefined") //Si es undefined, es porque no se ha calculado aun. Bug corregido Mauro
            {
                Ext.getCmp('21_DiasOtorgados').setValue(diasLaborables);
            }
        }
	},
	

	onChangeLDobleTurnoDias: function(field, newValue, oldValue, eOpts)
	{
		console.log('*** change Doble turno dias');
		var diasLaborables = this.contarDiasLaborales(Ext.getCmp('perDTDesde').getValue(), Ext.getCmp('perDTHasta').getValue(), false, false);
		if(typeof diasLaborables !== "undefined") //Si es undefined, es porque no se ha calculado aun. Bug corregido Mauro
        {
            Ext.getCmp('perDTDias').setValue(diasLaborables);
        }
	},

	onSaveLExtraordinaria: function(button) {
		console.log('*** Guardar Lic Extraordinaria');
		var frm = button.up('form').getForm();
		if(Ext.getCmp('17_DiasOtorgados').getValue() == 0)
        {
            Ext.Msg.alert('Error', 'Los dias otorgados son incorrectos, cargue otra fecha');
        }
        else
        {
            button.setDisabled(true); //Desactivo el boton guardar para evitar multiples cargas
            button.setText("Espere...");
            console.log('*** clicked the Save button');
            this.saveLicencia(frm, 17, button);
        }
	},

	onSaveLDobleTurno: function(button) {
		console.log('*** Guardar Lic Doble Turno');
		var frm = button.up('form').getForm();
		if(Ext.getCmp('18_DiasOtorgados').getValue() == 0)
        {
            Ext.Msg.alert('Error', 'Los dias otorgados son incorrectos, cargue otra fecha');
        }
        else
        {
            button.setDisabled(true); //Desactivo el boton guardar para evitar multiples cargas
            button.setText("Espere...");
            console.log('*** clicked the Save button');
            this.saveLicencia(frm, 18, button);
        }
	},

	onSaveLInjustificada: function(button) {
		console.log('*** Guardar Lic Injustificada');
		var frm = button.up('form').getForm();
		if(Ext.getCmp('19_DiasOtorgados').getValue() == 0)
        {
            Ext.Msg.alert('Error', 'Los dias otorgados son incorrectos, cargue otra fecha');
        }
        else
        {
            button.setDisabled(true); //Desactivo el boton guardar para evitar multiples cargas
            button.setText("Espere...");
            console.log('*** clicked the Save button');
            this.saveLicencia(frm, 19, button);
        }
	},

	onSaveLReligion: function(button) {
		console.log('*** Guardar Lic religion');
		var frm = button.up('form').getForm();
		if(Ext.getCmp('20_DiasOtorgados').getValue() == 0)
        {
            Ext.Msg.alert('Error', 'Los dias otorgados son incorrectos, cargue otra fecha');
        }
        else
        {
            button.setDisabled(true); //Desactivo el boton guardar para evitar multiples cargas
            button.setText("Espere...");
            console.log('*** clicked the Save button');
            this.saveLicencia(frm, 20, button);
        }
	},

	onSaveLAdopcion: function(button)
	{
		console.log('*** Guardar Lic adopcion');
		var frm = button.up('form').getForm();
		if(Ext.getCmp('21_DiasOtorgados').getValue() == 0)
        {
            Ext.Msg.alert('Error', 'Los dias otorgados son incorrectos, cargue otra fecha');
        }
        else
        {
            button.setDisabled(true); //Desactivo el boton guardar para evitar multiples cargas
            button.setText("Espere...");
            console.log('*** clicked the Save button');
            this.saveLicencia(frm, 21, button);
        }
	}

});
