Ext.define('personal.view.licencias.MasivasCompensatorias', {
	extend: 'Ext.window.Window',
	alias: 'widget.MasivasCompensatorias',
	title: 'Carga masiva de licencias compensatorias',
    id: 'idMasivasCompensatorias',
    frame: true,
    closeAction: 'destroy',
    modal: true,
    //layout: 'fit',
    layout: {
        type: 'vbox',       
        align: 'stretch',    
        padding: 5
    },
    width: 900,
    minHeight: 500,
    maxHeight: 1000,
    items: [],
    buttons: [{
        text: 'Cancelar',
        action: 'cancelar',
        iconCls: 'cancel2'
    }],
    initComponent: function() {
        
        var selModel = Ext.create('Ext.selection.CheckboxModel',{
                checkOnly: true,
                mode: 'MULTI'/*,
                renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
                {
                    var baseCSSPrefix = Ext.baseCSSPrefix;
                    metaData.tdCls = baseCSSPrefix + 'grid-cell-special ' + baseCSSPrefix + 'grid-cell-row-checker';
                    return record.get('id') == 1 ? '' : '<div class="' + baseCSSPrefix + 'grid-row-checker">&#160;</div>';
                }*/
            });

        var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
        clicksToMoveEditor: 1,
        autoCancel: false,
        listeners: {
        'canceledit': function(rowEditing, context) {
            
            // Si se agreg� un registro nuevo de forma local, y no se guard�, lo elimino cuando el usuario hace click en "Cancelar"
            selModel.deselect(context.record);
            if (context.record.phantom) {
                context.store.remove(context.record);
            }
            },
        'edit': function(rowEditing, context) {
            selModel.deselect(context.record);
            }
        }
        });

        var formPanel = Ext.create('Ext.form.Panel',{
            id: 'formMasivaComp',
            border: false,
            autoScroll: true,
            flex: 2,
            fieldDefaults: 
            {
                anchor: '90%',
                labelAlign: 'top'
            },
            defaultType: 'textfield',
            bodyPadding: 10,
            items: [
                {
                    id: 'comboAgenteComp',
                    xtype: 'combo',
                    name: 'agentesComp',
                    displayField: 'apNomLeg',
                    valueField: 'id',
                    fieldLabel: 'Seleccione agente (escribir para buscar por nombre, apellido o legajo)',
                    queryMode: 'remote',
                    store: 'AgentesStore',
                    validateOnChange: 'true',
                    validator: function(value){
                    if (!value)
                        return ("Seleccione agente");
                    else
                        return true;
                    },
                    anchor: '100%'
                }],//End items
            buttons: [{
                text: 'Agregar',
                action: 'agregarAgenteComp',
                id: 'agregarAgenteComp',
                iconCls: 'agregarComp',
                scale: 'medium',
                disabled: true
            }]
        });

        var gridItems = Ext.create('Ext.grid.Panel', 
        {
            store: 'licencias.CompensatoriaMasivaStore',
            id: 'grillaMasivaCompensatorias',
            alias: 'grillaMasivaCompensatorias',
            markDirty: true,
            flex: 6,
            renderTo: Ext.getCmp('idMasivasCompensatorias'),
            tbar: [{
                id: 'cargaMasivaLicenciasComp',
                text: 'Subir licencias',
                action: 'cargaMasivaLicenciasComp',
                iconCls: 'upload_16',
                tooltip: 'Realiza carga masiva de licencias compensatorias',
                disabled: true
            },'->', {
                id: 'eliminaLicencia',
                text: 'Eliminar item',
                iconCls: 'delete',
                tooltip: 'Dar de baja a un item existente.',
                action: 'eliminaLicencia',
                disabled: true
            },'-', {
                id: 'guardarCargaMasivaCompensatorias',
                text: 'Guardar cambios',
                iconCls: 'save2',
                tooltip: 'Guardar los cambios realizados.',
                action: 'guardarCargaMasivaCompensatorias'

            }],
            selModel: selModel,
            columns: [
                    {header: 'Legajo', dataIndex: 'legajo', flex: 2},
                    {header: 'Agente', dataIndex: 'apellido_nombre', flex: 12},
                    {header: 'Fecha desde', dataIndex: 'desde', renderer: Ext.util.Format.dateRenderer('d-m-Y'), flex: 3, editor: {xtype: 'datefield',
                        allowBlank: false,
                        id: 'fechaDesdeMasiva', 
                        format: 'd-m-Y',
                        onChange: function(){
                            var desde = Ext.getCmp('fechaDesdeMasiva').getValue();
                            var hasta = Ext.getCmp('fechaHastaMasiva').getValue();
                            var resta = hasta - desde;
                            var dias = resta/1000/60/60/24+1;
                            console.log(dias);
                            
                            if(desde <= hasta)
                            {
                                //console.log('else desde'+desde+'-'+hasta);
                                Ext.getCmp('totalDiasMasiva').setValue(dias);
                            }
                            else
                            {
                                Ext.getCmp('totalDiasMasiva').setValue('');
                            }
                            
                        }
                        }},
                    {header: 'Fecha hasta', dataIndex: 'hasta', renderer: Ext.util.Format.dateRenderer('d-m-Y'), flex: 3, editor: {xtype: 'datefield',
                        allowBlank: false,
                        id: 'fechaHastaMasiva', 
                        format: 'd-m-Y',
                        onChange: function(){
                            var desde = Ext.getCmp('fechaDesdeMasiva').getValue();
                            var hasta = Ext.getCmp('fechaHastaMasiva').getValue();
                            var resta = hasta - desde;
                            var dias = resta/1000/60/60/24+1;
                            console.log(dias);
                            
                            if(desde <= hasta)
                            {
                                //console.log('else desde'+desde+'-'+hasta);
                                Ext.getCmp('totalDiasMasiva').setValue(dias);
                            }
                            else
                            {
                                Ext.getCmp('totalDiasMasiva').setValue('');
                            }
                            
                        }
                        }},
                    {header: 'Cant. d&iacute;as', dataIndex: 'totaldias', flex: 2, editor: {allowBlank: false,
                        id: 'totalDiasMasiva',readOnly: true}}  
            ],
            plugins: [rowEditing]
        });
        var items = [formPanel,{
            xtype: 'splitter'
        }];
        items.push(gridItems);
        Ext.apply(this.items,items);
        
		this.callParent(arguments);
        console.log(this);
	}
});