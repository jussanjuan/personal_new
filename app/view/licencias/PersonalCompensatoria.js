Ext.define('personal.view.licencias.PersonalCompensatoria', {
	extend: 'Ext.window.Window',
	alias: 'widget.PersonalCompensatoria',
	title: 'Agregar Licencia Compensatoria',
	//frame: true,
    //border: false,
    //flex: 1,
    defaultType: 'textfield',
    closeAction: 'destroy',
    border: false,
    modal: true,
    fieldDefaults: {
       	anchor: '90%',
       	labelAlign: 'top'
    },

    items: [{
        xtype: 'form',
        bodyPadding: 5,
        method: 'POST',

        items: [{
            xtype: 'textfield',
            id: 'legajo',
            name: 'legajo',
            fieldLabel: 'Legajo',
            readOnly: true
            
        }, {
            xtype: 'textfield',
            name: 'anio',
            id: 'perComAnio',
            fieldLabel: 'Año',
            validateOnChange: 'true',
            value: new Date().getFullYear() + 1,
            validator: function(value){
            var stringPad=/^[1-2]{1}[0-9]{3}$/; //Expresion regular, para que ingresen el a�o con 4 digitos y solo comience con 1 o 2 (O sea, valido hasta el a�o 2999)
            if (!stringPad.test(value))
                {return ("Introduzca el a&ntilde;o con 4 d&iacute;gitos");}
            else
                {return true;}
            }
            
            
            
        }, {
            id: 'perComDesde',
    	    xtype: 'textfield',
            name: 'desde',
            fieldLabel: 'Trabaja desde',
            onChange: function(){
                var desde = parseInt(Ext.getCmp('perComDesde').getValue());
                var hasta = parseInt(Ext.getCmp('perComHasta').getValue());
                console.log('Desde '+desde+' Hasta '+hasta);
                var suma = parseInt(1);
		          if(desde > 31 || desde < 1)
                {
                    //console.log('if desde'+desde+'-'+hasta);
                    this.setValue('');
                    Ext.getCmp('perComDias').setValue('');
                    
                }
                else
                {
                    
                    if(desde <= hasta)
                    {
                        //console.log('else desde'+desde+'-'+hasta);
                        Ext.getCmp('perComDias').setValue(Ext.getCmp('perComHasta').getValue()-this.getValue()+suma);
                    }
                    else
                    {
                        Ext.getCmp('perComDias').setValue('');
                    }
                }
		
            },
            maskRe: new RegExp(/^\d{1}$/),
            allowBlank: false
            //value: new Date( '01/01/' + (new Date().getFullYear() + 1) )
        }, {
            id: 'perComHasta',
    	    xtype: 'textfield',
            name: 'hasta',
            fieldLabel: 'Trabaja hasta',
            onChange: function(){
                var desde = parseInt(Ext.getCmp('perComDesde').getValue());
                var hasta = parseInt(Ext.getCmp('perComHasta').getValue());
                var suma = parseInt(1);
                console.log('Desde '+desde+' Hasta '+hasta);
		if(hasta > 31 || hasta < 1)
                {
                    //console.log('if hasta'+desde+'-'+hasta);
                    this.setValue('');
                    Ext.getCmp('perComDias').setValue('');
                    
                }
                else
                {
                    
                    if(desde <= hasta)
                    {
                        //console.log('else hasta'+desde+'-'+hasta);
                        Ext.getCmp('perComDias').setValue(this.getValue()-Ext.getCmp('perComDesde').getValue()+suma);
                    }
                    else
                    {
                        Ext.getCmp('perComDias').setValue('');
                    }
                }
		
            },
            allowBlank: false,
            maskRe: new RegExp(/^\d{1}$/)
            //value: new Date( '01/01/' + (new Date().getFullYear() + 1) )
        }, {
            id: 'perComDias',
    	    xtype: 'textfield',
            name: 'totaldias',
            maskRe: new RegExp(/^\d{1}$/),
    	    fieldLabel: 'Total de dias',
            allowBlank: false,
            readOnly: true
        }]
    }],

    buttons: [{
    	text: 'Guardar',
        action: 'saveCompensatoria',
        iconCls: 'save2'
        
    }, {
    	text: 'Cancelar',
        action: 'cancelar',
        iconCls: 'cancel2'
        
    }],

	initComponent: function() {

		this.callParent(arguments);
	}
});