Ext.define('personal.view.licencias.PersonalDobleTurno', {
	extend: 'Ext.window.Window',
	alias: 'widget.PersonalDobleTurno',
	title: 'Agregar Licencia por doble turno',
	//frame: true,
    //border: false,
    //flex: 1,
    defaultType: 'textfield',
    closeAction: 'destroy',
    border: false,
    modal: true,
    fieldDefaults: {
       	anchor: '90%',
       	labelAlign: 'top'
    },

    items: [{
        xtype: 'form',
        bodyPadding: 5,
        method: 'POST',
        id: 'formDiasDobleTurno',
        items: [{
            xtype: 'textfield',
            id: 'legajo',
            name: 'legajo',
            fieldLabel: 'Legajo',
            readOnly: true
            
        },{
            id: 'perDTDesde',
    	    name: 'DTDesde',
            xtype: 'datefield',
            fieldLabel: 'Trabaja desde',
            format: 'd/m/Y',
            //minValue: new Date(),
            value: new Date(),
            allowBlank: false
        }, {
            id: 'perDTHasta',
    	    name: 'DTHasta',
            xtype: 'datefield',
            fieldLabel: 'Trabaja hasta',
            format: 'd/m/Y',
            //minValue: new Date(),
            allowBlank: false
        }, {
            id: 'perDTDias',
    	    xtype: 'textfield',
            name: 'totaldias',
            maskRe: new RegExp(/^\d{1}$/),
    	    fieldLabel: 'Total de dias',
            allowBlank: false
            //readOnly: true
        }]
    }],

    buttons: [{
    	text: 'Guardar',
        action: 'saveDobleTurno',
        iconCls: 'save2'
        
    }, {
    	text: 'Cancelar',
        action: 'cancelar',
        iconCls: 'cancel2'
        
    }],

	initComponent: function() {

		this.callParent(arguments);
	}
});