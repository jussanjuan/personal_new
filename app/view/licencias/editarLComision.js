Ext.define('personal.view.licencias.editarLComision', {
	extend: 'Ext.window.Window',
	alias: 'widget.editarLComision',
	title: 'Editar comisi&oacute;n',
    reference: 'editarLComision',
	defaultType: 'textfield',
    closeAction: 'destroy',
    border: false,
    modal: true,
    width: 500,
    frame: true,
    closeAction: 'destroy',
    layout: 'fit',
    //minHeight: 500,
    //maxHeight: 1000,
    fieldDefaults: {
       	anchor: '100%',
       	labelAlign: 'top'
    },

    items: [{
        xtype: 'form',
        bodyPadding: 5,
        method: 'POST',
        id: 'formEditarComision',
        items: [{
            xtype: 'textfield',
            id: 'legajoEditarComision',
            name: 'legajo',
            fieldLabel: 'Legajo',
            readOnly: true,
            allowBlank: false,
            anchor: '95%'
            
        },{
            id: 'editarComisionDesde',
    	    name: 'editarComisionDesde',
            xtype: 'datefield',
            fieldLabel: 'Trabaja desde',
            format: 'd/m/Y',
            //minValue: new Date(),
            value: new Date(),
            allowBlank: false,
            anchor: '95%'
        }, {
            id: 'editarComisionHasta',
    	    name: 'editarComisionHasta',
            xtype: 'datefield',
            fieldLabel: 'Trabaja hasta',
            format: 'd/m/Y',
            //minValue: new Date(),
            allowBlank: false,
            anchor: '95%'
        }, {
            id: 'editarComisionTotalDias',
    	    xtype: 'textfield',
            name: 'editarComisionTotalDias',
            //maskRe: new RegExp(/^\d{1}$/),
    	    fieldLabel: 'Total de dias',
            allowBlank: false,
            readOnly: true,
            anchor: '95%'
        },{
            id: 'editarComisionCausa',
            xtype: 'textfield',
            name: 'editarComisionCausa',
            fieldLabel: 'Causa',
            anchor: '95%'
        },{
            id: 'editarComisionExpediente',
            xtype: 'textfield',
            name: 'editarComisionExpediente',
            maskRe: new RegExp(/^\d{1}$/),
            fieldLabel: 'Expediente',
            anchor: '95%'
        },{
            id: 'editarComisionObservaciones',
            xtype: 'textareafield',
            grow: true,
            anchor: '95%',
            name: 'editarComisionObservaciones',
            fieldLabel: 'Observaciones'
        }]
    }],

    buttons: [{
    	text: 'Guardar',
        action: 'saveEditarComision',
        iconCls: 'save2'
        
    }, {
    	text: 'Cancelar',
        action: 'cancelar',
        iconCls: 'cancel2'
        
    }],

	initComponent: function() {

		this.callParent(arguments);
	}
});