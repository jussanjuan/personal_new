Ext.define('personal.view.login.Login', {
    extend: 'Ext.window.Window',
    xtype: 'login',
    
    requires: [
        'personal.view.login.LoginController',
        'Ext.form.Panel'
    ],

    controller: 'login',
    bodyPadding: 10,
    title: 'Iniciar sesi&oacute;n',
    closable: false,
    autoShow: true,

    items: {
        xtype: 'form',
        id: 'loginFrm',
        reference: 'form',
        items: [{
            id:'loginUsername',
            name:'loginUsername', 
            xtype: 'textfield',
            fieldLabel: 'Usuario',
            allowBlank: false,
            blankText: 'Este campo es obligatorio',
            listeners: {
                specialkey: function(field, e)
                {
                    if (e.getKey() == e.ENTER) 
                    {
                        logUsr();
                    }

                }
            } 
        }, {
            xtype: 'textfield',
            id:'loginPassword',
            name: 'loginPassword',
            inputType: 'password',
            fieldLabel: 'Contraseña',
            allowBlank: false,
            blankText: 'Este campo es obligatorio',
            listeners: {
                specialkey: function(field, e)
                {
                    if (e.getKey() == e.ENTER) 
                    {
                        logUsr();
                    }
                }
            }
        }/*, {
            xtype: 'displayfield',
            hideEmptyLabel: false,
            value: 'Enter any non-blank password',

        }*/],
        buttons: [{
            text: 'Ingresar',
            formBind: true,
            listeners: {
                click: 'onLoginClick'
            }
        }]
    }
});