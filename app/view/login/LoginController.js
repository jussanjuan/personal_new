Ext.define('personal.view.login.LoginController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.login',
    onCargaFeriados: function(response)
    {
         AppGlobals.set(feriados, []);
        
        var obj = Ext.decode(response.responseText);
        //console.log(obj);
        for (i=0; i < obj.results.length; i++) {
            AppGlobals.feriados.push(obj.results[i].mes+'-'+obj.results[i].dia);
        }
    },
    onLoginClick: function(button) {

        var frm = button.up().up().getForm();
        var me = this;
        if(frm.isValid())
        {
            frm.submit({ 
                url: 'app/usr.login.php',
                method: 'POST',
                params: {
                    loginMethod:'normal',
                    user: ''
                },
                waitTitle:'Conectando', 
                waitMsg:'Autenticando usuario...',
                success:function(form, action)
                {   
                    console.log("User login: "+action.response.responseText);
                    var obj = Ext.decode(action.response.responseText);
                    var res = eval(obj);
                    //var redirect = "../v2/app.index.php?"+obj.sessionID; 
                    // Set the localStorage value to true
                    localStorage.setItem("PersonalLoggedIn", true);

                    // Remove Login Window
                    me.getView().destroy();

                    Ext.Ajax.request({
                        url: 'app/usr.session.php',
                        async: false,
                        success: function(response, opts) 
                        {
                            //console.log("user session "+response.responseText);
                            var obj = Ext.decode(response.responseText);
                            //console.table(obj);

                            AppGlobals.sucursal= obj.lugar;
                            AppGlobals.usuario=obj.nombre_real;
                            AppGlobals.permiso= obj.permiso;
                            AppGlobals.evaluador= obj.evaluador;
                            AppGlobals.legajoUser= obj.legajo;
                            AppGlobals.exist_evaluacion = obj.exist_evaluacion;
                            AppGlobals.permisos = obj.permisos;

                            console.log(AppGlobals.permisos[0]);
                            if(typeof AppGlobals.permisos[0] == 'undefined')
                            {
                                Ext.Msg.alert('Error!', 'Error al cargar los permisos, o el usuario no tiene permisos asignados para el sistema.')
                            }
                            console.log("Existe evaluacion?: "+AppGlobals.exist_evaluacion);
                            if(AppGlobals.evaluador == "si")
                            {
                                AppGlobals.legEvaluadorEntry = obj.legajo;
                            }
                            AppGlobals.primeraContr= obj.primeraContr;
                            //console.log("Primera contrasena: "+AppGlobals.primeraContr);
                            if(AppGlobals.primeraContr == 'si')
                            {
                                Ext.Msg.alert('Informaci&oacute;n', 'Si este es su primer acceso al sistema o su contrase&ntilde;a nunca ha sido cambiada, se recomienda hacerlo haciendo click en el icono de llaves ubicado en la esquina superior derecha.')
                            }
                            var tipo_user="";
                            if( AppGlobals.permiso == "1")
                            {
                                tipo_user = "ADMINISTRADOR";
                            }
                            else
                            {
                                tipo_user = "INVITADO";
                            }
                            AppGlobals.rrhh=obj.rrhh;
                            AppGlobals.dpto_personal = obj.dpto_personal;    
                        },
                        
                        failure: function(response, opts) 
                        {
                            console.log(response.responseText);
                            var obj = Ext.decode(response.responseText);
                            Ext.msg.alert("Error", obj.error);
                            
                        }
                    });
                    
                    // Carga los feriados
                    Ext.Ajax.request({
                        url: 'app/proxy.feriados.php',
                        params: {
                            tarea: 'diasLaborales',
                            method: 'GET',
                            anio: new Date().getFullYear()
                        },
                        success: function(response){
                            var obj = Ext.decode(response.responseText);
                            console.log(obj);
                            if(obj.results.length==0)
                            {
                                //Muestro el cartel solo si el usuarios pertenece al departamento de personal o RRHH, dado que ellos tienen incumbencia en la carga de feriados
                                if(AppGlobals.rrhh== "si" || AppGlobals.dpto_personal== "si")
                                {
                                    Ext.Msg.alert('Advertencia', 'Hay pocos o ning&uacute;n feriado cargado, por favor revise y cargue todos los feriados del per&iacute;odo actual.');
                                }
                            }
                            else
                            {
                                //onCargaFeriados(response);    
                            }
                            
                        },
                        failure: function(response, opts) {
                            Ext.Msg.alert('Error', 'No se pudo conectar a la base de datos para cargar los feriados del a&ntilde;o actual. Error de servidor. <br>Estado: ' + response.status);
                            console.log('server-side failure with status code ' + response.status);
                        }
                    });

                    // Add the main view to the viewport
                    Ext.create({
                        xtype: 'app-main'
                    });
                },

                failure:function(form, action){ 
                    console.log(action.failureType);
                    if(action.failureType == 'server'){ 
                        obj = Ext.decode(action.response.responseText); 
                        Ext.Msg.alert('Error de autenticacion', obj.error); 
                    } else { 
                        Ext.Msg.alert('Atencion!', 'Intente conectarse nuevamente.  '); 
                    } 
                    frm.reset();
                } 
            });
                
        }
        else
        {
            Ext.Msg.alert('Error', 'Complete los campos.'); 
        }
    }

    
});