/*var status_bar = Ext.define('Ext.ux.StatusBar', {
        id: 'my-status',

        // defaults to use when the status is cleared:
        defaultText: 'Default status text',
        defaultIconCls: 'default-icon',

        // values to set initially:
        text: 'Ready',
        iconCls: 'ready-icon',

        // any standard Toolbar items:
        items: [{
            text: 'A Button'
        }, '-', 'Plain Text']
    });*/
Ext.define('personal.view.main.Main', 
{
    extend: 'Ext.tab.Panel',
    xtype: 'app-main',
    requires: [
        'Ext.plugin.Viewport',
        'Ext.window.MessageBox',
        'personal.view.PersonalGrilla',
        'personal.view.PersonalFicha',
        'personal.view.auditoria.AuditoriaPanel',
        'personal.view.changePassword',
        'personal.view.PersonalBaja',
        'personal.view.estadisticas.EstadisticasPanel'
    ],
    controller: 'main',
    viewModel: 'main',
    
    plugins: 'viewport',
    ui: 'navigation',
    tabBarHeaderPosition: 1,
    titleRotation: 0,
    tabRotation: 0,
    //tabBarPosition: 'bottom',
    header: {
        layout: {
            align: 'stretchmax'
        },
        title: {
            bind: {
                text: '{name}'
            },
            flex: 0
        },
        iconCls: 'fa-th-list',
        items: [{
            xtype: 'button',
            text: 'Cerrar sesi&oacute;n',
            margin: '10 0',
            handler: 'onLogoutClick'
        },
        {
            id: 'changPassBtn',
            xtype: 'button',
            text:'Cambiar contrase&ntilde;a',
            action: 'changPass',
            //iconCls: 'changePass',
            tooltip: 'Cambiar contrase&ntilde;a',
            handler: 'showWindowChangePass'
        }]
    },
    tabBar: {
        flex: 1,
        layout: {
            align: 'stretch',
            overflowHandler: 'none'
        }
    },

    responsiveConfig: {
        tall: {
            headerPosition: 'top'
        },
        wide: {
            headerPosition: 'left'
        }
    },

    defaults: {
        bodyPadding: 20,
        tabConfig: {
            responsiveConfig: {
                wide: {
                    iconAlign: 'left',
                    textAlign: 'left'
                },
                tall: {
                    iconAlign: 'top',
                    textAlign: 'center',
                    width: 120
                }
            }
        }
    },
    tbar: [],
    items: [],  
    defaults: 
    {
        listeners: 
        {
            activate: function(tab, eOpts) 
            {
                console.log(tab.id);
                if(tab.id== "auditoriaPanel")
                {
                    console.log("Click en auditoria");
                }
                
            }
            
        }
    },
    /*bbar: [{
            xtype: 'statusbar',
            id: 'my-status',

            // defaults to use when the status is cleared:
            defaultText: 'Default status text',
            defaultIconCls: 'default-icon',

            // values to set initially:
            text: 'Ready',
            iconCls: 'ready-icon',

            // any standard Toolbar items:
            items: [{
                text: 'A Button'
            }, '-', 'Plain Text']
        }
    ],*/
    initComponent: function() 
    {
        //Controlo los permisos de usuario, y en base a eso muestro las pesta�as

        var items=[]; //Arreglo de items
        if(typeof AppGlobals.permisos[0] !== 'undefined' && AppGlobals.permisos[0].pesta_personal == 1)
        {
            items.push(
            {
                xtype: 'container',
                title: 'Personal',
                id: 'tabPersonal',
                iconCls: 'x-fa fa-users',
                layout: {
                    type: 'hbox',       // Arrange child items vertically
                    align: 'stretch',    // Each takes up full width
                    padding: 5,
                    border: 0
                },
                items:[{
                    xtype: 'PersonalGrilla',
                    anchor: '100%'
                }, {
                    xtype: 'splitter'

                },{
                    xtype: 'PersonalFicha',
                    title: 'Ficha'
                }]
            });
        }
        if(typeof AppGlobals.permisos[0] !== 'undefined' && AppGlobals.permisos[0].pesta_reportes == 1)
        {
            items.push({
                title: 'Reportes',
                id: 'tabReportes',
                html: '<iframe src="app/reportes.php" width="100%" height="100%"></iframe>',
                iconCls: 'x-fa fa-filter',
                title: 'Reportes'
            });
        }
        if(typeof AppGlobals.permisos[0] !== 'undefined' && AppGlobals.permisos[0].pesta_estadisticas == 1)
        {
            items.push({
                xtype: 'EstadisticasPanel'
                
            });
            
        }

        if(typeof AppGlobals.permisos[0] !== 'undefined' && AppGlobals.permisos[0].pesta_auditoria == 1)
        {
            items.push(
            {
                xtype: 'AuditoriaPanel',
                iconCls: 'x-fa fa-eye',
                title: 'Auditoria'
            });
        }
        //Aplico el arreglo de paneles a la vista para mostrar
        Ext.apply(this, {items: items});
        this.callParent(arguments);
    }

});