Ext.define('personal.view.main.MainController', {
	extend: 'Ext.app.ViewController',
    alias: 'controller.main',
	stores: [
		'PersonalStore',
        'FamiliaStore',
        'RelacionStore',
        'SancionStore',
        'FueroStore',
        'historial.HLicenciaStore',
		'historial.HCargosStore',
        'TituloStore',
		'historial.HLugarStore',
        'historial.HTituloStore',
        'historial.HSancionStore',
        'historial.HCapacitacionStore',
        'historial.HFirmaDigitalStore',
        'TipoEstudioStore',
        'NivelEstudiosStore',
        'PaisStore',
        'PersonalPermisosStore',
        'CapacitacionStore',
        'CapacitacionTipoStore',
        'CapacitacionModalidadStore',
        'CapacitacionOrigenStore',
        'AniosCertificadosStore',
        'auditoria.AuditoriaStore',
        'FirmaDigitalTipoStore',
        'historial.HistoriaClinicaStore',
        'HDobleTurnoStoreDiasDT',
        'HCompensatoriaStoreDiasFeria'
        
    ],
    models:[
    	'Persona',
        'Familia',
        'Relacion',
        'Lugar',
        'Sancion',
        'Fuero',
        'historial.HLicencia',
    	'historial.HCargos',
    	'historial.HLugar',
        'Titulo',
        'historial.HTitulo',
        'historial.HSancion',
        'historial.HCapacitacion',
        'historial.HFirmaDigital',
        'TipoEstudio',
        'NivelEstudios',
        'Pais',
        'PersonaPermisos',
        'Capacitacion',
        'CapacitacionTipo',
        'CapacitacionModalidad',
        'CapacitacionOrigen',
        'AniosCertificados',
        'auditoria.Auditoria',
        'FirmaDigitalTipo',
        'HDobleTurnoDiasDT'
    ],
    views: [
        'Main',
        'PersonalGrilla',
        'PersonalFicha',
        'PersonalFichaLegajo',
        'PersonalFichaLicencia',
        'PersonalFamilia',
        'PersonalAgregarFamiliar',
        'PersonalEditarFamiliar',
        'PersonalCargos',
        'PersonalLugares',
        'PersonalWebcam',
        'PersonalBaja',
        'PersonalTitulos',
        'PersonalAgregarTitulo',
        'PersonalAltaTitulo',
        'PersonalBajaTitulo',
        'PersonalModificarTitulo',
        'changePassword',
        'PersonalAgregarFirmaDigital',
        'PersonalModificarFirmaDigital',
        'PersonalHistoriaClinica',
        'PersonalEditarHistoriaClinica',
        'PersonalVerHistoriaClinica',
        'Feriados'

    ],
    refs: [{
		ref: 'Personalgrilla',
		selector: 'PersonalGrilla'
	}, {
		ref: 'Personalficha',
		selector: 'PersonalFicha'
	}, {
		ref: 'Personalfichalegajo',
		selector: 'PersonalFichaLegajo'
	}, {
		ref: 'PersonalAgregarcargo',
		selector: 'PersonalAgregarCargo'
	}, {
		ref: 'PersonalAltacargo',
		selector: 'PersonalAltaCargo'
	}, {
		ref: 'PersonalBajacargo',
		selector: 'PersonalBajaCargo'
	}, {
		ref: 'PersonalModificarcargo',
		selector: 'PersonalModificarCargo'
	}, {
		ref: 'PersonalAgregarlugar',
		selector: 'PersonalAgregarLugar'
	}, {
		ref: 'PersonalAltalugar',
		selector: 'PersonalAltaLugar'
	}, {
		ref: 'PersonalBajalugar',
		selector: 'PersonalBajaLugar'
	}, {
		ref: 'PersonalModificarlugar',
		selector: 'PersonalModificarLugar'
	}, {
		ref: 'PersonalAgregartitulo',
		selector: 'PersonalAgregarTitulo'
	}, {
		ref: 'PersonalAltatitulo',
		selector: 'PersonalAltaTitulo'
	}, {
		ref: 'PersonalBajatitulo',
		selector: 'PersonalBajaTitulo'
	}, {
        ref: 'PersonalBaja',
        selector: 'PersonalBaja'
    }, {
		ref: 'PersonalModificartitulo',
		selector: 'PersonalModificarTitulo'
	}, {
        ref: 'Personalpermisos',
        selector: 'PersonalPermisos'
    },{
        ref: 'PersonalAgregarsancion',
        selector: 'PersonalAgregarSancion'
    },{
        ref: 'PersonalAgregarcapacitacion',
        selector: 'PersonalAgregarCapacitacion'
    },{
        ref: 'PersonalModificarcapacitacion',
        selector: 'PersonalModificarCapacitacion'
    },{
        ref: 'PersonalAgregarfirmadigital',
        selector: 'PersonalAgregarFirmaDigital'
    },{
        ref: 'PersonalModificarfirmadigital',
        selector: 'PersonalModificarFirmaDigital'
    },{
        ref: 'PersonalAgregarfamiliar',
        selector: 'PersonalAgregarFamiliar'
    },{
        ref: 'PersonalEditarfamiliar',
        selector: 'PersonalEditarFamiliar'
    }],

	init: function()
    {
		console.log('Controlador Main inicializados!');
                
		var accion;

		this.control({
            'Main button[action=changPass]': {
                click: this.showWindowChangePass
            },
            'changePassword button[action=guardar]': {
                click: this.changePass
            },
            'changePassword button[action=cancelar]': {
                click: this.closeWindowChangePass
            },
			'PersonalFichaLegajo': {
				beforerender: this.initLegajo
			},
			'PersonalFichaLegajo image[id=foto]': {
				click: this.onClickFoto
			},

			'toolbar > button#btnEditAgente': {
				click: this.onClickEditAgente
			},
            'PersonalGrilla': {
                activate: this.controlEditing
            },
			'PersonalGrilla': {
				itemdobletap: this.controlEditing
			},

            'AuditoriaPanel textfield': {
                change: this.onTypeSearchAuditoria,
                specialkey: this.onEnterSearchAuditoria
            },

            'PersonalGrilla button[action=agregar]': {
				click: this.addForm
			},
			'PersonalGrilla button[action=editar]': {
				click: this.editForm
			},
            'PersonalGrilla button[action=darBaja]': {
				click: this.darDeBaja
			},
            'PersonalGrilla button[action=actualizar_antiguedades]': {
                click: this.actualizar_antiguedades
            },
            'PersonalGrilla button[action=verTodosAgentes]': {
				click: this.verTodos
			},
            'PersonalGrilla button[action=imprimirAgentes]': {
				click: this.imprimirAgentes
			},
            'PersonalGrilla textfield': {
                change: this.onTypeSearchAgentes,
                specialkey: this.onEnterSearchAgentes
            },
			'PersonalFichaLegajo button[action=guardar]': {
				click: this.saveForm
			},
			'PersonalFichaLegajo button[action=cancelar]': {
				click: this.cancelForm
			},
			'PersonalFichaLegajo button[action=limpiar]': {
				click: this.clearForm
			},
            /*'PersonalFichaLegajo button[action=darBaja]': {
				click: this.darDeBaja
			},*/
            'PersonalBaja button[action=saveBaja]': {
				click: this.guardarBaja
			},
            'PersonalBaja button[action=cancelar]': {
				click: this.cancelFormBaja
			},


            //Familiares*********************************************************************
            'PersonalFamilia': {
                activate: this.onActivateFamilia
                //selectionchange: this.activateBtnEliminarAndEditarFamiliar
            },
            'PersonalGrillaFamiliares': {
                selectionchange: this.activateBtnEliminarAndEditarFamiliar
            },   
            'PersonalFamilia button[action=guardarCert]': {
                click: this.guardarCertificados
            },  
            'PersonalFamilia button[action=cambiarAnioCertificados]': {
                click: this.cambiarAnioCertificados
            },        
            'PersonalFamilia button[action=agregar]': {
                click: this.VentanaFamiliar
            },
            'PersonalFamilia combo[name=anioCertificado]': {
                change: this.onChangeAnioCertificado
                //selectionchange: this.activateBtnEliminarAndEditarFamiliar
            },
            'PersonalFamilia textareafield[name=observaciones_tesoreria]': {
                change: this.onChangeObsTesoreria
            },
            'PersonalFamilia button[action=eliminarFamiliar]': {
                click: this.eliminarFamiliar
            },
            'PersonalFamilia button[action=openWindowModificarFamiliar]': {
                click: this.openWindowModificarFamiliar
            },
            'PersonalAgregarFamiliar button[action=guardarAsigFamiliar]': {
                click: this.onAgregarFamiliar
            },
            'PersonalAgregarFamiliar button[action=cancelar]': {
                click: this.onCancelarFamiliar
            },
            'PersonalAgregarFamiliar combo[name=relacion]': {
                change: this.onChangeRelacion
            },
            'PersonalEditarFamiliar combo[name=relacion]': {
                change: this.onChangeRelacion
            },

            'PersonalEditarFamiliar button[action=cancelar]': {
                click: this.onCancelarEditarFamiliar
            },
            'PersonalEditarFamiliar button[action=guardarEditFamiliar]': {
                click: this.onGuardarEditarFamiliar
            },

            //Cargos*********************************************************************
			'PersonalCargos': {
				activate: this.onActivateCargos,
                selectionchange: this.selectionChangeCargo
			},
			'PersonalCargos button[action=agregar]': {
				click: this.VentanaCargo
			},
            //Elimina un cargo existente asignado a una persona
            'PersonalCargos button[action=eliminarCargo]': {
				click: this.eliminaCargo
			},
            //Asigna un cargo existente a una persona
			'PersonalAgregarCargo button[action=guardar]': {
				click: this.onAgregarCargo
			},
            'PersonalAgregarCargo button[action=cancelar]': {
				click: this.cancelaCargo
			},
            'PersonalAgregarCargo #fechaDesdeAgregarCargo': {
                change: this.changeFechaAltaCargo
            },
            'PersonalAgregarCargo #fechaHastaAgregarCargo': {
                change: this.changeFechaAltaCargo
            },
            'PersonalCargos button[action=altaCargo]': {
				click: this.VentanaAltaCargo
			},
            'PersonalCargos button[action=bajaCargo]': {
				click: this.VentanaBajaCargo
			},   
            'PersonalCargos button[action=modificarCargo]': {
				click: this.VentanaModificarCargo
			},
            //Crea un nuevo cargo en el sistema
            'PersonalAltaCargo button[action=guardar]': {
				click: this.altaCargo
			},
            'PersonalAltaCargo button[action=cancelar]': {
				click: this.cancelaAltaCargo
			},
            //Elimina un cargo del sistema
            'PersonalBajaCargo button[action=eliminar]': {
				click: this.bajaCargo
			},
            'PersonalBajaCargo button[action=cancelar]': {
				click: this.cancelaBajaCargo
			},
            //Modifica un cargo del sistema
            'PersonalModificarCargo button[action=modificar]': {
				click: this.modificaCargo
			},
            'PersonalModificarCargo button[action=cancelar]': {
				click: this.cancelaModificaCargo
			},
            'PersonalModificarCargo #cargoModifCombo': {
                change: this.changeSelectModifCargo
            },
            
            
			'PersonalLugares': {
				activate: this.onActivateLugares,
                selectionchange: this.selectionChangeLugares
			},
			'PersonalLugares button[action=agregar]': {
				click: this.VentanaLugar
			},
            'PersonalLugares button[action=eliminarLugar]': {
				click: this.eliminaLugar
			},
			'PersonalAgregarLugar button[action=guardar]': {
				click: this.onAgregarLugar
			},
            'PersonalAgregarLugar button[action=cancelar]': {
				click: this.cancelaLugar
			},
            'PersonalAgregarLugar #fechaDesdeAgregarLugar': {
                change: this.changeFechaAltaLugar
            },
            'PersonalAgregarLugar #fechaHastaAgregarLugar': {
                change: this.changeFechaAltaLugar
            },
            'PersonalLugares button[action=altaLugar]': {
				click: this.VentanaAltaLugar
			},
            'PersonalLugares button[action=bajaLugar]': {
				click: this.VentanaBajaLugar
			},
            'PersonalLugares button[action=modificarLugar]': {
				click: this.VentanaModificarLugar
			},
            //Crea un nuevo lugar en el sistema
            'PersonalAltaLugar button[action=guardar]': {
				click: this.altaLugar
			},
            'PersonalAltaLugar button[action=cancelar]': {
				click: this.cancelaAltaLugar
			},
            //Elimina un lugar del sistema
            'PersonalBajaLugar button[action=eliminar]': {
				click: this.bajaLugar
			},
            'PersonalBajaLugar button[action=cancelar]': {
    			click: this.cancelaBajaLugar
			},
            //Modifica un lugar del sistema
            'PersonalModificarLugar button[action=modificar]': {
				click: this.modificaLugar
			},
            'PersonalModificarLugar button[action=cancelar]': {
				click: this.cancelaModificaLugar
			},
            'PersonalModificarLugar #lugarModifCombo': {
                change: this.changeSelectModifLugar
            },
            'PersonalTitulos': {
				activate: this.onActivateTitulos,
                itemclick: this.clickTitulo,
                selectionchange: this.selectionChangeTitulo
			},
            'PersonalTitulos button[action=agregarTitulo]': {
				click: this.VentanaTitulo
			},
            'PersonalTitulos button[action=editarTitulo]': {
                    click: this.modificarTituloAgente
            },
            'PersonalTitulos button[action=eliminarTitulo]': {
				click: this.eliminaTitulo
			},
            'PersonalTitulos button[action=guardarCambiosTitulo]': {
                    click: this.guardarCambiosTitulo
            },
            'PersonalAgregarTitulo button[action=guardar]': {
				click: this.onAgregarTitulo
			},
            'PersonalAgregarTitulo button[action=cancelar]': {
				click: this.cancelaTitulo
			},
            'PersonalTitulos button[action=altaTitulo]': {
				click: this.VentanaAltaTitulo
			},
            'PersonalTitulos button[action=bajaTitulo]': {
				click: this.VentanaBajaTitulo
			},
            'PersonalTitulos button[action=modificarTitulo]': {
				click: this.VentanaModificarTitulo
			},
            //Crea un nuevo titulo en el sistema
            'PersonalAltaTitulo button[action=guardar]': {
				click: this.altaTitulo
			},
            'PersonalAltaTitulo button[action=cancelar]': {
				click: this.cancelaAltaTitulo
			},
            //Elimina un titulo del sistema
            'PersonalBajaTitulo button[action=eliminar]': {
				click: this.bajaTitulo
			},
            'PersonalBajaTitulo button[action=cancelar]': {
				click: this.cancelaBajaTitulo
			},
            //Modifica un titulo del sistema
            'PersonalModificarTitulo button[action=modificar]': {
				click: this.modificaTitulo
			},
            'PersonalModificarTitulo button[action=cancelar]': {
    		    click: this.cancelaModificaTitulo
			},
            'PersonalPermisos': {
                activate: this.onActivatePermisos
            },
            'PersonalPermisos button[action=guardarPerm]': {
                click: this.savePermisos
            },
            'PersonalPermisos combo[name=tipoPermVista]': {
                change: this.enableListadoPersonasPermisos
            },
            'PersonalSanciones': {
                activate: this.onActivateSanciones
            },
            'PersonalSanciones button[action=agregarSancion]': {
                click: this.VentanaSancion
            },
            'PersonalSanciones button[action=eliminarSancion]': {
                click: this.eliminaSancion
            },
            'PersonalAgregarSancion button[action=guardar]': {
                click: this.onAgregarSancion
            },
            'PersonalAgregarSancion #fechaDesdeAgregarSancion': {
                change: this.changeFechaAgregarSancion
            },
            'PersonalAgregarSancion #fechaHastaAgregarSancion': {
                change: this.changeFechaAgregarSancion
            },
            'PersonalAgregarSancion button[action=cancelar]': {
                click: this.cancelaSancion
            },

            'PersonalCapacitaciones': {
                activate: this.onActivateCapacitaciones
            },
            'PersonalCapacitaciones button[action=agregarCapacitacion]': {
                click: this.VentanaCapacitacion
            },
            'PersonalCapacitaciones button[action=editarCapacitacion]': {
                click: this.VentanaEditarCapacitacion
            },
            'PersonalCapacitaciones button[action=eliminarCapacitacion]': {
                click: this.eliminaCapacitacion
            },
            'PersonalAgregarCapacitacion button[action=guardar]': {
                click: this.onAgregarCapacitacion
            },
            
            'PersonalAgregarCapacitacion button[action=cancelar]': {
                click: this.cancelaCapacitacion
            },
            'PersonalModificarCapacitacion button[action=guardar]': {
                click: this.onEditarCapacitacion
            },
            
            'PersonalModificarCapacitacion button[action=cancelar]': {
                click: this.cancelaEditarCapacitacion
            },

            'PersonalFirmasDigitales': {
                activate: this.onActivateFirmasDigitales
            },
            'PersonalFirmasDigitales button[action=agregarFirmaDigital]': {
                click: this.VentanaFirmaDigital
            },
            'PersonalFirmasDigitales button[action=editarFirmaDigital]': {
                click: this.VentanaEditarFirmaDigital
            },
            'PersonalFirmasDigitales button[action=eliminarFirmaDigital]': {
                click: this.eliminaFirmaDigital
            },
            'PersonalAgregarFirmaDigital button[action=saveFirmaDigital]': {
                click: this.onAgregarFirmaDigital
            },
            
            'PersonalAgregarFirmaDigital button[action=cancelar]': {
                click: this.cancelaFirmaDigital
            },
            'PersonalModificarFirmaDigital button[action=editarFirmaDigital]': {
                click: this.onEditarFirmaDigital
            },
            
            'PersonalModificarFirmaDigital button[action=cancelar]': {
                click: this.cancelaEditarFirmaDigital
            },

            /*Historia clinica*/
            'PersonalFichaHistoriaClinica button[action=agregarRegistroHistoria]': {
                click: this.onAgregarRegistroHistoria
            },
            'PersonalFichaHistoriaClinica button[action=editarRegistroHistoria]': {
                click: this.onEditarRegistroHistoria
            },
            'PersonalFichaHistoriaClinica button[action=quitarRegistroHistoria]': {
                click: this.onEliminarRegistroHistoria
            },
            'PersonalFichaHistoriaClinica button[action=verRegistroHistoria]': {
                click: this.onVerRegistroHistoria
            },
            'PersonalHistoriaClinica button[action=saveRegistroHistoria]': {
                click: this.onSaveRegistroHistoria
            },
            'PersonalHistoriaClinica button[action=cancelar]': {
                click: this.cancelaRegistroHistoria
            },
            'PersonalEditarHistoriaClinica button[action=saveEditarRegistroHistoria]': {
                click: this.onSaveEditarRegistroHistoria
            },
            'PersonalEditarHistoriaClinica button[action=cancelar]': {
                click: this.cancelaEditarRegistroHistoria
            },
            'PersonalFichaHistoriaClinica button[action=borrarCertificadoHistoria]': {
                click: this.eliminarCertificadoHistoria
            },
            'PersonalVerHistoriaClinica button[action=cancelar]': {
                click: this.cancelaVerRegistroHistoria
            },
            'PersonalFichaHistoriaClinica': {
                selectionchange: this.selectHistoriaClinica,
                itemdblclick: this.onVerRegistroHistoriaDblClick
            },

            //Alerto al usuario si está por salir de la pestaña sin guardar cambios
            'Main': {
                beforetabchange: this.onBeforeTabChange 
            },
            

            /* Feriados */
            'Feriados button[action=addFeriado]': {
                click: this.onAddFeriado
            },
            'FeriadosGrilla': {
                selectionchange: this.onSelectFeriado
            },
            'Feriados button[action=saveFeriados]': {
                click: this.onSaveFeriados
            },
            'Feriados button[action=eliminarFeriado]': {
                click: this.onEliminarFeriado
            },

            'Feriados button[action=cancelaFeriados]': {
                click: this.onCancelaFeriados
            },
            'PersonalGrilla button[action=editFeriados]': {
                click: this.openWindowFeriados
            },
            
            //Agregar dias de compensatoria
            'PersonalFichaDiasTrabajados button[action=addCompensatoria]': {
                click: this.onAddCompensatoria
            },
            'PersonalCompensatoria button[action=saveCompensatoria]': {
                click: this.onSaveCompensatoria
            },
            'PersonalCompensatoria button[action=cancelar]': {
                click: this.cancelaCompensatoria
            },

            //Click en un registro de dias trabajados
            'PersonalFichaDiasTrabajados': {
                selectionchange: this.clickLicencia
            },
            //Agregar dias trabajados en doble turno
            'PersonalFichaDiasTrabajados button[action=addDobleTurno]': {
                click: this.onAddDobleTurno
            },
            'PersonalDobleTurno button[action=saveDobleTurno]': {
                click: this.onSaveDobleTurno
            },
            'PersonalDobleTurno button[action=cancelar]': {
                click: this.cancelaDobleTurno
            },

            'PersonalDobleTurno #formDiasDobleTurno > #perDTDesde': {
                change: this.onChangeLDobleTurnoDias
            },
            
            'PersonalDobleTurno #formDiasDobleTurno > #perDTHasta': {
                change: this.onChangeLDobleTurnoDias
            },
                        
            /*Comisiones (no son licencias per se, dado que la persona en realidad está trabajando, solo que en otro lado, pero al ser muy similar el tratamiento, lo ubico acá)*/
            'PersonalFichaComisiones button[action=agregarComision]': {
                click: this.onAgregarComision
            },
            'PersonalFichaComisiones button[action=editarComision]': {
                click: this.onEditarComision
            },
            'PersonalFichaComisiones button[action=quitarComision]': {
                click: this.onEliminarComision
            },

            'LComision #formComision > #comisionDesde': {
                change: this.onChangeComisionHasta
            },
            'LComision #formComision > #comisionHasta': {
                change: this.onChangeComisionHasta
            },
            'editarLComision #formEditarComision > #editarComisionDesde': {
                change: this.onChangeEditarComisionHasta
            },
            'editarLComision #formEditarComision > #editarComisionHasta': {
                change: this.onChangeEditarComisionHasta
            },
            'LComision button[action=saveComision]': {
                click: this.onSaveComision
            },
            'LComision button[action=cancelar]': {
                click: this.cancelaComision
            },
            'editarLComision button[action=saveEditarComision]': {
                click: this.onSaveEditarComision
            },
            'editarLComision button[action=cancelar]': {
                click: this.cancelaEditarComision
            },
            'PersonalFichaComisiones': {
                selectionchange: this.selectComision
            },

            
            // Maternidad
            'LMaternidad > #5_Hasta': {
                change: this.onChangeLMaternidadHasta
            },
            //Agrego el evento al campo desde. Bug corregido Mauro
            'LMaternidad > #5_Desde': {
                change: this.onChangeLMaternidadHasta
            },
            'LMaternidad button[action=guardar]': {
                click: this.onSaveLicencia
            },
            'LMaternidad button[action=limpiar]': {
                click: this.clearForm
            },
            // Estudio 
            'LEstudio > #12_Hasta': {
                change: this.onChangeLEstudioHasta
            },
            //Agrego el evento al campo desde. Bug corregido Mauro
            'LEstudio > #12_Desde': {
                change: this.onChangeLEstudioHasta
            },
            'LEstudio button[action=guardar]': {
                click: this.onSaveLicencia
            },
            'LEstudio button[action=limpiar]': {
                click: this.clearForm
            },
            
            /*Solicitud de licencias*/
            'PersonalFichaSolicitudLicencias button[action=agregarSolicitudLicencia]': {
                click: this.onAgregarSolicitudLicencia
            },
            'PersonalFichaSolicitudLicencias button[action=editarSolicitudLicencia]': {
                click: this.onEditarSolicitudLicencia
            },
            'PersonalFichaSolicitudLicencias button[action=quitarSolicitudLicencia]': {
                click: this.onEliminarSolicitudLicencia
            },
            'PersonalFichaSolicitudLicencias button[action=confirmarSolicitudLicencia]': {
                click: this.onConfirmarSolicitudLicencia
            },
            'SolicitudLicencia button[action=saveSolicitudLicencia]': {
                click: this.onSaveSolicitudLicencia
            },
            'SolicitudLicencia button[action=cancelar]': {
                click: this.cancelaSolicitudLicencia
            },
            'ModificarSolicitudLicencia button[action=modificarSaveSolicitudLicencia]': {
                click: this.onSaveEditarSolicitudLicencia
            },
            'ModificarSolicitudLicencia button[action=cancelar]': {
                click: this.cancelaEditarSolicitudLicencia
            },
            'ConfirmarSolicitudLicencia button[action=confirmarSaveSolicitudLicencia]': {
                click: this.onSaveConfirmarSolicitudLicencia
            },
            'ConfirmarSolicitudLicencia button[action=cancelar]': {
                click: this.cancelaConfirmarSolicitudLicencia
            },
            'ConfirmarSolicitudLicencia #confirmarSolicitudLicenciaHasta': {
                change: this.onChangeSolicitudLicenciahasta
            },
            'ConfirmarSolicitudLicencia #confirmarSolicitudLicenciaDesde': {
                change: this.onChangeSolicitudLicenciahasta
            },
            'PersonalFichaSolicitudLicencias': {
                selectionchange: this.selectSolicitudLicencia,
                //itemdblclick: this.onVerRegistroHistoriaDblClick
            },

            //Controladores para Historial

            
            'HMaternidad button[action=editarMaternidad]': {
                    click: this.modificarLicMaternidad
            },
            'HMaternidad button[action=guardarCambiosMaternidad]': {
                    click: this.guardarCambiosLicMaternidad
            },  
            'PersonalFichaDiasTrabajados': {
                selectionchange: this.clickLicenciaFeria
            },
            'HEstudio': {
                itemclick: this.clickLicencia
            },
            'HDobleTurnoDiasDT': {
                itemclick: this.clickLicenciaDT
            },
            'PersonalFichaLicencias button[action=agregarLicencia]': {
                click: this.onAltaLicencia
            },
            'PersonalFichaLicencia button[action=desistir]': {
                click: this.desisteLicencia
            },
            'PersonalFichaLicencia button[action=eliminar_licencia]': {
                click: this.eliminarLicencia
            },
            'PersonalFichaLicencias button[action=edit_licencia]': {
                click: this.onEditarLicencia
            },
            'PersonalFichaDiasTrabajados button[action=vaciar_dias_trabajados]': {
                click: this.vaciarDiasTrabajados
            },
            'PersonalFichaLicencias': {
                itemclick: this.clickLicencia,
                selectionchange: this.activateBtnEditLicencia
            },

            //Nuevo ALTA LICENCIA
            'AltaLicencia #formAltaLicencia > #comboTipoLicenciaAltaLicencia': {
                change: this.onChangeTipoLicencia
            },
            'AltaLicencia #formAltaLicencia > fieldcontainer > #altaLicenciaDesde': {
                change: this.onChangeAltaLicenciaHasta
            },
            'AltaLicencia #formAltaLicencia > fieldcontainer > #altaLicenciaHasta': {
                change: this.onChangeAltaLicenciaHasta
            },  
            'AltaLicencia button[action=saveAltaLicencia]': {
                click: this.onSaveAltaLicencia
            }, 
            'AltaLicencia button[action=cancelar]': {
                click: this.onCancelarAltaLicencia
            },          
            'PersonalFichaHistorial > #treeHistorial': {
                selectionchange: this.onClickHistorial
            },
            'ModificarLicencia button[action=modificarSaveLicencia]': {
                click: this.onSaveEditarLicencia
            }, 
            'ModificarLicencia button[action=cancelar]': {
                click: this.onCancelarEditarLicencia
            }
                        			
		});
	},
    requires:[
        //'personal.view.Feriados'
    ],
    
    /**
    * Abre una ventana para cambiar la contraseña
    * @method showWindowChangePass
    **/
    onLogoutClick: function (sender, record) 
    {
        Ext.Msg.confirm({
            title: 'Confirmaci&oacute;n', 
            message: 'Desea cerrar la sesi&oacute;n actual?', 
            fn: 'logout',
            icon: Ext.window.MessageBox.QUESTION,
            scope: this, 
            buttonText : 
            {
                yes : 'Si',
                no : 'No',
            }
        });
    },

    showWindowChangePass: function() 
    {
        console.log("Ventana para cambiar contraseña");
        var win = Ext.lookupReference('changePassword');
        console.log(win);
        //win.show();
    },

    logout: function (choice)
    {
        if (choice === 'yes') 
        {
            var me = this;
            console.log("Logout");
            Ext.Ajax.request({
                url: 'app/usr.login.php',
                params: {
                    acc: 'logout'
                },
                success: function(response, opts) 
                {
                    console.log(response.responseText);
                    var obj = Ext.decode(response.responseText);
                                    
                    //Borro variables globales
                    AppGlobals.agente = 0;
                    AppGlobals.nombreAgente= '';
                    AppGlobals.usuario= '[vacio]';
                    AppGlobals.sucursal= 'RRHH';
                    AppGlobals.feriados= new Array();
                    AppGlobals.itemsPorPagina= 50;
                    AppGlobals.permiso= 0;
                    // Remove the localStorage key/value
                    localStorage.removeItem('PersonalLoggedIn');

                    // Remove Main View
                    me.getView().destroy();

                    // Add the Login Window
                    Ext.create({
                        xtype: 'login'
                    });                
                
                },
                failure: function(response, opts) 
                {
                    console.log("Cerro sesion");
                    console.log(response.responseText);
                    var obj = Ext.decode(response.responseText);
                    Ext.msg.alert("Error", obj.error);
                    var redirect = "../v2/index.php"; 
                    window.location = redirect;
                }
            });
        }
    },

    showWindowChangePass: function() 
    {
        console.log("Ventana para cambiar contraseña");
        var win = Ext.widget('changePassword');
        win.show();
    },
    
    /**
    * Cierra la ventana para cambiar la contraseña
    * @method closeWindowChangePass
    **/
    closeWindowChangePass: function() 
    {
        console.log("Cierra ventana para cambiar contraseña");
        Ext.ComponentQuery.query('changePassword')[0].close();
    },

    onActivateFicha: function(tab, eOpts)
    {
        console.log("activate ficha");
        if(tab.id=="PersonalFichaLicencia")
        {
            console.log("Click en pestaña licencias");
            Ext.getStore('HLicenciaStore').load({
            params: {
                act: 'getAllLicencias',
                legajo: AppGlobals.legAgente
                }
             });

            Ext.getStore('HSolicitudLicenciaStore').load({
            params: {
                act: 'getSolicitudes',
                legajo: AppGlobals.legAgente
                }
             });

            Ext.getStore('HDiasTrabajadosStore').load({
            params: {
                act: 'getDiasTrabajados',
                legajo: AppGlobals.legAgente
                }
             });

        }
        if(tab.id == "PersonalFichaLicencias") 
        {
            console.log("Click en licencias tomadas");
            var sto = Ext.getStore('HLicenciaStore');
            sto.load({
            params: {
                act: 'getAllLicencias',
                legajo: AppGlobals.legAgente
                }
             });
        }
        if(tab.id == "PersonalFichaSolicitudLicencias") 
        {
            console.log("Click en solicitudes de licencias");
            var sto = Ext.getStore('HSolicitudLicenciaStore');
            console.log(sto);
            sto.load({
            params: {
                act: 'getSolicitudes',
                legajo: AppGlobals.legAgente
                }
             });
        }
        if(tab.id == "PersonalFichaDiasTrabajados") 
        {
            console.log("Click en dias trabajados");
            var sto = Ext.getStore('HDiasTrabajadosStore');
            console.log(sto);
            sto.load({
            params: {
                act: 'getDiasTrabajados',
                legajo: AppGlobals.legAgente
                }
             });
        }
        if(tab.id == "PersonalFichaComisiones") 
        {
            console.log("Click en comisiones");
            var sto = Ext.getStore('ComisionStore');
            console.log(sto);
            sto.load({
            params: {
                legajo: AppGlobals.legAgente,
                tarea: 'getComisiones'
                }
             });
        }
        if(tab.id == "PersonalFichaHistoriaClinica") 
        {
            console.log("Click en historia clinica");
            var sto = Ext.getStore('HistoriaClinicaStore');
            console.log(sto);
            sto.load({
            params: {
                legajo: AppGlobals.legAgente,
                tarea: 'getHistoriaClinica'
                }
             });
        }
    },
    /**
    * Alerto al usuario si está por salir de la pestaña sin guardar cambios
    * @method onBeforeTabChange
    **/
    onBeforeTabChange: function(panel, nextTab, oldTab) 
    {
        bReturn = true;
        if (null != oldTab && AppGlobals.isEditing=="si")
        {
            bReturn = false;
            Ext.Msg.show({
                title: 'Advertencia',
                msg: 'Desea salir sin guardar cambios?',
                buttons: Ext.Msg.YESNO,
                icon: Ext.Msg.WARNING,
                closable: false,
                buttonText: {
                    yes: 'Si, adelante!',
                    no: 'No, guadar&eacute; los cambios primero'
                },
                fn: function(buttonId) 
                {
                    if (buttonId == "yes")
                    {
                        AppGlobals.isEditing = "no";
                        panel.setActiveTab(nextTab);
                    }

                }
            });
        }
        return bReturn;
    },
    /**
    * Controla el formulario y cambia la contraseña
    * @method changePass
    * @param  {object} button
    **/
    changePass: function(button)
    {
        console.log("Cambia contraseña");
        var frm = button.up('window').down('form').getForm();
        button.setDisabled(true);
        if(frm.isValid())
        {
        frm.submit(
        {
            url: 'app/proxy.usuarios.php',
            method: 'POST',
            waitMsg: 
            {
                message: 'Procesando...'
            },
            params: 
            {
                act: 'chgPass'
            },
            success: function(frm, action) 
            {
                console.log("***Success: "+action.response.responseText);
                var obj = Ext.decode(action.response.responseText);
                Ext.toast(obj.msg, 'Informaci&oacute;n');
                button.setDisabled(false); //Activo el botón de guardar nuevamente
                button.setText("Guardar");
                Ext.ComponentQuery.query('changePassword')[0].close();
                 
            },
            failure: function(frm, action) 
            {
                console.log("***Failure");
                var obj = Ext.decode(action.response.responseText);
                console.log(obj);
                switch (action.failureType) {
                case Ext.form.action.Action.CLIENT_INVALID:
                    Ext.Msg.alert('Error', 'Complete los campos con valores v&aacute;lidos.');
                    break;
                case Ext.form.action.Action.CONNECT_FAILURE:
                    Ext.Msg.alert('Error', 'Error en la comunicacion con la base de datos. Recargue la p&aacute;gina (F5) e intente nuevamente. ('+action.response.statusText+')');
                    break;
                case Ext.form.action.Action.SERVER_INVALID:
                    Ext.Msg.alert('Error', action.result.msg);
                default:
                    Ext.Msg.alert('Error', obj.error);
            }
            button.setDisabled(false);//Activo el botón de guardar nuevamente
            button.setText("Guardar");
            }
        });
    }
    else
    {
        Ext.Msg.alert('Error', 'Complete los campos con valores v&aacute;lidos.');
        button.setDisabled(false);//Activo el botón de guardar nuevamente
        button.setText("Guardar");
    }
    },
    
    /**
    * Coloca on/off el readonly de todos los fields del form
    * @method setReadOnlyForAll
    * @param  {boolean} readOnly
    * @param  {boolean} leg
    **/
    setReadOnlyForAll: function(readOnly, leg) 
    {
        var fl = this.lookupReference('PersonalFichaLegajo');
        var frm = fl.getActiveTab(0);
        Ext.suspendLayouts();
        
        frm.getForm().getFields().each(function(field){
            if (field.name == "legajo")
            {
                field.setReadOnly(leg);
            }
            else 
            {
                if (field.name != "titulo" && field.name != "cargo" && field.name != "nombrelugar")
                {
                    field.setReadOnly(readOnly);
                }
            }

            
        });
        Ext.resumeLayouts();
    },
    
    /**
    * Coloca on/off el readonly de algunos fields del form. 
    * Este metodo se usa para usuarios con permisos limitados, que solo pueden modificar ciertos campos de su legajo unicamente (por ahora domicilio, telefono y estado civil)
    * @method setReadOnlyForSomeFields
    * @param  {boolean} readOnly
    * @param  {boolean} leg
    **/
    setReadOnlyForSomeFields: function(readOnly) 
    {
    	var fl = this.getPersonalfichalegajo();
        var frm = fl.getActiveTab(0);
        Ext.suspendLayouts();
        
        frm.getForm().getFields().each(function(field)
        {
            if (field.name == "domic" || field.name == "telef" || field.name == "estaciv")
            {
                field.setReadOnly(readOnly);
            }
        });
        Ext.resumeLayouts();
    },
    
    
    /**
    * Vuelve al punto de partida de la aplicacion (bloquea todas las pestañas y limpia las variables globales)
    * @method setInitState
    **/
    setInitState: function() 
    {
        console.log("Estado inicial");
        //Bloqueo todas las petañas hijas de la ficha
        this.lookupReference('PersonalFicha').items.each(function(tab, index){
            tab.setDisabled(true);
        }); 
                
        //Deshabilito botones
        Ext.getCmp('btnEditarAgente').setDisabled(true);
        Ext.getCmp('btnBajaAgente').setDisabled(true);
        
        //Pongo la pestaña de legajo como activa y la limpio
        var tabs = this.lookupReference('PersonalFicha');
        tabs.setActiveTab(0);
        var ficha = this.lookupReference('PersonalFichaLegajo');
        ficha.setActiveTab(0);
        var frm = ficha.getActiveTab();
        frm.getForm().reset();
        
        //Limpio variables globales
        AppGlobals.diasDisponibles= 0;
        AppGlobals.diasLicPorAnio= 0;
        AppGlobals.id_licencia= 0;
        AppGlobals.licencia= 0;
        AppGlobals.nombreAgente = '';
        AppGlobals.legAgente = 0;
        
        //Reinicio encabezado
        this.lookupReference('PersonalFicha').setTitle('Ficha');
    
    },
    
    initLegajo: function() 
    {
    	console.log('*** Init legajo.....');
        this.setReadOnlyForAll(true, true);
    },

    onClickFoto: function() 
    {
        if (AppGlobals.legAgente === undefined)//Si no se seleccionó ningun agente, no hago nada
        {
    	   console.log('No se ha elegido agente');
        }
        else
        {
            console.log('*** Click en la foto');
        	var win = Ext.widget('PersonalWebcam');
        	win.show();

        	var frm = document.getElementById('Cam');
        	var frm = Ext.getDom('Cam');
        	frm.src = "camara/camara.php?legajo="+AppGlobals.legAgente;
        }

    },

	addForm: function(button) 
    {
        if(AppGlobals.permisos[0].cargar_agente == 1)//if(AppGlobals.permiso=="1")
		{
            console.log('*** Nuevo agente');
            //Muestro mensaje temporal 
            Ext.toast('Agregar nuevo empleado al sistema', 'Nuevo empleado');
            
            AppGlobals.isEditing = 'si';

            //Habilito panel de legajo y deshabilito los demas
            Ext.getCmp('PersonalFichaLegajo').setDisabled(false);
            if(AppGlobals.permisos[0].pesta_licencias == 1)
            {
                Ext.getCmp('PersonalFichaLicencias').setDisabled(true);
            }
            //Bloqueo todas las petañas hijas de la ficha
            this.lookupReference('PersonalFicha').items.each(function(tab, index){
                tab.setDisabled(true);
            });
            Ext.getCmp('PersonalFichaLegajo').setDisabled(false);
            this.lookupReference('PersonalFichaLegajo').items.each(function(tab, index){
                tab.setDisabled(true);
            });
            this.lookupReference('PersonalFichaLegajoForm').setDisabled(false);
            
            var tabs = this.lookupReference('PersonalFicha');
            tabs.setActiveTab(0);

            var ficha = this.lookupReference('PersonalFichaLegajo');
            ficha.setActiveTab(0);
            var frm = ficha.getActiveTab();
            frm.getForm().reset();
            
            //Tildo la casilla de retencion
            Ext.getCmp('retencion_ganancias').setValue(true);
            
            this.setReadOnlyForAll(false, false);
            accion = 1;

            Ext.getCmp('btnGuardar').setDisabled(false);
            Ext.getCmp('btnCancelar').setDisabled(false);
            //Ext.getCmp('btnEditarAgente').setDisabled(false);
            Ext.getCmp('btnLimpiar').setDisabled(false);
           // Ext.getCmp('btnBaja').setDisabled(true);
            Ext.getCmp('foto').setSrc('res/img/profile1.jpg');
        }
        else
        {
            Ext.Msg.alert("Alerta", "Opci&oacute;n no permitida");
        }
	},

	editForm: function(button) 
    {
        AppGlobals.isEditing = 'si';
		if(AppGlobals.permisos[0].editar_agente == 1)
        {
            console.log('*** Editar form admin');
            //Muestro mensaje temporal 
            Ext.toast('Editar datos del empleado '+AppGlobals.nombreAgente, 'Editar empleado');
            var tabs = this.lookupReference('PersonalFicha');
            tabs.setActiveTab(0);
            accion = 2;
            var ficha = this.lookupReference('PersonalFichaLegajo');
            ficha.setActiveTab(0);
            var frm = ficha.getActiveTab();
            Ext.getCmp('btnGuardar').setDisabled(false);
            Ext.getCmp('btnCancelar').setDisabled(false);
            Ext.getCmp('btnLimpiar').setDisabled(false);
            //Ext.getCmp('btnBaja').setDisabled(false);
            this.setReadOnlyForAll(false, true);
        }
        else
        {
            if(AppGlobals.legAgente == AppGlobals.legajoUser)
            {
                console.log('*** Editar form usuario comun');
                //Muestro mensaje temporal 
                Ext.toast('Editar empleado','Editar mis datos');
                var tabs = this.getPersonalficha();
                tabs.setActiveTab(0);
                accion = 2;
                var ficha = this.getPersonalfichalegajo();
                ficha.setActiveTab(0);
                var frm = ficha.getActiveTab();
                Ext.getCmp('btnGuardar').setDisabled(false);
                Ext.getCmp('btnCancelar').setDisabled(false);
                this.setReadOnlyForSomeFields(false);
            }
        }
    },

    verTodos: function(button)
    {
        console.log('Ver todos los empleados (activos y dados de baja)');
        if(button.getText() == "Ver todos")
        {
            AppGlobals.showAgentes = 'activosYBajas';
            
            var stoPers = Ext.getStore('PersonalStore');
            
            stoPers.load({
               params: {
                   act: 'activosYBajas'
               } 
            });
            button.setText("Ver solo activos");}
        else
        {
            AppGlobals.showAgentes = '';
            
            var stoPers = Ext.getStore('PersonalStore');
            stoPers.load();
            stoPers.load({
               params: {
                   act: ''
               } 
            });
            button.setText("Ver todos")
        }    
    },

    imprimirAgentes: function(button)
    {
        console.log('Imprimir agentes');
    },

    onTypeSearchAuditoria: function(field, newValue, oldValue, eOpts)
    {
        
    },

    onEnterSearchAuditoria: function(field, e, eOpts)
    {
        if (e.getKey() == e.ENTER) 
        {
            this.onSearchGrid(field.getValue(), this.lookupReference('AuditoriaPanel'), 'server');
        }
    },

    onTypeSearchAgentes: function(field, newValue, oldValue, eOpts)
    {
        this.onSearchGrid(field.getValue(), this.lookupReference('PersonalGrilla'));
    },

    onEnterSearchAgentes: function(field, e, eOpts)
    {
        if (e.getKey() == e.ENTER) 
        {
            this.onSearchGrid(field.getValue(), this.lookupReference('PersonalGrilla'));
        }
    },
    /*
    Funcion que busca en un grid y devuelve los resultados. Hay 2 tipos de busquedas. 
    'local' busca en el store en el cliente, usando filtros de store
    'server' busca en el servidor, enviando los datos por parametro y recargando el store
    */
    onSearchGrid: function(value, grid, typeSearch='local')
    {
        var store = grid.getStore();
        var campos = grid.columns;
        
        if(typeSearch=='local')
        {
            store.clearFilter();
            //grid.mask("Buscando...");
            if(value)    
            {
                store.filterBy(function(record)
                {
                    var retval = false;
                    Ext.each(campos, function(campo)
                    {   
                        //console.log(campo.dataIndex);
                        var rv = record.get(campo.dataIndex);
                        
                        var re = new RegExp(value, 'gi');
                        retval = re.test(rv);
                        if(retval) 
                        { 
                            return false; 
                        }
                    });
                    return retval;
                }, this);
            }
            //grid.unmask();
        }
        else
        {
            var fields = '[';
            Ext.each(campos, function(campo)
            {   
                fields+=campo.dataIndex+",";
            });
            fields = fields.substr(0,fields.length-1);
            fields+=']';
            store.load({
               params: {
                   query: value,
                   fields: fields
               } 
            });
        }
    },

    saveForm: function(button) 
    {
        console.log('*** Guardar form');
        var ficha = this.lookupReference('PersonalFichaLegajo');
        var frm = ficha.getActiveTab(0);
        var f = button.up('form');

        var t = this;
        
        Ext.Msg.show({
            title:'Guardar Cambios?',
            msg: 'Ha intruducido cambios en la ficha personal. Desea guardarlos?',
            buttons: Ext.Msg.YESNOCANCEL,
            icon: Ext.Msg.QUESTION,
            buttonText : 
                {
                    yes : 'Si',
                    no : 'No',
                    cancel : 'Cancelar'
                },
            fn: function (buttonId) 
            {
                if (buttonId==="yes") 
                {
                    //script para obtener los campos invalidos
                    var fieldNames = [];                
                    var fields = ficha.getInvalidFields();
                    if(fields.length>0)//si hay campos invalidos
                    {
                        for(var i=0; i <  fields.length; i++)
                        {
                            var field = fields[i];
                            fieldNames.push(field.getName());
                        }
                        console.debug(fieldNames);
                        Ext.MessageBox.alert('Datos mal cargados', 'Algunos campos est&aacute;n mal cargados');
                    }
                    else//no hay campos invalidos--->submiteo
                    {
                        AppGlobals.isEditing = 'no';
                        Ext.getCmp('btnEditarAgente').setDisabled(false);
                        Ext.getCmp('btnCancelar').setDisabled(true);
                        //Ext.getCmp('btnBaja').setDisabled(true);
                        button.setDisabled(true);
                        t.setReadOnlyForAll(true, true);
                        if (accion == 1) 
                        {
                            console.log('nuevo');
                            //Habilito el resto de los paneles
                            Ext.getCmp('PersonalCargos').setDisabled(false);
                            Ext.getCmp('PersonalLugares').setDisabled(false);
                            Ext.getCmp('PersonalTitulos').setDisabled(false);
                            //Ext.getCmp('PersonalPermisos').setDisabled(false);

                            if(AppGlobals.permisos[0].pesta_legajo_familiares_ver == 1)
                            {
                                Ext.getCmp('PersonalFamilia').setDisabled(false);
                            }

                            if(AppGlobals.permisos[0].pesta_legajo_sanciones_ver == 1)
                            {
                                Ext.getCmp('PersonalSanciones').setDisabled(false);
                            }
                            
                            if(AppGlobals.permisos[0].pesta_legajo_capacitaciones_ver == 1)
                            {
                                Ext.getCmp('PersonalCapacitaciones').setDisabled(false);
                            }

                            if(AppGlobals.permisos[0].pesta_legajo_firmasdigitales_ver == 1)
                            {
                                Ext.getCmp('PersonalFirmasDigitales').setDisabled(false);
                            }

                            frm.submit({
                                url: 'app/proxy.agentes.php',
                                method: 'POST',
                                params: 
                                {
                                    act: "setAgente",
                                    accion: 'agregar'
                                },
                                success: function(frm, action) 
                                {
                                    var obj = Ext.decode(action.response.responseText);
                                    
                                    console.log(action.response.responseText);
                                    
                                    Ext.toast('Datos guardados. Numero de legajo: ' + obj.legajo, 'Informaci&oacute;n');
                                    
                                    //Actualizo variables globales
                                    AppGlobals.dniAgente = obj.nrodoc;
                                    AppGlobals.legAgente = obj.legajo;
                                    AppGlobals.nombreAgente = obj.apellido + ', ' + obj.nombre;
                                    
                                    //Informo con un cartel la creacion del usuario y contraseña
                                    Ext.Msg.show(
                                    {
                                        title : 'Usuario creado',
                                        msg : 'Por favor, tomar nota del usuario y contrase&ntilde;a creados para el nuevo agente, e informarle que debe cambiar la contrase&ntilde;a apenas ingrese al sistema.<br> Usuario: '+obj.user+'<br> Contrase&ntilde;a: '+obj.pass,
                                        width : 500,
                                        closable : false,
                                        buttons : Ext.Msg.OK,
                                        buttonText : 
                                        {
                                            ok : 'Ok, entendido.'
                                            
                                        },
                                        icon : Ext.Msg.INFO
                                    });

                                    // Inicializa la pestaÃ±a de Licencias
                                    var card = Ext.getCmp('pnlLicenciasCard');
                                    card.getLayout().setActiveItem('cardZero');
                                    
                                    //Recargo store
                                    var sto = Ext.getStore('PersonalStore');
                                    sto.reload();
                                },
                                failure: function(frm, action) 
                                {
                                    console.log('failure');
                                    //var obj = Ext.decode(action.response.responseText);
                                    console.log(action.response.responseText);
                                    var obj = Ext.decode(action.response.responseText);
                                    Ext.Msg.alert('Error', obj.error);
                                    Ext.getCmp('btnEditarAgente').setDisabled(false);
                                    Ext.getCmp('btnCancelar').setDisabled(false);
                                    
                                    button.setDisabled(false);
                                    t.setReadOnlyForAll(false, false);
                                }
                            });
                        } 
                        else 
                        {
                            console.log('editar');
                            frm.submit({
                                url: 'app/proxy.agentes.php',
                                method: 'POST',
                                params: 
                                {
                                    act: "editAgente",
                                    accion: 'editar',
                                    legajo: AppGlobals.legAgente
                                },
                                success: function(frm, action) 
                                {
                                    console.log("Entra al success");
                                    var sto = Ext.getStore('PersonalStore');
                                    var obj2 = Ext.decode(action.response.responseText);
                                    sto.reload();
                                    Ext.toast('Se guardaron los datos del agente', 'Informaci&oacute;n');
                                },
                                failure: function(frm, action) 
                                {
                                    console.log("Entra al failure");
                                    var obj2 = Ext.decode(action.response.responseText);
                                    console.log(action.response.responseText);
                                    //var obj2 = Ext.decode(action.response.responseText);
                                    Ext.Msg.alert('Informacion', obj2.error);
                                }
                            });
                        }
                    }
                }
            }
        });
	},

	cancelForm: function(button) 
    {
        console.log('*** Cancela form');
        AppGlobals.isEditing = 'no';
        Ext.getCmp('btnEditarAgente').setDisabled(false);
        Ext.getCmp('btnGuardar').setDisabled(true);
        button.setDisabled(true);
        if(AppGlobals.permisos[0].editar_agente == 1)//if(AppGlobals.permiso === "1")
        {
            Ext.getCmp('btnLimpiar').setDisabled(true);
            this.setReadOnlyForAll(true, true);
        }
        else
        {
            this.setReadOnlyForSomeFields(true);
        }
	},
        
    clearForm: function(button) 
    {
    	console.log('*** Limpiar form');
    	button.up('form').getForm().reset();
	},
        
    /* Ventana de Baja */

	darDeBaja: function(button) 
    {
		console.log('*** Click dar Baja');
        console.log(AppGlobals.registroAgente.activa);
        if(AppGlobals.registroAgente.activa==3)
        {
            Ext.Msg.alert('Error', 'El agente ya fue dado de baja');
        }
        else
        {
            Ext.Msg.show({
                title:'Dar de baja al agente',
                msg: 'Est&aacute; a punto de dar de baja al agente '+AppGlobals.nombreAgente+', legajo '+AppGlobals.legAgente+'. Desea continuar?',
                buttons: Ext.Msg.YESNOCANCEL,
                icon: Ext.Msg.QUESTION,
                buttonText : 
                {
                    yes : 'Si, seguro',
                    no : 'No, no tanto',
                    cancel : 'Cancelar'
                },
                fn: function (buttonId) 
                {
                    if(buttonId==="yes")
                    {   
                        var win = Ext.widget('PersonalBaja');
                        win.show();
                        Ext.getCmp("legajo").setValue(AppGlobals.legAgente);
                        if(AppGlobals.registroAgente.activa==2)
                        {
                            Ext.getCmp("tramite_baja").setValue(true);
                        }
                        else
                        {
                            Ext.getCmp("tramite_baja").setValue(false);
                        }
                    }
                }
            });
        }
    },

    /* Actualizar antiguedades */

    actualizar_antiguedades: function(button) 
    {
        Ext.Msg.show({
            title:'Confirmacion para actualizar antiguedades',
            msg: 'Este proceso actualizar&aacute; las antiguedades de todos los agentes que apliquen. Desea continuar?',
            buttons: Ext.Msg.YESNOCANCEL,
            icon: Ext.Msg.QUESTION,
            fn: function (buttonId) 
            {
                var grilla = this.lookupReference('PersonalGrilla');
                grilla.mask("Actualizando antiguedades, este proceso puede tomar varios minutos. Espere...");
                
                console.log('*** Click actualizar antiguedades');
                Ext.Ajax.request(
                {
                    url: 'app/proxy.agentes.php',
                    params: {
                        act: 'updateSeniority'
                    },
                    success: function(response)
                    {
                        grilla.unmask()
                        var obj = Ext.decode(response.responseText);
                        if(obj.success === true)
                        {
                            Ext.toast(obj.cant_procesados+" agentes procesados. "+obj.cant_actualizados+" antiguedades actualizadas.", 'Informaci&oacute;n');
                        }
                        else
                        {
                            Ext.Msg.alert("Error", obj.error)
                        }
                        var sto = Ext.getStore('PersonalStore');
                        sto.reload();
                        
                    },
                    failure: function(response, opts) 
                    {
                        grilla.unmask()
                        Ext.Msg.alert('Error', 'Error de servidor. <br>Estado: ' + response.status);
                        console.log(response);
                    }
                });
            }
        });
    },    
        
    guardarBaja: function(button)
    {
	    console.log('*** Guardar Baja');
        var frm = button.up('window').down('form').getForm();
        //var ficha = this.getPersonalfichalegajo();
        //var frmLegajo = ficha.getActiveTab(0); //Obtengo la referencia al formulario del legajo
        var me = this;
        //Pregunto una vez mas si se desea dar de baja al empleado
        Ext.Msg.show({
            title:'Dar de baja al agente',
            msg: 'Est&aacute; a punto de dar de baja al agente '+AppGlobals.nombreAgente+', legajo '+AppGlobals.legAgente+'. Seguro que desea continuar? Esta acci&oacute;n es irreversible',
            buttons: Ext.Msg.YESNOCANCEL,
            icon: Ext.Msg.QUESTION,
            buttonText : 
            {
                yes : 'Si, estoy segur&iacute;simo',
                no : 'No, mejor no',
                cancel : 'No, sacame de aqu&iacute;!'
            },
            fn: function (buttonId) 
            {
                if(buttonId==="yes")
                {   
                    if(Ext.getCmp('tramite_baja').checked==true)
                    {
                        Ext.getCmp('fecha_baja').allowBlank = true;
                        Ext.getCmp('acordada_baja').allowBlank = true;
                    }
                    else
                    {
                        Ext.getCmp('fecha_baja').allowBlank = false;
                        Ext.getCmp('acordada_baja').allowBlank = false;
                    }
                    if (frm.isValid() || Ext.getCmp('tramite_baja').checked==true)
                    {
                        console.log(frm);
                        button.setDisabled(true); //Desactivo el boton para que no haya multiples cargas. Bug corregido Mauro
                        Ext.getCmp('btnEditarAgente').setDisabled(false); //Deshabilito boton editar
                        frm.submit({
                            url: 'app/proxy.agentes.php',
                            method: 'POST',
                            params: {
                                act: 'darBaja',
                                legajo: AppGlobals.legAgente
                            },
                            clientValidation: false,
                            success: function(frm, action) {
                                console.log("Entro al success: "+action.response.responseText);
                                Ext.toast('Se dio de baja al agente correctamente', 'Informacion');
                                frm.reset();
                                var sto = Ext.getStore('PersonalStore');
                                sto.reload();
                                Ext.ComponentQuery.query('PersonalBaja')[0].close();
                                me.setInitState();
                                                    
                            },
                            failure: function(frm, action) {
                                console.log(action.failureType);
                                button.setDisabled(false); //Habilito nuevamente el boton
                            }
                        });
                    }
                }
                if(buttonId==="cancel")
                {
                    Ext.ComponentQuery.query('PersonalBaja')[0].close();
                    me.setInitState();
                }
            }
        });
    },
        
    cancelFormBaja: function() 
    {
        console.log('*** Cierra ventana de dar de baja');
        Ext.ComponentQuery.query('PersonalBaja')[0].close();
	},

	/*** CARGOS ***/
	onActivateCargos: function() {
		console.log('*** Activate cargos');
		var sto = Ext.getStore('HCargosStore');
        
		sto.load({
			params: {
				legajo: AppGlobals.legAgente
			}
		});
        console.log(sto);
	},

    selectionChangeCargo: function(view, records)
    {
        if(AppGlobals.permisos[0].pesta_legajo_cargos_editar == 1)
        {
            this.lookupReference('PersonalCargos').down('button[action=eliminarCargo]').setDisabled(!records.length);
        }
    },

	VentanaCargo: function(button) 
    {
		console.log('*** Ventana de cargos');
		var win = Ext.widget('PersonalAgregarCargo');
		win.show();
	},

    eliminaCargo: function(button)
    {
        console.log("Elimina cargo");
        Ext.Msg.show({
            title:'Confirmacion para eliminar',
            msg: 'Seguro que desea eliminar el cargo?',
            buttons: Ext.Msg.YESNOCANCEL,
            icon: Ext.Msg.QUESTION,
            buttonText : 
            {
                yes : 'Si, seguro',
                no : 'No, mejor no',
                cancel : 'Eliminar? No! Sacame de aqu&iacute;!'
            },
            fn: function (buttonId) 
            {
                if(buttonId==='yes')
                {
                    var sm = button.up('PersonalCargos').getSelectionModel();
                    var arr = sm.getSelection();
                    console.log(arr[0].data.id);
                    Ext.Ajax.request(
                    {
                        url: 'app/proxy.cargos.php',
                        
                        params: {
                            idCargoElimin: arr[0].data.id
                            
                        },
                        success:function(response){
                        var obj = Ext.decode(response.responseText);
                        if(obj.success === true)
                        {
                            Ext.toast(obj.msg, 'Informaci&oacute;n');
                        }
                        else
                        {
                            Ext.Msg.alert(obj.error, 'Error')
                        }
                        var sto = Ext.getStore('HCargosStore');
                        sto.reload();
                        var stoGen = Ext.getStore('PersonalStore');
                        stoGen.reload();
                            
                        },
                        failure: function(response, opts) 
                        {
                            Ext.Msg.alert('Error', 'Error de servidor. <br>Estado: ' + response.status);
                            console.log('server-side failure with status code ' + response.status);
                        }
                    });
                    //Ext.getStore('historial.HCargosStore').remove(sm.getSelection());
                    if (Ext.getStore('HCargosStore').getCount() > 0) {
                        sm.select(0);
                    }
                }
                
            }
        });
    },
        
    cancelaCargo: function() 
    {
        console.log('*** Cierra ventana de agregar cargo');
        Ext.ComponentQuery.query('PersonalAgregarCargo')[0].close();
    },

    changeFechaAltaCargo: function(field, newValue, oldValue, eOpts) 
    {
        var fechaDesde = Ext.getCmp('fechaDesdeAgregarCargo').getValue();
        var fechaHasta = Ext.getCmp('fechaHastaAgregarCargo').getValue();
        if(fechaHasta)
        {
            if(fechaDesde >= fechaHasta)
            {
                Ext.Msg.alert('Error', 'Rango incorrecto de fechas');
                Ext.getCmp('fechaHastaAgregarCargo').setValue(""); 
            }
        }
    }, 

	onAgregarCargo: function(button) 
    {
		console.log('*** Agregar cargo al agente');
		var f = this.lookupReference('PersonalAgregarCargo');
		var frm = f.down('form').getForm();
        //script para obtener los campos invalidos
        var fieldNames = [];                
        var fields = f.getInvalidFields();
		if(fields.length>0)//si hay campos invalidos
        {
            for(var i=0; i <  fields.length; i++)
            {
                var field = fields[i];
                fieldNames.push(field.getName());
                console.debug(fieldNames);
                Ext.MessageBox.alert('Datos mal cargados', 'Algunos campos est&aacute;n mal cargados');
            }
        }
        else
        {   //submiteo
            //Desactivo el boton guardar para evitar cargas duplicadas    
            Ext.getCmp('guardarAsigCargo').disabled = true;    
		    frm.submit({
        		url: 'app/proxy.cargos.php',
        		method: 'POST',
        		params: {
        			legajo: AppGlobals.legAgente
        		},
    			success: function(form, action) 
                {
    				var sto = Ext.getStore('HCargosStore');
                    var stoPers = Ext.getStore('PersonalStore');
    				sto.reload();
                    stoPers.reload();
                    console.log("Entra al success");
                    console.log(action.response.responseText);
                    var obj = Ext.decode(action.response.responseText);
    				Ext.toast(obj.msg, 'Informaci&oacute;n');
                    Ext.ComponentQuery.query('PersonalAgregarCargo')[0].close();
    			},
    			failure: function(form, action) 
                {
                    console.log("Entra al failure");
                    console.log(action.response.responseText);
                    var obj = Ext.decode(action.response.responseText);
    				Ext.Msg.alert('Error', obj.error);
                    Ext.getCmp('guardarAsigCargo').disabled = false;    
    			}
		    });
        }
	},
        
    VentanaAltaCargo: function(button) 
    {
    	console.log('*** Ventana de alta de cargos');
    	var win = Ext.widget('PersonalAltaCargo');
    	win.show();
	},

    altaCargo:function(button)
    {
        console.log('*** Alta de cargo');
        var f = this.lookupReference('PersonalAltaCargo');
        var frm = f.down('form').getForm();
        //script para obtener los campos invalidos
        var fieldNames = [];                
        var fields = f.getInvalidFields();
 		if(fields.length>0)//si hay campos invalidos
        {
            
            for(var i=0; i <  fields.length; i++)
            {
                var field = fields[i];
                fieldNames.push(field.getName());
                console.debug(fieldNames);
                Ext.MessageBox.alert('Datos mal cargados', 'Algunos campos est&aacute;n mal cargados');
            }
        }
        else
        {   //submiteo
            frm.submit({
    			url: 'app/proxy.cargos.php',
    			method: 'POST',
    			params: {
    				legajo: AppGlobals.legAgente,
                    act: "altaCargo"
    			},
    			success: function(form, action) 
                {
    				var sto = Ext.getStore('CargosStore');
                    sto.reload();
                    console.log("Entra al success");
                    var obj = Ext.decode(action.response.responseText);
                    console.log(action.response.responseText);
    				Ext.toast(obj.msg, 'Informaci&oacute;n');
                    Ext.ComponentQuery.query('PersonalAltaCargo')[0].close();
    			},
    			failure: function(form, action) 
                {
                    console.log("Entra al failure");
                    console.log(action.response.responseText);
                    var obj = Ext.decode(action.response.responseText);
    				Ext.Msg.alert('Error', obj.error);
    			}
    		});
        }
    },
    cancelaAltaCargo: function() 
    {
        console.log('*** Cierra ventana de alta de cargo');
        Ext.ComponentQuery.query('PersonalAltaCargo')[0].close();
    },

    VentanaBajaCargo:function(button)
    {
        console.log('*** Ventana Baja de cargo');
        var win = Ext.widget('PersonalBajaCargo');
        win.show();
    },

    changeSelectModifCargo: function (combo)
    {
        console.log(combo);
        Ext.getCmp('nombreCargoModif').setValue(combo.rawValue);
        Ext.Ajax.request(
        {
            url: 'app/proxy.cargos.php',
            method: 'GET',
            params: {
                act: 'getCargoById',
                idCargo: combo.value
                
            },
            success:function(response){
                var obj = Ext.decode(response.responseText);
                console.log(obj.results[0].escalafon);
                Ext.getCmp('comboEscalafonModifCargo').setValue(obj.results[0].escalafon);
            },
            failure: function(response, opts) {
                console.log('server-side failure with status code ' + response.status);
            }
        });
    },

    onActivateFamilia: function(tab) 
    {
        console.log('*** Activate familia');
        //console.log(algo);

        //Reinicio formulario
        if(AppGlobals.permisos[0].pesta_legajo_familiares_editar == 1)
        {
            tab.getComponent('formCertificados').getForm().reset();
            this.lookupReference('PersonalFamilia').down('combo[name=anioCertificado]').setValue('0');
            //Obtengo las observaciones de tesoreria
            Ext.Ajax.request(
            {
                url: 'app/proxy.familia.php',
                method: 'GET',
                params: {
                    act: 'getObservacionesTesoreria',
                    legajo: AppGlobals.legAgente
                    
                },
                success:function(response){
                    var obj = Ext.decode(response.responseText);
                    console.log(obj.msg);
                    Ext.ComponentQuery.query('#observaciones_tesoreria')[0].setValue(obj.observaciones_tesoreria);
                    Ext.ComponentQuery.query('#suspendido_asignacion')[0].setValue(obj.susp_asignacion);
                },
                failure: function(response, opts) {
                    console.log('server-side failure with status code ' + response.status);
                }
            });
        }
        var sto = Ext.getStore('FamiliaStore');
        sto.load({
            params: {
                legajo: AppGlobals.legAgente
            }
        });
    },

    guardarCertificados: function(button)
    {
        console.log("Guardar certificados");
        
        var frm = button.up().up().getForm();
        
        frm.submit(
        {
            url: 'app/proxy.familia.php',
            method: 'POST',
            params: {
                act: 'setCertificadosByYearEmployee',
                legajo: AppGlobals.legAgente,
                anio: Ext.getCmp('anioCertificado').rawValue
                
            },
            success: function(form, action){
                var obj = Ext.decode(action.response.responseText);
                
                Ext.toast(obj.msg, 'Informaci&oacute;n');
            },
            failure: function(form, action) {
                console.log("Entra al failure");
                var obj = Ext.decode(action.response.responseText);
                Ext.Msg.alert('Error', obj.error);
            }
        });

    },

    cambiarAnioCertificados: function(button)
    {
        console.log("Cambia los certificados");
        Ext.Ajax.request(
        {
            url: 'app/proxy.familia.php',
            method: 'GET',
            params: {
                act: 'getCurrentYear'
                
            },
            success: function(response)
            {
                var obj = Ext.decode(response.responseText);
                console.log(obj);
                var mensaje = "";
                
                switch(obj.actual)
                {
                    case 'vacio':
                        var fecha = new Date();
                        var ano = fecha.getFullYear();
                        mensaje = obj.msg+" Desea establecer el año actual "+ano+"?";
                        break;
                    default:
                        var ano = parseInt(obj.actual)+1;
                        mensaje = obj.msg+" "+obj.actual+". Desea cambiar al año "+ano+"?";
                        break
                }

                Ext.Msg.show({
                    title:'Confirmacion para cambiar de período',
                    msg: mensaje,
                    buttons: Ext.Msg.YESNO,
                    icon: Ext.Msg.QUESTION,
                    buttonText : 
                    {
                        yes : 'Si',
                        no : 'No'
                    },
                    fn: function (buttonId) 
                    {
                        if(buttonId==='yes')    
                        {   //submiteo
                            console.log("si");
                            
                            Ext.Ajax.request(
                            {
                                url: 'app/proxy.familia.php',
                                method: 'POST',
                                params: {
                                    act: 'closeAllCertificados'
                                },
                                success: function(response){
                                    var obj = Ext.decode(response.responseText);
                                    Ext.toast(obj.msg, "Información");
                                    Ext.getCmp('idPersonalGrillaFamiliares').getStore().reload();
                                    

                                },
                                failure: function(response, opts) {
                                    console.log("Entra al failure");
                                    var obj = Ext.decode(response.responseText);
                                    Ext.toast(obj.error, 'Error');
                                }
                            });
                        }
                    }
                    });                    
                //Ext.toast(obj.msg, 'Informaci&oacute;n');
            },
            failure: function(response, opts) 
            {
                console.log("Entra al failure");
                Ext.Msg.alert("Error al consultar la base de datos");
            }
        });

    },

    VentanaFamiliar: function(button) 
    {
        console.log('*** Ventana de agregar familiar');
        var win = Ext.widget('PersonalAgregarFamiliar');
        win.show();
    },

    onChangeAnioCertificado: function(combo)
    {
        console.log("Cambio el combo año certificado");
        if(combo.value==0)
        {
            console.log(this.lookupReference('PersonalFamilia').down('checkboxgroup').items.items);
            Ext.ComponentQuery.query('#declaracion_jurada_asig')[0].setDisabled(true);
            Ext.ComponentQuery.query('#certificacion_anses')[0].setDisabled(true);
            Ext.ComponentQuery.query('#guardarCert')[0].setDisabled(true);
            
        }
        else
        {
            Ext.ComponentQuery.query('#declaracion_jurada_asig')[0].setDisabled(false);
            Ext.ComponentQuery.query('#certificacion_anses')[0].setDisabled(false);
            Ext.ComponentQuery.query('#guardarCert')[0].setDisabled(false);
            Ext.Ajax.request(
            {
                url: 'app/proxy.familia.php',
                method: 'GET',
                params: {
                    act: 'getCertificadosByYearEmployee',
                    legajo: AppGlobals.legAgente,
                    anio: this.lookupReference('PersonalFamilia').down('combo[name=anioCertificado]').rawValue
                    
                },
                success: function(response){
                    var obj = Ext.decode(response.responseText);
                    if(obj.results.length>0)
                    {
                        Ext.ComponentQuery.query('#declaracion_jurada_asig')[0].setValue(obj.results[0].asignacion_familiar=='1'?true:false);
                        Ext.ComponentQuery.query('#certificacion_anses')[0].setValue(obj.results[0].cert_neg_anses=='1'?true:false);
                    }
                    else
                    {
                        Ext.ComponentQuery.query('#declaracion_jurada_asig')[0].setDisabled(false);
                        Ext.ComponentQuery.query('#certificacion_anses')[0].setDisabled(false);
                    }
                    //sto.reload();
                    
                    //Ext.toast(obj.msg, 'Informaci&oacute;n');
                },
                failure: function(response, opts) {
                    console.log("Entra al failure");
                    var obj = Ext.decode(action.response.responseText);
                    Ext.Msg.alert('Error', obj.error);
                }
            });
        }
    },

    onChangeObsTesoreria: function()
    {
        console.log("Cambio el text area");
        Ext.ComponentQuery.query('#guardarCert')[0].setDisabled(false);
    },

    eliminarFamiliar: function(button) 
    {
        console.log('*** Eliminar familiar');
        Ext.Msg.show({
            title:'Confirmacion para eliminar',
            msg: 'Seguro que desea eliminar el familiar?',
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            buttonText : 
            {
                yes : 'Si, seguro',
                no : 'No, mejor no'
            },
            fn: function (buttonId) 
            {
                if(buttonId==='yes')    
                {   //submiteo
                    var sm = button.up('PersonalGrillaFamiliares').getSelectionModel();
                    var arr = sm.getSelection();
                    
                    var idElim = arr[0].data.id;
                    console.log(idElim); 
                    
                    var sto = Ext.getStore('FamiliaStore');
                    Ext.Ajax.request(
                    {
                        url: 'app/proxy.familia.php',
                        method: 'POST',
                        params: {
                            act: 'deleteFamiliar',
                            idFam: idElim
                            
                        },
                        success: function(response){
                            var obj = Ext.decode(response.responseText);
                            var sto = Ext.getStore('FamiliaStore');
                            sto.reload();
                            
                            Ext.toast(obj.msg, 'Informaci&oacute;n');
                        },
                        failure: function(response, opts) {
                            console.log("Entra al failure");
                            var obj = Ext.decode(action.response.responseText);
                            Ext.Msg.alert('Error', obj.error);
                        }
                    });
                }
            }
        });     
    },

    openWindowModificarFamiliar: function(button)
    {
        console.log("Abrir ventana para modificar");
        var win = Ext.widget('PersonalEditarFamiliar');
        win.show();
        var form = win.items.items[0].form;
        form.reset();
        var sm = button.up('PersonalGrillaFamiliares').getSelectionModel();
        var arr = sm.getSelection();
        console.log(arr[0].data);
        form.setValues(arr[0].data);
        if(arr[0].data.certificado_inicio=='Si')
        {
            Ext.getCmp('certificado_inicio').setValue(true);
        }
        else
        {
            Ext.getCmp('certificado_inicio').setValue(false);
        }
        if(arr[0].data.certificado_fin=='Si')
        {
            Ext.getCmp('certificado_fin').setValue(true);
        }
        else
        {
            Ext.getCmp('certificado_fin').setValue(false);
        }
        if(arr[0].data.guardapolvo=='Si')
        {
            Ext.getCmp('guardapolvo').setValue(true);
        }
        else
        {
            Ext.getCmp('guardapolvo').setValue(false);
        }

        if(arr[0].data.relacion=="2" || arr[0].data.relacion=="7") // si es hijo o nieto, habilito los certificados
        {
            Ext.getCmp('certificado_inicio').setDisabled(false);
            Ext.getCmp('certificado_fin').setDisabled(false);
            Ext.getCmp('guardapolvo').setDisabled(false);
        }

    },

    onCancelarEditarFamiliar: function ()
    {
        Ext.ComponentQuery.query('PersonalEditarFamiliar')[0].close();
    },
    
    onChangeRelacion: function(combo)
    {
        console.log("Cambio el combo");
        if(combo.value==2 || combo.value==7)
        {
            Ext.getCmp('certificado_inicio').setDisabled(false);
            Ext.getCmp('certificado_fin').setDisabled(false);
            Ext.getCmp('guardapolvo').setDisabled(false);
        }
        else
        {
            Ext.getCmp('certificado_inicio').setDisabled(true);
            Ext.getCmp('certificado_fin').setDisabled(true);   
            Ext.getCmp('guardapolvo').setDisabled(true); 
        }
    },

    onGuardarEditarFamiliar: function()
    {
        console.log('*** Edita familiar');
        var f = this.lookupReference('PersonalEditarFamiliar');
        var frm = f.down('form').getForm();
        console.log(frm);
        //script para obtener los campos invalidos
        var fieldNames = [];                
        var fields = f.getInvalidFields();
        if(fields.length>0)//si hay campos invalidos
        {
            for(var i=0; i <  fields.length; i++)
            {
                var field = fields[i];
                fieldNames.push(field.getName());
                console.debug(fieldNames);
                Ext.MessageBox.alert('Datos mal cargados', 'Algunos campos est&aacute;n mal cargados');
            }
        }
        else
        {   //submiteo
            //Desactivo el boton guardar para evitar cargas duplicadas    
            Ext.getCmp('guardarEditFamiliar').disabled = true;   

            var sm = Ext.getCmp('idPersonalGrillaFamiliares').getSelectionModel();
            var arr = sm.getSelection();
            frm.submit({
                url: 'app/proxy.familia.php',
                method: 'POST',
                params: {
                    act: 'editFamiliar',
                    id_fam: arr[0].data.id,
                    legajo: AppGlobals.legAgente,
                    id_certificado: arr[0].data.id_certificado
                },
                success: function(form, action) 
                {
                    var sto = Ext.getStore('FamiliaStore');
                    sto.reload();
                    
                    var obj = Ext.decode(action.response.responseText);
                    Ext.toast(obj.msg, 'Informaci&oacute;n');
                    Ext.ComponentQuery.query('PersonalEditarFamiliar')[0].close();
                },
                failure: function(form, action) 
                {
                    console.log("Entra al failure");
                    console.log(action.response.responseText);
                    var obj = Ext.decode(action.response.responseText);
                    Ext.Msg.alert('Error', obj.error);
                    Ext.getCmp('guardarEditFamiliar').disabled = false;    
                }
            });
        }
    },

    onAgregarFamiliar: function(button) 
    {
        console.log('*** Agregar familiar al agente');
        var f = this.lookupReference('PersonalAgregarFamiliar');
        var frm = f.down('form').getForm();
        console.log(frm);
        //script para obtener los campos invalidos
        var fieldNames = [];                
        var fields = f.getInvalidFields();
        if(fields.length>0)//si hay campos invalidos
        {
            for(var i=0; i <  fields.length; i++)
            {
                var field = fields[i];
                fieldNames.push(field.getName());
                console.debug(fieldNames);
                Ext.MessageBox.alert('Datos mal cargados', 'Algunos campos est&aacute;n mal cargados');
            }
        }
        else
        {   //submiteo
            //Desactivo el boton guardar para evitar cargas duplicadas    
            Ext.getCmp('guardarAsigFamiliar').disabled = true;    
            frm.submit({
                url: 'app/proxy.familia.php',
                method: 'POST',
                params: {
                    act: 'setFamiliar',
                    legajo: AppGlobals.legAgente
                },
                success: function(form, action) 
                {
                    var sto = Ext.getStore('FamiliaStore');
                    sto.reload();
                    var obj = Ext.decode(action.response.responseText);
                    Ext.toast(obj.msg, 'Informaci&oacute;n');
                    Ext.ComponentQuery.query('PersonalAgregarFamiliar')[0].close();
                },
                failure: function(form, action) 
                {
                    console.log("Entra al failure");
                    console.log(action.response.responseText);
                    var obj = Ext.decode(action.response.responseText);
                    Ext.Msg.alert('Error', obj.error);
                    Ext.getCmp('guardarAsigFamiliar').disabled = false;    
                }
            });
        }
    },

    onCancelarFamiliar: function ()
    {
        Ext.ComponentQuery.query('PersonalAgregarFamiliar')[0].close();
    },

    activateBtnEliminarAndEditarFamiliar: function(selectionModel, records)
    {
        if(AppGlobals.permisos[0].pesta_legajo_familiares_editar == 1)
        {
            Ext.getCmp('eliminarFamiliar').setDisabled(!records.length);
            Ext.getCmp('modificarFamiliar').setDisabled(!records.length);
        }
    }, 

    bajaCargo:function(button)
    {
        console.log('*** Baja de cargo');
        var f = this.lookupReference('PersonalBajaCargo');
        var frm = f.down('form').getForm();
        var sto = Ext.getStore('CargosStore');
        //script para obtener los campos invalidos
        var fieldNames = [];                
        var fields = f.getInvalidFields();
        if(fields.length>0)//si hay campos invalidos
        {
            for(var i=0; i <  fields.length; i++)
            {
                var field = fields[i];
                fieldNames.push(field.getName());
                console.debug(fieldNames);
                Ext.MessageBox.alert('Datos mal cargados', 'Algunos campos est&aacute;n mal cargados');
            }
        }
        else
        {
            Ext.Msg.show({
                title:'Confirmacion para eliminar',
                msg: 'Seguro que desea eliminar el cargo del sistema?',
                buttons: Ext.Msg.YESNOCANCEL,
                icon: Ext.Msg.QUESTION,
                buttonText : 
                {
                    yes : 'Si, seguro',
                    no : 'No, mejor no',
                    cancel : 'Eliminar? No! Sacame de aqu&iacute;!'
                },
                fn: function (buttonId) 
                {
                    if(buttonId==='yes')    
                    {   //submiteo
                        frm.submit({
                            url: 'app/proxy.cargos.php',
                            method: 'POST',
                            params: {
                                legajo: AppGlobals.legAgente,
                                act: "bajaCargo"
                            },
                            success: function(form, action) 
                            {
                                var sto = Ext.getStore('CargosStore');
                                sto.reload();
                                console.log("Entra al success");
                                var obj = Ext.decode(action.response.responseText);
                                console.log(action.response.responseText);
                                Ext.toast(obj.msg, 'Informaci&oacute;n');
                                Ext.ComponentQuery.query('PersonalBajaCargo')[0].close();
                            },
                            failure: function(form, action) 
                            {
                                console.log("Entra al failure");
                                console.log(action.response.responseText);
                                var obj = Ext.decode(action.response.responseText);
                                Ext.Msg.alert('Error', obj.error);
                            }
                        });
                    } //endif buttonid===yes
                }
            }); //end Ext.Msg.show
        }//end else
    },

    cancelaBajaCargo: function() 
    {
    	console.log('*** Cierra ventana de baja de cargo');
    	Ext.ComponentQuery.query('PersonalBajaCargo')[0].close();
                
	},
        
    VentanaModificarCargo:function(button)
    {
        console.log('*** Ventana modifica cargo');
        var win = Ext.widget('PersonalModificarCargo');
        win.show();
    },

    modificaCargo:function(button)
    {
        console.log('*** Modifica cargo');
        var f = this.lookupReference('PersonalModificarCargo');
        var frm = f.down('form').getForm();
        //script para obtener los campos invalidos
        var fieldNames = [];                
        var fields = f.getInvalidFields();
        if(fields.length>0)//si hay campos invalidos
        {
            for(var i=0; i <  fields.length; i++)
            {
                var field = fields[i];
                fieldNames.push(field.getName());
                console.debug(fieldNames);
                Ext.MessageBox.alert('Datos mal cargados', 'Algunos campos est&aacute;n mal cargados');
            }
        }
        else
        {
            Ext.Msg.show({
                title:'Confirmacion para modificar',
                msg: 'Seguro que desea modificar el cargo?',
                buttons: Ext.Msg.YESNOCANCEL,
                icon: Ext.Msg.QUESTION,
                buttonText : 
                {
                    yes : 'Si, seguro',
                    no : 'No, mejor no',
                    cancel : 'Cancelar'
                },
                fn: function (buttonId) 
                {
                    if(buttonId==='yes')    
                    {   //submiteo
                        frm.submit({
                        url: 'app/proxy.cargos.php',
                        method: 'POST',
                        params: {
                                legajo: AppGlobals.legAgente,
                                act: "modificaCargo"
                        },
                        success: function(form, action) {
                                var sto = Ext.getStore('CargosStore');
                                sto.reload();
                                console.log("Entra al success");
                                var obj = Ext.decode(action.response.responseText);
                                console.log(action.response.responseText);
                                Ext.toast(obj.msg, 'Informaci&oacute;n');
                                Ext.ComponentQuery.query('PersonalModificarCargo')[0].close();
                        },
                        failure: function(form, action) {
                                console.log("Entra al failure");
                                console.log(action.response.responseText);
                                var obj = Ext.decode(action.response.responseText);
                                Ext.Msg.alert('Error', obj.error);
                        }
                        });
                    } //endif buttonid===yes
                }
            }); //end Ext.Msg.show
        }//end else
    },
    cancelaModificaCargo: function() 
    {
		console.log('*** Cierra ventana de modificar cargo');
		Ext.ComponentQuery.query('PersonalModificarCargo')[0].close();
                
	},

	/*** LUGARES ***/
	onActivateLugares: function() 
    {
		console.log('*** Activate lugares');
		var sto = Ext.getStore('HLugarStore');
		sto.load({
			params: {
				legajo: AppGlobals.legAgente
			}
		});
	},
    selectionChangeLugares: function(view, records)
    {
        if(AppGlobals.permisos[0].pesta_legajo_lugares_editar == 1)
        {
            this.lookupReference('PersonalLugares').down('button[action=eliminarLugar]').setDisabled(!records.length);
        }
    },
    eliminaLugar: function(button)
    {
        console.log("Elimina lugar");
        Ext.Msg.show({
            title:'Confirmacion para eliminar',
            msg: 'Seguro que desea eliminar el lugar?',
            buttons: Ext.Msg.YESNOCANCEL,
            icon: Ext.Msg.QUESTION,
            buttonText : 
            {
                yes : 'Si, seguro',
                no : 'No, mejor no',
                cancel : 'Eliminar? No! Sacame de aqu&iacute;!'
            },
            fn: function (buttonId) 
            {
                if(buttonId==='yes')
                {
                    var sm = button.up('PersonalLugares').getSelectionModel();
                    var arr = sm.getSelection();
                    console.log(arr[0].data.id);
                    Ext.Ajax.request(
                        {
                            url: 'app/proxy.lugar.php',
                            
                            params: {
                                idLugarElimin: arr[0].data.id
                                
                            },
                            success:function(response)
                            {
                                var obj = Ext.decode(response.responseText);
                                if(obj.success === true)
                                {
                                    Ext.toast(obj.msg, 'Informaci&oacute;n');
                                }
                                else
                                {
                                    Ext.Msg.alert("Error", obj.error)
                                }
                                var sto = Ext.getStore('HLugarStore');
                                sto.reload();
                                var stoGen = Ext.getStore('PersonalStore');
                                stoGen.reload();
                            },
                            failure: function(response, opts) 
                            {
                                Ext.Msg.alert('Error', 'Error de servidor. <br>Estado: ' + response.status);
                                console.log('server-side failure with status code ' + response.status);
                            }
                        });
                    //Ext.getStore('historial.HCargosStore').remove(sm.getSelection());
                    if (Ext.getStore('HLugarStore').getCount() > 0) 
                    {
                        sm.select(0);
                    }
                }
            }
        });
    },

	VentanaLugar: function(button) 
    {
		console.log('*** Ventana de lugares');
		var win = Ext.widget('PersonalAgregarLugar');
		win.show();
	},
        
    cancelaLugar: function() 
    {
    	console.log('*** Cierra ventana de agregar lugar');
    	Ext.ComponentQuery.query('PersonalAgregarLugar')[0].close();
                
	},
    
    changeFechaAltaLugar: function(field, newValue, oldValue, eOpts) 
    {
        var fechaDesde = Ext.getCmp('fechaDesdeAgregarLugar').getValue();
        var fechaHasta = Ext.getCmp('fechaHastaAgregarLugar').getValue();
        if(fechaHasta)
        {
            if(fechaDesde >= fechaHasta)
            {
                Ext.Msg.alert('Error', 'Rango incorrecto de fechas');
                Ext.getCmp('fechaHastaAgregarLugar').setValue(""); 
            }
        }
    },   

	onAgregarLugar: function(button) 
    {
		console.log('*** Agregar lugar al agente');
		var f = this.lookupReference('PersonalAgregarLugar');
		var frm = f.down('form').getForm();
                
        //script para obtener los campos invalidos
        var fieldNames = [];                
        var fields = f.getInvalidFields();
 		if(fields.length>0)//si hay campos invalidos
        {
            for(var i=0; i <  fields.length; i++)
            {
                var field = fields[i];
                fieldNames.push(field.getName());
                console.debug(fieldNames);
                Ext.MessageBox.alert('Datos mal cargados', 'Algunos campos est&aacute;n mal cargados');
            }
        }
        else //submiteo
        {
            //Desactivo el boton guardar para evitar cargas duplicadas    
            Ext.getCmp('guardarAsigLugar').disabled = true;
		    frm.submit({
                url: 'app/proxy.lugar.php',
            	method: 'POST',
            	params: {
            		legajo: AppGlobals.legAgente
            	},
        		success: function(form, action) 
                {
        			var sto = Ext.getStore('HLugarStore');
        			sto.reload();
                    var stoPers = Ext.getStore('PersonalStore');
        			stoPers.reload();
                    Ext.toast('Se agreg&oacute; un nuevo lugar al agente', 'Informaci&oacute;n');
        			Ext.ComponentQuery.query('PersonalAgregarLugar')[0].close();
        		},
        		failure: function(form, action) 
                {
                    var obj = Ext.decode(action.response.responseText);
        			Ext.Msg.alert('Error', obj.error);
                    Ext.getCmp('guardarAsigLugar').disabled = false;
        		}
		    });
        }
	},

    VentanaAltaLugar: function(button) 
    {
    	console.log('*** Ventana de alta de lugares');
    	var win = Ext.widget('PersonalAltaLugar');
    	win.show();
	},

    altaLugar:function(button)
    {
        console.log('*** Alta de lugar');
        var f = this.lookupReference('PersonalAltaLugar');
	    var frm = f.down('form').getForm();
        //script para obtener los campos invalidos
        var fieldNames = [];                
        var fields = f.getInvalidFields();
 		if(fields.length>0)//si hay campos invalidos
        {
            for(var i=0; i <  fields.length; i++)
            {
                var field = fields[i];
                fieldNames.push(field.getName());
                console.debug(fieldNames);
                Ext.MessageBox.alert('Datos mal cargados', 'Algunos campos est&aacute;n mal cargados');
            }
        }
        else
        {//submiteo
            frm.submit({
    			url: 'app/proxy.lugar.php',
    			method: 'POST',
    			params: {
    				act: "altaLugar"
    			},
    			success: function(form, action) 
                {
    				var sto = Ext.getStore('LugarStore');
                    sto.reload();
                    console.log("Entra al success");
                    var obj = Ext.decode(action.response.responseText);
                    console.log(action.response.responseText);
                    Ext.toast(obj.msg, 'Informaci&oacute;n');

                    Ext.ComponentQuery.query('PersonalAltaLugar')[0].close();
    			},
    			failure: function(form, action) 
                {
                    console.log("Entra al failure");
                    console.log(action.response.responseText);
                    var obj = Ext.decode(action.response.responseText);
    				Ext.Msg.alert('Error', obj.error);
    			}
    		});
        }
    },

    cancelaAltaLugar: function() 
    {
    	console.log('*** Cierra ventana de alta de lugar');
    	Ext.ComponentQuery.query('PersonalAltaLugar')[0].close();
                
	},

    VentanaBajaLugar:function(button)
    {
        console.log('*** Ventana Baja de lugar');
        var win = Ext.widget('PersonalBajaLugar');
        win.show();
    },

    bajaLugar:function(button)
    {
        console.log('*** Baja de lugar');
        var f = this.lookupReference('PersonalBajaLugar');
        var frm = f.down('form').getForm();
        //script para obtener los campos invalidos
        var fieldNames = [];                
        var fields = f.getInvalidFields();
        if(fields.length>0)//si hay campos invalidos
        {
            for(var i=0; i <  fields.length; i++)
            {
                var field = fields[i];
                fieldNames.push(field.getName());
                console.debug(fieldNames);
                Ext.MessageBox.alert('Datos mal cargados', 'Algunos campos est&aacute;n mal cargados');
            }
        }
        else
        {
            Ext.Msg.show({
                title:'Confirmacion para eliminar',
                msg: 'Seguro que desea eliminar el lugar del sistema?',
                buttons: Ext.Msg.YESNOCANCEL,
                icon: Ext.Msg.QUESTION,
                buttonText : 
                {
                    yes : 'Si, seguro',
                    no : 'No, mejor no',
                    cancel : 'Eliminar? No! Sacame de aqu&iacute;!'
                },
                fn: function (buttonId) 
                {
                    if(buttonId==='yes')    
                    {   //submiteo
                        frm.submit({
                            url: 'app/proxy.lugar.php',
                            method: 'POST',
                            params: {
                                act: "bajaLugar"
                            },
                            success: function(form, action) 
                            {
                                var sto = Ext.getStore('LugarStore');
                                sto.reload();
                                console.log("Entra al success");
                                var obj = Ext.decode(action.response.responseText);
                                console.log(action.response.responseText);
                                Ext.toast(obj.msg, 'Informaci&oacute;n');
                                Ext.ComponentQuery.query('PersonalBajaLugar')[0].close();
                            },
                            failure: function(form, action) 
                            {
                                console.log("Entra al failure");
                                console.log(action.response.responseText);
                                var obj = Ext.decode(action.response.responseText);
                                Ext.Msg.alert('Error', obj.error);
                            }
                        });
                    } //endif buttonid===yes
                }
            }); //end Ext.Msg.show
        }//end else
    },

    cancelaBajaLugar: function() 
    {
    	console.log('*** Cierra ventana de baja de lugar');
    	Ext.ComponentQuery.query('PersonalBajaLugar')[0].close();
                
	},

    VentanaModificarLugar:function(button)
    {
        console.log('*** Ventana modifica lugar');
        var win = Ext.widget('PersonalModificarLugar');
        win.show();
    },

    modificaLugar:function(button)
    {
        console.log('*** Modifica lugar');
        var f = this.lookupReference('PersonalModificarLugar');
        var frm = f.down('form').getForm();
        //script para obtener los campos invalidos
        var fieldNames = [];                
        var fields = f.getInvalidFields();
        if(fields.length>0)//si hay campos invalidos
        {
            for(var i=0; i <  fields.length; i++)
            {
                var field = fields[i];
                fieldNames.push(field.getName());
                console.debug(fieldNames);
                Ext.MessageBox.alert('Datos mal cargados', 'Algunos campos est&aacute;n mal cargados');
            }
        }
        else
        {
            Ext.Msg.show({
            title:'Confirmacion para modificar',
            msg: 'Seguro que desea modificar el lugar?',
            buttons: Ext.Msg.YESNOCANCEL,
            icon: Ext.Msg.QUESTION,
            buttonText : 
            {
                yes : 'Si, seguro',
                no : 'No, mejor no',
                cancel : 'Cancelar'
            },
            fn: function (buttonId) 
            {
                if(buttonId==='yes')    
                {   //submiteo
                    frm.submit({
                        url: 'app/proxy.lugar.php',
                        method: 'POST',
                        params: {
                            legajo: AppGlobals.legAgente,
                            act: "modificaLugar"
                        },
                        success: function(form, action) {
                            var sto = Ext.getStore('LugarStore');
                            sto.reload();
                            console.log("Entra al success");
                            var obj = Ext.decode(action.response.responseText);
                            console.log(action.response.responseText);
                            Ext.toast(obj.msg, 'Informaci&oacute;n');
                            Ext.ComponentQuery.query('PersonalModificarLugar')[0].close();
                        },
                        failure: function(form, action) {
                            console.log("Entra al failure");
                            console.log(action.response.responseText);
                            var obj = Ext.decode(action.response.responseText);
                            Ext.Msg.alert('Error', obj.error);
                        }
                    });
                } //endif buttonid===yes
            }
            }); //end Ext.Msg.show
        }//end else
    },

    cancelaModificaLugar: function() 
    {
    	console.log('*** Cierra ventana de modificar lugar');
    	Ext.ComponentQuery.query('PersonalModificarLugar')[0].close();
                
	},
    /*TITULOS*/
    onActivateTitulos: function() 
    {
    	console.log('*** Activate titulos');
    	var sto = Ext.getStore('HTituloStore');
        
    	sto.load({
    		params: {
    			legajo: AppGlobals.legAgente,
                accion: "titulos"
    		}
    	});
        console.log(sto);
	},

    changeSelectModifLugar: function (combo)
    {
        console.log(combo);
        Ext.getCmp('nombreLugarModif').setValue(combo.rawValue);
        Ext.Ajax.request(
        {
            url: 'app/proxy.lugar.php',
            method: 'GET',
            params: {
                act: 'getLugarById',
                idLugar: combo.value
                
            },
            success:function(response){
                var obj = Ext.decode(response.responseText);
                console.log(obj.results[0].nro_fuero);
                Ext.getCmp('comboFueroModifLugar').setValue(obj.results[0].nro_fuero);
            },
            failure: function(response, opts) {
                console.log('server-side failure with status code ' + response.status);
            }
        });
    },

    eliminaTitulo: function(button)
    {
        console.log("Elimina titulo");
        Ext.Msg.show({
            title:'Confirmacion para eliminar',
            msg: 'Seguro que desea eliminar el titulo?',
            buttons: Ext.Msg.YESNOCANCEL,
            icon: Ext.Msg.QUESTION,
            buttonText : 
            {
                yes : 'Si, seguro',
                no : 'No, mejor no',
                cancel : 'Eliminar? No! Sacame de aqu&iacute;!'
            },
            fn: function (buttonId) 
            {
                if(buttonId==='yes')
                {
                    var sm = button.up('PersonalTitulos').getSelectionModel();
                    var arr = sm.getSelection();
                    console.log(arr[0].data.id);
                    Ext.Ajax.request(
                        {
                            url: 'app/proxy.titulo.php',
                            params: {
                                idTituloElimin: arr[0].data.id
                                
                            },
                            success:function(response)
                            {
                                var obj = Ext.decode(response.responseText);
                                if(obj.success === true)
                                {
                                    Ext.toast(obj.msg, "Informacion");
                                }
                                else
                                {
                                    Ext.Msg.alert(obj.error, "Error")
                                }
                                var sto = Ext.getStore('HTituloStore');
                                sto.reload();
                                //var stoGen = Ext.getStore('TituloStore');
                                //stoGen.reload();
                            },
                            failure: function(response, opts) 
                            {
                                Ext.Msg.alert('Error', 'Error de servidor. <br>Estado: ' + response.status);
                                console.log('server-side failure with status code ' + response.status);
                            }
                        });
                    //Ext.getStore('historial.HCargosStore').remove(sm.getSelection());
                    if (Ext.getStore('HTituloStore').getCount() > 0) {
                        sm.select(0);
                    }
                }
                
            }
        });
    },

    clickTitulo: function(grid, recordHist) 
    {
        console.log("click licencia");
        //AppGlobals.licencia = recordHist.data;
        if(AppGlobals.permisos[0].pesta_legajo_titulos_editar == 1)
        {
            console.log(recordHist.data);
            //AppGlobals.licencia = recordHist.data;
        }
    },

    selectionChangeTitulo: function(view, records)
    {
        if(AppGlobals.permisos[0].pesta_legajo_titulos_editar == 1)
        {
            this.lookupReference('PersonalTitulos').down('button[action=eliminarTitulo]').setDisabled(!records.length);
            this.lookupReference('PersonalTitulos').down('button[action=editarTitulo]').setDisabled(!records.length);
            this.lookupReference('PersonalTitulos').down('button[action=guardarCambiosTitulo]').setDisabled(!records.length);
        }
    },

    VentanaTitulo: function(button) 
    {
    	console.log('*** Ventana de titulos');
    	var win = Ext.widget('PersonalAgregarTitulo');
    	win.show();
    },

    modificarTituloAgente: function (button)
    {
        //console.log(button.up().up());
        button.up().up().plugins[0].cancelEdit();
        var sm = button.up().up().getSelectionModel();
        var arr = sm.getSelection();
        if(arr.length > 0)
        {
            var id_edit = parseInt(arr[0].index);
            button.up().up().plugins[0].startEdit(id_edit, 0);
        }
    },
    guardarCambiosTitulo: function(button)
    {
        console.log("Guardar cambios titulo");
        var modRecords = button.up().up().getStore('HTituloStore').getModifiedRecords();
        var sto = button.up().up().getStore('HTituloStore');
        
        console.log(modRecords);
        //Si hubo modificaciones, guardo los cambios
        if(!Ext.isEmpty(modRecords))
        {
            var recordsToSend = [];
            sto.each(function(record)
            {
                recordsToSend.push(record.data);
            });
            
            button.up().up().el.mask('Guardando...', 'x-mask-loading'); 
                            
            recordsToSend = Ext.encode(recordsToSend); 
            console.log(recordsToSend);
            //console.log("Tipo de datos: "+typeof recordsToSend);
            Ext.Ajax.request({      
            url : 'app/proxy.titulo.php',
            method: 'POST',
            params: {
                records : recordsToSend,
                act: 'editTituloAgente'
            },
            
            success : function(response) 
            {
                var obj = Ext.decode(response.responseText);
                console.log(response.responseText);
                
                button.up().up().el.unmask();
                button.up().up().getStore().commitChanges();
                Ext.toast(obj.msg, "Informaci&oacute;n");
                
                //Recargo la grilla principal y la de evaluados para reflejar los cambios
                Ext.getStore('HTituloStore').reload();
            },
            failure: function(response)
            {
                //var obj = Ext.decode(response.responseText);
                console.log("Entra al failure "+response.responseText);
                button.up().up().el.unmask();
                button.up().up().getStore().commitChanges();
                Ext.toast("Error al guardar cambios: "+obj.error, "Error");
            }
            });
        }
        else
        {
            Ext.toast("No hay cambios para guardar", "Error");
        }
    },
        
    onAgregarTitulo: function(button) 
    {
    	console.log('*** Agregar titulo al agente');
    	var f = this.lookupReference('PersonalAgregarTitulo');
    	var frm = f.down('form').getForm();
                
        //script para obtener los campos invalidos
        var fieldNames = [];                
        var fields = f.getInvalidFields();
    	if(fields.length>0)//si hay campos invalidos
        {
            for(var i=0; i <  fields.length; i++)
            {
                var field = fields[i];
                fieldNames.push(field.getName());
                console.debug(fieldNames);
                Ext.MessageBox.alert('Datos mal cargados', 'Algunos campos est&aacute;n mal cargados');
            }
        }
        else //submiteo
        {
            //Desactivo el boton guardar para evitar cargas duplicadas    
            Ext.getCmp('guardarAsigTitulo').disabled = true;    
            frm.submit({
                url: 'app/proxy.titulo.php',
                method: 'POST',
                params: {
                    legajo: AppGlobals.legAgente
                },
        		success: function(form, action) 
                {
                    console.log("success");
                    console.log(action.response.responseText);
        			var sto = Ext.getStore('HTituloStore');
        			sto.reload();
                    var stoPers = Ext.getStore('PersonalStore');
        			stoPers.reload();
                    var obj = Ext.decode(action.response.responseText);
        			Ext.toast(obj.msg, "Informacion");
                    Ext.ComponentQuery.query('PersonalAgregarTitulo')[0].close();
        		},
        		failure: function(form, action) 
                {
                    console.log("failure");
                    console.log(action.response.responseText);
                    var obj = Ext.decode(action.response.responseText);
        			Ext.Msg.alert('Error', obj.error);
        		}
           });
        }
	},
        
    cancelaTitulo: function() 
    {
        console.log('*** Cierra ventana de agregar titulo');
        Ext.ComponentQuery.query('PersonalAgregarTitulo')[0].close();
    },

    VentanaAltaTitulo: function(button) 
    {
		console.log('*** Ventana de alta de titulos');
		var win = Ext.widget('PersonalAltaTitulo');
		win.show();
	},

    altaTitulo:function(button)
    {
        console.log('*** Alta de titulo');
        var f = this.lookupReference('PersonalAltaTitulo');
        var frm = f.down('form').getForm();
        //script para obtener los campos invalidos
        var fieldNames = [];                
        var fields = f.getInvalidFields();
 		if(fields.length>0)//si hay campos invalidos
        {
            for(var i=0; i <  fields.length; i++)
            {
                var field = fields[i];
                fieldNames.push(field.getName());
                console.debug(fieldNames);
                Ext.MessageBox.alert('Datos mal cargados', 'Algunos campos est&aacute;n mal cargados');
            }
        }
        else
        {//submiteo
            frm.submit(
            {
        		url: 'app/proxy.titulo.php',
        		method: 'POST',
        		params: {
        			act: "altaTitulo"
        		},
                success: function(form, action) 
                {
        			var sto = Ext.getStore('TipoEstudioStore');
                    sto.reload();
                    console.log("Entra al success");
                    var obj = Ext.decode(action.response.responseText);
                    console.log(action.response.responseText);
        			Ext.toast(obj.msg, "Informacion");
                    Ext.ComponentQuery.query('PersonalAltaTitulo')[0].close();
                },
            	failure: function(form, action) 
                {
                    console.log("Entra al failure");
                    console.log(action.response.responseText);
                    var obj = Ext.decode(action.response.responseText);
            		Ext.Msg.alert('Error', obj.error);
            	}
            });
        }
    },
    cancelaAltaTitulo: function() 
    {
		console.log('*** Cierra ventana de alta de titulo');
		Ext.ComponentQuery.query('PersonalAltaTitulo')[0].close();
                
	},
        
    VentanaBajaTitulo:function(button)
    {
        console.log('*** Ventana Baja de titulo');
        var win = Ext.widget('PersonalBajaTitulo');
        win.show();
    },
        
    bajaTitulo:function(button)
    {
        console.log('*** Baja de titulo');
        var f = this.lookupReference('PersonalBajaTitulo');
        var frm = f.down('form').getForm();
        //script para obtener los campos invalidos
        var fieldNames = [];                
        var fields = f.getInvalidFields();
        if(fields.length>0)//si hay campos invalidos
        {
            for(var i=0; i <  fields.length; i++)
            {
                var field = fields[i];
                fieldNames.push(field.getName());
                console.debug(fieldNames);
                Ext.MessageBox.alert('Datos mal cargados', 'Algunos campos est&aacute;n mal cargados');
            }
        }
        else
        {
            Ext.Msg.show({
                title:'Confirmacion para eliminar',
                msg: 'Seguro que desea eliminar el titulo del sistema?',
                buttons: Ext.Msg.YESNOCANCEL,
                icon: Ext.Msg.QUESTION,
                buttonText : 
                {
                    yes : 'Si, seguro',
                    no : 'No, mejor no',
                    cancel : 'Eliminar? No! Sacame de aqu&iacute;!'
                },
                fn: function (buttonId) 
                {
                    if(buttonId==='yes')    
                    {   //submiteo
                        frm.submit({
                            url: 'app/proxy.titulo.php',
                            method: 'POST',
                            params: {
                                act: "bajaTitulo"
                            },
                            success: function(form, action) 
                            {
                                var sto = Ext.getStore('TipoEstudioStore');
                                sto.reload();
                                console.log("Entra al success");
                                var obj = Ext.decode(action.response.responseText);
                                console.log(action.response.responseText);
                                Ext.toast(obj.msg, "Informacion");
                                Ext.ComponentQuery.query('PersonalBajaTitulo')[0].close();
                            },
                            failure: function(form, action) 
                            {
                                console.log("Entra al failure");
                                console.log(action.response.responseText);
                                var obj = Ext.decode(action.response.responseText);
                                Ext.Msg.alert('Error', obj.error);
                            }
                        });
                    } //endif buttonid===yes
                }
            }); //end Ext.Msg.show
        }//end else
    },

    cancelaBajaTitulo: function() 
    {
		console.log('*** Cierra ventana de baja de titulo');
		Ext.ComponentQuery.query('PersonalBajaTitulo')[0].close();
    },
        
    VentanaModificarTitulo:function(button)
    {
        console.log('*** Ventana modifica titulo');
        var win = Ext.widget('PersonalModificarTitulo');
        win.show();
    },

    modificaTitulo:function(button)
    {
        console.log('*** Modifica titulo');
        var f = this.lookupReference('PersonalModificarTitulo');
        var frm = f.down('form').getForm();
        //script para obtener los campos invalidos
        var fieldNames = [];                
        var fields = f.getInvalidFields();
        if(fields.length>0)//si hay campos invalidos
        {
            for(var i=0; i <  fields.length; i++)
            {
                var field = fields[i];
                fieldNames.push(field.getName());
                console.debug(fieldNames);
                Ext.MessageBox.alert('Datos mal cargados', 'Algunos campos est&aacute;n mal cargados');
            }
        }
        else
        {
            Ext.Msg.show({
                title:'Confirmacion para modificar',
                msg: 'Seguro que desea modificar el titulo?',
                buttons: Ext.Msg.YESNOCANCEL,
                icon: Ext.Msg.QUESTION,
                buttonText : 
                {
                    yes : 'Si, seguro',
                    no : 'No, mejor no',
                    cancel : 'No, sacame de aqu&iacute;!'
                },
                fn: function (buttonId) 
                {
                    if(buttonId==='yes')    
                    {   //submiteo
                        frm.submit({
                            url: 'app/proxy.titulo.php',
                            method: 'POST',
                            params: {
                                legajo: AppGlobals.legAgente,
                                act: "modificaTitulo"
                            },
                            success: function(form, action) {
                                var sto = Ext.getStore('TipoEstudioStore');
                                sto.reload();
                                console.log("Entra al success");
                                var obj = Ext.decode(action.response.responseText);
                                console.log(action.response.responseText);
                                Ext.toast(obj.msg, "Informacion");
                                Ext.ComponentQuery.query('PersonalModificarTitulo')[0].close();
                            },
                            failure: function(form, action) {
                                console.log("Entra al failure");
                                console.log(action.response.responseText);
                                var obj = Ext.decode(action.response.responseText);
                                Ext.Msg.alert('Error', obj.error);
                            }
                        });
                    } //endif buttonid===yes
                }
            }); //end Ext.Msg.show
        }//end else
    },

    cancelaModificaTitulo: function() 
    {
		console.log('*** Cierra ventana de modificar titulo');
		Ext.ComponentQuery.query('PersonalModificarTitulo')[0].close();
                
	},
        
    /*** SANCIONES ***/
    onActivateSanciones: function() 
    {
        console.log('*** Activate sanciones');
        var sto = Ext.getStore('HSancionStore');
        console.log(sto);
        sto.load({
            params: {
                legajo: AppGlobals.legAgente
            }
        });
        
    },

    VentanaSancion: function(button) 
    {
        console.log('*** Ventana de sanciones');
        var win = Ext.widget('PersonalAgregarSancion');
        win.show();
   },

    changeFechaAgregarSancion: function(field, newValue, oldValue, eOpts) 
    {
        var fechaDesde = Ext.getCmp('fechaDesdeAgregarSancion').getValue();
        var fechaHasta = Ext.getCmp('fechaHastaAgregarSancion').getValue();
        if(fechaHasta)
        {
            if(fechaDesde > fechaHasta)
            {
                Ext.Msg.alert('Error', 'Rango incorrecto de fechas');
                Ext.getCmp('fechaHastaAgregarSancion').setValue(""); 
            }
        }
    },  

    onAgregarSancion: function(button) 
    {
        console.log('*** Agregar sancion al agente');
        var f = this.lookupReference('PersonalAgregarSancion');
        var frm = f.down('form').getForm();
                
        //script para obtener los campos invalidos
        var fieldNames = [];                
        var fields = f.getInvalidFields();
        if(fields.length>0)//si hay campos invalidos
        {
            for(var i=0; i <  fields.length; i++)
            {
                var field = fields[i];
                fieldNames.push(field.getName());
                console.debug(fieldNames);
                Ext.MessageBox.alert('Datos mal cargados', 'Algunos campos est&aacute;n mal cargados');
            }
        }
        else //submiteo
        {
            //Desactivo el boton guardar para evitar cargas duplicadas    
            Ext.getCmp('guardarAsigSancion').disabled = true;    
            frm.submit({
                url: 'app/proxy.sanciones.php',
                method: 'POST',
                params: {
                legajo: AppGlobals.legAgente
                },
            success: function(form, action) {
                console.log("success");
                console.log(action.response.responseText);
                var sto = Ext.getStore('HSancionStore');
                sto.reload();
                /*var stoPers = Ext.getStore('SancionStore');
                stoPers.reload();*/
                var obj = Ext.decode(action.response.responseText);
                Ext.toast(obj.msg, "Informacion");
                Ext.ComponentQuery.query('PersonalAgregarSancion')[0].close();
            },
            failure: function(form, action) {
                console.log("failure");
                console.log(action.response.responseText);
                var obj = Ext.decode(action.response.responseText);
                Ext.Msg.alert('Error', obj.error);
                
                }
            });
        }
    },

    eliminaSancion: function(button)
    {
        console.log("Elimina sancion");
        Ext.Msg.show({
            title:'Confirmacion para eliminar',
            msg: 'Seguro que desea eliminar la sanci&oacute;n?',
            buttons: Ext.Msg.YESNOCANCEL,
            icon: Ext.Msg.QUESTION,
            buttonText : 
                {
                    yes : 'Si, seguro',
                    no : 'No, mejor no',
                    cancel : 'Eliminar? No! Sacame de aqu&iacute;!'
                },
                fn: function (buttonId) 
                {
                    if(buttonId==='yes')
                    {
                        var sm = button.up('PersonalSanciones').getSelectionModel();
                        var arr = sm.getSelection();
                        console.log(arr[0].data.id);
                        Ext.Ajax.request(
                            {
                                url: 'app/proxy.sanciones.php',
                                
                                params: {
                                    idSancionElimin: arr[0].data.id
                                    
                                },
                                success:function(response){
                                    var obj = Ext.decode(response.responseText);
                                    if(obj.success === true)
                                    {
                                        Ext.toast(obj.msg, 'Informaci&oacute;n');
                                    }
                                    else
                                    {
                                        Ext.Msg.alert("Error", obj.error)
                                    }
                                var sto = Ext.getStore('HSancionStore');
                                sto.reload();
                                },
                                failure: function(response, opts) {
                                    Ext.Msg.alert('Error', 'Error de servidor. <br>Estado: ' + response.status);
                                    console.log('server-side failure with status code ' + response.status);
                                }
                            });
                        //Ext.getStore('historial.HCargosStore').remove(sm.getSelection());
                        if (Ext.getStore('HSancionStore').getCount() > 0) {
                            sm.select(0);
                        }
                    }
                    
                }
        });
    },    
    
    cancelaSancion: function() 
    {
        console.log('*** Cierra ventana de agregar sancion');
        Ext.ComponentQuery.query('PersonalAgregarSancion')[0].close();
    },   

    /*** CAPACITACIONES ***/
    onActivateCapacitaciones: function() {
        console.log('*** Activate capacitaciones');
        var sto = Ext.getStore('HCapacitacionStore');
        console.log(sto);
        sto.load({
            params: {
                legajo: AppGlobals.legAgente
            }
        });
        
    },

    VentanaCapacitacion: function(button) 
    {
        console.log('*** Ventana de capacitaciones');
        var win = Ext.widget('PersonalAgregarCapacitacion');
        win.show();
    },

    VentanaEditarCapacitacion: function(button) 
    {
        console.log('*** Ventana de editar capacitacion');
        var win = Ext.widget('PersonalModificarCapacitacion');
        win.show();
        //Relleno el formulario con los datos
        var f = this.lookupReference('PersonalModificarCapacitacion');
        var frm = f.down('form').getForm();
        var reg = button.up('grid').getSelectionModel().getSelection();
        
        Ext.getCmp('observacionesCapacitacionEdit').setValue(reg[0].data.observaciones);
        Ext.getCmp('tipoCapacitacionEdit').setValue(reg[0].data.idtipo);
        Ext.getCmp('nombreCapacitacionEdit').setValue(reg[0].data.nombre);
        Ext.getCmp('origenCapacitacionEdit').setValue(reg[0].data.idorigen);
        Ext.getCmp('fechaCapacitacionEdit').setValue(reg[0].data.fecha);
        Ext.getCmp('modalidadCapacitacionEdit').setValue(reg[0].data.idmodalidad);
        Ext.getCmp('notaCapacitacionEdit').setValue(reg[0].data.nota_evaluacion);
        Ext.getCmp('inherenciaCapacitacionEdit').setValue(reg[0].data.inherencia);
        Ext.getCmp('cantHorasCapacitacionEdit').setValue(reg[0].data.cant_horas);
        Ext.getCmp('evaluacionCapacitacionEdit').setValue(reg[0].data.evaluacion);
    },

    onAgregarCapacitacion: function(button) 
    {
        console.log('*** Agregar capacitacion al agente');
        var f = this.getPersonalAgregarcapacitacion();
        var frm = f.down('form').getForm();
                
        //script para obtener los campos invalidos
        var fieldNames = [];                
        var fields = f.getInvalidFields();
        if(fields.length>0)//si hay campos invalidos
        {
            for(var i=0; i <  fields.length; i++)
            {
                var field = fields[i];
                fieldNames.push(field.getName());
                console.debug(fieldNames);
                Ext.MessageBox.alert('Datos mal cargados', 'Algunos campos est&aacute;n mal cargados');
            }
        }
        else //submiteo
        {
            //Desactivo el boton guardar para evitar cargas duplicadas    
            Ext.getCmp('guardarAsigCapacitacion').disabled = true;    
            frm.submit({
                url: 'app/proxy.capacitaciones.php',
                method: 'POST',
                params: {
                legajo: AppGlobals.legAgente
                },
            success: function(form, action) {
                console.log("success");
                console.log(action.response.responseText);
                var sto = Ext.getStore('HCapacitacionStore');
                sto.reload();
                /*var stoPers = Ext.getStore('SancionStore');
                stoPers.reload();*/
                var obj = Ext.decode(action.response.responseText);
                Ext.toast(obj.msg, "Informacion");
                Ext.ComponentQuery.query('PersonalAgregarCapacitacion')[0].close();
            },
            failure: function(form, action) {
                console.log("failure");
                console.log(action.response.responseText);
                var obj = Ext.decode(action.response.responseText);
                Ext.Msg.alert('Error', obj.error);
                
                }
            });
        }
    },

    onEditarCapacitacion: function(button)
    {
        console.log('*** Editar capacitacion al agente');
        var f = this.lookupReference('PersonalModificarCapacitacion');
        var frm = f.down('form').getForm();
        var reg = this.lookupReference('PersonalCapacitaciones').getSelectionModel().getSelection();        
        //script para obtener los campos invalidos
        console.log(reg);
        
        var fieldNames = [];                
        var fields = f.getInvalidFields();
        if(fields.length>0)//si hay campos invalidos
        {
            for(var i=0; i <  fields.length; i++)
            {
                var field = fields[i];
                fieldNames.push(field.getName());
                console.debug(fieldNames);
                Ext.MessageBox.alert('Datos mal cargados', 'Algunos campos est&aacute;n mal cargados');
            }
        }
        else //submiteo
        {
            //Desactivo el boton guardar para evitar cargas duplicadas    
            Ext.getCmp('editarAsigCapacitacion').disabled = true;    
            frm.submit({
                url: 'app/proxy.capacitaciones.php',
                method: 'POST',
                params: {
                    legajo: AppGlobals.legAgente,
                    act: 'editCapacitacion',
                    idCap: reg[0].id
                },
            success: function(form, action) {
                console.log("success");
                console.log(action.response.responseText);
                var sto = Ext.getStore('HCapacitacionStore');
                sto.reload();
                
                var obj = Ext.decode(action.response.responseText);
                Ext.toast(obj.msg, "Informacion");
                Ext.ComponentQuery.query('PersonalModificarCapacitacion')[0].close();
            },
            failure: function(form, action) {
                console.log("failure");
                console.log(action.response.responseText);
                var obj = Ext.decode(action.response.responseText);
                Ext.Msg.alert('Error', obj.error);
                
                }
            });
        }
    },

    eliminaCapacitacion: function(button)
    {
        console.log("Elimina capacitacion");
        Ext.Msg.show({
            title:'Confirmacion para eliminar',
            msg: 'Seguro que desea eliminar la sanci&oacute;n?',
            buttons: Ext.Msg.YESNOCANCEL,
            icon: Ext.Msg.QUESTION,
            buttonText : 
                {
                    yes : 'Si, seguro',
                    no : 'No, mejor no',
                    cancel : 'Eliminar? No! Sacame de aqu&iacute;!'
                },
            fn: function (buttonId) 
            {
                if(buttonId==='yes')
                {
                    var sm = button.up('PersonalCapacitaciones').getSelectionModel();
                    var arr = sm.getSelection();
                    console.log(arr[0].data.id);
                    Ext.Ajax.request(
                        {
                            url: 'app/proxy.capacitaciones.php',
                            params: {
                                idCapacitacionElimin: arr[0].data.id
                                
                            },
                            success:function(response){
                                var obj = Ext.decode(response.responseText);
                                if(obj.success === true)
                                {
                                    Ext.toast(obj.msg, 'Informaci&oacute;n');
                                }
                                else
                                {
                                    Ext.Msg.alert("Error", obj.error)
                                }
                            var sto = Ext.getStore('HCapacitacionStore');
                            sto.reload();
                            },
                            failure: function(response, opts) {
                                Ext.Msg.alert('Error', 'Error de servidor. <br>Estado: ' + response.status);
                                console.log('server-side failure with status code ' + response.status);
                            }
                        });
                    //Ext.getStore('historial.HCargosStore').remove(sm.getSelection());
                    if (Ext.getStore('HCapacitacionStore').getCount() > 0) {
                        sm.select(0);
                    }
                }
            }
        });
    },

    cancelaCapacitacion: function() 
    {
        console.log('*** Cierra ventana de agregar capacitacion');
        Ext.ComponentQuery.query('PersonalAgregarCapacitacion')[0].close();
    },

    cancelaEditarCapacitacion: function() 
    {
        console.log('*** Cierra ventana de agregar capacitacion');
        Ext.ComponentQuery.query('PersonalModificarCapacitacion')[0].close();
    }, 

    /*** FIRMAS DIGITALES ***/
    onActivateFirmasDigitales: function() 
    {
        console.log('*** Activate firmas digitales');
        var sto = Ext.getStore('HFirmaDigitalStore');
        console.log(sto);
        sto.load({
            params: {
                legajo: AppGlobals.legAgente
            }
        });
        
    },

    VentanaFirmaDigital: function(button) 
    {
        console.log('*** Ventana de agregar firma digital');
        var win = Ext.widget('PersonalAgregarFirmaDigital');
        win.show();
    },

    VentanaEditarFirmaDigital: function(button) 
    {
        console.log('*** Ventana de editar firma digital');
        var win = Ext.widget('PersonalModificarFirmaDigital');
        win.show();
        //Relleno el formulario con los datos
        var f = this.lookupReference('PersonalModificarFirmaDigital');
        var frm = f.down('form').getForm();
        var reg = button.up('grid').getSelectionModel().getSelection();
        
        var dt = new Date(reg[0].data.fecha_alta);
        var drev = new Date(reg[0].data.fecha_revocacion);
        Ext.getCmp('tipoFirmaDigitalModificar').setValue(reg[0].data.tipo);
        Ext.getCmp('descripcionFirmaDigitalModificar').setValue(reg[0].data.descripcion_firma);
        Ext.getCmp('fechaAltaModificar').setValue(dt);
        Ext.getCmp('fechaRevocacionModificar').setValue(drev);
        console.log(Ext.Date.format(dt,"Y-m-d H:i:s"));

    },

    onAgregarFirmaDigital: function(button) 
    {
        console.log('*** Agregar firma digital al agente');
        var f = this.lookupReference('PersonalAgregarFirmaDigital');
        var frm = f.down('form').getForm();
        console.log(f);
        //script para obtener los campos invalidos
        var fieldNames = [];                
        var fields = f.getInvalidFields();
        if(fields.length>0)//si hay campos invalidos
        {
            for(var i=0; i <  fields.length; i++)
            {
                var field = fields[i];
                fieldNames.push(field.getName());
                console.debug(fieldNames);
                Ext.MessageBox.alert('Datos mal cargados', 'Algunos campos est&aacute;n mal cargados');
            }
        }
        else //submiteo
        {
            //Desactivo el boton guardar para evitar cargas duplicadas    
            Ext.getCmp('saveFirmaDigital').disabled = true;    
            frm.submit({
                url: 'app/proxy.firmasdigitales.php',
                method: 'POST',
                params: {
                    act: 'setFirmaDigital',
                    legajo: AppGlobals.legAgente
                },
            success: function(form, action) {
                console.log("success");
                console.log(action.response.responseText);
                var sto = Ext.getStore('HFirmaDigitalStore');
                sto.reload();
                
                var obj = Ext.decode(action.response.responseText);
                Ext.toast(obj.msg, "Informacion");
                Ext.ComponentQuery.query('PersonalAgregarFirmaDigital')[0].close();
            },
            failure: function(form, action) {
                console.log("failure");
                console.log(action.response.responseText);
                var obj = Ext.decode(action.response.responseText);
                Ext.Msg.alert('Error', obj.error);
                
                }
            });
        }
    },

    onEditarFirmaDigital: function(button)
    {
        console.log('*** Editar firma digital al agente');
        var f = this.lookupReference('PersonalModificarFirmaDigital');
        var frm = f.down('form').getForm();
        var reg = this.lookupReference('PersonalFirmasDigitales').getSelectionModel().getSelection();
        //script para obtener los campos invalidos
        var fieldNames = [];                
        var fields = f.getInvalidFields();
        if(fields.length>0)//si hay campos invalidos
        {
            for(var i=0; i <  fields.length; i++)
            {
                var field = fields[i];
                fieldNames.push(field.getName());
                console.debug(fieldNames);
                Ext.MessageBox.alert('Datos mal cargados', 'Algunos campos est&aacute;n mal cargados');
            }
        }
        else //submiteo
        {
            //Desactivo el boton guardar para evitar cargas duplicadas    
            Ext.getCmp('saveEditarFirmaDigital').disabled = true;    
            frm.submit({
                url: 'app/proxy.firmasdigitales.php',
                method: 'POST',
                params: {
                    legajo: AppGlobals.legAgente,
                    act: 'editFirmaDigital',
                    idFirma: reg[0].data.id
                },
            success: function(form, action) {
                console.log("success");
                console.log(action.response.responseText);
                var sto = Ext.getStore('HFirmaDigitalStore');
                sto.reload();
                
                var obj = Ext.decode(action.response.responseText);
                Ext.toast(obj.msg, "Informacion");
                Ext.ComponentQuery.query('PersonalModificarFirmaDigital')[0].close();
            },
            failure: function(form, action) {
                console.log("failure");
                console.log(action.response.responseText);
                var obj = Ext.decode(action.response.responseText);
                Ext.Msg.alert('Error', obj.error);
                
                }
            });
        }
    },

    eliminaFirmaDigital: function(button)
    {
        console.log("Elimina firma digital");
        Ext.Msg.show({
            title:'Confirmacion para eliminar',
            msg: 'Seguro que desea eliminar la firma digital?',
            buttons: Ext.Msg.YESNOCANCEL,
            icon: Ext.Msg.QUESTION,
            buttonText : 
                {
                    yes : 'Si, seguro',
                    no : 'No, mejor no',
                    cancel : 'Eliminar? No! Sacame de aqu&iacute;!'
                },
            fn: function (buttonId) 
            {
                if(buttonId==='yes')
                {
                    var sm = button.up('PersonalFirmasDigitales').getSelectionModel();
                    var arr = sm.getSelection();
                    console.log(arr[0].data.id);
                    Ext.Ajax.request(
                        {
                            url: 'app/proxy.firmasdigitales.php',
                            method: 'POST',
                            params: {
                                act: 'deleteFirmaDigital',
                                idFirmaElimin: arr[0].data.id
                            },
                            success:function(response){
                            var obj = Ext.decode(response.responseText);
                            if(obj.success === true)
                            {
                                Ext.toast(obj.msg, 'Informaci&oacute;n');
                            }
                            else
                            {
                                Ext.Msg.alert("Error", obj.error)
                            }
                            var sto = Ext.getStore('HFirmaDigitalStore');
                            sto.reload();
                            },
                            failure: function(response, opts) {
                                Ext.Msg.alert('Error', 'Error de servidor. <br>Estado: ' + response.status);
                                console.log('server-side failure with status code ' + response.status);
                            }
                        });
                    //Ext.getStore('historial.HCargosStore').remove(sm.getSelection());
                    if (Ext.getStore('HFirmaDigitalStore').getCount() > 0) 
                    {
                        sm.select(0);
                    }
                }
                
            }
        });
    },   

    cancelaFirmaDigital: function() 
    {
        console.log('*** Cierra ventana de agregar firma digital');
        Ext.ComponentQuery.query('PersonalAgregarFirmaDigital')[0].close();
    }, 

    cancelaEditarFirmaDigital: function() 
    {
        console.log('*** Cierra ventana de editar firma digital');
        Ext.ComponentQuery.query('PersonalModificarFirmaDigital')[0].close();
    }, 
    /* Ventana agregar historia clinica */

    onAgregarRegistroHistoria: function(button) 
    {
        console.log('*** Click Agregar registro historia');
        var win = Ext.widget('PersonalHistoriaClinica');
        win.show();
        Ext.getCmp("legajoHistoriaClinica").setValue(AppGlobals.legAgente);
                
    },

    /* Ventana editar historia clinica */

    onEditarRegistroHistoria: function(button) 
    {
        console.log('*** Click editar registro historia');
        var win = Ext.widget('PersonalEditarHistoriaClinica');
        var reg = button.up('grid').getSelectionModel().getSelection();
        console.log(reg);
        win.show();
        //Relleno el formulario con los datos
        //var f = this.getPersonalModificarcapacitacion();
        //var frm = f.down('form').getForm();
        //console.log(AppGlobals.recordEditHistoriaClinica[0].raw);
        Ext.getCmp("legajoEditarHistoriaClinica").setValue(reg[0].data.legajo);
        Ext.getCmp('fechaHoraEditarHistoriaClinica').setValue(reg[0].data.fecha_hora);
        Ext.getCmp('observacionesEditarHistoriaClinica').setValue(reg[0].data.observaciones);
        if(reg[0].data.certificado!='No')
        {
            Ext.getCmp("certificadoEditarHistoriaClinica").destroy();
            Ext.getCmp("formHistoriaClinica").add({
                xtype: 'displayfield',
                id: 'certificadoEditarHistoriaClinica',
                name: 'certificadoEditarHistoriaClinica',
                fieldLabel: 'Certificado',
                anchor: '95%',
                value: reg[0].data.certificado
            });
        }
    },

    onVerRegistroHistoria: function(button) 
    {
        console.log('*** Click ver registro historia');
        var win = Ext.widget('PersonalVerHistoriaClinica');
        var reg = button.up('grid').getSelectionModel().getSelection();
        win.show();
        
        Ext.getCmp("legajoVerHistoriaClinica").setValue(reg[0].data.legajo);
        Ext.getCmp('fechaHoraVerHistoriaClinica').setValue(reg[0].data.fecha_hora);
        Ext.getCmp('observacionesVerHistoriaClinica').setValue(reg[0].data.observaciones);
        if(reg[0].data.certificado!='No')
        {
            Ext.getCmp("formVerHistoriaClinica").add({
                xtype: 'displayfield',
                id: 'certificadoVerHistoriaClinica',
                name: 'certificadoVerHistoriaClinica',
                fieldLabel: 'Certificado',
                anchor: '95%',
                value: reg[0].data.certificado
            });
        }
    },

    onVerRegistroHistoriaDblClick: function(grid, selectionReg) 
    {
        console.log('*** Click ver registro historia');
        var win = Ext.widget('PersonalVerHistoriaClinica');
        var reg = grid.getSelectionModel().getSelection();
        win.show();
        Ext.getCmp("legajoVerHistoriaClinica").setValue(reg[0].data.legajo);
        Ext.getCmp('fechaHoraVerHistoriaClinica').setValue(reg[0].data.fecha_hora);
        Ext.getCmp('observacionesVerHistoriaClinica').setValue(reg[0].data.observaciones);
    },

    onEliminarRegistroHistoria: function(button)
    {
        console.log('*** Delete registro historia');
        var sto = Ext.getStore('HistoriaClinicaStore');    
        var reg = this.lookupReference('PersonalFichaHistoriaClinica').getSelectionModel().getSelection();             
        Ext.Msg.show({
            title:'Eliminar registro historia cl&iacute;nica',
            msg: 'Seguro que desea eliminar registro historia cl&iacute;nica?',
            buttons: Ext.Msg.YESNOCANCEL,
            buttonText : 
            {
                yes : 'Si',
                no : 'No',
                cancel : 'Cancelar'
            },
            icon: Ext.Msg.QUESTION,
            fn: function(buttonId)
            {
                if(buttonId === 'yes')
                {
                    button.setDisabled(true); //Desactivo el boton para que no haya multiples cargas. Bug corregido Mauro
                    Ext.Ajax.request({
                        url: 'app/proxy.historiaclinica.php',
                        method: 'POST',
                        params: 
                        {
                            tarea: 'deleteRegistroHistoria',
                            idHistoria: reg[0].data.id,
                            legajo: AppGlobals.legAgente
                        },
                        success: function(action) 
                        {
                            console.log(action);
                            Ext.toast('Registro eliminado correctamente', 'Informaci&oacute;n');
                            button.setDisabled(false); //Habilito nuevamente el boton
                            sto.reload();
                                
                        },
                        failure: function(action) 
                        {
                            var obj = Ext.decode(action.responseText);
                            console.log("Entro al failure: "+action.responseText);
                            Ext.Msg.alert('Error', obj.error);
                            button.setDisabled(false); //Habilito nuevamente el boton
                        }
                    });
                }// end if(buttonId === 'yes')
            }
        });
    },

    onSaveRegistroHistoria: function (button) 
    {
        console.log('*** Save registro historia');
        var frm = button.up('window').down('form').getForm();
        var sto = Ext.getStore('HistoriaClinicaStore');                 
        if (frm.isValid())
        {
            Ext.Msg.show({
                title:'Guardar registro historia cl&iacute;nica',
                msg: 'Guardar registro historia cl&iacute;nica el '+Ext.Date.format(Ext.getCmp('fechaHoraHistoriaClinica').getValue(),"d-m-Y")+'?',
                buttons: Ext.Msg.YESNOCANCEL,
                buttonText : 
                {
                    yes : 'Si',
                    no : 'No',
                    cancel : 'Cancelar'
                },
                icon: Ext.Msg.QUESTION,
                fn: function(buttonId)
                {
                    if(buttonId === 'yes')
                    {
                        button.setDisabled(true); //Desactivo el boton para que no haya multiples cargas. Bug corregido Mauro
                        frm.submit({
                        url: 'app/proxy.historiaclinica.php',
                        method: 'POST',
                        params: {
                                tarea: 'saveRegistroHistoria',
                                legajo: AppGlobals.legAgente
                        },
                        success: function(frm, action) {
                                console.log("Entro al success: "+action.response.responseText);
                                Ext.toast('Registro cargado correctamente', 'Informaci&oacute;n');
                                button.setDisabled(false); //Habilito nuevamente el boton
                                Ext.ComponentQuery.query('PersonalHistoriaClinica')[0].close();
                                sto.reload();
                                
                        },
                        failure: function(frm, action) {
                            var obj = Ext.decode(action.response.responseText);
                                console.log("Entro al failure: "+action.response.responseText);
                                Ext.Msg.alert('Error', obj.error);
                                button.setDisabled(false); //Habilito nuevamente el boton
                        }
                    });
                    }// end if(buttonId === 'yes')
                }
            });
        }
        else
        {
            Ext.Msg.alert("Error", "Complete correcatamente los campos");
        }
    },

    onSaveEditarRegistroHistoria: function (button) 
    {
        console.log('*** Save Editar registro historia');
        var frm = button.up('window').down('form').getForm();
        var sto = Ext.getStore('HistoriaClinicaStore'); 
        var reg = this.lookupReference('PersonalFichaHistoriaClinica').getSelectionModel().getSelection();
        console.log(reg);
        if(frm.isValid())
        {
            Ext.Msg.show({
                title:'Guardar cambios en historia cl&iacute;nica',
                msg: 'Guardar cambios en historia cl&iacute;nica el '+Ext.Date.format(Ext.getCmp('fechaHoraEditarHistoriaClinica').getValue(),"d-m-Y")+'?',
                buttons: Ext.Msg.YESNOCANCEL,
                buttonText : 
                {
                    yes : 'Si',
                    no : 'No',
                    cancel : 'Cancelar'
                },
                icon: Ext.Msg.QUESTION,
                fn: function(buttonId)
                {
                    if(buttonId === 'yes')
                    {
                        button.setDisabled(true); //Desactivo el boton para que no haya multiples cargas. Bug corregido Mauro
                        frm.submit({
                        url: 'app/proxy.historiaclinica.php',
                        method: 'POST',
                        params: {
                                tarea: 'editRegistroHistoria',
                                idHistoria: reg[0].data.id,
                                legajo: AppGlobals.legAgente

                        },
                        success: function(frm, action) {
                                console.log("Entro al success: "+action.response.responseText);
                                Ext.toast('Registro editado correctamente', 'Informaci&oacute;n');
                                button.setDisabled(false); //Habilito nuevamente el boton
                                Ext.ComponentQuery.query('PersonalEditarHistoriaClinica')[0].close();
                                sto.reload();
                                
                        },
                        failure: function(frm, action) {
                            var obj = Ext.decode(action.response.responseText);
                                console.log("Entro al failure: "+action.response.responseText);
                                Ext.Msg.alert('Error', obj.error);
                                button.setDisabled(false); //Habilito nuevamente el boton
                        }
                    });
                    }// end if(buttonId === 'yes')
                }
            });
        }
        else
        {
            Ext.Msg.alert("Error", "Complete correcatamente los campos");
        }
                
        
    },
    
    cancelaRegistroHistoria: function() 
    {
        console.log('*** Cierra ventana de agregar historia clinica');
        Ext.ComponentQuery.query('PersonalHistoriaClinica')[0].close();
    },

    cancelaEditarRegistroHistoria: function() 
    {
        console.log('*** Cierra ventana de editar historia clinica');
        Ext.ComponentQuery.query('PersonalEditarHistoriaClinica')[0].close();
    },
    eliminarCertificadoHistoria: function(button)
    {
        console.log("Eliminar certificado");
        var sto = this.lookupReference('PersonalFichaHistoriaClinica').getStore();      
        var reg = this.lookupReference('PersonalFichaHistoriaClinica').getSelectionModel().getSelection();
        console.log(reg);           
        Ext.Msg.show({
            title:'Eliminar certificado',
            msg: 'Seguro que desea eliminar el certificado de esta historia cl&iacute;nica?',
            buttons: Ext.Msg.YESNOCANCEL,
            buttonText : 
            {
                yes : 'Si',
                no : 'No',
                cancel : 'Cancelar'
            },
            icon: Ext.Msg.QUESTION,
            fn: function(buttonId)
            {
                if(buttonId === 'yes')
                {
                    button.setDisabled(true); //Desactivo el boton para que no haya multiples cargas. Bug corregido Mauro
                    Ext.Ajax.request({
                    url: 'app/proxy.historiaclinica.php',
                    method: 'POST',
                    params: {
                            tarea: 'deleteCertificadoHistoria',
                            idHistoria: reg[0].data.id,
                            legajo: AppGlobals.legAgente
                    },
                    success: function(action) {
                            console.log(action);
                            Ext.toast('Certificado eliminado correctamente', 'Informaci&oacute;n');
                            button.setDisabled(false); //Habilito nuevamente el boton
                            sto.reload();
                            
                    },
                    failure: function(action) {
                        var obj = Ext.decode(action.responseText);
                            console.log("Entro al failure: "+action.responseText);
                            Ext.Msg.alert('Error', obj.error);
                            button.setDisabled(false); //Habilito nuevamente el boton
                    }
                });
                }// end if(buttonId === 'yes')
            }
        });
    },
    cancelaVerRegistroHistoria: function() 
    {
        console.log('*** Cierra ventana de ver historia clinica');
        Ext.ComponentQuery.query('PersonalVerHistoriaClinica')[0].close();
    },

    selectHistoriaClinica: function (selectionModel, records)
    {
        console.log('*** Selecciona historia clinica');
        if(AppGlobals.permisos[0].pesta_historiaclinica_ver==1)
        {
            Ext.getCmp('verRegistroHistoria').setDisabled(!records.length);
        }
        if(AppGlobals.permisos[0].pesta_historiaclinica_editar==1)
        {
            Ext.getCmp('editarRegistroHistoria').setDisabled(!records.length);
            Ext.getCmp('quitarRegistroHistoria').setDisabled(!records.length);
            if(records.length>0 && records[0].data.certificado!="No")
            {
                Ext.getCmp('borrarCertificadoHistoria').setDisabled(!records.length);
            }
            else
            {
                Ext.getCmp('borrarCertificadoHistoria').setDisabled(true);
            }
        }
        //Guardo el registro seleccionado, para rellenar la ventana con los datos si el usuario quiere editar la historia clinica
        AppGlobals.recordEditHistoriaClinica = records;
        console.log(AppGlobals.recordEditHistoriaClinica);
    },

    onClickRowAgente: function(record) 
    {
        console.log('*** Click en grilla Agentes');
        
        AppGlobals.registroAgente = record.data;
        //Deshabilito botones
        Ext.getCmp('btnEditarAgente').setDisabled(true);
        Ext.getCmp('btnGuardar').setDisabled(true);
        Ext.getCmp('btnCancelar').setDisabled(true);
        Ext.getCmp('btnLimpiar').setDisabled(true);
        console.log(this.lookupReference('PersonalFichaLegajo'));
        var color = "";
        switch(AppGlobals.registroAgente.activa)
        {
            case "1": 
                color = "#ffffff";
                break;
            case "2": 
                color = "#fcf0da";
                break;
            case "3":
                color = "#ffe2e2";
                break;
        }
        this.lookupReference('PersonalFichaLegajoForm').setBodyStyle('backgroundColor', color);
        
        //Habilito paneles

        this.lookupReference('PersonalFicha').items.each(function(tab, index){
            tab.setDisabled(false);
        });
        

        //Recargo stores
        var stoHist = Ext.getStore('HLicenciaStore');
        var stoHist = Ext.getStore('HCargosStore');
        stoHist.reload();
        AppGlobals.dniAgente = record.data.nrodoc;
        AppGlobals.legAgente = record.data.legajo;
        AppGlobals.nombreAgente = record.data.apellido + ', ' + record.data.nombre;

        //Habilito botones para editar
        if(AppGlobals.permisos[0].editar_agente == 1)
        {
            Ext.getCmp('btnEditarAgente').setDisabled(false);
        }
        else //Habilito el boton de edicion solo si el usuario quiere editar su propio legajo
        {
            console.log("Legajo user: "+AppGlobals.legajoUser);
            if(AppGlobals.legAgente == AppGlobals.legajoUser)
            {
                Ext.getCmp('btnEditarAgente').setDisabled(false);
            }
        }
        
        if(AppGlobals.permisos[0].baja_agente == 1)
        {
            Ext.getCmp('btnBajaAgente').setDisabled(false);
        }
        
        // Muestra la pestaÃ±a de Legajo
        var tabs = this.lookupReference('PersonalFicha');
        tabs.setActiveTab(0);

        var ficha = this.lookupReference('PersonalFichaLegajo');
        ficha.setActiveTab(0);
        var frm = ficha.getActiveTab();
                //console.trace();
        frm.getForm().reset();
        frm.getForm().setValues(record.data);
                
        //Todo a modo de lectura
        this.setReadOnlyForAll(true, true);
                
        // Inicializa la pestaÃ±a de Licencias
        if(AppGlobals.permisos[0].pesta_licencias == 1)
        {
            var card = this.lookupReference('PersonalFichaLicencias');
            //card.getLayout().setActiveItem('cardZero');
        }
                
        // Cargar foto
        Ext.Ajax.request({
            url: 'res/fotos/'+AppGlobals.legAgente+'.jpg',
            method: 'POST',     
            success: function (responseObject) {
                console.log('FOTO CARGADA!!!');
                        //console.log(responseObject);
                //var headers = responseObject.getAllResponseHeaders();
                //console.info('FOTO ' + Ext.encode(headers));
                Ext.getCmp('foto').setSrc('res/fotos/'+AppGlobals.legAgente+'.jpg');
            },
            failure: function (responseObject) {
                console.log('ERROR AL CARGAR FOTO');
                //console.log(responseObject.status);
                Ext.getCmp('foto').setSrc('res/img/profile1.jpg');
            }
        });

        
        if(record.data.titulo!="") 
        {  
            //console.log("Tiene titulo"); 
                        //alert("entra al titulo");
            // Cargar titulo
            Ext.Ajax.request({
                url: 'app/proxy.titulo.php',
                method: 'GET',
                params: {
                    legajo: AppGlobals.legAgente
                },
                success: function(response)
                {
                    var obj = Ext.decode(response.responseText);
                    //console.log(obj.titulo);
                    if(obj.titulo == null)
                    {
                        Ext.getCmp('titulo').setValue("");
                    }
                    else
                    {
                        Ext.getCmp('titulo').setValue(obj.titulo);
                    }
                },
                failure: function(response) {
                    Ext.Msg.alert('Error', 'Error de servidor. <br>Estado: ' + response.status);
                    console.log('server-side failure with status code ' + response.status);
                }
            });
        }
        var state = "";
        switch(record.data.activa)
        {
            case '1':
                state = "";
                break;
            case '2':
                state = "(JUBILACIÓN EN TRÁMITE)";
                break;
            case '3':
                state = "(JUBILADO)";
                break;

        }
        this.lookupReference('PersonalFicha').setTitle('Ficha de ' + record.data.apellido + ', ' + record.data.nombre+' '+state);
    },

    //Controlo si el usuario está editando algo, muestro un cartel advirtiendole
    controlEditing: function(grid, record)
    {
        if(AppGlobals.isEditing=='si')
        {
            Ext.Msg.show({
            title: 'Advertencia', 
            msg: 'Est&aacute; por salir sin guardar cambios. Desea continuar?',
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.WARNING,
            closable: false,
            buttonText: 
            {
                yes : 'Si, seguro',
                no : 'No, guardar&eacute; los cambios primero'                
            },
            fn: function (buttonId)
            {
                if(buttonId=="yes")
                {
                   AppGlobals.isEditing = 'no';
                   this.onClickRowAgente(record);
                }
                else
                {
                    console.log("click en no");
                    return;
                }
            }
            });
        }
        else
        {
            this.onClickRowAgente(record);
        }
    },

	clearForm: function(button) 
    {
        console.log('*** Limpiar form: ');
        button.up('form').getForm().reset();
    },
        
    contarDiasLaborales: function(pInicio, pFin, pDiasHabiles, pMismoAnio)
    {
        // pInicio: fecha de inicio para contar
        // pFin: fecha de fin hasta donde contar
        // pDiasHabiles: si son dias habiles o corridos, default: true (habiles) 
        // Todo debe ser dentro del mismo aÃ±o, salvo excepciones

        //Primero que todo, controlo que ambos campos esten completos, si no no hago nada. Bug corregido Mauro
        if(pInicio !== "" && pInicio !== null && pFin !== "" && pFin !== null)
        {
            console.log(pInicio);
            console.log(pFin);
            var pHoy = new Date();
            
            //Obtengo el numero de días redondeado, sin contar horas, minutos, segundos ni milisegundos. 
            var pHoy_redond = Math.floor(pHoy.getTime()/1000/60/60/24);
            var pInicio_redond = Math.floor(pInicio.getTime()/1000/60/60/24);
            var pFin_redond = Math.floor(pFin.getTime()/1000/60/60/24);
            /*console.log("Hoy: "+pHoy_redond);
            console.log("Inicio: "+pInicio_redond);
            console.log("Fin: "+pFin_redond);*/
            
            //Luego controlo que el rango sea correcto. Bug corregido Mauro
            //if(pInicio_redond >= pHoy_redond && pFin_redond >= pHoy_redond)
            //{
            if(pInicio_redond>pFin_redond)
            {
                //Ext.Msg.alert('Error', 'Rango incorrecto de fechas');
                return 0;
            }
            else
            {
                //pMismoAnio = pMismoAnio || true; // Inicializar el parametro
                var dia = 86400;
                var inicio = new Date(pInicio).getTime() / 1000;
                var fin = new Date(pFin).getTime() / 1000;

                var feriados = AppGlobals.feriados;

                var dt1 = new Date(inicio * 1000);
                var dt2 = new Date(fin * 1000);
                
                //Si es true, controla que los dias no pasen de un año a otro, o sea, que sean en un mismo año
                if(pMismoAnio)
                {
                    if (dt1.getYear() != dt2.getYear()) 
                    {
                        console.log(dt1);        
                        console.log(dt1.getYear());
                        console.log(dt2);
                        console.log(dt2.getYear());
                        Ext.Msg.alert('Informacion', 'Las fechas deben ser dentro del mismo aÃ±o');
                        return 0;
                    }
                }    

                var anio_actual = new Date().getFullYear();
                var anio_licencia = dt1.getFullYear();
                var arreglo_feriados = new Array();

                Ext.Ajax.request({
                url: 'app/proxy.feriados.php',
                async: false,
                params: {
                    tarea: 'diasLaborales',
                    method: 'GET',
                    anio: anio_licencia
                },
                success: function(response){
                    var obj = Ext.decode(response.responseText);
                    //console.log(obj);
                    for (i=0; i < obj.results.length; i++) 
                    {
                        arreglo_feriados.push(obj.results[i].mes+'-'+obj.results[i].dia);
                    }
                    
                },
                failure: function(response, opts) {
                    Ext.Msg.alert('Error', 'No se pudo conectar a la base de datos para cargar los feriados del a&ntilde;o actual. Error de servidor. <br>Estado: ' + response.status);
                    console.log('server-side failure with status code ' + response.status);
                }
                });
                //console.log(arreglo_feriados);
                var _mes;
                var _dia;
                var total_dias = 0;
                var diaspasados = 0;
                while( inicio <= fin) 
                {
                    var date = new Date(inicio * 1000);
                    _mes = date.getMonth() + 1; //getMonth devuelve el mes empezando por 0
                    _dia = date.getDate(); //getDate devuelve el dia del mes       
                    mes_dia = _mes + '-' + _dia;
                    dia_semana = date.getDay();
                    //console.log(mes_dia + ' # ' + dia_semana + ' | ' + Ext.Array.indexOf(feriados, mes_dia));
                    if (pDiasHabiles) 
                    {
                        if ( (Ext.Array.indexOf(arreglo_feriados, mes_dia) == -1 ) && (dia_semana > 0 && dia_semana < 6)) 
                        { 
                            diaspasados ++; 
                        }
                    } 
                    else 
                    {
                        diaspasados++;  
                        //if ( (Ext.Array.indexOf(feriados, mes_dia) == -1 ) && (dia_semana > 0 && dia_semana < 6)) { diaspasados ++; }
                    }

                    inicio = inicio + dia;
                    total_dias ++;
                }
                console.log(pDiasHabiles);
                console.log('Total de dias: ' + total_dias + ' Laborables: ' + diaspasados);

                return diaspasados;
            }
        } 
    },
        
    diasDisponibles: function(item) 
    {
        var me = this;   
        AppGlobals.diasDisponibles = 0;
        switch(item)
        {
            case 10://Compensatoria
                var tarea = 'diasDisponiblesCompensatoria';
                break;
            case 18: //Doble turno
                var tarea = 'diasDisponiblesDobleTurno';
                break;
            default:
                var tarea = 'diasDisponibles';
                break;  
        }        
        Ext.Ajax.request(
        {
            url: 'app/proxy.licencias.php',
            params: {
                tarea: tarea,
                tipoLic: item,
                legajo: AppGlobals.legAgente
            },
            success: function(response, opts) 
            {
                var obj = Ext.decode(response.responseText);
                if(obj.success==true)
                {
                    console.log("Success: "+response.responseText);
                    var tomados = parseInt(obj.diasTomados);
                    var disponibles = obj.diasDisponibles==""?"":parseInt(obj.diasDisponibles);
                    var totalDias = typeof obj.totalDias!='undefined'?parseInt(totalDias):0;
                    //Ext.getCmp('disponibles').setValue(disponibles);
                    //Ext.getCmp('tomados').setValue(tomados);
                    AppGlobals.diasDisponibles = disponibles;
                    AppGlobals.diasLicPorAnio = totalDias;
                    
                }
                else
                {
                    Ext.Msg.alert('Error', obj.error);
                }
            },
            failure: function(response, opts) 
            {
                Ext.Msg.alert('Error', 'Error de servidor. <br>Estado: ' + response.status);
                console.log('server-side failure with status code ' + response.status);
            }
        });
    },

    saveLicencia: function(frm, tipolic, button) 
    {
        console.log("Entra al savelicencia");
        var form = frm;
        var me = this;
        
        if(frm.isValid())
        {
            frm.submit({
                url: 'app/proxy.licencias.php',
                method: 'POST',
                params: {
                    tarea: 'saveLicencia',
                    tipoLic: tipolic,
                    legajo: AppGlobals.legAgente
                    },
                success: function(frm, action) 
                {
                    me.diasDisponibles(tipolic);
                    
                    Ext.toast('Se guard&oacute; la licencia en el sistema', 'Informaci&oacute;n');
                    button.setDisabled(false); //Activo el botón de guardar nuevamente
                    button.setText("Guardar");
                                    
                    me.reiniciarFormLicencias(tipolic);
                    //Vuelvo el panel a cero para ocultar el formulario de carga
                    var cardLic = Ext.getCmp('pnlLicenciasCard');
                    //cardLic.getLayout().setActiveItem('cardZero'); 
                },
                failure: function(frm, action) 
                {
                    console.log("***Failure: "+action.response.responseText);
                    var obj = Ext.decode(action.response.responseText);
                    console.log(obj);
                    switch (action.failureType) 
                    {
                        case Ext.form.action.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Error', 'Complete los campos con valores v&aacute;lidos.');
                            break;
                        case Ext.form.action.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Error', 'Error en la comunicacion con la base de datos. Recargue la p&aacute;gina (F5) e intente nuevamente. ('+action.response.statusText+')');
                            break;
                        case Ext.form.action.Action.SERVER_INVALID:
                            Ext.Msg.alert('Error', action.result.msg);
                            default:
                                    Ext.Msg.alert('Error', obj.error);
                    }
                    button.setDisabled(false);//Activo el botón de guardar nuevamente
                    button.setText("Guardar");
                }
            });
        }
        else
        {
            Ext.Msg.alert('Error', 'Complete los campos con valores v&aacute;lidos.');
            button.setDisabled(false);//Activo el botón de guardar nuevamente
            button.setText("Guardar");
        }
    },
        
    reiniciarFormLicencias: function(index)
    {
        console.log("Entra a reiniciarFormLicencias");
        //Ext.getCmp(index + '_Desde').setValue(new Date());
        Ext.getCmp('hasta').setValue("");
        Ext.getCmp('diasLicencia').setValue("");
        Ext.getCmp('observaciones').setValue("");
        Ext.getCmp('causa').setValue("");
        Ext.getCmp('expediente').setValue("");
        switch(index)
        {
            case 5: //Maternidad
            {
                Ext.getCmp('fpp').setValue("");
                Ext.getCmp('fp').setValue("");
                Ext.getCmp('lactahasta').setValue("");
                Ext.getCmp('lactahorario').setValue("");
                break;
            }
            case 12: //Estudio
            {
                Ext.getCmp('12_asignatura').setValue("");
                Ext.getCmp('12_carrera').setValue("");
                Ext.getCmp('12_fecha_examen').setValue("");
                break;
            }
        }
    },
        
    onClickLicencia: function(t, record, item, index, ex, eOpts)
    {
        console.log('*** Click en licencia'+index);
        AppGlobals.licenciaSeleccionadaAlta = index;
        this.reiniciarFormLicencias(index);
        // Muestra el panel de la lic seleccionada
        var pnl = Ext.getCmp('pnlLicenciasCard');

        //Obtengo el nombre de la licencia
        var nombre_licencia = "Alta licencia ";
        Ext.Ajax.request(
        {      
            url : 'app/proxy.licencias.php',
            method: 'GET',
            async: false,
            params :{
                idLic : index,
                tarea: 'getTipoLicenciaById'
            },
            
            success : function(response) 
            {
                var obj = Ext.decode(response.responseText);
                nombre_licencia+= obj.results[0].nombre;
                AppGlobals.licenciaSeleccionadaAltaCorridos = obj.results[0].dias_corridos;
                AppGlobals.licenciaSeleccionadaMismoAnio = obj.results[0].mismo_anio;
                console.log("Dias habiles o corridos: "+AppGlobals.licenciaSeleccionadaAltaCorridos);
                var checked = AppGlobals.licenciaSeleccionadaAltaCorridos==1?false:true;
                Ext.getCmp('habiles').setValue(checked);
            },
            failure: function(response)
            {
                //var obj = Ext.decode(response.responseText);
                console.log("Entra al failure "+response.responseText);
                Ext.toast("Error al guardar cambios: "+obj.error,"Error");
            }
        });
        switch (index) 
        {
            case 5: //Maternidad
                pnl.getLayout().setActiveItem(2);
                break;
            case 12://Estudio
                pnl.getLayout().setActiveItem(3);
                break;
            default: //Todas las demas usan el mismo grid para mostrarse
                pnl.getLayout().setActiveItem(1);
                pnl.getLayout().activeItem.header.setTitle(nombre_licencia);
                break;
                
        }
        
        AppGlobals.diasDisponibles = 0;
        
        switch(index)
        {
            case 10: //Compensatoria
                var tarea = 'diasDisponiblesCompensatoria';
                break;
            case 18: //Doble Turno
                var tarea = 'diasDisponiblesDobleTurno';
                break;
            default://Todas las demas
                var tarea = 'diasDisponibles';
                break;      
        }

        Ext.Ajax.request({
            url: 'app/proxy.licencias.php',
            params:
            {
                tarea: tarea,
                tipoLic: index,
                legajo: AppGlobals.legAgente
            },
            success: function(response)
            {
                console.log("Success: "+response.responseText);
                var obj = Ext.decode(response.responseText);
                var tomados = parseInt(obj.diasTomados);
                var disponibles = obj.diasDisponibles==""?"":parseInt(obj.diasDisponibles);
                var totalDias = typeof obj.totalDias!='undefined'?parseInt(totalDias):0;
                switch(index)
                {
                    case 5: //Maternidad
                        break;
                    case 12: //Estudio
                        Ext.getCmp('12_disponibles').setValue(disponibles);
                        Ext.getCmp('12_tomados').setValue(tomados);
                        break;
                    default:
                        Ext.getCmp('disponibles').setValue(disponibles);
                        Ext.getCmp('tomados').setValue(tomados);
                        break;
                }
                
                AppGlobals.diasDisponibles = disponibles;
                AppGlobals.diasLicPorAnio = totalDias;

                switch(index)
                {
                    case 10: //Compensatorias, acá SI bloqueo si se ha tomado todos los dias
                    {
                        if(disponibles<=0)
                        {
                            Ext.Msg.alert('Informaci&oacute;n', 'No tiene dias disponibles, cargue compensatoria primero');
                            Ext.getCmp('desde').setReadOnly(true);
                            Ext.getCmp('hasta').setReadOnly(true);
                            Ext.getCmp('causa').setReadOnly(true);
                        }
                        else
                        {
                            Ext.getCmp('desde').setReadOnly(false);
                            Ext.getCmp('hasta').setReadOnly(false);
                            Ext.getCmp('causa').setReadOnly(false);
                        }
                        break;
                    }
                    case 18: //Doble Turno, aca si bloqueo si se ha tomado todos los dias
                    {
                        if(disponibles<=0)
                        {
                            Ext.Msg.alert('Informaci&oacute;n', 'No tiene dias disponibles, cargue dias trabajados doble turno primero');
                            Ext.getCmp('desde').setReadOnly(true);
                            Ext.getCmp('hasta').setReadOnly(true);
                            Ext.getCmp('causa').setReadOnly(true);
                        }
                        else
                        {
                            Ext.getCmp('desde').setReadOnly(false);
                            Ext.getCmp('hasta').setReadOnly(false);
                            Ext.getCmp('causa').setReadOnly(false);
                        }
                        break;
                    }
                    default:
                    {
                        if(tomados>totalDias) //Si se tomó mas dias que los disponbiles, largo una alerta pero igual dejo seguir cargando.
                        {
                            Ext.Msg.alert('Advertencia', 'Atenci&oacute;n, el agente ya se tom&oacute; todos los d&iacute;as que ten&iacute;a disponibles en el año');
                        }
                        break;
                    }
                } //end //end switch(index)
            }, //End success
            failure: function(response)
            {
                console.log("Failure: "+response.responseText);
                Ext.Msg.alert('Informacion', 'Error de servidor. <br>Estado: ' + response.status);
                console.log('server-side failure with status code ' + response.status);
            }
        });
    },

    /* Ventana de Feriados  */
    onCargaFeriados: function(response)
    {
        AppGlobals.feriados = [];
        var obj = Ext.decode(response.responseText);
        //console.log(obj);
        for (i=0; i < obj.results.length; i++) 
        {
            AppGlobals.feriados.push(obj.results[i].mes+'-'+obj.results[i].dia);
        }
    },

    onAddFeriado: function(button)
    {
        console.log('*** Add Feriado');
        console.log(button.up('grid').getPlugin('pluginRowEditing'));
        button.up('grid').getPlugin('pluginRowEditing').cancelEdit();
        button.up('grid').down('button[name=guardarFeriados]').setDisabled(false);
        var r = Ext.create('personal.model.Feriado', 
        {
            id: 0,
            /*anio: '2017',
            mes: '12',
            dia: '12',*/
            ocasion: 'Nombre feriado',
            fecha: Ext.Date.clearTime(new Date())
                                        
        });
        console.log(r);
        Ext.getStore('FeriadosStore').insert(0, r);
        button.up('grid').getPlugin('pluginRowEditing').startEdit(0, 0);
    },

    onSelectFeriado: function(selectionModel, records)
    {
        console.log("Click feriado");
        this.lookupReference('FeriadosGrilla').down('button[name=eliminarFeriado]').setDisabled(!records.length);
    },

    onEliminarFeriado: function(button)
    {
        console.log('*** Eliminar Feriados');
        var sm = button.up('grid').getSelectionModel();
        var arr = sm.getSelection();
        //console.log(button.up('grid'));
        button.up('grid').down('button[name=guardarFeriados]').setDisabled(false);
        //console.log(this.up('Feriados').getStore());
        //console.log(this.up('Feriados').getSelectionModel().view);
        button.up('grid').getSelectionModel().view.addRowCls(2, 'hidden');
        button.up('grid').getPlugin('pluginRowEditing').cancelEdit();
        Ext.getStore('FeriadosStore').remove(sm.getSelection());
        if (Ext.getStore('FeriadosStore').getCount() > 0) 
        {
            sm.select(0);
        }
    },

    onCancelaFeriados: function(button)
    {
        console.log('*** Cierra ventana de agregar feriados');
        //Ext.ComponentQuery.query('Feriados')[0].close();
        console.log(Ext.ComponentQuery.query('Feriados')[0]);
    },

    onSaveFeriados: function(button)
    {
        console.log('*** Save Feriados');
        var me = this;
        var modified = button.up('grid').getStore().getModifiedRecords();
        console.log(modified);        
        //Si hubo modificaciones, guardo los cambios
        //Hago un arreglo de los registros eliminados
        var eliminados = button.up('grid').getStore().getRemovedRecords();
        console.log(eliminados);
        var recordsToDelete = [];
        for(i=0;i<eliminados.length;i++)
        {
            //console.log(eliminados[i].internalId);
            recordsToDelete.push(Ext.apply({id:eliminados[i].internalId},eliminados[i].data));
        }
        recordsToDelete = Ext.encode(recordsToDelete);
        
        if(!Ext.isEmpty(modified) || !Ext.isEmpty(eliminados))
        {
            var recordsToSend = [];
            Ext.each(modified, function(record) 
            { 
                console.log(record.data);
                recordsToSend.push(Ext.apply({id:record.id},record.data));
            });
            button.up('grid').el.mask('Guardando…', 'x-mask-loading'); 
            
                
            recordsToSend = Ext.encode(recordsToSend); 
            console.log(recordsToSend);
            //console.log("Tipo de datos: "+typeof recordsToSend);
            Ext.Ajax.request({      
                url : 'app/proxy.feriados.php',
                method: 'POST',
                params :{
                    records : recordsToSend,
                    del: recordsToDelete,
                    tarea: 'setFeriados'
                },
                scope:this,
                success : function(response) 
                {
                    var obj = Ext.decode(response.responseText);
                    console.log(response.responseText);
                    console.log("Entra al success: "+obj.cambios);
                    button.up('FeriadosGrilla').el.unmask();
                    button.up('FeriadosGrilla').getStore().commitChanges();
                    Ext.toast('Cambios guardados correctamente: '+obj.cambios+' feriados(s) modificado(s), '+obj.nuevos+' feriado(s) nuevo(s), '+obj.eliminados+' feriado(s) eliminado(s)', 'Informaci&oacute;n');
                    
                    // Recargo nuevamente los feriados con los cambios realizados
                    Ext.Ajax.request(
                    {
                        url: 'app/proxy.feriados.php',
                        params: {
                            tarea: 'diasLaborales',
                            anio: new Date().getFullYear()
                        },
                        success:function(response){
                            //console.log(response.responseText)
                            me.onCargaFeriados(response);
                        },
                        failure: function(response, opts) 
                        {
                            Ext.Msg.alert('Informacion', 'Error de servidor. <br>Estado: ' + response.status);
                            console.log('server-side failure with status code ' + response.status);
                        }
                    });
                },
                failure: function(response)
                {
                    var obj = Ext.decode(response.responseText);
                    console.log("Entra al failure "+response.responseText);
                    button.up('grid').el.unmask();
                    button.up('grid').getStore().commitChanges();
                    Ext.Msg.alert("Error", "Error al guardar cambios: "+obj.error);
                }
            });
        }
        else
        {
            Ext.Msg.alert("Error", "No hay cambios para guardar");
        }
    },

    openWindowFeriados: function(button)
    {
        console.log('*** Edit Feriados');
        var win = Ext.widget('Feriados');
        win.show();
    },
    
    /* Ventana de Lic Compensatoria */

    onAddCompensatoria: function(button)
    {
        console.log('*** Click Agregar Compensatoria');
        var win = Ext.widget('PersonalCompensatoria');
        win.show();
        Ext.getCmp("legajo").setValue(AppGlobals.legAgente);
    },


    onSaveCompensatoria: function (button)
    {
        console.log('*** Save Compensatoria');
        var frm = button.up('window').down('form').getForm();
        var me = this;
        if (frm.isValid())
        {
            //Valido nuevamente que la cantidad de días sea la correcta (de 1 a 31 dias de licencia)
            if(Ext.getCmp('perComDias').getValue()>0 && Ext.getCmp('perComDias').getValue()<32)
            {
                Ext.Msg.show({
                    title:'Guardar licencia',
                    msg: 'Guardar licencia compensatoria desde el '+Ext.getCmp('perComDesde').getValue()+' hasta el '+Ext.getCmp('perComHasta').getValue()+' de enero de '+Ext.getCmp('perComAnio').getValue()+', total '+Ext.getCmp('perComDias').getValue()+' dias?',
                    buttons: Ext.Msg.YESNOCANCEL,
                    buttonText :
                    {
                        yes : 'Si',
                        no : 'No',
                        cancel : 'Cancelar'
                    },
                    icon: Ext.Msg.QUESTION,
                    fn: function(buttonId)
                    {
                        if(buttonId === 'yes')
                        {
                            button.setDisabled(true); //Desactivo el boton para que no haya multiples cargas. Bug corregido Mauro
                            frm.submit({
                                url: 'app/proxy.licencias.php',
                                method: 'POST',
                                params: {
                                        tarea: 'setDiasCompensatoria',
                                        legajo: AppGlobals.legAgente
                                },
                                success: function(frm, action) 
                                {
                                    console.log("Entro al success: "+action.response.responseText);
                                    Ext.toast('D&iacute;as cargados correctamente', 'Informaci&oacute;n');
                                    frm.reset();
                                    button.setDisabled(false); //Habilito nuevamente el boton
                                    Ext.ComponentQuery.query('PersonalCompensatoria')[0].close();
                                    Ext.getStore('HDiasTrabajadosStore').reload();
                                    me.diasDisponibles(10); //Actualizo dias disponibles. Bug corregido Mauro
                                    //Desbloqueo los campos
                                },
                                failure: function(frm, action) 
                                {
                                    var obj = Ext.decode(action.response.responseText);
                                    console.log("Entro al failure: "+action.response.responseText);
                                    Ext.Msg.alert('Error', obj.error);
                                    button.setDisabled(false); //Habilito nuevamente el boton
                                }
                            });
                        }// end if(buttonId === 'yes')
                    }
                });
            }
        }
    },

    cancelaCompensatoria: function()
    {
        console.log('*** Cierra ventana de agregar compensatoria');
        Ext.ComponentQuery.query('PersonalCompensatoria')[0].close();
        //Ext.widget('PersonalCompensatoria').close();
    },
    
    /* Ventana de Lic Doble Turno */

    onAddDobleTurno: function(button) {
        console.log('*** Click Agregar Doble Turno');
        var win = Ext.widget('PersonalDobleTurno');
        win.show();
        Ext.getCmp("legajo").setValue(AppGlobals.legAgente);
                
    },


    onSaveDobleTurno: function (button) 
    {
        console.log('*** Save Doble Turno');
        var frm = button.up('window').down('form').getForm();
        var me = this;   
        if (frm.isValid())
        {
            //Valido nuevamente que la cantidad de días sea la correcta (de 1 a 31 dias de licencia)
            if(Ext.getCmp('perDTDias').getValue()>0)
            {
                Ext.Msg.show({
                    title:'Guardar licencia por doble turno',
                    msg: 'Guardar licencia por doble turno desde el '+Ext.Date.format(Ext.getCmp('perDTDesde').getValue(),"d-m-Y")+' hasta el '+Ext.Date.format(Ext.getCmp('perDTHasta').getValue(),"d-m-Y")+', total '+Ext.getCmp('perDTDias').getValue()+' dias?',
                    buttons: Ext.Msg.YESNOCANCEL,
                    buttonText : 
                    {
                        yes : 'Si',
                        no : 'No',
                        cancel : 'Cancelar'
                    },
                    icon: Ext.Msg.QUESTION,
                    fn: function(buttonId)
                    {
                        if(buttonId === 'yes')
                        {
                            button.setDisabled(true); //Desactivo el boton para que no haya multiples cargas. Bug corregido Mauro
                            frm.submit({
                                url: 'app/proxy.licencias.php',
                                method: 'POST',
                                params: {
                                        tarea: 'setDiasDobleTurno',
                                        legajo: AppGlobals.legAgente
                                },
                                success: function(frm, action) 
                                {
                                    console.log("Entro al success: "+action.response.responseText);
                                    Ext.toast('D&iacute;as cargados correctamente', 'Informaci&oacute;n');
                                    frm.reset();
                                    button.setDisabled(false); //Habilito nuevamente el boton
                                    Ext.ComponentQuery.query('PersonalDobleTurno')[0].close();
                                    Ext.getStore('HDiasTrabajadosStore').reload();
                                },
                                failure: function(frm, action) 
                                {
                                    var obj = Ext.decode(action.response.responseText);
                                    console.log("Entro al failure: "+action.response.responseText);
                                    Ext.Msg.alert('Error', obj.error);
                                    button.setDisabled(false); //Habilito nuevamente el boton
                                }
                            });
                        }// end if(buttonId === 'yes')
                    }
                });
            }
        }
        else
        {
            Ext.Msg.alert("Error", "Complete correcatamente los campos");
        }
    },

    cancelaDobleTurno: function() 
    {
        console.log('*** Cierra ventana de agregar doble turno');
        Ext.ComponentQuery.query('PersonalDobleTurno')[0].close();
    },

    /* Ventana agregar comision */

    onAgregarComision: function(button) {
        console.log('*** Click Agregar Comision');
        var win = Ext.widget('LComision');
        win.show();
        Ext.getCmp("legajoComision").setValue(AppGlobals.legAgente);
                
    },

    /* Ventana editar comision */

    onEditarComision: function(button) 
    {
        console.log('*** Click editar Comision');
        var win = Ext.widget('editarLComision');
        win.show();
        var reg = button.up('grid').getSelectionModel().getSelection();
        //Relleno el formulario con los datos
        //var f = this.getPersonalModificarcapacitacion();
        //var frm = f.down('form').getForm();
        
        Ext.getCmp("legajoEditarComision").setValue(AppGlobals.legAgente);
        Ext.getCmp('editarComisionDesde').setValue(reg[0].data.desde);
        Ext.getCmp('editarComisionHasta').setValue(reg[0].data.hasta);
        Ext.getCmp('editarComisionTotalDias').setValue(reg[0].data.total_dias);
        Ext.getCmp('editarComisionCausa').setValue(reg[0].data.causa);
        Ext.getCmp('editarComisionExpediente').setValue(reg[0].data.expediente);
        Ext.getCmp('editarComisionObservaciones').setValue(reg[0].data.observaciones);
    },

    onEliminarComision: function(button)
    {
        console.log('*** Click eliminar Comision');
        var reg = button.up('grid').getSelectionModel().getSelection();
        var sto = Ext.getStore('ComisionStore');                 
        Ext.Msg.show({
            title:'Eliminar comision',
            msg: 'Seguro que desea eliminar la comisi&oacute;n?',
            buttons: Ext.Msg.YESNOCANCEL,
            buttonText : 
            {
                yes : 'Si',
                no : 'No',
                cancel : 'Cancelar'
            },
            icon: Ext.Msg.QUESTION,
            fn: function(buttonId)
            {
                if(buttonId === 'yes')
                {
                    button.setDisabled(true); //Desactivo el boton para que no haya multiples cargas. Bug corregido Mauro
                    Ext.Ajax.request({
                    url: 'app/proxy.licencias.php',
                    method: 'POST',
                    params: {
                            tarea: 'eliminarComision',
                            id: reg[0].data.id,
                    },
                    success: function(action) 
                    {
                        var obj = Ext.decode(action.responseText);
                        if(obj.success)
                        {
                            Ext.toast(obj.msg, 'Informaci&oacute;n');
                        }
                        else
                        {
                            Ext.toast(obj.error, 'Error');
                        }
                        button.setDisabled(false); //Habilito nuevamente el boton
                        sto.reload();
                            
                    },
                    failure: function(action) 
                    {
                        var obj = Ext.decode(action.responseText);
                        console.log("Entro al failure: "+action.responseText);
                        Ext.Msg.alert('Error', obj.error);
                        button.setDisabled(false); //Habilito nuevamente el boton
                    }
                });
                }// end if(buttonId === 'yes')
            }
        });
        
    },
    /* Calcular cantidad de dias en funcion del periodo qu ese elige para la comision */
    
    onChangeComisionHasta: function(field, newValue, oldValue, eOpts) 
    {
        var fechaDesde = Ext.getCmp('comisionDesde').getValue();
        var fechaHasta = Ext.getCmp('comisionHasta').getValue();
        if(fechaDesde!="" && fechaDesde!=null && fechaHasta!="" && fechaHasta!=null)
        {
            var diasLaborables = this.contarDiasLaborales(Ext.getCmp('comisionDesde').getValue(), Ext.getCmp('comisionHasta').getValue(), false, true);//Bug corregido Mauro
            if(typeof (diasLaborables) != 'undefined') //Si es undefined, es porque no se ha calculado aun. Bug corregido Mauro
            {
                Ext.getCmp('comisionTotalDias').setValue(diasLaborables);
            }
        }
    },

    onChangeEditarComisionHasta: function(field, newValue, oldValue, eOpts) 
    {
        var fechaDesde = Ext.getCmp('editarComisionDesde').getValue();
        var fechaHasta = Ext.getCmp('editarComisionHasta').getValue();
        if(fechaDesde!="" && fechaDesde!=null && fechaHasta!="" && fechaHasta!=null)
        {
            var diasLaborables = this.contarDiasLaborales(Ext.getCmp('editarComisionDesde').getValue(), Ext.getCmp('editarComisionHasta').getValue(), false, true);//Bug corregido Mauro
            if(typeof (diasLaborables) != 'undefined') //Si es undefined, es porque no se ha calculado aun. Bug corregido Mauro
            {
                Ext.getCmp('editarComisionTotalDias').setValue(diasLaborables);
            }
        }
    },

    onSaveComision: function (button) 
    {
        console.log('*** Save Comision');
        var frm = button.up('window').down('form').getForm();
        var sto = Ext.getStore('ComisionStore');                 
        if (frm.isValid())
        {
            //Valido nuevamente que la cantidad de días sea la correcta (de 1 a 31 dias de licencia)
            if(Ext.getCmp('comisionTotalDias').getValue()>0)
            {
                Ext.Msg.show({
                title:'Guardar comisi&oacute;n',
                msg: 'Guardar comisi&oacute;n desde el '+Ext.Date.format(Ext.getCmp('comisionDesde').getValue(),"d-m-Y")+' hasta el '+Ext.Date.format(Ext.getCmp('comisionHasta').getValue(),"d-m-Y")+', total '+Ext.getCmp('comisionTotalDias').getValue()+' dias?',
                buttons: Ext.Msg.YESNOCANCEL,
                buttonText : 
                {
                    yes : 'Si',
                    no : 'No',
                    cancel : 'Cancelar'
                },
                icon: Ext.Msg.QUESTION,
                fn: function(buttonId)
                {
                    if(buttonId === 'yes')
                    {
                        button.setDisabled(true); //Desactivo el boton para que no haya multiples cargas. Bug corregido Mauro
                        frm.submit({
                        url: 'app/proxy.licencias.php',
                        method: 'POST',
                        params: {
                                tarea: 'saveComision',
                                legajo: AppGlobals.legAgente
                        },
                        success: function(frm, action) {
                                console.log("Entro al success: "+action.response.responseText);
                                Ext.toast('Comisi&oacute;n cargada correctamente', 'Informaci&oacute;n');
                                button.setDisabled(false); //Habilito nuevamente el boton
                                Ext.ComponentQuery.query('LComision')[0].close();
                                sto.reload();
                                
                        },
                        failure: function(frm, action) {
                            var obj = Ext.decode(action.response.responseText);
                                console.log("Entro al failure: "+action.response.responseText);
                                Ext.Msg.alert('Error', obj.error);
                                button.setDisabled(false); //Habilito nuevamente el boton
                        }
                    });
                    }// end if(buttonId === 'yes')
                }
            });
            }
            else
            {
                Ext.Msg.alert("Error", "Complete correcatamente los campos");
            }
        }
        else
        {
            Ext.Msg.alert("Error", "Complete correcatamente los campos");
        }
                
        
    },

    onSaveEditarComision: function (button) 
    {
        console.log('*** Save Editar Comision');
        var frm = button.up('window').down('form').getForm();
        var sto = Ext.getStore('ComisionStore'); 
        var reg = this.lookupReference('PersonalFichaComisiones').getSelectionModel().getSelection();
        if (frm.isValid())
        {
            //Valido nuevamente que la cantidad de días sea la correcta (de 1 a 31 dias de licencia)
            if(Ext.getCmp('editarComisionTotalDias').getValue()>0)
            {
                Ext.Msg.show({
                title:'Guardar comisi&oacute;n',
                msg: 'Guardar comisi&oacute;n desde el '+Ext.Date.format(Ext.getCmp('editarComisionDesde').getValue(),"d-m-Y")+' hasta el '+Ext.Date.format(Ext.getCmp('editarComisionHasta').getValue(),"d-m-Y")+', total '+Ext.getCmp('editarComisionTotalDias').getValue()+' dias?',
                buttons: Ext.Msg.YESNOCANCEL,
                buttonText : 
                {
                    yes : 'Si',
                    no : 'No',
                    cancel : 'Cancelar'
                },
                icon: Ext.Msg.QUESTION,
                fn: function(buttonId)
                {
                    if(buttonId === 'yes')
                    {
                        button.setDisabled(true); //Desactivo el boton para que no haya multiples cargas. Bug corregido Mauro
                        frm.submit({
                        url: 'app/proxy.licencias.php',
                        method: 'POST',
                        params: {
                                tarea: 'editComision',
                                idLicencia: reg[0].data.id

                        },
                        success: function(frm, action) {
                                console.log("Entro al success: "+action.response.responseText);
                                Ext.toast('Comisi&oacute;n editada correctamente', 'Informaci&oacute;n');
                                button.setDisabled(false); //Habilito nuevamente el boton
                                Ext.ComponentQuery.query('editarLComision')[0].close();
                                sto.reload();
                                
                        },
                        failure: function(frm, action) {
                            var obj = Ext.decode(action.response.responseText);
                                console.log("Entro al failure: "+action.response.responseText);
                                Ext.Msg.alert('Error', obj.error);
                                button.setDisabled(false); //Habilito nuevamente el boton
                        }
                    });
                    }// end if(buttonId === 'yes')
                }
            });
            }
            else
            {
                Ext.Msg.alert("Error", "Complete correcatamente los campos");
            }
        }
        else
        {
            Ext.Msg.alert("Error", "Complete correcatamente los campos");
        }
    },
    
    cancelaComision: function() 
    {
        console.log('*** Cierra ventana de agregar comision');
        Ext.ComponentQuery.query('LComision')[0].close();
    },

    cancelaEditarComision: function() 
    {
        console.log('*** Cierra ventana de editar comision');
        Ext.ComponentQuery.query('editarLComision')[0].close();
    },

    selectComision: function (selectionModel, records)
    {
        console.log('*** Selecciona comision');
        Ext.getCmp('editarComision').setDisabled(!records.length);
        Ext.getCmp('quitarComision').setDisabled(!records.length);
        //Guardo el registro seleccionado, para rellenar la ventana con los datos si el usuario quiere editar la comision
        AppGlobals.recordEditComision = records;
        console.log(AppGlobals.recordEditComision);
    },
    /* ################################ LICENCIAS ################################*/

    /*** Licencias***/

    onChangeHasta: function(field, newValue, oldValue, eOpts) 
    {
        console.log("Pick date licencia");
        var fechaDesde = Ext.getCmp('desde').getValue();
        var fechaHasta = Ext.getCmp('hasta').getValue();
        var dias_habiles = AppGlobals.licenciaSeleccionadaAltaCorridos==1?false:true;
        var mismo_anio = AppGlobals.licenciaSeleccionadaMismoAnio==1?true:false;
        if(fechaDesde!="" && fechaDesde!=null && fechaHasta!="" && fechaHasta!=null)
        {
            var diasLaborables = this.contarDiasLaborales(Ext.getCmp('desde').getValue(), Ext.getCmp('hasta').getValue(), Ext.getCmp('habiles').getValue(), mismo_anio);
            if(typeof (diasLaborables) != 'undefined') //Si es undefined, es porque no se ha calculado aun. Bug corregido Mauro
            {
                if (diasLaborables > AppGlobals.diasDisponibles) 
                {
                    Ext.Msg.alert('Atencion', 'Esta a punto de cargar m&aacute;s d&iacute;as de los que tiene disponibles.'); 
                } 
                Ext.getCmp('diasLicencia').setValue(diasLaborables);
            }
        }
    },
        
    onSaveLicencia: function(button) 
    {
        var frm = button.up('form').getForm();
        //console.log(Ext.getCmp('1_DiasOtorgados').getValue());
        var diasLicencia = 0;
        switch(AppGlobals.licenciaSeleccionadaAlta)
        {
            case 5://Maternidad
                diasLicencia = Ext.getCmp('5_diasLicencia').getValue();
                break;
            case 12://Estudio
                diasLicencia = Ext.getCmp('12_diasLicencia').getValue();
                break;  
            default:
                diasLicencia = Ext.getCmp('diasLicencia').getValue();
        }

        if(diasLicencia == 0)
        {
            Ext.Msg.alert('Error', 'Los dias otorgados son incorrectos, cargue otra fecha');
        }
        else
        {
            button.setDisabled(true); //Desactivo el boton guardar para evitar multiples cargas
            button.setText("Espere...");
            console.log('*** clicked the Save button');
            this.saveLicencia(frm, AppGlobals.licenciaSeleccionadaAlta, button);
        }
    },
    /* Ventana agregar solicitud licencia */

    onAgregarSolicitudLicencia: function(button) {
        console.log('*** Click Agregar solicitud licencia');
        var win = Ext.widget('SolicitudLicencia');

        //Busco el domicilio del agente
        Ext.Ajax.request({
            url: 'app/proxy.agentes.php',
            method: 'GET',
            params: {
                    act: 'getAgenteByLegajo',
                    leg: AppGlobals.legAgente
            },
            success: function(action) 
            {
                var obj = Ext.decode(action.responseText);
                console.log(obj);
                Ext.getCmp("domicilioSolicitante").setValue(obj.results[0].domic);
            },
            failure: function(action) {
                var obj = Ext.decode(action.responseText);
                console.log("Entro al failure: "+action.responseText);
                Ext.toast('No se pudo cargar el domicilio del agente. Por favor cargarlo manualmente.', 'Error');
            }
        });
        Ext.getCmp("legajoSolicitudLicencia").setValue(AppGlobals.legAgente);
        win.show();
    },

    /* Ventana editar solicitud licencia */

    onEditarSolicitudLicencia: function(button) {
        console.log('*** Click editar solicitud licencia');
        var win = Ext.widget('ModificarSolicitudLicencia');
        var reg = Ext.getCmp('PersonalFichaSolicitudLicencias').getSelectionModel().getSelection();
        console.log(reg);
        win.show();
        //Relleno el formulario con los datos
        Ext.getCmp("modificarTipoLicencia").setValue(reg[0].data.id_tipo_licencia);
        Ext.getCmp('modificarLegajoSolicitudLicencia').setValue(AppGlobals.legAgente);
        Ext.getCmp('modificarSolicitudLicenciaDesde').setValue(reg[0].data.desde);
        Ext.getCmp('modificarSolicitudCausa').setValue(reg[0].data.causa);
        Ext.getCmp('modificarSolicitudExpediente').setValue(reg[0].data.expediente);
        Ext.getCmp('modificarSolicitudObservaciones').setValue(reg[0].data.observaciones);
        Ext.getCmp('modificarSolicitudSolicitante').setValue(reg[0].data.nombre_solicitante);
        Ext.getCmp('modificarDomicilioSolicitante').setValue(reg[0].data.direccion);
        //Ext.getCmp('modificarHoraSolicitud').setValue(reg[0].data.hora_solicitud);
        Ext.getCmp('modificarPierdePresentismoSolicitud').setValue(reg[0].data.pierde_presentismo=="NO"?false:true);
        
    },

    onEliminarSolicitudLicencia: function(button)
    {
        console.log('*** Delete solicitud licencia');
        var sto = Ext.getStore('HSolicitudLicenciaStore');     
        var reg = button.up('grid').getSelectionModel().getSelection();            
        Ext.Msg.show({
                title:'Eliminar solicitud de licencia',
                msg: 'Seguro que desea eliminar la solicitud?',
                buttons: Ext.Msg.YESNOCANCEL,
                buttonText : 
                {
                    yes : 'Si',
                    no : 'No',
                    cancel : 'Cancelar'
                },
                icon: Ext.Msg.QUESTION,
                fn: function(buttonId)
                {
                    if(buttonId === 'yes')
                    {
                        button.setDisabled(true); //Desactivo el boton para que no haya multiples cargas. Bug corregido Mauro
                        Ext.Ajax.request({
                        url: 'app/proxy.licencias.php',
                        method: 'POST',
                        params: {
                                tarea: 'deleteSolicitudLicencia',
                                idSolicitud: reg[0].data.id,
                                legajo: AppGlobals.legAgente
                        },
                        success: function(action) {
                                var obj = Ext.decode(action.responseText);
                                if(obj.success)
                                {
                                    Ext.toast(obj.msg, 'Informaci&oacute;n');
                                }
                                else
                                {
                                    Ext.toast(obj.error, 'Error');
                                }
                                button.setDisabled(false); //Habilito nuevamente el boton
                                sto.reload();
                                
                        },
                        failure: function(action) {
                            var obj = Ext.decode(action.responseText);
                                console.log("Entro al failure: "+action.responseText);
                                Ext.Msg.alert('Error', obj.error);
                                button.setDisabled(false); //Habilito nuevamente el boton
                        }
                    });
                    }// end if(buttonId === 'yes')
                }
            });
    },

    onConfirmarSolicitudLicencia: function(button)
    {
        console.log('*** Click confirmar solicitud licencia');
        var win = Ext.widget('ConfirmarSolicitudLicencia');
        var reg = button.up('grid').getSelectionModel().getSelection();
        console.log(reg);
        win.show();
        
        //Obtengo los dias disponibles
        Ext.Ajax.request({
            url: 'app/proxy.licencias.php',
            params:
            {
                tarea: 'diasDisponibles',
                tipoLic: reg[0].data.id_tipo_licencia,
                legajo: AppGlobals.legAgente
            },
            success: function(response)
            {
                console.log("Success dias disponibles: "+response.responseText);
                var obj = Ext.decode(response.responseText);
                var tomados = parseInt(obj.diasTomados);
                var disponibles = obj.diasDisponibles==""?"":parseInt(obj.diasDisponibles);
                var totalDias = typeof obj.totalDias!='undefined'?parseInt(totalDias):0;
                AppGlobals.diasDisponibles = disponibles;
                AppGlobals.diasLicPorAnio = totalDias;

                if(tomados>totalDias) //Si se tomó mas dias que los disponbiles, largo una alerta pero igual dejo seguir cargando.
                {
                    Ext.Msg.alert('Advertencia', 'Atenci&oacute;n, el agente ya se tom&oacute; todos los d&iacute;as que ten&iacute;a disponibles en el año');
                }
            }, //End success
            failure: function(response)
            {
                console.log("Failure: "+response.responseText);
                Ext.Msg.alert('Informacion', 'Error de servidor. <br>Estado: ' + response.status);
                console.log('server-side failure with status code ' + response.status);
            }
        });
        //Relleno el formulario con los datos
        Ext.getCmp("confirmarTipoLicencia").setValue(reg[0].data.id_tipo_licencia);
        Ext.getCmp('confirmarLegajoSolicitudLicencia').setValue(AppGlobals.legAgente);
        Ext.getCmp('confirmarSolicitudLicenciaDesde').setValue(reg[0].data.desde);
        Ext.getCmp('confirmarSolicitudCausa').setValue(reg[0].data.causa);
        Ext.getCmp('confirmarSolicitudExpediente').setValue(reg[0].data.expediente);
        Ext.getCmp('confirmarSolicitudObservaciones').setValue(reg[0].data.observaciones);
        Ext.getCmp('confirmarSolicitudSolicitante').setValue(reg[0].data.nombre_solicitante);
        Ext.getCmp('confirmarDomicilioSolicitante').setValue(reg[0].data.direccion);
        //Ext.getCmp('confirmarHoraSolicitud').setValue(reg[0].data.hora_solicitud);
        Ext.getCmp('confirmarPierdePresentismoSolicitud').setValue(reg[0].data.pierde_presentismo==1?true:false);
        Ext.getCmp('habiles_confirmar_licencia').setValue(reg[0].data.dias_corridos==1?false:true);
        AppGlobals.confirmarLicenciaAltaCorridos = reg[0].data.dias_corridos;
        console.log("Dias habiles o corridos: "+AppGlobals.confirmarLicenciaAltaCorridos);
    },

    onSaveSolicitudLicencia: function (button) 
    {
        console.log('*** Save solicitud licencia');
        var frm = button.up('window').down('form').getForm();
        var sto = Ext.getStore('HSolicitudLicenciaStore');                 
        if (frm.isValid())
        {
            Ext.Msg.show({
                title:'Guardar solicitud de licencia',
                msg: 'Guardar solicitud de licencia?',
                buttons: Ext.Msg.YESNOCANCEL,
                buttonText : 
                {
                    yes : 'Si',
                    no : 'No',
                    cancel : 'Cancelar'
                },
                icon: Ext.Msg.QUESTION,
                fn: function(buttonId)
                {
                    if(buttonId === 'yes')
                    {
                        button.setDisabled(true); //Desactivo el boton para que no haya multiples cargas. Bug corregido Mauro
                        frm.submit({
                        url: 'app/proxy.licencias.php',
                        method: 'POST',
                        params: {
                                tarea: 'saveSolicitudLicencia',
                                legajo: AppGlobals.legAgente
                        },
                        success: function(frm, action) {
                                console.log("Entro al success: "+action.response.responseText);
                                Ext.toast('Solicitud cargada correctamente', 'Informaci&oacute;n');
                                button.setDisabled(false); //Habilito nuevamente el boton
                                Ext.ComponentQuery.query('SolicitudLicencia')[0].close();
                                sto.reload();
                                
                        },
                        failure: function(frm, action) {
                            var obj = Ext.decode(action.response.responseText);
                                console.log("Entro al failure: "+action.response.responseText);
                                Ext.Msg.alert('Error', obj.error);
                                button.setDisabled(false); //Habilito nuevamente el boton
                        }
                    });
                    }// end if(buttonId === 'yes')
                }
            });
        }
        else
        {
            Ext.Msg.alert("Error", "Complete correcatamente los campos");
        }
    },

    cancelaSolicitudLicencia: function() 
    {
        console.log('*** Cierra ventana de agregar solicitud de licencia');
        Ext.ComponentQuery.query('SolicitudLicencia')[0].close();
    },

    cancelaEditarSolicitudLicencia: function() 
    {
        console.log('*** Cierra ventana de editar solicitud de licencia');
        Ext.ComponentQuery.query('ModificarSolicitudLicencia')[0].close();
    },

    cancelaConfirmarSolicitudLicencia: function() 
    {
        console.log('*** Cierra ventana de confirmar solicitud de licencia');
        Ext.ComponentQuery.query('ConfirmarSolicitudLicencia')[0].close();
    },

    selectSolicitudLicencia: function (selectionModel, records)
    {
        console.log('*** Selecciona solicitud licencia');
        if(AppGlobals.permisos[0].pesta_solicitud_licencias_editar==1)
        {
            if(records.length)
            {
                if(records[0].data.estado!=1 && records[0].data.estado!=3)
                {
                    Ext.getCmp('editarSolicitudLicencia').setDisabled(!records.length);
                    Ext.getCmp('quitarSolicitudLicencia').setDisabled(!records.length);
                    Ext.getCmp('confirmarSolicitudLicencia').setDisabled(!records.length);
                }
                else
                {
                    Ext.getCmp('editarSolicitudLicencia').setDisabled(true);
                    Ext.getCmp('quitarSolicitudLicencia').setDisabled(true);
                    Ext.getCmp('confirmarSolicitudLicencia').setDisabled(true);
                }
            }
            else
            {
                Ext.getCmp('editarSolicitudLicencia').setDisabled(true);
                Ext.getCmp('quitarSolicitudLicencia').setDisabled(true);
                Ext.getCmp('confirmarSolicitudLicencia').setDisabled(true);
            }
        }
        //Guardo el registro seleccionado, para rellenar la ventana con los datos si el usuario quiere editar la solicitud de licencia
        AppGlobals.recordEditSolicitudLicencia = records;
    },

    onSaveEditarSolicitudLicencia: function (button) 
    {
        console.log('*** Save edicion solicitud licencia');
        var frm = button.up('window').down('form').getForm();
        var sto = Ext.getStore('HSolicitudLicenciaStore');   
        var reg = this.lookupReference('PersonalFichaSolicitudLicencias').getSelectionModel().getSelection();          
        if (frm.isValid())
        {
            Ext.Msg.show({
                title:'Guardar cambios en solicitud de licencia',
                msg: 'Guardar cambios en solicitud de licencia?',
                buttons: Ext.Msg.YESNOCANCEL,
                buttonText : 
                {
                    yes : 'Si',
                    no : 'No',
                    cancel : 'Cancelar'
                },
                icon: Ext.Msg.QUESTION,
                fn: function(buttonId)
                {
                    if(buttonId === 'yes')
                    {
                        button.setDisabled(true); //Desactivo el boton para que no haya multiples cargas. Bug corregido Mauro
                        frm.submit({
                            url: 'app/proxy.licencias.php',
                            method: 'POST',
                            params: {
                                tarea: 'editSolicitudLicencia',
                                idSolicitud: reg[0].data.id,
                                legajo: AppGlobals.legAgente
                            },
                            success: function(frm, action) 
                            {
                                console.log("Entro al success: "+action.response.responseText);
                                Ext.toast('Solicitud cargada correctamente', 'Informaci&oacute;n');
                                button.setDisabled(false); //Habilito nuevamente el boton
                                Ext.ComponentQuery.query('ModificarSolicitudLicencia')[0].close();
                                sto.reload();
                                    
                            },
                            failure: function(frm, action) 
                            {
                                var obj = Ext.decode(action.response.responseText);
                                console.log("Entro al failure: "+action.response.responseText);
                                Ext.Msg.alert('Error', obj.error);
                                button.setDisabled(false); //Habilito nuevamente el boton
                            }
                        });
                    }// end if(buttonId === 'yes')
                }
            });
        }
        else
        {
            Ext.Msg.alert("Error", "Complete correcatamente los campos");
        }
    },

    onSaveConfirmarSolicitudLicencia: function (button) 
    {
        console.log('*** Confirmacion de licencia');
        var frm = button.up('window').down('form').getForm();
        var sto = Ext.getStore('HSolicitudLicenciaStore'); 
        var reg = this.lookupReference('PersonalFichaSolicitudLicencias').getSelectionModel().getSelection();
        if (frm.isValid() && Ext.getCmp('confirmarSolicitudTotalDias').getValue() != 0)
        {
            Ext.Msg.show({
                title:'Confirmar la licencia',
                msg: 'Confirmar la licencia?',
                buttons: Ext.Msg.YESNOCANCEL,
                buttonText : 
                {
                    yes : 'Si',
                    no : 'No',
                    cancel : 'Cancelar'
                },
                icon: Ext.Msg.QUESTION,
                fn: function(buttonId)
                {
                    if(buttonId === 'yes')
                    {
                        button.setDisabled(true); //Desactivo el boton para que no haya multiples cargas. Bug corregido Mauro
                        frm.submit({
                        url: 'app/proxy.licencias.php',
                        method: 'POST',
                        params: {
                                tarea: 'confirmarSolicitudLicencia',
                                idSolicitud: reg[0].data.id,
                                legajo: AppGlobals.legAgente
                        },
                        success: function(frm, action) {
                                console.log("Entro al success: "+action.response.responseText);
                                Ext.toast('Solicitud cargada correctamente', 'Informaci&oacute;n');
                                button.setDisabled(false); //Habilito nuevamente el boton
                                Ext.ComponentQuery.query('ConfirmarSolicitudLicencia')[0].close();
                                sto.reload();
                                
                        },
                        failure: function(frm, action) {
                            var obj = Ext.decode(action.response.responseText);
                                console.log("Entro al failure: "+action.response.responseText);
                                Ext.Msg.alert('Error', obj.error);
                                button.setDisabled(false); //Habilito nuevamente el boton
                        }
                    });
                    }// end if(buttonId === 'yes')
                }
            });
        }
        else
        {
            Ext.Msg.alert("Error", "Complete correcatamente los campos");
        }
    },

    onChangeSolicitudLicenciahasta: function(field, newValue, oldValue, eOpts) {
        var fechaDesde = Ext.getCmp('confirmarSolicitudLicenciaDesde').getValue();
        var fechaHasta = Ext.getCmp('confirmarSolicitudLicenciaHasta').getValue();
        var habiles = AppGlobals.confirmarLicenciaAltaCorridos==1?false:true;
        
        
        if(fechaDesde!="" && fechaDesde!=null && fechaHasta!="" && fechaHasta!=null)
        {
            var diasLaborables = this.contarDiasLaborales(Ext.getCmp('confirmarSolicitudLicenciaDesde').getValue(), Ext.getCmp('confirmarSolicitudLicenciaHasta').getValue(), Ext.getCmp('habiles_confirmar_licencia').getValue(), true);
            if(typeof diasLaborables !== "undefined") //Si es undefined, es porque no se ha calculado aun. Bug corregido Mauro
            {
                if (diasLaborables > AppGlobals.diasDisponibles) 
                {
                    Ext.Msg.alert('Atencion', 'Esta a punto de cargar m&aacute;s d&iacute;as de los que tiene disponibles.'); //Seteo nuevamente las fechas. Bug corregido Mauro

                } 
                Ext.getCmp('confirmarSolicitudTotalDias').setValue(diasLaborables);
            }
        }
    },
    

    
    onChangeLDobleTurnoDias: function(field, newValue, oldValue, eOpts)
    {
        console.log('*** change Doble turno dias');
        var diasLaborables = this.contarDiasLaborales(Ext.getCmp('perDTDesde').getValue(), Ext.getCmp('perDTHasta').getValue(), false, false);
        if(typeof diasLaborables !== "undefined") //Si es undefined, es porque no se ha calculado aun. Bug corregido Mauro
        {
            Ext.getCmp('perDTDias').setValue(diasLaborables);
        }
    },

    clickLicencia: function(grid, recordHist) 
    {
        console.log("click licencia");
        AppGlobals.licencia = recordHist.data;
        if(AppGlobals.permisos[0].pesta_historial_editar == 1)
        {
            Ext.getCmp('btnDesiste').setDisabled(false);
            Ext.getCmp('btnEliminaLicencia').setDisabled(false);
            console.log(recordHist.data);
            AppGlobals.licencia = recordHist.data;
        }
    },

    modificarLicMaternidad: function (button)
    {
        //console.log(button.up().up());
        button.up().up().plugins[0].cancelEdit();
        var sm = button.up().up().getSelectionModel();
        var arr = sm.getSelection();
        if(arr.length > 0)
        {
            var id_edit = parseInt(arr[0].index);
            button.up().up().plugins[0].startEdit(id_edit, 0);
        }
    },

    guardarCambiosLicMaternidad: function(button)
    {
        console.log("Guardar cambios maternidad");
        var modRecords = button.up().up().getStore('historial.HMaternidadStore').getModifiedRecords();
        var sto = button.up().up().getStore('historial.HMaternidadStore');
        
        console.log(modRecords);
        //Si hubo modificaciones, guardo los cambios
        
        if(!Ext.isEmpty(modRecords))
        {
            var recordsToSend = [];
            sto.each(function(record)
            {
                recordsToSend.push(record.data);
            });
            
            button.up().up().el.mask('Guardando�', 'x-mask-loading'); 
                            
            recordsToSend = Ext.encode(recordsToSend); 
            console.log(recordsToSend);
            //console.log("Tipo de datos: "+typeof recordsToSend);
            Ext.Ajax.request({      
            url : 'app/proxy.licencias.php',
            method: 'POST',
            params :{
                records : recordsToSend,
                tarea: 'editLicenciaMaternidad'
            },
            
            success : function(response) 
            {
                var obj = Ext.decode(response.responseText);
                console.log(response.responseText);
                
                button.up().up().el.unmask();
                button.up().up().getStore().commitChanges();
                Ext.toast(obj.msg, "Informaci&oacute;n");
                
                //Recargo la grilla principal y la de evaluados para reflejar los cambios
                Ext.getStore('historial.HMaternidadStore').reload();
            },
            failure: function(response)
            {
                //var obj = Ext.decode(response.responseText);
                console.log("Entra al failure "+response.responseText);
                button.up().up().el.unmask();
                button.up().up().getStore().commitChanges();
                Ext.toast("Error al guardar cambios: "+obj.error, "Error");
            }
            });
        }
        else
        {
            Ext.toast("No hay cambios para guardar", "Error");
        }
    },

    activateBtnsMat: function(selectionModel, records)
    {
        Ext.getCmp('btnEditarMaternidad').setDisabled(!records.length);   
        Ext.getCmp('btnGuardarMaternidad').setDisabled(!records.length);
        Ext.getCmp('btnDesiste').setDisabled(!records.length); 
        Ext.getCmp('btnEliminaLicencia').setDisabled(!records.length);
    },

    activateBtnEditLicencia: function(selectionModel, records)
    {
        console.log("click edit");
        Ext.getCmp('btnEditLicencia').setDisabled(!records.length); 
        Ext.getCmp('btnDesiste').setDisabled(!records.length);
        Ext.getCmp('btnEliminaLicencia').setDisabled(!records.length);  
    },

    onChangeTipoLicencia: function(source) 
    {
        console.log("Change tipo licencia: "+source.value);
        var index = source.value;
        AppGlobals.licenciaSeleccionadaAlta = index;
        console.log(index);
        switch(index)
        {
            case "5": //Maternidad
                Ext.getCmp('matFieldsAltaLicencia_1').setHidden(false);
                Ext.getCmp('matFieldsAltaLicencia_2').setHidden(false);
                Ext.getCmp('estFieldsAltaLicencia').setHidden(true);
                Ext.getCmp('est_altaLicenciaAsignatura').allowBlank = true;
                Ext.getCmp('est_altaLicenciaCarrera').allowBlank = true;
                Ext.getCmp('est_altaLicenciaFechaExamen').allowBlank = true;
                
                Ext.getCmp('mat_altaLicenciaFpp').allowBlank = false;
                
                break;
            case "12": //Estudio
                Ext.getCmp('matFieldsAltaLicencia_1').setHidden(true);
                Ext.getCmp('matFieldsAltaLicencia_2').setHidden(true);
                Ext.getCmp('estFieldsAltaLicencia').setHidden(false);
                Ext.getCmp('est_altaLicenciaAsignatura').allowBlank = false;
                Ext.getCmp('est_altaLicenciaCarrera').allowBlank = false;
                Ext.getCmp('est_altaLicenciaFechaExamen').allowBlank = false;
                Ext.getCmp('mat_altaLicenciaFpp').allowBlank = true;
                break;
            default:
                Ext.getCmp('matFieldsAltaLicencia_1').setHidden(true);
                Ext.getCmp('matFieldsAltaLicencia_2').setHidden(true);
                Ext.getCmp('estFieldsAltaLicencia').setHidden(true);
                Ext.getCmp('est_altaLicenciaAsignatura').allowBlank = true;
                Ext.getCmp('est_altaLicenciaCarrera').allowBlank = true;
                Ext.getCmp('est_altaLicenciaFechaExamen').allowBlank = true;
                Ext.getCmp('mat_altaLicenciaFpp').allowBlank = true;
                break;
        }
        if(index)
        {
            Ext.Ajax.request(
            {      
                url : 'app/proxy.licencias.php',
                method: 'GET',
                async: false,
                params :{
                    idLic : index,
                    tarea: 'getTipoLicenciaById'
                },
                
                success : function(response) 
                {
                    var obj = Ext.decode(response.responseText);
                    
                    AppGlobals.licenciaSeleccionadaAltaCorridos = obj.results[0].dias_corridos;
                    AppGlobals.licenciaSeleccionadaMismoAnio = obj.results[0].mismo_anio;
                    console.log("Dias habiles o corridos: "+AppGlobals.licenciaSeleccionadaAltaCorridos);
                    var checked = AppGlobals.licenciaSeleccionadaAltaCorridos==1?false:true;
                    Ext.getCmp('altaLicenciaHabiles').setValue(checked);
                },
                failure: function(response)
                {
                    //var obj = Ext.decode(response.responseText);
                    console.log("Entra al failure "+response.responseText);
                    Ext.toast("Error al guardar cambios: "+obj.error,"Error");
                }
            });
            AppGlobals.diasDisponibles = 0;
                
            switch(index)
            {
                case "10": //Compensatoria
                    var tarea = 'diasDisponiblesCompensatoria';
                    break;
                case "18": //Doble Turno
                    var tarea = 'diasDisponiblesDobleTurno';
                    break;
                default://Todas las demas
                    var tarea = 'diasDisponibles';
                    break;      
            }
    
            Ext.Ajax.request({
                url: 'app/proxy.licencias.php',
                params:
                {
                    tarea: tarea,
                    tipoLic: index,
                    legajo: AppGlobals.legAgente
                },
                success: function(response)
                {
                    console.log("Success: "+response.responseText);
                    var obj = Ext.decode(response.responseText);
                    var tomados = parseInt(obj.diasTomados);
                    var disponibles = obj.diasDisponibles==""?"":parseInt(obj.diasDisponibles);
                    var totalDias = typeof obj.totalDias!='undefined'?parseInt(totalDias):0;
                    Ext.getCmp('disponiblesAltaLicencia').setValue(disponibles);
                    Ext.getCmp('tomadosAltaLicencia').setValue(tomados);
                    
                    AppGlobals.diasDisponibles = disponibles;
                    AppGlobals.diasLicPorAnio = totalDias;
    
                    switch(index)
                    {
                        case "10": //Compensatorias, acá SI bloqueo si se ha tomado todos los dias
                        case "18": //Doble Turno, aca si bloqueo si se ha tomado todos los dias
                        {
                            if(disponibles<=0)
                            {
                                Ext.Msg.alert('Advertencia', 'No tiene dias disponibles');
                                Ext.getCmp('altaLicenciaDesde').setReadOnly(true);
                                Ext.getCmp('altaLicenciaHasta').setReadOnly(true);
                                Ext.getCmp('altaLicenciaCausa').setReadOnly(true);
                                Ext.getCmp('altaLicenciaExpediente').setReadOnly(true);
                                Ext.getCmp('altaLicenciaObservaciones').setReadOnly(true);
                                Ext.getCmp('altaLicenciaPresentismo').setReadOnly(true);
                                Ext.getCmp('altaLicenciaHabiles').setReadOnly(true);
                                Ext.getCmp('saveAltaLicencia').setDisabled(true);
                            }
                            else
                            {
                                Ext.getCmp('altaLicenciaDesde').setReadOnly(false);
                                Ext.getCmp('altaLicenciaHasta').setReadOnly(false);
                                Ext.getCmp('altaLicenciaCausa').setReadOnly(false);
                                Ext.getCmp('altaLicenciaExpediente').setReadOnly(false);
                                Ext.getCmp('altaLicenciaObservaciones').setReadOnly(false);
                                Ext.getCmp('altaLicenciaPresentismo').setReadOnly(false);
                                Ext.getCmp('altaLicenciaHabiles').setReadOnly(false);
                                Ext.getCmp('saveAltaLicencia').setDisabled(false);
                            }
                            break;
                        }
                        default:
                        {
                            Ext.getCmp('altaLicenciaDesde').setReadOnly(false);
                            Ext.getCmp('altaLicenciaHasta').setReadOnly(false);
                            Ext.getCmp('altaLicenciaCausa').setReadOnly(false);
                            Ext.getCmp('altaLicenciaExpediente').setReadOnly(false);
                            Ext.getCmp('altaLicenciaObservaciones').setReadOnly(false);
                            Ext.getCmp('altaLicenciaPresentismo').setReadOnly(false);
                            Ext.getCmp('altaLicenciaHabiles').setReadOnly(false);
                            Ext.getCmp('saveAltaLicencia').setDisabled(false);
                            if(tomados>totalDias) //Si se tomó mas dias que los disponbiles, largo una alerta pero igual dejo seguir cargando.
                            {
                                Ext.Msg.alert('Advertencia', 'Atenci&oacute;n, el agente ya se tom&oacute; todos los d&iacute;as que ten&iacute;a disponibles en el año');
                            }
                            break;
                        }
                    } //end //end switch(index)
                }, //End success
                failure: function(response)
                {
                    console.log("Failure: "+response.responseText);
                    Ext.Msg.alert('Informacion', 'Error de servidor. <br>Estado: ' + response.status);
                    console.log('server-side failure with status code ' + response.status);
                }
            });
        }
        
    },

    onChangeAltaLicenciaHasta: function(field, newValue, oldValue, eOpts) 
    {
        console.log("Pick date Alta licencia");
        var fechaDesde = Ext.getCmp('altaLicenciaDesde').getValue();
        var fechaHasta = Ext.getCmp('altaLicenciaHasta').getValue();
        var dias_habiles = AppGlobals.licenciaSeleccionadaAltaCorridos==1?false:true;
        var mismo_anio = AppGlobals.licenciaSeleccionadaMismoAnio==1?true:false;
        
        if(fechaDesde!="" && fechaDesde!=null && fechaHasta!="" && fechaHasta!=null)
        {
            var diasLaborables = this.contarDiasLaborales(Ext.getCmp('altaLicenciaDesde').getValue(), Ext.getCmp('altaLicenciaHasta').getValue(), Ext.getCmp('altaLicenciaHabiles').getValue(), mismo_anio);
            if(diasLaborables==0)
            {
                console.log("entra?");
                Ext.getCmp('errorLicencia').setHtml('<span style="color:#cd7373">Error!: Rango incorrecto de fechas</span>');
                Ext.getCmp('errorLicencia').setHidden(false); 
            }
            else
            {
                Ext.getCmp('errorLicencia').setHidden(true);
            }
            if(typeof (diasLaborables) != 'undefined') //Si es undefined, es porque no se ha calculado aun. Bug corregido Mauro
            {
                console.log("Dias laborables: "+diasLaborables);
                console.log("Dias disponibles: "+AppGlobals.diasDisponibles);
                if (diasLaborables > AppGlobals.diasDisponibles) 
                {
                    Ext.getCmp('errorLicencia').setHtml('<span style="color:#cd7373">Atenci&oacute;n!: Esta a punto de cargar m&aacute;s d&iacute;as de los que tiene disponibles.</span>');
                    Ext.getCmp('errorLicencia').setHidden(false); 
                } 
                else
                {
                    if(diasLaborables!=0)
                    {
                        Ext.getCmp('errorLicencia').setHidden(true); 
                    }
                }
                Ext.getCmp('diasLicenciaAltaLicencia').setValue(diasLaborables);
            }
        }
    },

    onSaveAltaLicencia: function(button) 
    {
        var frm = button.up().up().down('form').getForm();
        var me = this;
        if(frm.isValid())        
        {
            var diasLicencia = Ext.getCmp('diasLicenciaAltaLicencia').getValue();
            var tipoLic = Ext.getCmp('comboTipoLicenciaAltaLicencia').getValue();
            if(diasLicencia == 0)
            {
                Ext.Msg.alert('Error', 'Los dias otorgados son incorrectos, cargue otra fecha');
            }
            else
            {
                button.setDisabled(true); //Desactivo el boton guardar para evitar multiples cargas
                button.setText("Espere...");
                console.log('*** clicked the Save button');
                
                frm.submit({
                    url: 'app/proxy.licencias.php',
                    method: 'POST',
                    params: {
                        tarea: 'saveLicencia',
                        tipoLic: tipoLic,
                        diasLicenciaAltaLicencia: diasLicencia,
                        legajo: AppGlobals.legAgente
                    },
                    success: function(frm, action) 
                    {
                        //me.diasDisponibles(tipolic);
                        
                        Ext.toast('Se guard&oacute; la licencia en el sistema', 'Informaci&oacute;n');
                        button.setDisabled(false); //Activo el botón de guardar nuevamente
                        button.setText("Guardar");
                                        
                        me.lookupReference('PersonalFichaLicencias').store.reload();
                        Ext.ComponentQuery.query('AltaLicencia')[0].close();
                    },
                    failure: function(frm, action) 
                    {
                        console.log("***Failure: "+action.response.responseText);
                        var obj = Ext.decode(action.response.responseText);
                        console.log(obj);
                        switch (action.failureType) 
                        {
                            case Ext.form.action.Action.CLIENT_INVALID:
                                Ext.Msg.alert('Error', 'Complete los campos con valores v&aacute;lidos.');
                                break;
                            case Ext.form.action.Action.CONNECT_FAILURE:
                                Ext.Msg.alert('Error', 'Error en la comunicacion con la base de datos. Recargue la p&aacute;gina (F5) e intente nuevamente. ('+action.response.statusText+')');
                                break;
                            case Ext.form.action.Action.SERVER_INVALID:
                                Ext.Msg.alert('Error', action.result.msg);
                            default:
                                Ext.Msg.alert('Error', obj.error);
                        }
                        button.setDisabled(false);//Activo el botón de guardar nuevamente
                        button.setText("Guardar");
                    }
                });
            }
        }
        else
        {
            Ext.Msg.alert('Error', 'Complete correctamente los campos');
        }
    },
    clickLicenciaFeria: function(selectionModel, records) 
    {
        if(AppGlobals.permisos[0].pesta_historial_editar == 1)
        {
            Ext.getCmp('btnDesiste').setDisabled(!records.length);
            Ext.getCmp('btnEliminaLicencia').setDisabled(!records.length);
            Ext.getCmp('btnVaciarDias').setDisabled(!records.length);
            console.log(records);
            //AppGlobals.licencia = recordHist.data;
            //AppGlobals.id_licencia = 101; //Para identificar a licencia compensatoria dias feria
        }
    },
    
    clickLicenciaDT: function(grid, recordHist) 
    {
        if(AppGlobals.permisos[0].pesta_historial_editar == 1)
        {
            Ext.getCmp('btnDesiste').setDisabled(false);
            Ext.getCmp('btnEliminaLicencia').setDisabled(false);
            console.log(recordHist.data);
            AppGlobals.licencia = recordHist.data;
            AppGlobals.id_licencia = 181; //Para identificar a licencia Doble turno dias}
        }
    },    

    onAltaLicencia: function(button) 
    {
        console.log('*** Click Agregar solicitud licencia');
        var win = Ext.widget('AltaLicencia');
        Ext.getCmp('legajoAltaLicencia').setValue(AppGlobals.legAgente);
        win.show();
    },

    desisteLicencia: function(button)
    {
        
        var fichaActiva = this.lookupReference('PersonalFichaLicencia').getActiveTab();
        var reg = fichaActiva.getSelectionModel().getSelection();
        
        var dateDesde = new Date(reg[0].data.desde);
        var dateHasta = new Date(reg[0].data.hasta);
        var tarea = "";
        var licencia = "";
        var sto = "";
        if(fichaActiva.id=='PersonalFichaLicencias')
        {
            licencia = "licencia ";
            tarea = "desisteLicencia";
            sto = Ext.getStore('HLicenciaStore');
        }
        else
        {
            licencia = "dias trabajados ";   
            tarea = "desisteDiasTrabajados";
            sto = Ext.getStore('HDiasTrabajadosStore')
        }
        Ext.Msg.show({
            title:'Desistir',
            msg: 'Desistir '+licencia+' "'+reg[0].data.nombre+'" ('+Ext.Date.format(dateDesde, 'd-m-Y')+' - '+Ext.Date.format(dateHasta, 'd-m-Y')+'). Desea continuar?',
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: function (buttonId)
            {
                if (buttonId==="yes") 
                {
                    Ext.Ajax.request({
                        url: 'app/proxy.licencias.php',
                        params: 
                        {
                            tarea: tarea,
                            tipoLic: reg[0].data.tipo_licencia,
                            idLic: reg[0].data.id
                        },
                        success: function(response, opts) 
                        {
                            console.log("entra al success: "+response.responseText);
                            var obj = Ext.decode(response.responseText);
                            if(obj.success)
                            {
                                Ext.toast(obj.msg, 'Informaci&oacute;n');
                            }
                            else
                            {
                                Ext.Msg.alert("Error", obj.error);
                            }
                            //Recargo stores
                            sto.reload();
                        },
                        failure: function(response, opts) 
                        {
                            console.log("entra al failure: "+response.responseText);
                            var obj = Ext.decode(response.responseText);
                            Ext.Msg.alert("Error", obj.error);
                        }
                    });
                }
            }
        });
    },
    
    eliminarLicencia: function(button)
    {
        console.log("Eliminar licencia");
        var fichaActiva = this.lookupReference('PersonalFichaLicencia').getActiveTab();
        var reg = fichaActiva.getSelectionModel().getSelection();
        
        var dateDesde = new Date(reg[0].data.desde);
        var dateHasta = new Date(reg[0].data.hasta);
        var tarea = "";
        var licencia = "";
        var sto = "";
        if(fichaActiva.id=='PersonalFichaLicencias')
        {
            licencia = "licencia ";
            tarea = "eliminarLicencia";
            sto = Ext.getStore('HLicenciaStore');
        }
        else
        {
            licencia = "dias trabajados ";   
            tarea = "eliminarDiasTrabajados";
            sto = Ext.getStore('HDiasTrabajadosStore')
        }
                
        Ext.Msg.show({
            title:'Eliminar licencia',
            msg: 'Eliminar '+licencia+' "'+reg[0].data.nombre+'" ('+Ext.Date.format(dateDesde, 'd-m-Y')+' - '+Ext.Date.format(dateHasta, 'd-m-Y')+'). Desea continuar?',
            buttons: Ext.Msg.YESNOCANCEL,
            icon: Ext.Msg.QUESTION,
            fn: function (buttonId)
            {
                if (buttonId==="yes") 
                {
                    Ext.Ajax.request({
                        url: 'app/proxy.licencias.php',
                        params: 
                        {
                            tarea: tarea,
                            tipoLic: reg[0].data.tipo_licencia,
                            idLic: reg[0].data.id
                                                                
                        },
                        success: function(response, opts) 
                        {
                            console.log("entra al success: "+response.responseText);
                            var obj = Ext.decode(response.responseText);
                            if(obj.success)
                            {
                                Ext.toast(obj.info, 'Informaci&oacute;n');
                            }
                            else
                            {
                                Ext.Msg.alert("Error", obj.error);
                            }

                            //Recargo stores
                            sto.reload();
                        },
                        failure: function(response, opts) 
                        {
                            console.log("entra al failure: "+response.responseText);
                            var obj = Ext.decode(response.responseText);
                            Ext.Msg.alert("Error", obj.error);
                        }
                    });
                }
            }
        });
    },

    /* Ventana editar solicitud licencia */

    onEditarLicencia: function(button) 
    {
        console.log('*** Click editar licencia');
        var win = Ext.widget('ModificarLicencia');
        //var reg = Ext.getCmp('idPanelHLicencia').getSelectionModel().getSelection();
        var reg = button.up('grid').getSelectionModel().getSelection();
        console.log(reg);
        win.show();
        //Relleno el formulario con los datos
        Ext.getCmp("editTipoLicencia").setValue(reg[0].data.id_tipo_licencia);
        Ext.getCmp('editLegajoLicencia').setValue(AppGlobals.legAgente);
        Ext.getCmp('editCausa').setValue(reg[0].data.causa);
        Ext.getCmp('editExpediente').setValue(reg[0].data.expediente);
        Ext.getCmp('editObservaciones').setValue(reg[0].data.observaciones);
        Ext.getCmp('idEditLicencia').setValue(reg[0].data.id);
        Ext.getCmp('editPierdePresentismo').setValue(reg[0].data.pierde_presentismo=='NO'?false:true);
        
    },

    onSaveEditarLicencia: function (button) 
    {
        console.log('*** Save editar licencia');
        var frm = button.up('window').down('form').getForm();
        
        var sto = this.lookupReference('PersonalFichaLicencias').store;                 
        if (frm.isValid())
        {
            Ext.Msg.show({
                title:'Guardar cambios en la licencia',
                msg: 'Guardar cambios en la licencia?',
                buttons: Ext.Msg.YESNOCANCEL,
                buttonText : 
                {
                    yes : 'Si',
                    no : 'No',
                    cancel : 'Cancelar'
                },
                icon: Ext.Msg.QUESTION,
                fn: function(buttonId)
                {
                    if(buttonId === 'yes')
                    {
                        button.setDisabled(true); //Desactivo el boton para que no haya multiples cargas. Bug corregido Mauro
                        frm.submit({
                        url: 'app/proxy.licencias.php',
                        method: 'POST',
                        params: {
                                tarea: 'editLicencia'
                        },
                        success: function(frm, action) {
                                console.log("Entro al success: "+action.response.responseText);
                                var obj = Ext.decode(action.response.responseText);
                                Ext.toast(obj.msg, 'Informaci&oacute;n');
                                button.setDisabled(false); //Habilito nuevamente el boton
                                Ext.ComponentQuery.query('ModificarLicencia')[0].close();
                                sto.reload();
                                
                        },
                        failure: function(frm, action) {
                                var obj = Ext.decode(action.response.responseText);
                                console.log("Entro al failure: "+action.response.responseText);
                                Ext.Msg.alert('Error', obj.error);
                                button.setDisabled(false); //Habilito nuevamente el boton
                        }
                    });
                    }// end if(buttonId === 'yes')
                }
            });
        }
        else
        {
            Ext.Msg.alert("Error", "Complete correcatamente los campos");
        }
    },

    onCancelarAltaLicencia: function() 
    {
        console.log('*** Cierra ventana de alta licencia');
        Ext.ComponentQuery.query('AltaLicencia')[0].close();
    },

    onCancelarEditarLicencia: function() 
    {
        console.log('*** Cierra ventana de editar licencia');
        Ext.ComponentQuery.query('ModificarLicencia')[0].close();
    },

    vaciarDiasTrabajados: function(button)
    {
        console.log("Vaciar dias trabajados");
        var reg = button.up('grid').getSelectionModel().getSelection();
        console.log(reg);
        Ext.Msg.show({
            title:'Vaciar d&iacute;as trabajados',
            msg: 'Este proceso dejar&aacute; en 0 los d&iacute;as trabajados. Est&aacute; seguro que desea continuar?',
            buttons: Ext.Msg.YESNOCANCEL,
            icon: Ext.Msg.QUESTION,
            fn: function (buttonId)
            {
                if (buttonId==="yes") 
                {
                    Ext.Ajax.request({
                        url: 'app/proxy.licencias.php',
                        params: 
                        {
                            tarea: 'vaciarDiasTrabajados',
                            idLic: reg[0].data.id
                                                                
                        },
                        success: function(response, opts) 
                        {
                            console.log("entra al success: "+response.responseText);
                            var obj = Ext.decode(response.responseText);
                            if(obj.success)
                            {
                                Ext.toast(obj.info, "Informaci&oacute;n");
                            }
                            else
                            {
                                Ext.Msg.alert("Error", obj.error);
                            }
                            Ext.getStore('HDiasTrabajadosStore').reload();
                            
                        },
                        failure: function(response, opts) 
                        {
                            console.log("entra al failure: "+response.responseText);
                            var obj = Ext.decode(response.responseText);
                            Ext.Msg.alert("Error", obj.error);
                            
                        }
                    });
                }
            }
        });
    },

    onClickHistorial: function(t, record, item, index, ex, eOpts) 
    {
        console.log('*** Click en historial ' + index);
        //Bloqueo el boton desiste
        if(AppGlobals.permisos[0].pesta_historial_editar == 1)
        {
            Ext.getCmp('btnDesiste').setDisabled(true);
            Ext.getCmp('btnEliminaLicencia').setDisabled(true);
        }
        //Guardo el id de la licencia en una variable global para poder acceder y eliminarla si el usuario lo desea
        AppGlobals.id_licencia = index; 
        var pnl = Ext.getCmp('pnlHistorialCard');
        var sto;
        console.log(index);
        var nombre_licencia = "";
        
        //Obtengo el nombre de la licencia
        if(index!=0)
        {
            Ext.Ajax.request(
            {      
                url : 'app/proxy.licencias.php',
                method: 'GET',
                async: false,
                params :{
                    idLic : index,
                    tarea: 'getTipoLicenciaById'
                },
                
                success : function(response) 
                {
                    var obj = Ext.decode(response.responseText);
                    nombre_licencia = obj.results[0].nombre;
                },
                failure: function(response)
                {
                    //var obj = Ext.decode(response.responseText);
                    console.log("Entra al failure "+response.responseText);
                    Ext.toast("Error al guardar cambios: "+obj.error, "Error");
                }
            });


            if (index != 10 && index != 18) 
            {
                switch (index) 
                {
                    case 5: //Maternidad
                        pnl.getLayout().setActiveItem(2);
                        Ext.getCmp('hMaternidad').store.load({
                            params: {
                                tipoLic: index,
                                legajo: AppGlobals.legAgente
                            }
                        });
                        break;
                    case 12://Estudio
                        pnl.getLayout().setActiveItem(3);
                        Ext.getCmp('hEstudio').store.load({
                            params: {
                                tipoLic: index,
                                legajo: AppGlobals.legAgente
                            }
                        });
                        break;
                    default: //Todas las demas usan el mismo grid para mostrarse
                        pnl.getLayout().setActiveItem(1);
                        pnl.getLayout().activeItem.header.setTitle(nombre_licencia);
                        Ext.getCmp('hLicencia').store.load({
                            params: {
                                tipoLic: index,
                                legajo: AppGlobals.legAgente
                            }
                        });
                        break;
                        
                }
            } 
            else 
            {
                if(index == 10) //Compensatorias
                {
                    pnl.getLayout().setActiveItem(4);
                    Ext.getCmp('hCompensatoria').store.load({
                        params: {
                            tipoLic: index,
                            legajo: AppGlobals.legAgente,
                            opc: 'dias'
                        }
                    });
                    Ext.getCmp('hCompensatoriaDiasFeria').store.load({
                        params: {
                            tipoLic: index,
                            legajo: AppGlobals.legAgente,
                            opc: 'feria'
                        }
                    });
                }
                else //Doble turno
                {
                    pnl.getLayout().setActiveItem(5);
                    var sto1 = Ext.getStore('HLicenciaStore');
                    sto1.load({
                        params: {
                            tipoLic: index,
                            legajo: AppGlobals.legAgente,
                            opc: 'dias'
                        }
                    });
                    console.log(sto1);
                    var sto2 = Ext.getStore('HDobleTurnoStoreDiasDT');
                    sto2.load({
                        params: {
                            tipoLic: index,
                            legajo: AppGlobals.legAgente,
                            opc: 'DT'
                        }
                    });
                    console.log(sto2);
                }
            }
        }
    }
});